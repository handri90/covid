SELECT *
FROM
(
	SELECT
	hasil_skrining_petugas,
	is_done,
	nik,
	IF(hasil_skrining_petugas='1',
		UNIX_TIMESTAMP(DATE_FORMAT(
			IF(before_after_schedule_vaksin = '1',
				IF(satuan_interval = 'HOUR',
					DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
					DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
				),
				IF(satuan_interval = 'HOUR',
					DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
					DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
				)
			), '%Y-%m-%d %H:%i:%s')
		),''
	) AS timestamp_tanggal_jam_vaksin,
	UNIX_TIMESTAMP() AS timestam_jam_now,
	is_confirm
	FROM daftar_vaksin
	LEFT JOIN jadwal_vaksin ON id_jadwal_vaksin=jadwal_vaksin_id
	WHERE daftar_vaksin.deleted_at IS NULL 
	AND nik IN ('3692581470987456','0852741369874521','1234567890798656','6985321478052147')
) AS tbl
WHERE 
-- if(hasil_skrining_petugas = '1' and is_confirm = '1' and (is_done = '1' or is_done is null),1,0) or 
IF(hasil_skrining_petugas = '1' AND is_confirm = '2',1,0) OR 
IF(hasil_skrining_petugas = '1' AND is_confirm IS NULL AND UNIX_TIMESTAMP() < timestamp_tanggal_jam_vaksin,1,0) OR 
IF(hasil_skrining_petugas = '2',1,0) OR 
IF(hasil_skrining_petugas = '3',1,0)

