SELECT *
FROM (
    SELECT 
    id_peserta,
    peserta.nama,
    nik,
    b.level_user_id,
    GROUP_CONCAT(IFNULL(als_trx_pemeriksaan.id_trx_pemeriksaan,'') ORDER BY als_trx_pemeriksaan.id_trx_pemeriksaan ASC SEPARATOR '|') AS id_trx_pemeriksaan, 
    GROUP_CONCAT(IFNULL(a.level_user_id,'') ORDER BY als_trx_pemeriksaan.id_trx_pemeriksaan ASC SEPARATOR '|') AS level_user_id_pemeriksaan, 
    GROUP_CONCAT(IFNULL(nama_hasil_pemeriksaan,'') ORDER BY als_trx_pemeriksaan.id_trx_pemeriksaan ASC SEPARATOR '|') AS nama_hasil_pemeriksaan, 
    GROUP_CONCAT(IFNULL(nama_jenis_pemeriksaan,'') ORDER BY als_trx_pemeriksaan.id_trx_pemeriksaan ASC SEPARATOR '|') AS nama_jenis_pemeriksaan, 
    GROUP_CONCAT(IFNULL(is_sembuh,'') ORDER BY als_trx_pemeriksaan.id_trx_pemeriksaan ASC SEPARATOR '|') AS is_sembuh, 
    GROUP_CONCAT(IFNULL(is_meninggal,'') ORDER BY als_trx_pemeriksaan.id_trx_pemeriksaan ASC SEPARATOR '|') AS is_meninggal, 
    GROUP_CONCAT(IFNULL(is_probable,'') ORDER BY als_trx_pemeriksaan.id_trx_pemeriksaan ASC SEPARATOR '|') AS is_probable, 
    GROUP_CONCAT(IFNULL(class_badge,'') ORDER BY als_trx_pemeriksaan.id_trx_pemeriksaan ASC SEPARATOR '|') AS class_badge, 
    GROUP_CONCAT(IFNULL(als_trx_pemeriksaan.usulan_faskes,'') ORDER BY als_trx_pemeriksaan.id_trx_pemeriksaan ASC SEPARATOR '|') AS usulan_pemeriksaan_faskes,
    GROUP_CONCAT(IFNULL(DATE_FORMAT(als_trx_pemeriksaan.created_at,'%d-%m-%Y %H:%i:%s'),'') ORDER BY als_trx_pemeriksaan.id_trx_pemeriksaan ASC SEPARATOR '|') AS date_created,
    GROUP_CONCAT(IFNULL(DATE_FORMAT(als_trx_pemeriksaan.updated_at,'%d-%m-%Y %H:%i:%s'),'') ORDER BY als_trx_pemeriksaan.id_trx_pemeriksaan ASC SEPARATOR '|') AS date_updated,
    GROUP_CONCAT(IFNULL(a.nama_lengkap,'') ORDER BY als_trx_pemeriksaan.id_trx_pemeriksaan ASC SEPARATOR '|') AS nama_user_verification,
    c.*,
    'rsud' AS peserta_dari
    FROM peserta
    LEFT JOIN trx_pemeriksaan AS als_trx_pemeriksaan ON id_peserta=peserta_id AND als_trx_pemeriksaan.deleted_at IS NULL
    LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL
    LEFT JOIN jenis_pemeriksaan ON id_jenis_pemeriksaan=als_trx_pemeriksaan.jenis_pemeriksaan_id AND jenis_pemeriksaan.deleted_at IS NULL
    LEFT JOIN USER AS a ON als_trx_pemeriksaan.id_user_created=a.id_user
    LEFT JOIN USER AS b ON peserta.id_user_created=b.id_user
    LEFT JOIN 
	(
	SELECT 
	id_trx_pemeriksaan AS id_trx_pemeriksaan_last,
	peserta_id,
	level_user_id AS level_user_id_last,
	class_badge AS class_badge_last,
	usulan_faskes AS usulan_faskes_pemeriksaan,
	trx_pemeriksaan.jenis_pemeriksaan_id,
	trx_pemeriksaan.hasil_pemeriksaan_id AS hasil_pemeriksaan_id_last,
	is_sembuh AS is_sembuh_last,
	is_meninggal AS is_meninggal_last,
	is_probable AS is_probable_last,
	trx_pemeriksaan.created_at AS created_at_last,
	status_rawat,
	DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') AS last_created_pemeriksaan
	FROM trx_pemeriksaan 
	LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
	LEFT JOIN USER ON id_user=id_user_created 
	WHERE id_trx_pemeriksaan IN (
	    SELECT MAX(id_trx_pemeriksaan) AS id_trx_pemeriksaan 
	    FROM trx_pemeriksaan 
	    WHERE trx_pemeriksaan.deleted_at IS NULL
	    GROUP BY peserta_id
	    ORDER BY id_trx_pemeriksaan DESC
	    ) 
	AND trx_pemeriksaan.deleted_at IS NULL
	) AS c ON c.peserta_id=id_peserta
    WHERE peserta.deleted_at IS NULL
    GROUP BY id_peserta
) AS tbl 
WHERE last_created_pemeriksaan BETWEEN '2021-06-13' AND '2021-07-12'
ORDER BY id_trx_pemeriksaan_last DESC