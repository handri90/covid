SELECT 
                id_peserta_non_puskesmas AS id_peserta,
                nama,
                nik,
                CONCAT(
                    '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other ORDER BY id_trx_pemeriksaan_non_puskesmas DESC SEPARATOR '|'),'|',1),''),'</div>',
                    '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan ORDER BY id_trx_pemeriksaan_non_puskesmas DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan ORDER BY id_trx_pemeriksaan_non_puskesmas DESC SEPARATOR '|'),'|',1),''),'</div>',
                    '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan ORDER BY id_trx_pemeriksaan_non_puskesmas DESC SEPARATOR '|'),'|',1),''),'</span></div>',
                    '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat ORDER BY id_trx_pemeriksaan_non_puskesmas DESC SEPARATOR '|'),'|',1),''),'</span></div>'
                ) AS kolom,
                IF(peserta_id IS NOT NULL,CONCAT('<span class=\'badge badge-light badge-striped badge-striped-left border-left-success\'>Sinkronisasi data oleh Puskesmas ',nama_puskesmas,'</span>'),'') AS sinkronisasi_data
                FROM (
                    SELECT 
                    id_trx_pemeriksaan_non_puskesmas,
                    IF(level_user_id IN ('2','7'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan,
                    IF(level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan,
                    IF(level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other,
                    IF(level_user_id IN ('2','7'),a.class_badge,NULL) AS class_badge,
                    IF(level_user_id IN ('2','7'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan,
                    IF(level_user_id IN ('2','7'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan,
                    IF(level_user_id IN ('2','7'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat,
                    id_peserta_non_puskesmas,
                    peserta_non_puskesmas.nama,
                    nik,
                    peserta_id,
                    c.*,
                    d.nama_puskesmas
                    FROM peserta_non_puskesmas
                    LEFT JOIN
                    (
                    SELECT 
                        id_trx_pemeriksaan_non_puskesmas,
                        peserta_non_puskesmas_id,
                        level_user_id,
                        IF(
                        trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NULL AND hasil_pemeriksaan_id IS NULL,
                            IF(is_sembuh = '1',
                            '<span class=\'badge badge-success\'>Sembuh</span>',
                            IF(is_meninggal = '1',
                            '<span class=\'badge badge-secondary\'>Meninggal</span>',
                            IF(is_probable = '1',
                            '<span class=\'badge badge-secondary\'>Probable</span>',''
                            )
                            )
                            ),
                            IF(
                            trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NULL,
                            '<span class=\'badge badge-secondary\'>Hasil Belum Keluar</span>',
                            IF(
                                trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NOT NULL,
                                IF(
                                class_badge = '1',
                                    CONCAT('<span class=\'badge badge-success\'>',nama_hasil_pemeriksaan,'</span>'),
                                IF(class_badge = '2',
                                    CONCAT('<span class=\'badge badge-warning\'>',nama_hasil_pemeriksaan,'</span>'),''
                                )
                                ),
                                ''
                            )
                            )
                        ) AS nama_hasil_pemeriksaan,
                        IF(usulan_faskes = '0',
                        '<span class=\'badge badge-danger\'>Usulan Faskes : SWAB PCR</span>',
                        IF(usulan_faskes = '1',
                        '<span class=\'badge badge-danger\'>Usulan Faskes : Isolasi Mandiri</span>',
                        IF(usulan_faskes = '2',
                        '<span class=\'badge badge-success\'>Usulan Faskes : Bebas Isolasi Mandiri</span>',
                        IF(usulan_faskes = '3',
                        '<span class=\'badge badge-warning\'>Usulan Faskes : Rujuk Rumah Sakit</span>',''
                        )
                        )
                        )
                        ) AS nama_jenis_pemeriksaan_other,
                        IF(trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NOT NULL,nama_jenis_pemeriksaan,'') AS nama_jenis_pemeriksaan,
                        IFNULL(is_sembuh,''),
                        IFNULL(is_meninggal,''),
                        IFNULL(is_probable,''),
                        IFNULL(class_badge,'') AS class_badge,
                        IFNULL(usulan_faskes,'') AS usulan_pemeriksaan,
                        IFNULL(DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_buat_pemeriksaan,
                        IFNULL(DATE_FORMAT(trx_pemeriksaan_non_puskesmas.updated_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_ubah_pemeriksaan,
                        IFNULL(nama_lengkap,'') AS nama_asal_pembuat
                    FROM trx_pemeriksaan_non_puskesmas
                    LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL
                    LEFT JOIN jenis_pemeriksaan ON id_jenis_pemeriksaan=trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id AND jenis_pemeriksaan.deleted_at IS NULL
                    INNER JOIN USER ON id_user_created=id_user
                    ) AS a ON a.peserta_non_puskesmas_id=id_peserta_non_puskesmas
                    LEFT JOIN 
                    (
                    SELECT 
                        id_trx_pemeriksaan_non_puskesmas AS id_trx_pemeriksaan_non_puskesmas_last,
                        peserta_non_puskesmas_id,
                        level_user_id AS level_user_id_last,
                        class_badge AS class_badge_last,
                        usulan_faskes AS usulan_faskes_pemeriksaan_last,
                        trx_pemeriksaan_non_puskesmas.created_at AS created_at_last,
                        DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') AS created_pemeriksaan_last,
                        status_rawat AS status_rawat_last,
                        trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id AS jenis_pemeriksaan_id_last,
                        hasil_pemeriksaan_id AS hasil_pemeriksaan_id_last,
                        is_sembuh AS is_sembuh_last,
                        is_meninggal AS is_meninggal_last,
                        is_probable AS is_probable_last
                    FROM trx_pemeriksaan_non_puskesmas 
                    LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                    LEFT JOIN USER ON id_user=id_user_created 
                    WHERE id_trx_pemeriksaan_non_puskesmas IN 
                    (
                        SELECT MAX(id_trx_pemeriksaan_non_puskesmas) AS id_trx_pemeriksaan_non_puskesmas 
                        FROM trx_pemeriksaan_non_puskesmas 
                        WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                        GROUP BY peserta_non_puskesmas_id
                        ORDER BY id_trx_pemeriksaan_non_puskesmas DESC
                    ) 
                    AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS c ON c.peserta_non_puskesmas_id=id_peserta_non_puskesmas
                    LEFT JOIN
                    (
                    SELECT id_peserta,nama_puskesmas
                    FROM peserta
                    JOIN USER ON id_user_created=id_user
                    JOIN master_puskesmas ON puskesmas_id=id_master_puskesmas
                    ) AS d ON d.id_peserta=peserta_id
                    WHERE peserta_non_puskesmas.id_user_created='11' AND peserta_non_puskesmas.deleted_at IS NULL
                    ORDER BY id_trx_pemeriksaan_non_puskesmas ASC
                ) AS tbl
                WHERE created_pemeriksaan_last BETWEEN '2021-06-14' AND '2021-07-13'
                GROUP BY id_peserta_non_puskesmas
                ORDER BY id_trx_pemeriksaan_non_puskesmas_last DESC