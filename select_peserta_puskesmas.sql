SELECT
id_peserta,
nama,
nik,
CONCAT(
'<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_faskes ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
'<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_faskes ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_faskes ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
'<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_faskes ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
'<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_faskes ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
) AS kolom_faskes,

SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_faskes ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS nama_hasil_pemeriksaan_faskes,
SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_faskes ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS nama_jenis_pemeriksaan_faskes,
SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_faskes ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS nama_jenis_pemeriksaan_other_faskes,
SUBSTRING_INDEX(GROUP_CONCAT(class_badge_faskes ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS class_badge_faskes,
SUBSTRING_INDEX(GROUP_CONCAT(usulan_pemeriksaan_faskes ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS usulan_pemeriksaan_faskes,
SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_faskes ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS tanggal_buat_pemeriksaan_faskes,
SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_faskes ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS nama_asal_pembuat_faskes,
                
SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_labkesda ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS nama_hasil_pemeriksaan_labkesda,
SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_labkesda ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS nama_jenis_pemeriksaan_labkesda,
SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_labkesda ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS nama_jenis_pemeriksaan_other_labkesda,
SUBSTRING_INDEX(GROUP_CONCAT(class_badge_labkesda ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS class_badge_labkesda,
SUBSTRING_INDEX(GROUP_CONCAT(usulan_pemeriksaan_labkesda ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS usulan_pemeriksaan_labkesda,
SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_labkesda ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS tanggal_buat_pemeriksaan_labkesda,
SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_labkesda ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS nama_asal_pembuat_labkesda,

SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_rs ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS nama_hasil_pemeriksaan_rs,
SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_rs ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS nama_jenis_pemeriksaan_rs,
SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_rs ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS nama_jenis_pemeriksaan_other_rs,
SUBSTRING_INDEX(GROUP_CONCAT(class_badge_rs ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS class_badge_rs,
SUBSTRING_INDEX(GROUP_CONCAT(usulan_pemeriksaan_rs ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS usulan_pemeriksaan_rs,
SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_rs ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS tanggal_buat_pemeriksaan_rs,
SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_rs ORDER BY id_trx_pemeriksaan DESC SEPARATOR '|'),'|',1) AS nama_asal_pembuat_rs
FROM (
    SELECT 
	id_trx_pemeriksaan,
	IF(a.level_user_id IN ('2','7'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_faskes,
	IF(a.level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_faskes,
	IF(a.level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_faskes,
	IF(a.level_user_id IN ('2','7'),a.class_badge,NULL) AS class_badge_faskes,
	IF(a.level_user_id IN ('2','7'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_faskes,
	IF(a.level_user_id IN ('2','7'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_faskes,
	IF(a.level_user_id IN ('2','7'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_faskes,
	
	IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ("2"),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_labkesda,
	IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ("2"),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_labkesda,
	IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ("2"),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_labkesda,
	IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ("2"),a.class_badge,NULL) AS class_badge_labkesda,
	IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ("2"),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_labkesda,
	IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ("2"),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_labkesda,
	IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ("2"),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_labkesda,
	
	IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ("2","3"),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_rs,
	IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ("2","3"),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_rs,
	IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ("2","3"),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_rs,
	IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ("2","3"),a.class_badge,NULL) AS class_badge_rs,
	IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ("2","3"),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_rs,
	IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ("2","3"),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_rs,
	IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ("2","3"),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_rs,
	id_peserta,
	peserta.nama,
	nik,
	kategori_data,
	c.*,
	usr.level_user_id as level_user_id_peserta
    FROM peserta
    LEFT JOIN
    (
	SELECT 
	    id_trx_pemeriksaan,
	    peserta_id,
	    level_user_id,
	    IF(
		trx_pemeriksaan.jenis_pemeriksaan_id IS NULL AND hasil_pemeriksaan_id IS NULL,
		    IF(is_sembuh = '1',
			'<span class=\'badge badge-success\'>Sembuh</span>',
		    IF(is_meninggal = '1',
			'<span class=\'badge badge-secondary\'>Meninggal</span>',
		    IF(is_probable = '1',
			'<span class=\'badge badge-secondary\'>Probable</span>',''
		    )
		    )
		    ),
		    IF(
			trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NULL,
			'<span class=\'badge badge-secondary\'>Hasil Belum Keluar</span>',
			IF(
			    trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NOT NULL,
			    IF(
				class_badge = '1',
				    CONCAT('<span class=\'badge badge-success\'>',nama_hasil_pemeriksaan,'</span>'),
				IF(class_badge = '2',
				    CONCAT('<span class=\'badge badge-warning\'>',nama_hasil_pemeriksaan,'</span>'),''
				)
			    ),
			    ''
			)
		    )
	    ) AS nama_hasil_pemeriksaan,
	    IF(usulan_faskes = '0',
		'<span class=\'badge badge-danger\'>Usulan Faskes : SWAB PCR</span>',
	    IF(usulan_faskes = '1',
		'<span class=\'badge badge-danger\'>Usulan Faskes : Isolasi Mandiri</span>',
	    IF(usulan_faskes = '2',
		'<span class=\'badge badge-success\'>Usulan Faskes : Bebas Isolasi Mandiri</span>',
	    IF(usulan_faskes = '3',
		'<span class=\'badge badge-warning\'>Usulan Faskes : Rujuk Rumah Sakit</span>',''
	    )
	    )
	    )
	    ) AS nama_jenis_pemeriksaan_other,
	    IF(trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL,nama_jenis_pemeriksaan,'') AS nama_jenis_pemeriksaan,
	    IFNULL(is_sembuh,''),
	    IFNULL(is_meninggal,''),
	    IFNULL(is_probable,''),
	    IFNULL(class_badge,'') AS class_badge,
	    IFNULL(usulan_faskes,'') AS usulan_pemeriksaan,
	    IFNULL(DATE_FORMAT(trx_pemeriksaan.created_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_buat_pemeriksaan,
	    IFNULL(DATE_FORMAT(trx_pemeriksaan.updated_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_ubah_pemeriksaan,
	    IFNULL(nama_lengkap,'') AS nama_asal_pembuat
	FROM trx_pemeriksaan
	LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL
	LEFT JOIN jenis_pemeriksaan ON id_jenis_pemeriksaan=trx_pemeriksaan.jenis_pemeriksaan_id AND jenis_pemeriksaan.deleted_at IS NULL
	INNER JOIN USER ON id_user_created=id_user
    ) AS a ON a.peserta_id=id_peserta
    LEFT JOIN 
    (
	SELECT 
	    id_trx_pemeriksaan AS id_trx_pemeriksaan_last,
	    peserta_id,
	    level_user_id AS level_user_id_last,
	    class_badge AS class_badge_last,
	    usulan_faskes AS usulan_faskes_pemeriksaan_last,
	    trx_pemeriksaan.created_at AS created_at_last,
	    DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') AS created_pemeriksaan_last,
	    status_rawat AS status_rawat_last,
	    trx_pemeriksaan.jenis_pemeriksaan_id AS jenis_pemeriksaan_id_last,
	    hasil_pemeriksaan_id,is_sembuh AS is_sembuh_last,
	    is_meninggal AS is_meninggal_last,
	    is_probable AS is_probable_last
	FROM trx_pemeriksaan 
	LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
	LEFT JOIN USER ON id_user=id_user_created 
	WHERE id_trx_pemeriksaan IN 
	(
	    SELECT MAX(id_trx_pemeriksaan) AS id_trx_pemeriksaan 
	    FROM trx_pemeriksaan 
	    WHERE trx_pemeriksaan.deleted_at IS NULL
	    GROUP BY peserta_id
	    ORDER BY id_trx_pemeriksaan DESC
	) 
	AND trx_pemeriksaan.deleted_at IS NULL
    ) AS c ON c.peserta_id=id_peserta
    left join user as usr on peserta.id_user_created=id_user
    WHERE peserta.deleted_at IS NULL
    ORDER BY id_trx_pemeriksaan ASC
) AS tbl
WHERE IF(kategori_data IS NOT NULL,(kategori_data = '2' OR (kategori_data = '1' AND class_badge_last IS NOT NULL)),1)
AND 
            (
                (
                    IF(level_user_id_last = '2' AND (usulan_faskes_pemeriksaan_last = '0' AND (class_badge_last = '2' OR class_badge_last IS NULL)),1,0)
                )
                OR 
                (
                    (level_user_id_last = '4' AND jenis_pemeriksaan_id_last != '1') OR 
                    (level_user_id_last = '4' AND jenis_pemeriksaan_id_last = '1' AND (class_badge_last IS NULL OR class_badge_last = '1'))
                )
                OR 
                (
                    (level_user_id_last = '3')
                )
                OR 
                (
                    (level_user_id_last = '6')
                )
                OR
                (
                    level_user_id_peserta = '6'
                ) 
                OR
                (
                    level_user_id_peserta = '6'
                )
            )
AND created_pemeriksaan_last BETWEEN '2021-06-14' AND '2021-07-13'
GROUP BY id_peserta
ORDER BY id_trx_pemeriksaan_last DESC