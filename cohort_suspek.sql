SELECT 
	nama,
	nik,
	DATE_FORMAT(tanggal_lahir,"%d/%m/%Y") AS tanggal_lahir,
	jenis_kelamin,
	nomor_telepon,
	IF(kelurahan_master_wilayah_id != "109",
		CONCAT(alamat_domisili," RT. ",
			IF(master_rt_domisili_id IS NULL,
				rt_domisili,
				rt
			),
			" ",klasifikasi," ",
			nama_wilayah
		),
		"Wilayah Kobar Lainnya"
	) AS alamat_domisili_custom,
	CONCAT(alamat_ktp," RT. ",rt_ktp) AS alamat_ktp_custom,
	pekerjaan_inti,
	IFNULL(nama_puskesmas,"") AS nama_puskesmas,
	"Suspek" AS kriteria_pasien,
	b.jumlah_swab AS jumlah_swab,
	kode_sample
FROM peserta
inner join
(
	SELECT id_trx_pemeriksaan AS id_trx_pemeriksaan_last,peserta_id,kode_sample
	FROM trx_pemeriksaan 
	WHERE id_trx_pemeriksaan IN 
	(
	    SELECT MAX(id_trx_pemeriksaan) AS id_trx_pemeriksaan 
	    FROM trx_pemeriksaan 
	    WHERE trx_pemeriksaan.deleted_at IS NULL
	    GROUP BY peserta_id
	    ORDER BY id_trx_pemeriksaan DESC
	 ) 
	 AND trx_pemeriksaan.deleted_at IS null
	 and jenis_pemeriksaan_id = '1'
	 AND id_user_created = '6'
	 and hasil_pemeriksaan_id is null
) as a on a.peserta_id=id_peserta
inner join master_wilayah on id_master_wilayah=kelurahan_master_wilayah_id
left join master_rt on id_master_rt=master_rt_domisili_id
LEFT JOIN master_puskesmas ON id_master_puskesmas=puskesmas_id
left join
(
	select count(*) as jumlah_swab,peserta_id
	from trx_pemeriksaan
	where trx_pemeriksaan.deleted_at is null and jenis_pemeriksaan_id = '1'
	group by peserta_id
) as b on b.peserta_id=id_peserta
where peserta.deleted_at is null