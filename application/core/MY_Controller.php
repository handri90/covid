<?php defined('BASEPATH') or exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

    protected $title_main;
    protected $header_main = 'template_admin/header';
    protected $sidebar_main = 'template_admin/sidebar';
    protected $index_main = 'template_admin/main';
    protected $footer_main = 'template_admin/footer';

    function __construct()
    {
        parent::__construct();
        $this->load->model('menu/menu_model', 'menu_model');
        $this->load->model('privilage_level/privilage_level_model', 'privilage_level_model');
    }

    public function _remap($method, $params)
    {
        $class_name = $this->router->class;
        $level_user_id = $this->session->userdata('level_user_id');
        if ($class_name != "request") {
            if ($class_name != "Custom404") {
                $menu = $this->menu_model->get(
                    array(
                        "join" => array(
                            "privilage_level_menu" => "id_menu=menu_id"
                        ),
                        "where" => array(
                            "nama_class" => $class_name,
                            "level_user_id" => $level_user_id
                        )
                    ),
                    "row"
                );

                $check_privilage = $this->privilage_level_model->get(
                    array(
                        "where" => array(
                            "level_user_id" => $level_user_id,
                            "menu_id" => $menu->id_menu
                        )
                    ),
                    "row"
                );

                if ($check_privilage->view_content != 1) {
                    $this->page_error();
                }
            }

            if (method_exists($this, $method)) {
                return call_user_func_array(array($this, $method), $params);
            }
        } else {
            if (method_exists($this, $method)) {
                return call_user_func_array(array($this, $method), $params);
            }
            $this->page_error();
        }
    }

    public function menu($parent_id = 0, $level_user_id)
    {
        $str = "";
        $master = $this->menu_model->query("SELECT id_menu,nama_menu,nama_module,nama_class,class_icon,IFNULL(a.jml_child,0) AS jml_child FROM `menu` LEFT JOIN (SELECT COUNT(*) AS jml_child, id_parent_menu FROM menu WHERE `menu`.`deleted_at` IS NULL GROUP BY id_parent_menu) AS a ON a.id_parent_menu=id_menu WHERE menu.`id_parent_menu` = " . $parent_id . " AND `menu`.`deleted_at` IS NULL AND id_menu IN (SELECT menu_id FROM privilage_level_menu WHERE level_user_id  = " . $level_user_id . " AND view_content = 1 AND deleted_at IS NULL) ORDER BY order_menu")->result_array();

        for ($i = 0; $i < count($master); $i++) {
            $child = "";
            $link = "";
            $li_class = "nav-item";
            $a_class = "nav_link";
            $icon_class = "";
            if ($parent_id == 0) {
                if ($master[$i]['jml_child'] == 0) {
                    $link = "<a href='" . site_url($master[$i]['nama_module']) . "' class='nav-link'><i class='" . $master[$i]['class_icon'] . "'></i><span>" . $master[$i]['nama_menu'] . "</span></a>";
                } else {
                    $li_class .= " nav-item-submenu";
                    $link = "<a href='#' class='nav-link'><i class='" . $master[$i]['class_icon'] . "'></i><span>" . $master[$i]['nama_menu'] . "</span></a>";
                    $child = "<ul class='nav nav-group-sub'>" . $this->menu($master[$i]['id_menu'], $level_user_id) . "</ul>";
                }
            } else {
                if ($master[$i]['jml_child'] == 0) {
                    $child = "<a href='" . site_url($master[$i]['nama_module']) . "' class='nav-link'>" . $master[$i]['nama_menu'] . "</a>";
                } else {
                    $li_class .= " nav-item-submenu";
                    $link = "<a href='#' class='nav-link'><span>" . $master[$i]['nama_menu'] . "</span></a>";
                    $child = "<ul class='nav nav-group-sub'>" . $this->menu($master[$i]['id_menu'], $level_user_id) . "</ul>";
                }
            }

            $str .= "<li class='" . $li_class . "'>" . $link;
            $str .= $child;
            $str .= "</li>";
        }

        return $str;
    }

    public function execute($page, $data = array())
    {
        $CI = &get_instance();
        $CI->load->library('session');
        if ($CI->session->userdata("is_logged_in")) {
            $level_user_id = $CI->session->userdata('level_user_id');
            $data['sidebar'] = $this->menu(0, $level_user_id);
            $data['title_main'] = $this->config->item('APP_TITLE');
            $data['header_main'] = $this->load->view($this->header_main, $data, true);
            $data['sidebar_main'] = $this->load->view($this->sidebar_main, $data, true);
            $data['footer_main'] = $this->load->view($this->footer_main, $data, true);
            $data['content_main'] = $this->load->view($page, $data, true);
            $this->load->view($this->index_main, $data);
        } else {
            redirect("Login");
        }
    }

    public function ipost($name = "")
    {
        return $this->input->post($name, true);
    }

    public function iget($name = "")
    {
        return $this->input->get($name, true);
    }

    public function datetime()
    {
        return $this->config->item('date_now');
    }

    public function upload_file($name_field = "", $upload_path = "", $upload_menu = "", $type_upload = "image")
    {
        if ((!file_exists($upload_path)) && !(is_dir($upload_path))) {
            mkdir($upload_path, 0777);
        }
        $config = array();
        $status = "";
        if ($type_upload == 'image') {
            $filename = md5(uniqid(rand(), true));
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['file_name'] = $filename;
        } else {
            $filename = md5(uniqid(rand(), true));
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'docx|pdf|xlsx|ppt|xls';
            $config['file_name'] = $filename;
        }

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($name_field)) {
            $status = array('error' => $this->upload->display_errors());
        } else {
            $status = array('data' => $this->upload->data());
            if ($upload_menu == "berita") {
                $this->thumbnailResizeImage($status['data']['file_name'], $upload_path);
            }
        }

        return $status;
    }

    public function thumbnailResizeImage($filename, $filepath)
    {
        $source_path = $filepath . '/' . $filename;
        $target_path = $filepath;

        if ((!file_exists($target_path . "/thumbnail")) && !(is_dir($target_path . "/thumbnail"))) {
            mkdir($target_path . "/thumbnail");
        }

        $config = array(
            array(
                'image_library' => 'gd2',
                'source_image' => $source_path,
                'new_image' => $target_path . "/thumbnail/" . $filename,
                'maintain_ratio' => TRUE,
                'width' => 230
            ),
            array(
                'image_library' => 'gd2',
                'source_image' => $source_path,
                'new_image' => $target_path . "/" . $filename,
                'maintain_ratio' => TRUE,
                'width' => 660
            )
        );

        $this->load->library('image_lib', $config[0]);
        foreach ($config as $item) {
            $this->image_lib->initialize($item);
            if (!$this->image_lib->resize()) {
                return false;
            }
            $this->image_lib->clear();
        }
    }

    public function send_notification_mobile($title, $pesan, $token, $id_daftar_vaksin)
    {
        // $url = 'https://fcm.googleapis.com/fcm/send';

        // // Put your Server Key here
        // $apiKey = "AAAAFjsIwBs:APA91bGuFwbJ6-Sas4hdIeHWu51TGRa3AfeW1ph_L9P1mqzOK9HH_pWlrqYYE8oWoeLUcW4osJYM2_IGyjeH5K2EXNu3zK-14XsAYebCHwe-wa-qWAnOY7j_35sjrIdpfhV9yLH3ZR0L";

        // // Compile headers in one variable
        // $headers = array(
        //     'Authorization:key=' . $apiKey,
        //     'Content-Type:application/json'
        // );

        // // Create the api body
        // $apiBody = [
        //     'notification' => [
        //         'title' => $title,
        //         'body' => $pesan,
        //         'channel_id' => rand(),
        //         //Action/Activity - Optional
        //     ], // optional - In Seconds
        //     //'to' => '/topics/mytargettopic'
        //     'registration_ids' => [$token],
        //     // 'to' => 'evM31_n3Tt-1X_CBJX-zOJ:APA91bGSNl8NKGTZbTAX6F42gEIfrDZeOvu0s_EsKkLo5AJTQ-cv_PjkXldRArX11jXnI_-yk0L5tF1k3-1der6m1H5FF4rtJrDp3NnjPEt7OPrqddxizU3X1YVH6WxzLC0TUNc_25uO'
        //     'data' => array(
        //         "id_daftar_vaksin" => $id_daftar_vaksin,
        //     )
        // ];

        // // Initialize curl with the prepared headers and body
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, $url);
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($apiBody));

        // // Execute call and save result
        // $result = curl_exec($ch);
        // // Close curl after call
        // curl_close($ch);

        // return $result;
    }

    public function page_error()
    {
        redirect('404_override');
    }

    public function is_login()
    {
        if (!$this->session->userdata("is_logged_in")) {
            redirect('Login');
        }
    }
}
