<?php

class Trx_pemeriksaan_non_puskesmas_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "trx_pemeriksaan_non_puskesmas";
        $this->primary_id = "id_trx_pemeriksaan_non_puskesmas";
    }
}
