<?php

class Trx_pemeriksaan_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "trx_pemeriksaan";
        $this->primary_id = "id_trx_pemeriksaan";
    }
}
