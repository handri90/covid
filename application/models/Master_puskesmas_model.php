<?php

class Master_puskesmas_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "master_puskesmas";
        $this->primary_id = "id_master_puskesmas";
    }
}
