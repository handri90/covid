<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Import_peserta_dinkes extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model("peserta/master_wilayah_model", "master_wilayah_model");
        $this->load->model("peserta/peserta_model", "peserta_model");
        $this->load->model("trx_pemeriksaan_model");
        $this->load->model("master_rt_model");
        $this->load->model("status_peserta/trx_terkonfirmasi_model", "trx_terkonfirmasi_model");
        $this->load->model("peserta_rilis/trx_terkonfirmasi_rilis_model", "trx_terkonfirmasi_rilis_model");
    }

    public function index()
    {
        if (empty($_FILES)) {
            $data['breadcrumb'] = [['link' => false, 'content' => 'Import Peserta', 'is_active' => true]];
            $this->execute('form_backup', $data);
        } else {

            $input_name = 'file_excel';
            $upload_file = $this->upload_file($input_name, $this->config->item('path_excell'), "", "excell");

            $bool_upload_error = false;
            $bool_tanggal_pengambilan_swab = false;
            $tanggal_pengambilan_swab_row = 0;
            $bool_tanggal_hasil_swab = false;
            $tanggal_hasil_swab_row = 0;
            $bool_tanggal_lahir = false;
            $tanggal_lahir_row = 0;
            $bool_jenis_kelamin = false;
            $jenis_kelamin_row = 0;
            $bool_kelurahan = false;
            $kelurahan_row = 0;
            $bool_rt = false;
            $rt_row = 0;
            $bool_hasil_pemeriksaan = false;
            $hasil_pemeriksaan_row = 0;

            if (!isset($upload_file['error'])) {
                require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel.php');

                $excelreader = new PHPExcel_Reader_Excel2007();

                $objPHPExcel = PHPExcel_IOFactory::load('./' . $this->config->item('path_excell') . '/' . $upload_file['data']['file_name']);
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                $objWriter->save(str_replace('.xls', '.xlsx', './' . $this->config->item('path_excell') . '/' . $upload_file['data']['file_name']));

                chmod(str_replace('.xls', '.xlsx', './' . $this->config->item('path_excell') . '/' . $upload_file['data']['file_name']), 0777);

                $loadexcel = $excelreader->load(str_replace('.xls', '.xlsx', './' . $this->config->item('path_excell') . '/' . $upload_file['data']['file_name']));

                $sheet = $loadexcel->getActiveSheet();
                $worksheet = $objPHPExcel->getSheet(0);
                $lastRow = $worksheet->getHighestRow();

                $database_pekerjaan = array("Polisi", "TNI", "ASN", "BUMN / BANK", "Tenaga Kesehatan", "Dosen / Guru", "Pelajar/Mahasiswa", "Ibu Rumah Tangga", "Ulama / Pendeta /  Rohaniawan", "Swasta", "Wiraswasta", "Umum / Keluarga / Pensiunan / Unidentifi", "Lainnya");

                for ($row = 1; $row <= $lastRow; $row++) {
                    if ($row > 1) {
                        if($worksheet->getCell('A' . $row)->getValue() != ""){
                            $nomor_urut = $worksheet->getCell('A' . $row)->getValue();
                            $nik = $worksheet->getCell('B' . $row)->getValue();
                            $nama = $worksheet->getCell('C' . $row)->getValue();
                            $jenis_kelamin = $worksheet->getCell('D' . $row)->getValue();
                            $tanggal_lahir = $worksheet->getCell('G' . $row)->getValue();
                            $hp = $worksheet->getCell('E' . $row)->getValue();
                            $alamat = $worksheet->getCell('I' . $row)->getValue();
                            $rt = $worksheet->getCell('K' . $row)->getValue();
                            $desa_kelurahan = $worksheet->getCell('M' . $row)->getValue();
                            $tanggal_pengambilan_swab_val = $worksheet->getCell('N' . $row)->getValue();
                            $tanggal_hasil_swab_val = $worksheet->getCell('O' . $row)->getValue();
                            $hasil_pemeriksaan = $worksheet->getCell('P' . $row)->getValue();
                            $tanggal_sembuh_val = $worksheet->getCell('Q' . $row)->getValue();
                            $tanggal_meninggal_val = $worksheet->getCell('R' . $row)->getValue();
    
                            $tanggal_lahir_convert = ($tanggal_lahir - 25569) * 86400;
                            $tanggal_lahir_val = gmdate("Y-m-d", $tanggal_lahir_convert);
    
                            $tanggal_pengambilan_swab_convert = ($tanggal_pengambilan_swab_val - 25569) * 86400;
                            $tanggal_pengambilan_swab_val = gmdate("Y-m-d", $tanggal_pengambilan_swab_convert);
    
                            $tanggal_hasil_swab_convert = ($tanggal_hasil_swab_val - 25569) * 86400;
                            $tanggal_hasil_swab_val = gmdate("Y-m-d", $tanggal_hasil_swab_convert);
    
                            if ($tanggal_sembuh_val) {
                                $tanggal_sembuh_convert = ($tanggal_sembuh_val - 25569) * 86400;
                                $tanggal_sembuh_val = gmdate("Y-m-d", $tanggal_sembuh_convert);
                            }
    
                            if ($tanggal_meninggal_val) {
                                $tanggal_meninggal_convert = ($tanggal_meninggal_val - 25569) * 86400;
                                $tanggal_meninggal_val = gmdate("Y-m-d", $tanggal_meninggal_convert);
                            }
    
                            $master_wilayah = $this->master_wilayah_model->get(
                                array(
                                    "where" => array(
                                        "nama_wilayah" => ucwords(strtolower(trim($desa_kelurahan))),
                                    ),
                                    "where_false" => "klasifikasi IN ('DESA','KEL')"
                                ),
                                "row"
                            );
    
                            $tanggal_lahir_expl = explode("-", $tanggal_lahir_val);
                            $tanggal_pengambilan_swab_val_expl = explode("-", $tanggal_pengambilan_swab_val);
                            $tanggal_hasil_swab_val_expl = explode("-", $tanggal_hasil_swab_val);
    
                            $status_rt = false;
                            if ($master_wilayah) {
                                if ($rt) {
                                    $get_data_master_rt = $this->master_rt_model->get(
                                        array(
                                            "where" => array(
                                                "master_wilayah_id" => $master_wilayah->id_master_wilayah,
                                                "rt" => $rt
                                            )
                                        ),
                                        "row"
                                    );
                                    if ($get_data_master_rt) {
                                        $status_rt = true;
                                    } else {
                                        $status_rt = false;
                                    }
                                } else {
                                    $get_data_master_rt = $this->master_rt_model->get(
                                        array(
                                            "where" => array(
                                                "master_wilayah_id" => $master_wilayah->id_master_wilayah,
                                                "rt" => "00"
                                            )
                                        ),
                                        "row"
                                    );
                                    if ($get_data_master_rt) {
                                        $status_rt = true;
                                    } else {
                                        $status_rt = false;
                                    }
                                }
                            }
    
                            if (count($tanggal_lahir_expl) == "3") {
                                //jika tanggal lahir benar
                                $checkbirthdate = checkdate((int) $tanggal_lahir_expl[1], (int) $tanggal_lahir_expl[2], (int) $tanggal_lahir_expl[0]);
    
                                if ($checkbirthdate) {
                                    //jika tanggal lahir benar
                                    if (in_array($jenis_kelamin, array("L", "P"))) {
                                        //jika jenis kelamin benar
                                        if (isset($master_wilayah)) {
                                            //jika desa/kelurahan benar
    
                                            if ($status_rt) {
                                                if (count($tanggal_pengambilan_swab_val_expl) == "3") {
                                                    //jika tanggal lahir benar
                                                    $checkambilswabdate = checkdate((int) $tanggal_pengambilan_swab_val_expl[1], (int) $tanggal_pengambilan_swab_val_expl[2], (int) $tanggal_pengambilan_swab_val_expl[0]);
    
                                                    if ($checkambilswabdate) {
    
                                                        if (count($tanggal_hasil_swab_val_expl) == "3") {
                                                            //jika tanggal lahir benar
                                                            $checkambilhasilswabdate = checkdate((int) $tanggal_hasil_swab_val_expl[1], (int) $tanggal_hasil_swab_val_expl[2], (int) $tanggal_hasil_swab_val_expl[0]);
    
                                                            if ($checkambilhasilswabdate) {
                                                                if (in_array($hasil_pemeriksaan, array("POSITIF", "NEGATIF"))) {
                                                                    //jika hasil pemeriksaan benar
                                                                    $bool_tanggal_pengambilan_swab = false;
                                                                    $tanggal_pengambilan_swab_row = 0;
                                                                    $bool_tanggal_hasil_swab = false;
                                                                    $tanggal_hasil_swab_row = 0;
                                                                    $bool_tanggal_lahir = false;
                                                                    $tanggal_lahir_row = 0;
                                                                    $bool_jenis_kelamin = false;
                                                                    $jenis_kelamin_row = 0;
                                                                    $bool_kelurahan = false;
                                                                    $kelurahan_row = 0;
                                                                    $bool_hasil_pemeriksaan = false;
                                                                    $hasil_pemeriksaan_row = 0;
                                                                    $bool_rt = false;
                                                                    $rt_row = 0;
                                                                } else {
                                                                    //jika hasil pemeriksaan benar
                                                                    $bool_hasil_pemeriksaan = true;
                                                                    $hasil_pemeriksaan_row = $nomor_urut;
                                                                    break;
                                                                }
                                                            } else {
                                                                $bool_tanggal_hasil_swab = true;
                                                                $tanggal_hasil_swab_row = $nomor_urut;
                                                                break;
                                                            }
                                                        } else {
                                                            $bool_tanggal_hasil_swab = true;
                                                            $tanggal_hasil_swab_row = $nomor_urut;
                                                            break;
                                                        }
                                                    } else {
                                                        $bool_tanggal_pengambilan_swab = true;
                                                        $tanggal_pengambilan_swab_row = $nomor_urut;
                                                        break;
                                                    }
                                                } else {
                                                    $bool_tanggal_pengambilan_swab = true;
                                                    $tanggal_pengambilan_swab_row = $nomor_urut;
                                                    break;
                                                }
                                            } else {
                                                $bool_rt = true;
                                                $rt_row = $nomor_urut;
                                                break;
                                            }
                                        } else {
                                            //jika desa/kelurahan salah
                                            $bool_kelurahan = true;
                                            $kelurahan_row = $nomor_urut . " " . $nama;
                                            break;
                                        }
                                    } else {
                                        //jika jenis kelamin salah
                                        $bool_jenis_kelamin = true;
                                        $jenis_kelamin_row = $nomor_urut;
                                        break;
                                    }
                                } else {
                                    //tanggal lahir salah
                                    $bool_tanggal_lahir = true;
                                    $tanggal_lahir_row = $nomor_urut;
                                    break;
                                }
                            } else {
                                //tangal lahir salah
                                $bool_tanggal_lahir = true;
                                $tanggal_lahir_row = $nomor_urut;
                                break;
                            }
                        }
                    }
                }

                if ($bool_tanggal_lahir) {
                    $this->session->set_flashdata('message', 'Penulisan Tanggal Lahir Salah pada nomor urut ' . $tanggal_lahir_row);
                    $this->session->set_flashdata('error', 'danger');
                } else if ($bool_jenis_kelamin) {
                    $this->session->set_flashdata('message', 'Penulisan Jenis Kelamin Salah pada nomor urut ' . $jenis_kelamin_row);
                    $this->session->set_flashdata('error', 'danger');
                } else if ($bool_rt) {
                    $this->session->set_flashdata('message', 'Data RT tidak ada di database, pada nomor urut ' . $rt_row);
                    $this->session->set_flashdata('error', 'danger');
                } else if ($bool_kelurahan) {
                    $this->session->set_flashdata('message', 'Penulisan Kelurahan Salah pada nomor urut ' . $kelurahan_row);
                    $this->session->set_flashdata('error', 'danger');
                } else if ($bool_tanggal_pengambilan_swab) {
                    $this->session->set_flashdata('message', 'Penulisan Tanggal Pengambilan SWAB salah pada nomor urut ' . $tanggal_pengambilan_swab_row);
                    $this->session->set_flashdata('error', 'danger');
                } else if ($bool_tanggal_hasil_swab) {
                    $this->session->set_flashdata('message', 'Penulisan Tanggal Hasil SWAB salah pada nomor urut ' . $tanggal_hasil_swab_row);
                    $this->session->set_flashdata('error', 'danger');
                } else if ($bool_hasil_pemeriksaan) {
                    $this->session->set_flashdata('message', 'Penulisan Hasil Pemeriksaan Salah pada nomor urut ' . $hasil_pemeriksaan_row);
                    $this->session->set_flashdata('error', 'danger');
                } else {
                    for ($row = 1; $row <= $lastRow; $row++) {
                        if ($row > 1) {
                            if($worksheet->getCell('A' . $row)->getValue() != ""){

                                $nomor_urut = $worksheet->getCell('A' . $row)->getValue();
                                $nik = $worksheet->getCell('B' . $row)->getValue();
                                $nama = $worksheet->getCell('C' . $row)->getValue();
                                $jenis_kelamin = $worksheet->getCell('D' . $row)->getValue();
                                $tanggal_lahir = $worksheet->getCell('G' . $row)->getValue();
                                $hp = $worksheet->getCell('E' . $row)->getValue();
                                $alamat = $worksheet->getCell('I' . $row)->getValue();
                                $rt = $worksheet->getCell('K' . $row)->getValue();
                                $desa_kelurahan = $worksheet->getCell('M' . $row)->getValue();
                                $tanggal_pengambilan_swab_val = $worksheet->getCell('N' . $row)->getValue();
                                $tanggal_hasil_swab_val = $worksheet->getCell('O' . $row)->getValue();
                                $hasil_pemeriksaan = $worksheet->getCell('P' . $row)->getValue();
                                $tanggal_sembuh_val = $worksheet->getCell('Q' . $row)->getValue();
                                $tanggal_meninggal_val = $worksheet->getCell('R' . $row)->getValue();
                                $status_rilis = $worksheet->getCell('AE' . $row)->getValue();
    
                                $tanggal_lahir_convert = ($tanggal_lahir - 25569) * 86400;
                                $tanggal_lahir_val = gmdate("Y-m-d", $tanggal_lahir_convert);
    
                                $tanggal_pengambilan_swab_convert = ($tanggal_pengambilan_swab_val - 25569) * 86400;
                                $tanggal_pengambilan_swab_val = gmdate("Y-m-d", $tanggal_pengambilan_swab_convert);
    
                                $tanggal_hasil_swab_convert = ($tanggal_hasil_swab_val - 25569) * 86400;
                                $tanggal_hasil_swab_val = gmdate("Y-m-d", $tanggal_hasil_swab_convert);
    
                                if ($tanggal_sembuh_val) {
                                    $tanggal_sembuh_convert = ($tanggal_sembuh_val - 25569) * 86400;
                                    $tanggal_sembuh_val = gmdate("Y-m-d", $tanggal_sembuh_convert);
                                }
    
                                if ($tanggal_meninggal_val) {
                                    $tanggal_meninggal_convert = ($tanggal_meninggal_val - 25569) * 86400;
                                    $tanggal_meninggal_val = gmdate("Y-m-d", $tanggal_meninggal_convert);
                                }
    
                                $master_wilayah = $this->master_wilayah_model->get(
                                    array(
                                        "where" => array(
                                            "nama_wilayah" => ucwords(strtolower(trim($desa_kelurahan))),
                                        ),
                                        "where_false" => "klasifikasi IN ('DESA','KEL')"
                                    ),
                                    "row"
                                );
    
                                $cek_peserta_by_nik = $this->peserta_model->get(
                                    array(
                                        "where" => array(
                                            "nik" => $nik
                                        )
                                    ),
                                    "row"
                                );
    
                                $asal_sampel = "2";
    
                                if ($hasil_pemeriksaan == "POSITIF") {
                                    $hasil_pemeriksaan = "1";
                                } else if ($hasil_pemeriksaan == "NEGATIF") {
                                    $hasil_pemeriksaan = "2";
                                }
    
                                $id_master_rt = "";
                                if ($master_wilayah) {
                                    if ($rt) {
                                        $get_data_master_rt = $this->master_rt_model->get(
                                            array(
                                                "where" => array(
                                                    "master_wilayah_id" => $master_wilayah->id_master_wilayah,
                                                    "rt" => $rt
                                                )
                                            ),
                                            "row"
                                        );
                                    } else {
                                        $get_data_master_rt = $this->master_rt_model->get(
                                            array(
                                                "where" => array(
                                                    "master_wilayah_id" => $master_wilayah->id_master_wilayah,
                                                    "rt" => "00"
                                                )
                                            ),
                                            "row"
                                        );
                                    }
                                    $id_master_rt = $get_data_master_rt->id_master_rt;
                                }
    
                                if ($nik != "") {
                                    if ($cek_peserta_by_nik) {
                                        $cek_trx_pemeriksaan_positif = $this->trx_pemeriksaan_model->get(
                                            array(
                                                "where" => array(
                                                    "peserta_id" => $cek_peserta_by_nik->id_peserta
                                                ),
                                                "order_by" => array(
                                                    "id_trx_pemeriksaan" => "DESC"
                                                ),
                                                "limit" => "1"
                                            ),
                                            "row"
                                        );
    
                                        $status = NULL;
                                        if ($cek_trx_pemeriksaan_positif) {
                                            if ($cek_trx_pemeriksaan_positif->jenis_pemeriksaan_id == "1" && $cek_trx_pemeriksaan_positif->hasil_pemeriksaan_id == "1") {
                                                $status = $cek_trx_pemeriksaan_positif->id_trx_pemeriksaan;
                                            } else {
                                                $data_trx_pemeriksaan = array(
                                                    "peserta_id"             => $cek_peserta_by_nik->id_peserta,
                                                    "jenis_pemeriksaan_id"   => "1",
                                                    "hasil_pemeriksaan_id"   => $hasil_pemeriksaan,
                                                    "kode_sample"            => NULL,
                                                    "from_excel_import_rssi" => "2",
                                                    "created_at"             => $tanggal_pengambilan_swab_val . " " . date("H:i:s"),
                                                    "updated_at"             => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                    "id_user_created"        => "29",
                                                );
    
                                                $status = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);
                                            }
                                        } else {
                                            $data_trx_pemeriksaan = array(
                                                "peserta_id"             => $cek_peserta_by_nik->id_peserta,
                                                "jenis_pemeriksaan_id"   => "1",
                                                "hasil_pemeriksaan_id"   => $hasil_pemeriksaan,
                                                "kode_sample"            => NULL,
                                                "from_excel_import_rssi" => "2",
                                                "created_at"             => $tanggal_pengambilan_swab_val . " " . date("H:i:s"),
                                                "updated_at"             => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                "id_user_created"        => "29",
                                            );
    
                                            $status = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);
                                        }
    
                                        if ($hasil_pemeriksaan == "1") {
                                            $cek_trx_terkonfirmasi_positif = $this->trx_terkonfirmasi_model->get(
                                                array(
                                                    "where" => array(
                                                        "peserta_id" => $cek_peserta_by_nik->id_peserta,
                                                        "trx_pemeriksaan_id" => $status,
                                                    ),
                                                    "order_by" => array(
                                                        "id_trx_terkonfirmasi" => "DESC"
                                                    ),
                                                    "limit" => "1"
                                                ),
                                                "row"
                                            );
    
                                            if ($cek_trx_terkonfirmasi_positif) {
                                                if ($cek_trx_terkonfirmasi_positif->status != "1") {
                                                    $data_trx_terkonfirmasi = array(
                                                        "peserta_id"             => $cek_peserta_by_nik->id_peserta,
                                                        "from_excel_import_rssi" => "2",
                                                        "status"                 => "1",
                                                        "trx_pemeriksaan_id"     => $status,
                                                        "created_at"             => $tanggal_pengambilan_swab_val . " " . date("H:i:s"),
                                                        "updated_at"             => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                        "id_user_created"        => "29",
                                                    );
    
                                                    $status_terkonfirmasi = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi);
    
                                                    if ($status_rilis == "rilis") {
                                                        $data_trx_terkonfirmasi_rilis = array(
                                                            "trx_terkonfirmasi_id"  => $status_terkonfirmasi,
                                                            "peserta_id"            => $cek_peserta_by_nik->id_peserta,
                                                            "tanggal_terkonfirmasi" => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                            "status"                => "1",
                                                            "trx_pemeriksaan_id"    => $status,
                                                            "created_at"            => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                            "updated_at"            => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                            "id_user_created"       => "3"
                                                        );
    
                                                        $status_terkonfirmasi_rilis = $this->trx_terkonfirmasi_rilis_model->save($data_trx_terkonfirmasi_rilis);
                                                    }
                                                } else if ($cek_trx_terkonfirmasi_positif->status != "1") {
                                                    if ($status_rilis == "rilis") {
                                                        $data_trx_terkonfirmasi_rilis = array(
                                                            "trx_terkonfirmasi_id"  => $cek_trx_terkonfirmasi_positif->id_trx_terkonfirmasi,
                                                            "peserta_id"            => $cek_peserta_by_nik->id_peserta,
                                                            "tanggal_terkonfirmasi" => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                            "status"                => "1",
                                                            "trx_pemeriksaan_id"    => $status,
                                                            "created_at"            => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                            "updated_at"            => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                            "id_user_created"       => "3"
                                                        );
    
                                                        $status_terkonfirmasi_rilis = $this->trx_terkonfirmasi_rilis_model->save($data_trx_terkonfirmasi_rilis);
                                                    }
                                                }
                                            } else {
                                                $data_trx_terkonfirmasi = array(
                                                    "peserta_id"             => $cek_peserta_by_nik->id_peserta,
                                                    "from_excel_import_rssi" => "2",
                                                    "status"                 => "1",
                                                    "trx_pemeriksaan_id"     => $status,
                                                    "created_at"             => $tanggal_pengambilan_swab_val . " " . date("H:i:s"),
                                                    "updated_at"             => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                    "id_user_created"        => "29",
                                                );
    
                                                $status_terkonfirmasi = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi);
    
                                                if ($status_rilis == "rilis") {
                                                    $data_trx_terkonfirmasi_rilis = array(
                                                        "trx_terkonfirmasi_id"  => $status_terkonfirmasi,
                                                        "peserta_id"            => $cek_peserta_by_nik->id_peserta,
                                                        "tanggal_terkonfirmasi" => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                        "status"                => "1",
                                                        "trx_pemeriksaan_id"    => $status,
                                                        "created_at"            => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                        "updated_at"            => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                        "id_user_created"       => "3"
                                                    );
    
                                                    $status_terkonfirmasi_rilis = $this->trx_terkonfirmasi_rilis_model->save($data_trx_terkonfirmasi_rilis);
                                                }
                                            }
                                        }
    
                                        //jika sembuh 
                                        if ($tanggal_sembuh_val) {
                                            if ($cek_trx_pemeriksaan_positif) {
                                                if ($cek_trx_pemeriksaan_positif->is_sembuh != '1') {
                                                    $data_trx_pemeriksaan_sembuh = array(
                                                        "peserta_id"             => $cek_peserta_by_nik->id_peserta,
                                                        "jenis_pemeriksaan_id"   => NULL,
                                                        "hasil_pemeriksaan_id"   => NULL,
                                                        "kode_sample"            => NULL,
                                                        "from_excel_import_rssi" => "2",
                                                        "is_sembuh"              => "1",
                                                        "created_at"             => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                        "updated_at"             => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                        "id_user_created"        => "3",
                                                    );
    
                                                    $status_pemeriksaan_sembuh = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan_sembuh);
    
                                                    $data_trx_terkonfirmasi_sembuh = array(
                                                        "peserta_id"             => $cek_peserta_by_nik->id_peserta,
                                                        "from_excel_import_rssi" => "2",
                                                        "status"                 => "2",
                                                        "trx_pemeriksaan_id"     => $status_pemeriksaan_sembuh,
                                                        "created_at"             => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                        "updated_at"             => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                        "id_user_created"        => "3",
                                                    );
    
                                                    $status_terkonfirmasi_sembuh = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi_sembuh);
    
                                                    if ($status_rilis == "rilis") {
                                                        $data_trx_terkonfirmasi_rilis_sembuh = array(
                                                            "trx_terkonfirmasi_id"  => $status_terkonfirmasi_sembuh,
                                                            "peserta_id"            => $cek_peserta_by_nik->id_peserta,
                                                            "tanggal_terkonfirmasi" => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                            "status"                => "2",
                                                            "trx_pemeriksaan_id"    => $status_pemeriksaan_sembuh,
                                                            "created_at"            => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                            "updated_at"            => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                            "id_user_created"       => "3"
                                                        );
    
                                                        $status_terkonfirmasi_rilis_sembuh = $this->trx_terkonfirmasi_rilis_model->save($data_trx_terkonfirmasi_rilis_sembuh);
                                                    }
                                                } else if ($cek_trx_pemeriksaan_positif->is_sembuh == "1") {
                                                    $status_pemeriksaan_sembuh = $cek_trx_pemeriksaan_positif->id_trx_pemeriksaan;
    
                                                    $data_trx_terkonfirmasi_sembuh = array(
                                                        "peserta_id"             => $cek_peserta_by_nik->id_peserta,
                                                        "from_excel_import_rssi" => "2",
                                                        "status"                 => "2",
                                                        "trx_pemeriksaan_id"     => $status_pemeriksaan_sembuh,
                                                        "created_at"             => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                        "updated_at"             => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                        "id_user_created"        => "3",
                                                    );
    
                                                    $status_terkonfirmasi_sembuh = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi_sembuh);
    
                                                    if ($status_rilis == "rilis") {
                                                        $data_trx_terkonfirmasi_rilis_sembuh = array(
                                                            "trx_terkonfirmasi_id"  => $status_terkonfirmasi_sembuh,
                                                            "peserta_id"            => $cek_peserta_by_nik->id_peserta,
                                                            "tanggal_terkonfirmasi" => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                            "status"                => "2",
                                                            "trx_pemeriksaan_id"    => $status_pemeriksaan_sembuh,
                                                            "created_at"            => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                            "updated_at"            => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                            "id_user_created"       => "3"
                                                        );
    
                                                        $status_terkonfirmasi_rilis_sembuh = $this->trx_terkonfirmasi_rilis_model->save($data_trx_terkonfirmasi_rilis_sembuh);
                                                    }
                                                }
                                            } else {
                                                $data_trx_pemeriksaan_sembuh = array(
                                                    "peserta_id"             => $cek_peserta_by_nik->id_peserta,
                                                    "jenis_pemeriksaan_id"   => NULL,
                                                    "hasil_pemeriksaan_id"   => NULL,
                                                    "kode_sample"            => NULL,
                                                    "from_excel_import_rssi" => "2",
                                                    "is_sembuh"              => "1",
                                                    "created_at"             => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                    "updated_at"             => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                    "id_user_created"        => "3",
                                                );
    
                                                $status_pemeriksaan_sembuh = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan_sembuh);
    
                                                $data_trx_terkonfirmasi_sembuh = array(
                                                    "peserta_id"             => $cek_peserta_by_nik->id_peserta,
                                                    "from_excel_import_rssi" => "2",
                                                    "status"                 => "2",
                                                    "trx_pemeriksaan_id"     => $status_pemeriksaan_sembuh,
                                                    "created_at"             => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                    "updated_at"             => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                    "id_user_created"        => "3",
                                                );
    
                                                $status_terkonfirmasi_sembuh = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi_sembuh);
    
                                                if ($status_rilis == "rilis") {
                                                    $data_trx_terkonfirmasi_rilis_sembuh = array(
                                                        "trx_terkonfirmasi_id"  => $status_terkonfirmasi_sembuh,
                                                        "peserta_id"            => $cek_peserta_by_nik->id_peserta,
                                                        "tanggal_terkonfirmasi" => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                        "status"                => "2",
                                                        "trx_pemeriksaan_id"    => $status_pemeriksaan_sembuh,
                                                        "created_at"            => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                        "updated_at"            => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                        "id_user_created"       => "3"
                                                    );
    
                                                    $status_terkonfirmasi_rilis_sembuh = $this->trx_terkonfirmasi_rilis_model->save($data_trx_terkonfirmasi_rilis_sembuh);
                                                }
                                            }
                                        }
    
                                        //jika meninggal 
                                        if ($tanggal_meninggal_val) {
                                            if ($cek_trx_pemeriksaan_positif) {
                                                if ($cek_trx_pemeriksaan_positif->is_meninggal != '1') {
                                                    $data_trx_pemeriksaan_meninggal = array(
                                                        "peserta_id"             => $cek_peserta_by_nik->id_peserta,
                                                        "jenis_pemeriksaan_id"   => NULL,
                                                        "hasil_pemeriksaan_id"   => NULL,
                                                        "kode_sample"            => NULL,
                                                        "from_excel_import_rssi" => "2",
                                                        "is_meninggal"              => "1",
                                                        "created_at"             => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                        "updated_at"             => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                        "id_user_created"        => "3",
                                                    );
    
                                                    $status_pemeriksaan_meninggal = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan_meninggal);
    
                                                    $data_trx_terkonfirmasi_meninggal = array(
                                                        "peserta_id"             => $cek_peserta_by_nik->id_peserta,
                                                        "from_excel_import_rssi" => "2",
                                                        "status"                 => "3",
                                                        "trx_pemeriksaan_id"     => $status_pemeriksaan_meninggal,
                                                        "created_at"             => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                        "updated_at"             => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                        "id_user_created"        => "3",
                                                    );
    
                                                    $status_terkonfirmasi_meninggal = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi_meninggal);
    
                                                    if ($status_rilis == "rilis") {
                                                        $data_trx_terkonfirmasi_rilis_meninggal = array(
                                                            "trx_terkonfirmasi_id"  => $status_terkonfirmasi_meninggal,
                                                            "peserta_id"            => $cek_peserta_by_nik->id_peserta,
                                                            "tanggal_terkonfirmasi" => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                            "status"                => "3",
                                                            "trx_pemeriksaan_id"    => $status_pemeriksaan_meninggal,
                                                            "created_at"            => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                            "updated_at"            => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                            "id_user_created"       => "3"
                                                        );
    
                                                        $status_terkonfirmasi_rilis_meninggal = $this->trx_terkonfirmasi_rilis_model->save($data_trx_terkonfirmasi_rilis_meninggal);
                                                    }
                                                } else if ($cek_trx_pemeriksaan_positif->is_meninggal == "1") {
                                                    $status_pemeriksaan_meninggal = $cek_trx_pemeriksaan_positif->id_trx_pemeriksaan;
    
                                                    $data_trx_terkonfirmasi_meninggal = array(
                                                        "peserta_id"             => $cek_peserta_by_nik->id_peserta,
                                                        "from_excel_import_rssi" => "2",
                                                        "status"                 => "3",
                                                        "trx_pemeriksaan_id"     => $status_pemeriksaan_meninggal,
                                                        "created_at"             => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                        "updated_at"             => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                        "id_user_created"        => "3",
                                                    );
    
                                                    $status_terkonfirmasi_meninggal = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi_meninggal);
    
                                                    if ($status_rilis == "rilis") {
                                                        $data_trx_terkonfirmasi_rilis_meninggal = array(
                                                            "trx_terkonfirmasi_id"  => $status_terkonfirmasi_meninggal,
                                                            "peserta_id"            => $cek_peserta_by_nik->id_peserta,
                                                            "tanggal_terkonfirmasi" => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                            "status"                => "3",
                                                            "trx_pemeriksaan_id"    => $status_pemeriksaan_meninggal,
                                                            "created_at"            => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                            "updated_at"            => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                            "id_user_created"       => "3"
                                                        );
    
                                                        $status_terkonfirmasi_rilis_meninggal = $this->trx_terkonfirmasi_rilis_model->save($data_trx_terkonfirmasi_rilis_meninggal);
                                                    }
                                                }
                                            } else {
                                                $data_trx_pemeriksaan_meninggal = array(
                                                    "peserta_id"             => $cek_peserta_by_nik->id_peserta,
                                                    "jenis_pemeriksaan_id"   => NULL,
                                                    "hasil_pemeriksaan_id"   => NULL,
                                                    "kode_sample"            => NULL,
                                                    "from_excel_import_rssi" => "2",
                                                    "is_meninggal"              => "1",
                                                    "created_at"             => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                    "updated_at"             => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                    "id_user_created"        => "3",
                                                );
    
                                                $status_pemeriksaan_meninggal = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan_meninggal);
    
                                                $data_trx_terkonfirmasi_meninggal = array(
                                                    "peserta_id"             => $cek_peserta_by_nik->id_peserta,
                                                    "from_excel_import_rssi" => "2",
                                                    "status"                 => "3",
                                                    "trx_pemeriksaan_id"     => $status_pemeriksaan_meninggal,
                                                    "created_at"             => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                    "updated_at"             => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                    "id_user_created"        => "3",
                                                );
    
                                                $status_terkonfirmasi_meninggal = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi_meninggal);
    
                                                if ($status_rilis == "rilis") {
                                                    $data_trx_terkonfirmasi_rilis_meninggal = array(
                                                        "trx_terkonfirmasi_id"  => $status_terkonfirmasi_meninggal,
                                                        "peserta_id"            => $cek_peserta_by_nik->id_peserta,
                                                        "tanggal_terkonfirmasi" => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                        "status"                => "3",
                                                        "trx_pemeriksaan_id"    => $status_pemeriksaan_meninggal,
                                                        "created_at"            => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                        "updated_at"            => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                        "id_user_created"       => "3"
                                                    );
    
                                                    $status_terkonfirmasi_rilis_meninggal = $this->trx_terkonfirmasi_rilis_model->save($data_trx_terkonfirmasi_rilis_meninggal);
                                                }
                                            }
                                        }
                                    } else {
                                        $data_peserta_insert = [
                                            "nama"                        => $nama,
                                            "nik"                         => $nik,
                                            "tanggal_lahir"               => $tanggal_lahir_val,
                                            "jenis_kelamin"               => $jenis_kelamin,
                                            "alamat_ktp"                  => $alamat,
                                            "rt_ktp"                      => $rt,
                                            "is_copy_ktp"                 => "1",
                                            "alamat_domisili"             => $alamat,
                                            "rt_domisili"                 => $rt,
                                            "master_rt_domisili_id"       => $id_master_rt,
                                            "kelurahan_master_wilayah_id" => $master_wilayah->id_master_wilayah,
                                            "nomor_telepon"               => $hp,
                                            "master_puskesmas_id"         => $master_wilayah->puskesmas_id,
                                            "kategori_data"               => $asal_sampel,
                                            "from_excel_import_rssi"      => "2",
                                            "created_at"                  => $tanggal_pengambilan_swab_val . " " . date("H:i:s"),
                                            "id_user_created"             => "29",
                                        ];
    
    
                                        $id_peserta = $this->peserta_model->save($data_peserta_insert);
    
                                        //input trx pemeriksaan
                                        $data_trx_pemeriksaan = array(
                                            "peserta_id"             => $id_peserta,
                                            "jenis_pemeriksaan_id"   => "1",
                                            "hasil_pemeriksaan_id"   => $hasil_pemeriksaan,
                                            "kode_sample"            => NULL,
                                            "from_excel_import_rssi" => "2",
                                            "created_at"             => $tanggal_pengambilan_swab_val . " " . date("H:i:s"),
                                            "updated_at"             => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                            "id_user_created"        => "29",
                                        );
    
                                        $status = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);
    
                                        if ($hasil_pemeriksaan == "1") {
                                            $data_trx_terkonfirmasi = array(
                                                "peserta_id"             => $id_peserta,
                                                "from_excel_import_rssi" => "2",
                                                "status"                 => "1",
                                                "trx_pemeriksaan_id"     => $status,
                                                "created_at"             => $tanggal_pengambilan_swab_val . " " . date("H:i:s"),
                                                "updated_at"             => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                "id_user_created"        => "29",
                                            );
    
                                            $status_terkonfirmasi = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi);
    
                                            if ($status_rilis == "rilis") {
                                                $data_trx_terkonfirmasi_rilis = array(
                                                    "trx_terkonfirmasi_id"  => $status_terkonfirmasi,
                                                    "peserta_id"            => $id_peserta,
                                                    "tanggal_terkonfirmasi" => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                    "status"                => "1",
                                                    "trx_pemeriksaan_id"    => $status,
                                                    "created_at"            => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                    "updated_at"            => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                    "id_user_created"       => "3"
                                                );
    
                                                $status_terkonfirmasi_rilis = $this->trx_terkonfirmasi_rilis_model->save($data_trx_terkonfirmasi_rilis);
                                            }
                                        }
    
                                        //jika sembuh 
                                        if ($tanggal_sembuh_val) {
                                            $data_trx_pemeriksaan_sembuh = array(
                                                "peserta_id"             => $id_peserta,
                                                "jenis_pemeriksaan_id"   => NULL,
                                                "hasil_pemeriksaan_id"   => NULL,
                                                "kode_sample"            => NULL,
                                                "from_excel_import_rssi" => "2",
                                                "is_sembuh"              => "1",
                                                "created_at"             => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                "updated_at"             => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                "id_user_created"        => "3",
                                            );
    
                                            $status_pemeriksaan_sembuh = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan_sembuh);
    
                                            $data_trx_terkonfirmasi_sembuh = array(
                                                "peserta_id"             => $id_peserta,
                                                "from_excel_import_rssi" => "2",
                                                "status"                 => "2",
                                                "trx_pemeriksaan_id"     => $status_pemeriksaan_sembuh,
                                                "created_at"             => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                "updated_at"             => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                "id_user_created"        => "3",
                                            );
    
                                            $status_terkonfirmasi_sembuh = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi_sembuh);
    
                                            if ($status_rilis == "rilis") {
                                                $data_trx_terkonfirmasi_rilis_sembuh = array(
                                                    "trx_terkonfirmasi_id"  => $status_terkonfirmasi_sembuh,
                                                    "peserta_id"            => $id_peserta,
                                                    "tanggal_terkonfirmasi" => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                    "status"                => "2",
                                                    "trx_pemeriksaan_id"    => $status_pemeriksaan_sembuh,
                                                    "created_at"            => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                    "updated_at"            => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                    "id_user_created"       => "3"
                                                );
    
                                                $status_terkonfirmasi_rilis_sembuh = $this->trx_terkonfirmasi_rilis_model->save($data_trx_terkonfirmasi_rilis_sembuh);
                                            }
                                        }
    
                                        //jika meninggal 
                                        if ($tanggal_meninggal_val) {
                                            $data_trx_pemeriksaan_meninggal = array(
                                                "peserta_id"             => $id_peserta,
                                                "jenis_pemeriksaan_id"   => NULL,
                                                "hasil_pemeriksaan_id"   => NULL,
                                                "kode_sample"            => NULL,
                                                "from_excel_import_rssi" => "2",
                                                "is_meninggal"              => "1",
                                                "created_at"             => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                "updated_at"             => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                "id_user_created"        => "3",
                                            );
    
                                            $status_pemeriksaan_meninggal = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan_meninggal);
    
                                            $data_trx_terkonfirmasi_meninggal = array(
                                                "peserta_id"             => $id_peserta,
                                                "from_excel_import_rssi" => "2",
                                                "status"                 => "3",
                                                "trx_pemeriksaan_id"     => $status_pemeriksaan_meninggal,
                                                "created_at"             => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                "updated_at"             => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                "id_user_created"        => "3",
                                            );
    
                                            $status_terkonfirmasi_meninggal = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi_meninggal);
    
                                            if ($status_rilis == "rilis") {
                                                $data_trx_terkonfirmasi_rilis_meninggal = array(
                                                    "trx_terkonfirmasi_id"  => $status_terkonfirmasi_meninggal,
                                                    "peserta_id"            => $id_peserta,
                                                    "tanggal_terkonfirmasi" => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                    "status"                => "3",
                                                    "trx_pemeriksaan_id"    => $status_pemeriksaan_meninggal,
                                                    "created_at"            => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                    "updated_at"            => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                    "id_user_created"       => "3"
                                                );
    
                                                $status_terkonfirmasi_rilis_meninggal = $this->trx_terkonfirmasi_rilis_model->save($data_trx_terkonfirmasi_rilis_meninggal);
                                            }
                                        }
                                    }
                                } else {
                                    $data_peserta_insert = [
                                        "nama"                        => $nama,
                                        "nik"                         => $nik,
                                        "tanggal_lahir"               => $tanggal_lahir_val,
                                        "jenis_kelamin"               => $jenis_kelamin,
                                        "alamat_ktp"                  => $alamat,
                                        "rt_ktp"                      => $rt,
                                        "is_copy_ktp"                 => "1",
                                        "alamat_domisili"             => $alamat,
                                        "rt_domisili"                 => $rt,
                                        "master_rt_domisili_id"       => $id_master_rt,
                                        "kelurahan_master_wilayah_id" => $master_wilayah->id_master_wilayah,
                                        "nomor_telepon"               => $hp,
                                        "master_puskesmas_id"         => $master_wilayah->puskesmas_id,
                                        "kategori_data"               => $asal_sampel,
                                        "from_excel_import_rssi"      => "2",
                                        "created_at"                  => $tanggal_pengambilan_swab_val . " " . date("H:i:s"),
                                        "id_user_created"             => "29",
                                    ];
    
                                    $id_peserta = $this->peserta_model->save($data_peserta_insert);
    
                                    //input trx pemeriksaan
                                    $data_trx_pemeriksaan = array(
                                        "peserta_id"             => $id_peserta,
                                        "jenis_pemeriksaan_id"   => "1",
                                        "hasil_pemeriksaan_id"   => $hasil_pemeriksaan,
                                        "kode_sample"            => NULL,
                                        "from_excel_import_rssi" => "2",
                                        "created_at"             => $tanggal_pengambilan_swab_val . " " . date("H:i:s"),
                                        "updated_at"             => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                        "id_user_created"        => "29",
                                    );
    
                                    $status = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);
    
                                    if ($hasil_pemeriksaan == "1") {
                                        $data_trx_terkonfirmasi = array(
                                            "peserta_id"             => $id_peserta,
                                            "from_excel_import_rssi" => "2",
                                            "status"                 => "1",
                                            "trx_pemeriksaan_id"     => $status,
                                            "created_at"             => $tanggal_pengambilan_swab_val . " " . date("H:i:s"),
                                            "updated_at"             => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                            "id_user_created"        => "29",
                                        );
    
                                        $status_terkonfirmasi = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi);
    
                                        if ($status_rilis == "rilis") {
                                            $data_trx_terkonfirmasi_rilis = array(
                                                "trx_terkonfirmasi_id"  => $status_terkonfirmasi,
                                                "peserta_id"            => $id_peserta,
                                                "tanggal_terkonfirmasi" => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                "status"                => "1",
                                                "trx_pemeriksaan_id"    => $status,
                                                "created_at"            => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                "updated_at"            => $tanggal_hasil_swab_val . " " . date("H:i:s"),
                                                "id_user_created"       => "3"
                                            );
    
                                            $status_terkonfirmasi_rilis = $this->trx_terkonfirmasi_rilis_model->save($data_trx_terkonfirmasi_rilis);
                                        }
                                    }
    
                                    //jika sembuh 
                                    if ($tanggal_sembuh_val) {
                                        $data_trx_pemeriksaan_sembuh = array(
                                            "peserta_id"             => $id_peserta,
                                            "jenis_pemeriksaan_id"   => NULL,
                                            "hasil_pemeriksaan_id"   => NULL,
                                            "kode_sample"            => NULL,
                                            "from_excel_import_rssi" => "2",
                                            "is_sembuh"              => "1",
                                            "created_at"             => $tanggal_sembuh_val . " " . date("H:i:s"),
                                            "updated_at"             => $tanggal_sembuh_val . " " . date("H:i:s"),
                                            "id_user_created"        => "3",
                                        );
    
                                        $status_pemeriksaan_sembuh = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan_sembuh);
    
                                        $data_trx_terkonfirmasi_sembuh = array(
                                            "peserta_id"             => $id_peserta,
                                            "from_excel_import_rssi" => "2",
                                            "status"                 => "2",
                                            "trx_pemeriksaan_id"     => $status_pemeriksaan_sembuh,
                                            "created_at"             => $tanggal_sembuh_val . " " . date("H:i:s"),
                                            "updated_at"             => $tanggal_sembuh_val . " " . date("H:i:s"),
                                            "id_user_created"        => "3",
                                        );
    
                                        $status_terkonfirmasi_sembuh = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi_sembuh);
    
                                        if ($status_rilis == "rilis") {
                                            $data_trx_terkonfirmasi_rilis_sembuh = array(
                                                "trx_terkonfirmasi_id"  => $status_terkonfirmasi_sembuh,
                                                "peserta_id"            => $id_peserta,
                                                "tanggal_terkonfirmasi" => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                "status"                => "2",
                                                "trx_pemeriksaan_id"    => $status_pemeriksaan_sembuh,
                                                "created_at"            => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                "updated_at"            => $tanggal_sembuh_val . " " . date("H:i:s"),
                                                "id_user_created"       => "3"
                                            );
    
                                            $status_terkonfirmasi_rilis_sembuh = $this->trx_terkonfirmasi_rilis_model->save($data_trx_terkonfirmasi_rilis_sembuh);
                                        }
                                    }
    
                                    //jika meninggal 
                                    if ($tanggal_meninggal_val) {
                                        $data_trx_pemeriksaan_meninggal = array(
                                            "peserta_id"             => $id_peserta,
                                            "jenis_pemeriksaan_id"   => NULL,
                                            "hasil_pemeriksaan_id"   => NULL,
                                            "kode_sample"            => NULL,
                                            "from_excel_import_rssi" => "2",
                                            "is_meninggal"              => "1",
                                            "created_at"             => $tanggal_meninggal_val . " " . date("H:i:s"),
                                            "updated_at"             => $tanggal_meninggal_val . " " . date("H:i:s"),
                                            "id_user_created"        => "3",
                                        );
    
                                        $status_pemeriksaan_meninggal = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan_meninggal);
    
                                        $data_trx_terkonfirmasi_meninggal = array(
                                            "peserta_id"             => $id_peserta,
                                            "from_excel_import_rssi" => "2",
                                            "status"                 => "3",
                                            "trx_pemeriksaan_id"     => $status_pemeriksaan_meninggal,
                                            "created_at"             => $tanggal_meninggal_val . " " . date("H:i:s"),
                                            "updated_at"             => $tanggal_meninggal_val . " " . date("H:i:s"),
                                            "id_user_created"        => "3",
                                        );
    
                                        $status_terkonfirmasi_meninggal = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi_meninggal);
    
                                        if ($status_rilis == "rilis") {
                                            $data_trx_terkonfirmasi_rilis_meninggal = array(
                                                "trx_terkonfirmasi_id"  => $status_terkonfirmasi_meninggal,
                                                "peserta_id"            => $id_peserta,
                                                "tanggal_terkonfirmasi" => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                "status"                => "3",
                                                "trx_pemeriksaan_id"    => $status_pemeriksaan_meninggal,
                                                "created_at"            => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                "updated_at"            => $tanggal_meninggal_val . " " . date("H:i:s"),
                                                "id_user_created"       => "3"
                                            );
    
                                            $status_terkonfirmasi_rilis_meninggal = $this->trx_terkonfirmasi_rilis_model->save($data_trx_terkonfirmasi_rilis_meninggal);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $this->session->set_flashdata('message', 'Import Data Excel Berhasil');
                }
            } else {
                $bool_upload_erro = true;
            }
            redirect("import_peserta_dinkes");
        }
    }
}
