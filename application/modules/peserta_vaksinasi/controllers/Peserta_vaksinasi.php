<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Peserta_vaksinasi extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model('vaksinasi/trx_vaksinasi_model', 'trx_vaksinasi_model');
        $this->load->model('peserta_vaksinasi_model');
        $this->load->model('master_kategori_bpjs_model');
        $this->load->model('master_merk_vaksin_model');
    }

    public function index()
    {
        $data['breadcrumb'] = [['link' => false, 'content' => 'Peserta Vaksinasi', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function tambah_peserta_vaksinasi()
    {
        if (empty($_POST)) {
            $data['master_bpjs'] = $this->master_kategori_bpjs_model->get();
            $data['master_merk_vaksin'] = $this->master_merk_vaksin_model->get();

            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'peserta_vaksinasi', 'content' => 'Peserta Vaksinasi', 'is_active' => false], ['link' => false, 'content' => 'Tambah Peserta Vaksinasi', 'is_active' => true]];

            $this->execute('form_peserta_vaksinasi', $data);
        } else {

            $data = array(
                "nama" => $this->ipost('nama'),
                "nik" => $this->ipost('nik'),
                "jenis_kelamin" => $this->ipost('jenis_kelamin'),
                "pekerjaan" => $this->ipost('pekerjaan'),
                "alamat" => $this->ipost('alamat'),
                "umur" => $this->ipost('umur'),
                "no_hp" => $this->ipost('no_hp'),
                "master_kategori_bpjs_id" => decrypt_data($this->ipost('kategori_bpjs_id')),
                "master_merk_vaksin_id" => decrypt_data($this->ipost('merk_vaksin_id')),
                'created_at' => $this->datetime(),
                'id_user_created' => $this->session->userdata("id_user")
            );

            $status = $this->peserta_vaksinasi_model->save($data);

            if ($status) {
                $this->session->set_flashdata('message', 'Data baru berhasil ditambahkan');
            } else {
                $this->session->set_flashdata('message', 'Data baru gagal ditambahkan');
            }

            redirect('peserta_vaksinasi');
        }
    }

    public function edit_peserta_vaksinasi($id_peserta_vaksinasi)
    {
        $data_master = $this->peserta_vaksinasi_model->get(
            array(
                "where" => array(
                    "id_peserta_vaksinasi" => decrypt_data($id_peserta_vaksinasi)
                )
            ),
            "row"
        );

        if (!$data_master) {
            $this->page_error();
        }


        if (empty($_POST)) {
            $data['content'] = $data_master;

            $data['master_bpjs'] = $this->master_kategori_bpjs_model->get();
            $data['master_merk_vaksin'] = $this->master_merk_vaksin_model->get();

            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'peserta_vaksinasi', 'content' => 'Peserta Vaksinasi', 'is_active' => false], ['link' => false, 'content' => 'Ubah Peserta Vaksinasi', 'is_active' => true]];
            $this->execute('form_peserta_vaksinasi', $data);
        } else {
            $data = array(
                "nama" => $this->ipost('nama'),
                "nik" => $this->ipost('nik'),
                "jenis_kelamin" => $this->ipost('jenis_kelamin'),
                "pekerjaan" => $this->ipost('pekerjaan'),
                "alamat" => $this->ipost('alamat'),
                "umur" => $this->ipost('umur'),
                "no_hp" => $this->ipost('no_hp'),
                "master_kategori_bpjs_id" => decrypt_data($this->ipost('kategori_bpjs_id')),
                "master_merk_vaksin_id" => decrypt_data($this->ipost('merk_vaksin_id')),
                'updated_at' => $this->datetime(),
                'id_user_updated' => $this->session->userdata("id_user")
            );

            $status = $this->peserta_vaksinasi_model->edit(decrypt_data($id_peserta_vaksinasi), $data);

            if ($status) {
                $this->session->set_flashdata('message', 'Data berhasil diubah');
            } else {
                $this->session->set_flashdata('message', 'Data gagal diubah');
            }

            redirect('peserta_vaksinasi');
        }
    }

    public function delete_peserta_vaksinasi()
    {
        $id_peserta_vaksinasi = $this->iget('id_peserta_vaksinasi');
        $data_master = $this->peserta_vaksinasi_model->get_by(decrypt_data($id_peserta_vaksinasi));

        if (!$data_master) {
            $this->page_error();
        }

        $data = array(
            "deleted_at" => $this->datetime(),
            "id_user_deleted" => $this->session->userdata("id_user")
        );

        $status = $this->peserta_vaksinasi_model->edit(decrypt_data($id_peserta_vaksinasi), $data);


        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function action_form_vaksinasi()
    {
        $id_trx_vaksinasi = decrypt_data($this->ipost("id_trx_vaksinasi"));
        $id_peserta_vaksinasi = decrypt_data($this->ipost("id_peserta_vaksinasi"));
        $no_register = $this->ipost("no_register");
        $tanggal_vaksinasi = $this->ipost("tanggal_vaksinasi");
        $no_batch = $this->ipost("no_batch");
        $tanggal_screening = $this->ipost("tanggal_screening");
        $dosis_tahap = $this->ipost("dosis_tahap");
        $keterangan = $this->ipost("keterangan");

        if ($id_trx_vaksinasi) {
            //update
            $data_trx_vaksinasi = array(
                "peserta_id_vaksinasi" => $id_peserta_vaksinasi,
                "no_register" => $no_register,
                "tanggal_vaksinasi" => $tanggal_vaksinasi,
                "no_batch" => $no_batch,
                "tanggal_screening" => $tanggal_screening,
                "dosis_tahap" => $dosis_tahap,
                "keterangan" => $keterangan,
                'updated_at' => $this->datetime(),
                'id_user_updated' => $this->session->userdata("id_user")
            );

            $status = $this->trx_vaksinasi_model->edit($id_trx_vaksinasi, $data_trx_vaksinasi);
        } else {
            //insert
            $data_trx_vaksinasi = array(
                "peserta_id_vaksinasi" => $id_peserta_vaksinasi,
                "no_register" => $no_register,
                "tanggal_vaksinasi" => $tanggal_vaksinasi,
                "no_batch" => $no_batch,
                "tanggal_screening" => $tanggal_screening,
                "dosis_tahap" => $dosis_tahap,
                "keterangan" => $keterangan,
                'updated_at' => $this->datetime(),
                'id_user_updated' => $this->session->userdata("id_user")
            );

            $status = $this->trx_vaksinasi_model->save($data_trx_vaksinasi);
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function delete_trx_vaksinasi()
    {
        $id_trx_vaksinasi = $this->iget('id_trx_vaksinasi');
        $data_master = $this->trx_vaksinasi_model->get_by(decrypt_data($id_trx_vaksinasi));

        if (!$data_master) {
            $this->page_error();
        }

        $data = array(
            "deleted_at" => $this->datetime(),
            "id_user_deleted" => $this->session->userdata("id_user")
        );

        $status = $this->trx_vaksinasi_model->edit(decrypt_data($id_trx_vaksinasi), $data);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }
}
