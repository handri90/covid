<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('vaksinasi/trx_vaksinasi_model', 'trx_vaksinasi_model');
        $this->load->model('peserta_vaksinasi_model');
    }

    public function get_peserta_vaksinasi()
    {
        $wh = array();
        if ($this->session->userdata("level_user_id") == "2") {
            $wh = array(
                "peserta_vaksinasi.id_user_created" => $this->session->userdata("id_user")
            );
        }

        $data_peserta = $this->peserta_vaksinasi_model->get(
            array(
                "fields" => "peserta_vaksinasi.*,nama_merk_vaksin",
                "join" => array(
                    "master_merk_vaksin" => "master_merk_vaksin_id=id_master_merk_vaksin AND master_merk_vaksin.deleted_at IS NULL"
                ),
                "where" => $wh
            )
        );

        $templist = array();
        foreach ($data_peserta as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta_vaksinasi);

            if ($row->pekerjaan == "1") {
                $templist[$key]['pekerjaan_parse'] = "Petugas Medis dan Non Medis di Fasilitas Pelayanan Kesehatan";
            } else if ($row->pekerjaan == "2") {
                $templist[$key]['pekerjaan_parse'] = "Petugas pelayanan publik yang berhadapan langsung dengan masyarakat";
            } else if ($row->pekerjaan == "3") {
                $templist[$key]['pekerjaan_parse'] = "Administrator Pemerintahan";
            } else if ($row->pekerjaan == "4") {
                $templist[$key]['pekerjaan_parse'] = "Lainnya";
            } else if ($row->pekerjaan == "5") {
                $templist[$key]['pekerjaan_parse'] = "Tidak Bekerja";
            }
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
