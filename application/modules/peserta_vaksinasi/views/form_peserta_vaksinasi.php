<style>
    .calendars {
        width: 350px;
    }

    .place_other_pendidikan,
    .place_other_pekerjaan {
        margin-top: -.5rem;
        margin-left: .5rem;
    }

    .ktp-image-custom {
        margin-bottom: 20px;
    }

    .kk-image-custom {
        margin-bottom: 20px;
    }
</style>
<div class="content">
    <!-- Form inputs -->
    <div class="card">
        <div class="card-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-validate-jquery')); ?>
            <fieldset class="mb-3">
                <legend class="text-uppercase font-size-sm font-weight-bold">Peserta Rapid</legend>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nama <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->nama : ""; ?>" name="nama" required placeholder="Nama" oninput="inpueUppercase(event)">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">NIK <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="number" class="form-control" value="<?php echo !empty($content) ? $content->nik : ""; ?>" name="nik" placeholder="NIK">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Umur <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="number" class="form-control" value="<?php echo !empty($content) ? $content->umur : ""; ?>" name="umur" placeholder="Umur">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Jenis Kelamin <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-input-styled" name="jenis_kelamin" value="L" <?php echo !empty($content) ? ($content->jenis_kelamin == "L" ? "checked" : "") : ""; ?> data-fouc>
                                Laki-Laki
                            </label>
                        </div>

                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-input-styled" name="jenis_kelamin" value="P" <?php echo !empty($content) ? ($content->jenis_kelamin == "P" ? "checked" : "") : ""; ?> data-fouc>
                                Perempuan
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Pekerjaan <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-input-styled" name="pekerjaan" value="1" <?php echo !empty($content) ? ($content->pekerjaan == "1" ? "checked" : "") : ""; ?> data-fouc>
                                Petugas Medis dan Non Medis di Fasilitas Pelayanan Kesehatan
                            </label>
                        </div>

                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-input-styled" name="pekerjaan" value="2" <?php echo !empty($content) ? ($content->pekerjaan == "2" ? "checked" : "") : ""; ?> data-fouc>
                                Petugas pelayanan publik yang berhadapan langsung dengan masyarakat
                            </label>
                        </div>

                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-input-styled" name="pekerjaan" value="3" <?php echo !empty($content) ? ($content->pekerjaan == "3" ? "checked" : "") : ""; ?> data-fouc>
                                Administrator Pemerintahan
                            </label>
                        </div>

                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-input-styled" name="pekerjaan" value="4" <?php echo !empty($content) ? ($content->pekerjaan == "4" ? "checked" : "") : ""; ?> data-fouc>
                                Lainnya
                            </label>
                        </div>

                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-input-styled" name="pekerjaan" value="5" <?php echo !empty($content) ? ($content->pekerjaan == "5" ? "checked" : "") : ""; ?> data-fouc>
                                Tidak Bekerja
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Alamat <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <textarea class="form-control" name="alamat" required placeholder="Alamat"><?php echo !empty($content) ? $content->alamat : ""; ?></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nomor HP <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="number" class="form-control" value="<?php echo !empty($content) ? $content->no_hp : ""; ?>" name="no_hp" required placeholder="Nomor HP">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Kategori BPJS <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <select class="form-control select-search" name="kategori_bpjs_id" required>
                            <option value="">-- Pilih Kategori BPJS --</option>
                            <?php
                            foreach ($master_bpjs as $key => $row) {
                                $selected = "";
                                if (!empty($content)) {
                                    if ($row->id_master_kategori_bpjs == $content->master_kategori_bpjs_id) {
                                        $selected = "selected";
                                    }
                                }
                            ?>
                                <option <?php echo $selected; ?> value="<?php echo encrypt_data($row->id_master_kategori_bpjs); ?>"><?php echo $row->nama_kategori_bpjs; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Merk Vaksin <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <select class="form-control select-search" name="merk_vaksin_id" required>
                            <option value="">-- Pilih Merk Vaksin --</option>
                            <?php
                            foreach ($master_merk_vaksin as $key => $row) {
                                $selected = "";
                                if (!empty($content)) {
                                    if ($row->id_master_merk_vaksin == $content->master_merk_vaksin_id) {
                                        $selected = "selected";
                                    }
                                }
                            ?>
                                <option <?php echo $selected; ?> value="<?php echo encrypt_data($row->id_master_merk_vaksin); ?>"><?php echo $row->nama_merk_vaksin; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <!-- /form inputs -->
</div>

<script>
    $('.form-input-styled').uniform({
        fileButtonClass: 'action btn bg-pink-400'
    });
</script>