<style>
    .info-column {
        margin: 4px 0;
    }
</style>

<div class="content">
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url() . 'peserta_vaksinasi/tambah_peserta_vaksinasi'; ?>" class="btn btn-info">Tambah Peserta Vaksinasi</a>
            </div>
        </div>
        <table id="datatablePesertaVaksinasi" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th rowspan="2">Nama</th>
                    <th rowspan="2">NIK</th>
                    <th rowspan="2">Jenis Kelamin</th>
                    <th rowspan="2">Pekerjaan</th>
                    <th rowspan="2">Umur</th>
                    <th colspan="3" style="text-align:center;">Kepesertaan BPJS</th>
                    <th rowspan="2">Merk Vaksin</th>
                    <th rowspan="2">Actions</th>
                </tr>
                <tr>
                    <th>BPJS PBI</th>
                    <th>BPJS Non PBI</th>
                    <th>Non Anggota</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    let datatablePesertaVaksinasi = $("#datatablePesertaVaksinasi").DataTable({
        "deferRender": true,
        "columns": [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            {
                "width": "10%"
            }
        ]
    });

    get_peserta_vaksinasi();

    function get_peserta_vaksinasi() {

        datatablePesertaVaksinasi.clear().draw();
        $.ajax({
            url: base_url + 'peserta_vaksinasi/request/get_peserta_vaksinasi',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    datatablePesertaVaksinasi.row.add([
                        value.nama,
                        value.nik,
                        value.jenis_kelamin,
                        value.pekerjaan_parse,
                        value.umur,
                        (value.master_kategori_bpjs_id == "1" ? "Ya" : ""),
                        (value.master_kategori_bpjs_id == "2" ? "Ya" : ""),
                        (value.master_kategori_bpjs_id == "3" ? "Ya" : ""),
                        value.nama_merk_vaksin,
                        "<a href='" + base_url + "peserta_vaksinasi/edit_peserta_vaksinasi/" + value.id_encrypt + "' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>"
                    ]).draw(false);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function confirm_delete(id_peserta_vaksinasi) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta_vaksinasi/delete_peserta_vaksinasi',
                    data: {
                        id_peserta_vaksinasi: id_peserta_vaksinasi
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_peserta_vaksinasi();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_peserta_vaksinasi();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta_vaksinasi();
                    }
                });
            }
        });
    }
</script>