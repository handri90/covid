<?php

class Master_kategori_bpjs_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "master_kategori_bpjs";
        $this->primary_id = "id_master_kategori_bpjs";
    }
}
