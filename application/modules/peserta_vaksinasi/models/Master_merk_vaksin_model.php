<?php

class Master_merk_vaksin_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "master_merk_vaksin";
        $this->primary_id = "id_master_merk_vaksin";
    }
}
