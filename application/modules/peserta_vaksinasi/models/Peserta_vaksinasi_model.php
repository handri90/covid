<?php

class Peserta_vaksinasi_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "peserta_vaksinasi";
        $this->primary_id = "id_peserta_vaksinasi";
    }
}
