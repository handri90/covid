<?php

class Master_faskes_vaksin_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "master_faskes_vaksin";
        $this->primary_id = "id_master_faskes_vaksin";
    }
}
