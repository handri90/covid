<?php

class Master_peserta_registrasi_vaksin_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "master_peserta_registrasi_vaksin";
        $this->primary_id = "id_master_peserta_registrasi_vaksin";
    }
}
