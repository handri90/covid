<?php

class Master_kategori_peserta_vaksin_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "master_kategori_peserta_vaksin";
        $this->primary_id = "id_master_kategori_peserta_vaksin";
    }
}
