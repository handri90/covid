<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Import_vaksin extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model("master_kategori_peserta_vaksin_model");
        $this->load->model("master_sub_kategori_peserta_vaksin_model");
        $this->load->model("master_peserta_registrasi_vaksin_model");
        $this->load->model("master_faskes_vaksin_model");
    }

    public function index()
    {
        $data['breadcrumb'] = [['link' => false, 'content' => 'Import Vaksin', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function import_peserta_vaksin()
    {
        if (empty($_FILES)) {
            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'import_vaksin', 'content' => 'Import Vaksin', 'is_active' => false], ['link' => false, 'content' => 'Import Peserta Vaksin', 'is_active' => true]];
            $this->execute('form_import_peserta_vaksin', $data);
        } else {
            $input_name  = 'file_csv';
            $upload_file = $this->upload_file($input_name, $this->config->item('path_csv'), "", "csv");

            if (!isset($upload_file['error'])) {
                $file                   = fopen("./" . $this->config->item('path_csv') . "/" . $upload_file['data']['file_name'], "r");
                $id_kategori_vaksin     = 0;
                $id_sub_kategori_vaksin = 0;
                $id_master_faskes_vaksin = 0;
                $status_master_faskes = true;
                $row = 1;
                $row_shame = array();
                while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
                    if ($row != 1) {
                        //get master faskes
                        if ($column[2] != 'SELURUH FASKES VAKSINASI COVID-19') {
                            $data_master_faskes = $this->master_faskes_vaksin_model->get(
                                array(
                                    "where" => array(
                                        "faskes" => $column[2]
                                    )
                                ),
                                "row"
                            );
                            if (!$data_master_faskes) {
                                $status_master_faskes = false;
                                break;
                            }

                            $id_master_faskes_vaksin = $data_master_faskes->id_master_faskes_vaksin;
                        } else {
                            $id_master_faskes_vaksin = 99999;
                        }


                        //cek kategori
                        $cek_kategori = $this->master_kategori_peserta_vaksin_model->get(
                            array(
                                "where" => array(
                                    "nama_kategori_vaksin" => $column[6]
                                )
                            ),
                            "row"
                        );

                        if (!$cek_kategori) {
                            $id_kategori_vaksin = $this->master_kategori_peserta_vaksin_model->save(array("nama_kategori_vaksin" => $column[6]));
                        } else {
                            $id_kategori_vaksin = $cek_kategori->id_master_kategori_peserta_vaksin;
                        }

                        //cek sub kategori
                        $cek_sub_kategori = $this->master_sub_kategori_peserta_vaksin_model->get(
                            array(
                                "join" => array(
                                    "master_kategori_peserta_vaksin" => "id_master_kategori_peserta_vaksin=master_kategori_peserta_vaksin_id"
                                ),
                                "where" => array(
                                    "nama_kategori_vaksin"     => $column[6],
                                    "nama_sub_kategori_vaksin" => $column[7],
                                )
                            ),
                            "row"
                        );

                        if (!$cek_sub_kategori) {
                            $id_sub_kategori_vaksin = $this->master_sub_kategori_peserta_vaksin_model->save(array("master_kategori_peserta_vaksin_id" => $id_kategori_vaksin, "nama_sub_kategori_vaksin" => $column[7]));
                        } else {
                            $id_sub_kategori_vaksin = $cek_sub_kategori->id_master_sub_kategori_peserta_vaksin;
                        }

                        //save peserta
                        $data_peserta = array(
                            "provinsi"                => $column[0],
                            "kabupaten"               => $column[1],
                            "master_faskes_vaksin_id" => $id_master_faskes_vaksin,
                            "nama"                    => $column[3],
                            "jenis_kelamin"           => $column[4],
                            "kelompok_usia"           => $column[5],
                            "sub_kategori_id"         => $id_sub_kategori_vaksin,
                            "kanal"                   => $column[8],
                            "status"                  => $column[9],
                        );

                        $cek_peserta_vaksin = $this->master_peserta_registrasi_vaksin_model->get(
                            array(
                                "where" => array(
                                    "provinsi"                => $column[0],
                                    "kabupaten"               => $column[1],
                                    "master_faskes_vaksin_id" => $id_master_faskes_vaksin,
                                    "nama"                    => $column[3],
                                    "jenis_kelamin"           => $column[4],
                                    "kelompok_usia"           => $column[5],
                                    "sub_kategori_id"         => $id_sub_kategori_vaksin,
                                    "kanal"                   => $column[8],
                                    "status"                  => $column[9],
                                )
                            ),
                            "row"
                        );

                        if ($cek_peserta_vaksin) {
                            array_push($row_shame, $row);
                            $status = $this->master_peserta_registrasi_vaksin_model->edit($cek_peserta_vaksin->id_master_peserta_registrasi_vaksin, $data_peserta);
                        } else {
                            $status = $this->master_peserta_registrasi_vaksin_model->save($data_peserta);
                        }
                    }

                    $row++;
                }

                echo count($row_shame) . "<br>";
                print_r($row_shame);
                die;
                if (!$status_master_faskes) {
                    $this->session->set_flashdata('message', 'Gagal upload file : Cek Acuan Faskes');
                    redirect('import_vaksin/import_peserta_vaksin');
                } else {
                    $this->session->set_flashdata('message', 'Data Berhasil ditambahkan');
                    redirect('import_vaksin');
                }
            } else {
            }
        }
    }
}
