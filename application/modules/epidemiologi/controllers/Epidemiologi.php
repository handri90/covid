<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Epidemiologi extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model("peserta/master_wilayah_model", "master_wilayah_model");
        $this->load->model("konstanta_epidemiologi_model");
    }

    public function index()
    {
        $data['list_kecamatan'] = $this->master_wilayah_model->get(
            array(
                "where" => array(
                    "klasifikasi" => "KEC"
                ),
                "order_by" => array(
                    "nama_wilayah" => "ASC"
                )
            )
        );

        $data['breadcrumb'] = [['link' => false, 'content' => 'Epidemiologi', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function save_jumlah_penduduk()
    {
        $id_master_wilayah = $this->ipost("id_master_wilayah");
        $val_jumlah_penduduk = replace_dot($this->ipost("val_jumlah_penduduk"));

        $data = array(
            "jumlah_penduduk" => $val_jumlah_penduduk
        );

        $status = $this->master_wilayah_model->edit($id_master_wilayah, $data);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function save_konstanta()
    {
        $val_konstanta = replace_dot($this->ipost("val_konstanta"));

        $data = array(
            "const_attack_rate" => $val_konstanta
        );

        $status = $this->konstanta_epidemiologi_model->edit("1", $data);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }
}
