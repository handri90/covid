<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("peserta/master_wilayah_model", "master_wilayah_model");
        $this->load->model("konstanta_epidemiologi_model");
        $this->load->model("positivity_rate/positivity_rate_model","positivity_rate_model");
        $this->load->model("grafik_vaksin/grafik_vaksin_model","grafik_vaksin_model");
    }

    public function get_data_attack_rate()
    {
        $kode_wilayah = decrypt_data($this->iget("kode_wilayah"));
        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");

        $data_attack_rate = $this->master_wilayah_model->query("
        SELECT 
        CONCAT(klasifikasi,' - ',nama_wilayah) AS nama_wilayah,
        IFNULL(jumlah_penduduk,0) AS jumlah_penduduk,
        IFNULL(a.jumlah_kasus_positif,0) AS jumlah_kasus_positif,
        IFNULL(((IFNULL(a.jumlah_kasus_positif,0)/IFNULL(jumlah_penduduk,0))*(SELECT const_attack_rate FROM konstanta_epidemiologi)),0) AS attack_rate,
        id_master_wilayah
        FROM master_wilayah
        LEFT JOIN (
            SELECT COUNT(id_peserta) AS jumlah_kasus_positif,kode_wilayah
            FROM (
                SELECT peserta.*,kode_wilayah,kode_induk,nama_wilayah,klasifikasi,jumlah_penduduk,id_master_wilayah
                FROM master_wilayah
                INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
                INNER JOIN trx_terkonfirmasi_rilis ON id_peserta=peserta_id
                WHERE status = '1' AND trx_terkonfirmasi_rilis.deleted_at IS NULL AND peserta.deleted_at IS NULL AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."'
                GROUP BY peserta_id
            ) AS tbl
            GROUP BY kode_wilayah
        ) AS a ON a.kode_wilayah=master_wilayah.kode_wilayah
        WHERE kode_induk = '" . $kode_wilayah . "'
        ORDER BY nama_wilayah
        ")->result();
        
        $data_attack_rate_kecamatan = $this->master_wilayah_model->query("
        SELECT
            IFNULL(SUM(a.jumlah_kasus_positif),0) AS jml_kasus_positif,
            IFNULL(SUM(jumlah_penduduk),0) AS jumlah_penduduk,
            IFNULL((IFNULL(SUM(a.jumlah_kasus_positif),0) / IFNULL(SUM(jumlah_penduduk),0)) * (SELECT 
                    const_attack_rate
                FROM
                    konstanta_epidemiologi),0) AS attack_rate
        FROM
            master_wilayah
                LEFT JOIN
            (SELECT 
                COUNT(id_peserta) AS jumlah_kasus_positif, kode_wilayah
            FROM
                (SELECT 
                peserta.*,
                    kode_wilayah,
                    kode_induk,
                    nama_wilayah,
                    klasifikasi,
                    jumlah_penduduk,
                    id_master_wilayah
            FROM
                master_wilayah
            INNER JOIN peserta ON kelurahan_master_wilayah_id = id_master_wilayah
                AND peserta.`deleted_at` IS NULL
            INNER JOIN trx_terkonfirmasi_rilis ON id_peserta = peserta_id
            WHERE
                status = '1'
                    AND trx_terkonfirmasi_rilis.deleted_at IS NULL
                    AND peserta.deleted_at IS NULL
                    AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."'
            GROUP BY peserta_id) AS tbl
            GROUP BY kode_wilayah) AS a ON a.kode_wilayah = master_wilayah.kode_wilayah
        WHERE
            kode_induk = '" . $kode_wilayah . "'
        ORDER BY nama_wilayah;
        ")->row();

        $templist = array();
        foreach ($data_attack_rate as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data = array("per_wilayah"=>$templist,"kecamatan"=>$data_attack_rate_kecamatan->attack_rate,"jumlah_kasus_kecamatan"=>$data_attack_rate_kecamatan->jml_kasus_positif);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_data_case_fatality_rate()
    {
        $data_kasus_kematian = $this->master_wilayah_model->query("
        SELECT IFNULL(COUNT(id_peserta),0) AS jumlah_kasus_kematian
        FROM (
            SELECT peserta.*,kode_wilayah,kode_induk,nama_wilayah,klasifikasi,jumlah_penduduk,id_master_wilayah
            FROM master_wilayah
            INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
            INNER JOIN trx_terkonfirmasi_rilis ON id_peserta=peserta_id
            WHERE status = '3' AND trx_terkonfirmasi_rilis.deleted_at IS NULL AND peserta.deleted_at IS NULL
            GROUP BY peserta_id
        ) AS tbl
        ")->row();

        $data_kasus_konfirmasi = $this->master_wilayah_model->query("
        SELECT IFNULL(COUNT(id_peserta),0) AS jumlah_kasus_konfirmasi
        FROM (
            SELECT peserta.*,kode_wilayah,kode_induk,nama_wilayah,klasifikasi,jumlah_penduduk,id_master_wilayah
            FROM master_wilayah
            INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
            INNER JOIN trx_terkonfirmasi_rilis ON id_peserta=peserta_id
            WHERE status = '1' AND trx_terkonfirmasi_rilis.deleted_at IS NULL AND peserta.deleted_at IS NULL
            GROUP BY peserta_id
        ) AS tbl
        ")->row();

        $data['jumlah_kasus_kematian'] = $data_kasus_kematian->jumlah_kasus_kematian;
        $data['jumlah_kasus_konfirmasi'] = $data_kasus_konfirmasi->jumlah_kasus_konfirmasi;
        $data['case_fatality_rate'] = number_format(((int) $data_kasus_kematian->jumlah_kasus_kematian / (int) $data_kasus_konfirmasi->jumlah_kasus_konfirmasi) * 100, 2, '.', '');

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_data_case_positive_rate()
    {
        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");

        $data_kasus_konfirmasi = $this->master_wilayah_model->query("
        SELECT 
            COUNT(peserta_id) AS jumlah_kasus_konfirmasi
        FROM
            trx_terkonfirmasi
        WHERE
            trx_pemeriksaan_id IN (SELECT 
                    MAX(trx_pemeriksaan.id_trx_pemeriksaan)
                FROM
                    peserta
                        INNER JOIN
                    trx_pemeriksaan ON id_peserta = peserta_id
                        INNER JOIN
                    master_wilayah ON id_master_wilayah = kelurahan_master_wilayah_id
                WHERE
                    jenis_pemeriksaan_id = '1'
                        AND trx_pemeriksaan.deleted_at IS NULL
                        AND peserta.deleted_at IS NULL
                        AND DATE_FORMAT(trx_pemeriksaan.created_at, '%Y-%m-%d') BETWEEN '${start_date}' AND '${end_date}' GROUP BY peserta_id)
                AND status = '1'
        ")->row();

        $data_peserta_tes = $this->master_wilayah_model->query("
        SELECT COUNT(id_peserta) AS jumlah_peserta_tes
        FROM (
            SELECT peserta.*,kode_wilayah,kode_induk,nama_wilayah,klasifikasi,jumlah_penduduk,id_master_wilayah
            FROM master_wilayah
            INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
            INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
            WHERE jenis_pemeriksaan_id = '1' AND trx_pemeriksaan.deleted_at IS NULL AND peserta.deleted_at IS NULL AND DATE_FORMAT(trx_pemeriksaan.created_at, '%Y-%m-%d') BETWEEN '${start_date}' AND '${end_date}'
            GROUP BY peserta_id
        ) AS tbl
        ")->row();

        $data['jumlah_kasus_konfirmasi'] = $data_kasus_konfirmasi->jumlah_kasus_konfirmasi;
        $data['jumlah_peserta_tes'] = $data_peserta_tes->jumlah_peserta_tes;
        
        $data['case_positive_rate'] = $data_kasus_konfirmasi->jumlah_kasus_konfirmasi != 0 ? number_format(((int) $data_kasus_konfirmasi->jumlah_kasus_konfirmasi / (int) $data_peserta_tes->jumlah_peserta_tes) * 100, 2, '.', ''):0;

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_data_const_attack_rate()
    {
        $data = $this->konstanta_epidemiologi_model->get_by("1");
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_trend_kasus_covid(){
        $start_date = new DateTime($this->iget("start_date"));
        $end_date = new DateTime($this->iget("end_date"));

        $arr_tanggal =[];
        $arr_data =[];
        $arr_data_fatality =[];
        for($i = $start_date;$i <= $end_date;$i->modify("+1 day")){
            array_push($arr_tanggal,$i->format("d M"));

            $data_attack_rate = $this->master_wilayah_model->query("SELECT
                IFNULL(SUM(a.jumlah_kasus_positif),0) AS jml_kasus_positif,
                IFNULL(SUM(jumlah_penduduk),0) AS jumlah_penduduk,
                IFNULL((IFNULL(SUM(a.jumlah_kasus_positif),0) / IFNULL(SUM(jumlah_penduduk),0)) * (SELECT 
                        const_attack_rate
                    FROM
                        konstanta_epidemiologi),0) AS attack_rate
            FROM
                master_wilayah
                    LEFT JOIN
                (SELECT 
                    COUNT(id_peserta) AS jumlah_kasus_positif, kode_wilayah
                FROM
                    (SELECT 
                    peserta.*,
                        kode_wilayah,
                        kode_induk,
                        nama_wilayah,
                        klasifikasi,
                        jumlah_penduduk,
                        id_master_wilayah
                FROM
                    master_wilayah
                INNER JOIN peserta ON kelurahan_master_wilayah_id = id_master_wilayah
                    AND peserta.`deleted_at` IS NULL
                INNER JOIN trx_terkonfirmasi_rilis ON id_peserta = peserta_id
                WHERE
                    status = '1'
                        AND trx_terkonfirmasi_rilis.deleted_at IS NULL
                        AND peserta.deleted_at IS NULL
                        AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d') = '".$i->format("Y-m-d")."'
                GROUP BY peserta_id) AS tbl
                GROUP BY kode_wilayah) AS a ON a.kode_wilayah = master_wilayah.kode_wilayah
            ORDER BY nama_wilayah
            ")->row();

            $data_kasus_kematian = $this->master_wilayah_model->query("
            SELECT IFNULL(COUNT(id_peserta),0) AS jumlah_kasus_kematian
            FROM (
                SELECT peserta.*,kode_wilayah,kode_induk,nama_wilayah,klasifikasi,jumlah_penduduk,id_master_wilayah
                FROM master_wilayah
                INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
                INNER JOIN trx_terkonfirmasi_rilis ON id_peserta=peserta_id
                WHERE status = '3' AND trx_terkonfirmasi_rilis.deleted_at IS NULL AND peserta.deleted_at IS NULL
                AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d') = '".$i->format("Y-m-d")."'
                GROUP BY peserta_id
            ) AS tbl
            ")->row();

            $data_kasus_konfirmasi = $this->master_wilayah_model->query("
            SELECT IFNULL(COUNT(id_peserta),0) AS jumlah_kasus_konfirmasi
            FROM (
                SELECT peserta.*,kode_wilayah,kode_induk,nama_wilayah,klasifikasi,jumlah_penduduk,id_master_wilayah
                FROM master_wilayah
                INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
                INNER JOIN trx_terkonfirmasi_rilis ON id_peserta=peserta_id
                WHERE status = '1' AND trx_terkonfirmasi_rilis.deleted_at IS NULL AND peserta.deleted_at IS NULL
                AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d') = '".$i->format("Y-m-d")."'
                GROUP BY peserta_id
            ) AS tbl
            ")->row();

            $case_fatality_rate = $data_kasus_kematian->jumlah_kasus_kematian != 0 && $data_kasus_konfirmasi->jumlah_kasus_konfirmasi != 0 ?number_format(((double) $data_kasus_kematian->jumlah_kasus_kematian / (double) $data_kasus_konfirmasi->jumlah_kasus_konfirmasi) * 100, 2, '.', ''):0;

            array_push($arr_data,(double) $data_attack_rate->attack_rate);
            array_push($arr_data_fatality,(double) $case_fatality_rate);
        }

        $data = [
            "tanggal"=>$arr_tanggal,
            "data"=>$arr_data,
            "data_case_fatality"=>$arr_data_fatality,
        ];

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_grafik_positive_rate()
    {
        $start_date = new DateTime($this->iget("start_date"));
        $end_date = new DateTime($this->iget("end_date"));

        $arr_tanggal =[];
        $arr_data =[];
        for($i = $start_date;$i <= $end_date;$i->modify("+1 day")){
            array_push($arr_tanggal,$i->format("d M"));

            // $data_kasus_konfirmasi = $this->master_wilayah_model->query("
            // SELECT 
            //     COUNT(peserta_id) AS jumlah_kasus_konfirmasi
            // FROM
            //     trx_terkonfirmasi
            // WHERE
            //     trx_pemeriksaan_id IN (SELECT 
            //             MAX(trx_pemeriksaan.id_trx_pemeriksaan)
            //         FROM
            //             peserta
            //                 INNER JOIN
            //             trx_pemeriksaan ON id_peserta = peserta_id
            //         WHERE
            //             jenis_pemeriksaan_id = '1'
            //                 AND trx_pemeriksaan.deleted_at IS NULL
            //                 AND peserta.deleted_at IS NULL
            //                 AND DATE_FORMAT(trx_pemeriksaan.created_at, '%Y-%m-%d') = '".$i->format("Y-m-d")."' GROUP BY peserta_id)
            //         AND status = '1'
            // ")->row();

            // $data_peserta_tes = $this->master_wilayah_model->query("
            // SELECT COUNT(id_peserta) AS jumlah_peserta_tes
            // FROM (
            //     SELECT peserta.*,kode_wilayah,kode_induk,nama_wilayah,klasifikasi,jumlah_penduduk,id_master_wilayah
            //     FROM master_wilayah
            //     INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
            //     INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
            //     WHERE jenis_pemeriksaan_id = '1' AND trx_pemeriksaan.deleted_at IS NULL AND peserta.deleted_at IS NULL AND DATE_FORMAT(trx_pemeriksaan.created_at, '%Y-%m-%d') = '".$i->format("Y-m-d")."'
            //     GROUP BY peserta_id
            // ) AS tbl
            // ")->row();
            
            // $case_positive_rate = $data_kasus_konfirmasi->jumlah_kasus_konfirmasi != 0 ? number_format(((int) $data_kasus_konfirmasi->jumlah_kasus_konfirmasi / (int) $data_peserta_tes->jumlah_peserta_tes) * 100, 2, '.', ''):0;

            $case_positive_rate = $this->positivity_rate_model->get(
                array(
                    "where"=>array(
                        "tanggal"=>$i->format("Y-m-d")
                    )
                ),"row"
            );
            array_push($arr_data,$case_positive_rate?(double) $case_positive_rate->nilai:0);
        }

        $data = ["tanggal"=>$arr_tanggal,"data"=>$arr_data];

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_pie_ar_berdasarkan_umur(){
        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");

        $data_attack_rate_0_5 = $this->master_wilayah_model->query("
        SELECT 
            IFNULL(((IFNULL(COUNT(a.id_peserta), 0) / (SELECT 
                            SUM(jumlah_penduduk)
                        FROM
                            master_wilayah)) * (SELECT 
                            const_attack_rate
                        FROM
                            konstanta_epidemiologi)),
                    0) AS attack_rate
        FROM
            (SELECT 
                id_peserta
            FROM
                peserta
            INNER JOIN trx_terkonfirmasi_rilis ON id_peserta = peserta_id
            WHERE
                status = '1'
                    AND trx_terkonfirmasi_rilis.deleted_at IS NULL
                    AND peserta.deleted_at IS NULL
                    AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."'
                    AND TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d')) BETWEEN '0' AND '5'
            GROUP BY peserta_id) AS a
        ")->row();

        $data_attack_rate_6_12 = $this->master_wilayah_model->query("
        SELECT 
            IFNULL(((IFNULL(COUNT(a.id_peserta), 0) / (SELECT 
                            SUM(jumlah_penduduk)
                        FROM
                            master_wilayah)) * (SELECT 
                            const_attack_rate
                        FROM
                            konstanta_epidemiologi)),
                    0) AS attack_rate
        FROM
            (SELECT 
                id_peserta
            FROM
                peserta
            INNER JOIN trx_terkonfirmasi_rilis ON id_peserta = peserta_id
            WHERE
                status = '1'
                    AND trx_terkonfirmasi_rilis.deleted_at IS NULL
                    AND peserta.deleted_at IS NULL
                    AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."'
                    AND TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d')) BETWEEN '6' AND '12'
            GROUP BY peserta_id) AS a
        ")->row();

        $data_attack_rate_13_18 = $this->master_wilayah_model->query("
        SELECT 
            IFNULL(((IFNULL(COUNT(a.id_peserta), 0) / (SELECT 
                            SUM(jumlah_penduduk)
                        FROM
                            master_wilayah)) * (SELECT 
                            const_attack_rate
                        FROM
                            konstanta_epidemiologi)),
                    0) AS attack_rate
        FROM
            (SELECT 
                id_peserta
            FROM
                peserta
            INNER JOIN trx_terkonfirmasi_rilis ON id_peserta = peserta_id
            WHERE
                status = '1'
                    AND trx_terkonfirmasi_rilis.deleted_at IS NULL
                    AND peserta.deleted_at IS NULL
                    AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."'
                    AND TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d')) BETWEEN '13' AND '18'
            GROUP BY peserta_id) AS a
        ")->row();
        
        $data_attack_rate_19_60 = $this->master_wilayah_model->query("
        SELECT 
            IFNULL(((IFNULL(COUNT(a.id_peserta), 0) / (SELECT 
                            SUM(jumlah_penduduk)
                        FROM
                            master_wilayah)) * (SELECT 
                            const_attack_rate
                        FROM
                            konstanta_epidemiologi)),
                    0) AS attack_rate
        FROM
            (SELECT 
                id_peserta
            FROM
                peserta
            INNER JOIN trx_terkonfirmasi_rilis ON id_peserta = peserta_id
            WHERE
                status = '1'
                    AND trx_terkonfirmasi_rilis.deleted_at IS NULL
                    AND peserta.deleted_at IS NULL
                    AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."'
                    AND TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d')) BETWEEN '19' AND '60'
            GROUP BY peserta_id) AS a
        ")->row();
        
        $data_attack_more_than_60 = $this->master_wilayah_model->query("
        SELECT 
            IFNULL(((IFNULL(COUNT(a.id_peserta), 0) / (SELECT 
                            SUM(jumlah_penduduk)
                        FROM
                            master_wilayah)) * (SELECT 
                            const_attack_rate
                        FROM
                            konstanta_epidemiologi)),
                    0) AS attack_rate
        FROM
            (SELECT 
                id_peserta
            FROM
                peserta
            INNER JOIN trx_terkonfirmasi_rilis ON id_peserta = peserta_id
            WHERE
                status = '1'
                    AND trx_terkonfirmasi_rilis.deleted_at IS NULL
                    AND peserta.deleted_at IS NULL
                    AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."'
                    AND TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d')) > '60'
            GROUP BY peserta_id) AS a
        ")->row();

        $data = [
            "rentang_umur"=>["0 - 5 Tahun","6 - 12 Tahun","13 - 18 Tahun","19 - 60 Tahun","> 60 Tahun"],
            "data"=>[
                ["value"=>(double) $data_attack_rate_0_5->attack_rate,"name"=>"0 - 5 Tahun"],
                ["value"=>(double) $data_attack_rate_6_12->attack_rate,"name"=>"6 - 12 Tahun"],
                ["value"=>(double) $data_attack_rate_13_18->attack_rate,"name"=>"13 - 18 Tahun"],
                ["value"=>(double) $data_attack_rate_19_60->attack_rate,"name"=>"19 - 60 Tahun"],
                ["value"=>(double) $data_attack_more_than_60->attack_rate,"name"=>"> 60 Tahun"],
                ]
        ];
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
    
    public function data_kasus_covid_saat_ini(){
        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");

        $kasus_terkonfirmasi_pcr = $this->master_wilayah_model->query("
        SELECT 
            COUNT(id_peserta) AS jumlah
        FROM
            (SELECT 
                id_peserta
            FROM
                peserta
            INNER JOIN trx_terkonfirmasi_rilis ON id_peserta = peserta_id
            INNER JOIN master_wilayah ON id_master_wilayah = kelurahan_master_wilayah_id
            WHERE
                status = '1'
                    AND trx_terkonfirmasi_rilis.deleted_at IS NULL
                    AND peserta.deleted_at IS NULL
                    AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d') between '".$start_date."' and '".$end_date."'
            GROUP BY id_peserta) AS A;
        ")->row();
        
        $kasus_terkonfirmasi_antigen_nar = $this->master_wilayah_model->query("
        SELECT 
            COUNT(id_peserta) AS jumlah
        FROM
            (SELECT 
                id_peserta
            FROM
                master_wilayah
            INNER JOIN peserta ON kelurahan_master_wilayah_id = id_master_wilayah
                AND peserta.`deleted_at` IS NULL
            INNER JOIN trx_pemeriksaan ON id_peserta = peserta_id
            WHERE
                trx_pemeriksaan.deleted_at IS NULL
                    AND jenis_pemeriksaan_id = '2'
                    AND hasil_pemeriksaan_id = '3'
                    AND trx_pemeriksaan.from_excel_import_faskes = '2'
                    AND DATE_FORMAT(trx_pemeriksaan.created_at, '%Y-%m-%d') >= '2021-08-11'
                    AND DATE_FORMAT(trx_pemeriksaan.created_at, '%Y-%m-%d') between '".$start_date."' and '".$end_date."'
            GROUP BY peserta_id) AS A
        ")->row();
        
        $kasus_terkonfirmasi_luar_wilayah = $this->master_wilayah_model->query("
        SELECT 
            COUNT(id_peserta) AS jumlah
        FROM
            (SELECT 
                id_peserta
            FROM
                peserta
            INNER JOIN trx_terkonfirmasi_rilis ON id_peserta = peserta_id
            INNER JOIN master_wilayah ON id_master_wilayah = kelurahan_master_wilayah_id
            WHERE
                status = '1'
                    AND trx_terkonfirmasi_rilis.deleted_at IS NULL
                    AND peserta.deleted_at IS NULL
                    AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."'
                    AND kelurahan_master_wilayah_id = '109'
            GROUP BY peserta_id) AS A
        ")->row();
        
        $kasus_terkonfirmasi_antigen_luar_wilayah = $this->master_wilayah_model->query("
        SELECT 
            COUNT(id_peserta) AS jumlah
        FROM
            (SELECT 
                id_peserta
            FROM
                master_wilayah
            INNER JOIN peserta ON kelurahan_master_wilayah_id = id_master_wilayah
                AND peserta.`deleted_at` IS NULL
            INNER JOIN trx_pemeriksaan ON id_peserta = peserta_id
            WHERE
                trx_pemeriksaan.deleted_at IS NULL
                    AND kelurahan_master_wilayah_id = '109'
                    AND jenis_pemeriksaan_id = '2'
                    AND hasil_pemeriksaan_id = '3'
                    AND trx_pemeriksaan.from_excel_import_faskes = '2'
                    AND DATE_FORMAT(trx_pemeriksaan.created_at, '%Y-%m-%d') >= '2021-08-11'
                    AND DATE_FORMAT(trx_pemeriksaan.created_at, '%Y-%m-%d') between '".$start_date."' and '".$end_date."'
            GROUP BY peserta_id) AS A
        ")->row();
        
        $kasus_isolasi_mandiri = $this->master_wilayah_model->query("
        SELECT 
            COUNT(id_peserta) AS jumlah
        FROM
            (SELECT 
                id_peserta
            FROM
                peserta
            INNER JOIN trx_terkonfirmasi_rilis ON id_peserta = trx_terkonfirmasi_rilis.peserta_id
            INNER JOIN trx_pemeriksaan ON id_peserta = trx_pemeriksaan.peserta_id
            INNER JOIN master_wilayah ON id_master_wilayah = kelurahan_master_wilayah_id
            WHERE
                status = '1'
                    AND trx_pemeriksaan.status_rawat = '1'
                    AND trx_terkonfirmasi_rilis.deleted_at IS NULL
                    AND peserta.deleted_at IS NULL
                    AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."'
            GROUP BY id_peserta) AS A
        ")->row();
        
        $kasus_isolasi_rumah_sakit = $this->master_wilayah_model->query("
        SELECT 
            COUNT(id_peserta) AS jumlah
        FROM
            (SELECT 
                id_peserta
            FROM
                peserta
            INNER JOIN trx_terkonfirmasi_rilis ON id_peserta = trx_terkonfirmasi_rilis.peserta_id
            JOIN status_rawat ON id_peserta = status_rawat.peserta_id
            INNER JOIN master_wilayah ON id_master_wilayah = kelurahan_master_wilayah_id
            WHERE
                status = '1'
                    AND status_rawat.is_status = '1'
                    AND trx_terkonfirmasi_rilis.deleted_at IS NULL
                    AND peserta.deleted_at IS NULL
                    AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."'
            GROUP BY id_peserta) AS A
        ")->row();

        $data = ["terkonfirmasi"=>(int) $kasus_terkonfirmasi_pcr->jumlah + (int) $kasus_terkonfirmasi_antigen_nar->jumlah,"luar_wilayah"=>(int) $kasus_terkonfirmasi_luar_wilayah->jumlah + (int) $kasus_terkonfirmasi_antigen_luar_wilayah->jumlah,"isolasi_mandiri"=>(int) $kasus_isolasi_mandiri->jumlah,"isolasi_rsud"=>(int) $kasus_isolasi_rumah_sakit->jumlah];
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
    
    public function data_kumulatif_covid_saat_ini(){
        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");

        $kasus_terkonfirmasi_pcr = $this->master_wilayah_model->query("
        SELECT 
            COUNT(id_peserta) AS jumlah
        FROM
            (SELECT 
                id_peserta
            FROM
                peserta
            INNER JOIN trx_terkonfirmasi_rilis ON id_peserta = peserta_id
            INNER JOIN master_wilayah ON id_master_wilayah = kelurahan_master_wilayah_id
            WHERE
                status = '1'
                    AND trx_terkonfirmasi_rilis.deleted_at IS NULL
                    AND peserta.deleted_at IS NULL
                    AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d') between '".$start_date."' and '".$end_date."'
            GROUP BY id_peserta) AS A;
        ")->row();
        
        $kasus_terkonfirmasi_antigen_nar = $this->master_wilayah_model->query("
        SELECT 
            COUNT(id_peserta) AS jumlah
        FROM
            (SELECT 
                id_peserta
            FROM
                master_wilayah
            INNER JOIN peserta ON kelurahan_master_wilayah_id = id_master_wilayah
                AND peserta.`deleted_at` IS NULL
            INNER JOIN trx_pemeriksaan ON id_peserta = peserta_id
            WHERE
                trx_pemeriksaan.deleted_at IS NULL
                    AND jenis_pemeriksaan_id = '2'
                    AND hasil_pemeriksaan_id = '3'
                    AND trx_pemeriksaan.from_excel_import_faskes = '2'
                    AND DATE_FORMAT(trx_pemeriksaan.created_at, '%Y-%m-%d') >= '2021-08-11'
                    AND DATE_FORMAT(trx_pemeriksaan.created_at, '%Y-%m-%d') between '".$start_date."' and '".$end_date."'
            GROUP BY peserta_id) AS A
        ")->row();
        
        $kasus_terkonfirmasi_luar_wilayah = $this->master_wilayah_model->query("
        SELECT 
            COUNT(id_peserta) AS jumlah
        FROM
            (SELECT 
                id_peserta
            FROM
                peserta
            INNER JOIN trx_terkonfirmasi_rilis ON id_peserta = peserta_id
            INNER JOIN master_wilayah ON id_master_wilayah = kelurahan_master_wilayah_id
            WHERE
                status = '1'
                    AND trx_terkonfirmasi_rilis.deleted_at IS NULL
                    AND peserta.deleted_at IS NULL
                    AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."'
                    AND kelurahan_master_wilayah_id = '109'
            GROUP BY peserta_id) AS A
        ")->row();
        
        $kasus_terkonfirmasi_antigen_luar_wilayah = $this->master_wilayah_model->query("
        SELECT 
            COUNT(id_peserta) AS jumlah
        FROM
            (SELECT 
                id_peserta
            FROM
                master_wilayah
            INNER JOIN peserta ON kelurahan_master_wilayah_id = id_master_wilayah
                AND peserta.`deleted_at` IS NULL
            INNER JOIN trx_pemeriksaan ON id_peserta = peserta_id
            WHERE
                trx_pemeriksaan.deleted_at IS NULL
                    AND kelurahan_master_wilayah_id = '109'
                    AND jenis_pemeriksaan_id = '2'
                    AND hasil_pemeriksaan_id = '3'
                    AND trx_pemeriksaan.from_excel_import_faskes = '2'
                    AND DATE_FORMAT(trx_pemeriksaan.created_at, '%Y-%m-%d') >= '2021-08-11'
                    AND DATE_FORMAT(trx_pemeriksaan.created_at, '%Y-%m-%d') between '".$start_date."' and '".$end_date."'
            GROUP BY peserta_id) AS A
        ")->row();
        
        $kasus_sembuh = $this->master_wilayah_model->query("
        SELECT 
            COUNT(*) AS jumlah
        FROM
            (SELECT 
                id_peserta
            FROM
                master_wilayah
            INNER JOIN peserta ON kelurahan_master_wilayah_id = id_master_wilayah
                AND peserta.`deleted_at` IS NULL
            WHERE
                id_peserta IN (SELECT 
                        peserta_id
                    FROM
                        trx_terkonfirmasi_rilis
                    WHERE
                        id_trx_terkonfirmasi_rilis IN (SELECT 
                                MAX(id_trx_terkonfirmasi_rilis)
                            FROM
                                trx_terkonfirmasi_rilis
                            WHERE
                                trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d') BETWEEN '".$start_date."' and '".$end_date."'
                            GROUP BY peserta_id)
                            AND STATUS = '2'
                            AND trx_terkonfirmasi_rilis.deleted_at IS NULL)) AS A
        ")->row();
        
        $kasus_meninggal = $this->master_wilayah_model->query("
        SELECT 
            COUNT(*) AS jumlah
        FROM
            (SELECT 
                id_peserta
            FROM
                master_wilayah
            INNER JOIN peserta ON kelurahan_master_wilayah_id = id_master_wilayah
                AND peserta.`deleted_at` IS NULL
            WHERE
                id_peserta IN (SELECT 
                        peserta_id
                    FROM
                        trx_terkonfirmasi_rilis
                    WHERE
                        id_trx_terkonfirmasi_rilis IN (SELECT 
                                MAX(id_trx_terkonfirmasi_rilis)
                            FROM
                                trx_terkonfirmasi_rilis
                            WHERE
                                trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d') BETWEEN '".$start_date."' and '".$end_date."'
                            GROUP BY peserta_id)
                            AND STATUS = '3'
                            AND trx_terkonfirmasi_rilis.deleted_at IS NULL)) AS A
        ")->row();

        $data = ["terkonfirmasi"=>(int) $kasus_terkonfirmasi_pcr->jumlah + (int) $kasus_terkonfirmasi_antigen_nar->jumlah,"luar_wilayah"=>(int) $kasus_terkonfirmasi_luar_wilayah->jumlah + (int) $kasus_terkonfirmasi_antigen_luar_wilayah->jumlah,"sembuh"=>(int) $kasus_sembuh->jumlah,"meninggal"=>(int) $kasus_meninggal->jumlah];
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    function get_statistik_vaksin()
    {
        $start_date = new DateTime($this->iget("start_date"));
        $end_date = new DateTime($this->iget("end_date"));

        $tanggal = [];
        $dosis_1 = [];
        $dosis_2 = [];
        $dosis_3 = [];
        for($i = $start_date;$i <= $end_date;$i->modify("+1 day")){
            array_push($tanggal,$i->format("d M"));

            $data_vaksin = $this->grafik_vaksin_model->get(
                array(
                    "where"=>array(
                        "tanggal"=>$i->format("Y-m-d")
                    )
                ),"row"
            );

            array_push($dosis_1,$data_vaksin?$data_vaksin->dosis_1:0);
            array_push($dosis_2,$data_vaksin?$data_vaksin->dosis_2:0);
            array_push($dosis_3,$data_vaksin?$data_vaksin->dosis_3:0);
        }

        $data = array("tanggal" => $tanggal,"dosis_1" => $dosis_1, "dosis_2" => $dosis_2, "dosis_3" => $dosis_3);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_rawat_inap()
    {
        $start_date = new DateTime($this->iget("start_date"));
        $end_date = new DateTime($this->iget("end_date"));

        $arr_tanggal =[];
        $arr_data =[];
        for($i = $start_date;$i <= $end_date;$i->modify("+1 day")){
            array_push($arr_tanggal,$i->format("d M"));

            $data_rawat_inap = $this->positivity_rate_model->query(
                "
                SELECT 
                    COUNT(id_peserta) AS jumlah
                FROM
                    (SELECT 
                        id_peserta
                    FROM
                        peserta
                    INNER JOIN trx_terkonfirmasi_rilis ON id_peserta = peserta_id
                    INNER JOIN master_wilayah ON id_master_wilayah = kelurahan_master_wilayah_id
                    LEFT JOIN (SELECT 
                        peserta_id, is_status
                    FROM
                        status_rawat
                    ORDER BY created_at
                    LIMIT 1) AS a ON a.peserta_id = id_peserta
                    WHERE
                        status = '1' AND a.is_status = '1'
                            AND trx_terkonfirmasi_rilis.deleted_at IS NULL
                            AND peserta.deleted_at IS NULL
                            AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d') = '".$i->format("Y-m-d")."'
                    GROUP BY id_peserta) AS A;
                "
            )->row();
            array_push($arr_data,$data_rawat_inap?(int) $data_rawat_inap->jumlah:0);
        }

        $data = ["tanggal"=>$arr_tanggal,"data"=>$arr_data];

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
