<div class="content">
    <!-- Basic datatable -->
    <div class="card">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Tanggal</label>
                <div class="col-lg-10">
                    <button type="button" class="btn btn-light daterange-predefined">
                        <i class="icon-calendar22 mr-2"></i>
                        <span></span>
                    </button>

                    <input type="hidden" name="start_date" />
                    <input type="hidden" name="end_date" />
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <ul class="nav nav-tabs">
                <li class="nav-item"><a href="#attack-rate" class="nav-link active" data-toggle="tab">Attack Rate</a></li>
                <li class="nav-item"><a href="#case-fatality-rate" class="nav-link" data-toggle="tab">Case Fatality Rate</a></li>
                <li class="nav-item"><a href="#case-positive-rate" class="nav-link" data-toggle="tab">Case Positive Rate</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade show active" id="attack-rate">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Kecamatan : </label>
                                    <select class="form-control select-search" name="kecamatan" onchange="get_data_attack_rate()">
                                        <option value="">-- Pilih Kecamatan --</option>
                                        <?php
                                        foreach ($list_kecamatan as $key => $row) {
                                        ?>
                                            <option value="<?php echo encrypt_data($row->kode_wilayah); ?>"><?php echo $row->nama_wilayah; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                Jumlah Kasus : <span class="jumlah_kasus_kecamatan"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                Attack Rate : <span class="attack_rate_kecamatan"></span>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="text-right">
                            <div style="display:inline-flex">
                                <span>Konstanta : </span>
                                <input style="width:auto;" type="text" name="konstanta_attack_rate" class="form-control ml-1 input-const-attack-rate" />
                                <a class='btn btn-info btn-icon ml-1' onClick="save_konstanta()" href='#'><i class='icon-check'></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="card card-table table-responsive shadow-0 mb-0">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Nama Wilayah</th>
                                    <th class="text-center">Jumlah Kasus</th>
                                    <th class="text-center">Jumlah Penduduk Desa</th>
                                    <th class="text-center">Attack Rate</th>
                                </tr>
                            </thead>
                            <tbody class="data_wilayah">

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="case-fatality-rate">
                    <div class="card card-table table-responsive shadow-0 mb-0">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-center">Jumlah Kasus Kematian</th>
                                    <th class="text-center">Jumlah Kasus Konfirmasi</th>
                                    <th class="text-center">Case Fatality Rate</th>
                                </tr>
                            </thead>
                            <tbody class="data_case_fatality_rate">

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="case-positive-rate">
                    <div class="card card-table table-responsive shadow-0 mb-0">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-center">Jumlah Kasus Positif</th>
                                    <th class="text-center">Jumlah Peserta SWAB</th>
                                    <th class="text-center">Case Positive Rate</th>
                                </tr>
                            </thead>
                            <tbody class="data_case_positive_rate">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /basic datatable -->
</div>

<script>
    function get_data_attack_rate() {
        let kode_wilayah = $("select[name='kecamatan']").val();
        let start_date = $("input[name='start_date']").val();
        let end_date = $("input[name='end_date']").val();
        $(".data_wilayah").html("");
        $(".jumlah_kasus_kecamatan").html("");
        $(".attack_rate_kecamatan").html("");
        if (kode_wilayah) {
            $.ajax({
                url: base_url + 'epidemiologi/request/get_data_attack_rate',
                data: {
                    kode_wilayah: kode_wilayah,
                    start_date: start_date,
                    end_date: end_date,
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    html = "";
                    $.each(response.per_wilayah, function(index, value) {
                        html += "<tr>" +
                            "<td>" + value.nama_wilayah + "</td>" +
                            "<td class='text-center'>" + value.jumlah_kasus_positif + "</td>" +
                            "<td class='text-center'><div class='col-6 d-inline-flex'><input type='text' class='form-control input-jumlah-penduduk-" + value.id_master_wilayah + "' name='jumlah_penduduk_" + value.id_master_wilayah + "' value='" + value.jumlah_penduduk + "' /><a class='btn btn-info btn-icon ml-1' onClick=\"calc_attack_rate(" + value.id_master_wilayah + ")\" href='#'><i class='icon-reload-alt'></i></a></div></td>" +
                            "<td class='text-center'>" + value.attack_rate + "</td>" +
                            "</tr>";
                    });

                    $(".jumlah_kasus_kecamatan").html(response.jumlah_kasus_kecamatan);
                    $(".attack_rate_kecamatan").html(response.kecamatan);

                    $(".data_wilayah").html(html);
                    $.each(response.per_wilayah, function(index, value) {
                        var cleave = new Cleave('.input-jumlah-penduduk-' + value.id_master_wilayah, {
                            numeral: true,
                            numeralThousandsGroupStyle: 'thousand',
                            numeralDecimalMark: ',',
                            delimiter: '.',
                        });
                    });
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        }
    }

    function get_data_case_fatality_rate() {
        $.ajax({
            url: base_url + 'epidemiologi/request/get_data_case_fatality_rate',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                html = "";
                html += "<tr>" +
                    "<td class='text-center'>" + response.jumlah_kasus_kematian + "</td>" +
                    "<td class='text-center'>" + response.jumlah_kasus_konfirmasi + "</td>" +
                    "<td class='text-center'>" + response.case_fatality_rate + "</td>" +
                    "</tr>";

                $(".data_case_fatality_rate").html(html);
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function get_data_case_positive_rate() {
        let start_date = $("input[name='start_date']").val();
        let end_date = $("input[name='end_date']").val();
        $.ajax({
            url: base_url + 'epidemiologi/request/get_data_case_positive_rate',
            data: {
                    start_date: start_date,
                    end_date: end_date,
                },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                html = "";
                html += "<tr>" +
                    "<td class='text-center'>" + response.jumlah_kasus_konfirmasi + "</td>" +
                    "<td class='text-center'>" + response.jumlah_peserta_tes + "</td>" +
                    "<td class='text-center'>" + response.case_positive_rate + "</td>" +
                    "</tr>";

                $(".data_case_positive_rate").html(html);
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function get_data_const_attack_rate() {
        $.ajax({
            url: base_url + 'epidemiologi/request/get_data_const_attack_rate',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $("input[name='konstanta_attack_rate']").val(response.const_attack_rate);
                var cleave = new Cleave('.input-const-attack-rate', {
                    numeral: true,
                    numeralThousandsGroupStyle: 'thousand',
                    numeralDecimalMark: ',',
                    delimiter: '.',
                });
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function calc_attack_rate(id_master_wilayah) {
        let val_jumlah_penduduk = $("input[name='jumlah_penduduk_" + id_master_wilayah + "']").val();
        $.ajax({
            url: base_url + 'epidemiologi/save_jumlah_penduduk',
            data: {
                id_master_wilayah: id_master_wilayah,
                val_jumlah_penduduk: val_jumlah_penduduk
            },
            type: 'POST',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                get_data_attack_rate();
                get_data_case_fatality_rate();
                get_data_case_positive_rate();
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function save_konstanta() {
        let val_konstanta = $("input[name='konstanta_attack_rate']").val();
        $.ajax({
            url: base_url + 'epidemiologi/save_konstanta',
            data: {
                val_konstanta: val_konstanta
            },
            type: 'POST',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                get_data_attack_rate();
                get_data_case_fatality_rate();
                get_data_case_positive_rate();
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    $('.daterange-predefined').daterangepicker({
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.daterange-predefined span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date]').val(end.format('YYYY-MM-DD'));
            get_data_attack_rate();
            get_data_case_positive_rate();
        }
    );

    // Display date format
    $('.daterange-predefined span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date]').val(moment().subtract(29, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date]').val(moment().format('YYYY-MM-DD'));
    get_data_attack_rate();
    get_data_case_fatality_rate();
    get_data_case_positive_rate();
    get_data_const_attack_rate();
</script>