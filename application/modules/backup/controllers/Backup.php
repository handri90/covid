<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Backup extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model("peserta/master_wilayah_model", "master_wilayah_model");
        $this->load->model("peserta/peserta_model", "peserta_model");
        $this->load->model("trx_pemeriksaan_model");
        $this->load->model("status_peserta/trx_terkonfirmasi_model", "trx_terkonfirmasi_model");
        $this->load->model("peserta_rilis/trx_terkonfirmasi_rilis_model", "trx_terkonfirmasi_rilis_model");
    }

    public function index()
    {
        // if (empty($_FILES)) {
        //     $data['breadcrumb'] = [['link' => false, 'content' => 'Backup', 'is_active' => true]];
        //     $this->execute('form_backup', $data);
        // } else {
        //     print_r($_FILES);
        //     die;
        // }
        // phpinfo();
        // $this->import_excel();
    }

    public function import_excel()
    {
        require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel.php');

        $excelreader = new PHPExcel_Reader_Excel2007();

        $input_file_name = './assets/rekap_covid.xlsx';
        $loadexcel       = $excelreader->load('./assets/rekap_covid.xlsx');

        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

        $numrow = 1;
        foreach ($sheet as $row) {
            if ($numrow > 0) {

                $pekerjaan      = NULL;
                $pekerjaan_lain = NULL;
                $db_pekerjaan   = array("wiraswasta", "asn", "tni/polri", "swasta", "tenaga kesehatan", "pelajar/mahasiswa");

                if (strtolower(trim($row['F'])) == "swasta" || strtolower(trim($row['F'])) == "karyawan" || strtolower(trim($row['F'])) == "buruh" || strtolower(trim($row['F'])) == "profesional" || strtolower(trim($row['F'])) == "karyawan swasta") {
                    $pekerjaan = "Swasta";
                } else if (strtolower(trim($row['F'])) == "wiraswasta" || strtolower(trim($row['F'])) == "perdagangan" || strtolower(trim($row['F'])) == "pedagang") {
                    $pekerjaan = "Wiraswasta";
                } else if (strtolower(trim($row['F'])) == "asn" || strtolower(trim($row['F'])) == "pns" || strtolower(trim($row['F'])) == "guru") {
                    $pekerjaan = "ASN";
                } else if (strtolower(trim($row['F'])) == "tni/polri" || strtolower(trim($row['F'])) == "tni" || strtolower(trim($row['F'])) == "polri" || strtolower(trim($row['F'])) == "polisi") {
                    $pekerjaan = "TNI/POLRI";
                } else if (strtolower(trim($row['F'])) == "nakes" || strtolower(trim($row['F'])) == "tenaga kesehatan" || strtolower(trim($row['F'])) == "bidan") {
                    $pekerjaan = "Tenaga Kesehatan";
                } else if (strtolower(trim($row['F'])) == "pelajar" || strtolower(trim($row['F'])) == "mahasiswa" || strtolower(trim($row['F'])) == "pelajar/mahasiswa") {
                    $pekerjaan = "Pelajar/Mahasiswa";
                } else if (!in_array(strtolower(trim($row['F'])), $db_pekerjaan)) {
                    $pekerjaan = "Lainnya";
                }

                $master_wilayah = $this->master_wilayah_model->get(
                    array(
                        "where" => array(
                            "nama_wilayah" => ucwords(strtolower(trim($row['K'])))
                        )
                    ),
                    "row"
                );

                $tanggal_lahir_val = NULL;
                if (trim($row['D']) != "") {
                    $excel_date = trim($row['D']); //here is that value 41621 or 41631
                    $tanggal_lahir = ($excel_date - 25569) * 86400;
                    $tanggal_lahir_val = gmdate("Y-m-d", $tanggal_lahir);
                }

                $data = array(
                    "nama"                        => trim($row['A']),
                    "nik"                         => trim($row['B']),
                    "tanggal_lahir"               => $tanggal_lahir_val,
                    "jenis_kelamin"               => trim($row['E']),
                    "pekerjaan_inti"              => $pekerjaan,
                    "is_copy_ktp"                 => "0",
                    "alamat_ktp"                  => trim($row['G']),
                    "alamat_domisili"                  => trim($row['I']),
                    "kelurahan_master_wilayah_id" => ($master_wilayah ? $master_wilayah->id_master_wilayah : "109"),
                    "nomor_telepon"               => str_replace("-", "", trim($row['L'])),
                    "from_excel"                  => "1",
                    'created_at'                  => date("Y-m-d h:i:s", (trim($row['M']) - 25569) * 86400),
                    'id_user_created'             => "4"
                );

                $status = $this->peserta_model->save($data);

                if ($status) {
                    $data_trx_pemeriksaan = array(
                        "peserta_id"           => $status,
                        "jenis_pemeriksaan_id" => "1",
                        "hasil_pemeriksaan_id" => "1",
                        "kode_sample"          => NULL,
                        "from_excel"           => "1",
                        'created_at'           => date("Y-m-d h:i:s", (trim($row['M']) - 25569) * 86400),
                        'updated_at'           => date("Y-m-d h:i:s", (trim($row['N']) - 25569) * 86400),
                        'id_user_created'      => "4"
                    );

                    $status_pemeriksaan = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);

                    $data_trx_terkonfirmasi = array(
                        "peserta_id"         => $status,
                        "from_excel"         => "1",
                        "status"             => "1",
                        "trx_pemeriksaan_id" => $status_pemeriksaan,
                        'created_at'         => date("Y-m-d h:i:s", (trim($row['N']) - 25569) * 86400),
                        'id_user_created'    => "3"
                    );

                    $status_terkonfirmasi = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi);

                    $data_trx_terkonfirmasi_rilis = array(
                        "peserta_id"            => $status,
                        "status"                => "1",
                        "trx_terkonfirmasi_id"  => $status_terkonfirmasi,
                        "trx_pemeriksaan_id"    => $status_pemeriksaan,
                        'tanggal_terkonfirmasi' => date("Y-m-d h:i:s", (trim($row['N']) - 25569) * 86400),
                        'created_at'            => date("Y-m-d h:i:s", (trim($row['N']) - 25569) * 86400),
                        'id_user_created'       => "3"
                    );

                    $status_terkonfirmasi_rilis = $this->trx_terkonfirmasi_rilis_model->save($data_trx_terkonfirmasi_rilis);

                    if (in_array(trim($row['O']), array("2", "3"))) {

                        if (trim($row['O']) == "2") {
                            $data_trx_pemeriksaan_sembuh_meninggal = array(
                                "peserta_id"           => $status,
                                "jenis_pemeriksaan_id" => NULL,
                                "hasil_pemeriksaan_id" => NULL,
                                "kode_sample"          => NULL,
                                "from_excel"           => "1",
                                "is_sembuh"            => "1",
                                'created_at'           => date("Y-m-d h:i:s", (trim($row['P']) - 25569) * 86400),
                                'id_user_created'      => "3"
                            );
                        } else if (trim($row['O']) == "3") {
                            $data_trx_pemeriksaan_sembuh_meninggal = array(
                                "peserta_id"           => $status,
                                "jenis_pemeriksaan_id" => NULL,
                                "hasil_pemeriksaan_id" => NULL,
                                "kode_sample"          => NULL,
                                "from_excel"           => "1",
                                "is_meninggal"         => "1",
                                'created_at'           => date("Y-m-d h:i:s", (trim($row['P']) - 25569) * 86400),
                                'id_user_created'      => "3"
                            );
                        }

                        $status_pemeriksaan_sembuh_meninggal = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan_sembuh_meninggal);

                        $data_trx_terkonfirmasi_2 = array(
                            "peserta_id"         => $status,
                            "from_excel"         => "1",
                            "status"             => trim($row['O']),
                            "trx_pemeriksaan_id" => $status_pemeriksaan_sembuh_meninggal,
                            'created_at'         => date("Y-m-d h:i:s", (trim($row['P']) - 25569) * 86400),
                            'id_user_created'    => "3"
                        );

                        $status_terkonfirmasi = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi_2);

                        $data_trx_terkonfirmasi_rilis = array(
                            "peserta_id"            => $status,
                            "status"                => trim($row['O']),
                            "trx_terkonfirmasi_id"  => $status_terkonfirmasi,
                            "trx_pemeriksaan_id"    => $status_pemeriksaan_sembuh_meninggal,
                            'tanggal_terkonfirmasi' => date("Y-m-d h:i:s", (trim($row['P']) - 25569) * 86400),
                            'created_at'            => date("Y-m-d h:i:s", (trim($row['P']) - 25569) * 86400),
                            'id_user_created'       => "3"
                        );

                        $this->trx_terkonfirmasi_rilis_model->save($data_trx_terkonfirmasi_rilis);
                    }
                }
            }

            $numrow++; // Tambah 1 setiap kali looping
        }
    }
}
