<?php

class Peserta_aktif_aplikasi_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "peserta_aktif_aplikasi";
        $this->primary_id = "id_peserta_aplikasi";
    }
}
