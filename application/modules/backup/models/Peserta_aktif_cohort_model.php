<?php

class Peserta_aktif_cohort_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "peserta_aktif_cohort";
        $this->primary_id = "id_peserta_aplikasi";
    }
}
