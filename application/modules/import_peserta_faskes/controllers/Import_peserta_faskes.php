<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Import_peserta_faskes extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model("peserta/master_wilayah_model", "master_wilayah_model");
        $this->load->model("peserta/peserta_model", "peserta_model");
        $this->load->model("peserta/peserta_non_puskesmas_model", "peserta_non_puskesmas_model");
        $this->load->model("trx_pemeriksaan_model");
        $this->load->model("trx_pemeriksaan_non_puskesmas_model");
        $this->load->model("master_rt_model");
        $this->load->model("status_peserta/trx_terkonfirmasi_model", "trx_terkonfirmasi_model");
        $this->load->model("peserta_rilis/trx_terkonfirmasi_rilis_model", "trx_terkonfirmasi_rilis_model");
    }

    public function index()
    {
        if (empty($_FILES)) {
            $data['breadcrumb'] = [['link' => false, 'content' => 'Import Peserta Puskesmas', 'is_active' => true]];
            $this->execute('form_backup', $data);
        } else {

            $input_name = 'file_excel';
            $upload_file = $this->upload_file($input_name, $this->config->item('path_excell_puskesmas'), "", "excell");

            $bool_upload_error = false;
            $bool_tanggal_pengambilan_swab = false;
            $tanggal_pengambilan_swab_row = 0;
            $bool_tanggal_hasil_swab = false;
            $tanggal_hasil_swab_row = 0;
            $bool_tanggal_lahir = false;
            $tanggal_lahir_row = 0;
            $bool_jenis_kelamin = false;
            $jenis_kelamin_row = 0;
            $bool_kelurahan = false;
            $kelurahan_row = 0;
            $bool_rt = false;
            $rt_row = 0;
            $bool_hasil_pemeriksaan = false;
            $hasil_pemeriksaan_row = 0;

            if (!isset($upload_file['error'])) {
                require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel.php');

                $excelreader = new PHPExcel_Reader_Excel2007();

                $objPHPExcel = PHPExcel_IOFactory::load('./' . $this->config->item('path_excell_puskesmas') . '/' . $upload_file['data']['file_name']);
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                $objWriter->save(str_replace('.xls', '.xlsx', './' . $this->config->item('path_excell_puskesmas') . '/' . $upload_file['data']['file_name']));

                chmod(str_replace('.xls', '.xlsx', './' . $this->config->item('path_excell_puskesmas') . '/' . $upload_file['data']['file_name']), 0777);

                $loadexcel = $excelreader->load(str_replace('.xls', '.xlsx', './' . $this->config->item('path_excell_puskesmas') . '/' . $upload_file['data']['file_name']));

                $sheet = $loadexcel->getActiveSheet();
                $worksheet = $objPHPExcel->getSheet(0);
                $lastRow = $worksheet->getHighestRow();

                for ($row = 1; $row <= $lastRow; $row++) {
                    if ($row > 1) {
                        $nik = str_replace('\'', '', $worksheet->getCell('B' . $row)->getValue());
                        $nama = $worksheet->getCell('A' . $row)->getValue();
                        $tanggal_lahir = $worksheet->getCell('C' . $row)->getValue();
                        $jenis_kelamin = $worksheet->getCell('F' . $row)->getValue();
                        $alamat_ktp = $worksheet->getCell('G' . $row)->getValue();
                        $alamat_domisili = $worksheet->getCell('H' . $row)->getValue();
                        $alamat_domisili_kabupaten = $worksheet->getCell('J' . $row)->getValue();
                        $desa_kelurahan = $worksheet->getCell('L' . $row)->getValue();
                        $rt_domisili = $worksheet->getCell('N' . $row)->getValue();
                        $hp = $worksheet->getCell('O' . $row)->getValue();
                        $tanggal_pemeriksaan = $worksheet->getCell('W' . $row)->getValue();
                        $hasil_pemeriksaan = $worksheet->getCell('Z' . $row)->getValue();

                        $tanggal_lahir_val = $tanggal_lahir;

                        $status_rt = false;

                        if ($alamat_domisili_kabupaten == "KOTAWARINGIN BARAT") {
                            $master_wilayah = $this->master_wilayah_model->get(
                                array(
                                    "where" => array(
                                        "nama_wilayah" => ucwords(strtolower(trim($desa_kelurahan))),
                                    ),
                                    "where_false" => "klasifikasi IN ('DESA','KEL')"
                                ),
                                "row"
                            );

                            if ($master_wilayah) {
                                if ($rt_domisili) {
                                    $get_data_master_rt = $this->master_rt_model->get(
                                        array(
                                            "where" => array(
                                                "master_wilayah_id" => $master_wilayah->id_master_wilayah,
                                                "rt" => $rt_domisili
                                            )
                                        ),
                                        "row"
                                    );
                                    if ($get_data_master_rt) {
                                        $status_rt = true;
                                    } else {
                                        $get_data_master_rt = $this->master_rt_model->get(
                                            array(
                                                "where" => array(
                                                    "master_wilayah_id" => $master_wilayah->id_master_wilayah,
                                                    "rt" => "00"
                                                )
                                            ),
                                            "row"
                                        );

                                        if ($get_data_master_rt) {
                                            $status_rt = true;
                                        } else {
                                            $status_rt = false;
                                        }
                                    }
                                } else {
                                    $get_data_master_rt = $this->master_rt_model->get(
                                        array(
                                            "where" => array(
                                                "master_wilayah_id" => $master_wilayah->id_master_wilayah,
                                                "rt" => "00"
                                            )
                                        ),
                                        "row"
                                    );
                                    if ($get_data_master_rt) {
                                        $status_rt = true;
                                    } else {
                                        $status_rt = false;
                                    }
                                }
                            }
                        } else {
                            $master_wilayah = array("id_master_wilayah" => "109");
                            $master_wilayah = ((object) $master_wilayah);
                            $status_rt = true;
                        }

                        $tanggal_lahir_expl = explode("-", $tanggal_lahir_val);
                        $tanggal_pemeriksaan_val_expl = explode("-", $tanggal_pemeriksaan);


                        if (count($tanggal_lahir_expl) == "3") {
                            //jika tanggal lahir benar
                            $checkbirthdate = checkdate((int) $tanggal_lahir_expl[1], (int) $tanggal_lahir_expl[2], (int) $tanggal_lahir_expl[0]);

                            if ($checkbirthdate) {
                                //jika tanggal lahir benar
                                if (in_array($jenis_kelamin, array("L", "P"))) {
                                    //jika jenis kelamin benar
                                    if (isset($master_wilayah)) {
                                        //jika desa/kelurahan benar

                                        if ($status_rt) {
                                            if (count($tanggal_pemeriksaan_val_expl) == "3") {
                                                //jika tanggal lahir benar
                                                $checkambilantigendate = checkdate((int) $tanggal_pemeriksaan_val_expl[1], (int) $tanggal_pemeriksaan_val_expl[2], (int) $tanggal_pemeriksaan_val_expl[0]);

                                                if ($checkambilantigendate) {

                                                    if (in_array($hasil_pemeriksaan, array("POSITIF", "NEGATIF"))) {
                                                        //jika hasil pemeriksaan benar
                                                        $bool_tanggal_pengambilan_swab = false;
                                                        $tanggal_pengambilan_swab_row = 0;
                                                        $bool_tanggal_hasil_swab = false;
                                                        $tanggal_hasil_swab_row = 0;
                                                        $bool_tanggal_lahir = false;
                                                        $tanggal_lahir_row = 0;
                                                        $bool_jenis_kelamin = false;
                                                        $jenis_kelamin_row = 0;
                                                        $bool_kelurahan = false;
                                                        $kelurahan_row = 0;
                                                        $bool_hasil_pemeriksaan = false;
                                                        $hasil_pemeriksaan_row = 0;
                                                        $bool_rt = false;
                                                        $rt_row = 0;
                                                    } else {
                                                        //jika hasil pemeriksaan benar
                                                        $bool_hasil_pemeriksaan = true;
                                                        $hasil_pemeriksaan_row = $row;
                                                        break;
                                                    }
                                                } else {
                                                    $bool_tanggal_pengambilan_swab = true;
                                                    $tanggal_pengambilan_swab_row = $row;
                                                    break;
                                                }
                                            } else {
                                                $bool_tanggal_pengambilan_swab = true;
                                                $tanggal_pengambilan_swab_row = $row;
                                                break;
                                            }
                                        } else {
                                            $bool_rt = true;
                                            $rt_row = $row;
                                            break;
                                        }
                                    } else {
                                        //jika desa/kelurahan salah
                                        $bool_kelurahan = true;
                                        $kelurahan_row = $row;
                                        break;
                                    }
                                } else {
                                    //jika jenis kelamin salah
                                    $bool_jenis_kelamin = true;
                                    $jenis_kelamin_row = $row;
                                    break;
                                }
                            } else {
                                //tanggal lahir salah
                                $bool_tanggal_lahir = true;
                                $tanggal_lahir_row = $row;
                                break;
                            }
                        } else {
                            //tangal lahir salah
                            $bool_tanggal_lahir = true;
                            $tanggal_lahir_row = $row;
                            break;
                        }
                    }
                }

                if ($bool_tanggal_lahir) {
                    $this->session->set_flashdata('message', 'Penulisan Tanggal Lahir Salah pada baris ' . $tanggal_lahir_row);
                    $this->session->set_flashdata('error', 'danger');
                } else if ($bool_jenis_kelamin) {
                    $this->session->set_flashdata('message', 'Penulisan Jenis Kelamin Salah pada baris ' . $jenis_kelamin_row);
                    $this->session->set_flashdata('error', 'danger');
                } else if ($bool_rt) {
                    $this->session->set_flashdata('message', 'Data RT tidak ada di database, pada baris ' . $rt_row);
                    $this->session->set_flashdata('error', 'danger');
                } else if ($bool_kelurahan) {
                    $this->session->set_flashdata('message', 'Penulisan Kelurahan Salah pada baris ' . $kelurahan_row);
                    $this->session->set_flashdata('error', 'danger');
                } else if ($bool_tanggal_pengambilan_swab) {
                    $this->session->set_flashdata('message', 'Penulisan Tanggal Pemeriksaan salah pada baris ' . $tanggal_pengambilan_swab_row);
                    $this->session->set_flashdata('error', 'danger');
                } else if ($bool_hasil_pemeriksaan) {
                    $this->session->set_flashdata('message', 'Penulisan Hasil Pemeriksaan Salah pada baris ' . $hasil_pemeriksaan_row);
                    $this->session->set_flashdata('error', 'danger');
                } else {
                    for ($row = 1; $row <= $lastRow; $row++) {
                        if ($row > 1) {
                            $nik = str_replace('\'', '', $worksheet->getCell('B' . $row)->getValue());
                            $nama = $worksheet->getCell('A' . $row)->getValue();
                            $tanggal_lahir = $worksheet->getCell('C' . $row)->getValue();
                            $jenis_kelamin = $worksheet->getCell('F' . $row)->getValue();
                            $alamat_ktp = $worksheet->getCell('G' . $row)->getValue();
                            $alamat_domisili = $worksheet->getCell('H' . $row)->getValue();
                            $alamat_domisili_kabupaten = $worksheet->getCell('J' . $row)->getValue();
                            $desa_kelurahan = $worksheet->getCell('L' . $row)->getValue();
                            $rt_domisili = $worksheet->getCell('N' . $row)->getValue();
                            $hp = $worksheet->getCell('O' . $row)->getValue();
                            $tanggal_pemeriksaan = $worksheet->getCell('W' . $row)->getValue();
                            $hasil_pemeriksaan = $worksheet->getCell('Z' . $row)->getValue();

                            if ($alamat_domisili_kabupaten == "KOTAWARINGIN BARAT") {
                                $master_wilayah = $this->master_wilayah_model->get(
                                    array(
                                        "where" => array(
                                            "nama_wilayah" => ucwords(strtolower(trim($desa_kelurahan))),
                                        ),
                                        "where_false" => "klasifikasi IN ('DESA','KEL')"
                                    ),
                                    "row"
                                );

                                $id_master_rt = NULL;
                                if ($master_wilayah) {
                                    if ($rt_domisili) {
                                        $get_data_master_rt = $this->master_rt_model->get(
                                            array(
                                                "where" => array(
                                                    "master_wilayah_id" => $master_wilayah->id_master_wilayah,
                                                    "rt" => $rt_domisili
                                                )
                                            ),
                                            "row"
                                        );
                                        if ($get_data_master_rt) {
                                            $id_master_rt = $get_data_master_rt->id_master_rt;
                                            $status_rt = true;
                                        } else {
                                            $get_data_master_rt = $this->master_rt_model->get(
                                                array(
                                                    "where" => array(
                                                        "master_wilayah_id" => $master_wilayah->id_master_wilayah,
                                                        "rt" => "00"
                                                    )
                                                ),
                                                "row"
                                            );

                                            if ($get_data_master_rt) {
                                                $id_master_rt = $get_data_master_rt->id_master_rt;
                                                $status_rt = true;
                                            } else {
                                                $status_rt = false;
                                            }
                                        }
                                    } else {
                                        $get_data_master_rt = $this->master_rt_model->get(
                                            array(
                                                "where" => array(
                                                    "master_wilayah_id" => $master_wilayah->id_master_wilayah,
                                                    "rt" => "00"
                                                )
                                            ),
                                            "row"
                                        );
                                        if ($get_data_master_rt) {
                                            $id_master_rt = $get_data_master_rt->id_master_rt;
                                            $status_rt = true;
                                        } else {
                                            $status_rt = false;
                                        }
                                    }
                                }
                            } else {
                                $master_wilayah = array("id_master_wilayah" => "109");
                                $master_wilayah = ((object) $master_wilayah);
                                $status_rt = true;
                            }

                            $asal_sampel = "2";

                            if ($hasil_pemeriksaan == "POSITIF") {
                                $hasil_pemeriksaan = "3";
                            } else if ($hasil_pemeriksaan == "NEGATIF") {
                                $hasil_pemeriksaan = "4";
                            }

                            if (in_array($this->session->userdata("level_user_id"), array("2", "3", "4"))) {
                                $cek_peserta_by_nik = $this->peserta_model->get(
                                    array(
                                        "where" => array(
                                            "nik" => $nik
                                        )
                                    ),
                                    "row"
                                );

                                if ($nik != "") {
                                    if ($cek_peserta_by_nik) {
                                        $data_trx_pemeriksaan = array(
                                            "peserta_id"               => $cek_peserta_by_nik->id_peserta,
                                            "jenis_pemeriksaan_id"     => "2",
                                            "hasil_pemeriksaan_id"     => $hasil_pemeriksaan,
                                            "usulan_faskes"            => ($hasil_pemeriksaan == "3" ? "1" : NULL),
                                            "kode_sample"              => NULL,
                                            "from_excel_import_faskes" => "2",
                                            "created_at"               => $tanggal_pemeriksaan . " " . date("H:i:s"),
                                            "id_user_created"          => $this->session->userdata("id_user"),
                                        );

                                        $status = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);
                                    } else {
                                        $data_peserta_insert = [
                                            "nama"                        => $nama,
                                            "nik"                         => $nik,
                                            "tanggal_lahir"               => $tanggal_lahir_val,
                                            "jenis_kelamin"               => $jenis_kelamin,
                                            "alamat_ktp"                  => $alamat_ktp,
                                            "alamat_domisili"             => $alamat_domisili,
                                            "rt_domisili"                 => $rt_domisili,
                                            "master_rt_domisili_id"       => $id_master_rt,
                                            "kelurahan_master_wilayah_id" => $master_wilayah->id_master_wilayah,
                                            "nomor_telepon"               => $hp,
                                            "from_excel_import_faskes"    => "2",
                                            "created_at"                  => $tanggal_pemeriksaan . " " . date("H:i:s"),
                                            "id_user_created"             => $this->session->userdata("id_user"),
                                        ];

                                        $id_peserta = $this->peserta_model->save($data_peserta_insert);

                                        //input trx pemeriksaan
                                        $data_trx_pemeriksaan = array(
                                            "peserta_id"               => $id_peserta,
                                            "jenis_pemeriksaan_id"     => "2",
                                            "hasil_pemeriksaan_id"     => $hasil_pemeriksaan,
                                            "usulan_faskes"            => ($hasil_pemeriksaan == "3" ? "1" : NULL),
                                            "kode_sample"              => NULL,
                                            "from_excel_import_faskes" => "2",
                                            "created_at"               => $tanggal_pemeriksaan . " " . date("H:i:s"),
                                            "id_user_created"          => $this->session->userdata("id_user"),
                                        );

                                        $status = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);
                                    }
                                } else {
                                    $data_peserta_insert = [
                                        "nama"                        => $nama,
                                        "nik"                         => $nik,
                                        "tanggal_lahir"               => $tanggal_lahir_val,
                                        "jenis_kelamin"               => $jenis_kelamin,
                                        "alamat_ktp"                  => $alamat_ktp,
                                        "alamat_domisili"             => $alamat_domisili,
                                        "rt_domisili"                 => $rt_domisili,
                                        "master_rt_domisili_id"       => $id_master_rt,
                                        "kelurahan_master_wilayah_id" => $master_wilayah->id_master_wilayah,
                                        "nomor_telepon"               => $hp,
                                        "from_excel_import_faskes"    => "2",
                                        "created_at"                  => $tanggal_pemeriksaan . " " . date("H:i:s"),
                                        "id_user_created"             => $this->session->userdata("id_user"),
                                    ];

                                    $id_peserta = $this->peserta_model->save($data_peserta_insert);

                                    //input trx pemeriksaan
                                    $data_trx_pemeriksaan = array(
                                        "peserta_id"               => $id_peserta,
                                        "jenis_pemeriksaan_id"     => "2",
                                        "hasil_pemeriksaan_id"     => $hasil_pemeriksaan,
                                        "usulan_faskes"            => ($hasil_pemeriksaan == "3" ? "1" : NULL),
                                        "kode_sample"              => NULL,
                                        "from_excel_import_faskes" => "2",
                                        "created_at"               => $tanggal_pemeriksaan . " " . date("H:i:s"),
                                        "id_user_created"          => $this->session->userdata("id_user"),
                                    );

                                    $status = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);
                                }
                            } else {
                                $cek_peserta_by_nik = $this->peserta_non_puskesmas_model->get(
                                    array(
                                        "where" => array(
                                            "nik" => $nik
                                        )
                                    ),
                                    "row"
                                );

                                if ($nik != "") {
                                    if ($cek_peserta_by_nik) {
                                        $data_trx_pemeriksaan = array(
                                            "peserta_non_puskesmas_id" => $cek_peserta_by_nik->id_peserta_non_puskesmas,
                                            "jenis_pemeriksaan_id"     => "2",
                                            "hasil_pemeriksaan_id"     => $hasil_pemeriksaan,
                                            "kode_sample"              => NULL,
                                            "from_excel_import_faskes" => "2",
                                            "created_at"               => $tanggal_pemeriksaan . " " . date("H:i:s"),
                                            "id_user_created"          => $this->session->userdata("id_user"),
                                        );

                                        $status = $this->trx_pemeriksaan_non_puskesmas_model->save($data_trx_pemeriksaan);

                                        if ($cek_peserta_by_nik->peserta_id) {
                                            $data_peserta = $this->peserta_model->get_by($cek_peserta_by_nik->peserta_id);

                                            $data_trx_pemeriksaan_linked = array(
                                                "peserta_id" => $cek_peserta_by_nik->peserta_id,
                                                "jenis_pemeriksaan_id" => "2",
                                                "hasil_pemeriksaan_id" => $hasil_pemeriksaan,
                                                "tracking_pemeriksaan" => "1",
                                                "from_non_puskes" => "1",
                                                "from_excel_import_faskes" => "2",
                                                "id_user_non_puskes" => $this->session->userdata("id_user"),
                                                "usulan_faskes" => ($hasil_pemeriksaan == "3" ? "1" : NULL),
                                                'created_at' => $tanggal_pemeriksaan . " " . date("H:i:s"),
                                                'id_user_created' => $data_peserta->id_user_created
                                            );

                                            $id_trx_pemeriksaan = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan_linked);

                                            $this->trx_pemeriksaan_non_puskesmas_model->edit($status, array("trx_pemeriksaan_id" => $id_trx_pemeriksaan));
                                        }
                                    } else {
                                        $data_peserta_insert = [
                                            "nama"                        => $nama,
                                            "nik"                         => $nik,
                                            "tanggal_lahir"               => $tanggal_lahir_val,
                                            "jenis_kelamin"               => $jenis_kelamin,
                                            "alamat_ktp"                  => $alamat_ktp,
                                            "alamat_domisili"             => $alamat_domisili,
                                            "rt_domisili"                 => $rt_domisili,
                                            "master_rt_domisili_id"       => $id_master_rt,
                                            "kelurahan_master_wilayah_id" => $master_wilayah->id_master_wilayah,
                                            "nomor_telepon"               => $hp,
                                            "from_excel_import_faskes"    => "2",
                                            "created_at"                  => $tanggal_pemeriksaan . " " . date("H:i:s"),
                                            "id_user_created"             => $this->session->userdata("id_user"),
                                        ];

                                        $id_peserta = $this->peserta_non_puskesmas_model->save($data_peserta_insert);

                                        //input trx pemeriksaan
                                        $data_trx_pemeriksaan = array(
                                            "peserta_non_puskesmas_id"               => $id_peserta,
                                            "jenis_pemeriksaan_id"     => "2",
                                            "hasil_pemeriksaan_id"     => $hasil_pemeriksaan,
                                            "kode_sample"              => NULL,
                                            "from_excel_import_faskes" => "2",
                                            "created_at"               => $tanggal_pemeriksaan . " " . date("H:i:s"),
                                            "id_user_created"          => $this->session->userdata("id_user"),
                                        );

                                        $status = $this->trx_pemeriksaan_non_puskesmas_model->save($data_trx_pemeriksaan);
                                    }
                                } else {
                                    $data_peserta_insert = [
                                        "nama"                        => $nama,
                                        "nik"                         => $nik,
                                        "tanggal_lahir"               => $tanggal_lahir_val,
                                        "jenis_kelamin"               => $jenis_kelamin,
                                        "alamat_ktp"                  => $alamat_ktp,
                                        "alamat_domisili"             => $alamat_domisili,
                                        "rt_domisili"                 => $rt_domisili,
                                        "master_rt_domisili_id"       => $id_master_rt,
                                        "kelurahan_master_wilayah_id" => $master_wilayah->id_master_wilayah,
                                        "nomor_telepon"               => $hp,
                                        "from_excel_import_faskes"    => "2",
                                        "created_at"                  => $tanggal_pemeriksaan . " " . date("H:i:s"),
                                        "id_user_created"             => $this->session->userdata("id_user"),
                                    ];

                                    $id_peserta = $this->peserta_non_puskesmas_model->save($data_peserta_insert);

                                    //input trx pemeriksaan
                                    $data_trx_pemeriksaan = array(
                                        "peserta_non_puskesmas_id"               => $id_peserta,
                                        "jenis_pemeriksaan_id"     => "2",
                                        "hasil_pemeriksaan_id"     => $hasil_pemeriksaan,
                                        "kode_sample"              => NULL,
                                        "from_excel_import_faskes" => "2",
                                        "created_at"               => $tanggal_pemeriksaan . " " . date("H:i:s"),
                                        "id_user_created"          => $this->session->userdata("id_user"),
                                    );

                                    $status = $this->trx_pemeriksaan_non_puskesmas_model->save($data_trx_pemeriksaan);
                                }
                            }
                        }
                    }
                    $this->session->set_flashdata('message', 'Import Data Excel Berhasil');
                    redirect("import_peserta_faskes");
                }
            } else {
                $this->session->set_flashdata('message', 'Import Data Excel Gagal');
                $this->session->set_flashdata('error', 'danger');
                redirect("import_peserta_faskes");
            }
        }
    }
}
