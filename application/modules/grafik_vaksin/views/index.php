<div class="content">
    <!-- Basic datatable -->
    <div class="card">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url(); ?>grafik_vaksin/tambah_grafik_vaksin" class="btn btn-info">Tambah</a>
                <a href="<?php echo base_url(); ?>grafik_vaksin/import_vaksin" class="btn btn-info">Import</a>
            </div>
        </div>
        <table id="datatableGrafikVaksin" class="table datatable-save-state">
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Dosis 1</th>
                    <th>Dosis 2</th>
                    <th>Dosis 3</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /basic datatable -->
</div>

<script>
    let datatableGrafikVaksin = $("#datatableGrafikVaksin").DataTable();
    get_data_grafik_vaksin();

    function get_data_grafik_vaksin() {
        datatableGrafikVaksin.clear().draw();
        $.ajax({
            url: base_url + 'grafik_vaksin/request/get_data_grafik_vaksin',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    datatableGrafikVaksin.row.add([
                        value.tanggal,
                        value.dosis_1,
                        value.dosis_2,
                        value.dosis_3,
                        "<a href='" + base_url + "grafik_vaksin/edit_grafik_vaksin/" + value.id_encrypt + "' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>"
                    ]).draw(false);
                });
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function confirm_delete(id_grafik_vaksin) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'grafik_vaksin/delete_grafik_vaksin',
                    data: {
                        id_grafik_vaksin: id_grafik_vaksin
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_data_grafik_vaksin();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_data_grafik_vaksin();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_data_grafik_vaksin();
                    }
                });
            }
        });
    }
</script>