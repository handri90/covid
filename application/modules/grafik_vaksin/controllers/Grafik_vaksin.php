<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Grafik_vaksin extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model('grafik_vaksin_model');
    }

    public function index()
    {
        $data['breadcrumb'] = [['link' => false, 'content' => 'Grafik Vaksin', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function tambah_grafik_vaksin()
    {
        if (empty($_POST)) {
            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'grafik_vaksin', 'content' => 'Grafik Vaksin', 'is_active' => false], ['link' => false, 'content' => 'Tambah Grafik Vaksin', 'is_active' => true]];
            $this->execute('form_grafik_vaksin', $data);
        } else {

            $exp_tanggal = explode("/",$this->ipost("tanggal"));
            $new_tanggal = $exp_tanggal[2]."-".$exp_tanggal[1]."-".$exp_tanggal[0];

            $data = array(
                "tanggal" => $new_tanggal,
                "dosis_1" => $this->ipost('dosis_1'),
                "dosis_2" => $this->ipost('dosis_2'),
                "dosis_3" => $this->ipost('dosis_3'),
                'created_at' => $this->datetime()
            );

            $status = $this->grafik_vaksin_model->save($data);
            if ($status) {
                $this->session->set_flashdata('message', 'Data baru berhasil ditambahkan');
            } else {
                $this->session->set_flashdata('message', 'Data baru gagal ditambahkan');
            }

            redirect('grafik_vaksin');
        }
    }

    public function edit_grafik_vaksin($id_grafik_vaksin)
    {
        $data_master = $this->grafik_vaksin_model->get_by(decrypt_data($id_grafik_vaksin));

        if (!$data_master) {
            $this->page_error();
        }

        if (empty($_POST)) {
            $exp_tanggal = explode("-",$data_master->tanggal);
            $data_master->tanggal = $exp_tanggal[2]."/".$exp_tanggal[1]."/".$exp_tanggal[0];

            $data['content'] = $data_master;
            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'grafik_vaksin', 'content' => 'Grafik Vaksin', 'is_active' => false], ['link' => false, 'content' => 'Tambah Grafik Vaksin', 'is_active' => true]];
            $this->execute('form_grafik_vaksin', $data);
        } else {
            $exp_tanggal = explode("/",$this->ipost("tanggal"));
            $new_tanggal = $exp_tanggal[2]."-".$exp_tanggal[1]."-".$exp_tanggal[0];

            $data = array(
                "tanggal" => $new_tanggal,
                "dosis_1" => $this->ipost('dosis_1'),
                "dosis_2" => $this->ipost('dosis_2'),
                "dosis_3" => $this->ipost('dosis_3'),
                'created_at' => $this->datetime()
            );

            $status = $this->grafik_vaksin_model->edit(decrypt_data($id_grafik_vaksin), $data);
            if ($status) {
                $this->session->set_flashdata('message', 'Data berhasil diubah');
            } else {
                $this->session->set_flashdata('message', 'Data gagal diubah');
            }

            redirect('grafik_vaksin');
        }
    }

    public function delete_grafik_vaksin()
    {
        $id_grafik_vaksin = decrypt_data($this->iget('id_grafik_vaksin'));
        $data_master = $this->grafik_vaksin_model->get_by($id_grafik_vaksin);

        if (!$data_master) {
            $this->page_error();
        }

        $status = $this->grafik_vaksin_model->remove($id_grafik_vaksin);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function import_vaksin(){
        if(empty($_FILES)){
            $data['breadcrumb'] = [['link' => false, 'content' => 'Import peserta RS', 'is_active' => true]];
            $this->execute('form_import', $data);
        }else{
            $input_name = 'file_excel';
            $upload_file = $this->upload_file($input_name, $this->config->item('path_excell'), "", "excell");

            if (!isset($upload_file['error'])) {
                require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel.php');

                $excelreader = new PHPExcel_Reader_Excel2007();

                $objPHPExcel = PHPExcel_IOFactory::load('./' . $this->config->item('path_excell') . '/' . $upload_file['data']['file_name']);
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                $objWriter->save(str_replace('.xls', '.xlsx', './' . $this->config->item('path_excell') . '/' . $upload_file['data']['file_name']));

                chmod(str_replace('.xls', '.xlsx', './' . $this->config->item('path_excell') . '/' . $upload_file['data']['file_name']), 0777);

                $loadexcel = $excelreader->load(str_replace('.xls', '.xlsx', './' . $this->config->item('path_excell') . '/' . $upload_file['data']['file_name']));

                $sheet = $loadexcel->getActiveSheet();
                $worksheet = $objPHPExcel->getSheet(0);
                $lastRow = $worksheet->getHighestRow();
                $realLastRow = 0;
                for ($row = 1; $row <= $lastRow; $row++) {
                    if($worksheet->getCell('B' . $row)->getValue() != ""){
                        $realLastRow++;
                    }
                }

                $tanggal_sebelum = 0;
                $jumlah_dosis_1 = 0;
                $jumlah_dosis_2 = 0;
                $jumlah_dosis_3 = 0;
                $str_insert = "";
                for ($row = 1; $row <= $lastRow; $row++) {
                    if ($row > 1) {
                        if($worksheet->getCell('B' . $row)->getValue() != ""){

                            $tanggal = $worksheet->getCell('A' . $row)->getValue();
                            $tanggal_convert = ($tanggal - 25569) * 86400;
                            $tanggal_val = gmdate("Y-m-d", $tanggal_convert);
                            $tanggal = $tanggal_val;
                            $dosis_1 = $worksheet->getCell('H' . $row)->getValue();
                            $dosis_2 = $worksheet->getCell('I' . $row)->getValue();
                            $dosis_3 = $worksheet->getCell('J' . $row)->getValue();
    
                            if($tanggal_sebelum == 0 || $tanggal_sebelum == $tanggal){
                                $jumlah_dosis_1 += $dosis_1;
                                $jumlah_dosis_2 += $dosis_2;
                                $jumlah_dosis_3 += $dosis_3;
    
                                if($row == $realLastRow){
                                    $cek_data_vaksin = $this->grafik_vaksin_model->get(
                                        array(
                                            "where"=>array(
                                                "tanggal"=>$tanggal,
                                            )
                                        ),"row"
                                    );
    
                                    if($cek_data_vaksin){
                                        //update
                                        $data_vaksin = array(
                                            "dosis_1"                => $jumlah_dosis_1,
                                            "dosis_2"                => $jumlah_dosis_2,
                                            "dosis_3"                => $jumlah_dosis_3,
                                            "created_at"             => $this->datetime(),
                                        );
                            
                                        $status = $this->grafik_vaksin_model->edit($cek_data_vaksin->id_grafik_vaksin,$data_vaksin);
                                    }else{
                                        //insert
                                        $data_vaksin = array(
                                            "tanggal"                => $tanggal,
                                            "dosis_1"                => $jumlah_dosis_1,
                                            "dosis_2"                => $jumlah_dosis_2,
                                            "dosis_3"                => $jumlah_dosis_3,
                                            "created_at"             => $this->datetime(),
                                        );
                            
                                        $status = $this->grafik_vaksin_model->save($data_vaksin);
                                    }
                                    $tanggal_sebelum = 0;
                                    $jumlah_dosis_1 = 0;
                                    $jumlah_dosis_2 = 0;
                                    $jumlah_dosis_3 = 0;
                                }
                                $tanggal_sebelum = $tanggal;
                            }else if($tanggal_sebelum != $tanggal){
                                $cek_data_vaksin = $this->grafik_vaksin_model->get(
                                    array(
                                        "where"=>array(
                                            "tanggal"=>$tanggal_sebelum,
                                        )
                                    ),"row"
                                );
    
                                if($cek_data_vaksin){
                                    //update
                                    $data_vaksin = array(
                                        "dosis_1"                => $jumlah_dosis_1,
                                        "dosis_2"                => $jumlah_dosis_2,
                                        "dosis_3"                => $jumlah_dosis_3,
                                        "created_at"             => $this->datetime(),
                                    );
                        
                                    $status = $this->grafik_vaksin_model->edit($cek_data_vaksin->id_grafik_vaksin,$data_vaksin);
                                }else{
                                    //insert
                                    $data_vaksin = array(
                                        "tanggal"                => $tanggal_sebelum,
                                        "dosis_1"                => $jumlah_dosis_1,
                                        "dosis_2"                => $jumlah_dosis_2,
                                        "dosis_3"                => $jumlah_dosis_3,
                                        "created_at"             => $this->datetime(),
                                    );
                        
                                    $status = $this->grafik_vaksin_model->save($data_vaksin);
                                }
                                $tanggal_sebelum = 0;
                                $jumlah_dosis_1 = 0;
                                $jumlah_dosis_2 = 0;
                                $jumlah_dosis_3 = 0;
    
                                if($row == $realLastRow){
                                    $jumlah_dosis_1 += $dosis_1;
                                    $jumlah_dosis_2 += $dosis_2;
                                    $jumlah_dosis_3 += $dosis_3;
        
                                    $cek_data_vaksin = $this->grafik_vaksin_model->get(
                                        array(
                                            "where"=>array(
                                                "tanggal"=>$tanggal,
                                            )
                                        ),"row"
                                    );
        
                                    if($cek_data_vaksin){
                                        //update
                                        $data_vaksin = array(
                                            "dosis_1"                => $jumlah_dosis_1,
                                            "dosis_2"                => $jumlah_dosis_2,
                                            "dosis_3"                => $jumlah_dosis_3,
                                            "created_at"             => $this->datetime(),
                                        );
                            
                                        $status = $this->grafik_vaksin_model->edit($cek_data_vaksin->id_grafik_vaksin,$data_vaksin);
                                    }else{
                                        //insert
                                        $data_vaksin = array(
                                            "tanggal"                => $tanggal,
                                            "dosis_1"                => $jumlah_dosis_1,
                                            "dosis_2"                => $jumlah_dosis_2,
                                            "dosis_3"                => $jumlah_dosis_3,
                                            "created_at"             => $this->datetime(),
                                        );
                                        $status = $this->grafik_vaksin_model->save($data_vaksin);
                                    }
    
                                    $tanggal_sebelum = 0;
                                    $jumlah_dosis_1 = 0;
                                    $jumlah_dosis_2 = 0;
                                    $jumlah_dosis_3 = 0;
                                }
                                $jumlah_dosis_1 += $dosis_1;
                                $jumlah_dosis_2 += $dosis_2;
                                $jumlah_dosis_3 += $dosis_3;
                                
                                $tanggal_sebelum = $tanggal;
                            }
                        }
                    }
                }
            }
            redirect("grafik_vaksin");
        }
    }
}
