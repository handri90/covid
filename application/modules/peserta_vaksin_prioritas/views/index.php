<style>
    .ft-bld {
        font-weight: bold;
    }
</style>

<div class="content">
    <div class="card">
        <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-highlight nav-justified">
                <li class="nav-item"><a href="#peserta-vaksinasi" class="nav-link active" data-toggle="tab" onclick="get_peserta_vaksin_puskesmas()">Peserta Vaksinasi</a></li>
                <li class="nav-item"><a href="#jadwal-vaksinasi" class="nav-link" data-toggle="tab" onclick="peserta_lanjut_vaksinasi()">Jadwal Vaksinasi</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade show active" id="peserta-vaksinasi">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Kelurahan</label>
                            <div class="col-lg-10">
                                <select class="form-control select-search" name="kelurahan_desa" onchange="get_rt()">
                                    <option value="">-- Semua --</option>
                                    <?php
                                    foreach ($master_wilayah as $key => $row) {
                                    ?>
                                        <option value="<?php echo encrypt_data($row->id_master_wilayah); ?>"><?php echo ucwords(strtolower($row->nama_wilayah)); ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">RT</label>
                            <div class="col-lg-10">
                                <select class="form-control select-search" name="rt_domisili" id="rt_domisili" onchange="reload_datatable()">
                                    <option value="">-- Semua --</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Kategori Ticket</label>
                            <div class="col-lg-10">
                                <select class="form-control select-search" name="kategori_ticket" onchange="reload_datatable()">
                                    <option value="">-- Semua --</option>
                                    <option value="1">Umum</option>
                                    <option value="2">Anak-Anak</option>
                                    <option value="3">Ibu Hamil</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Pilihan Dosis</label>
                            <div class="col-lg-10">
                                <select class="form-control select-search" name="pilihan_dosis" onchange="reload_datatable()">
                                    <option value="">-- Semua --</option>
                                    <option value="1">Dosis 1</option>
                                    <option value="2">Dosis 2</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Range Umur</label>
                            <div class="col-lg-10">
                                <div class="row">
                                    <div class="col-md-1">
                                        <input type="text" class="form-control" name="start_age" />
                                    </div>
                                    <div class="col-md-1 text-center">
                                        s/d
                                    </div>
                                    <div class="col-md-1">
                                        <input type="text" class="form-control" name="end_age" />
                                    </div>
                                    <div class="col-md-1">
                                        <a class='btn btn-info btn-icon' href='#reloadAge' onclick="reload_datatable()"><i class='icon-reload-alt'></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-table">
                        <div class="card-header header-elements-inline mb-1">
                            <div class="text-right">
                                <a href="#newPagePPKM" onclick="lanjut_vaksinasi()" id="new_page_ppkm" class="btn btn-success">Lanjut Vaksinasi</a>
                                <input type="hidden" name="arr_peserta_id" />
                            </div>
                        </div>
                        <table id="datatablePesertaVaksin" class="table datatable-save-state table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><input type="button" onclick="uncheck_all_checkbox()" value="Uncheck All" /> </th>
                                    <th>&nbsp;</th>
                                    <th>Nama</th>
                                    <th>NIK</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Umur</th>
                                    <th>Alamat</th>
                                    <th>Kelurahan/Desa</th>
                                    <th>RT</th>
                                    <th>Kategori Tiket</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="jadwal-vaksinasi">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Jadwal Vaksin</label>
                            <div class="col-lg-10">
                                <select class="form-control select-search" name="jadwal_vaksin" onchange="get_jenis_vaksin()">
                                    <option value="">-- Semua --</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Jenis Vaksin</label>
                            <div class="col-lg-10">
                                <select class="form-control select-search" name="jenis_vaksin" onchange="get_peserta_vaksin()">
                                    <option value="">-- Semua --</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-header header-elements-inline mb-1">
                        <div class="text-right">
                            <a href="#newPagePPKM" onclick="kembalikan_data_tidak_hadir()" id="new_page_ppkm" class="btn btn-success">Kembalikan Peserta Yang Tidak Hadir Ke Peserta Vaksinasi</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="card card-table">
                            <table class="table" style="width:40%">
                                <tr>
                                    <td>
                                        Jumlah Peserta Hadir
                                    </td>
                                    <td>:</td>
                                    <td class="peserta-hadir"></td>
                                </tr>
                                <tr>
                                    <td>
                                        Jumlah Peserta Tidak Hadir
                                    </td>
                                    <td>:</td>
                                    <td class="peserta-tidak-hadir"></td>
                                </tr>
                                <tr>
                                    <td>
                                        Jumlah Peserta Belum Konfirimasi
                                    </td>
                                    <td>:</td>
                                    <td class="peserta-belum-konfirmasi"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="card card-table table-responsive shadow-0 mb-0 peserta-jadwal-vaksinasi">
                        <table id="datatableJadwalVaksinasi" class="table table-bordered ">
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Nama</td>
                                    <td>NIK</td>
                                    <td>No Urut</td>
                                    <td>Status Konfirmasi</td>
                                    <td>Batas Waktu Konfirmasi</td>
                                    <td>Sudah Vaksin</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="showAturJadwal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <span class="alert_form"></span>
                <input type="hidden" name="id_daftar_vaksin" />
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Jadwal Vaksin : <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <select class="form-control select-search" name="jadwal_vaksin_input">
                            <option value="">-- Pilih Jadwal Vaksin --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Jenis Vaksin : <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <select class="form-control select-search" name="jenis_vaksin_input">
                            <option value="">-- Pilih Jenis Vaksin --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Sebagai Undangan Prioritas :</label>
                    <div class="col-lg-9">
                        <input type="checkbox" name="is_undangan_prioritas" onclick="show_input_kode()" />
                    </div>
                </div>
                <div class="form-group row hide-kode-prioritas">
                    <label class="col-form-label col-lg-3">Kode Undangan Prioritas :</label>
                    <div class="col-lg-1">
                        <input type="text" name="kode_undangan_prioritas" class="form-control" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Sebagai Undangan Cadangan :</label>
                    <div class="col-lg-9">
                        <input type="checkbox" name="is_undangan_cadangan" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Batas Konfirmasi : <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <select class="form-control select-search" name="batas_konfirmasi">
                            <option value="">-- Pilih --</option>
                            <option value="1">Sebelum</option>
                            <option value="2">Sesudah</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Satuan Waktu : <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <div class="row">
                            <div class='col-md-3'>
                                <select class="form-control select-search" name="satuan_interval" onchange="generate_waktu_interval()">
                                    <option value="">-- Pilih --</option>
                                    <option value="HOUR">JAM</option>
                                    <option value="MINUTE">MENIT</option>
                                </select>
                            </div>
                            <div class='col-md-3'>
                                <select class="form-control select-search" name="waktu_interval">
                                    <option value="">-- Pilih --</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-table table-responsive shadow-0 mb-0">
                    <table id="datablePesertaChecklist" class="table table-bordered ">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Nama</td>
                                <td>NIK</td>
                                <td>Tanggal Lahir</td>
                                <td>Umur</td>
                                <td>Kategori Tiket</td>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="text-right mt-2">
                    <button type="submit" class="btn btn-primary" onclick="action_lanjut_vaksin()">Lanjut Vaksin <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="showDetailPertanyaan" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="card card-table table-responsive shadow-0 mb-0">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>Nama</td>
                                <td><span class="nama_modal_pertanyaan"></span></td>
                            </tr>
                            <tr>
                                <td>Nik</td>
                                <td><span class="nik_modal_pertanyaan"></span></td>
                            </tr>
                            <tr>
                                <td>Tanggal Lahir</td>
                                <td><span class="tanggal_lahir_modal_pertanyaan"></span></td>
                            </tr>
                            <tr>
                                <td>Umur</td>
                                <td><span class="umur_modal_pertanyaan"></span></td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td><span class="alamat_modal_pertanyaan"></span></td>
                            </tr>
                            <tr>
                                <td>Kelurahan/Desa</td>
                                <td><span class="kelurahan_desa_modal_pertanyaan"></span></td>
                            </tr>
                            <tr>
                                <td>RT</td>
                                <td><span class="rt_modal_pertanyaan"></span></td>
                            </tr>
                            <tr>
                                <td>Kategori Tiket</td>
                                <td><span class="kategori_tiket_modal_pertanyaan"></span></td>
                            </tr>
                            <tr class="usia_kehamilan_panel">
                                <td>Usia Kehamilan</td>
                                <td><span class="usia_kehamilan_modal_pertanyaan"></span></td>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="modal-body">
                <div class="card card-table table-responsive shadow-0 mb-0">
                    <table class="table">
                        <thead class="load_pertanyaan">

                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(".hide-kode-prioritas").hide();
    let arr_peserta_id = [];

    function reload_datatable() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let start_age = $("input[name='start_age']").val();
        let end_age = $("input[name='end_age']").val();
        if ((start_age && !end_age) || (!start_age && end_age)) {
            swalInit(
                'Gagal',
                'Range Umur Harus Diisi',
                'error'
            );
        } else if (start_age && end_age) {
            if (start_age > end_age) {
                swalInit(
                    'Gagal',
                    'Range Umur Dari Terkecil sampai Terbesar',
                    'error'
                );
            }
        }
        $("#datatablePesertaVaksin").DataTable().ajax.reload();
    }

    get_peserta_vaksin_puskesmas();

    function get_peserta_vaksin_puskesmas() {
        $("select[name='jadwal_vaksin']").html("");
        if ($.fn.DataTable.isDataTable('#datatablePesertaVaksin')) {
            $('#datatablePesertaVaksin').DataTable().clear();
            $('#datatablePesertaVaksin').DataTable().destroy();
        }

        $("#datatablePesertaVaksin").DataTable({
            ajax: {
                "url": base_url + 'peserta_vaksin_prioritas/request/get_peserta_vaksin_puskesmas',
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "data": function(d) {
                    return $.extend({}, d, {
                        "kategori_ticket": $("select[name='kategori_ticket']").val(),
                        "pilihan_dosis": $("select[name='pilihan_dosis']").val(),
                        "kel_desa": $("select[name='kelurahan_desa']").val(),
                        "rt": $("select[name='rt_domisili']").val(),
                        "start_age": $("input[name='start_age']").val(),
                        "end_age": $("input[name='end_age']").val(),
                        "arr_peserta_id": $("input[name='arr_peserta_id']").val(),
                    });
                },
                "dataSrc": '',
                "complete": function(response) {
                    HoldOn.close();
                }
            },
            "ordering": false,
            "columns": [{
                data: "checkbox_vaksin"
            }, {
                "width": "15%",
                "render": function(data, type, full, meta) {
                    return "<a class='btn btn-info btn-icon' onClick=\"show_detail_pertanyaan('" + full.id_encrypt + "')\" href='#DetailPertanyaanPendaftar'><i class='icon-eye'></i> Detail Peserta</a>";
                }
            }, {
                data: "nama"
            }, {
                data: "nik"
            }, {
                data: "tanggal_lahir"
            }, {
                data: "umur"
            }, {
                data: "alamat"
            }, {
                data: "nama_wilayah"
            }, {
                data: "rt"
            }, {
                data: "kategori_ticket"
            }]
        });
    }

    function show_detail_pertanyaan(id_daftar_vaksin) {
        $(".usia_kehamilan_panel").hide();
        $.ajax({
            url: base_url + 'peserta_vaksin_prioritas/request/get_daftar_peserta_vaksin_by_id',
            data: {
                id_daftar_vaksin: id_daftar_vaksin
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $("#showDetailPertanyaan").modal("show");
                $(".nama_modal_pertanyaan").html(response.nama);
                $(".nik_modal_pertanyaan").html(response.nik);
                $(".tanggal_lahir_modal_pertanyaan").html(response.tanggal_lahir);
                $(".umur_modal_pertanyaan").html(response.umur);
                $(".alamat_modal_pertanyaan").html(response.alamat);
                $(".kelurahan_desa_modal_pertanyaan").html(response.nama_wilayah);
                $(".rt_modal_pertanyaan").html(response.rt);
                $(".kategori_tiket_modal_pertanyaan").html(response.kategori_ticket_custom);
                if (response.kategori_ticket == "3") {
                    $(".usia_kehamilan_panel").show();
                } else {
                    $(".usia_kehamilan_panel").hide();
                }
                $(".usia_kehamilan_modal_pertanyaan").html(response.usia_kehamilan);

                let html_pertanyaan = "";

                if (response.kategori_ticket == "1") {
                    html_pertanyaan = "<tr>" +
                        "<td>Apakah anda memiliki riwayat alergi berat seperti sesak napas, bengkak dan urtikaria seluruh badan atau reaksi berat lainnya karena vaksin ? </td>" +
                        "<td>" + response.pertanyaan_1 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah anda memiliki riwayat alergi berat setelah divaksinasi COVID-19 sebelumnya?</td>" +
                        "<td>" + response.pertanyaan_2 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah Anda mengidap penyakit autoimun seperti lupus</td>" +
                        "<td>" + response.pertanyaan_3 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah Anda sedang mendapat pengobatan untuk gangguan pembekuan darah, kelainan darah, defisiensi imun dan penerima produk darah/transfusi?</td>" +
                        "<td>" + response.pertanyaan_4 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah Anda sedang mendapat pengobatan immunosupressant seperti kortikosteroid dan kemoterapi?</td>" +
                        "<td>" + response.pertanyaan_5 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah Anda memiliki penyakit jantung berat atau asma dalam keadaan sesak?</td>" +
                        "<td>" + response.pertanyaan_6 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah Anda pernah terkonfirmasi menderita COVID-19?</td>" +
                        "<td>" + response.pertanyaan_7 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah Anda mengalami kesulitan untuk naik 10 anak tangga?</td>" +
                        "<td>" + response.pertanyaan_8 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah Anda sering merasa kelelahan?</td>" +
                        "<td>" + response.pertanyaan_9 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah Anda memiliki paling sedikit 5 dari 11 penyakit (Hipertensi, diabetes, kanker, penyakit paru kronis, serangan jantung, gagal jantung kongestif, nyeri dada, asma, nyeri sendi, stroke dan penyakit ginjal)?</td>" +
                        "<td>" + response.pertanyaan_10 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah Anda mengalami kesulitan berjalan kira-kira 100 sampai 200 meter?</td>" +
                        "<td>" + response.pertanyaan_11 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah Anda mengalami penurunan berat badan yang bermakna dalam setahun terakhir?</td>" +
                        "<td>" + response.pertanyaan_12 + "</td>" +
                        "</tr>";
                } else if (response.kategori_ticket == "2") {
                    html_pertanyaan = "<tr>" +
                        "<td>Apakah anak mendapat vaksin lain kurang dari 1 bulan sebelumnya? </td>" +
                        "<td>" + response.pertanyaan_1 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah anak pernah sakit COVID-19?</td>" +
                        "<td>" + response.pertanyaan_2 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah dalam keluarga terdapat kontak dengan pasien COVID-19?</td>" +
                        "<td>" + response.pertanyaan_3 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah dalam 7 hari terakhir anak menderita demam atau batuk pilek atau nyeri menelan atau muntah atau diare?</td>" +
                        "<td>" + response.pertanyaan_4 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah dalam 7 hari terakhir anak perlu perawatan di RS atau menderita kedaruratan medis seperti sesak napas, kejang, tidak sadar, berdebar-debar, perdarahan, hipertensi, tremor hebat?</td>" +
                        "<td>" + response.pertanyaan_5 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah anak sedang menderita gangguan imunitas (hiperimun: auto imun, alergi berat dan defisiensi imun: gizi buruk, HIV berat, keganasan)?</td>" +
                        "<td>" + response.pertanyaan_6 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah saat ini anak sedang menjalani pengobatan imunosupresan jangka panjang (stereoid lebih dari 2 minggu, sitostika)?</td>" +
                        "<td>" + response.pertanyaan_7 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah anak mempunyai riwayat alergi berat seperti sesak napas, bengkak, urtikaria, di seluruh tubuh atau gejala syok anafilaksis (tidak sadar) setelah vaksinasi sebelumnya?</td>" +
                        "<td>" + response.pertanyaan_8 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah anak penyandang penyakit hemofilia/kelainan pembekuan darah?</td>" +
                        "<td>" + response.pertanyaan_9 + "</td>" +
                        "</tr>" +
                        "<tr>";
                } else if (response.kategori_ticket == "3") {
                    html_pertanyaan = "<tr>" +
                        "<td>Apakah Ibu Memiliki Keluhan dan tanda preeklampsia</td>" +
                        "<td>" + response.pertanyaan_1 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah Anda mempunyai penyakit penyerta, seperti</td>" +
                        "<td>" + response.pertanyaan_2 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah Anda mengidap penyakit autoimun seperti lupus</td>" +
                        "<td>" + response.pertanyaan_3 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah Anda sedang mendapat pengobatan untuk gangguan pembekuan darah, kelainan darah, defisiensi imun dan penerima produk darah/transfusi?</td>" +
                        "<td>" + response.pertanyaan_4 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah Anda sedang mendapat pengobatan immunosupressant seperti kortikosteroid dan kemoterapi?</td>" +
                        "<td>" + response.pertanyaan_5 + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Apakah Anda pernah terkonfirmasi menderita COVID-19?</td>" +
                        "<td>" + response.pertanyaan_6 + "</td>" +
                        "</tr>" +
                        "<tr>";
                }

                $(".load_pertanyaan").html(html_pertanyaan);

            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function get_rt() {
        $.ajax({
            url: base_url + 'peserta_vaksin_prioritas/request/get_master_rt',
            data: {
                kel_desa: $("select[name='kelurahan_desa']").val()
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let html = "<option value=''>-- Pilih RT --</option>";
                $.each(response, function(index, value) {
                    html += "<option value='" + value.id_encrypt + "'>" + value.rt + "</option>";
                });
                $("select[name='rt_domisili']").html(html);
                $("#datatablePesertaVaksin").DataTable().ajax.reload();
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function uncheck_all_checkbox() {
        arr_peserta_id = [];
        $("input[name='arr_peserta_id']").val("");
        $("input[name='id_daftar_vaksin[]']").prop('checked', false);
    }

    function select_for_hasil_skrining(e) {
        if (arr_peserta_id.includes($(e, this).val()) === false) {
            arr_peserta_id.push($(e, this).val());
        } else {
            var index = arr_peserta_id.indexOf($(e, this).val());
            if (index !== -1) {
                arr_peserta_id.splice(index, 1);
            }
        }

        $("input[name='arr_peserta_id']").val(arr_peserta_id);
    }

    function lanjut_vaksinasi() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        if (arr_peserta_id.length == 0) {
            swalInit(
                'Gagal',
                'Peserta belum dipilih',
                'error'
            );

            return false;
        } else {
            $("#showAturJadwal").modal("show");
            $("input[name='is_undangan_prioritas']").prop('checked', false);
            $(".hide-kode-prioritas").hide();
            $("input[name='id_daftar_vaksin']").val(arr_peserta_id);
            load_jenis_vaksin();
            load_jadwal_vaksin();
            get_peserta_checlist();
        }
    }

    function get_peserta_checlist() {
        if ($.fn.DataTable.isDataTable('#datablePesertaChecklist')) {
            $('#datablePesertaChecklist').DataTable().clear();
            $('#datablePesertaChecklist').DataTable().destroy();
        }
        $("#datablePesertaChecklist").DataTable({
            ajax: {
                "url": base_url + 'peserta_vaksin_prioritas/request/get_peserta_checlist',
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "data": function(d) {
                    return $.extend({}, d, {
                        "id_daftar_vaksin": arr_peserta_id,
                    });
                },
                "dataSrc": '',
                "complete": function(response) {
                    HoldOn.close();
                }
            },
            "ordering": false,
            "columns": [{
                data: "no"
            }, {
                data: "nama"
            }, {
                data: "nik"
            }, {
                data: "tanggal_lahir"
            }, {
                data: "umur"
            }, {
                data: "kategori_ticket"
            }]
        });
    }

    function load_jenis_vaksin() {
        $.ajax({
            url: base_url + 'peserta_vaksin_prioritas/request/get_jenis_vaksin',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let html = "<option value=''>-- Pilih Jenis Vaksin --</option>";
                $.each(response, function(index, value) {
                    html += "<option value='" + value.id_encrypt + "'>" + value.nama_vaksin + "</option>";
                });
                $("select[name='jenis_vaksin_input']").html(html);
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function load_jadwal_vaksin() {
        $.ajax({
            url: base_url + 'peserta_vaksin_prioritas/request/get_jadwal_vaksin',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let html = "<option value=''>-- Pilih Jadwal Vaksin --</option>";
                $.each(response, function(index, value) {
                    html += "<option value='" + value.id_encrypt + "'>" + value.tempat_pelaksanaan + " - " + value.tanggal_jam_custom + "</option>";
                });
                $("select[name='jadwal_vaksin_input']").html(html);
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function get_jenis_vaksin() {
        $.ajax({
            url: base_url + 'peserta_vaksin_prioritas/request/get_jadwal_jenis_vaksinasi',
            data: {
                "jadwal_vaksin": $("select[name='jadwal_vaksin']").val()
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let html = "<option value=''>-- Semua --</option>";
                $.each(response, function(index, value) {
                    html += "<option value='" + value.id_encrypt + "'>" + value.nama_vaksin + "</option>";
                });
                $("select[name='jenis_vaksin']").html(html);
                get_peserta_vaksin();
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function action_lanjut_vaksin() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        jenis_vaksin = $("select[name='jenis_vaksin_input']").val();
        jadwal_vaksin = $("select[name='jadwal_vaksin_input']").val();
        batas_konfirmasi = $("select[name='batas_konfirmasi']").val();
        satuan_interval = $("select[name='satuan_interval']").val();
        waktu_interval = $("select[name='waktu_interval']").val();
        is_checked_undangan_cadangan = $("input[name='is_undangan_cadangan']").is(":checked");
        id_daftar_vaksin = $("input[name='id_daftar_vaksin']").val();
        is_undangan_prioritas = $("input[name='is_undangan_prioritas']").is(":checked");
        kode_undangan_prioritas = $("input[name='kode_undangan_prioritas']").val();

        if (!jenis_vaksin) {
            $(".alert_form").html("<div class='alert alert-danger'>Jenis Vaksin tidak boleh kosong.</div>");
        } else if (!jadwal_vaksin) {
            $(".alert_form").html("<div class='alert alert-danger'>Jadwal Vaksin tidak boleh kosong.</div>");
        } else if (!batas_konfirmasi) {
            $(".alert_form").html("<div class='alert alert-danger'>Batas Konfirmasi tidak boleh kosong.</div>");
        } else if (!satuan_interval) {
            $(".alert_form").html("<div class='alert alert-danger'>Satuan Waktu tidak boleh kosong.</div>");
        } else if (!waktu_interval) {
            $(".alert_form").html("<div class='alert alert-danger'>Satuan Waktu tidak boleh kosong.</div>");
        } else if (is_undangan_prioritas && !kode_undangan_prioritas) {
            $(".alert_form").html("<div class='alert alert-danger'>Kode Prioritas tidak boleh kosong.</div>");
        } else {
            swalInit({
                title: 'Hasil Skrining "Lanjut Vaksin!"',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya!',
                cancelButtonText: 'Batal!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'peserta_vaksin_prioritas/save_lanjut_vaksin',
                        data: {
                            jenis_vaksin: jenis_vaksin,
                            jadwal_vaksin: jadwal_vaksin,
                            batas_konfirmasi: batas_konfirmasi,
                            satuan_interval: satuan_interval,
                            waktu_interval: waktu_interval,
                            is_checked_undangan_cadangan: is_checked_undangan_cadangan,
                            id_daftar_vaksin: id_daftar_vaksin,
                            is_undangan_prioritas: is_undangan_prioritas,
                            kode_undangan_prioritas: kode_undangan_prioritas,
                        },
                        type: 'POST',
                        beforeSend: function() {
                            HoldOn.open(optionsHoldOn);
                        },
                        success: function(response) {
                            $("select[name='jenis_vaksin_input']").html("");
                            $("select[name='jadwal_vaksin_input']").html("");
                            $("select[name='waktu_interval']").html("<option value=''>-- Pilih --</option>");
                            $("select[name='batas_konfirmasi']").val('');
                            $("select[name='satuan_interval']").val('');
                            $("input[name='is_undangan_cadangan']").prop('checked', false);
                            $("input[name='id_daftar_vaksin']").val("");
                            $("input[name='is_undangan_prioritas']").prop('checked', false);
                            $("input[name='kode_undangan_prioritas']").val("");
                            $(".alert_form").html("");
                            $("#showAturJadwal").modal("toggle");
                            arr_peserta_id = [];
                            $("input[name='arr_peserta_id']").val("");
                            if (response) {
                                reload_datatable();
                                swalInit(
                                    'Berhasil',
                                    'Hasil Skrining Berhasil Ditambahkan',
                                    'success'
                                );
                            } else {
                                reload_datatable();
                                swalInit(
                                    'Gagal',
                                    'Hasil Skrining Gagal Ditambahkan',
                                    'error'
                                );
                            }
                        },
                        complete: function(response) {
                            HoldOn.close();
                        }
                    });
                } else if (result.dismiss === swal.DismissReason.cancel) {
                    swalInit(
                        'Batal',
                        'Data masih tersimpan!',
                        'error'
                    ).then(function(results) {
                        HoldOn.close();
                        if (result.results) {
                            reload_datatable();
                        }
                    });
                }
            });
        }
    }

    function peserta_lanjut_vaksinasi() {
        arr_peserta_id = [];
        $("input[name='arr_peserta_id']").val("");
        $("input[name='id_daftar_vaksin[]']").prop('checked', false);
        get_jadwal_vaksinasi();
        get_peserta_vaksin();
    }

    function get_jadwal_vaksinasi() {
        $.ajax({
            url: base_url + 'peserta_vaksin_prioritas/request/get_jadwal_vaksin',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let html = "<option value=''>-- Semua --</option>";
                $.each(response, function(index, value) {
                    html += "<option value='" + value.id_encrypt + "'>" + value.tempat_pelaksanaan + " - " + value.tanggal_jam_custom + "</option>";
                });
                $("select[name='jadwal_vaksin']").html(html);
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function get_peserta_vaksin() {
        if ($.fn.DataTable.isDataTable('#datatableJadwalVaksinasi')) {
            $('#datatableJadwalVaksinasi').DataTable().clear();
            $('#datatableJadwalVaksinasi').DataTable().destroy();
        }
        $("#datatableJadwalVaksinasi").DataTable({
            ajax: {
                "url": base_url + 'peserta_vaksin_prioritas/request/get_peserta_jadwal_vaksin',
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "data": function(d) {
                    return $.extend({}, d, {
                        "jadwal_vaksin": $("select[name='jadwal_vaksin']").val(),
                        "jenis_vaksin": $("select[name='jenis_vaksin']").val(),
                    });
                },
                "dataSrc": function(json) {
                    $(".peserta-hadir").html(json.info_konfirmasi.jumlah_hadir);
                    $(".peserta-tidak-hadir").html(json.info_konfirmasi.jumlah_tidak_hadir);
                    $(".peserta-belum-konfirmasi").html(json.info_konfirmasi.jumlah_belum_konfirmasi);
                    return json.data;
                },
                "complete": function(response) {
                    HoldOn.close();
                }
            },
            "ordering": false,
            "columns": [{
                data: "no"
            }, {
                data: "nama"
            }, {
                data: "nik"
            }, {
                data: "no_antrian"
            }, {
                data: "status_hadir"
            }, {
                data: "tanggal_terakhir_konfirmasi"
            }, {
                data: "checkbox_vaksin_selesai"
            }]
        });
    }

    function generate_waktu_interval() {
        let satuan_waktu = $("select[name='satuan_interval']").val();
        let html = "<option value=''>-- Pilih --</option>";
        if (satuan_waktu == 'HOUR') {
            for (let i = 1; i <= 48; i++) {
                html += "<option value='" + i + "'>" + i + "</option>";
            }
        } else if (satuan_waktu == 'MINUTE') {
            for (let i = 1; i <= 60; i++) {
                html += "<option value='" + i + "'>" + i + "</option>";
            }
        }

        $("select[name='waktu_interval']").html(html);
    }

    function show_input_kode() {
        $("input[name='kode_undangan_prioritas']").val("");
        is_undangan_prioritas = $("input[name='is_undangan_prioritas']").is(":checked");

        if (is_undangan_prioritas) {
            $(".hide-kode-prioritas").show();
        } else {
            $(".hide-kode-prioritas").hide();
        }
    }
</script>