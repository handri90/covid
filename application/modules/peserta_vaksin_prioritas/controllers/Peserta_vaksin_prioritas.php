<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Peserta_vaksin_prioritas extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model("daftar_vaksin_model");
        $this->load->model("peserta_vaksin/detail_daftar_vaksin_model", "detail_daftar_vaksin_model");
        $this->load->model("user_vaksin_model");
        $this->load->model("master_puskesmas_model");
        $this->load->model('peserta/master_wilayah_model', 'master_wilayah_model');
    }

    public function index()
    {
        $data['master_wilayah'] = $this->master_wilayah_model->get(
            array(
                "fields" => "master_wilayah.*,concat(klasifikasi,' - ',nama_wilayah) as nama_wilayah",
                "where" => array(
                    "id_master_wilayah != " => "109",
                ),
                "where_false" => "klasifikasi NOT IN ('PROV','KAB','KEC')",
                "order_by_false" => 'field (klasifikasi,"KEC","KEL","DESA"),nama_wilayah'
            )
        );

        $data['breadcrumb'] = [['link' => false, 'content' => 'Peserta Vaksinasi Prioritas', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function save_lanjut_vaksin()
    {
        $id_daftar_vaksin = $this->ipost("id_daftar_vaksin");
        $batas_konfirmasi = $this->ipost("batas_konfirmasi");
        $satuan_interval = $this->ipost("satuan_interval");
        $waktu_interval = $this->ipost("waktu_interval");
        $is_checked_undangan_cadangan = $this->ipost("is_checked_undangan_cadangan");
        $jenis_vaksin = decrypt_data($this->ipost("jenis_vaksin"));
        $jadwal_vaksin = decrypt_data($this->ipost("jadwal_vaksin"));
        $is_undangan_prioritas = $this->ipost("is_undangan_prioritas");
        $kode_undangan_prioritas = $this->ipost("kode_undangan_prioritas");

        $exp_id_daftar_vaksin = explode(",", $id_daftar_vaksin);

        if ($is_undangan_prioritas == 'true') {
            $jumlah_peserta = $this->daftar_vaksin_model->get(
                array(
                    "fields" => "COUNT(id_daftar_vaksin) AS jumlah_peserta",
                    "where" => array(
                        "jenis_vaksin_id" => $jenis_vaksin,
                        "jadwal_vaksin_id" => $jadwal_vaksin,
                    ),
                    "where_false" => "no_antrian LIKE '" . $kode_undangan_prioritas . "%'"
                ),
                "row"
            );
        } else {
            $jumlah_peserta = $this->daftar_vaksin_model->get(
                array(
                    "fields" => "COUNT(id_daftar_vaksin) AS jumlah_peserta",
                    "where" => array(
                        "jenis_vaksin_id" => $jenis_vaksin,
                        "jadwal_vaksin_id" => $jadwal_vaksin,
                    )
                ),
                "row"
            );
        }


        $no_antrian = $jumlah_peserta->jumlah_peserta + 1;

        foreach ($exp_id_daftar_vaksin as $key => $val) {
            $data_master = $this->daftar_vaksin_model->get_by($val);

            $data_user_vaksin = $this->user_vaksin_model->get(
                array(
                    "where" => array(
                        "is_login" => "2",
                        "id_user_vaksin" => $data_master->id_user_created,
                    )
                ),
                "row"
            );

            $data_daftar_vaksin = [
                "hasil_skrining_petugas" => "1",
                "jenis_vaksin_id" => $jenis_vaksin,
                "jadwal_vaksin_id" => $jadwal_vaksin,
                "time_interval" => $waktu_interval,
                "satuan_interval" => $satuan_interval,
                "before_after_schedule_vaksin" => $batas_konfirmasi,
                "is_undangan_cadangan" => $is_checked_undangan_cadangan == "true" ? "1" : "2",
                "is_undangan_prioritas" => $is_undangan_prioritas == "true" ? "1" : "2",
                "is_done" => "1",
                "no_antrian" => $kode_undangan_prioritas . $no_antrian,
                "updated_at" => $this->datetime()
            ];

            $status_daftar = $this->daftar_vaksin_model->edit($val, $data_daftar_vaksin);

            $no_antrian++;

            // if ($status_daftar && $data_user_vaksin) {
            //     if ($is_checked_undangan_cadangan == "true") {
            //         $this->send_notification_mobile("Panggilan Undangan Tambahan Vaksinasi", "Hasil Skrining a/n " . $data_master->nama . " Lanjut Vaksin. Segera Konfirmasi Untuk Mendapatkan Nomor Antrian", $data_user_vaksin->token, $val);
            //     } else {
            //         $this->send_notification_mobile("Panggilan Undangan Vaksinasi", "Hasil Skrining a/n " . $data_master->nama . " Lanjut Vaksin. Segera Konfirmasi Untuk Mendapatkan Nomor Antrian", $data_user_vaksin->token, $val);
            //     }
            // }
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status_daftar));
    }

    public function ditunda()
    {
        $id_daftar_vaksin = $this->ipost("id_daftar_vaksin");

        foreach ($id_daftar_vaksin as $key => $val) {
            $data_master = $this->daftar_vaksin_model->get_by($val);

            $data_user_vaksin = $this->user_vaksin_model->get(
                array(
                    "where" => array(
                        "is_login" => "2",
                        "id_user_vaksin" => $data_master->id_user_created,
                    )
                ),
                "row"
            );

            $data_daftar_vaksin = [
                "hasil_skrining_petugas" => "2",
                "updated_at" => $this->datetime()
            ];

            $status_daftar = $this->daftar_vaksin_model->edit($val, $data_daftar_vaksin);

            if ($status_daftar) {
                $this->send_notification_mobile("Hasil Skrining Petugas", "Hasil Skrining a/n " . $data_master->nama . " Ditunda", $data_user_vaksin->token, $val);
            }
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status_daftar));
    }

    public function tidak_diberikan()
    {
        $id_daftar_vaksin = $this->ipost("id_daftar_vaksin");

        foreach ($id_daftar_vaksin as $key => $val) {
            $data_master = $this->daftar_vaksin_model->get_by($val);

            $data_user_vaksin = $this->user_vaksin_model->get(
                array(
                    "where" => array(
                        "is_login" => "2",
                        "id_user_vaksin" => $data_master->id_user_created,
                    )
                ),
                "row"
            );

            $data_daftar_vaksin = [
                "hasil_skrining_petugas" => "3",
                "updated_at" => $this->datetime()
            ];

            $status_daftar = $this->daftar_vaksin_model->edit($val, $data_daftar_vaksin);

            if ($status_daftar) {
                $this->send_notification_mobile("Hasil Skrining Petugas", "Hasil Skrining a/n " . $data_master->nama . " Tidak Diberikan", $data_user_vaksin->token, $val);
            }
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status_daftar));
    }

    public function ubah_status_selesai_vaksin()
    {
        $id_daftar_vaksin = decrypt_data($this->iget("id_daftar_vaksin"));
        $status = $this->iget("status");

        $data_daftar_vaksin = [
            "is_done" => $status == "false" ? "1" : "2",
            "updated_at" => $this->datetime()
        ];

        $status_detail = $this->daftar_vaksin_model->edit($id_daftar_vaksin, $data_daftar_vaksin);
    }

    public function kembalikan_data_tunda()
    {

        $peserta = $this->daftar_vaksin_model->get(
            array(
                "where_false" => "hasil_skrining_petugas = '2' AND kelurahan_master_wilayah_id IN (SELECT id_master_wilayah FROM master_wilayah
                WHERE puskesmas_id = '" . $this->session->userdata('puskesmas_id') . "')"
            )
        );

        $templist = array();
        foreach ($peserta as $key => $row) {
            $data_daftar_vaksin = [
                "hasil_skrining_petugas" => NULL,
                "updated_at" => $this->datetime()
            ];

            $status_detail = $this->daftar_vaksin_model->edit($row->id_daftar_vaksin, $data_daftar_vaksin);
        }
    }

    public function kembalikan_data_tidak_hadir()
    {

        $peserta = $this->daftar_vaksin_model->query("
        SELECT *
        FROM
        (
            SELECT
            id_daftar_vaksin,
            IF(hasil_skrining_petugas='1',
                UNIX_TIMESTAMP(DATE_FORMAT(
                    IF(before_after_schedule_vaksin = '1',
                        IF(satuan_interval = 'HOUR',
                            DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                            DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                        ),
                        IF(satuan_interval = 'HOUR',
                            DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                            DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                        )
                    ), '%Y-%m-%d %H:%i:%s')
                ),''
            ) AS timestamp_tanggal_jam_vaksin,
            is_confirm
            FROM daftar_vaksin
            JOIN jadwal_vaksin ON id_jadwal_vaksin=jadwal_vaksin_id
            WHERE hasil_skrining_petugas = '1' AND (is_confirm IS NULL OR is_confirm = '2') AND kelurahan_master_wilayah_id IN 
            (
                SELECT id_master_wilayah 
                FROM master_wilayah 
                WHERE puskesmas_id = '" . $this->session->userdata('puskesmas_id') . "'
            ) AND daftar_vaksin.deleted_at IS NULL
        ) AS tbl
        WHERE UNIX_TIMESTAMP() > timestamp_tanggal_jam_vaksin OR is_confirm = '2'
        ")->result();

        foreach ($peserta as $key => $row) {
            $data_daftar_vaksin = [
                "hasil_skrining_petugas" => NULL,
                "jenis_vaksin_id" => NULL,
                "jadwal_vaksin_id" => NULL,
                "is_done" => NULL,
                "is_confirm" => NULL,
                "no_antrian" => NULL,
                "time_interval" => NULL,
                "satuan_interval" => NULL,
                "before_after_schedule_vaksin" => NULL,
                "is_undangan_cadangan" => NULL,
                "updated_at" => $this->datetime()
            ];

            $status_detail = $this->daftar_vaksin_model->edit($row->id_daftar_vaksin, $data_daftar_vaksin);
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status_detail));
    }
}
