<?php

class Surat_rekomendasi_penolakan_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "surat_rekomendasi_penolakan";
        $this->primary_id = "id_surat_rekomendasi_penolakan";
    }
}
