<style>
    .ft-bld {
        font-weight: bold;
    }
</style>

<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Jenis Kegiatan</label>
                <div class="col-lg-10">
                    <select class="form-control select-search" name="jenis_kegiatan" onchange="get_surat_rekomendasi()">
                        <option value="">-- Semua Kegiatan --</option>
                        <option value="1">Kegiatan Sekda</option>
                        <option value="2">Kegiatan Masyarakat</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url() . 'surat_rekomendasi_kegiatan/tambah_rekomendasi_kegiatan_sekda'; ?>" class="btn btn-info">Tambah Rekomendasi Kegiatan Sekda</a>
                <a href="<?php echo base_url() . 'surat_rekomendasi_kegiatan/tambah_rekomendasi_kegiatan_masyarakat'; ?>" class="btn btn-success">Tambah Rekomendasi Kegiatan Masyarakat</a>
            </div>
        </div>
        <table id="datatableSuratRekomendasi" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nomor Surat</th>
                    <th>Jenis Kegiatan</th>
                    <th>Nama Kegiatan</th>
                    <th>Nama Pemohon Rekomendasi</th>
                    <th>Waktu Kegiatan</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    let datatableSuratRekomendasi = $("#datatableSuratRekomendasi").DataTable({
        "deferRender": true,
        "ordering": false,
        "columns": [
            null,
            null,
            null,
            null,
            {
                "width": "15%"
            },
            {
                "width": "15%"
            },
        ]
    });

    get_surat_rekomendasi();

    function get_surat_rekomendasi() {
        let jenis_kegiatan = $("select[name='jenis_kegiatan']").val();
        datatableSuratRekomendasi.clear().draw();
        $.ajax({
            url: base_url + 'surat_rekomendasi_kegiatan/request/get_surat_rekomendasi',
            data: {
                jenis_kegiatan: jenis_kegiatan
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    datatableSuratRekomendasi.row.add([
                        value.nomor_surat,
                        value.jenis_surat_label,
                        value.nama_kegiatan_custom,
                        value.nama_peminta_rekomendasi,
                        value.waktu_kegiatan_custom,
                        "<a href='" + base_url + "surat_rekomendasi_kegiatan/edit_surat_rekomendasi_kegiatan/" + value.id_encrypt + "/" + value.jenis_surat + "' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a> <a href='" + base_url + "surat_rekomendasi_kegiatan/cetak_surat_rekomendasi_kegiatan/" + value.id_encrypt + "/" + value.jenis_surat + "' class='btn btn-success btn-icon' target='_blank'><i class='icon-printer'></i></a>"
                    ]).draw(false);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function confirm_delete(id_surat) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'surat_rekomendasi_kegiatan/delete_surat',
                    data: {
                        id_surat: id_surat
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_surat_rekomendasi();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_surat_rekomendasi();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_surat_rekomendasi();
                    }
                });
            }
        });
    }
</script>