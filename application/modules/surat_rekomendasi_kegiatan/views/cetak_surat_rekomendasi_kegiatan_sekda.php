<html>

<head>
    <style>
        body {
            font-family: Georgia, 'Times New Roman', serif;
            font-size: 11pt;
            font-weight: normal;
        }

        .head_print {
            text-align: center;
            font-weight: 100;
            line-height: 0.6;
        }

        .wd-number {
            width: 10px;
        }

        .wd-label {
            width: 100px;
        }

        .wd-label-2 {
            width: 60px;

        }

        .wd-content {
            width: 200px;
        }

        .wd-content-2 {
            width: 310px;
        }

        .wd-titik {
            width: 2px;
        }

        table {
            margin: 10px 0;
        }

        .tbl-sign {
            text-align: center;
        }

        .ruler {
            border-bottom: 2px double black;
        }

        .logo {
            float: left;
            width: 20%;
        }

        .deskripsi {
            float: left;
            width: 60%;
        }

        .alamat {
            line-height: 0.5;
        }

        .text-center {
            text-align: center;
        }
    </style>
</head>

<body>
    <table style="width:100%;margin-top:20px;border-collapse:collapse;">
        <tr>
            <td rowspan="4" width="100"><img src="./assets/template_admin/kobar_cetak.png" width="80" height="90" /> </td>
            <td class="text-center" style="font-weight:bold;height:10px;">PEMERINTAH KABUPATEN KOTAWARINGIN BARAT</td>
            <td rowspan="4" width="30"></td>
        </tr>
        <tr>
            <td class="text-center" style="font-weight:bold;">SEKRETARIAT DAERAH</td>
        </tr>
        <tr>
            <td class="text-center">Jalan. Sutan Syahrir No. 2 Telp. 21126</td>
        </tr>
        <tr>
            <td class="text-center" style="font-weight:bold;">PANGKALAN BUN 74112</td>
        </tr>
        <tr>
            <td colspan="3" height="10" style="border-bottom:1px solid #000000;">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" style="border-top:4px solid #000000;">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" class="text-center" style="letter-spacing: 4px;font-weight:bold;"><span style="border-bottom:1px solid #000000;">REKOMENDASI</span></td>
        </tr>
        <tr>
            <td colspan="3" class="text-center">Nomor: 440/<?php echo isset($content) ? $content->nomor_surat : ""; ?>/PEM</td>
        </tr>
    </table>
    <table width="100%" style="border-collapse:collapse;">
        <tr>
            <td colspan="3">Yang bertanda tangan di bawah ini :</td>
        </tr>
        <tr>
            <td style="width:14%">Nama</td>
            <td style="width:1%">:</td>
            <td style="width:84%;font-weight:bold;padding-left:10px;">SUYANTO, S.H., M.H.</td>
        </tr>
        <tr>
            <td style="width:14%">Jabatan</td>
            <td style="width:1%">:</td>
            <td style="width:84%;padding-left:10px;">Sekretaris Daerah Kabupaten Kotawaringin Barat</td>
        </tr>
        <tr>
            <td colspan="3">Dengan ini memberikan rekomendasi kepada :</td>
        </tr>
        <tr>
            <td style="width:14%">Nama</td>
            <td style="width:1%">:</td>
            <td style="width:84%;font-weight:bold;padding-left:10px;"><?php echo isset($content) ? $content->nama_peminta_rekomendasi : ""; ?></td>
        </tr>
        <tr>
            <td style="width:14%">Jabatan</td>
            <td style="width:1%">:</td>
            <td style="width:84%;padding-left:10px;"><?php echo isset($content) ? $content->jabatan_peminta_rekomendasi : ""; ?></td>
        </tr>
        <tr>
            <td colspan="3" style="line-height:1.5;text-align:justify;">Berdasarkan Data Zonasi Penyebaran <span style="font-style:italic;">Corona Virus Disease 2019</span> (COVID-19) di Kabupaten Kotawaringin Barat Tanggal <?php echo isset($content) ? date_indo(date("Y-m-d", strtotime($content->tanggal_data_penyebaran))) : ""; ?>, Kelurahan/Desa <?php echo isset($content) ? $content->nama_wilayah_zonasi : ""; ?> masuk Zona <?php echo isset($content) ? $content->warna_zona : ""; ?> Oleh karena itu Rekomendasi dapat diberikan untuk pelaksanaan <?php echo ($content->jenis_surat == "1" ? "Kegiatan " . $content->nama_kegiatan : ($content->jenis_surat == "2" ? ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "1" ? "Resepsi Pernikahan " . $content->nama_mempelai_pria . " dan " . $content->nama_mempelai_wanita : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "2" ? "Tasyakuran Aqiqah dan Tasmiyah " . $content->nama_yang_menggelar_acara : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "3" ? "Khitanan " . $content->nama_yang_menggelar_acara : ""))) : "")); ?> yang akan dilaksanakan pada:</td>
        </tr>
    </table>
    <table width="100%" style="border-collapse:collapse;">
        <tr>
            <td style="width:14%">Hari</td>
            <td style="width:1%">:</td>
            <td style="width:88%;padding-left:10px;"><?php echo isset($content) ? $content->day_kegiatan : ""; ?></td>
        </tr>
        <tr>
            <td style="width:14%">Tanggal</td>
            <td style="width:1%">:</td>
            <td style="width:88%;padding-left:10px;"><?php echo isset($content) ? $content->date_kegiatan : ""; ?></td>
        </tr>
        <tr>
            <td style="width:14%">Pukul</td>
            <td style="width:1%">:</td>
            <td style="width:88%;padding-left:10px;"> <?php echo isset($content) ? $content->time_kegiatan : ""; ?> WIB s/d Selesai</td>
        </tr>
        <tr>
            <td style="width:14%">Tempat</td>
            <td style="width:1%">:</td>
            <td style="width:88%;padding-left:10px;"><?php echo isset($content) ? $content->alamat : ""; ?></td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
            <td style="padding-left:10px;">Kelurahan <?php echo isset($content) ? $content->nama_wilayah_zonasi : ""; ?> Kecamatan <?php echo isset($content) ? $content->nama_kecamatan_zonasi : ""; ?></td>
        </tr>
    </table>
    <table width="100%" style="border-collapse:collapse;">
        <tr>
            <td colspan="3">Dengan memperhatikan hal-hal sebagai berikut:</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">1.</td>
            <td style="width:96%">Pelaksanaan kegiatan mengacu pada ketentuan pencegahan dan pengendalian <span style="font-style:italic;">Covid-19</span>;</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">2.</td>
            <td style="width:96%">Menerapkan <span style="font-style:italic;">Physical distancing</span> dalam pelaksanaan kegiatan;</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">3.</td>
            <td style="width:96%">Menyediakan fasilitas cuci tangan pakai sabun dan <span style="font-style:italic;">hand sanitizer</span> di lokasi kegiatan;</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">4.</td>
            <td style="width:96%">Mengukur suhu terhadap pelaksana dan peserta kegiatan;</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">5.</td>
            <td style="width:96%">Melakukan pembersihan dan disinfeksi ruangan dan fasilitas lainnya sebelum dan sesudah pelaksanaan kegiatan;</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">6.</td>
            <td style="width:96%">Menyediakan Konsumsi dalam kemasan kotak;</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">7.</td>
            <td style="width:96%">Membatasi jumlah peserta 25% dari kapasitas normal tempat kegiatan;</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">8.</td>
            <td style="width:96%">Panitia dan peserta kegiatan wajib menunjukkan surat keterangan hasil negatif <span style="font-style:italic;">Rapid Test Antigen</span>, yang sampelnya diambil dalam kurun waktu maksimal 1x24 jam sebelum pelaksanaan;</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">9.</td>
            <td style="width:96%">Pelaksanaan Kegiatan paling lambat sampai dengan pukul 20.00 WIB;</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">10.</td>
            <td style="width:96%">Pelaksana kegiatan bersedia menghentikan kegiatan dan menerima sanksi sesuai ketentuan yang berlaku apabila terjadi pelanggaran protokol kesehatan.</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">11.</td>
            <td style="width:96%">Apabila H-1 kegiatan, terjadi peningkatan terkonfirmasi positif <span style="font-style:italic;">Corona Virus Disease 2019</span> (COVID-19) pada Kelurahan/Desa tersebut. Maka pelaksana wajib berkoordinasi dengan Satgas Penanganan Covid-19 tingkat Kelurahan/Desa untuk dilakukan pengawasan ketat pelaksanaan kegiatan.</td>
        </tr>
        <tr>
            <td colspan="3">Demikian Rekomendasi ini diberikan untuk dapat dipergunakan sebagaimana mestinya.</td>
        </tr>
    </table>
    <table class="tbl-sign" width="100%" style="border-collapse:collapse;">
        <tr>
            <td>&nbsp;</td>
            <td style="width:30%"></td>
            <td>Pangkalan Bun, <?php echo date_indo(date("Y-m-d")); ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td></td>
            <td style="font-weight:bold;">SEKRETARIS DAERAH</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td></td>
            <td style="font-weight:bold;">KABUPATEN KOTAWARINGIN BARAT</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="padding-top:60px;">&nbsp;</td>
            <td></td>
            <td style="padding-top:60px;">SUYANTO, S.H., M.H.</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>Pembina Utama Madya (IV/d)</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>NIP. 19640418 199203 1 009</td>
        </tr>
    </table>
    <table width="100%" style="font-size:8pt;border-collapse:collapse;">
        <tr>
            <td colspan="3">Tembusan disampaikan kepada Yth:</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">1.</td>
            <td style="width:96%">Bupati Kotawaringin Barat sebagai Laporan.</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">2.</td>
            <td style="width:96%">Satgas Penanganan COVID-19 Bidang Penegakan Hukum dan Pendisiplinan</td>
        </tr>
    </table>

    <pagebreak />

    <table style="width:100%;margin-top:20px;border-collapse:collapse;">
        <tr>
            <td rowspan="4" width="100"><img src="./assets/template_admin/kobar_cetak.png" width="80" height="90" /> </td>
            <td class="text-center" style="font-weight:bold;height:10px;">PEMERINTAH KABUPATEN KOTAWARINGIN BARAT</td>
            <td rowspan="4" width="30"></td>
        </tr>
        <tr>
            <td class="text-center" style="font-weight:bold;">SEKRETARIAT DAERAH</td>
        </tr>
        <tr>
            <td class="text-center">Jalan. Sutan Syahrir No. 2 Telp. 21126</td>
        </tr>
        <tr>
            <td class="text-center" style="font-weight:bold;">PANGKALAN BUN 74112</td>
        </tr>
        <tr>
            <td colspan="3" height="10" style="border-bottom:1px solid #000000;">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" style="border-top:4px solid #000000;">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" class="text-center" style="letter-spacing: 4px;font-weight:bold;"><span style="border-bottom:1px solid #000000;">REKOMENDASI</span></td>
        </tr>
        <tr>
            <td colspan="3" class="text-center">Nomor: 440/<?php echo isset($content) ? $content->nomor_surat : ""; ?>/PEM</td>
        </tr>
    </table>
    <table width="100%" style="border-collapse: collapse;">
        <tr>
            <td colspan="3">Yang bertanda tangan di bawah ini :</td>
        </tr>
        <tr>
            <td style="width:14%">Nama</td>
            <td style="width:1%">:</td>
            <td style="width:84%;font-weight:bold;padding-left:10px;">Drs. TENGKU ALI SYAHBANA, M.Si</td>
        </tr>
        <tr>
            <td style="width:14%">Jabatan</td>
            <td style="width:1%">:</td>
            <td style="width:84%;padding-left:10px;">Asisten Pemerintahan dan Kesra Sekretariat Daerah Kabupaten Kotawaringin Barat</td>
        </tr>
        <tr>
            <td colspan="3">Dengan ini memberikan rekomendasi kepada :</td>
        </tr>
        <tr>
            <td style="width:14%">Nama</td>
            <td style="width:1%">:</td>
            <td style="width:84%;font-weight:bold;padding-left:10px;"><?php echo isset($content) ? $content->nama_peminta_rekomendasi : ""; ?></td>
        </tr>
        <tr>
            <td style="width:14%">Jabatan</td>
            <td style="width:1%">:</td>
            <td style="width:84%;padding-left:10px;"><?php echo isset($content) ? $content->jabatan_peminta_rekomendasi : ""; ?></td>
        </tr>
        <tr>
            <td colspan="3" style="line-height:1.5;text-align:justify;">Berdasarkan Data Zonasi Penyebaran <span style="font-style:italic;">Corona Virus Disease 2019</span> (COVID-19) di Kabupaten Kotawaringin Barat Tanggal <?php echo isset($content) ? date_indo(date("Y-m-d", strtotime($content->tanggal_data_penyebaran))) : ""; ?>, Kelurahan/Desa <?php echo isset($content) ? $content->nama_wilayah_zonasi : ""; ?> masuk Zona <?php echo isset($content) ? $content->warna_zona : ""; ?> Oleh karena itu Rekomendasi dapat diberikan untuk pelaksanaan Kegiatan <?php echo isset($content) ? $content->nama_kegiatan : ""; ?> yang akan dilaksanakan pada:</td>
        </tr>
    </table>
    <table width="100%" style="border-collapse: collapse;">
        <tr>
            <td style="width:14%">Hari</td>
            <td style="width:1%">:</td>
            <td style="width:88%;padding-left:10px;"><?php echo isset($content) ? $content->day_kegiatan : ""; ?></td>
        </tr>
        <tr>
            <td style="width:14%">Tanggal</td>
            <td style="width:1%">:</td>
            <td style="width:88%;padding-left:10px;"><?php echo isset($content) ? $content->date_kegiatan : ""; ?></td>
        </tr>
        <tr>
            <td style="width:14%">Pukul</td>
            <td style="width:1%">:</td>
            <td style="width:88%;padding-left:10px;"> <?php echo isset($content) ? $content->time_kegiatan : ""; ?> WIB s/d Selesai</td>
        </tr>
        <tr>
            <td style="width:14%">Tempat</td>
            <td style="width:1%">:</td>
            <td style="width:88%;padding-left:10px;"><?php echo isset($content) ? $content->alamat : ""; ?></td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
            <td style="padding-left:10px;">Kelurahan <?php echo isset($content) ? $content->nama_wilayah_zonasi : ""; ?> Kecamatan <?php echo isset($content) ? $content->nama_kecamatan_zonasi : ""; ?></td>
        </tr>
    </table>
    <table width="100%" style="border-collapse: collapse;">
        <tr>
            <td colspan="3">Dengan memperhatikan hal-hal sebagai berikut:</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">1.</td>
            <td style="width:96%">Pelaksanaan kegiatan mengacu pada ketentuan pencegahan dan pengendalian <span style="font-style:italic;">Covid-19</span>;</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">2.</td>
            <td style="width:96%">Menerapkan <span style="font-style:italic;">Physical distancing</span> dalam pelaksanaan kegiatan;</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">3.</td>
            <td style="width:96%">Menyediakan fasilitas cuci tangan pakai sabun dan <span style="font-style:italic;">hand sanitizer</span> di lokasi kegiatan;</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">4.</td>
            <td style="width:96%">Mengukur suhu terhadap pelaksana dan peserta kegiatan;</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">5.</td>
            <td style="width:96%">Melakukan pembersihan dan disinfeksi ruangan dan fasilitas lainnya sebelum dan sesudah pelaksanaan kegiatan;</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">6.</td>
            <td style="width:96%">Menyediakan Konsumsi dalam kemasan kotak;</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">7.</td>
            <td style="width:96%">Membatasi jumlah peserta 25% dari kapasitas normal tempat kegiatan;</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">8.</td>
            <td style="width:96%">Panitia dan peserta kegiatan wajib menunjukkan surat keterangan hasil negatif <span style="font-style:italic;">Rapid Test Antigen</span>, yang sampelnya diambil dalam kurun waktu maksimal 1x24 jam sebelum pelaksanaan;</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">9.</td>
            <td style="width:96%">Pelaksanaan Kegiatan paling lambat sampai dengan pukul 20.00 WIB;</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">10.</td>
            <td style="width:96%">Pelaksana kegiatan bersedia menghentikan kegiatan dan menerima sanksi sesuai ketentuan yang berlaku apabila terjadi pelanggaran protokol kesehatan.</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">11.</td>
            <td style="width:96%">Apabila H-1 kegiatan, terjadi peningkatan terkonfirmasi positif <span style="font-style:italic;">Corona Virus Disease 2019</span> (COVID-19) pada Kelurahan/Desa tersebut. Maka pelaksana wajib berkoordinasi dengan Satgas Penanganan Covid-19 tingkat Kelurahan/Desa untuk dilakukan pengawasan ketat pelaksanaan kegiatan.</td>
        </tr>
        <tr>
            <td colspan="3">Demikian Rekomendasi ini diberikan untuk dapat dipergunakan sebagaimana mestinya.</td>
        </tr>
    </table>
    <table class="tbl-sign" width="100%" style="border-collapse: collapse;">
        <tr>
            <td>&nbsp;</td>
            <td style="width:30%"></td>
            <td>Pangkalan Bun, <?php echo date_indo(date("Y-m-d")); ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td></td>
            <td style="font-weight:bold;">An. SEKRETARIS DAERAH</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td></td>
            <td style="font-weight:bold;">ASISTEN PEMERINTAHAN DAN KESRA</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td></td>
            <td style="font-weight:bold;">KABUPATEN KOTAWARINGIN BARAT</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="padding-top:60px;">&nbsp;</td>
            <td></td>
            <td style="padding-top:60px;">Drs. TENGKU ALI SYAHBANA, M.Si</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>Pembina Utama Muda (IV/c)</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>NIP. 19680703 198911 1 001</td>
        </tr>
    </table>
    <table width="100%" style="font-size:8pt;border-collapse:collapse;">
        <tr>
            <td colspan="3">Tembusan disampaikan kepada Yth:</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">1.</td>
            <td style="width:96%">Bupati Kotawaringin Barat sebagai Laporan.</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">2.</td>
            <td style="width:96%">Satgas Penanganan COVID-19 Bidang Penegakan Hukum dan Pendisiplinan</td>
        </tr>
    </table>

</body>

</html>