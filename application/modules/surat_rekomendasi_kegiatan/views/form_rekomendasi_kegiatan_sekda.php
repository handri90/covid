<div class="content">
    <!-- Form inputs -->
    <div class="card">
        <div class="card-body">
            <?php echo form_open(); ?>
            <fieldset class="mb-3">
                <legend class="text-uppercase font-size-sm font-weight-bold">Surat Rekomendasi Kegiatan Sekda</legend>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nomor Surat <span class="text-danger">*</span></label>
                    <div class="col-lg-4">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text">404/</span>
                            </span>
                            <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->nomor_surat : ""; ?>" name="nomor_surat" required placeholder="Nomor Surat">
                            <span class="input-group-append">
                                <span class="input-group-text">/PEM</span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nama Yang Meminta Rekomendasi <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->nama_peminta_rekomendasi : ""; ?>" name="nama_peminta_rekomendasi" required placeholder="Nama Yang Meminta Rekomendasi">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Jabatan Yang Meminta Rekomendasi <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->jabatan_peminta_rekomendasi : ""; ?>" name="jabatan_peminta_rekomendasi" required placeholder="Jabatan Yang Meminta Rekomendasi">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Tanggal Zonasi Penyebaran <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control daterange-single" readonly value="<?php echo !empty($content) ? $content->tanggal_data_penyebaran : ""; ?>" name="tanggal_data_penyebaran" required placeholder="Tanggal Lahir">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Kelurahan/Desa <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <select class="form-control select-search" name="kelurahan_desa" required onchange="get_data_zonasi()">
                            <option value="">-- Pilih Kelurahan/Desa --</option>
                            <?php
                            foreach ($master_wilayah as $key => $row) {
                                $selected = "";
                                if (!empty($content)) {
                                    if ($row->id_master_wilayah == $content->master_wilayah_id) {
                                        $selected = 'selected="selected"';
                                    }
                                }
                            ?>
                                <option <?php echo $selected; ?> value="<?php echo encrypt_data($row->id_master_wilayah); ?>"><?php echo ucwords(strtolower($row->nama_wilayah)); ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Zona <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <span class="warna_zona"><?php echo !empty($content) ? $content->warna_zona : ""; ?></span>
                        <input type="hidden" name="zona" value="<?php echo !empty($content) ? $content->warna_zona : ""; ?>" />
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nama Kegiatan <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->nama_kegiatan : ""; ?>" name="nama_kegiatan" required placeholder="Nama Kegiatan">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Waktu Kegiatan <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" name="waktu_kegiatan" value="<?php echo !empty($content) ? $content->waktu_kegiatan : ""; ?>" id="anytime-both" readonly placeholder="Tanggal/Jam">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Alamat Lokasi Kegiatan <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->alamat : ""; ?>" name="alamat" required placeholder="Alamat Lokasi Kegiatan">
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <!-- /form inputs -->
</div>

<script>
    $('.daterange-single').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD-MM-YYYY'
        },
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10)
    });

    $('#anytime-both').AnyTime_picker({
        format: '%d-%m-%Y %H:%i:%s',
    });

    function get_data_zonasi() {
        kel_desa = $("select[name='kelurahan_desa'").val();

        if (kel_desa) {
            $.ajax({
                url: base_url + 'surat_rekomendasi_kegiatan/request/get_data_zonasi',
                data: {
                    kel_desa: kel_desa
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $(".warna_zona").html(response);
                    $("input[name='zona']").val(response);
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        } else {
            $(".warna_zona").html("");
            $("input[name='zona']").val("");
        }
    }
</script>