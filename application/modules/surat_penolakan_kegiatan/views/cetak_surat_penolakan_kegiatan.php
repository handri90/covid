<html>

<head>
    <style>
        body {
            font-family: Georgia, 'Times New Roman', serif;
            font-size: 11pt;
            font-weight: normal;
        }

        .head_print {
            text-align: center;
            font-weight: 100;
            line-height: 0.6;
        }

        .wd-number {
            width: 10px;
        }

        .wd-label {
            width: 100px;
        }

        .wd-label-2 {
            width: 60px;

        }

        .wd-content {
            width: 200px;
        }

        .wd-content-2 {
            width: 310px;
        }

        .wd-titik {
            width: 2px;
        }

        table {
            margin: 10px 0;
        }

        .tbl-sign {
            margin-top: 20px;
            text-align: center;
        }

        .ruler {
            border-bottom: 2px double black;
        }

        .logo {
            float: left;
            width: 20%;
        }

        .deskripsi {
            float: left;
            width: 60%;
        }

        .alamat {
            line-height: 0.5;
        }

        .text-center {
            text-align: center;
        }
    </style>
</head>

<body>
    <table style="width:100%;border-collapse:collapse;">
        <tr>
            <td rowspan="4" width="100"><img src="./assets/template_admin/kobar_cetak.png" width="80" height="100" /> </td>
            <td class="text-center" style="font-weight:bold;">PEMERINTAH KABUPATEN KOTAWARINGIN BARAT</td>
            <td rowspan="4" width="30"></td>
        </tr>
        <tr>
            <td class="text-center" style="font-weight:bold;">SEKRETARIAT DAERAH</td>
        </tr>
        <tr>
            <td class="text-center">Jalan. Sutan Syahrir No. 2 Telp. 21126</td>
        </tr>
        <tr>
            <td class="text-center" style="font-weight:bold;">PANGKALAN BUN 74112</td>
        </tr>
        <tr>
            <td colspan="3" height="10" style="border-bottom:1px solid #000000;">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" style="border-top:4px solid #000000;">&nbsp;</td>
        </tr>
    </table>
    <table style="width:100%;border-collapse:collapse;">
        <tr>
            <td style="width:60%;">&nbsp;</td>
            <td>Pangkalan Bun, <?php echo date_indo(date("Y-m-d")); ?></td>
        </tr>
    </table>
    <table style="width:100%;border-collapse:collapse;">
        <tr>
            <td style="width:60%;">
                <table>
                    <tr>
                        <td style="vertical-align:top;">Nomor</td>
                        <td style="vertical-align:top;">:</td>
                        <td>440/<?php echo isset($content) ? $content->nomor_surat : ""; ?>/Pem.2021</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">Lampiran</td>
                        <td style="vertical-align:top;">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">Perihal</td>
                        <td style="vertical-align:top;">:</td>
                        <td>Penyelenggaraan <?php echo ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "1" ? "Resepsi Pernikahan" : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "2" ? "Tasyakuran Aqiqah dan Tasmiyah" : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "3" ? "Khitanan" : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "4" ? "Kegiatan" : "")))); ?></td>
                    </tr>
                </table>
            </td>
            <td>
                <table>
                    <tr>
                        <td>&nbsp;</td>
                        <td>Kepada</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">Yth.</td>
                        <td style="font-weight:bold;">Sdr. <?php echo isset($content) ? $content->nama_peminta_rekomendasi : ""; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>Di</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td style="padding-left:20px;font-weight:bold">Tempat</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td rowspan="2" style="width:10%;">&nbsp;</td>
            <td style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sehubungan dengan Surat Permohonan Saudara tentang Pelaksanaan <?php echo ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "1" ? "Resepsi Pernikahan" : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "2" ? "Tasyakuran Aqiqah dan Tasmiyah" : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "3" ? "Khitanan" : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "4" ? "Kegiatan" : "")))); ?> dapat kami sampaikan hal-hal sebagai berikut :</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td style="vertical-align:top;">1.</td>
                        <td style="text-align:justify;">Berdasarkan Intruksi Menteri Dalam Negeri Nomor 17 Tahun 2021 tentang Perpanjangan Pemberlakuan Pembatasan Kegiatan Masyarakat Berbasis Mikro dan Mengoptimalkan Posko Penanganan <span style="font-style:italic;">Corona Virus Disease 2019</span> (COVID-19) di tingkat Desa dan Kelurahan untuk Pengendalian <span style="font-style:italic;">Corona Virus Disease 2019</span> (COVID-19) dan Surat Edaran Bupati Kotawaringin Barat Nomor 440/09/ PEM.2021 tentang penyelenggaraan Kegiatan Pada Masa Pandemi COVID-19 di Kabupaten Kotawaringin Barat, bahwa pelaksanaan penyelenggaraan pernikahan/khitanan/tasmiyahan/kegiatan lainnya hanya dapat dilaksanakan pada zona hijau dan kuning.</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">2.</td>
                        <td style="text-align:justify;">Mengacu pada Data Update Penetapan Zonasi Penyebaran Covid-19 di wilayah Kabupaten Kotawaringin Barat bahwa di Kelurahan/Desa <?php echo isset($content) ? $content->nama_wilayah : ""; ?> Kecamatan <?php echo isset($content) ? ucwords(strtolower($content->nama_kecamatan)) : ""; ?> termasuk dalam Zona <?php echo isset($content) ? $content->warna_zona : ""; ?> Level <?php echo isset($content) ? $content->tipe_zona : ""; ?> kasus aktif Penyebaran <span style="font-style:italic;">Corona Virus Disease 2019</span> (COVID-19) di Kabupaten Kotawaringin Barat mengalami peningkatan.</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width:10%;">&nbsp;</td>
            <td style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Berdasarkan angka 1 dan 2 tersebut diatas dan mempertimbangkan terjadinya kerumunan warga maka Pelaksanaan <?php echo ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "1" ? "Resepsi Pernikahan " . $content->nama_mempelai_pria . " dan " . $content->nama_mempelai_wanita : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "2" ? "Tasyakuran Aqiqah dan Tasmiyah " . $content->nama_yang_menggelar_acara : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "3" ? "Khitanan " . $content->nama_yang_menggelar_acara : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "4" ? $content->nama_kegiatan : "")))); ?> di Kelurahan/Desa <?php echo isset($content) ? $content->nama_wilayah : ""; ?> Kecamatan <?php echo isset($content) ? ucwords(strtolower($content->nama_kecamatan)) : ""; ?> <span style="font-weight:bold">tidak dapat diberikan rekomendasi untuk pelaksanaannya</span>.</td>
        </tr>
        <tr>
            <td style="width:10%;">&nbsp;</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demikian disampaikan atas perhatian dan kerjasamanya diucapkan terima kasih.</td>
        </tr>
    </table>
    <table class="tbl-sign" width="100%" style="border-collapse:collapse">
        <tr>
            <td>&nbsp;</td>
            <td style="width:30%"></td>
            <td style="font-weight:bold;">SEKRETARIS DAERAH</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td></td>
            <td style="font-weight:bold;">KABUPATEN KOTAWARINGIN BARAT</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="padding-top:60px;">&nbsp;</td>
            <td></td>
            <td style="padding-top:60px;">SUYANTO, S.H., M.H.</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>Pembina Utama Madya (IV/d)</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>NIP. 19640418 199203 1 009</td>
        </tr>
    </table>
    <table width="100%" style="font-size:8pt;border-collapse:collapse">
        <tr>
            <td colspan="3">Tembusan disampaikan kepada Yth:</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">1.</td>
            <td style="width:96%">Bupati Kotawaringin Barat sebagai Laporan.</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">2.</td>
            <td style="width:96%">Satgas Penanganan COVID-19 Bidang Penegakan Hukum dan Pendisiplinan</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">3.</td>
            <td style="width:96%">Camat <?php echo isset($content) ? ucwords(strtolower($content->nama_kecamatan)) : ""; ?></td>
        </tr>
    </table>

    <pagebreak />

    <table style="width:100%;border-collapse:collapse;">
        <tr>
            <td rowspan="4" width="100"><img src="./assets/template_admin/kobar_cetak.png" width="80" height="100" /> </td>
            <td class="text-center" style="font-weight:bold;">PEMERINTAH KABUPATEN KOTAWARINGIN BARAT</td>
            <td rowspan="4" width="30"></td>
        </tr>
        <tr>
            <td class="text-center" style="font-weight:bold;">SEKRETARIAT DAERAH</td>
        </tr>
        <tr>
            <td class="text-center">Jalan. Sutan Syahrir No. 2 Telp. 21126</td>
        </tr>
        <tr>
            <td class="text-center" style="font-weight:bold;">PANGKALAN BUN 74112</td>
        </tr>
        <tr>
            <td colspan="3" height="10" style="border-bottom:1px solid #000000;">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" style="border-top:4px solid #000000;">&nbsp;</td>
        </tr>
    </table>
    <table style="width:100%;border-collapse:collapse;">
        <tr>
            <td style="width:60%;">&nbsp;</td>
            <td>Pangkalan Bun, <?php echo date_indo(date("Y-m-d")); ?></td>
        </tr>
    </table>
    <table style="width:100%;border-collapse:collapse;">
        <tr>
            <td style="width:60%;">
                <table>
                    <tr>
                        <td style="vertical-align:top;">Nomor</td>
                        <td style="vertical-align:top;">:</td>
                        <td>440/<?php echo isset($content) ? $content->nomor_surat : ""; ?>/Pem.2021</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">Lampiran</td>
                        <td style="vertical-align:top;">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">Perihal</td>
                        <td style="vertical-align:top;">:</td>
                        <td>Penyelenggaraan <?php echo ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "1" ? "Resepsi Pernikahan" : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "2" ? "Tasyakuran Aqiqah dan Tasmiyah" : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "3" ? "Khitanan" : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "4" ? "Kegiatan" : "")))); ?></td>
                    </tr>
                </table>
            </td>
            <td>
                <table>
                    <tr>
                        <td>&nbsp;</td>
                        <td>Kepada</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">Yth.</td>
                        <td>Sdr. <?php echo isset($content) ? $content->nama_peminta_rekomendasi : ""; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>Di</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td style="padding-left:20px;font-weight:bold">Tempat</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td rowspan="2" style="width:10%;">&nbsp;</td>
            <td style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sehubungan dengan Surat Permohonan Saudara tentang Pelaksanaan <?php echo ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "1" ? "Resepsi Pernikahan" : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "2" ? "Tasyakuran Aqiqah dan Tasmiyah" : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "3" ? "Khitanan" : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "4" ? "Kegiatan" : "")))); ?> dapat kami sampaikan hal-hal sebagai berikut :</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td style="vertical-align:top;">1.</td>
                        <td style="text-align:justify;">Berdasarkan Intruksi Menteri Dalam Negeri Nomor 17 Tahun 2021 tentang Perpanjangan Pemberlakuan Pembatasan Kegiatan Masyarakat Berbasis Mikro dan Mengoptimalkan Posko Penanganan <span style="font-style:italic;">Corona Virus Disease 2019</span> (COVID-19) di tingkat Desa dan Kelurahan untuk Pengendalian <span style="font-style:italic;">Corona Virus Disease 2019</span> (COVID-19) dan Surat Edaran Bupati Kotawaringin Barat Nomor 440/09/ PEM.2021 tentang penyelenggaraan Kegiatan Pada Masa Pandemi COVID-19 di Kabupaten Kotawaringin Barat, bahwa pelaksanaan penyelenggaraan pernikahan/khitanan/tasmiyahan/kegiatan lainnya hanya dapat dilaksanakan pada zona hijau dan kuning.</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">2.</td>
                        <td style="text-align:justify;">Mengacu pada Data Update Penetapan Zonasi Penyebaran Covid-19 di wilayah Kabupaten Kotawaringin Barat bahwa di Kelurahan/Desa <?php echo isset($content) ? $content->nama_wilayah : ""; ?> Kecamatan <?php echo isset($content) ? ucwords(strtolower($content->nama_kecamatan)) : ""; ?> termasuk dalam Zona <?php echo isset($content) ? $content->warna_zona : ""; ?> Level <?php echo isset($content) ? $content->tipe_zona : ""; ?> kasus aktif Penyebaran <span style="font-style:italic;">Corona Virus Disease 2019</span> (COVID-19) di Kabupaten Kotawaringin Barat mengalami peningkatan.</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width:10%;">&nbsp;</td>
            <td style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Berdasarkan angka 1 dan 2 tersebut diatas dan mempertimbangkan terjadinya kerumunan warga maka Pelaksanaan <?php echo ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "1" ? "Resepsi Pernikahan " . $content->nama_mempelai_pria . " dan " . $content->nama_mempelai_wanita : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "2" ? "Tasyakuran Aqiqah dan Tasmiyah " . $content->nama_yang_menggelar_acara : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "3" ? "Khitanan " . $content->nama_yang_menggelar_acara : ($content->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "4" ? $content->nama_kegiatan : "")))); ?> di Kelurahan/Desa <?php echo isset($content) ? $content->nama_wilayah : ""; ?> Kecamatan <?php echo isset($content) ? ucwords(strtolower($content->nama_kecamatan)) : ""; ?> <span style="font-weight:bold">tidak dapat diberikan rekomendasi untuk pelaksanaannya</span>.</td>
        </tr>
        <tr>
            <td style="width:10%;">&nbsp;</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demikian disampaikan atas perhatian dan kerjasamanya diucapkan terima kasih.</td>
        </tr>
    </table>
    <table class="tbl-sign" width="100%" style="border-collapse:collapse">
        <tr>
            <td>&nbsp;</td>
            <td style="width:30%"></td>
            <td style="font-weight:bold;">An. SEKRETARIS DAERAH</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="width:30%"></td>
            <td style="font-weight:bold;">ASISTEN PEMERINTAHAN DAN KESRA</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td></td>
            <td style="font-weight:bold;">KABUPATEN KOTAWARINGIN BARAT</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="padding-top:60px;">&nbsp;</td>
            <td></td>
            <td style="padding-top:60px;">Drs. TENGKU ALI SYAHBANA, M.Si</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>Pembina Utama Muda (IV/c)</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>NIP. 19680703 198911 1 001</td>
        </tr>
    </table>
    <table width="100%" style="font-size:8pt;border-collapse:collapse;">
        <tr>
            <td colspan="3">Tembusan disampaikan kepada Yth:</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">1.</td>
            <td style="width:96%">Bupati Kotawaringin Barat sebagai Laporan.</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">2.</td>
            <td style="width:96%">Satgas Penanganan COVID-19 Bidang Penegakan Hukum dan Pendisiplinan</td>
        </tr>
        <tr>
            <td style="width:4%;vertical-align:top;">3.</td>
            <td style="width:96%">Camat <?php echo isset($content) ? ucwords(strtolower($content->nama_kecamatan)) : ""; ?></td>
        </tr>
    </table>
</body>

</html>