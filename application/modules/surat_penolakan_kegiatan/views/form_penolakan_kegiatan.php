<div class="content">
    <!-- Form inputs -->
    <div class="card">
        <div class="card-body">
            <?php echo form_open(); ?>
            <fieldset class="mb-3">
                <legend class="text-uppercase font-size-sm font-weight-bold">Surat Penolakan Kegiatan</legend>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nomor Surat <span class="text-danger">*</span></label>
                    <div class="col-lg-4">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text">440/</span>
                            </span>
                            <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->nomor_surat : ""; ?>" name="nomor_surat" required placeholder="Nomor Surat">
                            <span class="input-group-append">
                                <span class="input-group-text">/Pem.2021</span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nama Yang Meminta Rekomendasi <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->nama_peminta_rekomendasi : ""; ?>" name="nama_peminta_rekomendasi" required placeholder="Nama Yang Meminta Rekomendasi">
                    </div>
                </div>

                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Data Kegiatan</legend>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Jenis Kegiatan <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <select class="form-control select-search" name="is_jenis_surat_rekomendasi_kegiatan_masyarakat" required onchange="show_form()">
                                <option value="">-- Pilih Kegiatan --</option>
                                <?php
                                foreach ($jenis_kegiatan_masyarakat as $key => $row) {
                                    $selected = "";
                                    if (!empty($content)) {
                                        if ($key == $content->is_jenis_surat_rekomendasi_kegiatan_masyarakat) {
                                            $selected = 'selected="selected"';
                                        }
                                    }
                                ?>
                                    <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $row; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row is_show_nama_kegiatan">
                        <label class="col-form-label col-lg-2">Nama Kegiatan <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->nama_kegiatan : ""; ?>" name="nama_kegiatan" placeholder="Nama Kegiatan">
                        </div>
                    </div>

                    <div class="form-group row is_show_nama_penggelar_acara">
                        <label class="col-form-label col-lg-2">Nama Anak <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->nama_yang_menggelar_acara : ""; ?>" name="nama_yang_menggelar_acara" placeholder="Nama Anak">
                        </div>
                    </div>

                    <div class="form-group row is_show_pernikahan">
                        <label class="col-form-label col-lg-2">Nama Mempelai Pria <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->nama_mempelai_pria : ""; ?>" name="nama_mempelai_pria" placeholder="Nama Mempelai Pria">
                        </div>
                    </div>

                    <div class="form-group row is_show_pernikahan">
                        <label class="col-form-label col-lg-2">Nama Mempelai Wanita <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->nama_mempelai_wanita : ""; ?>" name="nama_mempelai_wanita" placeholder="Nama Mempelai Wanita">
                        </div>
                    </div>
                </fieldset>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Tanggal Kegiatan <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control tanggal-kegiatan" readonly value="<?php echo !empty($content) ? $content->waktu_kegiatan : ""; ?>" name="waktu_kegiatan" required placeholder="Tanggal Kegiatan">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Kelurahan/Desa <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <select class="form-control select-search" name="kelurahan_desa" required onchange="get_data_zonasi()">
                            <option value="">-- Pilih Kelurahan/Desa --</option>
                            <?php
                            foreach ($master_wilayah as $key => $row) {
                                $selected = "";
                                if (!empty($content)) {
                                    if ($row->id_master_wilayah == $content->master_wilayah_id) {
                                        $selected = 'selected="selected"';
                                    }
                                }
                            ?>
                                <option <?php echo $selected; ?> value="<?php echo encrypt_data($row->id_master_wilayah); ?>"><?php echo ucwords(strtolower($row->nama_wilayah)); ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Zona <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <span class="warna_zona"><?php echo !empty($content) ? $content->warna_zona : ""; ?></span>
                        <input type="hidden" name="zona" value="<?php echo !empty($content) ? $content->warna_zona : ""; ?>" />
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <!-- /form inputs -->
</div>

<script>
    $(".is_show_nama_kegiatan").hide();
    $(".is_show_nama_penggelar_acara").hide();
    $(".is_show_pernikahan").hide();

    $('.tanggal-kegiatan').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD-MM-YYYY'
        },
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10)
    });

    function get_data_zonasi() {
        kel_desa = $("select[name='kelurahan_desa'").val();

        if (kel_desa) {
            $.ajax({
                url: base_url + 'surat_rekomendasi_kegiatan/request/get_data_zonasi',
                data: {
                    kel_desa: kel_desa
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $(".warna_zona").html(response);
                    $("input[name='zona']").val(response);
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        } else {
            $(".warna_zona").html("");
            $("input[name='zona']").val("");
        }
    }

    show_form();

    function show_form() {
        let jenis_kegiatan_val = $("input[name='is_jenis_kegiatan_hidden']").val();

        let jenis_kegiatan = $("select[name='is_jenis_surat_rekomendasi_kegiatan_masyarakat']").val();

        if (jenis_kegiatan) {
            jenis_kegiatan_val = jenis_kegiatan;
        }

        $(".is_show_nama_kegiatan").hide();
        $(".is_show_nama_penggelar_acara").hide();
        $(".is_show_pernikahan").hide();
        $("input[name='nama_kegiatan']").attr("required", false);
        $("input[name='nama_yang_menggelar_acara']").attr("required", false);
        $("input[name='nama_mempelai_pria']").attr("required", false);
        $("input[name='nama_mempelai_wanita']").attr("required", false);

        if (jenis_kegiatan_val == '1') {
            $(".is_show_nama_kegiatan").hide();
            $(".is_show_nama_penggelar_acara").hide();
            $(".is_show_pernikahan").show();
            $("input[name='nama_kegiatan']").attr("required", false);
            $("input[name='nama_yang_menggelar_acara']").attr("required", false);
            $("input[name='nama_mempelai_pria']").attr("required", true);
            $("input[name='nama_mempelai_wanita']").attr("required", true);
        } else if (jenis_kegiatan_val == '2' || jenis_kegiatan_val == '3') {
            $(".is_show_nama_kegiatan").hide();
            $(".is_show_nama_penggelar_acara").show();
            $(".is_show_pernikahan").hide();
            $("input[name='nama_kegiatan']").attr("required", false);
            $("input[name='nama_yang_menggelar_acara']").attr("required", true);
            $("input[name='nama_mempelai_pria']").attr("required", false);
            $("input[name='nama_mempelai_wanita']").attr("required", false);
        } else if (jenis_kegiatan_val == '4') {
            $(".is_show_nama_kegiatan").show();
            $(".is_show_nama_penggelar_acara").hide();
            $(".is_show_pernikahan").hide();
            $("input[name='nama_kegiatan']").attr("required", true);
            $("input[name='nama_yang_menggelar_acara']").attr("required", false);
            $("input[name='nama_mempelai_pria']").attr("required", false);
            $("input[name='nama_mempelai_wanita']").attr("required", false);
        }
    }
</script>