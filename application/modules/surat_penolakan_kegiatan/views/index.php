<style>
    .ft-bld {
        font-weight: bold;
    }
</style>

<div class="content">
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url() . 'surat_penolakan_kegiatan/tambah_penolakan_kegiatan'; ?>" class="btn btn-info">Tambah Penolakan Kegiatan</a>
            </div>
        </div>
        <table id="datatableSuratPenolakan" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nomor Surat</th>
                    <th>Jenis Kegiatan</th>
                    <th>Nama Yang Meminta Rekomendasi</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    let datatableSuratPenolakan = $("#datatableSuratPenolakan").DataTable({
        "deferRender": true,
        "ordering": false,
        "columns": [
            null,
            null,
            null,
            {
                "width": "15%"
            },
        ]
    });

    get_surat_penolakan();

    function get_surat_penolakan() {
        datatableSuratPenolakan.clear().draw();
        $.ajax({
            url: base_url + 'surat_penolakan_kegiatan/request/get_surat_penolakan',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    datatableSuratPenolakan.row.add([
                        value.nomor_surat,
                        value.jenis_surat_label,
                        value.nama_peminta_rekomendasi,
                        "<a href='" + base_url + "surat_penolakan_kegiatan/edit_surat_penolakan_kegiatan/" + value.id_encrypt + "' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a> <a href='" + base_url + "surat_penolakan_kegiatan/cetak_surat_penolakan_kegiatan/" + value.id_encrypt + "' class='btn btn-success btn-icon' target='_blank'><i class='icon-printer'></i></a>"
                    ]).draw(false);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function confirm_delete(id_surat) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'surat_penolakan_kegiatan/delete_surat',
                    data: {
                        id_surat: id_surat
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_surat_penolakan();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_surat_penolakan();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_surat_penolakan();
                    }
                });
            }
        });
    }
</script>