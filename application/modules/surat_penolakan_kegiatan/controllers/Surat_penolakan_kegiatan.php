<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Surat_penolakan_kegiatan extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model("surat_rekomendasi_kegiatan/surat_rekomendasi_penolakan_model", "surat_rekomendasi_penolakan_model");
        $this->load->model("peserta/master_wilayah_model", "master_wilayah_model");
    }

    public function index()
    {
        $data['breadcrumb'] = [['link' => false, 'content' => 'Surat Penolakan Kegiatan', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function tambah_penolakan_kegiatan()
    {
        if (empty($_POST)) {

            $data['jenis_kegiatan_masyarakat'] = array("1" => "Resepsi Pernikahan", "2" => "Tasyakuran Aqiqah dan Tasmiyah", "3" => "Khitanan", "4" => "Kegiatan");

            $data['master_wilayah'] = $this->master_wilayah_model->get(
                array(
                    "fields" => "master_wilayah.*,concat(klasifikasi,' - ',nama_wilayah) as nama_wilayah",
                    "where" => array(
                        "id_master_wilayah != " => "109"
                    ),
                    "where_false" => "klasifikasi NOT IN ('PROV','KAB','KEC')",
                    "order_by_false" => 'field (klasifikasi,"KEC","KEL","DESA"),nama_wilayah'
                )
            );

            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'surat_penolakan_kegiatan', 'content' => 'Surat Penolakan Kegiatan', 'is_active' => false], ['link' => false, 'content' => 'Tambah Penolakan Kegiatan', 'is_active' => true]];

            $this->execute('form_penolakan_kegiatan', $data);
        } else {

            $data_zonasi = "";
            if ($this->ipost('zona') == "Merah") {
                $data_zonasi = "4";
            } else if ($this->ipost('zona') == "Orange") {
                $data_zonasi = "3";
            } else if ($this->ipost('zona') == "Kuning") {
                $data_zonasi = "2";
            } else if ($this->ipost('zona') == "Hijau") {
                $data_zonasi = "1";
            }

            $data = array(
                "nomor_surat" => $this->ipost('nomor_surat'),
                "nama_peminta_rekomendasi" => $this->ipost('nama_peminta_rekomendasi'),
                "waktu_kegiatan" => date("Y-m-d", strtotime($this->ipost('waktu_kegiatan'))) . " " . date("H:i:s"),
                "nama_kegiatan" => $this->ipost('nama_kegiatan'),
                "nama_yang_menggelar_acara" => $this->ipost('nama_yang_menggelar_acara'),
                "is_jenis_surat_rekomendasi_kegiatan_masyarakat" => $this->ipost('is_jenis_surat_rekomendasi_kegiatan_masyarakat'),
                "nama_mempelai_pria" => $this->ipost('nama_mempelai_pria'),
                "nama_mempelai_wanita" => $this->ipost('nama_mempelai_wanita'),
                "master_wilayah_id" => decrypt_data($this->ipost('kelurahan_desa')),
                "tipe_zona" => $data_zonasi,
                "jenis_surat" => "3",
                "created_at" => $this->datetime()
            );

            $status = $this->surat_rekomendasi_penolakan_model->save($data);

            if ($status) {
                $this->session->set_flashdata('message', 'Data berhasil ditambahkan');
            } else {
                $this->session->set_flashdata('message', 'Data gagal ditambahkan');
            }

            redirect('surat_penolakan_kegiatan');
        }
    }

    public function edit_surat_penolakan_kegiatan($id_surat_rekomendasi)
    {
        $data_master = $this->surat_rekomendasi_penolakan_model->get(
            array(
                "fields" => "surat_rekomendasi_penolakan.*,DATE_FORMAT(waktu_kegiatan,'%d-%m-%Y') AS waktu_kegiatan,IF(tipe_zona = 1,'Hijau',IF(tipe_zona = 2,'Kuning',IF(tipe_zona = 3, 'Orange',IF(tipe_zona = 4, 'Merah','')))) AS warna_zona,DATE_FORMAT(waktu_kegiatan,'%d-%m-%Y %H:%i:%s') AS waktu_kegiatan",
                "where" => array(
                    "id_surat_rekomendasi_penolakan" => decrypt_data($id_surat_rekomendasi)
                )
            ),
            "row"
        );

        if (!$data_master) {
            $this->page_error();
        }

        if (empty($_POST)) {
            $data['jenis_kegiatan_masyarakat'] = array("1" => "Resepsi Pernikahan", "2" => "Tasyakuran Aqiqah dan Tasmiyah", "3" => "Khitanan", "4" => "Kegiatan");

            $data['content'] = $data_master;
            $data['master_wilayah'] = $this->master_wilayah_model->get(
                array(
                    "fields" => "master_wilayah.*,concat(klasifikasi,' - ',nama_wilayah) as nama_wilayah",
                    "where" => array(
                        "id_master_wilayah != " => "109"
                    ),
                    "where_false" => "klasifikasi NOT IN ('PROV','KAB','KEC')",
                    "order_by_false" => 'field (klasifikasi,"KEC","KEL","DESA"),nama_wilayah'
                )
            );

            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'surat_penolakan_kegiatan', 'content' => 'Surat Penolakan Kegiatan', 'is_active' => false], ['link' => false, 'content' => 'Ubah Penolakan Kegiatan', 'is_active' => true]];

            $this->execute('form_penolakan_kegiatan', $data);
        } else {
            $data_zonasi = "";
            if ($this->ipost('zona') == "Merah") {
                $data_zonasi = "4";
            } else if ($this->ipost('zona') == "Orange") {
                $data_zonasi = "3";
            } else if ($this->ipost('zona') == "Kuning") {
                $data_zonasi = "2";
            } else if ($this->ipost('zona') == "Hijau") {
                $data_zonasi = "1";
            }

            $data = array(
                "nomor_surat" => $this->ipost('nomor_surat'),
                "nama_peminta_rekomendasi" => $this->ipost('nama_peminta_rekomendasi'),
                "waktu_kegiatan" => date("Y-m-d", strtotime($this->ipost('waktu_kegiatan'))) . " " . date("H:i:s"),
                "nama_kegiatan" => $this->ipost('nama_kegiatan'),
                "nama_yang_menggelar_acara" => $this->ipost('nama_yang_menggelar_acara'),
                "is_jenis_surat_rekomendasi_kegiatan_masyarakat" => $this->ipost('is_jenis_surat_rekomendasi_kegiatan_masyarakat'),
                "nama_mempelai_pria" => $this->ipost('nama_mempelai_pria'),
                "nama_mempelai_wanita" => $this->ipost('nama_mempelai_wanita'),
                "master_wilayah_id" => decrypt_data($this->ipost('kelurahan_desa')),
                "tipe_zona" => $data_zonasi,
                "updated_at" => $this->datetime()
            );

            $status = $this->surat_rekomendasi_penolakan_model->edit(decrypt_data($id_surat_rekomendasi), $data);

            if ($status) {
                $this->session->set_flashdata('message', 'Data berhasil diubah');
            } else {
                $this->session->set_flashdata('message', 'Data gagal diubah');
            }


            redirect('surat_penolakan_kegiatan');
        }
    }

    public function cetak_surat_penolakan_kegiatan($id_surat_penolakan)
    {
        $data['content'] = $this->surat_rekomendasi_penolakan_model->get(
            array(
                "fields" => "surat_rekomendasi_penolakan.*,DATE_FORMAT(waktu_kegiatan,'%Y-%m-%d') AS waktu_kegiatan,IF(tipe_zona = 1,'Hijau',IF(tipe_zona = 2,'Kuning',IF(tipe_zona = 3, 'Orange',IF(tipe_zona = 4, 'Merah','')))) AS warna_zona,DATE_FORMAT(waktu_kegiatan,'%d-%m-%Y %H:%i:%s') AS waktu_kegiatan,nama_wilayah",
                "where" => array(
                    "id_surat_rekomendasi_penolakan" => decrypt_data($id_surat_penolakan)
                ),
                "join" => array(
                    "master_wilayah" => "master_wilayah_id=id_master_wilayah"
                )
            ),
            "row"
        );

        $expl_day_date = explode(",", longdate_indo(date("Y-m-d", strtotime($data['content']->waktu_kegiatan))));
        $data['content']->day_kegiatan = $expl_day_date[0];
        $data['content']->date_kegiatan = $expl_day_date[1];
        $data['content']->time_kegiatan = date("H:i", strtotime($data['content']->waktu_kegiatan));

        $kecamatan = $this->master_wilayah_model->query(
            "
            SELECT *
            FROM master_wilayah
            WHERE kode_wilayah = 
            (
                SELECT kode_induk
                FROM master_wilayah
                WHERE id_master_wilayah = '" . $data['content']->master_wilayah_id . "'
            )
            "
        )->row();

        $data['content']->nama_kecamatan = $kecamatan->nama_wilayah;

        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'tempDir' => '/tmp', 'format' => 'Legal', 'margin_top' => 5, 'margin_bottom' => 0]);
        $data = $this->load->view('cetak_surat_penolakan_kegiatan', $data, TRUE);
        $mpdf->WriteHTML($data);
        $mpdf->Output();
    }

    public function delete_surat()
    {
        $id_surat = $this->iget('id_surat');
        $data_master = $this->surat_rekomendasi_penolakan_model->get_by(decrypt_data($id_surat));

        if (!$data_master) {
            $this->page_error();
        }

        $status = $this->surat_rekomendasi_penolakan_model->remove(decrypt_data($id_surat));
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }
}
