<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("surat_rekomendasi_kegiatan/surat_rekomendasi_penolakan_model", "surat_rekomendasi_penolakan_model");
    }

    public function get_surat_penolakan()
    {

        $surat = $this->surat_rekomendasi_penolakan_model->get(
            array(
                "fields" => "surat_rekomendasi_penolakan.*,CONCAT('440/',nomor_surat,'/Pem.2021') AS nomor_surat",
                "where" => array(
                    "jenis_surat" => "3"
                )
            )
        );

        $templist = array();
        foreach ($surat as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_surat_rekomendasi_penolakan);

            if ($row->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "4") {
                $templist[$key]['jenis_surat_label'] = "Kegiatan Sekda";
            } else if (in_array($row->is_jenis_surat_rekomendasi_kegiatan_masyarakat, array("1", "2", "3"))) {
                $templist[$key]['jenis_surat_label'] = ($row->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "1" ? "Resepsi Pernikahan" : ($row->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "2" ? "Tasyakuran Aqiqah dan Tasmiyah" : ($row->is_jenis_surat_rekomendasi_kegiatan_masyarakat == "3" ? "Khitanan" : "")));
            }
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_data_zonasi()
    {
        $id_master_wilayah = decrypt_data($this->iget("kel_desa"));
        $surat = $this->surat_rekomendasi_penolakan_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY jumlah_rumah DESC SEPARATOR '|') AS jumlah_rumah
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN (
                            SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                            FROM trx_terkonfirmasi_rilis 
                            WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                            GROUP BY peserta_id
                            ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL
                    GROUP BY kelurahan_master_wilayah_id,rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN 
                        (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis
                        WHERE deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND id_master_wilayah = '${id_master_wilayah}'
            GROUP BY id_master_wilayah
            "
        )->row();

        $warna_zonasi = "";
        $expl_jumlah_rumah_arsel = explode("|", $surat->jumlah_rumah);
        if ($expl_jumlah_rumah_arsel[0] == '0') {
            $warna_zonasi = "Hijau";
        } else if ($expl_jumlah_rumah_arsel[0] == '1' || $expl_jumlah_rumah_arsel[0] == '2') {
            $warna_zonasi = "Kuning";
        } else if ($expl_jumlah_rumah_arsel[0] == '3' || $expl_jumlah_rumah_arsel[0] == '4' || $expl_jumlah_rumah_arsel[0] == '5') {
            $warna_zonasi = "Orange";
        } else if ($expl_jumlah_rumah_arsel[0] >= '6') {
            $warna_zonasi = "Merah";
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($warna_zonasi));
    }
}
