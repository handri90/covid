<style>
    .info-column {
        margin: 10px 0;
        border: 1px solid grey;
    }

    .badge {
        margin-left: 10px;
    }
</style>

<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Kecamatan : </label>
                        <select class="form-control select-search" name="kecamatan" onchange="get_peserta()">
                            <option value="">-- Pilih Kecamatan --</option>
                            <?php
                            foreach ($list_kecamatan as $key => $row) {
                            ?>
                                <option value="<?php echo encrypt_data($row->kode_wilayah); ?>"><?php echo $row->nama_wilayah; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row page-rilis">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="card card-table card-table-kecamatan">
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary" onclick="save_peserta_rilis()">Simpan <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h5>Laporan Rilis</h5>
                    <div class="text-right">
                        <div style="display:inline">
                            <input type="text" class="form-control daterange-single" style="display:inline;width:auto;" readonly name="tanggal_rilis" required placeholder="Tanggal Rilis" onchange="get_peserta_rilis()">
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card card-table card-table-rilis">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.daterange-single').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD/MM/YYYY'
        },
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10)
    });

    $(".page-rilis").hide();

    function get_peserta() {
        $(".card-table-kecamatan").html("");
        get_peserta_rilis();

        let kecamatan = $("select[name='kecamatan']").val();

        $.ajax({
            url: base_url + 'peserta_rilis/request/get_peserta',
            data: {
                kecamatan: kecamatan
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $(".page-rilis").show();
                let html = "<table class='table'><th width='40%'>Nama Wilayah</th><th>Peserta</th>";
                $.each(response, function(index, value) {
                    html += "<tr>" +
                        "<td>" + value.nama_wilayah + "</td>" +
                        "<td>";
                    if (value.nama) {
                        let td_nama = value.nama.split("|");
                        let td_status = value.status.split("|");
                        let td_tanggal_masuk = value.tanggal_masuk.split("|");
                        let td_id_trx_terkonfirmasi = value.id_trx_terkonfirmasi.split("|");
                        html += "<ul class='list-unstyled mb-0'>";
                        for (let n = 0; n < td_nama.length; n++) {

                            html += "<li class='mb-3'>" +
                                "<div class='d-flex align-items-center mb-1'>" + td_nama[n] + (td_status[n] == "1" ? "<span class='badge badge-danger'>Positif</span>" : (td_status[n] == "2" ? "<span class='badge badge-info'>Sembuh</span>" : (td_status[n] == "3" ? "<span class='badge badge-dark'>Meninggal</span>" : (td_status[n] == "4" ? "<span class='badge badge-dark'>Probable</span>" : "")))) + "&nbsp;&nbsp;" + td_tanggal_masuk[n] + " <span class='text-muted ml-auto'><input type='checkbox' class='terkonfirmasi' name='terkonfirmasi[]' value='" + td_id_trx_terkonfirmasi[n] + "'></span></div>" +
                                "<hr />" +
                                "</li>";
                        }
                        html += "</ul>";
                    }
                    html += "</td>";
                    "</tr>";
                })
                html += "</table>";

                $(".card-table-kecamatan").html(html);
            },
            complete: function() {
                HoldOn.close();
            }
        });

    }

    function get_peserta_rilis() {
        $(".card-table-rilis").html("");

        let kecamatan = $("select[name='kecamatan']").val();
        let tanggal_rilis = $("input[name='tanggal_rilis']").val();

        $.ajax({
            url: base_url + 'peserta_rilis/request/get_peserta_rilis',
            data: {
                kecamatan: kecamatan,
                tanggal_rilis: tanggal_rilis
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let html = "<table class='table'><th>Nama Wilayah</th><th>Nama Peserta</th><th>Status</th><th>Action</th>";
                $.each(response, function(index, value) {
                    html += "<tr>" +
                        "<td>" + value.nama_wilayah + "</td>" +
                        "<td>" + value.nama + "</td>" +
                        "<td>" + (value.status == "1" ? "<span class='badge badge-danger'>Positif</span>" : (value.status == "2" ? "<span class='badge badge-info'>Sembuh</span>" : (value.status == "3" ? "<span class='badge badge-dark'>Meninggal</span>" : (value.status == "4" ? "<span class='badge badge-dark'>Probable</span>" : "")))) + "</td>" +
                        "<td><a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_terkonfirmasi_rilis_encrypt + "')\" href='#'><i class='icon-trash'></i></a></td>"

                    html += "</tr>";
                })
                html += "</table>";

                $(".card-table-rilis").html(html);
            },
            complete: function() {
                HoldOn.close();
            }
        });

    }

    function confirm_delete(id_trx_terkonfirmasi) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta_rilis/delete_peserta_rilis',
                    data: {
                        id_trx_terkonfirmasi: id_trx_terkonfirmasi
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_peserta();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_peserta();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }

    function save_peserta_rilis() {
        let tanggal_rilis = $("input[name='tanggal_rilis']").val();

        var terkonfirmasi_arr = [];
        $(".terkonfirmasi:checked").each(function() {
            terkonfirmasi_arr.push($(this).val());
        });

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        if (terkonfirmasi_arr.length > 0) {
            swalInit({
                title: 'Apakah anda yakin menambah data ini?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya!',
                cancelButtonText: 'Batal!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'peserta_rilis/save_peserta_rilis',
                        data: {
                            terkonfirmasi: terkonfirmasi_arr,
                            tanggal_rilis: tanggal_rilis
                        },
                        type: 'POST',
                        beforeSend: function() {
                            HoldOn.open(optionsHoldOn);
                        },
                        success: function(response) {
                            if (response) {
                                get_peserta();
                                swalInit(
                                    'Berhasil',
                                    'Data berhasil disimpan',
                                    'success'
                                );
                            } else {
                                get_peserta();
                                swalInit(
                                    'Gagal',
                                    'Data gagal disimpan',
                                    'error'
                                );
                            }
                        },
                        complete: function() {
                            HoldOn.close();
                        }
                    });
                } else if (result.dismiss === swal.DismissReason.cancel) {
                    swalInit(
                        'Batal',
                        'Data masih tersimpan!',
                        'error'
                    ).then(function(results) {
                        HoldOn.close();
                        if (result.results) {
                            get_peserta();
                        }
                    });
                }
            });
        } else {
            swalInit(
                'Gagal',
                'Data belum dipilih',
                'error'
            );
        }
    }
</script>