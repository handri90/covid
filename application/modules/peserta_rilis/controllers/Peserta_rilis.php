<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Peserta_rilis extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model('status_peserta/trx_terkonfirmasi_model', 'trx_terkonfirmasi_model');
        $this->load->model('trx_terkonfirmasi_rilis_model');
        $this->load->model('peserta/master_wilayah_model', 'master_wilayah_model');
    }

    public function index()
    {
        if (empty($_POST)) {
            $data['list_kecamatan'] = $this->master_wilayah_model->get(
                array(
                    "where" => array(
                        "klasifikasi" => "KEC"
                    ),
                    "order_by" => array(
                        "nama_wilayah" => "ASC"
                    )
                )
            );

            $data['breadcrumb'] = [['link' => false, 'content' => 'Peserta Rilis', 'is_active' => true]];

            $this->execute('index', $data);
        } else {
        }
    }

    public function save_peserta_rilis()
    {
        $terkonfirmasi = $this->ipost('terkonfirmasi');
        $tanggal_rilis = $this->ipost('tanggal_rilis');
        $expl_tanggal_rilis = explode("/", $tanggal_rilis);

        $status = false;
        foreach ($terkonfirmasi as $key => $row) {
            $data_terkonfirmasi_real = $this->trx_terkonfirmasi_model->get_by($row);
            if ($data_terkonfirmasi_real) {
                $data = array(
                    "trx_terkonfirmasi_id" => $data_terkonfirmasi_real->id_trx_terkonfirmasi,
                    "peserta_id" => $data_terkonfirmasi_real->peserta_id,
                    "tanggal_terkonfirmasi" => $expl_tanggal_rilis[2] . "-" . $expl_tanggal_rilis[1] . "-" . $expl_tanggal_rilis[0] . " " . date("H:i:s"),
                    "status" => $data_terkonfirmasi_real->status,
                    "trx_pemeriksaan_id" => $data_terkonfirmasi_real->trx_pemeriksaan_id,
                    "created_at" => $expl_tanggal_rilis[2] . "-" . $expl_tanggal_rilis[1] . "-" . $expl_tanggal_rilis[0] . " " . date("H:i:s"),
                    "updated_at" => $data_terkonfirmasi_real->updated_at,
                    "deleted_at" => $data_terkonfirmasi_real->deleted_at,
                    "id_user_created" => $data_terkonfirmasi_real->id_user_created,
                    "id_user_updated" => $data_terkonfirmasi_real->id_user_updated,
                    "id_user_deleted" => $data_terkonfirmasi_real->id_user_deleted
                );

                $status = $this->trx_terkonfirmasi_rilis_model->save($data);
            }
        }

        if ($status) {
            $data = true;
        } else {
            $data = false;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function delete_peserta_rilis()
    {
        $id_trx_terkonfirmasi = $this->iget('id_trx_terkonfirmasi');
        $data_master = $this->trx_terkonfirmasi_rilis_model->get_by(decrypt_data($id_trx_terkonfirmasi));

        if (!$data_master) {
            $this->page_error();
        }

        $data = array(
            "deleted_at" => $this->datetime()
        );

        $status = $this->trx_terkonfirmasi_rilis_model->edit(decrypt_data($id_trx_terkonfirmasi), $data);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }
}
