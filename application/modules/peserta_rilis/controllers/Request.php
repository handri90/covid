<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('trx_terkonfirmasi_rilis_model');
        $this->load->model('peserta/master_wilayah_model', 'master_wilayah_model');
    }

    public function get_peserta()
    {
        $kecamatan = decrypt_data($this->iget("kecamatan"));

        $data_peserta = $this->master_wilayah_model->query("
        SELECT 
        master_wilayah.*,
        GROUP_CONCAT(a.nama ORDER BY a.id_peserta SEPARATOR '|') AS nama,
        GROUP_CONCAT(a.status ORDER BY a.id_peserta SEPARATOR '|') AS status,
        GROUP_CONCAT(a.tanggal_masuk ORDER BY a.id_peserta SEPARATOR '|') AS tanggal_masuk,
        GROUP_CONCAT(a.id_trx_terkonfirmasi ORDER BY a.id_peserta SEPARATOR '|') AS id_trx_terkonfirmasi
        FROM master_wilayah
        LEFT JOIN (
            SELECT peserta.nama,kelurahan_master_wilayah_id,id_peserta,trx_terkonfirmasi.`status`,DATE_FORMAT(trx_terkonfirmasi.`created_at`,'%d-%m-%Y') AS tanggal_masuk,id_trx_terkonfirmasi
            FROM peserta
            INNER JOIN trx_terkonfirmasi ON id_peserta=peserta_id AND trx_terkonfirmasi.deleted_at IS NULL
            LEFT JOIN trx_terkonfirmasi_rilis ON id_trx_terkonfirmasi=trx_terkonfirmasi_id AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            WHERE id_trx_terkonfirmasi_rilis IS NULL AND peserta.`deleted_at` IS NULL
        ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
        WHERE kode_induk = '" . $kecamatan . "'
        GROUP BY id_master_wilayah
        ORDER BY klasifikasi DESC,nama_wilayah ASC
        ")->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
        }


        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_rilis()
    {
        $tanggal_rilis = $this->iget("tanggal_rilis");

        $data_peserta = $this->master_wilayah_model->query("
        SELECT peserta.nama,trx_terkonfirmasi_rilis.status AS status,b.nama_wilayah,id_trx_terkonfirmasi_rilis
        FROM peserta
        INNER JOIN master_wilayah a ON kelurahan_master_wilayah_id=a.id_master_wilayah
        LEFT JOIN master_wilayah b ON a.kode_induk=b.kode_wilayah
        INNER JOIN trx_terkonfirmasi ON id_peserta=peserta_id AND trx_terkonfirmasi.deleted_at IS NULL
        INNER JOIN trx_terkonfirmasi_rilis ON trx_terkonfirmasi_id=id_trx_terkonfirmasi AND trx_terkonfirmasi_rilis.deleted_at IS NULL
        WHERE peserta.deleted_at IS NULL AND id_trx_terkonfirmasi_rilis IS NOT NULL AND DATE_FORMAT(trx_terkonfirmasi_rilis.`created_at`,'%d/%m/%Y') = '" . $tanggal_rilis . "'
        ")->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_terkonfirmasi_rilis_encrypt'] = encrypt_data($row->id_trx_terkonfirmasi_rilis);
        }


        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
