<?php

class Trx_terkonfirmasi_rilis_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "trx_terkonfirmasi_rilis";
        $this->primary_id = "id_trx_terkonfirmasi_rilis";
    }
}
