<style>
	.user-image-custom {
		margin-bottom: 10px;
	}
</style>
<div class="content">
	<!-- Form inputs -->
	<div class="card">
		<div class="card-body">
			<?php echo form_open_multipart(current_url(), array('class' => 'form-validate-jquery')); ?>
			<fieldset class="mb-3">
				<legend class="text-uppercase font-size-sm font-weight-bold">User</legend>

				<div class="form-group row">
					<label class="col-form-label col-lg-2">Level <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<select <?php echo $is_select_disable ? "disabled" : ""; ?> class="form-control select-search" name="level_user" required onchange="show_field()">
							<option value="">-- Pilih Level User --</option>
							<?php
							foreach ($level_user as $key => $row) {
								$selected = "";
								if (!empty($content)) {
									if ($row->id_level_user == $content->level_user_id) {
										$selected = 'selected="selected"';
									}
								}
							?>
								<option <?php echo $selected; ?> value="<?php echo $row->id_level_user; ?>"><?php echo $row->nama_level_user; ?></option>
							<?php
							}
							?>
						</select>
					</div>
				</div>

				<div class="form-group row is-show-list-wilayah-tni-polri">
					<label class="col-form-label col-lg-2">Wilayah <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<select <?php echo $is_select_disable ? "disabled" : ""; ?> class="form-control select-search" name="wilayah">
							<option value="">-- Pilih Wilayah --</option>
							<?php
							foreach ($master_wilayah as $key => $row) {
								$selected = "";
								if (!empty($content)) {
									if ($row->id_master_wilayah == $content->master_wilayah_id_aparat) {
										$selected = 'selected="selected"';
									}
								}
							?>
								<option <?php echo $selected; ?> value="<?php echo encrypt_data($row->id_master_wilayah); ?>"><?php echo $row->nama_wilayah; ?></option>
							<?php
							}
							?>
						</select>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-form-label col-lg-2">Nama Lengkap <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" class="form-control" value="<?php echo !empty($content) ? $content->nama_lengkap : ""; ?>" name="nama_lengkap" required placeholder="Nama Lengkap">
						<input type="hidden" name="id_user" value="<?php echo !empty($content) ? encrypt_data($content->id_user) : ""; ?>" />
						<input type="hidden" name="id_level_user" value="<?php echo !empty($content) ? $content->level_user_id : ""; ?>" />
					</div>
				</div>

				<div class="form-group row is-show-nip">
					<label class="col-form-label col-lg-2">NIP <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" class="form-control" value="<?php echo !empty($content) ? $content->nip : ""; ?>" name="nip" placeholder="NIP">
					</div>
				</div>

				<div class="form-group row">
					<label class="col-form-label col-lg-2">Username <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" class="form-control" value="<?php echo !empty($content) ? $content->username : ""; ?>" name="username" required placeholder="Username">
					</div>
				</div>

				<div class="form-group row">
					<label class="col-form-label col-lg-2">Password <?php echo !empty($content) ? '' : '<span class="text-danger">*</span>'; ?></label>
					<div class="col-lg-10">
						<input type="password" name="password" id="password" <?php echo !empty($content) ? '' : 'required'; ?> placeholder="Password" class="form-control">
						<?php echo empty($content) ? '' : '<span class="form-text text-muted">Jika ingin merubah password, silahkan diisi</span>'; ?>
					</div>
				</div>

				<div class="form-group row is-show-list-puskesmas">
					<label class="col-form-label col-lg-2">Puskesmas <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<select class="form-control select-search" name="puskesmas" required>
							<option value="">-- Pilih Puskesmas --</option>
							<?php
							foreach ($list_puskesmas as $key => $row) {
								$selected = "";
								if (!empty($content)) {
									if ($row->id_master_puskesmas == $content->puskesmas_id) {
										$selected = 'selected="selected"';
									}
								}
							?>
								<option <?php echo $selected; ?> value="<?php echo $row->id_master_puskesmas; ?>"><?php echo $row->nama_puskesmas; ?></option>
							<?php
							}
							?>
						</select>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-lg-2 col-form-label">Foto </label>
					<div class="col-lg-10">
						<?php
						if (!empty($content) && $content->foto_user) {
						?>
							<img width="100" class="user-image-custom" src="<?php echo base_url() . $this->config->item('user_path') . "/" . ($content->foto_user != "" ? $content->foto_user : "default_profile.jpg"); ?>">
						<?php
						}
						?>
						<input type="file" class="file-input" name="foto_user" data-show-upload="false">
					</div>
				</div>
			</fieldset>

			<div class="text-right">
				<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
	<!-- /form inputs -->

</div>

<script>
	$(".is-show-list-puskesmas").hide();
	$(".is-show-nip").hide();
	$(".is-show-list-wilayah-tni-polri").hide();

	$(".file-input").on('change', function() {
		$(".user-image-custom").hide();
	});

	$("form").submit(function() {
		var swalInit = swal.mixin({
			buttonsStyling: false,
			confirmButtonClass: 'btn btn-primary',
			cancelButtonClass: 'btn btn-light'
		});

		let id_user = $("input[name=id_user]").val();
		let username = $("input[name=username]").val();
		let status = true;

		$.ajax({
			url: base_url + 'user/request/cek_username',
			async: false,
			data: {
				username: username,
				id_user: id_user
			},
			type: 'GET',
			beforeSend: function() {
				HoldOn.open(optionsHoldOn);
			},
			success: function(response) {
				// alert(response);
				status = response;
			},
			complete: function(response) {
				HoldOn.close();
			}
		});

		if (!status) {
			swalInit(
				'Gagal',
				'Username sudah digunakan',
				'error'
			);
			return false;
		}
	});

	show_field();

	function show_field() {
		let id_level = $("select[name='level_user']").val();
		if (!id_level) {
			id_level = $("input[name='id_level_user']").val()
		}
		$(".is-show-list-puskesmas").hide();
		$(".is-show-nip").hide();
		$(".is-show-list-wilayah-tni-polri").hide();

		$("select[name='puskesmas']").attr("required", false);
		$("select[name='wilayah']").attr("required", false);
		$("input[name='nip']").attr("required", false);
		if (id_level == "2") {
			$(".is-show-list-puskesmas").show();
			$("select[name='puskesmas']").attr("required", true);
		} else if (id_level == "9") {
			$(".is-show-nip").show();
			$("input[name='nip']").attr("required", true);
		} else if (id_level == "10") {
			$(".is-show-list-wilayah-tni-polri").show();
			$("select[name='wilayah']").attr("required", true);
		}
	}
</script>