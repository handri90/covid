<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Status_peserta extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model('trx_terkonfirmasi_model');
        $this->load->model('peserta/peserta_model', 'peserta_model');
    }

    public function index()
    {
        $data['list_status'] = array("1" => "Positif", "2" => "Sembuh", "3" => "Meninggal");

        $data['breadcrumb'] = [['link' => false, 'content' => 'Status Peserta', 'is_active' => true]];

        $data['list_peserta'] = $this->peserta_model->get();

        $this->execute('index', $data);
    }

    public function tambah_status_peserta()
    {
        if (empty($_POST)) {
            $data['list_peserta'] = $this->peserta_model->get();

            $data['list_status'] = array("1" => "Positif", "2" => "Sembuh", "3" => "Meninggal");

            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'status_peserta', 'content' => 'Status Peserta', 'is_active' => false], ['link' => false, 'content' => 'Tambah Status Peserta', 'is_active' => true]];

            $this->execute('form_status_peserta', $data);
        } else {
            $nama = $this->ipost("nama");
            $status_peserta = $this->ipost("status");
            $date_exp = explode("/", $this->ipost('tanggal_terkonfirmasi'));
            $tanggal_konfirmasi = $date_exp[2] . "-" . $date_exp[1] . "-" . $date_exp[0];

            if (!empty($nama)) {
                for ($i = 0; $i < count($nama); $i++) {
                    $data_peserta = array(
                        "peserta_id" => decrypt_data($nama[$i]),
                        "status" => $status_peserta,
                        "tanggal_terkonfirmasi" => $tanggal_konfirmasi . " " . date("H:i:s"),
                        'created_at' => $this->datetime(),
                        'id_user_created' => $this->session->userdata("id_user")
                    );

                    $status = $this->trx_terkonfirmasi_model->save($data_peserta);
                }
            }

            if ($status) {
                $this->session->set_flashdata('message', 'Data baru berhasil ditambahkan');
            } else {
                $this->session->set_flashdata('message', 'Data baru gagal ditambahkan');
            }

            redirect('status_peserta');
        }
    }

    public function edit_status_peserta($id_konfirmasi)
    {
        if (empty($_POST)) {
            $data['content'] = $this->trx_terkonfirmasi_model->get(
                array(
                    "fields" => "trx_terkonfirmasi.*,DATE_FORMAT(tanggal_terkonfirmasi,'%d/%m/%Y') as tanggal_terkonfirmasi",
                    "where" => array(
                        "id_trx_terkonfirmasi" => decrypt_data($id_konfirmasi)
                    )
                ),
                "row"
            );

            $data['list_peserta'] = $this->peserta_model->get();

            $data['list_status'] = array("1" => "Positif", "2" => "Sembuh", "3" => "Meninggal");

            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'status_peserta', 'content' => 'Status Peserta', 'is_active' => false], ['link' => false, 'content' => 'Tambah Status Peserta', 'is_active' => true]];

            $this->execute('form_status_peserta', $data);
        } else {
            $nama = $this->ipost("nama");
            $status_peserta = $this->ipost("status");
            $date_exp = explode("/", $this->ipost('tanggal_terkonfirmasi'));
            $tanggal_konfirmasi = $date_exp[2] . "-" . $date_exp[1] . "-" . $date_exp[0];

            $data_peserta = array(
                "peserta_id" => decrypt_data($nama),
                "status" => $status_peserta,
                "tanggal_terkonfirmasi" => $tanggal_konfirmasi . " " . date("H:i:s"),
                'updated_at' => $this->datetime(),
                'id_user_updated' => $this->session->userdata("id_user")
            );

            $status = $this->trx_terkonfirmasi_model->edit(decrypt_data($id_konfirmasi), $data_peserta);

            if ($status) {
                $this->session->set_flashdata('message', 'Data baru berhasil ditambahkan');
            } else {
                $this->session->set_flashdata('message', 'Data baru gagal ditambahkan');
            }

            redirect('status_peserta');
        }
    }

    public function delete_status_peserta()
    {
        $id_konfirmasi = $this->iget('id_konfirmasi');
        $data_master = $this->trx_terkonfirmasi_model->get_by(decrypt_data($id_konfirmasi));

        if (!$data_master) {
            $this->page_error();
        }

        $data = array(
            "deleted_at" => $this->datetime(),
            "id_user_deleted" => $this->session->userdata("id_user")
        );

        $status = $this->trx_terkonfirmasi_model->edit(decrypt_data($id_konfirmasi), $data);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }
}
