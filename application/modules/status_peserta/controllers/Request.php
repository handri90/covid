<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('trx_pemeriksaan_model');
        $this->load->model('peserta/peserta_model', 'peserta_model');
    }

    public function get_peserta()
    {
        $status_peserta = $this->iget("status_peserta");
        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");

        $wh = array();
        if ($status_peserta) {
            $wh = array(
                "status" => $status_peserta
            );
        }

        $data_peserta = $this->peserta_model->get(
            array(
                "fields" => "id_trx_terkonfirmasi,peserta.nama,DATE_FORMAT(tanggal_terkonfirmasi,'%Y-%m-%d') AS tanggal_terkonfirmasi,status",
                "join" => array(
                    "trx_terkonfirmasi" => "id_peserta=peserta_id AND trx_terkonfirmasi.deleted_at IS NULL"
                ),
                "where" => $wh,
                "where_false" => "DATE_FORMAT(tanggal_terkonfirmasi,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "' AND trx_terkonfirmasi.deleted_at IS NULL"
            )
        );

        $templist = array();
        foreach ($data_peserta as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_trx_terkonfirmasi);
            $templist[$key]['tanggal_terkonfirmasi'] = date_indo($row->tanggal_terkonfirmasi);
        }


        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
