<?php

class Trx_terkonfirmasi_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "trx_terkonfirmasi";
        $this->primary_id = "id_trx_terkonfirmasi";
    }
}
