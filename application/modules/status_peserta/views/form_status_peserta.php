<style>
    .user-image-custom {
        margin-bottom: 10px;
    }
</style>
<div class="content">
    <!-- Form inputs -->
    <div class="card">
        <div class="card-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-validate-jquery')); ?>
            <fieldset class="mb-3">
                <legend class="text-uppercase font-size-sm font-weight-bold">User</legend>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nama <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <?php

                        if (isset($content)) {
                        ?>
                            <select class="form-control select-search" name="nama" required>
                                <option value="all">-- Pilih Nama Peserta --</option>
                                <?php
                                foreach ($list_peserta as $key => $row) {
                                    $selected = "";
                                    if ($row->id_peserta == $content->peserta_id) {
                                        $selected = "selected";
                                    }
                                ?>
                                    <option <?php echo $selected; ?> value="<?php echo encrypt_data($row->id_peserta); ?>"><?php echo $row->nik . " - " . $row->nama; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        <?php
                        } else {
                        ?>
                            <select multiple="multiple" class="form-control select-search" name="nama[]" required>
                                <option value="all">-- Pilih Nama Peserta --</option>
                                <?php
                                foreach ($list_peserta as $key => $row) {
                                ?>
                                    <option value="<?php echo encrypt_data($row->id_peserta); ?>"><?php echo $row->nik . " - " . $row->nama; ?></option>
                                <?php
                                }
                                ?>
                            </select>

                        <?php
                        }

                        ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Tanggal <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control daterange-single" readonly value="<?php echo !empty($content) ? $content->tanggal_terkonfirmasi : ""; ?>" name="tanggal_terkonfirmasi" required placeholder="Tanggal">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Status <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <select class="form-control select-search" name="status" required>
                            <option value="">-- Pilih Status Peserta --</option>
                            <?php
                            foreach ($list_status as $key => $row) {
                                $selected = "";
                                if (!empty($content)) {
                                    if ($key == $content->status) {
                                        $selected = 'selected="selected"';
                                    }
                                }
                            ?>
                                <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $row; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <!-- /form inputs -->

</div>

<script>
    $('.daterange-single').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD/MM/YYYY'
        },
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10)
    });
</script>