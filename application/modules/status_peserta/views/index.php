<style>
    .info-column {
        margin: 4px 0;
    }
</style>

<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tanggal: </label><br>
                        <button type="button" class="btn btn-light daterange-predefined">
                            <i class="icon-calendar22 mr-2"></i>
                            <span></span>
                        </button>

                        <input type="hidden" name="start_date" />
                        <input type="hidden" name="end_date" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Status : </label>
                        <select class="form-control select-search" name="status" onchange="get_peserta()">
                            <option value="">-- Pilih Status --</option>
                            <?php
                            foreach ($list_status as $key => $row) {
                            ?>
                                <option value="<?php echo $key; ?>"><?php echo $row; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url() . 'status_peserta/tambah_status_peserta'; ?>" class="btn btn-info">Tambah Status Peserta</a>
            </div>
        </div>
        <table id="datatableStatusPeserta" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>Tanggal Terkonfirmasi</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    $(".is_show_detail").hide();
    let datatableStatusPeserta = $("#datatableStatusPeserta").DataTable({
        "ordering": false
    });

    function get_peserta() {

        let status_peserta = $("select[name='status']").val();
        let start_date = $("input[name='start_date']").val();
        let end_date = $("input[name='end_date']").val();

        datatableStatusPeserta.clear().draw();

        $.ajax({
            url: base_url + 'status_peserta/request/get_peserta',
            data: {
                status_peserta: status_peserta,
                start_date: start_date,
                end_date: end_date
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    datatableStatusPeserta.row.add([
                        value.nama,
                        value.tanggal_terkonfirmasi,
                        (value.status == "1" ? "<span class='badge badge-danger'>Positif</span>" : (value.status == "2" ? "<span class='badge badge-success'>Sembuh</span>" : (value.status == "3" ? "<span class='badge badge-secondary'>Meninggal</span>" : ""))),
                        "<a href='" + base_url + "status_peserta/edit_status_peserta/" + value.id_encrypt + "' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>"
                    ]).draw(false);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });

    }

    $('.daterange-predefined').daterangepicker({
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            dateLimit: {
                days: 60
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.daterange-predefined span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date]').val(end.format('YYYY-MM-DD'));
            get_peserta();
        }
    );

    // Display date format
    $('.daterange-predefined span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date]').val(moment().subtract(29, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date]').val(moment().format('YYYY-MM-DD'));
    get_peserta();

    function confirm_delete(id_konfirmasi) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'status_peserta/delete_status_peserta',
                    data: {
                        id_konfirmasi: id_konfirmasi
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_peserta();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_peserta();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }
</script>