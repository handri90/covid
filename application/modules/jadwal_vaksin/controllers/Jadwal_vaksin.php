<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jadwal_vaksin extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model("jadwal_vaksin_model");
    }

    public function index()
    {
        $data['breadcrumb'] = [['link' => false, 'content' => 'Jadwal Vaksinasi', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function tambah_jadwal()
    {
        $data['is_select_disable'] = false;
        if (empty($_POST)) {
            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'jadwal_vaksinasi', 'content' => 'Jadwal Vaksinasi', 'is_active' => false], ['link' => false, 'content' => 'Tambah Jadwal', 'is_active' => true]];
            $this->execute('form_jadwal_vaksinasi', $data);
        } else {
            $tanggal_vaksin = explode("/", $this->ipost('tanggal_vaksin'));
            $new_format_tanggal = $tanggal_vaksin[2] . "-" . $tanggal_vaksin[1] . "-" . $tanggal_vaksin[0];

            $data = array(
                "tanggal_vaksin" => $new_format_tanggal,
                "jam_vaksin" => date("H:i:s", strtotime($this->ipost('jam_vaksin'))),
                "tempat_pelaksanaan" => $this->ipost('tempat_pelaksanaan'),
                "id_user_created" => $this->session->userdata('id_user'),
                'created_at' => $this->datetime()
            );

            $status = $this->jadwal_vaksin_model->save($data);
            if ($status) {
                $this->session->set_flashdata('message', 'Data baru berhasil ditambahkan');
            } else {
                $this->session->set_flashdata('message', 'Data baru gagal ditambahkan');
            }


            redirect('jadwal_vaksin');
        }
    }

    public function ubah_jadwal($id_jadwal_vaksin)
    {


        $data_master = $this->jadwal_vaksin_model->get(
            array(
                "fields" => "tempat_pelaksanaan,DATE_FORMAT(tanggal_vaksin,'%d/%m/%Y') as tanggal_vaksin, DATE_FORMAT(jam_vaksin, '%H:%i') AS jam_vaksin",
                "where" => array(
                    "id_jadwal_vaksin" => decrypt_data($id_jadwal_vaksin)
                )
            ),
            "row"
        );

        if (!$data_master) {
            $this->page_error();
        }

        if (empty($_POST)) {
            $data['content'] = $data_master;

            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'jadwal_vaksin', 'content' => 'Jadwal Vaksin', 'is_active' => false], ['link' => false, 'content' => 'Ubah Jadwal Vaksin', 'is_active' => true]];

            $this->execute('form_jadwal_vaksinasi', $data);
        } else {

            $tanggal_vaksin = explode("/", $this->ipost('tanggal_vaksin'));
            $new_format_tanggal = $tanggal_vaksin[2] . "-" . $tanggal_vaksin[1] . "-" . $tanggal_vaksin[0];
            $data = array(
                "tanggal_vaksin" => $new_format_tanggal,
                "jam_vaksin" => date("H:i:s", strtotime($this->ipost('jam_vaksin'))),
                "tempat_pelaksanaan" => $this->ipost('tempat_pelaksanaan'),
                "id_user_updated" => $this->session->userdata('id_user'),
                'updated_at' => $this->datetime()
            );

            $status = $this->jadwal_vaksin_model->edit(decrypt_data($id_jadwal_vaksin), $data);

            if ($status) {
                $this->session->set_flashdata('message', 'Data berhasil diubah');
            } else {
                $this->session->set_flashdata('message', 'Data gagal diubah');
            }

            redirect('jadwal_vaksin');
        }
    }

    public function delete_jadwal_vaksin()
    {
        $id_jadwal_vaksin = $this->iget('id_jadwal_vaksin');
        $data_master = $this->jadwal_vaksin_model->get_by(decrypt_data($id_jadwal_vaksin));

        if (!$data_master) {
            $this->page_error();
        }

        $status = $this->jadwal_vaksin_model->remove(decrypt_data($id_jadwal_vaksin));
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }
}
