<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("jadwal_vaksin_model");
    }

    public function get_jadwal_vaksin()
    {
        $jadwal_vaksin = $this->jadwal_vaksin_model->get(
            array(
                "where" => array(
                    "id_user_created" => $this->session->userdata('id_user')
                ),
                "order_by_false" => "tanggal_vaksin,jam_vaksin DESC"
            )
        );

        $templist = array();
        foreach ($jadwal_vaksin as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['tanggal_custom'] = longdate_indo($row->tanggal_vaksin);
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_jadwal_vaksin);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
