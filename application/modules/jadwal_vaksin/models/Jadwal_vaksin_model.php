<?php

class Jadwal_vaksin_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "jadwal_vaksin";
        $this->primary_id = "id_jadwal_vaksin";
    }
}
