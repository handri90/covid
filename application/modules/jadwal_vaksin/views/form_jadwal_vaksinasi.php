<style>
	.user-image-custom {
		margin-bottom: 10px;
	}
</style>
<div class="content">
	<!-- Form inputs -->
	<div class="card">
		<div class="card-body">
			<?php echo form_open(); ?>
			<fieldset class="mb-3">
				<legend class="text-uppercase font-size-sm font-weight-bold">Jadwal Vaksin</legend>

				<div class="form-group row">
					<label class="col-form-label col-lg-2">Tanggal <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" class="form-control" value="<?php echo !empty($content) ? $content->tanggal_vaksin : ""; ?>" name="tanggal_vaksin" id="tanggal_vaksin" readonly placeholder="Tanggal/Jam">
					</div>
				</div>

				<div class="form-group row">
					<label class="col-form-label col-lg-2">Jam <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" class="form-control" value="<?php echo !empty($content) ? $content->jam_vaksin : ""; ?>" name="jam_vaksin" id="jam_vaksin" readonly placeholder="Tanggal/Jam">
					</div>
				</div>

				<div class="form-group row is-show-nip">
					<label class="col-form-label col-lg-2">Tempat Pelaksanaan <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" class="form-control" value="<?php echo !empty($content) ? $content->tempat_pelaksanaan : ""; ?>" name="tempat_pelaksanaan" placeholder="Tempat Pelaksanaan">
					</div>
				</div>
			</fieldset>

			<div class="text-right">
				<button type="submit" class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
	<!-- /form inputs -->

</div>

<script>
	$('#tanggal_vaksin').AnyTime_picker({
		format: '%d/%m/%Z'
	});

	$('#jam_vaksin').AnyTime_picker({
		format: '%H:%i'
	});
</script>