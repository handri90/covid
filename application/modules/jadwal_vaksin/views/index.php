<div class="content">
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url() . 'jadwal_vaksin/tambah_jadwal'; ?>" class="btn btn-info">Tambah Jadwal Vaksin</a>
            </div>
        </div>
        <table id="datatableJadwalVaksin" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Jam</th>
                    <th>Tempat Pelaksanaan</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    let datatableJadwalVaksin = $("#datatableJadwalVaksin").DataTable();

    get_jadwal_vaksin();

    function get_jadwal_vaksin() {
        datatableJadwalVaksin.clear().draw();
        $.ajax({
            url: base_url + 'jadwal_vaksin/request/get_jadwal_vaksin',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    datatableJadwalVaksin.row.add([
                        value.tanggal_custom,
                        value.jam_vaksin,
                        value.tempat_pelaksanaan,
                        "<a href='" + base_url + "jadwal_vaksin/ubah_jadwal/" + value.id_encrypt + "' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>"
                    ]).draw(false);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function confirm_delete(id_jadwal_vaksin) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'jadwal_vaksin/delete_jadwal_vaksin',
                    data: {
                        id_jadwal_vaksin: id_jadwal_vaksin
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_jadwal_vaksin();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_jadwal_vaksin();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_jadwal_vaksin();
                    }
                });
            }
        });
    }
</script>