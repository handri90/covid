<style>
    .ft-bld {
        font-weight: bold;
    }
</style>

<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <?php echo form_open(base_url() . "laporan_rilis/cetak_laporan", array("target" => "_blank", "method" => "get")); ?>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Kecamatan</label>
                <div class="col-lg-10">
                    <select class="form-control select-search" name="kecamatan" onchange="get_desa_kel()">
                        <option value="">-- Pilih Kecamatan --</option>
                        <?php
                        foreach ($list_kecamatan as $key => $row) {
                        ?>
                            <option value="<?php echo encrypt_data($row->id_master_wilayah); ?>"><?php echo $row->nama_wilayah; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Desa / Kel</label>
                <div class="col-lg-10">
                    <select class="form-control select-search" name="desa_kel" onchange="get_peserta()">
                        <option value="">-- Semua --</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Tanggal</label>
                <div class="col-lg-10">
                    <button type="button" class="btn btn-light daterange-predefined">
                        <i class="icon-calendar22 mr-2"></i>
                        <span></span>
                    </button>

                    <input type="hidden" name="start_date" />
                    <input type="hidden" name="end_date" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Jenis Kelamin</label>
                <div class="col-lg-10">
                    <select class="form-control select-search" name="jenis_kelamin" onchange="get_peserta()">
                        <option value="">-- Semua --</option>
                        <option value="L">Laki-Laki</option>
                        <option value="P">Perempuan</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Umur</label>
                <div class="col-lg-10">
                    <select class="form-control select-search" name="umur" onchange="get_peserta()">
                        <option value="">-- Semua --</option>
                        <?php
                        foreach ($list_umur as $key => $row) {
                        ?>
                            <option value="<?php echo $key; ?>"><?php echo $row; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Pekerjaan</label>
                <div class="col-lg-10">
                    <select class="form-control select-search" name="pekerjaan" onchange="get_peserta()">
                        <option value="">-- Semua --</option>
                        <?php
                        foreach ($list_pekerjaan as $key => $row) {
                            if ($key != '0') {
                        ?>
                                <option value="<?php echo $key; ?>"><?php echo $row; ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <div class="text-right">
                            <!-- <button type="submit" class="btn btn-success">Cetak</button> -->
                            <div class="btn-group">
                                <button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left dropdown-toggle" data-toggle="dropdown"><b><i class="icon-printer2"></i></b> Cetak</button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <button type="submit" class="dropdown-item" name="versi_cetak" value="pdf"><i class="icon-file-pdf"></i> PDF</button>
                                    <button type="submit" class="dropdown-item" name="versi_cetak" value="excel"><i class="icon-file-excel"></i> Excel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <div class="card card-table">
        <table id="datatablePesertaRapid" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>NIK</th>
                    <th>Jenis Kelamin</th>
                    <th>Alamat</th>
                    <th>RT</th>
                    <th>Desa / Kelurahan</th>
                    <th>Umur</th>
                    <th>Pekerjaan</th>
                    <th>Gejala Klinis</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    let datatablePesertaRapid = $("#datatablePesertaRapid").DataTable({
        "deferRender": true,
        "ordering": false,
        "columns": [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    function get_peserta() {
        let kecamatan = $("select[name='kecamatan']").val();
        let desa_kel = $("select[name='desa_kel']").val();
        let start_date = $("input[name='start_date']").val();
        let end_date = $("input[name='end_date']").val();
        let umur = $("select[name='umur']").val();
        let pekerjaan = $("select[name='pekerjaan']").val();
        let jenis_kelamin = $("select[name='jenis_kelamin']").val();

        datatablePesertaRapid.clear().draw();
        $.ajax({
            url: base_url + 'laporan_rilis/request/get_peserta',
            data: {
                kecamatan: kecamatan,
                desa_kel: desa_kel,
                start_date: start_date,
                end_date: end_date,
                umur: umur,
                pekerjaan: pekerjaan,
                jenis_kelamin: jenis_kelamin
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let no = 1;
                $.each(response, function(index, value) {
                    datatablePesertaRapid.row.add([
                        no,
                        value.nama,
                        value.nik,
                        value.jenis_kelamin,
                        value.alamat_ktp,
                        value.rt_ktp,
                        value.nama_wilayah,
                        value.umur_kena_covid,
                        value.pekerjaan_inti,
                        ""
                    ]).draw(false);
                    no++;
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function get_desa_kel() {
        let kecamatan = $("select[name='kecamatan']").val();

        if (!kecamatan) {
            $("select[name='desa_kel']").html("<option value=''>-- Semua --</option>");
        }

        $.ajax({
            url: base_url + 'laporan_rilis/request/get_desa_kel',
            data: {
                kecamatan: kecamatan
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let html = "";
                html += "<option value=''>-- Semua --</option>";
                $.each(response, function(index, value) {
                    html += "<option value='" + value.id_encrypt + "'>" + value.nama_wilayah + "</option>";
                });
                $("select[name='desa_kel']").html(html);
                get_peserta();
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    $('.daterange-predefined').daterangepicker({
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.daterange-predefined span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date]').val(end.format('YYYY-MM-DD'));
            get_peserta();
        }
    );

    // Display date format
    $('.daterange-predefined span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date]').val(moment().subtract(29, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date]').val(moment().format('YYYY-MM-DD'));
    get_peserta();
</script>