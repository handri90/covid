<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_rilis extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model("peserta/master_wilayah_model", "master_wilayah_model");
        $this->load->model("peserta/peserta_model", "peserta_model");
        $this->load->model('migrate_rt_domisili/master_rt_model', 'master_rt_model');
    }

    public function index()
    {
        $data['list_kecamatan'] = $this->master_wilayah_model->get(
            array(
                "where" => array(
                    "klasifikasi" => "KEC"
                ),
                "order_by" => array(
                    "nama_wilayah" => "ASC"
                )
            )
        );

        $data['list_pekerjaan'] = array("1" => "Polisi", "2" => "TNI", "3" => "ASN", "4" => "BUMN / BANK", "5" => "Tenaga Kesehatan", "6" => "Dosen / Guru", "7" => "Pelajar/Mahasiswa", "8" => "Ibu Rumah Tangga", "9" => "Ulama / Pendeta /  Rohaniawan", "10" => "Swasta", "11" => "Wiraswasta", "12" => "Umum / Keluarga / Pensiunan / Unidentifi", "13" => "Lainnya");

        $data['list_umur'] = ["1" => "0-5 Tahun", "2" => "6-11 Tahun", "3" => "12-25 Tahun", "4" => "26-45 Tahun", "5" => "46-65 Tahun", "6" => "> 65 Tahun", "7" => "0-18 Tahun"];

        $data['breadcrumb'] = [['link' => false, 'content' => 'Laporan', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function cetak_laporan()
    {
        $kecamatan = decrypt_data($this->iget("kecamatan"));
        $desa_kel = decrypt_data($this->iget("desa_kel"));
        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");
        $umur = $this->iget("umur");
        $pekerjaan = $this->iget("pekerjaan");
        $jenis_kelamin = $this->iget("jenis_kelamin");
        $versi_cetak = $this->iget("versi_cetak");

        $wh_kecamatan = "";
        if ($desa_kel) {
            $wh_kecamatan =
                "AND kelurahan_master_wilayah_id = '" . $desa_kel . "'";
        } elseif ($kecamatan) {
            $wh_kecamatan =
                "AND kelurahan_master_wilayah_id IN 
                (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    WHERE kode_induk = 
                    (
                        SELECT kode_wilayah
                        FROM master_wilayah
                        WHERE id_master_wilayah = '" . $kecamatan . "'
                    )
                )
            ";
        }

        $data['list_pekerjaan'] = array("1" => "Polisi", "2" => "TNI", "3" => "ASN", "4" => "BUMN / BANK", "5" => "Tenaga Kesehatan", "6" => "Dosen / Guru", "7" => "Pelajar/Mahasiswa", "8" => "Ibu Rumah Tangga", "9" => "Ulama / Pendeta /  Rohaniawan", "10" => "Swasta", "11" => "Wiraswasta", "12" => "Umum / Keluarga / Pensiunan / Unidentifi", "13" => "Lainnya");

        $wh_pekerjaan = "";
        if ($pekerjaan) {
            $wh_pekerjaan = "AND pekerjaan_inti = '" . $data['list_pekerjaan'][$pekerjaan] . "'";
        }

        $wh_umur = "";
        if ($umur) {
            if ($umur == '1') {
                $wh_umur = "AND TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_terkonfirmasi.created_at,'%Y-%m-%d')) between '0' and '5'";
            } elseif ($umur == '2') {
                $wh_umur = "AND TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_terkonfirmasi.created_at,'%Y-%m-%d')) between '6' and '11'";
            } elseif ($umur == '3') {
                $wh_umur = "AND TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_terkonfirmasi.created_at,'%Y-%m-%d')) between '12' and '25'";
            } elseif ($umur == '4') {
                $wh_umur = "AND TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_terkonfirmasi.created_at,'%Y-%m-%d')) between '26' and '45'";
            } elseif ($umur == '5') {
                $wh_umur = "AND TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_terkonfirmasi.created_at,'%Y-%m-%d')) between '46' and '65'";
            } elseif ($umur == '6') {
                $wh_umur = "AND TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_terkonfirmasi.created_at,'%Y-%m-%d')) between '66' and '200'";
            } elseif ($umur == '7') {
                $wh_umur = "AND TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_terkonfirmasi.created_at,'%Y-%m-%d')) between '0' and '18'";
            }
        }

        $wh_jenis_kelamin = "";
        if ($jenis_kelamin) {
            $wh_jenis_kelamin = "AND jenis_kelamin = '" . $jenis_kelamin . "'";
        }

        $peserta = $this->peserta_model->query(
            "
            SELECT IFNULL(TIMESTAMPDIFF(YEAR, `tanggal_lahir`, DATE_FORMAT(a.created_at, '%Y-%m-%d')), 0) AS umur_kena_covid, `peserta`.*,trim(peserta.nama) AS nama, b.`nama_wilayah` AS nama_desa_kel, nama_puskesmas,c.nama_wilayah AS nama_kecamatan,DATE_FORMAT(trx_pemeriksaan.created_at,'%d-%m-%Y') AS tanggal_swab
            FROM `peserta` 
            INNER JOIN 
            (
                SELECT *
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                )
            ) AS a ON a.peserta_id=id_peserta
            JOIN `master_wilayah` AS b ON `id_master_wilayah`=`kelurahan_master_wilayah_id` 
            JOIN trx_terkonfirmasi ON id_trx_terkonfirmasi=a.trx_terkonfirmasi_id
            JOIN trx_pemeriksaan ON id_trx_pemeriksaan=trx_terkonfirmasi.trx_pemeriksaan_id
            LEFT JOIN master_wilayah AS c ON c.kode_wilayah=b.kode_induk
            LEFT JOIN master_puskesmas ON id_master_puskesmas=b.puskesmas_id
            WHERE a.`status` = '1' 
            AND DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '${start_date}' AND '${end_date}' ${wh_kecamatan} ${wh_pekerjaan} ${wh_jenis_kelamin} ${wh_umur}
            AND `peserta`.`deleted_at` IS NULL 
            GROUP BY `id_peserta`
            ORDER BY nama
            "
        )->result();

        // $peserta = $this->peserta_model->query(
        //     "
        //     SELECT IFNULL(TIMESTAMPDIFF(YEAR, `tanggal_lahir`, DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d')), 0) AS umur_kena_covid, `peserta`.*, b.`nama_wilayah` AS nama_desa_kel, nama_puskesmas,c.nama_wilayah AS nama_kecamatan
        //     FROM `peserta` 
        //     INNER JOIN trx_terkonfirmasi_rilis ON id_peserta=peserta_id
        //     JOIN `master_wilayah` AS b ON `id_master_wilayah`=`kelurahan_master_wilayah_id` 
        //     JOIN master_wilayah AS c ON c.kode_wilayah=b.kode_induk 
        //     LEFT JOIN master_puskesmas ON id_master_puskesmas=b.puskesmas_id
        //     WHERE `status` = '1' AND trx_terkonfirmasi_rilis.deleted_at IS NULL
        //     AND `peserta`.`deleted_at` IS NULL 
        //     GROUP BY `id_peserta`
        //     "
        // )->result();

        if ($versi_cetak == "pdf") {
            $this->cetak_pdf($peserta);
        } else if ($versi_cetak == "excel") {
            $this->cetak_excel($peserta);
        }
    }

    public function cetak_pdf($peserta)
    {
        $data['peserta'] = $peserta;
        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'orientation' => 'L', 'tempDir' => '/tmp']);
        $data = $this->load->view('cetak_peserta', $data, TRUE);
        $mpdf->WriteHTML($data);
        $mpdf->Output();
    }

    public function cetak_excel($peserta)
    {
        require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel.php');
        require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

        $filename = "Laporan.xlsx";

        $objPHPExcel = new PHPExcel();

        $style_header = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true
            ), 'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'dae1f3')
            )
        );

        $style_data = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "Nama");
        $objPHPExcel->getActiveSheet()->SetCellValue("B1", "NIK");
        $objPHPExcel->getActiveSheet()->SetCellValue("C1", "Tanggal Lahir");
        $objPHPExcel->getActiveSheet()->SetCellValue("D1", "Jenis Kelamin");
        $objPHPExcel->getActiveSheet()->SetCellValue("E1", "Pekerjaan");
        $objPHPExcel->getActiveSheet()->SetCellValue("F1", "Alamat KTP");
        $objPHPExcel->getActiveSheet()->SetCellValue("G1", "RT KTP");
        $objPHPExcel->getActiveSheet()->SetCellValue("H1", "Alamat Domisili");
        $objPHPExcel->getActiveSheet()->SetCellValue("I1", "RT Domisili");
        $objPHPExcel->getActiveSheet()->SetCellValue("J1", "Nomor Telepon");
        $objPHPExcel->getActiveSheet()->SetCellValue("K1", "Puskemas");
        $objPHPExcel->getActiveSheet()->SetCellValue("L1", "Desa/Kel");
        $objPHPExcel->getActiveSheet()->SetCellValue("M1", "Kecamatan");
        $objPHPExcel->getActiveSheet()->SetCellValue("N1", "Tanggal SWAB");
        $objPHPExcel->getActiveSheet()->SetCellValue("O1", "Kontak Erat");
        $objPHPExcel->getActiveSheet()->getStyle("A1:O1")->applyFromArray($style_header);

        if ($peserta) {
            $row = 2;
            foreach ($peserta as $key => $val) {
                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val->nama);
                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $val->nik);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                    "B" . $row,
                    $val->nik,
                    PHPExcel_Cell_DataType::TYPE_STRING
                );
                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, date("d/m/Y", strtotime($val->tanggal_lahir)));
                $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $val->jenis_kelamin);
                $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $val->pekerjaan_inti);
                $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);

                $data_master = $this->master_wilayah_model->get_by($val->kelurahan_master_wilayah_id);

                $data_kecamatan = $this->master_wilayah_model->get(
                    array(
                        "fields" => "nama_wilayah,kode_induk",
                        "where" => array(
                            "kode_wilayah" => $data_master->kode_induk
                        )
                    ),
                    "row"
                );

                $data_kabupaten = $this->master_wilayah_model->get(
                    array(
                        "fields" => "nama_wilayah,kode_induk",
                        "where" => array(
                            "kode_wilayah" => $data_kecamatan->kode_induk
                        )
                    ),
                    "row"
                );

                $data_provinsi = $this->master_wilayah_model->get(
                    array(
                        "fields" => "nama_wilayah,kode_induk",
                        "where" => array(
                            "kode_wilayah" => $data_kabupaten->kode_induk
                        )
                    ),
                    "row"
                );

                if ($val->master_rt_domisili_id) {
                    $get_data = $this->master_rt_model->get_by($val->master_rt_domisili_id);
                    $rt_domisili = $get_data->rt;
                } else {
                    $rt_domisili = $val->rt_domisili;
                }

                $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $val->alamat_ktp);
                $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);

                $objPHPExcel->getActiveSheet()->SetCellValue("G" . $row, $val->rt_ktp);
                $objPHPExcel->getActiveSheet()->getStyle("G" . $row)->applyFromArray($style_data);

                $objPHPExcel->getActiveSheet()->SetCellValue("H" . $row, $val->alamat_domisili);
                $objPHPExcel->getActiveSheet()->getStyle("H" . $row)->applyFromArray($style_data);

                $objPHPExcel->getActiveSheet()->SetCellValue("I" . $row, $rt_domisili);
                $objPHPExcel->getActiveSheet()->getStyle("I" . $row)->applyFromArray($style_data);

                $objPHPExcel->getActiveSheet()->SetCellValue("J" . $row, $val->nomor_telepon);
                $objPHPExcel->getActiveSheet()->getStyle("J" . $row)->applyFromArray($style_data);

                $objPHPExcel->getActiveSheet()->SetCellValue("K" . $row, $val->nama_puskesmas);
                $objPHPExcel->getActiveSheet()->getStyle("K" . $row)->applyFromArray($style_data);

                $objPHPExcel->getActiveSheet()->SetCellValue("L" . $row, $val->nama_desa_kel);
                $objPHPExcel->getActiveSheet()->getStyle("L" . $row)->applyFromArray($style_data);

                $objPHPExcel->getActiveSheet()->SetCellValue("M" . $row, $val->nama_kecamatan);
                $objPHPExcel->getActiveSheet()->getStyle("M" . $row)->applyFromArray($style_data);

                $objPHPExcel->getActiveSheet()->SetCellValue("N" . $row, $val->tanggal_swab);
                $objPHPExcel->getActiveSheet()->getStyle("N" . $row)->applyFromArray($style_data);

                $objPHPExcel->getActiveSheet()->SetCellValue("O" . $row, $val->kontak_erat);
                $objPHPExcel->getActiveSheet()->getStyle("O" . $row)->applyFromArray($style_data);

                $row++;
            }
        }

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $writer->save('php://output');
        exit;
    }
}
