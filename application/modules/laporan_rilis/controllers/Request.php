<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("peserta/master_wilayah_model", "master_wilayah_model");
        $this->load->model("peserta/peserta_model", "peserta_model");
        $this->load->model("trx_pemeriksaan_model");
    }

    public function get_peserta()
    {
        $kecamatan = decrypt_data($this->iget("kecamatan"));
        $desa_kel = decrypt_data($this->iget("desa_kel"));
        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");
        $umur = $this->iget("umur");
        $pekerjaan = $this->iget("pekerjaan");
        $jenis_kelamin = $this->iget("jenis_kelamin");

        $wh_kecamatan = "";
        if ($desa_kel) {
            $wh_kecamatan =
                "AND kelurahan_master_wilayah_id = '" . $desa_kel . "'";
        } elseif ($kecamatan) {
            $wh_kecamatan =
                "AND kelurahan_master_wilayah_id IN 
                (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    WHERE kode_induk = 
                    (
                        SELECT kode_wilayah
                        FROM master_wilayah
                        WHERE id_master_wilayah = '" . $kecamatan . "'
                    )
                )
            ";
        }

        $data['list_pekerjaan'] = array("1" => "Polisi", "2" => "TNI", "3" => "ASN", "4" => "BUMN / BANK", "5" => "Tenaga Kesehatan", "6" => "Dosen / Guru", "7" => "Pelajar/Mahasiswa", "8" => "Ibu Rumah Tangga", "9" => "Ulama / Pendeta /  Rohaniawan", "10" => "Swasta", "11" => "Wiraswasta", "12" => "Umum / Keluarga / Pensiunan / Unidentifi", "13" => "Lainnya");

        $wh_pekerjaan = "";
        if ($pekerjaan) {
            $wh_pekerjaan = "AND pekerjaan_inti = '" . $data['list_pekerjaan'][$pekerjaan] . "'";
        }

        $wh_umur = "";
        if ($umur) {
            if ($umur == '1') {
                $wh_umur = "AND TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_terkonfirmasi.created_at,'%Y-%m-%d')) between '0' and '5'";
            } elseif ($umur == '2') {
                $wh_umur = "AND TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_terkonfirmasi.created_at,'%Y-%m-%d')) between '6' and '11'";
            } elseif ($umur == '3') {
                $wh_umur = "AND TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_terkonfirmasi.created_at,'%Y-%m-%d')) between '12' and '25'";
            } elseif ($umur == '4') {
                $wh_umur = "AND TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_terkonfirmasi.created_at,'%Y-%m-%d')) between '26' and '45'";
            } elseif ($umur == '5') {
                $wh_umur = "AND TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_terkonfirmasi.created_at,'%Y-%m-%d')) between '46' and '65'";
            } elseif ($umur == '6') {
                $wh_umur = "AND TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_terkonfirmasi.created_at,'%Y-%m-%d')) between '66' and '200'";
            } elseif ($umur == '7') {
                $wh_umur = "AND TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_terkonfirmasi.created_at,'%Y-%m-%d')) between '0' and '18'";
            }
        }

        $wh_jenis_kelamin = "";
        if ($jenis_kelamin) {
            $wh_jenis_kelamin = "AND jenis_kelamin = '" . $jenis_kelamin . "'";
        }

        $peserta = $this->peserta_model->query(
            "
            SELECT IFNULL(TIMESTAMPDIFF(YEAR, `tanggal_lahir`, DATE_FORMAT(a.created_at, '%Y-%m-%d')), 0) AS umur_kena_covid, `peserta`.*, `nama_wilayah` 
            FROM `peserta` 
            INNER JOIN 
            (
                SELECT *
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                )
            ) AS a ON a.peserta_id=id_peserta
            JOIN `master_wilayah` ON `id_master_wilayah`=`kelurahan_master_wilayah_id` 
            JOIN trx_terkonfirmasi ON id_trx_terkonfirmasi=a.trx_terkonfirmasi_id
            WHERE a.`status` = '1' 
            AND DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '${start_date}' AND '${end_date}' ${wh_kecamatan} ${wh_pekerjaan} ${wh_jenis_kelamin} ${wh_umur}
            AND `peserta`.`deleted_at` IS NULL 
            GROUP BY `id_peserta`
            "
        )->result();

        $templist = array();
        foreach ($peserta as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_desa_kel()
    {
        $kecamatan = decrypt_data($this->iget("kecamatan"));

        $list_desa_kel = $this->master_wilayah_model->get(
            array(
                "fields" => "master_wilayah.*,CONCAT(klasifikasi,' - ',nama_wilayah) as nama_wilayah",
                "where_false" => "kode_induk = 
                (
                    SELECT kode_wilayah
                    FROM master_wilayah
                    WHERE id_master_wilayah = '" . $kecamatan . "'
                )",
                "order_by" => array(
                    "nama_wilayah" => "ASC"
                )
            )
        );

        $templist = array();
        foreach ($list_desa_kel as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
