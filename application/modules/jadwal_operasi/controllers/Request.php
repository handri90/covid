<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('jadwal_operasi_model');
        $this->load->model('pelanggaran_model');
    }

    public function get_jadwal_operasi()
    {
        $data_jadwal_operasi = $this->jadwal_operasi_model->get();

        $templist = array();
        foreach ($data_jadwal_operasi as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_jadwal_operasi);
            $templist[$key]['tanggal_operasi'] = longdate_indo($row->tanggal_operasi);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_detail_jadwal_operasi()
    {
        $id_jadwal_operasi = decrypt_data($this->iget("id_jadwal_operasi"));

        $data_pelanggaran = $this->pelanggaran_model->get(
            array(
                "where" => array(
                    "jadwal_operasi_id" => $id_jadwal_operasi
                )
            )
        );

        $templist = array();
        foreach ($data_pelanggaran as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_pelanggaran);
            $templist[$key]['tanggal_lahir'] = date_indo($row->tanggal_lahir);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_detail_pelanggaran()
    {
        $id_pelanggaran = decrypt_data($this->iget("id_pelanggaran"));

        $data = $this->pelanggaran_model->get(
            array(
                "fields" => "pelanggaran.*,DATE_FORMAT(tanggal_lahir,'%d/%m/%Y') as tanggal_lahir",
                "where" => array(
                    "id_pelanggaran" => $id_pelanggaran
                )
            ),
            "row"
        );

        $data->id_encrypt = encrypt_data($data->id_pelanggaran);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
