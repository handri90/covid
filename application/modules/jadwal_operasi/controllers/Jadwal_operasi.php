<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jadwal_operasi extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model("jadwal_operasi_model");
        $this->load->model("pelanggaran_model");
    }

    public function index()
    {
        $data['breadcrumb'] = [['link' => false, 'content' => 'Jadwal Operasi', 'is_active' => true]];
        if ($this->session->userdata("level_user_id") == "5") {
            if ($this->session->userdata("id_user") == "7") {
                $this->execute('index', $data);
            } else {
                $this->execute('index_pendamping', $data);
            }
        }
    }

    public function tambah_jadwal_operasi()
    {
        if (empty($_POST)) {
            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'Jadwal Operasi', 'content' => 'Jadwal Operasi', 'is_active' => false], ['link' => false, 'content' => 'Tambah Jadwal Operasi', 'is_active' => true]];
            $this->execute('form_jadwal_operasi', $data);
        } else {

            $date_exp = explode("/", $this->ipost('tanggal_operasi'));
            $tanggal = $date_exp[2] . "-" . $date_exp[1] . "-" . $date_exp[0];

            $data = array(
                "tanggal_operasi" => $tanggal,
                "petugas_ppns" => $this->ipost('petugas_ppns'),
                "nip_petugas_ppns" => $this->ipost('nip_petugas_ppns'),
                "petugas_kodim" => $this->ipost('petugas_kodim'),
                "petugas_polres" => $this->ipost('petugas_polres'),
                "is_approve_ppns" => "0",
                "is_approve_kodim" => "0",
                "is_approve_polres" => "0",
                'created_at' => $this->datetime()
            );

            $status = $this->jadwal_operasi_model->save($data);

            if ($status) {
                $this->session->set_flashdata('message', 'Data baru berhasil ditambahkan');
            } else {
                $this->session->set_flashdata('message', 'Data baru gagal ditambahkan');
            }

            redirect('jadwal_operasi');
        }
    }

    public function edit_jadwal_operasi($id_jadwal_operasi)
    {
        $data_master = $this->jadwal_operasi_model->get(
            array(
                "fields" => "jadwal_operasi.*,DATE_FORMAT(tanggal_operasi,'%d-%m-%y') as tanggal_operasi",
                "where" => array(
                    "id_jadwal_operasi" => decrypt_data($id_jadwal_operasi)
                )
            ),
            "row"
        );

        if (!$data_master) {
            $this->page_error();
        }

        if (empty($_POST)) {
            $data['content'] = $data_master;

            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'jadwal_operasi', 'content' => 'Jadwal Operasi', 'is_active' => false], ['link' => false, 'content' => 'Ubah Jadwal Operasi', 'is_active' => true]];
            $this->execute('form_jadwal_operasi', $data);
        } else {

            $date_exp = explode("/", $this->ipost('tanggal_operasi'));
            $tanggal = $date_exp[2] . "-" . $date_exp[1] . "-" . $date_exp[0];

            $data = array(
                "tanggal_operasi" => $tanggal,
                "petugas_ppns" => $this->ipost('petugas_ppns'),
                "nip_petugas_ppns" => $this->ipost('nip_petugas_ppns'),
                "petugas_kodim" => $this->ipost('petugas_kodim'),
                "petugas_polres" => $this->ipost('petugas_polres'),
                'updated_at' => $this->datetime()
            );

            $status = $this->jadwal_operasi_model->edit(decrypt_data($id_jadwal_operasi), $data);

            if ($status) {
                $this->session->set_flashdata('message', 'Data berhasil diubah');
            } else {
                $this->session->set_flashdata('message', 'Data gagal diubah');
            }

            redirect('jadwal_operasi');
        }
    }

    public function delete_jadwal_operasi()
    {
        $id_jadwal_operasi = $this->iget('id_jadwal_operasi');
        $data_master = $this->jadwal_operasi_model->get_by(decrypt_data($id_jadwal_operasi));

        if (!$data_master) {
            $this->page_error();
        }

        $data = array(
            "deleted_at" => $this->datetime()
        );

        $status = $this->jadwal_operasi_model->edit(decrypt_data($id_jadwal_operasi), $data);


        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function detail_jadwal_operasi($id_jadwal_operasi)
    {
        $data['id_jadwal_operasi'] = $id_jadwal_operasi;
        $data['content'] = $this->jadwal_operasi_model->get_by(decrypt_data($id_jadwal_operasi));
        $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'jadwal_operasi', 'content' => 'Jadwal Operasi', 'is_active' => false], ['link' => false, 'content' => 'Detail Jadwal Operasi', 'is_active' => true]];

        if ($this->session->userdata("level_user_id") == "5") {
            if ($this->session->userdata("id_user") == "7") {
                $this->execute('detail_jadwal_operasi', $data);
            } else {
                $this->execute('detail_jadwal_operasi_pendamping', $data);
            }
        }
    }

    public function action_form_pelanggaran()
    {

        $id_jadwal_operasi = decrypt_data($this->ipost("id_jadwal_operasi"));
        $id_data_pelanggar = decrypt_data($this->ipost("id_pelanggaran"));
        $nama = $this->ipost("nama");
        $nik = $this->ipost("nik");
        $tempat_lahir = $this->ipost("tempat_lahir");
        $tanggal_lahir = $this->ipost("tanggal_lahir");
        $alamat = $this->ipost("alamat");
        $pekerjaan = $this->ipost("pekerjaan");
        $tempat_kejadian = $this->ipost("tempat_kejadian");
        $pelanggaran = $this->ipost("pelanggaran");
        $jam = $this->ipost('jam');
        $menit = $this->ipost('menit');
        $sanksi = $this->ipost('sanksi');
        $sanksi_lain = $this->ipost('sanksi_lain');

        $jam_menit = $jam . ":" . $menit;

        $date_exp = explode("/", $tanggal_lahir);
        $tanggal = $date_exp[2] . "-" . $date_exp[1] . "-" . $date_exp[0];

        if ($id_data_pelanggar) {
            $data = array(
                "jadwal_operasi_id" => $id_jadwal_operasi,
                "nama" => $nama,
                "nik" => $nik,
                "tempat_lahir" => $tempat_lahir,
                "tanggal_lahir" => $tanggal,
                "alamat" => $alamat,
                "pekerjaan" => $pekerjaan,
                "tempat_kejadian" => $tempat_kejadian,
                "pelanggaran" => $pelanggaran,
                "jam" => $jam_menit,
                "sanksi" => $sanksi,
                "sanksi_lain" => ($sanksi_lain == "" ? NULL : $sanksi_lain),
                "updated_at" => $this->datetime()
            );
            $status = $this->pelanggaran_model->edit($id_data_pelanggar, $data);
        } else {
            $data = array(
                "jadwal_operasi_id" => $id_jadwal_operasi,
                "nama" => $nama,
                "nik" => $nik,
                "tempat_lahir" => $tempat_lahir,
                "tanggal_lahir" => $tanggal,
                "alamat" => $alamat,
                "pekerjaan" => $pekerjaan,
                "tempat_kejadian" => $tempat_kejadian,
                "pelanggaran" => $pelanggaran,
                "jam" => $jam_menit,
                "sanksi" => $sanksi,
                "sanksi_lain" => ($sanksi_lain == "" ? NULL : $sanksi_lain),
                "created_at" => $this->datetime()
            );

            $status = $this->pelanggaran_model->save($data);
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function delete_pelanggaran()
    {
        $id_pelanggaran = $this->iget('id_pelanggaran');
        $data_master = $this->pelanggaran_model->get_by(decrypt_data($id_pelanggaran));

        if (!$data_master) {
            $this->page_error();
        }

        $data = array(
            "deleted_at" => $this->datetime()
        );

        $status = $this->pelanggaran_model->edit(decrypt_data($id_pelanggaran), $data);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function approve_operasi()
    {
        $id_jadwal_operasi = $this->iget('id_jadwal_operasi');
        $data_master = $this->jadwal_operasi_model->get_by(decrypt_data($id_jadwal_operasi));

        if (!$data_master) {
            $this->page_error();
        }

        if ($this->session->userdata("id_user") == "8") {
            $data = array(
                "is_approve_ppns" => "1"
            );
        } else if ($this->session->userdata("id_user") == "9") {
            $data = array(
                "is_approve_kodim" => "1"
            );
        } else if ($this->session->userdata("id_user") == "10") {
            $data = array(
                "is_approve_polres" => "1"
            );
        }


        $status = $this->jadwal_operasi_model->edit(decrypt_data($id_jadwal_operasi), $data);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function not_approve_operasi()
    {
        $id_jadwal_operasi = $this->iget('id_jadwal_operasi');
        $data_master = $this->jadwal_operasi_model->get_by(decrypt_data($id_jadwal_operasi));

        if (!$data_master) {
            $this->page_error();
        }

        if ($this->session->userdata("id_user") == "8") {
            $data = array(
                "is_approve_ppns" => "0"
            );
        } else if ($this->session->userdata("id_user") == "9") {
            $data = array(
                "is_approve_kodim" => "0"
            );
        } else if ($this->session->userdata("id_user") == "10") {
            $data = array(
                "is_approve_polres" => "0"
            );
        }


        $status = $this->jadwal_operasi_model->edit(decrypt_data($id_jadwal_operasi), $data);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function cetak_pelanggaran($id_jadwal_operasi)
    {
        $data_master = $this->jadwal_operasi_model->get_by(decrypt_data($id_jadwal_operasi));

        if (!$data_master || ($data_master->is_approve_ppns == "0" || $data_master->is_approve_kodim == "0" || $data_master->is_approve_polres == "0")) {
            redirect("jadwal_operasi/detail_jadwal_operasi/" . $id_jadwal_operasi);
        }
    }
}
