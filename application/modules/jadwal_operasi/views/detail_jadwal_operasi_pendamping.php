<style>
    .header_tbl {
        width: 15%;
    }

    .header_tbl2 {
        width: 1%;
    }

    .text-right-approve {
        margin-top: 10px;
        text-align: right !important;
    }
</style>
<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="card card-table table-responsive shadow-0 mb-0">
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="header_tbl">Jadwal Operasi</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? longdate_indo($content->tanggal_operasi) : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Nama Petugas PPNS</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->petugas_ppns . " (NIP : " . $content->nip_petugas_ppns . ")" : ""; ?>&nbsp;<?php echo isset($content) ? ($content->is_approve_ppns == "1" ? "<span class='badge badge-success'>Sudah Disetujui</span>" : "<span class='badge badge-danger'>Belum Disetujui</span>") : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Petugas KODIM</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->petugas_kodim : ""; ?>&nbsp;<?php echo isset($content) ? ($content->is_approve_kodim == "1" ? "<span class='badge badge-success'>Sudah Disetujui</span>" : "<span class='badge badge-danger'>Belum Disetujui</span>") : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Petugas POLRES</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->petugas_polres : ""; ?>&nbsp;<?php echo isset($content) ? ($content->is_approve_polres == "1" ? "<span class='badge badge-success'>Sudah Disetujui</span>" : "<span class='badge badge-danger'>Belum Disetujui</span>") : ""; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="text-right-approve">
                <?php
                if ($this->session->userdata("id_user") == "8") {
                    if (isset($content) && $content->is_approve_ppns == "0") {
                ?>
                        <a href="#setuju" id="setuju" class="btn btn-info">Setuju</a>
                    <?php
                    } else {
                    ?>
                        <a href="#batalkan" id="batalkan" class="btn btn-info">Batalkan</a>
                    <?php
                    }
                } else if ($this->session->userdata("id_user") == "9") {
                    if (isset($content) && $content->is_approve_kodim == "0") {
                    ?>
                        <a href="#setuju" id="setuju" class="btn btn-info">Setuju</a>
                    <?php
                    } else {
                    ?>
                        <a href="#batalkan" id="batalkan" class="btn btn-info">Batalkan</a>
                    <?php
                    }
                } else if ($this->session->userdata("id_user") == "10") {
                    if (isset($content) && $content->is_approve_polres == "0") {
                    ?>
                        <a href="#setuju" id="setuju" class="btn btn-info">Setuju</a>
                    <?php
                    } else {
                    ?>
                        <a href="#batalkan" id="batalkan" class="btn btn-info">Batalkan</a>
                <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <input type="hidden" name="id_jadwal_operasi" value="<?php echo !empty($id_jadwal_operasi) ? $id_jadwal_operasi : ""; ?>" />
                <?php
                if (isset($content)) {
                    if ($content->is_approve_ppns == "1" && $content->is_approve_kodim == "1" && $content->is_approve_polres == "1") {
                ?>
                        <a href="#cetakPelanggaran" id="cetakPelanggaran" class="btn bg-slate-400">Cetak</a>
                <?php
                    }
                }
                ?>
            </div>
        </div>
        <table id="datatablePelanggaran" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>NIK</th>
                    <th>Tempat/Tanggal Lahir</th>
                    <th>Alamat</th>
                    <th>Pekerjaan</th>
                    <th>Tempat Kejadian</th>
                    <th>Pelanggaran</th>
                    <th>Jam</th>
                    <th>Sanksi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="modalPelanggaran" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Pelanggaran</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <input type="hidden" name="id_pelanggaran" />
            </div>

            <div class="modal-body">
                <span class="alert_form"></span>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Jam <span class="text-danger">*</span></label>
                    <input type="hidden" class="form-control" name="id_pelanggaran">
                    <div class="col-lg-2">
                        <select class="form-control" name="jam">
                        </select>
                    </div>
                    :
                    <div class="col-lg-2">
                        <select class="form-control" name="menit">
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Nama <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="nama" placeholder="Nama">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Nomor KTP <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="number" class="form-control" name="nik" placeholder="Nomor KTP">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Tempat Lahir <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat Lahir">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Tanggal Lahir <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control daterange-single" name="tanggal_lahir" placeholder="Tanggal Lahir">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Alamat <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <textarea name="alamat" class="form-control" id="" cols="30" rows="2"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Pekerjaan <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <span class="list_pekerjaan"></span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Tempat Kejadian <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <textarea name="tempat_kejadian" class="form-control" id="" cols="30" rows="2"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Pelanggaran <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <textarea name="pelanggaran" class="form-control" id="" cols="30" rows="2"></textarea>
                    </div>
                </div>

                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Sanksi <span class="text-danger">*</span></legend>

                    <span class="list_sanksi"></span>
                </fieldset>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary" onclick="action_form_pelanggaran()">Simpan <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.daterange-single').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD/MM/YYYY'
        },
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10)
    });

    let datatablePelanggaran = $("#datatablePelanggaran").DataTable({
        "columns": [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
        ]
    });

    get_detail_pelanggaran();

    function get_detail_pelanggaran() {
        let id_jadwal_operasi = $("input[name='id_jadwal_operasi']").val();

        datatablePelanggaran.clear().draw();
        $.ajax({
            url: base_url + 'jadwal_operasi/request/get_detail_jadwal_operasi',
            data: {
                id_jadwal_operasi: id_jadwal_operasi
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let sanksi = [];
                sanksi[1] = "Teguran Tertulis";
                sanksi[2] = "Kerja Sosial";
                sanksi[3] = "Penghentian Sementara berupa penyegelan";
                sanksi[4] = "Denda Administrasi";
                sanksi[5] = "Pencabutan Izin";
                sanksi[6] = "Pengamanan KTP";
                sanksi[7] = "Pengamanan Barang yang terkait dengan pelanggaran";

                $.each(response, function(index, value) {
                    datatablePelanggaran.row.add([
                        value.nama,
                        value.nik,
                        value.tempat_lahir + ", " + value.tanggal_lahir,
                        value.alamat,
                        value.pekerjaan,
                        value.tempat_kejadian,
                        value.pelanggaran,
                        value.jam,
                        sanksi[value.sanksi] + (value.sanksi == "2" || value.sanksi == "4" ? " (" + value.sanksi_lain + ")" : "")
                    ]).draw(false);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    $(function() {
        $("#setuju").on("click", function() {
            let id_jadwal_operasi = $("input[name='id_jadwal_operasi']").val();

            $.ajax({
                url: base_url + 'jadwal_operasi/approve_operasi',
                data: {
                    id_jadwal_operasi: id_jadwal_operasi
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    location.reload();
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        });

        $("#batalkan").on("click", function() {
            let id_jadwal_operasi = $("input[name='id_jadwal_operasi']").val();

            $.ajax({
                url: base_url + 'jadwal_operasi/not_approve_operasi',
                data: {
                    id_jadwal_operasi: id_jadwal_operasi
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    location.reload();
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        });
    });
</script>