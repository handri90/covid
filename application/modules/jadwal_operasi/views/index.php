<div class="content">
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url() . 'jadwal_operasi/tambah_jadwal_operasi'; ?>" class="btn btn-info">Tambah Jadwal Operasi</a>
            </div>
        </div>
        <table id="datatableJadwalOperasi" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Petugas PPNS</th>
                    <th>Petugas KODIM</th>
                    <th>Petugas POLRES</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    let datatableJadwalOperasi = $("#datatableJadwalOperasi").DataTable();

    get_jadwal_operasi();

    function get_jadwal_operasi() {

        datatableJadwalOperasi.clear().draw();
        $.ajax({
            url: base_url + 'jadwal_operasi/request/get_jadwal_operasi',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    datatableJadwalOperasi.row.add([
                        value.tanggal_operasi,
                        value.petugas_ppns + " (NIP : " + value.nip_petugas_ppns + ")",
                        value.petugas_kodim,
                        value.petugas_polres,
                        "<a href='" + base_url + "jadwal_operasi/edit_jadwal_operasi/" + value.id_encrypt + "' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a> <a href='" + base_url + "jadwal_operasi/detail_jadwal_operasi/" + value.id_encrypt + "' class='btn btn-primary btn-icon'><i class='icon-eye'></i></a>"
                    ]).draw(false);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function confirm_delete(id_jadwal_operasi) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'jadwal_operasi/delete_jadwal_operasi',
                    data: {
                        id_jadwal_operasi: id_jadwal_operasi
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_jadwal_operasi();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_jadwal_operasi();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_jadwal_operasi();
                    }
                });
            }
        });
    }
</script>