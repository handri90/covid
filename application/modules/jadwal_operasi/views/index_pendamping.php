<div class="content">
    <div class="card card-table">
        <table id="datatableJadwalOperasi" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Petugas PPNS</th>
                    <th>Petugas KODIM</th>
                    <th>Petugas POLRES</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    let datatableJadwalOperasi = $("#datatableJadwalOperasi").DataTable();

    get_jadwal_operasi();

    function get_jadwal_operasi() {

        datatableJadwalOperasi.clear().draw();
        $.ajax({
            url: base_url + 'jadwal_operasi/request/get_jadwal_operasi',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    datatableJadwalOperasi.row.add([
                        value.tanggal_operasi,
                        value.petugas_ppns + " (NIP : " + value.nip_petugas_ppns + ")",
                        value.petugas_kodim,
                        value.petugas_polres,
                        "<a href='" + base_url + "jadwal_operasi/detail_jadwal_operasi/" + value.id_encrypt + "' class='btn btn-primary btn-icon'><i class='icon-eye'></i></a>"
                    ]).draw(false);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }
</script>