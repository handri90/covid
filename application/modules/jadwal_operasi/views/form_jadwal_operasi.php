<div class="content">
    <!-- Form inputs -->
    <div class="card">
        <div class="card-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-validate-jquery')); ?>
            <fieldset class="mb-3">
                <legend class="text-uppercase font-size-sm font-weight-bold">Jadwal Operasi</legend>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Tanggal <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control daterange-single" readonly value="<?php echo !empty($content) ? $content->tanggal_operasi : ""; ?>" name="tanggal_operasi" required placeholder="Tanggal">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nama Petugas PPNS <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->petugas_ppns : ""; ?>" name="petugas_ppns" required placeholder="Nama Petugas PPNS">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">NIP Petugas PPNS <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->nip_petugas_ppns : ""; ?>" name="nip_petugas_ppns" required placeholder="NIP Petugas PPNS">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nama Petugas KODIM <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->petugas_kodim : ""; ?>" name="petugas_kodim" required placeholder="Nama Petugas KODIM">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nama Petugas POLRES <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->petugas_polres : ""; ?>" name="petugas_polres" required placeholder="Nama Petugas POLRES">
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <!-- /form inputs -->
</div>

<script>
    $('.daterange-single').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD/MM/YYYY'
        },
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10)
    });
</script>