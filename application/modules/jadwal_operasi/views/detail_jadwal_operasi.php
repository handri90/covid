<style>
    .header_tbl {
        width: 15%;
    }

    .header_tbl2 {
        width: 1%;
    }
</style>
<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="card card-table table-responsive shadow-0 mb-0">
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="header_tbl">Jadwal Operasi</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? longdate_indo($content->tanggal_operasi) : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Nama Petugas PPNS</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->petugas_ppns . " (NIP : " . $content->nip_petugas_ppns . ")" : ""; ?>&nbsp;<?php echo isset($content) ? ($content->is_approve_ppns == "1" ? "<span class='badge badge-success'>Sudah Disetujui</span>" : "<span class='badge badge-danger'>Belum Disetujui</span>") : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Petugas KODIM</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->petugas_kodim : ""; ?>&nbsp;<?php echo isset($content) ? ($content->is_approve_kodim == "1" ? "<span class='badge badge-success'>Sudah Disetujui</span>" : "<span class='badge badge-danger'>Belum Disetujui</span>") : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Petugas POLRES</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->petugas_polres : ""; ?>&nbsp;<?php echo isset($content) ? ($content->is_approve_polres == "1" ? "<span class='badge badge-success'>Sudah Disetujui</span>" : "<span class='badge badge-danger'>Belum Disetujui</span>") : ""; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <input type="hidden" name="id_jadwal_operasi" value="<?php echo !empty($id_jadwal_operasi) ? $id_jadwal_operasi : ""; ?>" />
                <?php
                if (isset($content)) {
                    if ($content->is_approve_ppns == "1" && $content->is_approve_kodim == "1" && $content->is_approve_polres == "1") {
                ?>

                        <a target="_blank" href="<?php echo base_url(); ?>jadwal_operasi/cetak_pelanggaran/<?php echo !empty($id_jadwal_operasi) ? $id_jadwal_operasi : ""; ?>" id="cetakPelanggaran" class="btn bg-slate-400">Cetak</a>
                <?php
                    }
                }
                ?>
                <a href="#tambahPelanggaran" id="tambahPelanggaran" class="btn btn-info">Tambah Pelanggaran</a>
            </div>
        </div>
        <table id="datatablePelanggaran" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>NIK</th>
                    <th>Tempat/Tanggal Lahir</th>
                    <th>Alamat</th>
                    <th>Pekerjaan</th>
                    <th>Tempat Kejadian</th>
                    <th>Pelanggaran</th>
                    <th>Jam</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="modalPelanggaran" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Pelanggaran</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <input type="hidden" name="id_pelanggaran" />
            </div>

            <div class="modal-body">
                <span class="alert_form"></span>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Jam <span class="text-danger">*</span></label>
                    <input type="hidden" class="form-control" name="id_pelanggaran">
                    <div class="col-lg-2">
                        <select class="form-control" name="jam">
                        </select>
                    </div>
                    :
                    <div class="col-lg-2">
                        <select class="form-control" name="menit">
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Nama <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="nama" placeholder="Nama">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Nomor KTP <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="number" class="form-control" name="nik" placeholder="Nomor KTP">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Tempat Lahir <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat Lahir">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Tanggal Lahir <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control daterange-single" name="tanggal_lahir" placeholder="Tanggal Lahir">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Alamat <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <textarea name="alamat" class="form-control" id="" cols="30" rows="2"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Pekerjaan <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <span class="list_pekerjaan"></span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Tempat Kejadian <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <textarea name="tempat_kejadian" class="form-control" id="" cols="30" rows="2"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Pelanggaran <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <textarea name="pelanggaran" class="form-control" id="" cols="30" rows="2"></textarea>
                    </div>
                </div>

                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Sanksi <span class="text-danger">*</span></legend>

                    <span class="list_sanksi"></span>
                </fieldset>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary" onclick="action_form_pelanggaran()">Simpan <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.daterange-single').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD/MM/YYYY'
        },
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10)
    });

    let datatablePelanggaran = $("#datatablePelanggaran").DataTable({
        "columns": [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            {
                "width": "10%"
            }
        ]
    });

    get_detail_pelanggaran();

    function get_detail_pelanggaran() {
        let id_jadwal_operasi = $("input[name='id_jadwal_operasi']").val();

        datatablePelanggaran.clear().draw();
        $.ajax({
            url: base_url + 'jadwal_operasi/request/get_detail_jadwal_operasi',
            data: {
                id_jadwal_operasi: id_jadwal_operasi
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    datatablePelanggaran.row.add([
                        value.nama,
                        value.nik,
                        value.tempat_lahir + ", " + value.tanggal_lahir,
                        value.alamat,
                        value.pekerjaan,
                        value.tempat_kejadian,
                        value.pelanggaran,
                        value.jam,
                        "<a href='#editDetailPeserta' onClick=\"show_detail_pelanggaran('" + value.id_encrypt + "')\" class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>"
                    ]).draw(false);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function confirm_delete(id_pelanggaran) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'jadwal_operasi/delete_pelanggaran',
                    data: {
                        id_pelanggaran: id_pelanggaran
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_detail_pelanggaran();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_detail_pelanggaran();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_detail_pelanggaran();
                    }
                });
            }
        });
    }

    function action_form_pelanggaran() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let id_jadwal_operasi = $("input[name='id_jadwal_operasi']").val();
        let id_pelanggaran = $("input[name='id_pelanggaran']").val();
        let nama = $("input[name='nama']").val();
        let nik = $("input[name='nik']").val();
        let tempat_lahir = $("input[name='tempat_lahir']").val();
        let tanggal_lahir = $("input[name='tanggal_lahir']").val();
        let jam = $("select[name='jam']").val();
        let menit = $("select[name='menit']").val();
        let alamat = $("textarea[name='alamat']").val();
        let pekerjaan = $("input[name='pekerjaan']:checked").val();
        let tempat_kejadian = $("textarea[name='tempat_kejadian']").val();
        let pelanggaran = $("textarea[name='pelanggaran']").val();
        let sanksi = $("input[name='sanksi']:checked").val();
        let sanksi_lain = "";
        if (sanksi == "2" || sanksi == "4") {
            sanksi_lain = $("input[name='sanksi_lain']").val();
        }

        if (!nama) {
            $(".alert_form").html("<div class='alert alert-danger'>Nama tidak boleh kosong.</div>");
        } else if (!nik) {
            $(".alert_form").html("<div class='alert alert-danger'>Nik tidak boleh kosong.</div>");
        } else if (!tempat_lahir) {
            $(".alert_form").html("<div class='alert alert-danger'>Tempat Lahir tidak boleh kosong.</div>");
        } else if (!tanggal_lahir) {
            $(".alert_form").html("<div class='alert alert-danger'>Tanggal Lahir tidak boleh kosong.</div>");
        } else if (!alamat) {
            $(".alert_form").html("<div class='alert alert-danger'>Alamat tidak boleh kosong.</div>");
        } else if (!pekerjaan) {
            $(".alert_form").html("<div class='alert alert-danger'>Pekerjaan tidak boleh kosong.</div>");
        } else if (!tempat_kejadian) {
            $(".alert_form").html("<div class='alert alert-danger'>Tempat Kejadian tidak boleh kosong.</div>");
        } else if (!pelanggaran) {
            $(".alert_form").html("<div class='alert alert-danger'>Pelanggaran tidak boleh kosong.</div>");
        } else if (typeof(sanksi) === 'undefined') {
            $(".alert_form").html("<div class='alert alert-danger'>Sanksi tidak boleh kosong.</div>");
        } else if (sanksi == "2" && !sanksi_lain) {
            $(".alert_form").html("<div class='alert alert-danger'>Inputan Sanksi Kerja Sosial tidak boleh kosong.</div>");
        } else if (sanksi == "4" && !sanksi_lain) {
            $(".alert_form").html("<div class='alert alert-danger'>Inputan Sanksi Denda Administrasi tidak boleh kosong.</div>");
        } else {
            $(".alert_form").html("");
            $.ajax({
                url: base_url + 'jadwal_operasi/action_form_pelanggaran',
                data: {
                    id_jadwal_operasi: id_jadwal_operasi,
                    id_pelanggaran: id_pelanggaran,
                    jam: jam,
                    menit: menit,
                    nama: nama,
                    nik: nik,
                    tempat_lahir: tempat_lahir,
                    tanggal_lahir: tanggal_lahir,
                    alamat: alamat,
                    pekerjaan: pekerjaan,
                    tempat_kejadian: tempat_kejadian,
                    pelanggaran: pelanggaran,
                    sanksi: sanksi,
                    sanksi_lain: sanksi_lain
                },
                type: 'POST',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    if (response) {
                        $("#modalPelanggaran").modal("toggle");
                        get_detail_pelanggaran();
                        swalInit(
                            'Berhasil',
                            'Data berhasil ditambahkan',
                            'success'
                        );
                    } else {
                        $("#modalPelanggaran").modal("toggle");
                        get_detail_pelanggaran();
                        swalInit(
                            'Gagal',
                            'Data tidak bisa ditambahkan',
                            'error'
                        );
                    }
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }

    function show_detail_pelanggaran(id_pelanggaran) {
        $("input[name='id_pelanggaran']").val("");
        $("input[name='nama']").val("");
        $("input[name='nik']").val("");
        $("input[name='tempat_lahir']").val("");
        $("input[name='tanggal_lahir']").val(moment().format('DD/MM/YYYY'));
        jam_menit("00:00");
        $("textarea[name='alamat']").val("");
        $("input[name='pekerjaan']").val("");
        $("textarea[name='tempat_kejadian']").val("");
        $("textarea[name='pelanggaran']").val("");
        show_pekerjaan();
        show_sanksi();

        $.ajax({
            url: base_url + 'jadwal_operasi/request/get_detail_pelanggaran',
            data: {
                id_pelanggaran: id_pelanggaran
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $("#modalPelanggaran").modal("show");
                $("input[name='id_pelanggaran']").val(response.id_encrypt);
                $("input[name='nama']").val(response.nama);
                $("input[name='nik']").val(response.nik);
                $("input[name='tempat_lahir']").val(response.tempat_lahir);
                $("input[name='tanggal_lahir']").val(response.tanggal_lahir);
                jam_menit(response.jam);
                $("textarea[name='alamat']").val(response.alamat);
                $("textarea[name='tempat_kejadian']").val(response.tempat_kejadian);
                $("textarea[name='pelanggaran']").val(response.pelanggaran);
                show_sanksi(response.sanksi, response.sanksi_lain);
                show_other_input(response.sanksi, response.sanksi_lain)
                show_pekerjaan(response.pekerjaan);
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function jam_menit(jam_menit) {
        let jam = "";
        let menit = "";
        let split_jam_menit = "";
        if (jam_menit) {
            split_jam_menit = jam_menit.split(":");
            jam = split_jam_menit[0];
            menit = split_jam_menit[1];
        }

        let jam_html = "";
        for (let i = 0; i < 24; i++) {
            let selected = "";
            if (jam) {
                if (i == parseInt(jam)) {
                    selected = "selected";
                }
            }
            jam_html += "<option " + selected + " value='" + (i < 10 ? "0" : "") + i + "'>" + (i < 10 ? "0" : "") + i + "</option>";
        }

        let minute_html = "";
        for (let i = 0; i < 60; i++) {
            let selected = "";
            if (menit) {
                if (i == parseInt(menit)) {
                    selected = "selected";
                }
            }
            minute_html += "<option " + selected + " value='" + (i < 10 ? "0" : "") + i + "'>" + (i < 10 ? "0" : "") + i + "</option>";
        }

        $("select[name='jam']").html(jam_html);
        $("select[name='menit']").html(minute_html);
    }

    function show_sanksi(sanksi_db, sanksi_lain) {
        let sanksi = [];
        sanksi[1] = "Teguran Tertulis";
        sanksi[2] = "Kerja Sosial";
        sanksi[3] = "Penghentian Sementara berupa penyegelan";
        sanksi[4] = "Denda Administrasi";
        sanksi[5] = "Pencabutan Izin";
        sanksi[6] = "Pengamanan KTP";
        sanksi[7] = "Pengamanan Barang yang terkait dengan pelanggaran";

        let html = "";
        $.each(sanksi, function(index, value) {
            if (index != 0) {
                let checked = "";
                if (index == sanksi_db) {
                    checked = "checked";
                }
                html += "<div class='form-group row'>" +
                    "<div class='form-check'>" +
                    "<label class='form-check-label'>" +
                    "<input " + checked + " onclick='show_other_input()' type='radio' class='form-check-input' name='sanksi' value='" + index + "'>" +
                    value +
                    "</label>" +
                    "<span class='place_input_sanksi place_input_sanksi_" + index + "'></span>" +
                    "</div>" +
                    "</div>";
            }
        });

        $(".list_sanksi").html(html);
    }

    function show_pekerjaan(pekerjaan_db) {
        let pekerjaan = [];
        pekerjaan[1] = "Wiraswasta";
        pekerjaan[2] = "Swasta";
        pekerjaan[3] = "ASN";
        pekerjaan[4] = "TNI/POLRI";
        pekerjaan[5] = "Pelajar/Mahasiswa";
        pekerjaan[6] = "Tenaga Kesehatan";
        pekerjaan[7] = "Lainnya";

        let html = "";
        $.each(pekerjaan, function(index, value) {
            if (index != 0) {
                let checked = "";
                if (value == pekerjaan_db) {
                    checked = "checked";
                }
                html += "<div class='form-group row'>" +
                    "<div class='form-check'>" +
                    "<label class='form-check-label'>" +
                    "<input " + checked + " type='radio' class='form-check-input' name='pekerjaan' value='" + value + "'>" +
                    value +
                    "</label>" +
                    "</div>" +
                    "</div>";
            }
        });

        $(".list_pekerjaan").html(html);
    }

    function show_other_input(sanksi_db, sanksi_lain = "") {
        $(".place_input_sanksi").html("");
        let sanksi = $("input[name='sanksi']:checked").val();

        if (sanksi_db) {
            sanksi = sanksi_db;
        }

        if (sanksi == "2") {
            $(".place_input_sanksi_" + sanksi).html("<div> Berupa <input value='" + sanksi_lain + "' type='text' name='sanksi_lain' />");
        } else if (sanksi == "4") {
            $(".place_input_sanksi_" + sanksi).html("<div> Sebesar <input value='" + sanksi_lain + "' type='text' name='sanksi_lain' />");
        }
    }

    $(function() {
        $("#tambahPelanggaran").on("click", function() {
            $("#modalPelanggaran").modal("show");
            show_sanksi();
            $("input[name='id_pelanggaran']").val("");
            $("input[name='nama']").val("");
            $("input[name='nik']").val("");
            $("input[name='tempat_lahir']").val("");
            $("input[name='tanggal_lahir']").val(moment().format('DD/MM/YYYY'));
            jam_menit("00:00");
            $("textarea[name='alamat']").val("");
            $("input[name='pekerjaan']").val("");
            $("textarea[name='tempat_kejadian']").val("");
            $("textarea[name='pelanggaran']").val("");
            show_pekerjaan();
        });
    })
</script>