<?php

class Jadwal_operasi_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "jadwal_operasi";
        $this->primary_id = "id_jadwal_operasi";
    }
}
