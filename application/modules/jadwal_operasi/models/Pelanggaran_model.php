<?php

class Pelanggaran_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "pelanggaran";
        $this->primary_id = "id_pelanggaran";
    }
}
