<?php

class Trx_vaksinasi_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "trx_vaksinasi";
        $this->primary_id = "id_trx_vaksinasi";
    }
}
