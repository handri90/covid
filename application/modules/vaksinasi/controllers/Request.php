<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('trx_vaksinasi_model');
        $this->load->model('peserta_vaksinasi/peserta_vaksinasi_model', 'peserta_vaksinasi_model');
    }

    public function get_vaksinasi()
    {
        $wh = array();
        if ($this->session->userdata("level_user_id") == "2") {
            $wh = array(
                "dosis_tahap" => $this->config->item("dosis_tahap"),
                "peserta_vaksinasi.id_user_created" => $this->session->userdata("id_user")
            );
        } else {
            $wh = array(
                "dosis_tahap" => $this->config->item("dosis_tahap")
            );
        }

        $data_peserta = $this->trx_vaksinasi_model->get(
            array(
                "fields" => "trx_vaksinasi.*,nama,DATE_FORMAT(tanggal_screening,'%Y-%m-%d') as tanggal_screening,DATE_FORMAT(tanggal_vaksinasi,'%Y-%m-%d') as tanggal_vaksinasi",
                "join" => array(
                    "peserta_vaksinasi" => "id_peserta_vaksinasi=peserta_vaksinasi_id AND peserta_vaksinasi.deleted_at IS NULL"
                ),
                "where" => $wh
            )
        );

        $templist = array();
        foreach ($data_peserta as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_trx_vaksinasi);
            $templist[$key]['tanggal_screening'] = date_indo($row->tanggal_screening);
            $templist[$key]['tanggal_vaksinasi'] = date_indo($row->tanggal_vaksinasi);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
