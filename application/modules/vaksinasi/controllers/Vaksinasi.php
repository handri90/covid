<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Vaksinasi extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model('trx_vaksinasi_model');
        $this->load->model('peserta_vaksinasi/peserta_vaksinasi_model', 'peserta_vaksinasi_model');
    }

    public function index()
    {
        $data['breadcrumb'] = [['link' => false, 'content' => 'Vaksinasi', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function tambah_vaksinasi()
    {
        if (empty($_POST)) {
            $wh = array();
            if ($this->session->userdata("level_user_id") == "2") {
                $wh = array(
                    "peserta_vaksinasi.id_user_created" => $this->session->userdata("id_user")
                );
            }

            $data['list_peserta_vaksinasi'] = $this->peserta_vaksinasi_model->get(
                array(
                    "where" => $wh,
                    "where_false" => "id_peserta_vaksinasi NOT IN (SELECT peserta_vaksinasi_id FROM trx_vaksinasi WHERE dosis_tahap = '" . $this->config->item('dosis_tahap') . "' AND trx_vaksinasi.deleted_at IS NULL)"
                )
            );

            $data['list_keterangan'] = array("Ditunda", "Lanjut", "Tidak Diberikan");

            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'vaksinasi', 'content' => 'Vaksinasi', 'is_active' => false], ['link' => false, 'content' => 'Tambah Vaksinasi', 'is_active' => true]];

            $this->execute('form_vaksinasi', $data);
        } else {

            $date_exp = explode("/", $this->ipost('tanggal_vaksinasi'));
            $tanggal_vaksinasi = $date_exp[2] . "-" . $date_exp[1] . "-" . $date_exp[0];

            $date_exp = explode("/", $this->ipost('tanggal_screening'));
            $tanggal_screening = $date_exp[2] . "-" . $date_exp[1] . "-" . $date_exp[0];

            $data = array(
                "peserta_vaksinasi_id" => decrypt_data($this->ipost('peserta_vaksinasi_id')),
                "no_register" => $this->ipost('no_register'),
                "tanggal_vaksinasi" => $tanggal_vaksinasi,
                "no_batch" => $this->ipost('no_batch'),
                "tanggal_screening" => $tanggal_screening,
                "dosis_tahap" => $this->config->item("dosis_tahap"),
                "keterangan" => $this->ipost('keterangan'),
                'created_at' => $this->datetime(),
                'id_user_created' => $this->session->userdata("id_user")
            );

            $status = $this->trx_vaksinasi_model->save($data);

            if ($status) {
                $this->session->set_flashdata('message', 'Data baru berhasil ditambahkan');
            } else {
                $this->session->set_flashdata('message', 'Data baru gagal ditambahkan');
            }

            redirect('vaksinasi');
        }
    }

    public function edit_vaksinasi($id_vaksinasi)
    {
        $data_master = $this->trx_vaksinasi_model->get(
            array(
                "fields" => "trx_vaksinasi.*,DATE_FORMAT(tanggal_screening,'%d-%m-%Y') AS tanggal_screening,DATE_FORMAT(tanggal_vaksinasi,'%d-%m-%Y') AS tanggal_vaksinasi",
                "where" => array(
                    "id_trx_vaksinasi" => decrypt_data($id_vaksinasi)
                )
            ),
            "row"
        );

        if (!$data_master) {
            $this->page_error();
        }

        $wh = array();
        if ($this->session->userdata("level_user_id") == "2") {
            $wh = array(
                "peserta_vaksinasi.id_user_created" => $this->session->userdata("id_user")
            );
        }

        $data['list_peserta_vaksinasi'] = $this->peserta_vaksinasi_model->get(
            array(
                "where" => $wh,
                "where_false" => "id_peserta_vaksinasi NOT IN (SELECT peserta_vaksinasi_id FROM trx_vaksinasi WHERE dosis_tahap = " . $this->config->item('dosis_tahap') . " AND peserta_vaksinasi_id != '" . $data_master->peserta_vaksinasi_id . "')"
            )
        );

        $data['list_keterangan'] = array("Ditunda", "Lanjut", "Tidak Diberikan");


        if (empty($_POST)) {
            $data['content'] = $data_master;

            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'vaksinasi', 'content' => 'Vaksinasi', 'is_active' => false], ['link' => false, 'content' => 'Ubah Vaksinasi', 'is_active' => true]];
            $this->execute('form_vaksinasi', $data);
        } else {

            $date_exp = explode("/", $this->ipost('tanggal_vaksinasi'));
            $tanggal_vaksinasi = $date_exp[2] . "-" . $date_exp[1] . "-" . $date_exp[0];

            $date_exp = explode("/", $this->ipost('tanggal_screening'));
            $tanggal_screening = $date_exp[2] . "-" . $date_exp[1] . "-" . $date_exp[0];

            $data = array(
                "peserta_vaksinasi_id" => decrypt_data($this->ipost('peserta_vaksinasi_id')),
                "no_register" => $this->ipost('no_register'),
                "tanggal_vaksinasi" => $tanggal_vaksinasi,
                "no_batch" => $this->ipost('no_batch'),
                "tanggal_screening" => $tanggal_screening,
                "dosis_tahap" => $this->config->item("dosis_tahap"),
                "keterangan" => $this->ipost('keterangan'),
                'updated_at' => $this->datetime(),
                'id_user_updated' => $this->session->userdata("id_user")
            );

            $status = $this->trx_vaksinasi_model->edit(decrypt_data($id_vaksinasi), $data);

            if ($status) {
                $this->session->set_flashdata('message', 'Data berhasil diubah');
            } else {
                $this->session->set_flashdata('message', 'Data gagal diubah');
            }

            redirect('vaksinasi');
        }
    }

    public function delete_vaksinasi()
    {
        $id_vaksinasi = $this->iget('id_vaksinasi');
        $data_master = $this->trx_vaksinasi_model->get_by(decrypt_data($id_vaksinasi));

        if (!$data_master) {
            $this->page_error();
        }

        $data = array(
            "deleted_at" => $this->datetime(),
            "id_user_deleted" => $this->session->userdata("id_user")
        );

        $status = $this->trx_vaksinasi_model->edit(decrypt_data($id_vaksinasi), $data);


        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }
}
