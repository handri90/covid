<style>
    .info-column {
        margin: 4px 0;
    }
</style>

<div class="content">
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url() . 'vaksinasi/tambah_vaksinasi'; ?>" class="btn btn-info">Tambah Vaksinasi</a>
            </div>
        </div>
        <table id="datatableVaksinasi" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>No Register</th>
                    <th>Nama</th>
                    <th>Tanggal Vaksinasi</th>
                    <th>No Batch</th>
                    <th>Tanggal Screening</th>
                    <th>Dosis Tahap</th>
                    <th>Keterangan</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    let datatableVaksinasi = $("#datatableVaksinasi").DataTable({
        "deferRender": true
    });

    get_vaksinasi();

    function get_vaksinasi() {

        datatableVaksinasi.clear().draw();
        $.ajax({
            url: base_url + 'vaksinasi/request/get_vaksinasi',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    datatableVaksinasi.row.add([
                        value.no_register,
                        value.nama,
                        value.tanggal_vaksinasi,
                        value.no_batch,
                        value.tanggal_screening,
                        value.dosis_tahap,
                        value.keterangan,
                        "<a href='" + base_url + "vaksinasi/edit_vaksinasi/" + value.id_encrypt + "' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>"
                    ]).draw(false);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function confirm_delete(id_vaksinasi) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'vaksinasi/delete_vaksinasi',
                    data: {
                        id_vaksinasi: id_vaksinasi
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_vaksinasi();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_vaksinasi();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_vaksinasi();
                    }
                });
            }
        });
    }
</script>