<style>
    .calendars {
        width: 350px;
    }

    .place_other_pendidikan,
    .place_other_pekerjaan {
        margin-top: -.5rem;
        margin-left: .5rem;
    }

    .ktp-image-custom {
        margin-bottom: 20px;
    }

    .kk-image-custom {
        margin-bottom: 20px;
    }
</style>
<div class="content">
    <!-- Form inputs -->
    <div class="card">
        <div class="card-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-validate-jquery')); ?>
            <fieldset class="mb-3">
                <legend class="text-uppercase font-size-sm font-weight-bold">Peserta Rapid</legend>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nama <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <select class="form-control select-search" name="peserta_vaksinasi_id" required>
                            <option value="">-- Pilih Peserta Vaksinasi --</option>
                            <?php
                            foreach ($list_peserta_vaksinasi as $key => $row) {
                                $selected = "";
                                if (!empty($content)) {
                                    if ($row->id_peserta_vaksinasi == $content->peserta_vaksinasi_id) {
                                        $selected = "selected";
                                    }
                                }
                            ?>
                                <option <?php echo $selected; ?> value="<?php echo encrypt_data($row->id_peserta_vaksinasi); ?>"><?php echo $row->nama; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">No Batch <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="number" class="form-control" value="<?php echo !empty($content) ? $content->no_batch : ""; ?>" name="no_batch" placeholder="No Batch">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">No Register <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->no_register : ""; ?>" name="no_register" placeholder="No Register">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Tanggal Screening <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control daterange-single-screening" readonly value="<?php echo !empty($content) ? $content->tanggal_screening : ""; ?>" name="tanggal_screening" required placeholder="Tanggal Screening">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Tanggal Vaksinasi <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control daterange-single-vaksinasi" readonly value="<?php echo !empty($content) ? $content->tanggal_vaksinasi : ""; ?>" name="tanggal_vaksinasi" required placeholder="Tanggal Vaksinasi">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Keterangan <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <select class="form-control select-search" name="keterangan" required>
                            <option value="">-- Pilih Keterangan --</option>
                            <?php
                            foreach ($list_keterangan as $key => $row) {
                                $selected = "";
                                if (!empty($content)) {
                                    if ($row == $content->keterangan) {
                                        $selected = "selected";
                                    }
                                }
                            ?>
                                <option <?php echo $selected; ?> value="<?php echo $row; ?>"><?php echo $row; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <!-- /form inputs -->
</div>

<script>
    $('.form-input-styled').uniform({
        fileButtonClass: 'action btn bg-pink-400'
    });

    $('.daterange-single-screening').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD/MM/YYYY'
        },
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10)
    });

    $('.daterange-single-vaksinasi').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD/MM/YYYY'
        },
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10)
    });
</script>