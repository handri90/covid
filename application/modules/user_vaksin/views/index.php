<div class="content">
    <div class="card card-table">
        <table id="datatableUser" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nama Lengkap</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>No. HP</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    get_data_user();

    function get_data_user() {
        if ($.fn.DataTable.isDataTable('#datatableUser')) {
            $('#datatableUser').DataTable().clear();
            $('#datatableUser').DataTable().destroy();
        }

        $("#datatableUser").DataTable({
            ajax: {
                "url": base_url + 'user_vaksin/request/get_data_user',
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "data": '',
                "dataSrc": '',
                "complete": function(response) {
                    HoldOn.close();
                }
            },
            "ordering": false,
            "columns": [{
                data: "nama_lengkap"
            }, {
                data: "username"
            }, {
                data: "email"
            }, {
                data: "no_hp"
            }, {
                "width": "15%",
                "render": function(data, type, full, meta) {
                    return "<a href='" + base_url + "user_vaksin/edit_user/" + full.id_encrypt + "' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a>";
                }
            }]
        });
    }
</script>