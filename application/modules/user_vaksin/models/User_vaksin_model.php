<?php

class User_vaksin_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "user_vaksin";
        $this->primary_id = "id_user_vaksin";
    }
}
