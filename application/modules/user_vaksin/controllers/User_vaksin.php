<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_vaksin extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_vaksin_model');
    }

    public function index()
    {

        $data['breadcrumb'] = [['link' => false, 'content' => 'User Vaksin', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function edit_user($id_user)
    {
        $data_master = $this->user_vaksin_model->get_by(decrypt_data($id_user));

        if (!$data_master) {
            $this->page_error();
        }

        if (empty($_POST)) {
            $data['content'] = $data_master;
            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'user_vaksin', 'content' => 'User Vaksin', 'is_active' => false], ['link' => false, 'content' => 'Ubah User Vaksin', 'is_active' => true]];
            $this->execute('form_user', $data);
        } else {

            $password = "";
            if (!empty($this->ipost('password'))) {
                $password = password_hash($this->ipost('password'), PASSWORD_BCRYPT, array('cost' => 12));
            } else {
                $password = $data_master->password;
            }

            $data = array(
                'password' => $password,
            );

            $status = $this->user_vaksin_model->edit(decrypt_data($id_user), $data);

            if ($status) {
                $this->session->set_flashdata('message', 'Data berhasil diubah');
            } else {
                $this->session->set_flashdata('message', 'Data gagal diubah');
            }

            redirect('user_vaksin');
        }
    }
}
