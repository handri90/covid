<?php

class Positivity_rate_model extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->table="positivity_rate";
        $this->primary_id="id_positivity_rate";
    }
}