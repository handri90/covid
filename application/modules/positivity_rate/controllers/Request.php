<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('positivity_rate_model');
	}

	public function get_data_positivity_rate()
	{
        $data_positivity_rate = $this->positivity_rate_model->get(
            array(
                'order_by'=>array(
                    'tanggal'=>"DESC"
                )
            )
        );

        $templist = array();
        foreach($data_positivity_rate as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['tanggal'] = longdate_indo($row->tanggal);
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_positivity_rate);
        }

        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
}
