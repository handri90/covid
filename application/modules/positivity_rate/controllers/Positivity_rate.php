<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Positivity_rate extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model('positivity_rate_model');
    }

    public function index()
    {
        $data['breadcrumb'] = [['link' => false, 'content' => 'Positivity Rate', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function tambah_positivity_rate()
    {
        if (empty($_POST)) {
            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'positivity_rate', 'content' => 'Positivity Rate', 'is_active' => false], ['link' => false, 'content' => 'Tambah Positivity Rate', 'is_active' => true]];
            $this->execute('form_positivity_rate', $data);
        } else {

            $exp_tanggal = explode("/",$this->ipost("tanggal"));
            $new_tanggal = $exp_tanggal[2]."-".$exp_tanggal[1]."-".$exp_tanggal[0];

            $data = array(
                "tanggal" => $new_tanggal,
                "nilai" => $this->ipost('nilai'),
                'created_at' => $this->datetime()
            );

            $status = $this->positivity_rate_model->save($data);
            if ($status) {
                $this->session->set_flashdata('message', 'Data baru berhasil ditambahkan');
            } else {
                $this->session->set_flashdata('message', 'Data baru gagal ditambahkan');
            }

            redirect('positivity_rate');
        }
    }

    public function edit_positivity_rate($id_positivity_rate)
    {
        $data_master = $this->positivity_rate_model->get_by(decrypt_data($id_positivity_rate));

        if (!$data_master) {
            $this->page_error();
        }

        if (empty($_POST)) {
            $exp_tanggal = explode("-",$data_master->tanggal);
            $data_master->tanggal = $exp_tanggal[2]."/".$exp_tanggal[1]."/".$exp_tanggal[0];

            $data['content'] = $data_master;
            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'positivity_rate', 'content' => 'Positivity Rate', 'is_active' => false], ['link' => false, 'content' => 'Tambah Positivity Rate', 'is_active' => true]];
            $this->execute('form_positivity_rate', $data);
        } else {
            $exp_tanggal = explode("/",$this->ipost("tanggal"));
            $new_tanggal = $exp_tanggal[2]."-".$exp_tanggal[1]."-".$exp_tanggal[0];

            $data = array(
                "tanggal" => $new_tanggal,
                "nilai" => $this->ipost('nilai'),
                'created_at' => $this->datetime()
            );

            $status = $this->positivity_rate_model->edit(decrypt_data($id_positivity_rate), $data);
            if ($status) {
                $this->session->set_flashdata('message', 'Data berhasil diubah');
            } else {
                $this->session->set_flashdata('message', 'Data gagal diubah');
            }

            redirect('positivity_rate');
        }
    }

    public function delete_positivity_rate()
    {
        $id_positivity_rate = decrypt_data($this->iget('id_positivity_rate'));
        $data_master = $this->positivity_rate_model->get_by($id_positivity_rate);

        if (!$data_master) {
            $this->page_error();
        }

        $status = $this->positivity_rate_model->remove($id_positivity_rate);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }
}
