<!-- Content area -->
<div class="content">
    <div class="card">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Tanggal</label>
                <div class="col-lg-10">
                    <button type="button" class="btn btn-light tanggal_trend_kasus_positif">
                        <i class="icon-calendar22 mr-2"></i>
                        <span></span>
                    </button>

                    <input type="hidden" name="start_date_trend" />
                    <input type="hidden" name="end_date_trend" />
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="chart-container">
                <div class="chart has-fixed-height" id="trend_kasus_positif"></div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Tanggal</label>
                <div class="col-lg-10">
                    <button type="button" class="btn btn-light tanggal_grafik_positivity_rate">
                        <i class="icon-calendar22 mr-2"></i>
                        <span></span>
                    </button>

                    <input type="hidden" name="start_date_grafik" />
                    <input type="hidden" name="end_date_grafik" />
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="chart-container">
                <div class="chart has-fixed-height" id="grafik_positif_rate"></div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Tanggal</label>
                <div class="col-lg-10">
                    <button type="button" class="btn btn-light tanggal_rawat_inap">
                        <i class="icon-calendar22 mr-2"></i>
                        <span></span>
                    </button>

                    <input type="hidden" name="start_date_rawat_inap" />
                    <input type="hidden" name="end_date_rawat_inap" />
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="chart-container">
                <div class="chart has-fixed-height" id="rawat_inap"></div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Tanggal</label>
                <div class="col-lg-10">
                    <button type="button" class="btn btn-light tanggal_vaksin">
                        <i class="icon-calendar22 mr-2"></i>
                        <span></span>
                    </button>

                    <input type="hidden" name="start_date_vaksin" />
                    <input type="hidden" name="end_date_vaksin" />
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="chart-container">
                <div class="chart has-fixed-height" id="data_vaksin"></div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Tanggal</label>
                <div class="col-lg-10">
                    <button type="button" class="btn btn-light tanggal_pie_age">
                        <i class="icon-calendar22 mr-2"></i>
                        <span></span>
                    </button>

                    <input type="hidden" name="start_date_pie" />
                    <input type="hidden" name="end_date_pie" />
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="chart-container">
                <div class="chart has-fixed-height" id="pie_age"></div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Update Kasus Covid Saat Ini</label>
                <div class="col-lg-10">
                    <button type="button" class="btn btn-light tanggal_kasus_saat_ini">
                        <i class="icon-calendar22 mr-2"></i>
                        <span></span>
                    </button>

                    <input type="hidden" name="start_date_kasus" />
                    <input type="hidden" name="end_date_kasus" />
                </div>
            </div>
        </div>
        <div class="card-body">
            <span class="data_kasus_covid"></span>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Kumulatif</label>
                <div class="col-lg-10">
                    <button type="button" class="btn btn-light tanggal_kumulatif">
                        <i class="icon-calendar22 mr-2"></i>
                        <span></span>
                    </button>

                    <input type="hidden" name="start_date_kumulatif" />
                    <input type="hidden" name="end_date_kumulatif" />
                </div>
            </div>
        </div>
        <div class="card-body">
            <span class="data_kumulatif_covid"></span>
        </div>
    </div>
    <!-- <div class="card">
        <div class="card-body">
            <div class="chart-container">
                <div class="chart has-fixed-height" id="area_antigen_pcr"></div>
            </div>
        </div>
        <div class="card-body table-responsive">
            <div class="table-persentase">
            </div>
            <div class="text-center mt-2"><span class="badge badge-primary">%</span> POSITIVITY RATE HARIAN (HANYA PCR/RCM) <span class="badge badge-secondary ml-1">%</span> POSITIVITY RATE HARIAN (TOTAL)</div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="chart-container">
                <div class="chart has-fixed-height" id="bar_pcr_positif"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-pills nav-pills-bordered nav-pills-toolbar">
                        <li class="nav-item"><a href="#Month" class="nav-link rounded-left-round active" data-toggle="tab">Month</a></li>
                        <li class="nav-item"><a href="#Year" onclick="generate_chart_year()" class="nav-link rounded-right-round" data-toggle="tab">Year</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="Month">
                            <div class="chart-container">
                                <div class="chart has-fixed-height" id="columns_basic_month"></div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="Year">
                            <div class="chart-container">
                                <div class="chart has-fixed-height" id="columns_basic_year"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6">
            <div class="card">
                <div class="card-body">
                    <div class="chart-container">
                        <div class="chart has-fixed-height" id="pie_kecamatan"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="card">
                <div class="card-body">
                    <div class="chart-container">
                        <div class="chart has-fixed-height" id="pie_umur"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6">
            <div class="card">
                <div class="card-body">
                    <div class="chart-container">
                        <div class="chart has-fixed-height" id="pie_pekerjaan"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Faskes Non Puskesmas</h5>
                </div>
                <div class="card card-table table-responsive shadow-0 mb-0">
                    <table id="datatableFaskesNonPuskesmas" class="table datatable-save-state table-bordered table-striped text-center" style="width:100%">
                        <thead>
                            <tr>
                                <th rowspan="2">Faskes</th>
                                <th colspan="5">SWAB PCR</th>
                                <th colspan="3">RAPID ANTIGEN</th>
                                <th colspan="5">RAPID ANTIBODY</th>
                            </tr>
                            <tr>
                                <th>POSITIF</th>
                                <th>NEGATIF</th>
                                <th>INCON</th>
                                <th>INVALID</th>
                                <th>TOTAL</th>
                                <th>POSITIF</th>
                                <th>NEGATIF</th>
                                <th>TOTAL</th>
                                <th>REAKTIF IGG</th>
                                <th>REAKTIF IGM</th>
                                <th>REAKTIF IGG & IGM</th>
                                <th>NON REAKTIF</th>
                                <th>TOTAL</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div> -->
</div>
<!-- /content area -->

<script>
    $('.tanggal_trend_kasus_positif').daterangepicker({
            startDate: moment().subtract(7, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.tanggal_trend_kasus_positif span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date_trend]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date_trend]').val(end.format('YYYY-MM-DD'));
            get_trend_kasus_positif();
        }
    );

    // Display date format
    $('.tanggal_trend_kasus_positif span').html(moment().subtract(7, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date_trend]').val(moment().subtract(7, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date_trend]').val(moment().format('YYYY-MM-DD'));
    
    $('.tanggal_grafik_positivity_rate').daterangepicker({
            startDate: moment().subtract(7, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.tanggal_grafik_positivity_rate span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date_grafik]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date_grafik]').val(end.format('YYYY-MM-DD'));
            get_grafik_positive_rate();
        }
    );

    // Display date format
    $('.tanggal_grafik_positivity_rate span').html(moment().subtract(7, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date_grafik]').val(moment().subtract(7, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date_grafik]').val(moment().format('YYYY-MM-DD'));
    
    $('.tanggal_pie_age').daterangepicker({
            startDate: moment().subtract(7, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.tanggal_pie_age span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date_pie]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date_pie]').val(end.format('YYYY-MM-DD'));
            get_pie_ar_berdasarkan_umur();
        }
    );

    // Display date format
    $('.tanggal_pie_age span').html(moment().subtract(7, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date_pie]').val(moment().subtract(7, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date_pie]').val(moment().format('YYYY-MM-DD'));
    
    $('.tanggal_kasus_saat_ini').daterangepicker({
            startDate: moment().subtract(7, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.tanggal_kasus_saat_ini span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date_kasus]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date_kasus]').val(end.format('YYYY-MM-DD'));
            data_kasus_covid_saat_ini();
        }
    );

    // Display date format
    $('.tanggal_kasus_saat_ini span').html(moment().subtract(7, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date_kasus]').val(moment().subtract(7, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date_kasus]').val(moment().format('YYYY-MM-DD'));
    
    $('.tanggal_kumulatif').daterangepicker({
            startDate: moment().subtract(7, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.tanggal_kumulatif span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date_kumulatif]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date_kumulatif]').val(end.format('YYYY-MM-DD'));
            data_kumulatif_covid_saat_ini();
        }
    );

    // Display date format
    $('.tanggal_kumulatif span').html(moment().subtract(7, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date_kumulatif]').val(moment().subtract(7, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date_kumulatif]').val(moment().format('YYYY-MM-DD'));
    
    $('.tanggal_vaksin').daterangepicker({
            startDate: moment().subtract(7, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.tanggal_vaksin span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date_vaksin]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date_vaksin]').val(end.format('YYYY-MM-DD'));
            get_statistik_vaksin();
        }
    );

    // Display date format
    $('.tanggal_vaksin span').html(moment().subtract(7, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date_vaksin]').val(moment().subtract(7, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date_vaksin]').val(moment().format('YYYY-MM-DD'));
    
    $('.tanggal_rawat_inap').daterangepicker({
            startDate: moment().subtract(7, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.tanggal_rawat_inap span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date_rawat_inap]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date_rawat_inap]').val(end.format('YYYY-MM-DD'));
            get_statistik_rawat_inap();
        }
    );

    // Display date format
    $('.tanggal_rawat_inap span').html(moment().subtract(7, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date_rawat_inap]').val(moment().subtract(7, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date_rawat_inap]').val(moment().format('YYYY-MM-DD'));
    get_trend_kasus_positif();
    get_grafik_positive_rate();
    get_pie_ar_berdasarkan_umur();
    data_kasus_covid_saat_ini();
    data_kumulatif_covid_saat_ini();
    get_statistik_vaksin();
    get_statistik_rawat_inap();

    let datatableFaskesNonPuskesmas = $("#datatableFaskesNonPuskesmas").DataTable({
        paging: false,
        responsive: true,
        "ordering": false
    });

    $(function() {

        // $.ajax({
        //     url: base_url + 'dashboard/request/get_statistik_antigen_pcr',
        //     type: 'GET',
        //     beforeSend: function() {
        //         HoldOn.open(optionsHoldOn);
        //     },
        //     success: function(response) {
        //         // Define element
        //         var area_stacked_element = document.getElementById('area_antigen_pcr');


        //         //
        //         // Charts configuration
        //         //

        //         if (area_stacked_element) {

        //             // Initialize chart
        //             var area_stacked = echarts.init(area_stacked_element);

        //             let series = [{
        //                     name: 'ORANG DITES PCR',
        //                     type: "bar",
        //                     stack: 'Total',
        //                     areaStyle: {
        //                         normal: {
        //                             opacity: 0.25
        //                         }
        //                     },
        //                     smooth: true,
        //                     symbolSize: 7,
        //                     data: response.pcr
        //                 },
        //                 {
        //                     name: 'ORANG DITES ANTIGEN',
        //                     type: "bar",
        //                     stack: 'Total',
        //                     areaStyle: {
        //                         normal: {
        //                             opacity: 0.25
        //                         }
        //                     },
        //                     smooth: true,
        //                     symbolSize: 7,
        //                     data: response.antigen
        //                 },
        //                 {
        //                     name: '',
        //                     type: "bar",
        //                     stack: 'Total',
        //                     data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        //                 },
        //                 {
        //                     name: 'KASUS TERBARU PCR',
        //                     type: "line",
        //                     stack: 'Total',
        //                     smooth: true,
        //                     symbolSize: 7,
        //                     data: response.kasus_terbaru_pcr_positif
        //                 },
        //                 {
        //                     name: 'KASUS TERBARU ANTIGEN',
        //                     type: "line",
        //                     stack: 'Total',
        //                     smooth: true,
        //                     symbolSize: 7,
        //                     data: response.kasus_terbaru_antigen_positif
        //                 }
        //             ];

        //             genFormatter = (series) => {
        //                 return (param) => {
        //                     // console.log(series);
        //                     let sum = 0;
        //                     series.forEach(item => {
        //                         if (item.type == 'bar') {
        //                             sum += item.data[param.dataIndex];
        //                         }
        //                     });
        //                     return sum
        //                 }
        //             };

        //             // Options
        //             let option = {

        //                 // Define colors
        //                 color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],

        //                 // Global text styles
        //                 textStyle: {
        //                     fontFamily: 'Roboto, Arial, Verdana, sans-serif',
        //                     fontSize: 13
        //                 },

        //                 // Chart animation duration
        //                 animationDuration: 750,

        //                 // Setup grid
        //                 grid: {
        //                     left: 0,
        //                     right: 40,
        //                     top: 35,
        //                     bottom: 0,
        //                     containLabel: true
        //                 },

        //                 // Add legend
        //                 legend: {
        //                     data: ["ORANG DITES PCR", "ORANG DITES ANTIGEN", "KASUS TERBARU PCR", "KASUS TERBARU ANTIGEN"],
        //                     itemHeight: 8,
        //                     itemGap: 20
        //                 },

        //                 // Add tooltip
        //                 tooltip: {
        //                     trigger: 'axis',
        //                     backgroundColor: 'rgba(0,0,0,0.75)',
        //                     padding: [10, 15],
        //                     textStyle: {
        //                         fontSize: 13,
        //                         fontFamily: 'Roboto, sans-serif'
        //                     },
        //                     axisPointer: {
        //                         type: 'cross',
        //                         crossStyle: {
        //                             color: '#999'
        //                         }
        //                     }
        //                 },

        //                 // Horizontal axis
        //                 xAxis: [{
        //                     type: 'category',
        //                     data: response.tanggal_pemeriksaan,
        //                     axisLabel: {
        //                         color: '#333'
        //                     },
        //                     axisLine: {
        //                         lineStyle: {
        //                             color: '#999'
        //                         }
        //                     },
        //                     splitLine: {
        //                         show: true,
        //                         lineStyle: {
        //                             color: '#eee',
        //                             type: 'dashed'
        //                         }
        //                     }
        //                 }],

        //                 // Vertical axis
        //                 yAxis: [{
        //                     type: 'value',
        //                     axisLabel: {
        //                         color: '#333'
        //                     },
        //                     axisLine: {
        //                         lineStyle: {
        //                             color: '#999'
        //                         }
        //                     },
        //                     splitLine: {
        //                         lineStyle: {
        //                             color: '#eee'
        //                         }
        //                     },
        //                     splitArea: {
        //                         show: true,
        //                         areaStyle: {
        //                             color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
        //                         }
        //                     },
        //                     interval: 200
        //                 }],
        //                 series: series.map((item, index) => Object.assign(item, {
        //                     type: item.type,
        //                     stack: true,
        //                     label: {
        //                         show: true,
        //                         formatter: index == 2 ? genFormatter(series) : null,
        //                         position: index == 2 ? 'top' : item.type == 'bar' ? 'inside' : 'top'
        //                     },
        //                 }))
        //             };

        //             area_stacked.setOption(option);
        //         }

        //         // Resize function
        //         var triggerChartResize = function() {
        //             area_stacked_element && area_stacked.resize();
        //         };

        //         // On sidebar width change
        //         var sidebarToggle = document.querySelector('.sidebar-control');
        //         sidebarToggle && sidebarToggle.addEventListener('click', triggerChartResize);

        //         // On window resize
        //         var resizeCharts;
        //         window.addEventListener('resize', function() {
        //             clearTimeout(resizeCharts);
        //             resizeCharts = setTimeout(function() {
        //                 triggerChartResize();
        //             }, 200);
        //         });

        //         $(".table-persentase").html(response.html_table);
        //     },
        //     complete: function() {
        //         HoldOn.close();
        //     }
        // });

        // $.ajax({
        //     url: base_url + 'dashboard/request/get_statistik_pcr_kel_desa',
        //     type: 'GET',
        //     beforeSend: function() {
        //         HoldOn.open(optionsHoldOn);
        //     },
        //     success: function(response) {
        //         // Define element
        //         var bars_basic_element = document.getElementById('bar_pcr_positif');

        //         if (bars_basic_element) {

        //             // Initialize chart
        //             var bars_basic = echarts.init(bars_basic_element);


        //             //
        //             // Chart config
        //             //

        //             // Options
        //             bars_basic.setOption({

        //                 // Global text styles
        //                 textStyle: {
        //                     fontFamily: 'Roboto, Arial, Verdana, sans-serif',
        //                     fontSize: 13
        //                 },

        //                 // Chart animation duration
        //                 animationDuration: 750,

        //                 // Setup grid
        //                 grid: {
        //                     left: 0,
        //                     right: 30,
        //                     top: 35,
        //                     bottom: 0,
        //                     containLabel: true
        //                 },

        //                 // Add legend
        //                 legend: {
        //                     data: ['Kasus Hari Ini'],
        //                     itemHeight: 8,
        //                     itemGap: 20,
        //                     textStyle: {
        //                         padding: [0, 5]
        //                     }
        //                 },

        //                 // Add tooltip
        //                 tooltip: {
        //                     trigger: 'axis',
        //                     backgroundColor: 'rgba(0,0,0,0.75)',
        //                     padding: [10, 15],
        //                     textStyle: {
        //                         fontSize: 13,
        //                         fontFamily: 'Roboto, sans-serif'
        //                     },
        //                     axisPointer: {
        //                         type: 'shadow',
        //                         shadowStyle: {
        //                             color: 'rgba(0,0,0,0.025)'
        //                         }
        //                     }
        //                 },

        //                 // Horizontal axis
        //                 xAxis: [{
        //                     type: 'value',
        //                     boundaryGap: [0, 0.01],
        //                     axisLabel: {
        //                         color: '#333'
        //                     },
        //                     axisLine: {
        //                         lineStyle: {
        //                             color: '#999'
        //                         }
        //                     },
        //                     splitLine: {
        //                         show: true,
        //                         lineStyle: {
        //                             color: '#eee',
        //                             type: 'dashed'
        //                         }
        //                     }
        //                 }],

        //                 // Vertical axis
        //                 yAxis: [{
        //                     type: 'category',
        //                     data: response.nama_wilayah,
        //                     axisLabel: {
        //                         color: '#333'
        //                     },
        //                     axisLine: {
        //                         lineStyle: {
        //                             color: '#999'
        //                         }
        //                     },
        //                     splitLine: {
        //                         show: true,
        //                         lineStyle: {
        //                             color: ['#eee']
        //                         }
        //                     },
        //                     splitArea: {
        //                         show: true,
        //                         areaStyle: {
        //                             color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
        //                         }
        //                     }
        //                 }],

        //                 // Add series
        //                 series: [{
        //                     name: 'Kasus Hari Ini',
        //                     type: 'bar',
        //                     itemStyle: {
        //                         normal: {
        //                             color: '#ffb980'
        //                         }
        //                     },
        //                     data: response.jumlah_peserta
        //                 }]
        //             });
        //         }

        //         // Resize function
        //         var triggerChartResize = function() {
        //             bars_basic_element && bars_basic.resize();
        //         };

        //         // On sidebar width change
        //         var sidebarToggle = document.querySelector('.sidebar-control');
        //         sidebarToggle && sidebarToggle.addEventListener('click', triggerChartResize);

        //         // On window resize
        //         var resizeCharts;
        //         window.addEventListener('resize', function() {
        //             clearTimeout(resizeCharts);
        //             resizeCharts = setTimeout(function() {
        //                 triggerChartResize();
        //             }, 200);
        //         });
        //     },
        //     complete: function() {
        //         HoldOn.close();
        //     }
        // });

        // $.ajax({
        //     url: base_url + 'dashboard/request/get_statistik_per_bulan',
        //     type: 'GET',
        //     beforeSend: function() {
        //         HoldOn.open(optionsHoldOn);
        //     },
        //     success: function(response) {
        //         // Define element
        //         var columns_basic_element = document.getElementById('columns_basic_month');


        //         //
        //         // Charts configuration
        //         //

        //         if (columns_basic_element) {

        //             // Initialize chart
        //             var columns_basic = echarts.init(columns_basic_element);


        //             //
        //             // Chart config
        //             //

        //             // Options
        //             columns_basic.setOption({

        //                 // Define colors
        //                 color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],

        //                 // Global text styles
        //                 textStyle: {
        //                     fontFamily: 'Roboto, Arial, Verdana, sans-serif',
        //                     fontSize: 13
        //                 },

        //                 // Chart animation duration
        //                 animationDuration: 750,

        //                 // Setup grid
        //                 grid: {
        //                     left: 0,
        //                     right: 40,
        //                     top: 35,
        //                     bottom: 0,
        //                     containLabel: true
        //                 },

        //                 // Add legend
        //                 legend: {
        //                     data: ['Positif', 'Sembuh', 'Meninggal'],
        //                     itemHeight: 8,
        //                     itemGap: 20,
        //                     textStyle: {
        //                         padding: [0, 5]
        //                     }
        //                 },

        //                 // Add tooltip
        //                 tooltip: {
        //                     trigger: 'axis',
        //                     backgroundColor: 'rgba(0,0,0,0.75)',
        //                     padding: [10, 15],
        //                     textStyle: {
        //                         fontSize: 13,
        //                         fontFamily: 'Roboto, sans-serif'
        //                     }
        //                 },

        //                 // Horizontal axis
        //                 xAxis: [{
        //                     type: 'category',
        //                     data: JSON.parse(response.bulan),
        //                     axisLabel: {
        //                         color: '#333'
        //                     },
        //                     axisLine: {
        //                         lineStyle: {
        //                             color: '#999'
        //                         }
        //                     },
        //                     splitLine: {
        //                         show: true,
        //                         lineStyle: {
        //                             color: '#eee',
        //                             type: 'dashed'
        //                         }
        //                     }
        //                 }],

        //                 // Vertical axis
        //                 yAxis: [{
        //                     type: 'value',
        //                     axisLabel: {
        //                         color: '#333'
        //                     },
        //                     axisLine: {
        //                         lineStyle: {
        //                             color: '#999'
        //                         }
        //                     },
        //                     splitLine: {
        //                         lineStyle: {
        //                             color: ['#eee']
        //                         }
        //                     },
        //                     splitArea: {
        //                         show: true,
        //                         areaStyle: {
        //                             color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
        //                         }
        //                     }
        //                 }],

        //                 // Add series
        //                 series: [{
        //                         name: 'Positif',
        //                         type: 'bar',
        //                         data: response.positif,
        //                         itemStyle: {
        //                             normal: {
        //                                 label: {
        //                                     show: true,
        //                                     position: 'top',
        //                                     textStyle: {
        //                                         fontWeight: 500
        //                                     }
        //                                 }
        //                             }
        //                         }
        //                     },
        //                     {
        //                         name: 'Sembuh',
        //                         type: 'bar',
        //                         data: response.sembuh,
        //                         itemStyle: {
        //                             normal: {
        //                                 label: {
        //                                     show: true,
        //                                     position: 'top',
        //                                     textStyle: {
        //                                         fontWeight: 500
        //                                     }
        //                                 }
        //                             }
        //                         }
        //                     },
        //                     {
        //                         name: 'Meninggal',
        //                         type: 'bar',
        //                         data: response.meninggal,
        //                         itemStyle: {
        //                             normal: {
        //                                 label: {
        //                                     show: true,
        //                                     position: 'top',
        //                                     textStyle: {
        //                                         fontWeight: 500
        //                                     }
        //                                 }
        //                             }
        //                         }
        //                     }
        //                 ]
        //             });
        //         }

        //         // Resize function
        //         var triggerChartResize = function() {
        //             columns_basic_element && columns_basic.resize();
        //         };

        //         // On sidebar width change
        //         var sidebarToggle = document.querySelector('.sidebar-control');
        //         sidebarToggle && sidebarToggle.addEventListener('click', triggerChartResize);

        //         // On window resize
        //         var resizeCharts;
        //         window.addEventListener('resize', function() {
        //             clearTimeout(resizeCharts);
        //             resizeCharts = setTimeout(function() {
        //                 triggerChartResize();
        //             }, 200);
        //         });
        //     },
        //     complete: function() {
        //         HoldOn.close();
        //     }
        // });

        // $.ajax({
        //     url: base_url + 'dashboard/request/get_jumlah_pemeriksaan_non_puskesmas',
        //     type: 'GET',
        //     beforeSend: function() {
        //         HoldOn.open(optionsHoldOn);
        //     },
        //     success: function(response) {
        //         $.each(response, function(index, value) {
        //             datatableFaskesNonPuskesmas.row.add([
        //                 value.nama_lengkap,
        //                 value.jumlah_swab_pcr_positif,
        //                 value.jumlah_swab_pcr_negatif,
        //                 value.jumlah_swab_pcr_incon,
        //                 value.jumlah_swab_pcr_invalid,
        //                 value.jumlah_swab_pcr,
        //                 value.jumlah_rapid_antigen_positif,
        //                 value.jumlah_rapid_antigen_negatif,
        //                 value.jumlah_rapid_antigen,
        //                 value.jumlah_rapid_antibody_reaktif_igg,
        //                 value.jumlah_rapid_antibody_reaktif_igm,
        //                 value.jumlah_rapid_antibody_reaktif_igg_igm,
        //                 value.jumlah_rapid_antibody_non_reaktif,
        //                 value.jumlah_rapid_antibody
        //             ]).draw(false);
        //         });
        //     },
        //     complete: function() {
        //         HoldOn.close();
        //     }
        // });

        get_trend_kasus_positif();
        get_grafik_positive_rate();
    })

    function get_trend_kasus_positif(){
        let start_date = $("input[name='start_date_trend']").val();
        let end_date = $("input[name='end_date_trend']").val();

        $.ajax({
            url: base_url + 'epidemiologi/request/get_trend_kasus_covid',
            data: {
                start_date: start_date,
                end_date: end_date,
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                var trend_kasus_positif_element = document.getElementById('trend_kasus_positif');


                //
                // Charts configuration
                //

                if (trend_kasus_positif_element) {

                    // Initialize chart
                    var trend_kasus_positif = echarts.init(trend_kasus_positif_element);


                    //
                    // Chart config
                    //

                    // Options
                    trend_kasus_positif.setOption({
                        title: {
                            text: 'Trend Kasus Covid',
                            left: 'center',
                            textStyle: {
                                fontSize: 17,
                                fontWeight: 500
                            },
                            subtextStyle: {
                                fontSize: 12
                            }
                        },

                        // Define colors
                        color: ['#EF5350', '#66BB6A'],

                        // Global text styles
                        textStyle: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 13
                        },

                        // Chart animation duration
                        animationDuration: 750,

                        // Setup grid
                        grid: {
                            left: 0,
                            right: 40,
                            top: 35,
                            bottom: 0,
                            containLabel: true
                        },

                        // Add legend
                        legend: {
                            data: ['Trend Kasus Covid'],
                            itemHeight: 8,
                            itemGap: 20
                        },

                        // Add tooltip
                        tooltip: {
                            trigger: 'axis',
                            backgroundColor: 'rgba(0,0,0,0.75)',
                            padding: [10, 15],
                            textStyle: {
                                fontSize: 13,
                                fontFamily: 'Roboto, sans-serif'
                            }
                        },

                        // Horizontal axis
                        xAxis: [{
                            type: 'category',
                            boundaryGap: false,
                            data: response.tanggal,
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                lineStyle: {
                                    color: ['#eee']
                                }
                            }
                        }],

                        // Vertical axis
                        yAxis: [{
                            type: 'value',
                            axisLabel: {
                                formatter: '{value}',
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                lineStyle: {
                                    color: ['#eee']
                                }
                            },
                            splitArea: {
                                show: true,
                                areaStyle: {
                                    color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                                }
                            }
                        }],

                        // Add series
                        series: [
                            {
                                name: 'Attack Rate',
                                type: 'line',
                                data: response.data,
                                itemStyle: {
                                    normal: {
                                        borderWidth: 2
                                    }
                                }
                            },
                            {
                                name: 'Mortality',
                                type: 'line',
                                data: response.data_case_fatality,
                                itemStyle: {
                                    normal: {
                                        borderWidth: 2
                                    }
                                }
                            }
                        ]
                    });
                }


                //
                // Resize charts
                //

                // Resize function
                var triggerChartResize = function() {
                    trend_kasus_positif_element && trend_kasus_positif.resize();
                };

                // On sidebar width change
                var sidebarToggle = document.querySelector('.sidebar-control');
                sidebarToggle && sidebarToggle.addEventListener('click', triggerChartResize);

                // On window resize
                var resizeCharts;
                window.addEventListener('resize', function() {
                    clearTimeout(resizeCharts);
                    resizeCharts = setTimeout(function () {
                        triggerChartResize();
                    }, 200);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }
    
    function get_grafik_positive_rate(){
        let start_date = $("input[name='start_date_grafik']").val();
        let end_date = $("input[name='end_date_grafik']").val();

        $.ajax({
            url: base_url + 'epidemiologi/request/get_grafik_positive_rate',
            data: {
                start_date: start_date,
                end_date: end_date,
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                var grafik_positif_rate_element = document.getElementById('grafik_positif_rate');


                //
                // Charts configuration
                //

                if (grafik_positif_rate_element) {

                    // Initialize chart
                    var grafik_positif_rate = echarts.init(grafik_positif_rate_element);


                    //
                    // Chart config
                    //

                    // Options
                    grafik_positif_rate.setOption({
                        title: {
                            text: 'Positivity Rate',
                            left: 'center',
                            textStyle: {
                                fontSize: 17,
                                fontWeight: 500
                            },
                            subtextStyle: {
                                fontSize: 12
                            }
                        },

                        // Define colors
                        color: ['#EF5350', '#66BB6A'],

                        // Global text styles
                        textStyle: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 13
                        },

                        // Chart animation duration
                        animationDuration: 750,

                        // Setup grid
                        grid: {
                            left: 0,
                            right: 40,
                            top: 35,
                            bottom: 0,
                            containLabel: true
                        },

                        // Add legend
                        legend: {
                            data: ['Trend Kasus Positif'],
                            itemHeight: 8,
                            itemGap: 20
                        },

                        // Add tooltip
                        tooltip: {
                            trigger: 'axis',
                            backgroundColor: 'rgba(0,0,0,0.75)',
                            padding: [10, 15],
                            textStyle: {
                                fontSize: 13,
                                fontFamily: 'Roboto, sans-serif'
                            }
                        },

                        // Horizontal axis
                        xAxis: [{
                            type: 'category',
                            boundaryGap: false,
                            data: response.tanggal,
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                lineStyle: {
                                    color: ['#eee']
                                }
                            }
                        }],

                        // Vertical axis
                        yAxis: [{
                            type: 'value',
                            axisLabel: {
                                formatter: '{value}',
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                lineStyle: {
                                    color: ['#eee']
                                }
                            },
                            splitArea: {
                                show: true,
                                areaStyle: {
                                    color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                                }
                            }
                        }],

                        // Add series
                        series: [
                            {
                                name: 'Attack Rate',
                                type: 'line',
                                data: response.data,
                                itemStyle: {
                                    normal: {
                                        borderWidth: 2
                                    }
                                }
                            }
                        ]
                    });
                }


                //
                // Resize charts
                //

                // Resize function
                var triggerChartResize = function() {
                    grafik_positif_rate_element && grafik_positif_rate.resize();
                };

                // On sidebar width change
                var sidebarToggle = document.querySelector('.sidebar-control');
                sidebarToggle && sidebarToggle.addEventListener('click', triggerChartResize);

                // On window resize
                var resizeCharts;
                window.addEventListener('resize', function() {
                    clearTimeout(resizeCharts);
                    resizeCharts = setTimeout(function () {
                        triggerChartResize();
                    }, 200);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function generate_chart_year() {

        $.ajax({
            url: base_url + 'dashboard/request/get_statistik_per_tahun',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                // Define element
                var columns_basic_element = document.getElementById('columns_basic_year');


                //
                // Charts configuration
                //

                if (columns_basic_element) {

                    // Initialize chart
                    var columns_basic = echarts.init(columns_basic_element);


                    //
                    // Chart config
                    //

                    // Options
                    columns_basic.setOption({

                        // Define colors
                        color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],

                        // Global text styles
                        textStyle: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 13
                        },

                        // Chart animation duration
                        animationDuration: 750,

                        // Setup grid
                        grid: {
                            left: 0,
                            right: 40,
                            top: 35,
                            bottom: 0,
                            containLabel: true
                        },

                        // Add legend
                        legend: {
                            data: ['Positif', 'Sembuh', 'Meninggal'],
                            itemHeight: 8,
                            itemGap: 20,
                            textStyle: {
                                padding: [0, 5]
                            }
                        },

                        // Add tooltip
                        tooltip: {
                            trigger: 'axis',
                            backgroundColor: 'rgba(0,0,0,0.75)',
                            padding: [10, 15],
                            textStyle: {
                                fontSize: 13,
                                fontFamily: 'Roboto, sans-serif'
                            }
                        },

                        // Horizontal axis
                        xAxis: [{
                            type: 'category',
                            data: JSON.parse(response.tahun),
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                show: true,
                                lineStyle: {
                                    color: '#eee',
                                    type: 'dashed'
                                }
                            }
                        }],

                        // Vertical axis
                        yAxis: [{
                            type: 'value',
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                lineStyle: {
                                    color: ['#eee']
                                }
                            },
                            splitArea: {
                                show: true,
                                areaStyle: {
                                    color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                                }
                            }
                        }],

                        // Add series
                        series: [{
                                name: 'Positif',
                                type: 'bar',
                                data: response.positif,
                                itemStyle: {
                                    normal: {
                                        label: {
                                            show: true,
                                            position: 'top',
                                            textStyle: {
                                                fontWeight: 500
                                            }
                                        }
                                    }
                                }
                            },
                            {
                                name: 'Sembuh',
                                type: 'bar',
                                data: response.sembuh,
                                itemStyle: {
                                    normal: {
                                        label: {
                                            show: true,
                                            position: 'top',
                                            textStyle: {
                                                fontWeight: 500
                                            }
                                        }
                                    }
                                }
                            },
                            {
                                name: 'Meninggal',
                                type: 'bar',
                                data: response.meninggal,
                                itemStyle: {
                                    normal: {
                                        label: {
                                            show: true,
                                            position: 'top',
                                            textStyle: {
                                                fontWeight: 500
                                            }
                                        }
                                    }
                                }
                            }
                        ]
                    });
                }
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }


    // get_pie_sebaran_kecamatan();

    function get_pie_sebaran_kecamatan() {
        $.ajax({
            url: base_url + 'dashboard/request/get_pie_sebaran_kecamatan',
            type: 'GET',
            success: function(response) {
                // Define element
                var pie_basic_element = document.getElementById('pie_kecamatan');


                //
                // Charts configuration
                //

                if (pie_basic_element) {

                    // Initialize chart
                    var pie_basic = echarts.init(pie_basic_element);


                    //
                    // Chart config
                    //

                    // Options
                    pie_basic.setOption({

                        // Colors
                        color: [
                            '#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80',
                            '#8d98b3', '#e5cf0d', '#97b552', '#95706d', '#dc69aa',
                            '#07a2a4', '#9a7fd1', '#588dd5', '#f5994e', '#c05050',
                            '#59678c', '#c9ab00', '#7eb00a', '#6f5553', '#c14089'
                        ],

                        // Global text styles
                        textStyle: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 13
                        },

                        // Add title
                        title: {
                            text: 'Sebaran Beradasarkan Kecamatan',
                            left: 'center',
                            textStyle: {
                                fontSize: 17,
                                fontWeight: 500
                            },
                            subtextStyle: {
                                fontSize: 12
                            }
                        },

                        // Add tooltip
                        tooltip: {
                            trigger: 'item',
                            backgroundColor: 'rgba(0,0,0,0.75)',
                            padding: [10, 15],
                            textStyle: {
                                fontSize: 13,
                                fontFamily: 'Roboto, sans-serif'
                            },
                            formatter: "{a} <br/>{b}: {c} ({d}%)"
                        },

                        // Add legend
                        // legend: {
                        //     orient: 'vertical',
                        //     top: 'center',
                        //     left: 0,
                        //     data: response.nama_kecamatan,
                        //     itemHeight: 8,
                        //     itemWidth: 8
                        // },

                        // Add series
                        series: [{
                            name: 'Wilayah',
                            type: 'pie',
                            radius: '70%',
                            center: ['50%', '57.5%'],
                            itemStyle: {
                                normal: {
                                    borderWidth: 1,
                                    borderColor: '#fff'
                                }
                            },
                            data: response.data
                        }]
                    });
                }
            }
        });
    }

    // get_pie_sebaran_umur();

    function get_pie_sebaran_umur() {
        $.ajax({
            url: base_url + 'dashboard/request/get_pie_sebaran_umur',
            type: 'GET',
            success: function(response) {
                // Define element
                var pie_basic_element = document.getElementById('pie_umur');


                //
                // Charts configuration
                //

                if (pie_basic_element) {

                    // Initialize chart
                    var pie_basic = echarts.init(pie_basic_element);


                    //
                    // Chart config
                    //

                    // Options
                    pie_basic.setOption({

                        // Colors
                        color: [
                            '#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80',
                            '#8d98b3', '#e5cf0d', '#97b552', '#95706d', '#dc69aa',
                            '#07a2a4', '#9a7fd1', '#588dd5', '#f5994e', '#c05050',
                            '#59678c', '#c9ab00', '#7eb00a', '#6f5553', '#c14089'
                        ],

                        // Global text styles
                        textStyle: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 13
                        },

                        // Add title
                        title: {
                            text: 'Sebaran Berdasarkan Umur',
                            left: 'center',
                            textStyle: {
                                fontSize: 17,
                                fontWeight: 500
                            },
                            subtextStyle: {
                                fontSize: 12
                            }
                        },

                        // Add tooltip
                        tooltip: {
                            trigger: 'item',
                            backgroundColor: 'rgba(0,0,0,0.75)',
                            padding: [10, 15],
                            textStyle: {
                                fontSize: 13,
                                fontFamily: 'Roboto, sans-serif'
                            },
                            formatter: "{a} <br/>{b}: {c} ({d}%)"
                        },

                        // Add legend
                        // legend: {
                        //     orient: 'vertical',
                        //     top: 'center',
                        //     left: 0,
                        //     data: response.rentang_umur,
                        //     itemHeight: 8,
                        //     itemWidth: 8
                        // },

                        // Add series
                        series: [{
                            name: 'Umur',
                            type: 'pie',
                            radius: '70%',
                            center: ['50%', '57.5%'],
                            itemStyle: {
                                normal: {
                                    borderWidth: 1,
                                    borderColor: '#fff'
                                }
                            },
                            data: response.data
                        }]
                    });
                }
            }
        });
    }

    function get_pie_ar_berdasarkan_umur() {
        let start_date = $("input[name='start_date_pie']").val();
        let end_date = $("input[name='end_date_pie']").val();
        $.ajax({
            url: base_url + 'epidemiologi/request/get_pie_ar_berdasarkan_umur',
            data: {
                start_date: start_date,
                end_date: end_date,
            },
            type: 'GET',
            success: function(response) {
                // Define element
                var pie_basic_element = document.getElementById('pie_age');


                //
                // Charts configuration
                //

                if (pie_basic_element) {

                    // Initialize chart
                    var pie_basic = echarts.init(pie_basic_element);


                    //
                    // Chart config
                    //

                    // Options
                    pie_basic.setOption({

                        // Colors
                        color: [
                            '#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80',
                            '#8d98b3', '#e5cf0d', '#97b552', '#95706d', '#dc69aa',
                            '#07a2a4', '#9a7fd1', '#588dd5', '#f5994e', '#c05050',
                            '#59678c', '#c9ab00', '#7eb00a', '#6f5553', '#c14089'
                        ],

                        // Global text styles
                        textStyle: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 13
                        },

                        // Add title
                        title: {
                            text: 'Attack Rate dan Proporsi Kasus Covid-19 Berdasarkan Usia',
                            left: 'center',
                            textStyle: {
                                fontSize: 17,
                                fontWeight: 500
                            },
                            subtextStyle: {
                                fontSize: 12
                            }
                        },

                        label: {
                            alignTo: 'edge',
                            formatter: '{b}\n{c} ({d}%)',
                            minMargin: 5,
                            edgeDistance: 10,
                            lineHeight: 15,
                            rich: {
                                time: {
                                    fontSize: 10,
                                    color: '#999'
                                }
                            }
                        },

                        // Add tooltip
                        tooltip: {
                            trigger: 'item',
                            backgroundColor: 'rgba(0,0,0,0.75)',
                            padding: [10, 15],
                            textStyle: {
                                fontSize: 13,
                                fontFamily: 'Roboto, sans-serif'
                            },
                            formatter: "{a} <br/>{b}: {c} ({d}%)"
                        },

                        // Add legend
                        legend: {
                            orient: 'vertical',
                            top: 'center',
                            left: 0,
                            data: response.rentang_umur,
                            itemHeight: 8,
                            itemWidth: 8
                        },

                        // Add series
                        series: [{
                            name: 'Umur',
                            type: 'pie',
                            radius: '70%',
                            center: ['50%', '57.5%'],
                            itemStyle: {
                                normal: {
                                    borderWidth: 1,
                                    borderColor: '#fff'
                                }
                            },
                            data: response.data
                        }]
                    });
                }
            }
        });
    }

    // get_pie_sebaran_pekerjaan();

    function get_pie_sebaran_pekerjaan() {
        $.ajax({
            url: base_url + 'dashboard/request/get_pie_sebaran_pekerjaan',
            type: 'GET',
            success: function(response) {
                // Define element
                var pie_basic_element = document.getElementById('pie_pekerjaan');


                //
                // Charts configuration
                //

                if (pie_basic_element) {

                    // Initialize chart
                    var pie_basic = echarts.init(pie_basic_element);


                    //
                    // Chart config
                    //

                    // Options
                    pie_basic.setOption({

                        // Colors
                        color: [
                            '#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80',
                            '#8d98b3', '#e5cf0d', '#97b552', '#95706d', '#dc69aa',
                            '#07a2a4', '#9a7fd1', '#588dd5', '#f5994e', '#c05050',
                            '#59678c', '#c9ab00', '#7eb00a', '#6f5553', '#c14089'
                        ],

                        // Global text styles
                        textStyle: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 13
                        },

                        // Add title
                        title: {
                            text: 'Sebaran Berdasarkan Pekerjaan',
                            left: 'center',
                            textStyle: {
                                fontSize: 17,
                                fontWeight: 500
                            },
                            subtextStyle: {
                                fontSize: 12
                            }
                        },

                        label: {
                            alignTo: 'edge',
                            formatter: '{b}\n{c} ({d}%)',
                            minMargin: 5,
                            edgeDistance: 10,
                            lineHeight: 15,
                            rich: {
                                time: {
                                    fontSize: 10,
                                    color: '#999'
                                }
                            }
                        },

                        // Add tooltip
                        tooltip: {
                            trigger: 'item',
                            backgroundColor: 'rgba(0,0,0,0.75)',
                            padding: [10, 15],
                            textStyle: {
                                fontSize: 13,
                                fontFamily: 'Roboto, sans-serif'
                            },
                            formatter: "{a} <br/>{b}: {c} ({d}%)"
                        },

                        // Add legend
                        // legend: {
                        //     orient: 'vertical',
                        //     top: 'center',
                        //     left: 0,
                        //     data: response.pekerjaan,
                        //     itemHeight: 8,
                        //     itemWidth: 8
                        // },

                        // Add series
                        series: [{
                            name: 'Pekerjaan',
                            type: 'pie',
                            radius: '70%',
                            center: ['50%', '57.5%'],
                            itemStyle: {
                                normal: {
                                    borderWidth: 1,
                                    borderColor: '#fff'
                                }
                            },
                            data: response.data
                        }]
                    });
                }
            }
        });
    }
    
    function data_kasus_covid_saat_ini() {
        let start_date = $("input[name='start_date_kasus']").val();
        let end_date = $("input[name='end_date_kasus']").val();
        $.ajax({
            url: base_url + 'epidemiologi/request/data_kasus_covid_saat_ini',
            data:{
                start_date:start_date,
                end_date:end_date,
            },
            type: 'GET',
            success: function(response) {
                let html = "<table class='table'>"+
                    "<tr>"+
                    "<th>"+
                    "Terkonfirmasi"+
                    "</th>"+
                    "<th>"+
                    "Luar Wilayah"+
                    "</th>"+
                    "<th>"+
                    "Isolasi Mandiri"+
                    "</th>"+
                    "<th>"+
                    "Isolasi RSUD"+
                    "</th>"+
                    "</tr>";

                    html += "<tr><td>"+response.terkonfirmasi+"</td><td>"+response.luar_wilayah+"</td><td>"+response.isolasi_mandiri+"</td><td>"+response.isolasi_rsud+"</td></tr>";
                
                html += "</table>";
                $(".data_kasus_covid").html(html);
            }
        });
    }
    
    function data_kumulatif_covid_saat_ini() {
        let start_date = $("input[name='start_date_kumulatif']").val();
        let end_date = $("input[name='end_date_kumulatif']").val();
        $.ajax({
            url: base_url + 'epidemiologi/request/data_kumulatif_covid_saat_ini',
            data:{
                start_date:start_date,
                end_date:end_date,
            },
            type: 'GET',
            success: function(response) {
                let html = "<table class='table'>"+
                    "<tr>"+
                    "<th>"+
                    "Terkonfirmasi"+
                    "</th>"+
                    "<th>"+
                    "Luar Wilayah"+
                    "</th>"+
                    "<th>"+
                    "Sembuh"+
                    "</th>"+
                    "<th>"+
                    "meninggal"+
                    "</th>"+
                    "</tr>";

                    html += "<tr><td>"+response.terkonfirmasi+"</td><td>"+response.luar_wilayah+"</td><td>"+response.sembuh+"</td><td>"+response.meninggal+"</td></tr>";
                
                html += "</table>";
                $(".data_kumulatif_covid").html(html);
            }
        });
    }

    function get_statistik_vaksin(){
        let start_date = $("input[name='start_date_vaksin']").val();
        let end_date = $("input[name='end_date_vaksin']").val();
        $.ajax({
            url: base_url + 'epidemiologi/request/get_statistik_vaksin',
            data:{
                start_date:start_date,
                end_date:end_date,
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                // Define element
                var columns_basic_element = document.getElementById('data_vaksin');


                //
                // Charts configuration
                //

                if (columns_basic_element) {

                    // Initialize chart
                    var columns_basic = echarts.init(columns_basic_element);


                    //
                    // Chart config
                    //

                    // Options
                    columns_basic.setOption({

                        // Define colors
                        color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],

                        // Global text styles
                        textStyle: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 13
                        },

                        // Chart animation duration
                        animationDuration: 750,

                        // Setup grid
                        grid: {
                            left: 0,
                            right: 40,
                            top: 35,
                            bottom: 0,
                            containLabel: true
                        },

                        // Add legend
                        legend: {
                            data: ['Dosis 1', 'Dosis 2', 'Dosis 3'],
                            itemHeight: 8,
                            itemGap: 20,
                            textStyle: {
                                padding: [0, 5]
                            }
                        },

                        // Add tooltip
                        tooltip: {
                            trigger: 'axis',
                            backgroundColor: 'rgba(0,0,0,0.75)',
                            padding: [10, 15],
                            textStyle: {
                                fontSize: 13,
                                fontFamily: 'Roboto, sans-serif'
                            }
                        },

                        // Horizontal axis
                        xAxis: [{
                            type: 'category',
                            data: response.tanggal,
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                show: true,
                                lineStyle: {
                                    color: '#eee',
                                    type: 'dashed'
                                }
                            }
                        }],

                        // Vertical axis
                        yAxis: [{
                            type: 'value',
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                lineStyle: {
                                    color: ['#eee']
                                }
                            },
                            splitArea: {
                                show: true,
                                areaStyle: {
                                    color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                                }
                            }
                        }],

                        // Add series
                        series: [{
                                name: 'Dosis 1',
                                type: 'bar',
                                data: response.dosis_1,
                                itemStyle: {
                                    normal: {
                                        label: {
                                            show: true,
                                            position: 'top',
                                            textStyle: {
                                                fontWeight: 500
                                            }
                                        }
                                    }
                                }
                            },
                            {
                                name: 'Dosis 2',
                                type: 'bar',
                                data: response.dosis_2,
                                itemStyle: {
                                    normal: {
                                        label: {
                                            show: true,
                                            position: 'top',
                                            textStyle: {
                                                fontWeight: 500
                                            }
                                        }
                                    }
                                }
                            },
                            {
                                name: 'Dosis 3',
                                type: 'bar',
                                data: response.dosis_3,
                                itemStyle: {
                                    normal: {
                                        label: {
                                            show: true,
                                            position: 'top',
                                            textStyle: {
                                                fontWeight: 500
                                            }
                                        }
                                    }
                                }
                            }
                        ]
                    });
                }
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function get_statistik_rawat_inap(){
        let start_date = $("input[name='start_date_rawat_inap']").val();
        let end_date = $("input[name='end_date_rawat_inap']").val();

        $.ajax({
            url: base_url + 'epidemiologi/request/get_rawat_inap',
            data: {
                start_date: start_date,
                end_date: end_date,
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                var rawat_inap_element = document.getElementById('rawat_inap');


                //
                // Charts configuration
                //

                if (rawat_inap_element) {

                    // Initialize chart
                    var rawat_inap = echarts.init(rawat_inap_element);


                    //
                    // Chart config
                    //

                    // Options
                    rawat_inap.setOption({
                        title: {
                            text: 'Rawat Inap',
                            left: 'center',
                            textStyle: {
                                fontSize: 17,
                                fontWeight: 500
                            },
                            subtextStyle: {
                                fontSize: 12
                            }
                        },

                        // Define colors
                        color: ['#EF5350', '#66BB6A'],

                        // Global text styles
                        textStyle: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 13
                        },

                        // Chart animation duration
                        animationDuration: 750,

                        // Setup grid
                        grid: {
                            left: 0,
                            right: 40,
                            top: 35,
                            bottom: 0,
                            containLabel: true
                        },

                        // Add legend
                        legend: {
                            data: ['Trend Kasus Positif'],
                            itemHeight: 8,
                            itemGap: 20
                        },

                        // Add tooltip
                        tooltip: {
                            trigger: 'axis',
                            backgroundColor: 'rgba(0,0,0,0.75)',
                            padding: [10, 15],
                            textStyle: {
                                fontSize: 13,
                                fontFamily: 'Roboto, sans-serif'
                            }
                        },

                        // Horizontal axis
                        xAxis: [{
                            type: 'category',
                            boundaryGap: false,
                            data: response.tanggal,
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                lineStyle: {
                                    color: ['#eee']
                                }
                            }
                        }],

                        // Vertical axis
                        yAxis: [{
                            type: 'value',
                            axisLabel: {
                                formatter: '{value}',
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                lineStyle: {
                                    color: ['#eee']
                                }
                            },
                            splitArea: {
                                show: true,
                                areaStyle: {
                                    color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                                }
                            }
                        }],

                        // Add series
                        series: [
                            {
                                name: 'Attack Rate',
                                type: 'line',
                                data: response.data,
                                itemStyle: {
                                    normal: {
                                        borderWidth: 2
                                    }
                                }
                            }
                        ]
                    });
                }


                //
                // Resize charts
                //

                // Resize function
                var triggerChartResize = function() {
                    rawat_inap_element && rawat_inap.resize();
                };

                // On sidebar width change
                var sidebarToggle = document.querySelector('.sidebar-control');
                sidebarToggle && sidebarToggle.addEventListener('click', triggerChartResize);

                // On window resize
                var resizeCharts;
                window.addEventListener('resize', function() {
                    clearTimeout(resizeCharts);
                    resizeCharts = setTimeout(function () {
                        triggerChartResize();
                    }, 200);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }
</script>