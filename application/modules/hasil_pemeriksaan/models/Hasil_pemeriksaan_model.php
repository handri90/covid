<?php

class Hasil_pemeriksaan_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "hasil_pemeriksaan";
        $this->primary_id = "id_hasil_pemeriksaan";
    }
}
