<div class="content">
    <div class="card border-top-success">
        <div class="card-header">
            <div class="text-right">
                <a href="<?php echo base_url() . 'ppkm/upload_infografis_form'; ?>" class="btn btn-info">Upload</a>
            </div>
        </div>
        <div class="card-body">
            <div class="card card-table">
                <table id="datatablePesertaRapid" class="table datatable-save-state table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Gambar</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    let arr_id = [];

    let datatablePesertaRapid = $("#datatablePesertaRapid").DataTable({
        "deferRender": true,
        "ordering": false,
        "paging": false,
        "columns": [{
                "width": "25%"
            },
            null,
            {
                "width": "15%"
            },
        ]
    });

    get_upload_infografis();

    function get_upload_infografis() {

        datatablePesertaRapid.clear().draw();

        $.ajax({
            url: base_url + 'ppkm/request/get_upload_infografis',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    let spl_filename = value.filename.split("|");
                    let str_filename = "";
                    $.each(spl_filename, function(index_spl, val_spl) {
                        str_filename += "<span class='mr-1'><img width='200' src='" + base_url + "assets/infografis_path/" + val_spl + "' /></span>";
                    });

                    datatablePesertaRapid.row.add([
                        value.tanggal_custom,
                        str_filename,
                        "<a href='" + base_url + "ppkm/edit_upload_infografis/" + value.tanggal + "/" + value.tipe_upload + "' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.tanggal + "')\" href='#'><i class='icon-trash'></i></a>"
                    ]).draw(false);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function confirm_delete(tanggal) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus gambar ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'ppkm/delete_infografis',
                    data: {
                        tanggal: tanggal
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_upload_infografis();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_upload_infografis();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                )
            }
        });
    }
</script>