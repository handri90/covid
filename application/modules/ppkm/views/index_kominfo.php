<style>
    .header_tbl {
        width: 30%;
    }

    .header_tbl2 {
        width: 1%;
    }

    .info-column {
        margin: 4px 0;
    }

    .detail {
        float: right;
        font-weight: 400;
        line-height: 0.6;
        text-shadow: none;
        color: #fff !important;
        top: -5px;
    }

    .green-area {
        background-color: #2c9f45;
        color: #fff;
    }

    .yellow-area {
        background-color: #e4e932;
        color: #000;
    }

    .orange-area {
        background-color: #f48924;
        color: #000;
    }

    .red-area {
        background-color: #e4002b;
        color: #fff;
    }
</style>

<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <div class="header-elements">
                <div class="form-check form-check-switchery form-check-switchery-double">
                    <label class="form-check-label">
                        All Time
                        <input type="checkbox" name="report_today" class="form-check-input-switchery-primary" onclick="get_peserta_new()" data-fouc>
                        Today
                    </label>
                </div>
            </div>
            <div class="text-right">
                <a href="<?php echo base_url() . 'ppkm/cetak_peserta_ppkm_new'; ?>" id="cetak_peserta_ppkm_new" class="btn btn-info">Cetak</a>
                <a href="<?php echo base_url() . 'ppkm/upload_infografis'; ?>" id="cetak_peserta_ppkm" class="btn btn-warning">Upload Infografis</a>
                <a href="#newPagePPKM" onclick="show_new_page_ppkm()" id="new_page_ppkm" class="btn btn-success">Zonasi Penyebaran Covid 19</a>
            </div>
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-highlight nav-justified">
                <li class="nav-item"><a href="#kec-arsel" class="nav-link active" data-toggle="tab">KEC - ARSEL</a></li>
                <li class="nav-item"><a href="#kec-aruta" class="nav-link" data-toggle="tab">KEC - ARUTA</a></li>
                <li class="nav-item"><a href="#kec-kumai" class="nav-link" data-toggle="tab">KEC - KUMAI</a></li>
                <li class="nav-item"><a href="#kec-kolam" class="nav-link" data-toggle="tab">KEC - KOLAM</a></li>
                <li class="nav-item"><a href="#kec-pbanteng" class="nav-link" data-toggle="tab">KEC - P. BANTENG</a></li>
                <li class="nav-item"><a href="#kec-plada" class="nav-link" data-toggle="tab">KEC - P. LADA</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade show active" id="kec-arsel">
                    <div class="card card-table table-responsive shadow-0 mb-0">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="200" class="text-center">Nama Wilayah / RT</th>
                                    <th class="text-center">Jumlah Warga Positif</th>
                                    <th class="text-center" colspan='4'>Jumlah Rumah Kasus Aktif</th>
                                </tr>
                            </thead>
                            <tbody class="list_kecamatan_arsel">

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="kec-aruta">
                    <div class="card card-table table-responsive shadow-0 mb-0">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="200" class="text-center">Nama Wilayah / RT</th>
                                    <th class="text-center">Jumlah Warga Positif</th>
                                    <th class="text-center" colspan='4'>Jumlah Rumah Kasus Aktif</th>
                                </tr>
                            </thead>
                            <tbody class="list_kecamatan_aruta">

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="kec-kumai">
                    <div class="card card-table table-responsive shadow-0 mb-0">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="200" class="text-center">Nama Wilayah / RT</th>
                                    <th class="text-center">Jumlah Warga Positif</th>
                                    <th class="text-center" colspan='4'>Jumlah Rumah Kasus Aktif</th>
                                </tr>
                            </thead>
                            <tbody class="list_kecamatan_kumai">

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="kec-kolam">
                    <div class="card card-table table-responsive shadow-0 mb-0">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="200" class="text-center">Nama Wilayah / RT</th>
                                    <th class="text-center">Jumlah Warga Positif</th>
                                    <th class="text-center" colspan='4'>Jumlah Rumah Kasus Aktif</th>
                                </tr>
                            </thead>
                            <tbody class="list_kecamatan_kolam">

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="kec-pbanteng">
                    <div class="card card-table table-responsive shadow-0 mb-0">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="200" class="text-center">Nama Wilayah / RT</th>
                                    <th class="text-center">Jumlah Warga Positif</th>
                                    <th class="text-center" colspan='4'>Jumlah Rumah Kasus Aktif</th>
                                </tr>
                            </thead>
                            <tbody class="list_kecamatan_banteng">

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="kec-plada">
                    <div class="card card-table table-responsive shadow-0 mb-0">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="200" class="text-center">Nama Wilayah / RT</th>
                                    <th class="text-center">Jumlah Warga Positif</th>
                                    <th class="text-center" colspan='4'>Jumlah Rumah Kasus Aktif</th>
                                </tr>
                            </thead>
                            <tbody class="list_kecamatan_lada">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="showNewPagePPKM" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <?php echo form_open(base_url() . "ppkm/cetak_laporan_zonasi", array("target" => "_blank", "method" => "get")); ?>
                <div class="btn-group">
                    <button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left dropdown-toggle" data-toggle="dropdown"><b><i class="icon-printer2"></i></b> Cetak</button>
                    <div class="dropdown-menu dropdown-menu-left">
                        <button type="submit" class="dropdown-item" name="versi_cetak" value="pdf"><i class="icon-file-pdf"></i> PDF</button>
                        <button type="submit" class="dropdown-item" name="versi_cetak" value="excel"><i class="icon-file-excel"></i> Excel</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="card card-table table-responsive shadow-0 mb-0 table-new-page-ppkm">

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var primary = document.querySelector('.form-check-input-switchery-primary');
    var switchery = new Switchery(primary, {
        color: '#2196F3'
    });

    get_peserta_new();

    function get_peserta_new() {
        $(".list_kecamatan_arsel").html("");
        $(".list_kecamatan_aruta").html("");
        $(".list_kecamatan_kumai").html("");
        $(".list_kecamatan_kolam").html("");
        $(".list_kecamatan_banteng").html("");
        $(".list_kecamatan_lada").html("");
        let report_today = $("input[name='report_today']").is(":checked");

        if (report_today) {
            $("#cetak_peserta_ppkm_new").hide();
        } else {
            $("#cetak_peserta_ppkm_new").show();
        }

        $.ajax({
            url: base_url + "ppkm/request/get_peserta_kodim_new",
            data: {
                report_today: report_today
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let html_arsel = "";
                $.each(response['arsel'], function(index, value) {
                    html_arsel += "<tr><td>" + value.nama_wilayah + "</td><td></td><td colspan='4'></td></tr>";
                    if (value.rt_domisili) {
                        let split_rt_domisili_arsel = value.rt_domisili.split("|");
                        let split_id_rt_arsel = value.id_master_rt.split("|");
                        let split_jumlah_peserta_arsel = value.jumlah_peserta.split("|");
                        let split_jumlah_rumah_arsel = value.jumlah_rumah.split("|");
                        $.each(split_rt_domisili_arsel, function(index_rt_domisili, value_rt_domisili) {

                            html_arsel += "<tr><td class='text-right'>" + value_rt_domisili + "</td><td class='text-center'>" + split_jumlah_peserta_arsel[index_rt_domisili] + "</td><td class='text-center red-area'><h5>" + (split_jumlah_rumah_arsel[index_rt_domisili] >= 6 ? split_jumlah_rumah_arsel[index_rt_domisili] : '') + "</h5></td><td class='text-center orange-area'><h5>" + (split_jumlah_rumah_arsel[index_rt_domisili] == 3 || split_jumlah_rumah_arsel[index_rt_domisili] == 4 || split_jumlah_rumah_arsel[index_rt_domisili] == 5 ? split_jumlah_rumah_arsel[index_rt_domisili] : '') + "</h5></td><td class='text-center yellow-area'><h5>" + (split_jumlah_rumah_arsel[index_rt_domisili] == 1 || split_jumlah_rumah_arsel[index_rt_domisili] == 2 ? split_jumlah_rumah_arsel[index_rt_domisili] : '') + "</h5></td><td class='text-center green-area'><h5>" + (split_jumlah_rumah_arsel[index_rt_domisili] == 0 ? split_jumlah_rumah_arsel[index_rt_domisili] : '') + "</h5></td></tr>";
                        })
                    }
                })
                $(".list_kecamatan_arsel").html(html_arsel);

                let html_aruta = "";
                $.each(response['aruta'], function(index, value) {
                    html_aruta += "<tr><td>" + value.nama_wilayah + "</td><td></td><td></td></tr>";
                    if (value.rt_domisili) {
                        let split_rt_domisili_aruta = value.rt_domisili.split("|");
                        let split_id_rt_aruta = value.id_master_rt.split("|");
                        let split_jumlah_peserta_aruta = value.jumlah_peserta.split("|");
                        let split_jumlah_rumah_aruta = value.jumlah_rumah.split("|");
                        $.each(split_rt_domisili_aruta, function(index_rt_domisili, value_rt_domisili) {

                            html_aruta += "<tr><td class='text-right'>" + value_rt_domisili + "</td><td class='text-center'>" + split_jumlah_peserta_aruta[index_rt_domisili] + "</td><td class='text-center red-area'><h5>" + (split_jumlah_rumah_aruta[index_rt_domisili] >= 6 ? split_jumlah_rumah_aruta[index_rt_domisili] : '') + "</h5></td><td class='text-center orange-area'><h5>" + (split_jumlah_rumah_aruta[index_rt_domisili] == 3 || split_jumlah_rumah_aruta[index_rt_domisili] == 4 || split_jumlah_rumah_aruta[index_rt_domisili] == 5 ? split_jumlah_rumah_aruta[index_rt_domisili] : '') + "</h5></td><td class='text-center yellow-area'><h5>" + (split_jumlah_rumah_aruta[index_rt_domisili] == 1 || split_jumlah_rumah_aruta[index_rt_domisili] == 2 ? split_jumlah_rumah_aruta[index_rt_domisili] : '') + "</h5></td><td class='text-center green-area'><h5>" + (split_jumlah_rumah_aruta[index_rt_domisili] == 0 ? split_jumlah_rumah_aruta[index_rt_domisili] : '') + "</h5></td></tr>";
                        })
                    }
                })
                $(".list_kecamatan_aruta").html(html_aruta);

                let html_kumai = "";
                $.each(response['kumai'], function(index, value) {
                    html_kumai += "<tr><td>" + value.nama_wilayah + "</td><td></td><td></td></tr>";
                    if (value.rt_domisili) {
                        let split_rt_domisili_kumai = value.rt_domisili.split("|");
                        let split_id_rt_kumai = value.id_master_rt.split("|");
                        let split_jumlah_peserta_kumai = value.jumlah_peserta.split("|");
                        let split_jumlah_rumah_kumai = value.jumlah_rumah.split("|");
                        $.each(split_rt_domisili_kumai, function(index_rt_domisili, value_rt_domisili) {

                            html_kumai += "<tr><td class='text-right'>" + value_rt_domisili + "</td><td class='text-center'>" + split_jumlah_peserta_kumai[index_rt_domisili] + "</td><td class='text-center red-area'><h5>" + (split_jumlah_rumah_kumai[index_rt_domisili] >= 6 ? split_jumlah_rumah_kumai[index_rt_domisili] : '') + "</h5></td><td class='text-center orange-area'><h5>" + (split_jumlah_rumah_kumai[index_rt_domisili] == 3 || split_jumlah_rumah_kumai[index_rt_domisili] == 4 || split_jumlah_rumah_kumai[index_rt_domisili] == 5 ? split_jumlah_rumah_kumai[index_rt_domisili] : '') + "</h5></td><td class='text-center yellow-area'><h5>" + (split_jumlah_rumah_kumai[index_rt_domisili] == 1 || split_jumlah_rumah_kumai[index_rt_domisili] == 2 ? split_jumlah_rumah_kumai[index_rt_domisili] : '') + "</h5></td><td class='text-center green-area'><h5>" + (split_jumlah_rumah_kumai[index_rt_domisili] == 0 ? split_jumlah_rumah_kumai[index_rt_domisili] : '') + "</h5></td></tr>";
                        })
                    }
                })
                $(".list_kecamatan_kumai").html(html_kumai);

                let html_kolam = "";
                $.each(response['kolam'], function(index, value) {
                    html_kolam += "<tr><td>" + value.nama_wilayah + "</td><td></td><td></td></tr>";
                    if (value.rt_domisili) {
                        let split_rt_domisili_kolam = value.rt_domisili.split("|");
                        let split_id_rt_kolam = value.id_master_rt.split("|");
                        let split_jumlah_peserta_kolam = value.jumlah_peserta.split("|");
                        let split_jumlah_rumah_kolam = value.jumlah_rumah.split("|");
                        $.each(split_rt_domisili_kolam, function(index_rt_domisili, value_rt_domisili) {

                            html_kolam += "<tr><td class='text-right'>" + value_rt_domisili + "</td><td class='text-center'>" + split_jumlah_peserta_kolam[index_rt_domisili] + "</td><td class='text-center red-area'><h5>" + (split_jumlah_rumah_kolam[index_rt_domisili] >= 6 ? split_jumlah_rumah_kolam[index_rt_domisili] : '') + "</h5></td><td class='text-center orange-area'><h5>" + (split_jumlah_rumah_kolam[index_rt_domisili] == 3 || split_jumlah_rumah_kolam[index_rt_domisili] == 4 || split_jumlah_rumah_kolam[index_rt_domisili] == 5 ? split_jumlah_rumah_kolam[index_rt_domisili] : '') + "</h5></td><td class='text-center yellow-area'><h5>" + (split_jumlah_rumah_kolam[index_rt_domisili] == 1 || split_jumlah_rumah_kolam[index_rt_domisili] == 2 ? split_jumlah_rumah_kolam[index_rt_domisili] : '') + "</h5></td><td class='text-center green-area'><h5>" + (split_jumlah_rumah_kolam[index_rt_domisili] == 0 ? split_jumlah_rumah_kolam[index_rt_domisili] : '') + "</h5></td></tr>";
                        })
                    }
                })
                $(".list_kecamatan_kolam").html(html_kolam);

                let html_banteng = "";
                $.each(response['banteng'], function(index, value) {
                    html_banteng += "<tr><td>" + value.nama_wilayah + "</td><td></td><td></td></tr>";
                    if (value.rt_domisili) {
                        let split_rt_domisili_banteng = value.rt_domisili.split("|");
                        let split_id_rt_banteng = value.id_master_rt.split("|");
                        let split_jumlah_peserta_banteng = value.jumlah_peserta.split("|");
                        let split_jumlah_rumah_banteng = value.jumlah_rumah.split("|");
                        $.each(split_rt_domisili_banteng, function(index_rt_domisili, value_rt_domisili) {

                            html_banteng += "<tr><td class='text-right'>" + value_rt_domisili + "</td><td class='text-center'>" + split_jumlah_peserta_banteng[index_rt_domisili] + "</td><td class='text-center red-area'><h5>" + (split_jumlah_rumah_banteng[index_rt_domisili] >= 6 ? split_jumlah_rumah_banteng[index_rt_domisili] : '') + "</h5></td><td class='text-center orange-area'><h5>" + (split_jumlah_rumah_banteng[index_rt_domisili] == 3 || split_jumlah_rumah_banteng[index_rt_domisili] == 4 || split_jumlah_rumah_banteng[index_rt_domisili] == 5 ? split_jumlah_rumah_banteng[index_rt_domisili] : '') + "</h5></td><td class='text-center yellow-area'><h5>" + (split_jumlah_rumah_banteng[index_rt_domisili] == 1 || split_jumlah_rumah_banteng[index_rt_domisili] == 2 ? split_jumlah_rumah_banteng[index_rt_domisili] : '') + "</h5></td><td class='text-center green-area'><h5>" + (split_jumlah_rumah_banteng[index_rt_domisili] == 0 ? split_jumlah_rumah_banteng[index_rt_domisili] : '') + "</h5></td></tr>";
                        })
                    }
                })
                $(".list_kecamatan_banteng").html(html_banteng);

                let html_lada = "";
                $.each(response['lada'], function(index, value) {
                    html_lada += "<tr><td>" + value.nama_wilayah + "</td><td></td><td></td></tr>";
                    if (value.rt_domisili) {
                        let split_rt_domisili_lada = value.rt_domisili.split("|");
                        let split_id_rt_lada = value.id_master_rt.split("|");
                        let split_jumlah_peserta_lada = value.jumlah_peserta.split("|");
                        let split_jumlah_rumah_lada = value.jumlah_rumah.split("|");
                        $.each(split_rt_domisili_lada, function(index_rt_domisili, value_rt_domisili) {

                            html_lada += "<tr><td class='text-right'>" + value_rt_domisili + "</td><td class='text-center'>" + split_jumlah_peserta_lada[index_rt_domisili] + "</td><td class='text-center red-area'><h5>" + (split_jumlah_rumah_lada[index_rt_domisili] >= 6 ? split_jumlah_rumah_lada[index_rt_domisili] : '') + "</h5></td><td class='text-center orange-area'><h5>" + (split_jumlah_rumah_lada[index_rt_domisili] == 3 || split_jumlah_rumah_lada[index_rt_domisili] == 4 || split_jumlah_rumah_lada[index_rt_domisili] == 5 ? split_jumlah_rumah_lada[index_rt_domisili] : '') + "</h5></td><td class='text-center yellow-area'><h5>" + (split_jumlah_rumah_lada[index_rt_domisili] == 1 || split_jumlah_rumah_lada[index_rt_domisili] == 2 ? split_jumlah_rumah_lada[index_rt_domisili] : '') + "</h5></td><td class='text-center green-area'><h5>" + (split_jumlah_rumah_lada[index_rt_domisili] == 0 ? split_jumlah_rumah_lada[index_rt_domisili] : '') + "</h5></td></tr>";
                        })
                    }
                })
                $(".list_kecamatan_lada").html(html_lada);
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function show_new_page_ppkm() {
        $("#showNewPagePPKM").modal("show");
        get_akumulasi_desa();
    }

    function get_akumulasi_desa() {
        let report_today = $("input[name='report_today']").is(":checked");
        $.ajax({
            url: base_url + "ppkm/request/get_akumulasi_desa",
            data: {
                report_today: report_today
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let html = "<table class='table table-bordered'>";
                html += "<thead><tr><th rowspan='2' class='text-center'>Kecamatan</th><th colspan='4' class='text-center'>Kelurahan/Desa</th></tr>";
                html += "<tr><th class='text-center'>Level 4</th><th class='text-center'>Level 3</th><th class='text-center'>Level 2</th><th class='text-center'>Level 1</th></tr></thead>";
                html += "<tbody>";
                $.each(response.arsel.data, function(index, value) {
                    if (index == 0) {
                        html += "<tr><td rowspan='" + response.arsel.arr_max + "'>KECAMATAN ARSEL (" + response.arsel.jumlah_des_kel + " DESA/KELURAHAN)</td><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";
                    } else {
                        html += "<tr><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";

                    }
                });
                $.each(response.aruta.data, function(index, value) {
                    if (index == 0) {
                        html += "<tr><td rowspan='" + response.aruta.arr_max + "'>KECAMATAN ARUTA (" + response.aruta.jumlah_des_kel + " DESA/KELURAHAN)</td><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";
                    } else {
                        html += "<tr><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";

                    }
                });
                $.each(response.kumai.data, function(index, value) {
                    if (index == 0) {
                        html += "<tr><td rowspan='" + response.kumai.arr_max + "'>KECAMATAN KUMAI (" + response.kumai.jumlah_des_kel + " DESA/KELURAHAN)</td><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";
                    } else {
                        html += "<tr><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";

                    }
                });
                $.each(response.kolam.data, function(index, value) {
                    if (index == 0) {
                        html += "<tr><td rowspan='" + response.kolam.arr_max + "'>KECAMATAN KOLAM (" + response.kolam.jumlah_des_kel + " DESA/KELURAHAN)</td><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";
                    } else {
                        html += "<tr><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";

                    }
                });
                $.each(response.banteng.data, function(index, value) {
                    if (index == 0) {
                        html += "<tr><td rowspan='" + response.banteng.arr_max + "'>KECAMATAN BANTENG (" + response.banteng.jumlah_des_kel + " DESA/KELURAHAN)</td><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";
                    } else {
                        html += "<tr><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";

                    }
                });
                $.each(response.lada.data, function(index, value) {
                    if (index == 0) {
                        html += "<tr><td rowspan='" + response.lada.arr_max + "'>KECAMATAN LADA (" + response.lada.jumlah_des_kel + " DESA/KELURAHAN)</td><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";
                    } else {
                        html += "<tr><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";

                    }
                });
                html += "</tbody>";
                html += "</table>";

                $(".table-new-page-ppkm").html(html);
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }
</script>