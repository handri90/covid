<style>
    .header_tbl {
        width: 15%;
    }

    .header_tbl2 {
        width: 1%;
    }

    .info-column {
        margin: 4px 0;
    }

    #collectionOneHome {
        visibility: hidden;
        width: 100%;
        margin-left: -50%;
        background-color: #fff;
        color: #fff;
        border-radius: 10px 10px 0 0;
        padding: 16px;
        position: fixed;
        z-index: 10000;
        left: 50%;
        bottom: 0px;
        font-size: 17px;
        border-top: 1px solid orange;
    }

    #collectionOneHome.show {
        visibility: visible;
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }

    @-webkit-keyframes fadein {
        from {
            bottom: 0;
            opacity: 0;
        }

        to {
            bottom: 0px;
            opacity: 1;
        }
    }

    @keyframes fadein {
        from {
            bottom: 0;
            opacity: 0;
        }

        to {
            bottom: 0px;
            opacity: 1;
        }
    }
</style>
<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <input type="hidden" name="id_master_wilayah" value="<?php echo !empty($id_master_wilayah) ? $id_master_wilayah : ""; ?>" />
            <input type="hidden" name="rt_domisili" value="<?php echo !empty($rt_domisili) ? $rt_domisili : ""; ?>" />
            <input type="hidden" name="status_data" value="<?php echo !empty($status_data) ? $status_data : ""; ?>" />
            <input type="hidden" name="status_jenis_pemeriksaan" value="<?php echo !empty($status_jenis_pemeriksaan) ? $status_jenis_pemeriksaan : ""; ?>" />
            <input type="hidden" name="report_today" value="<?php echo !empty($report_today) ? $report_today : ""; ?>" />
            <input type="hidden" name="unique_id" />
            <div class="card card-table table-responsive shadow-0 mb-0">
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="header_tbl">Wilayah</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($wilayah) ? $wilayah->klasifikasi . " " . $wilayah->nama_wilayah : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">RT</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($rt) ? $rt->rt : ""; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="card">
        <?php echo form_open(current_url(), array("id" => "form_polres")); ?>
        <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-highlight">
                <li class="nav-item"><a href="#detail_peserta" onclick="get_detail_peserta()" class="nav-link active" data-toggle="tab">Detail Peserta</a></li>
                <li class="nav-item"><a href="#detail_peserta_rumah" onclick="get_data_peserta_per_rumah()" class="nav-link" data-toggle="tab">Detail Peserta / Rumah</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade show active" id="detail_peserta">
                    <div class="card card-table">
                        <table id="datatablePesertaRapid" class="table datatable-save-state table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Alamat Domisili</th>
                                    <th>Tanggal Terkonfirmasi</th>
                                    <th>RT Domisili</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <?php
                    if ($status_jenis_pemeriksaan != "antigen" && $rt->rt == "00") {
                    ?>
                        <div class="text-right">
                            <button type="submit" onclick="show_confirm_message(event)" class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
                        </div>
                    <?php
                    }
                    ?>
                </div>

                <div class="tab-pane fade" id="detail_peserta_rumah">
                    <div class="card card-table">
                        <table id="datatablePesertaRapidPerRumah" class="table datatable-save-state table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>UNIQUE ID</th>
                                    <th>Nama Peserta</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<div id="modalPemeriksaan" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Biodata</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="card card-table table-responsive shadow-0 mb-0">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td class="header_tbl">Nama</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="nama"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">NIK</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="nik"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">Tanggal Lahir</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="tanggal_lahir"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">Jenis Kelamin</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="jenis_kelamin"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">Pekerjaan</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="pekerjaan"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">Alamat KTP</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="alamat_ktp"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">Alamat Domisili</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="alamat_domisili"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">Nomor Telepon</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="nomor_telepon"></span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let arr_id = [];

    let datatablePesertaRapid = $("#datatablePesertaRapid").DataTable({
        "deferRender": true,
        "ordering": false,
        "paging": false,
        "columns": [{
                "width": "25%"
            },
            null,
            null,
            {
                "width": "20%"
            },
            {
                "width": "10%"
            },
        ]
    });

    let datatablePesertaRapidPerRumah = $("#datatablePesertaRapidPerRumah").DataTable({
        "deferRender": true,
        "ordering": false,
        "paging": false,
        "columns": [{
                "width": "15%"
            },
            null
        ]
    });

    get_detail_peserta();

    function get_detail_peserta() {
        let id_master_wilayah = $("input[name='id_master_wilayah']").val();
        let rt_domisili = $("input[name='rt_domisili']").val();
        let status_data = $("input[name='status_data']").val();
        let status_jenis_pemeriksaan = $("input[name='status_jenis_pemeriksaan']").val();
        let report_today = $("input[name='report_today']").val();
        let path_url = "";
        if (status_jenis_pemeriksaan == "pcr") {
            path_url = 'ppkm/request/get_list_peserta_polres_kodim_new';
        } else if (status_jenis_pemeriksaan == "antigen") {
            path_url = 'ppkm/request/get_list_peserta_polres_kodim_new_antigen';
        }

        datatablePesertaRapid.clear().draw();

        $.ajax({
            url: base_url + path_url,
            data: {
                id_master_wilayah: id_master_wilayah,
                rt_domisili: rt_domisili,
                report_today: report_today
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    let str_preg = JSON.stringify(value.nama).replace(/((^")|("$))/g, "").trim();
                    datatablePesertaRapid.row.add([
                        value.nama,
                        value.alamat_domisili,
                        value.tanggal_terkonfirmasi_custom,
                        (value.rt_domisili_dropdown ? value.rt_domisili_dropdown : ""),
                        "<a href='#detailPeserta' onClick=\"show_detail_peserta('" + value.id_encrypt + "')\" class='btn btn-primary btn-icon'><i class='icon-eye'></i></a> "
                    ]).draw(false);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function show_detail_peserta(id_peserta) {
        $("#modalPemeriksaan").modal("show");
        $(".nama").html("");
        $(".nik").html("");
        $(".tanggal_lahir").html("");
        $(".jenis_kelamin").html("");
        $(".pekerjaan").html("");
        $(".alamat_ktp").html("");
        $(".alamat_domisili").html("");
        $(".nomor_telepon").html("");
        $.ajax({
            url: base_url + 'ppkm/request/get_detail_peserta_tni_polri',
            data: {
                id_peserta: id_peserta
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $(".nama").html(response.nama);
                $(".nik").html(response.nik);
                $(".tanggal_lahir").html(response.tanggal_lahir);
                $(".jenis_kelamin").html(response.jenis_kelamin);
                $(".pekerjaan").html(response.pekerjaan);
                $(".alamat_ktp").html(response.alamat_ktp);
                $(".alamat_domisili").html(response.alamat_domisili);
                $(".nomor_telepon").html(response.telepon);
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function get_data_peserta_per_rumah() {
        let id_master_wilayah = $("input[name='id_master_wilayah']").val();
        let rt_domisili = $("input[name='rt_domisili']").val();
        let status_data = $("input[name='status_data']").val();
        let status_jenis_pemeriksaan = $("input[name='status_jenis_pemeriksaan']").val();
        let report_today = $("input[name='report_today']").val();
        let path_url = "";

        datatablePesertaRapidPerRumah.clear().draw();

        if (status_jenis_pemeriksaan != "antigen") {

            $.ajax({
                url: base_url + 'ppkm/request/get_list_peserta_polres_kodim_per_rumah_new',
                data: {
                    id_master_wilayah: id_master_wilayah,
                    rt_domisili: rt_domisili,
                    report_today: report_today
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $.each(response, function(index, value) {
                        let id_peserta_spl = value.id_peserta.split("|");
                        let nama_peserta_spl = value.nama_peserta.split("|");

                        let str = "";
                        if (id_peserta_spl) {
                            $.each(id_peserta_spl, function(index_spl, val_spl) {
                                str += "<span class='badge badge-info mr-1'><div class='d-inline-flex'><span class='mt-1'>" + nama_peserta_spl[index_spl] + "</span><div></div></div></span>";
                            });
                        }

                        datatablePesertaRapidPerRumah.row.add([
                            value.unique_id,
                            str
                        ]).draw(false);
                    });
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }

    function show_confirm_message(e) {
        e.preventDefault();
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit.fire({
            title: 'Pastikan semua data yang diinput telah sesuai. Perubahan RT Domisili menunggu verifikasi BPBD',
            text: "Lanjutkan untuk simpan?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $('#form_polres').submit();
            } else if (result.dismiss === swal.DismissReason.cancel) {
                return false;
            }
        });
    }

    function select_one_group(e) {
        if ($(e).attr("data-class-group") != "") {
            $('.group_' + $(e).attr("data-class-group") + ' :nth-child(' + ($(":selected", e).index() + 1) + ')').prop('selected', true);
        }
    }
</script>