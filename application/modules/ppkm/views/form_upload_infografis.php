<style>
    .user-image-custom {
        margin-bottom: 10px;
    }
</style>
<div class="content">
    <!-- Form inputs -->
    <div class="card">
        <div class="card-body">
            <?php echo form_open_multipart(); ?>
            <fieldset class="mb-3">
                <legend class="text-uppercase font-size-sm font-weight-bold">Upload Infografis</legend>
                <input type="hidden" name="tanggal_upload" value="<?php echo isset($content) ? $content->tanggal : ''; ?>" />
                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Tipe Upload <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <select class="form-control select-search" name="tipe_upload" required>
                            <option value="">-- Pilih Tipe Upload --</option>
                            <option value="1" <?php echo isset($content) ? ($content->tipe_upload == "1" ? "selected" : "") : ''; ?>>Harian</option>
                            <option value="2" <?php echo isset($content) ? ($content->tipe_upload == "2" ? "selected" : "") : ''; ?>>Mingguan</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Gambar </label>
                    <div class="col-lg-10">
                        <?php
                        if (!empty($content)) {
                        ?>
                            <table border="1" style="margin-bottom:10px">
                                <?php
                                $expl_filename = explode('|', $content->filename);
                                $expl_id_infografis = explode('|', $content->id_infografis);
                                foreach ($expl_filename as $key => $val) {
                                ?>
                                    <tr>
                                        <td style="padding:10px;"><img width="200" src="<?php echo base_url() . $this->config->item('infografis_path') . "/" . $val; ?>"></td>
                                        <td width="100" style="padding:10px;vertical-align:center;text-align:center;"><a class='btn btn-danger btn-icon' onClick="confirm_delete('<?php echo encrypt_data($expl_id_infografis[$key]); ?>',this)" href='#'><i class='icon-trash'></i></a></td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </table>
                        <?php
                        }
                        ?>
                        <input type="file" class="file-input" accept="image/*" multiple="multiple" name="foto_infografis[]" data-show-upload="false" <?php echo isset($content) ? "" : "required"; ?>>
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <!-- /form inputs -->

</div>

<script>
    $(".file-input").on('change', function() {
        $(".user-image-custom").hide();
    });

    $("form").submit(function() {
        let tanggal_upload = $("input[name='tanggal_upload']").val();
        let tipe_upload = $("select[name='tipe_upload']").val();
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });
        let status = true;
        let status2 = true;
        if (tanggal_upload == "") {
            if (tipe_upload == "1") {
                $.ajax({
                    url: base_url + 'ppkm/request/cek_date_upload',
                    async: false,
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        // alert(response);
                        status = response;
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else {
                $.ajax({
                    url: base_url + 'ppkm/request/cek_week_upload',
                    async: false,
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        // alert(response);
                        status2 = response;
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            }
        }



        if (!status || !status2) {
            if (!status) {
                swalInit(
                    'Gagal',
                    'Tanggal Upload Harian Tidak Boleh Sama',
                    'error'
                );
            } else if (!status2) {
                swalInit(
                    'Gagal',
                    'Upload Mingguan Tidak Boleh Sama',
                    'error'
                );
            }
            return false;
        }
    });

    function confirm_delete(id_infografis, elm) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus gambar ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'ppkm/delete_image_infografis',
                    data: {
                        id_infografis: id_infografis
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            $(elm).parent().parent().remove();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                )
            }
        });
    }
</script>