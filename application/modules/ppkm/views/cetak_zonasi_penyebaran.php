<html>

<head>
    <style>
        body {
            font-family: Georgia, 'Times New Roman', serif;
            font-size: 9pt;
            font-weight: normal;
        }

        .head_print {
            text-align: center;
            font-weight: 100;
            line-height: 0.6;
        }

        .wd-number {
            width: 10px;
        }

        .wd-label {
            width: 100px;
        }

        .wd-label-2 {
            width: 60px;

        }

        .wd-peserta {
            width: 200px;
        }

        .wd-peserta-2 {
            width: 310px;
        }

        .wd-titik {
            width: 2px;
        }

        table {
            margin: 5px 0;
        }

        .tbl-sign {
            margin-top: 20px;
            text-align: center;
        }

        .ruler {
            border-bottom: 2px double black;
        }

        .logo {
            float: left;
            width: 20%;
        }

        .deskripsi {
            float: left;
            width: 60%;
        }

        .alamat {
            line-height: 0.5;
        }

        .text-center {
            text-align: center;
        }

        .border-td {
            border: 1px solid #000;
        }

        .border-td-kecamatan {
            border: 1px solid #000;
            text-align: center;
        }

        .green-area {
            border: 1px solid #000;
            background-color: #2c9f45;
            color: #fff;
        }

        .yellow-area {
            border: 1px solid #000;
            background-color: #e4e932;
            color: #000;
        }

        .orange-area {
            border: 1px solid #000;
            background-color: #f48924;
            color: #000;
        }

        .red-area {
            border: 1px solid #000;
            background-color: #e4002b;
            color: #fff;
        }
    </style>
</head>

<body>
    <table width="100%" style="border-collapse:collapse;" autosize="1">
        <thead>
            <tr>
                <th class="border-td" width="20" rowspan="2">Kecamatan</th>
                <th class="border-td" width="160" colspan="4">Kelurahan/Desa</th>
            </tr>
            <tr>
                <th class="border-td" width="20">Level 4</th>
                <th class="border-td" width="20">Level 3</th>
                <th class="border-td" width="20">Level 2</th>
                <th class="border-td" width="20">Level 1</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($zonasi) {
                foreach ($zonasi['arsel']['data'] as $key => $val) {
                    if ($key == 0) {
            ?>
                        <tr>
                            <td class="border-td-kecamatan" rowspan="8">KECAMATAN ARSEL (<?php echo $zonasi['arsel']['jumlah_des_kel']; ?> DESA/KELURAHAN)</td>
                            <td class="red-area"><?php echo $val['3']; ?></td>
                            <td class="orange-area"><?php echo $val['2']; ?></td>
                            <td class="yellow-area"><?php echo $val['1']; ?></td>
                            <td class="green-area"><?php echo $val['0']; ?></td>
                        </tr>
                    <?php
                    } else {
                    ?>
                        <tr>
                            <td class="red-area"><?php echo $val['3']; ?></td>
                            <td class="orange-area"><?php echo $val['2']; ?></td>
                            <td class="yellow-area"><?php echo $val['1']; ?></td>
                            <td class="green-area"><?php echo $val['0']; ?></td>
                        </tr>
                    <?php
                    }
                }

                foreach ($zonasi['aruta']['data'] as $key => $val) {
                    if ($key == 0) {
                    ?>
                        <tr>
                            <td class="border-td-kecamatan" rowspan="<?php echo $zonasi['aruta']['arr_max']; ?>">KECAMATAN ARUTA (<?php echo $zonasi['aruta']['jumlah_des_kel']; ?> DESA/KELURAHAN)</td>
                            <td class="red-area"><?php echo $val['3']; ?></td>
                            <td class="orange-area"><?php echo $val['2']; ?></td>
                            <td class="yellow-area"><?php echo $val['1']; ?></td>
                            <td class="green-area"><?php echo $val['0']; ?></td>
                        </tr>
                    <?php
                    } else {
                    ?>
                        <tr>
                            <td class="red-area"><?php echo $val['3']; ?></td>
                            <td class="orange-area"><?php echo $val['2']; ?></td>
                            <td class="yellow-area"><?php echo $val['1']; ?></td>
                            <td class="green-area"><?php echo $val['0']; ?></td>
                        </tr>
                    <?php
                    }
                }

                foreach ($zonasi['kumai']['data'] as $key => $val) {
                    if ($key == 0) {
                    ?>
                        <tr>
                            <td class="border-td-kecamatan" rowspan="<?php echo $zonasi['kumai']['arr_max']; ?>">KECAMATAN KUMAI (<?php echo $zonasi['kumai']['jumlah_des_kel']; ?> DESA/KELURAHAN)</td>
                            <td class="red-area"><?php echo $val['3']; ?></td>
                            <td class="orange-area"><?php echo $val['2']; ?></td>
                            <td class="yellow-area"><?php echo $val['1']; ?></td>
                            <td class="green-area"><?php echo $val['0']; ?></td>
                        </tr>
                    <?php
                    } else {
                    ?>
                        <tr>
                            <td class="red-area"><?php echo $val['3']; ?></td>
                            <td class="orange-area"><?php echo $val['2']; ?></td>
                            <td class="yellow-area"><?php echo $val['1']; ?></td>
                            <td class="green-area"><?php echo $val['0']; ?></td>
                        </tr>
                    <?php
                    }
                }

                foreach ($zonasi['kolam']['data'] as $key => $val) {
                    if ($key == 0) {
                    ?>
                        <tr>
                            <td class="border-td-kecamatan" rowspan="<?php echo $zonasi['kolam']['arr_max']; ?>">KECAMATAN KOLAM (<?php echo $zonasi['kolam']['jumlah_des_kel']; ?> DESA/KELURAHAN)</td>
                            <td class="red-area"><?php echo $val['3']; ?></td>
                            <td class="orange-area"><?php echo $val['2']; ?></td>
                            <td class="yellow-area"><?php echo $val['1']; ?></td>
                            <td class="green-area"><?php echo $val['0']; ?></td>
                        </tr>
                    <?php
                    } else {
                    ?>
                        <tr>
                            <td class="red-area"><?php echo $val['3']; ?></td>
                            <td class="orange-area"><?php echo $val['2']; ?></td>
                            <td class="yellow-area"><?php echo $val['1']; ?></td>
                            <td class="green-area"><?php echo $val['0']; ?></td>
                        </tr>
                    <?php
                    }
                }

                foreach ($zonasi['banteng']['data'] as $key => $val) {
                    if ($key == 0) {
                    ?>
                        <tr>
                            <td class="border-td-kecamatan" rowspan="<?php echo $zonasi['banteng']['arr_max']; ?>">KECAMATAN BANTENG (<?php echo $zonasi['banteng']['jumlah_des_kel']; ?> DESA/KELURAHAN)</td>
                            <td class="red-area"><?php echo $val['3']; ?></td>
                            <td class="orange-area"><?php echo $val['2']; ?></td>
                            <td class="yellow-area"><?php echo $val['1']; ?></td>
                            <td class="green-area"><?php echo $val['0']; ?></td>
                        </tr>
                    <?php
                    } else {
                    ?>
                        <tr>
                            <td class="red-area"><?php echo $val['3']; ?></td>
                            <td class="orange-area"><?php echo $val['2']; ?></td>
                            <td class="yellow-area"><?php echo $val['1']; ?></td>
                            <td class="green-area"><?php echo $val['0']; ?></td>
                        </tr>
                    <?php
                    }
                }

                foreach ($zonasi['lada']['data'] as $key => $val) {
                    if ($key == 0) {
                    ?>
                        <tr>
                            <td class="border-td-kecamatan" rowspan="<?php echo $zonasi['lada']['arr_max']; ?>">KECAMATAN LADA (<?php echo $zonasi['lada']['jumlah_des_kel']; ?> DESA/KELURAHAN)</td>
                            <td class="red-area"><?php echo $val['3']; ?></td>
                            <td class="orange-area"><?php echo $val['2']; ?></td>
                            <td class="yellow-area"><?php echo $val['1']; ?></td>
                            <td class="green-area"><?php echo $val['0']; ?></td>
                        </tr>
                    <?php
                    } else {
                    ?>
                        <tr>
                            <td class="red-area"><?php echo $val['3']; ?></td>
                            <td class="orange-area"><?php echo $val['2']; ?></td>
                            <td class="yellow-area"><?php echo $val['1']; ?></td>
                            <td class="green-area"><?php echo $val['0']; ?></td>
                        </tr>
            <?php
                    }
                }
            }
            ?>
        </tbody>

    </table>
</body>

</html>