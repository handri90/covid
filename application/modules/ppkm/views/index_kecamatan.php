<style>
    .header_tbl {
        width: 30%;
    }

    .header_tbl2 {
        width: 1%;
    }

    .info-column {
        margin: 4px 0;
    }

    .detail {
        float: right;
        font-weight: 400;
        line-height: 0.6;
        text-shadow: none;
        color: #fff !important;
        top: -5px;
    }

    .green-area {
        background-color: #2c9f45;
        color: #fff;
    }

    .yellow-area {
        background-color: #e4e932;
        color: #000;
    }

    .orange-area {
        background-color: #f48924;
        color: #000;
    }

    .red-area {
        background-color: #e4002b;
        color: #fff;
    }
</style>

<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <div class="text-right">
                <a href="#newPagePPKM" onclick="show_new_page_ppkm()" id="new_page_ppkm" class="btn btn-success">Zonasi Penyebaran Covid 19</a>
            </div>
        </div>
        <div class="card-body">
            <div class="card card-table table-responsive shadow-0 mb-0">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="200" class="text-center">Nama Wilayah / RT</th>
                            <th class="text-center">Jumlah Warga Positif</th>
                            <th class="text-center" colspan='4'>Jumlah Rumah Kasus Aktif</th>
                        </tr>
                    </thead>
                    <tbody class="list_kecamatan">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="showNewPagePPKM" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="card card-table table-responsive shadow-0 mb-0 table-new-page-ppkm">

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#cetak_peserta_ppkm_new").show();
    $("#new_page_ppkm").show();

    get_peserta_new();

    function get_peserta_new() {
        $.ajax({
            url: base_url + "ppkm/request/get_peserta_kodim_new_by_kecamatan",
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let html = "";
                $.each(response, function(index, value) {
                    html += "<tr><td>" + value.nama_wilayah + "</td><td></td><td colspan='4'></td></tr>";
                    if (value.rt_domisili) {
                        let split_rt_domisili = value.rt_domisili.split("|");
                        let split_id_rt = value.id_master_rt.split("|");
                        let split_jumlah_peserta = value.jumlah_peserta.split("|");
                        let split_jumlah_rumah = value.jumlah_rumah.split("|");
                        $.each(split_rt_domisili, function(index_rt_domisili, value_rt_domisili) {

                            html += "<tr><td class='text-right'>" + value_rt_domisili + "</td><td class='text-center'>" + split_jumlah_peserta[index_rt_domisili] + "</td><td class='text-center red-area'><h5>" + (split_jumlah_rumah[index_rt_domisili] >= 6 ? split_jumlah_rumah[index_rt_domisili] : '') + "</h5></td><td class='text-center orange-area'><h5>" + (split_jumlah_rumah[index_rt_domisili] == 3 || split_jumlah_rumah[index_rt_domisili] == 4 || split_jumlah_rumah[index_rt_domisili] == 5 ? split_jumlah_rumah[index_rt_domisili] : '') + "</h5></td><td class='text-center yellow-area'><h5>" + (split_jumlah_rumah[index_rt_domisili] == 1 || split_jumlah_rumah[index_rt_domisili] == 2 ? split_jumlah_rumah[index_rt_domisili] : '') + "</h5></td><td class='text-center green-area'><h5>" + (split_jumlah_rumah[index_rt_domisili] == 0 ? split_jumlah_rumah[index_rt_domisili] : '') + "</h5></td></tr>";
                        })
                    }
                })
                $(".list_kecamatan").html(html);
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function show_new_page_ppkm() {
        $("#showNewPagePPKM").modal("show");
        get_akumulasi_desa();
    }

    function get_akumulasi_desa() {
        let report_today = $("input[name='report_today']").is(":checked");
        $.ajax({
            url: base_url + "ppkm/request/get_akumulasi_desa",
            data: {
                report_today: report_today
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let html = "<table class='table table-bordered'>";
                html += "<thead><tr><th rowspan='2' class='text-center'>Kecamatan</th><th colspan='4' class='text-center'>Kelurahan/Desa</th></tr>";
                html += "<tr><th class='text-center'>Level 4</th><th class='text-center'>Level 3</th><th class='text-center'>Level 2</th><th class='text-center'>Level 1</th></tr></thead>";
                html += "<tbody>";
                $.each(response.arsel.data, function(index, value) {
                    if (index == 0) {
                        html += "<tr><td rowspan='" + response.arsel.arr_max + "'>KECAMATAN ARSEL (" + response.arsel.jumlah_des_kel + " DESA/KELURAHAN)</td><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";
                    } else {
                        html += "<tr><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";

                    }
                });
                $.each(response.aruta.data, function(index, value) {
                    if (index == 0) {
                        html += "<tr><td rowspan='" + response.aruta.arr_max + "'>KECAMATAN ARUTA (" + response.aruta.jumlah_des_kel + " DESA/KELURAHAN)</td><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";
                    } else {
                        html += "<tr><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";

                    }
                });
                $.each(response.kumai.data, function(index, value) {
                    if (index == 0) {
                        html += "<tr><td rowspan='" + response.kumai.arr_max + "'>KECAMATAN KUMAI (" + response.kumai.jumlah_des_kel + " DESA/KELURAHAN)</td><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";
                    } else {
                        html += "<tr><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";

                    }
                });
                $.each(response.kolam.data, function(index, value) {
                    if (index == 0) {
                        html += "<tr><td rowspan='" + response.kolam.arr_max + "'>KECAMATAN KOLAM (" + response.kolam.jumlah_des_kel + " DESA/KELURAHAN)</td><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";
                    } else {
                        html += "<tr><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";

                    }
                });
                $.each(response.banteng.data, function(index, value) {
                    if (index == 0) {
                        html += "<tr><td rowspan='" + response.banteng.arr_max + "'>KECAMATAN BANTENG (" + response.banteng.jumlah_des_kel + " DESA/KELURAHAN)</td><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";
                    } else {
                        html += "<tr><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";

                    }
                });
                $.each(response.lada.data, function(index, value) {
                    if (index == 0) {
                        html += "<tr><td rowspan='" + response.lada.arr_max + "'>KECAMATAN LADA (" + response.lada.jumlah_des_kel + " DESA/KELURAHAN)</td><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";
                    } else {
                        html += "<tr><td class='red-area'>" + value[3] + "</td><td class='orange-area'>" + value[2] + "</td><td class='yellow-area'>" + value[1] + "</td><td class='green-area'>" + value[0] + "</td>";

                    }
                });
                html += "</tbody>";
                html += "</table>";

                $(".table-new-page-ppkm").html(html);
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }
</script>