<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ppkm extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model('peserta/peserta_model', 'peserta_model');
        $this->load->model('peserta/master_wilayah_model', 'master_wilayah_model');
        $this->load->model('migrate_rt_domisili/master_rt_model', 'master_rt_model');
        $this->load->model('peserta_dalam_satu_rumah_model');
        $this->load->model('detail_peserta_dalam_satu_rumah_model');
        $this->load->model('infografis_model');
    }

    public function index()
    {
        $data['breadcrumb'] = [['link' => false, 'content' => 'PPKM', 'is_active' => true]];
        if ($this->session->userdata("id_user") == "61") {
            $this->execute('index_kominfo', $data);
        } else if ($this->session->userdata("id_user") == "67") {
            $this->execute('index_tapem', $data);
        } else if (in_array($this->session->userdata("id_user"), array("70", "71", "72", "73", "74", "75"))) {
            $this->execute('index_kecamatan', $data);
        } else if (in_array($this->session->userdata("id_user"), array("49", "50", "51", "52", "53", "54"))) {
            $this->execute('index_polsek', $data);
        } else {
            $this->execute('index', $data);
        }
    }

    public function detail_peserta_polres_kodim_old($id_master_wilayah, $rt_domisili, $report_today)
    {
        $data['id_master_wilayah'] = $id_master_wilayah;
        $data['report_today'] = $report_today;
        $data['rt_domisili'] = $rt_domisili;
        $data['status_data'] = "old";
        $data['status_jenis_pemeriksaan'] = "pcr";

        $data['wilayah'] = $this->master_wilayah_model->get_by(decrypt_data($id_master_wilayah));
        $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'ppkm', 'content' => 'Peserta', 'is_active' => false], ['link' => false, 'content' => 'Detail Peserta', 'is_active' => true]];
        $data['rt'] = $this->master_rt_model->get_by($rt_domisili);
        if ($this->session->userdata("level_user_id") == "11") {
            $this->execute('detail_peserta_polres_kodim', $data);
        } else if (in_array($this->session->userdata("level_user_id"), array("12", "6"))) {
            $this->execute('detail_peserta', $data);
        }
    }

    public function detail_peserta_polres_kodim_antigen_old($id_master_wilayah, $rt_domisili, $report_today)
    {
        $data['id_master_wilayah'] = $id_master_wilayah;
        $data['report_today'] = $report_today;
        $data['rt_domisili'] = $rt_domisili;
        $data['status_data'] = "old";
        $data['status_jenis_pemeriksaan'] = "antigen";

        $data['wilayah'] = $this->master_wilayah_model->get_by(decrypt_data($id_master_wilayah));
        $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'ppkm', 'content' => 'Peserta', 'is_active' => false], ['link' => false, 'content' => 'Detail Peserta', 'is_active' => true]];
        $this->execute('detail_peserta_polres_kodim', $data);
    }

    public function detail_peserta_polres_kodim_antigen_new($id_master_wilayah, $rt_domisili, $report_today)
    {
        $data['id_master_wilayah'] = $id_master_wilayah;
        $data['rt_domisili'] = $rt_domisili;
        $data['report_today'] = $report_today;
        $data['status_data'] = "new";
        $data['status_jenis_pemeriksaan'] = "antigen";

        $data['wilayah'] = $this->master_wilayah_model->get_by(decrypt_data($id_master_wilayah));
        $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'ppkm', 'content' => 'Peserta', 'is_active' => false], ['link' => false, 'content' => 'Detail Peserta', 'is_active' => true]];
        $this->execute('detail_peserta_polres_kodim', $data);
    }

    public function detail_peserta_polres_kodim_new($id_master_wilayah, $rt_domisili, $report_today)
    {
        $data['report_today'] = $report_today;
        $data['id_master_wilayah'] = $id_master_wilayah;
        $data['rt_domisili'] = $rt_domisili;
        $data['status_data'] = "new";
        $data['status_jenis_pemeriksaan'] = "pcr";
        $data['wilayah'] = $this->master_wilayah_model->get_by(decrypt_data($id_master_wilayah));
        $data['rt'] = $this->master_rt_model->get_by($rt_domisili);
        if (empty($_POST)) {
            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'ppkm', 'content' => 'Peserta', 'is_active' => false], ['link' => false, 'content' => 'Detail Peserta', 'is_active' => true]];
            if ($this->session->userdata("level_user_id") == "11") {
                $this->execute('detail_peserta_polres_kodim', $data);
            } else if (in_array($this->session->userdata("level_user_id"), array("12", "6"))) {
                $this->execute('detail_peserta', $data);
            }
        } else {
            $wh = "";
            if ($report_today == 'true') {
                $wh = "AND DATE_FORMAT(c.created_at,'%Y-%m-%d') ='" . date("Y-m-d") . "'";
            }

            $data_peserta = $this->peserta_model->query(
                "
            SELECT id_peserta
            FROM peserta
            INNER JOIN 
            (
                SELECT *
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                )
            ) AS c ON c.peserta_id=id_peserta
            LEFT JOIN detail_peserta_dalam_satu_rumah ON detail_peserta_dalam_satu_rumah.peserta_id=c.peserta_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
            WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND kelurahan_master_wilayah_id = '" . decrypt_data($id_master_wilayah) . "' AND master_rt_domisili_id = '" . $rt_domisili . "'  " . $wh . "
            "
            )->result();

            if ($this->ipost("from_content") == "bpbd") {
                foreach ($data_peserta as $key => $value) {
                    if ($this->ipost("rt_domisili")[$value->id_peserta] != "") {
                        $detail_peserta_dalam_satu_rumah = $this->detail_peserta_dalam_satu_rumah_model->get(
                            array(
                                "where" => array(
                                    "peserta_id" => $value->id_peserta
                                )
                            ),
                            "row"
                        );

                        if ($detail_peserta_dalam_satu_rumah) {
                            $cek_data_peserta_change = $this->peserta_dalam_satu_rumah_model->get(
                                array(
                                    "where" => array(
                                        "id_peserta_dalam_satu_rumah" => $detail_peserta_dalam_satu_rumah->peserta_dalam_satu_rumah_id,
                                        "master_rt_domisili_id" => $rt_domisili
                                    )
                                )
                            );

                            if ($cek_data_peserta_change) {
                                $data_rt = $this->master_rt_model->get_by(decrypt_data($this->ipost("rt_domisili")[$value->id_peserta]));
                                $rt_domisili_cek = $data_rt->rt;

                                $cek_unique_code = $this->peserta_dalam_satu_rumah_model->get(
                                    array(
                                        "fields" => "count(*) as jumlah",
                                        "where_false" => "unique_id LIKE '" . (strtoupper(str_replace(" ", "", $data['wilayah']->nama_wilayah)) . $rt_domisili_cek) . "%'"
                                    ),
                                    "row"
                                );



                                $str_unique_code = strtoupper(str_replace(" ", "", $data['wilayah']->nama_wilayah)) . $rt_domisili_cek;

                                if ($cek_unique_code->jumlah >= 1) {
                                    $get_last_unique_id = $this->peserta_dalam_satu_rumah_model->get(
                                        array(
                                            "fields"      => "unique_id",
                                            "where_false" => "unique_id LIKE '" . (strtoupper(str_replace(" ", "", $data['wilayah']->nama_wilayah)) . $rt_domisili_cek) . "%'",
                                            "order_by"    => array(
                                                "created_at" => "DESC"
                                            ),
                                            "limit" => "1"
                                        ),
                                        "row"
                                    );

                                    $unique_id = (int) (str_replace((strtoupper(str_replace(" ", "", $data['wilayah']->nama_wilayah)) . $rt_domisili_cek), "", $get_last_unique_id->unique_id));

                                    $prefix = "";
                                    if (strlen($unique_id) == '1') {
                                        $prefix = "000000";
                                    } else if (strlen($unique_id) == '2') {
                                        $prefix = "00000";
                                    } else if (strlen($unique_id) == '3') {
                                        $prefix = "0000";
                                    } else if (strlen($unique_id) == '4') {
                                        $prefix = "000";
                                    } else if (strlen($unique_id) == '5') {
                                        $prefix = "00";
                                    } else if (strlen($unique_id) == '6') {
                                        $prefix = "0";
                                    }

                                    $str_unique_code .= $prefix . ($unique_id + 1);
                                } else {
                                    $str_unique_code .= "0000001";
                                }

                                $data_update_peserta_dalam_satu_rumah = array(
                                    "master_rt_domisili_id" => decrypt_data($this->ipost("rt_domisili")[$value->id_peserta]),
                                    "unique_id" => $str_unique_code,
                                    "created_at" => $this->session->userdata("id_user")
                                );
                            } else {
                                $data_update_peserta_dalam_satu_rumah = array(
                                    "master_rt_domisili_id" => decrypt_data($this->ipost("rt_domisili")[$value->id_peserta]),
                                    "created_at" => $this->session->userdata("id_user")
                                );
                            }

                            $this->peserta_dalam_satu_rumah_model->edit($detail_peserta_dalam_satu_rumah->peserta_dalam_satu_rumah_id, $data_update_peserta_dalam_satu_rumah);
                        }

                        $data_peserta_update = array(
                            "master_rt_domisili_id" => decrypt_data($this->ipost("rt_domisili")[$value->id_peserta]),
                            "updated_at" => $this->datetime()
                        );

                        $this->peserta_model->edit($value->id_peserta, $data_peserta_update);
                    }
                    $data_peserta_update = array(
                        "master_rt_domisili_id_ppkm" => NULL,
                        "updated_at" => $this->datetime()
                    );

                    $this->peserta_model->edit($value->id_peserta, $data_peserta_update);
                }
            } else {
                foreach ($data_peserta as $key => $value) {
                    if ($this->ipost("rt_domisili")[$value->id_peserta] != "") {
                        $data_peserta_update = array(
                            "master_rt_domisili_id_ppkm" => decrypt_data($this->ipost("rt_domisili")[$value->id_peserta]),
                            "updated_at" => $this->datetime()
                        );

                        $this->peserta_model->edit($value->id_peserta, $data_peserta_update);
                    }
                }
            }
            redirect('ppkm/detail_peserta_polres_kodim_new/' . $id_master_wilayah . "/" . $rt_domisili . "/" . $report_today);
        }
    }

    public function save_collection_home()
    {
        $id_master_wilayah = decrypt_data($this->ipost("id_master_wilayah"));
        $rt_domisili = $this->ipost("rt_domisili");
        $rt_domisili_id = $this->ipost("rt_domisili");
        $status_data = $this->ipost("status_data");
        $unique_id = $this->ipost("unique_id");
        $id_peserta = $this->ipost("id_peserta");

        if ($unique_id) {
            $id_detail = $this->peserta_dalam_satu_rumah_model->get(
                array(
                    "fields" => "id_peserta_dalam_satu_rumah",
                    "where" => array(
                        "unique_id" => $unique_id,
                    ),
                ),
                "row"
            );

            foreach ($id_peserta as $key => $val) {
                $id_peserta_decrypt = decrypt_data($val);

                $data_detail_peserta_dalam_satu_rumah = array(
                    "peserta_dalam_satu_rumah_id" => $id_detail->id_peserta_dalam_satu_rumah,
                    "peserta_id" => $id_peserta_decrypt,
                    "created_at" => $this->datetime(),
                    "id_user_created" => $this->session->userdata("id_user")
                );

                $this->detail_peserta_dalam_satu_rumah_model->save($data_detail_peserta_dalam_satu_rumah);
            }

            $id_peserta_dalam_satu_rumah = true;
        } else {
            $wilayah = $this->master_wilayah_model->get_by($id_master_wilayah);

            if ($status_data == "new") {
                $data_rt = $this->master_rt_model->get_by($rt_domisili);
                $rt_domisili = $data_rt->rt;
            }

            $cek_unique_code = $this->peserta_dalam_satu_rumah_model->get(
                array(
                    "fields" => "count(*) as jumlah",
                    "where_false" => "unique_id LIKE '" . (strtoupper(str_replace(" ", "", $wilayah->nama_wilayah)) . $rt_domisili) . "%'"
                ),
                "row"
            );



            $str_unique_code = strtoupper(str_replace(" ", "", $wilayah->nama_wilayah)) . $rt_domisili;

            if ($cek_unique_code->jumlah >= 1) {
                $get_last_unique_id = $this->peserta_dalam_satu_rumah_model->get(
                    array(
                        "fields"      => "unique_id",
                        "where_false" => "unique_id LIKE '" . (strtoupper(str_replace(" ", "", $wilayah->nama_wilayah)) . $rt_domisili) . "%'",
                        "order_by"    => array(
                            "created_at" => "DESC"
                        ),
                        "limit" => "1"
                    ),
                    "row"
                );

                $unique_id = (int) (str_replace((strtoupper(str_replace(" ", "", $wilayah->nama_wilayah)) . $rt_domisili), "", $get_last_unique_id->unique_id));

                $prefix = "";
                if (strlen($unique_id) == '1') {
                    $prefix = "000000";
                } else if (strlen($unique_id) == '2') {
                    $prefix = "00000";
                } else if (strlen($unique_id) == '3') {
                    $prefix = "0000";
                } else if (strlen($unique_id) == '4') {
                    $prefix = "000";
                } else if (strlen($unique_id) == '5') {
                    $prefix = "00";
                } else if (strlen($unique_id) == '6') {
                    $prefix = "0";
                }

                $str_unique_code .= $prefix . ($unique_id + 1);
            } else {
                $str_unique_code .= "0000001";
            }

            if ($status_data == "new") {
                $data_peserta_dalam_satu_rumah = array(
                    "kelurahan_master_wilayah_id" => $id_master_wilayah,
                    "master_rt_domisili_id" => $rt_domisili_id,
                    "unique_id" => $str_unique_code,
                    "created_at" => $this->datetime(),
                    "id_user_created" => $this->session->userdata("id_user")
                );
            } else if ($status_data == "old") {
                $data_peserta_dalam_satu_rumah = array(
                    "kelurahan_master_wilayah_id" => $id_master_wilayah,
                    "rt_domisili" => $rt_domisili,
                    "unique_id" => $str_unique_code,
                    "created_at" => $this->datetime(),
                    "id_user_created" => $this->session->userdata("id_user")
                );
            }

            $id_peserta_dalam_satu_rumah = $this->peserta_dalam_satu_rumah_model->save($data_peserta_dalam_satu_rumah);

            foreach ($id_peserta as $key => $val) {
                $id_peserta_decrypt = decrypt_data($val);

                $data_detail_peserta_dalam_satu_rumah = array(
                    "peserta_dalam_satu_rumah_id" => $id_peserta_dalam_satu_rumah,
                    "peserta_id" => $id_peserta_decrypt,
                    "created_at" => $this->datetime(),
                    "id_user_created" => $this->session->userdata("id_user")
                );

                $this->detail_peserta_dalam_satu_rumah_model->save($data_detail_peserta_dalam_satu_rumah);
            }
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($id_peserta_dalam_satu_rumah));
    }

    public function delete_peserta_per_rumah()
    {
        $unique_id = $this->ipost("unique_id");
        $id_peserta = $this->ipost("id_peserta");

        $id_detail = $this->peserta_dalam_satu_rumah_model->get(
            array(
                "fields" => "id_detail_peserta_dalam_satu_rumah",
                "join" => array(
                    "detail_peserta_dalam_satu_rumah" => "id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL"
                ),
                "where" => array(
                    "unique_id" => $unique_id,
                    "peserta_id" => $id_peserta,
                ),
            ),
            "row"
        );

        $data_delete = array(
            "deleted_at" => $this->datetime(),
            "id_user_deleted" => $this->session->userdata("id_user")
        );

        $status = $this->detail_peserta_dalam_satu_rumah_model->edit($id_detail->id_detail_peserta_dalam_satu_rumah, $data_delete);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function cetak_peserta_ppkm_old()
    {
        require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel.php');
        require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

        $filename = "Manifests.xlsx";

        $objPHPExcel = new PHPExcel();

        $style_colum_jumlah_rumah_hijau = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '2c9f45')
            ),
            'font' => array(
                'color' => array('rgb' => 'ffffff')
            )
        );

        $style_colum_jumlah_rumah_kuning = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'e4e932')
            ),
            'font' => array(
                'color' => array('rgb' => '000000')
            )
        );

        $style_colum_jumlah_rumah_orange = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'f48924')
            ),
            'font' => array(
                'color' => array('rgb' => '000000')
            )
        );

        $style_colum_jumlah_rumah_merah = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'be0027')
            ),
            'font' => array(
                'color' => array('rgb' => 'ffffff')
            )
        );

        $style_data_tengah = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );

        $style_data_kanan = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
            )
        );

        $style_data = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $style_header = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true
            ), 'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'dae1f3')
            )
        );

        $objPHPExcel->setActiveSheetIndex(0);

        $data_peserta_arsel = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != ''
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620101' AND rt_domisili IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "Nama Wilayah / RT");
        $objPHPExcel->getActiveSheet()->SetCellValue("B1", "Jumlah Warga Positif");
        $objPHPExcel->getActiveSheet()->SetCellValue("C1", "Jumlah Rumah Kasus Aktif");
        $objPHPExcel->getActiveSheet()->getStyle("A1:C1")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->mergeCells("C1:F1");

        if ($data_peserta_arsel) {
            $row = 2;
            foreach ($data_peserta_arsel as $key => $val) {
                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val->nama_wilayah);
                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("C" . $row . ":F" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->mergeCells("C" . $row . ":F" . $row);
                $row++;

                if ($val->rt_domisili) {
                    $spl_rt_domisili = explode("|", $val->rt_domisili);
                    $spl_jumlah_peserta = explode("|", $val->jumlah_peserta);
                    foreach ($spl_rt_domisili as $key_rt => $val_rt) {
                        $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                            "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$val->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.rt_domisili = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != ''
                            GROUP BY unique_id
                        ) AS tbl
                        "
                        )->row();

                        if ($this->session->userdata("id_user") == "61") {
                            if ($jumlah_rumah->jumlah_rumah != "0") {
                                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val_rt);
                                $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                    "A" . $row,
                                    $val_rt,
                                    PHPExcel_Cell_DataType::TYPE_STRING
                                );
                                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                                $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                                if ($jumlah_rumah->jumlah_rumah == '0') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                                } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                                } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                                } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                                }
                                $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                                $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                                $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                                $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                                // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                                $row++;
                            }
                        } else {
                            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val_rt);
                            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                "A" . $row,
                                $val_rt,
                                PHPExcel_Cell_DataType::TYPE_STRING
                            );
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                            if ($jumlah_rumah->jumlah_rumah == '0') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                            }
                            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                            $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                            $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                            // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                            $row++;
                        }
                    }
                }
            }
        }

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(false);

        $objPHPExcel->getActiveSheet()->setTitle('Kecamatan Arut Selatan');

        $objPHPExcel->createSheet();

        /* Add some data to the second sheet, resembling some different data types*/
        $objPHPExcel->setActiveSheetIndex(1);

        $data_peserta_aruta = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != ''
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620102' AND rt_domisili IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "Nama Wilayah / RT");
        $objPHPExcel->getActiveSheet()->SetCellValue("B1", "Jumlah Warga Positif");
        $objPHPExcel->getActiveSheet()->SetCellValue("C1", "Jumlah Rumah Kasus Aktif");
        $objPHPExcel->getActiveSheet()->getStyle("A1:C1")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->mergeCells("C1:F1");

        if ($data_peserta_aruta) {
            $row = 2;
            foreach ($data_peserta_aruta as $key => $val) {
                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val->nama_wilayah);
                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("C" . $row . ":F" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->mergeCells("C" . $row . ":F" . $row);
                $row++;
                if ($val->rt_domisili) {
                    $spl_rt_domisili = explode("|", $val->rt_domisili);
                    $spl_jumlah_peserta = explode("|", $val->jumlah_peserta);
                    foreach ($spl_rt_domisili as $key_rt => $val_rt) {
                        $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                            "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$val->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.rt_domisili = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != ''
                            GROUP BY unique_id
                        ) AS tbl
                        "
                        )->row();

                        if ($this->session->userdata("id_user") == "61") {
                            if ($jumlah_rumah->jumlah_rumah != "0") {
                                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val_rt);
                                $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                    "A" . $row,
                                    $val_rt,
                                    PHPExcel_Cell_DataType::TYPE_STRING
                                );
                                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                                $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                                if ($jumlah_rumah->jumlah_rumah == '0') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                                } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                                } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                                } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                                }
                                $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                                $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                                $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                                $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                                // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                                $row++;
                            }
                        } else {
                            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val_rt);
                            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                "A" . $row,
                                $val_rt,
                                PHPExcel_Cell_DataType::TYPE_STRING
                            );
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                            if ($jumlah_rumah->jumlah_rumah == '0') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                            }
                            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                            $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                            $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                            // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                            $row++;
                        }
                    }
                }
            }
        }

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(false);

        /* Rename 2nd sheet*/
        $objPHPExcel->getActiveSheet()->setTitle('Kecamatan Arut Utara');

        $objPHPExcel->createSheet();

        /* Add some data to the second sheet, resembling some different data types*/
        $objPHPExcel->setActiveSheetIndex(2);

        $data_peserta_kumai = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != ''
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620103' AND rt_domisili IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "Nama Wilayah / RT");
        $objPHPExcel->getActiveSheet()->SetCellValue("B1", "Jumlah Warga Positif");
        $objPHPExcel->getActiveSheet()->SetCellValue("C1", "Jumlah Rumah Kasus Aktif");
        $objPHPExcel->getActiveSheet()->getStyle("A1:C1")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->mergeCells("C1:F1");

        if ($data_peserta_kumai) {
            $row = 2;
            foreach ($data_peserta_kumai as $key => $val) {
                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val->nama_wilayah);
                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("C" . $row . ":F" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->mergeCells("C" . $row . ":F" . $row);
                $row++;
                if ($val->rt_domisili) {
                    $spl_rt_domisili = explode("|", $val->rt_domisili);
                    $spl_jumlah_peserta = explode("|", $val->jumlah_peserta);
                    foreach ($spl_rt_domisili as $key_rt => $val_rt) {
                        $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                            "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$val->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.rt_domisili = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != ''
                            GROUP BY unique_id
                        ) AS tbl
                        "
                        )->row();

                        if ($this->session->userdata("id_user") == "61") {
                            if ($jumlah_rumah->jumlah_rumah != "0") {
                                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val_rt);
                                $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                    "A" . $row,
                                    $val_rt,
                                    PHPExcel_Cell_DataType::TYPE_STRING
                                );
                                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                                $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                                if ($jumlah_rumah->jumlah_rumah == '0') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                                } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                                } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                                } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                                }
                                $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                                $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                                $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                                $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                                // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                                $row++;
                            }
                        } else {
                            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val_rt);
                            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                "A" . $row,
                                $val_rt,
                                PHPExcel_Cell_DataType::TYPE_STRING
                            );
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                            if ($jumlah_rumah->jumlah_rumah == '0') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                            }
                            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                            $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                            $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                            // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                            $row++;
                        }
                    }
                }
            }
        }

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(false);

        /* Rename 2nd sheet*/
        $objPHPExcel->getActiveSheet()->setTitle('Kecamatan Kumai');

        $objPHPExcel->createSheet();

        /* Add some data to the second sheet, resembling some different data types*/
        $objPHPExcel->setActiveSheetIndex(3);

        $data_peserta_kolam = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != ''
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620104' AND rt_domisili IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "Nama Wilayah / RT");
        $objPHPExcel->getActiveSheet()->SetCellValue("B1", "Jumlah Warga Positif");
        $objPHPExcel->getActiveSheet()->SetCellValue("C1", "Jumlah Rumah Kasus Aktif");
        $objPHPExcel->getActiveSheet()->getStyle("A1:C1")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->mergeCells("C1:F1");

        if ($data_peserta_kolam) {
            $row = 2;
            foreach ($data_peserta_kolam as $key => $val) {
                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val->nama_wilayah);
                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("C" . $row . ":F" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->mergeCells("C" . $row . ":F" . $row);
                $row++;
                if ($val->rt_domisili) {
                    $spl_rt_domisili = explode("|", $val->rt_domisili);
                    $spl_jumlah_peserta = explode("|", $val->jumlah_peserta);
                    foreach ($spl_rt_domisili as $key_rt => $val_rt) {
                        $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                            "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$val->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.rt_domisili = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != ''
                            GROUP BY unique_id
                        ) AS tbl
                        "
                        )->row();

                        if ($this->session->userdata("id_user") == "61") {
                            if ($jumlah_rumah->jumlah_rumah != "0") {
                                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val_rt);
                                $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                    "A" . $row,
                                    $val_rt,
                                    PHPExcel_Cell_DataType::TYPE_STRING
                                );
                                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                                $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                                if ($jumlah_rumah->jumlah_rumah == '0') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                                } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                                } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                                } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                                }
                                $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                                $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                                $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                                $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                                // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                                $row++;
                            }
                        } else {
                            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val_rt);
                            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                "A" . $row,
                                $val_rt,
                                PHPExcel_Cell_DataType::TYPE_STRING
                            );
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                            if ($jumlah_rumah->jumlah_rumah == '0') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                            }
                            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                            $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                            $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                            // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                            $row++;
                        }
                    }
                }
            }
        }

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(false);

        /* Rename 2nd sheet*/
        $objPHPExcel->getActiveSheet()->setTitle('Kecamatan Kolam');

        $objPHPExcel->createSheet();

        /* Add some data to the second sheet, resembling some different data types*/
        $objPHPExcel->setActiveSheetIndex(4);

        $data_peserta_banteng = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != ''
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620106' AND rt_domisili IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "Nama Wilayah / RT");
        $objPHPExcel->getActiveSheet()->SetCellValue("B1", "Jumlah Warga Positif");
        $objPHPExcel->getActiveSheet()->SetCellValue("C1", "Jumlah Rumah Kasus Aktif");
        $objPHPExcel->getActiveSheet()->getStyle("A1:C1")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->mergeCells("C1:F1");

        if ($data_peserta_banteng) {
            $row = 2;
            foreach ($data_peserta_banteng as $key => $val) {
                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val->nama_wilayah);
                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("C" . $row . ":F" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->mergeCells("C" . $row . ":F" . $row);
                $row++;
                if ($val->rt_domisili) {
                    $spl_rt_domisili = explode("|", $val->rt_domisili);
                    $spl_jumlah_peserta = explode("|", $val->jumlah_peserta);
                    foreach ($spl_rt_domisili as $key_rt => $val_rt) {
                        $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                            "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$val->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.rt_domisili = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != ''
                            GROUP BY unique_id
                        ) AS tbl
                        "
                        )->row();

                        if ($this->session->userdata("id_user") == "61") {
                            if ($jumlah_rumah->jumlah_rumah != "0") {
                                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val_rt);
                                $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                    "A" . $row,
                                    $val_rt,
                                    PHPExcel_Cell_DataType::TYPE_STRING
                                );
                                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                                $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                                if ($jumlah_rumah->jumlah_rumah == '0') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                                } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                                } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                                } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                                }
                                $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                                $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                                $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                                $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                                // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                                $row++;
                            }
                        } else {
                            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val_rt);
                            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                "A" . $row,
                                $val_rt,
                                PHPExcel_Cell_DataType::TYPE_STRING
                            );
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                            if ($jumlah_rumah->jumlah_rumah == '0') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                            }
                            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                            $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                            $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                            // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                            $row++;
                        }
                    }
                }
            }
        }

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(false);

        /* Rename 2nd sheet*/
        $objPHPExcel->getActiveSheet()->setTitle('Kecamatan Pangkalan Banteng');

        $objPHPExcel->createSheet();

        /* Add some data to the second sheet, resembling some different data types*/
        $objPHPExcel->setActiveSheetIndex(5);

        $data_peserta_lada = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != ''
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620105' AND rt_domisili IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "Nama Wilayah / RT");
        $objPHPExcel->getActiveSheet()->SetCellValue("B1", "Jumlah Warga Positif");
        $objPHPExcel->getActiveSheet()->SetCellValue("C1", "Jumlah Rumah Kasus Aktif");
        $objPHPExcel->getActiveSheet()->getStyle("A1:C1")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->mergeCells("C1:F1");

        if ($data_peserta_lada) {
            $row = 2;
            foreach ($data_peserta_lada as $key => $val) {
                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val->nama_wilayah);
                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("C" . $row . ":F" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->mergeCells("C" . $row . ":F" . $row);
                $row++;
                if ($val->rt_domisili) {
                    $spl_rt_domisili = explode("|", $val->rt_domisili);
                    $spl_jumlah_peserta = explode("|", $val->jumlah_peserta);
                    foreach ($spl_rt_domisili as $key_rt => $val_rt) {
                        $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                            "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$val->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.rt_domisili = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != ''
                            GROUP BY unique_id
                        ) AS tbl
                        "
                        )->row();

                        if ($this->session->userdata("id_user") == "61") {
                            if ($jumlah_rumah->jumlah_rumah != "0") {
                                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val_rt);
                                $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                    "A" . $row,
                                    $val_rt,
                                    PHPExcel_Cell_DataType::TYPE_STRING
                                );
                                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                                $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                                if ($jumlah_rumah->jumlah_rumah == '0') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                                } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                                } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                                } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                                }
                                $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                                $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                                $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                                $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                                // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                                $row++;
                            }
                        } else {
                            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val_rt);
                            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                "A" . $row,
                                $val_rt,
                                PHPExcel_Cell_DataType::TYPE_STRING
                            );
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                            if ($jumlah_rumah->jumlah_rumah == '0') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                            }
                            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                            $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                            $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                            // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                            $row++;
                        }
                    }
                }
            }
        }

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(false);

        /* Rename 2nd sheet*/
        $objPHPExcel->getActiveSheet()->setTitle('Kecamatan Pangkalan Lada');

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $writer->save('php://output');
        exit;
    }

    public function cetak_peserta_ppkm_new()
    {
        require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel.php');
        require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

        $filename = "Manifests.xlsx";

        $objPHPExcel = new PHPExcel();

        $style_colum_jumlah_rumah_hijau = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '2c9f45')
            ),
            'font' => array(
                'color' => array('rgb' => 'ffffff')
            )
        );

        $style_colum_jumlah_rumah_kuning = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'e4e932')
            ),
            'font' => array(
                'color' => array('rgb' => '000000')
            )
        );

        $style_colum_jumlah_rumah_orange = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'f48924')
            ),
            'font' => array(
                'color' => array('rgb' => '000000')
            )
        );

        $style_colum_jumlah_rumah_merah = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'be0027')
            ),
            'font' => array(
                'color' => array('rgb' => 'ffffff')
            )
        );

        $style_data_tengah = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );

        $style_data_kanan = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
            )
        );

        $style_data = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $style_header = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true
            ), 'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'dae1f3')
            )
        );

        $objPHPExcel->setActiveSheetIndex(0);

        $data_peserta_arsel = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt,id_master_rt
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
	            WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL
                GROUP BY kelurahan_master_wilayah_id,rt
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620101' AND rt IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "Nama Wilayah / RT");
        $objPHPExcel->getActiveSheet()->SetCellValue("B1", "Jumlah Warga Positif");
        $objPHPExcel->getActiveSheet()->SetCellValue("C1", "Jumlah Rumah Kasus Aktif");
        $objPHPExcel->getActiveSheet()->getStyle("A1:C1")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->mergeCells("C1:F1");

        if ($data_peserta_arsel) {
            $row = 2;
            foreach ($data_peserta_arsel as $key => $val) {
                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val->nama_wilayah);
                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("C" . $row . ":F" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->mergeCells("C" . $row . ":F" . $row);
                $row++;

                if ($val->id_master_rt) {
                    $spl_rt_domisili = explode("|", $val->id_master_rt);
                    $spl_rt = explode("|", $val->rt_domisili);
                    $spl_jumlah_peserta = explode("|", $val->jumlah_peserta);
                    foreach ($spl_rt_domisili as $key_rt => $val_rt) {
                        $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                            "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$val->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.master_rt_domisili_id = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                            GROUP BY unique_id
                        ) AS tbl
                        "
                        )->row();

                        if ($this->session->userdata("id_user") == "61") {
                            if ($jumlah_rumah->jumlah_rumah != "0") {
                                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $spl_rt[$key_rt]);
                                $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                    "A" . $row,
                                    $spl_rt[$key_rt],
                                    PHPExcel_Cell_DataType::TYPE_STRING
                                );
                                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                                $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                                if ($jumlah_rumah->jumlah_rumah == '0') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                                } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                                } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                                } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                                }
                                // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                                $row++;
                            }
                        } else {
                            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $spl_rt[$key_rt]);
                            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                "A" . $row,
                                $spl_rt[$key_rt],
                                PHPExcel_Cell_DataType::TYPE_STRING
                            );
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                            if ($jumlah_rumah->jumlah_rumah == '0') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                            }
                            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                            $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                            $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                            // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                            $row++;
                        }
                    }
                }
            }
        }

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(false);

        $objPHPExcel->getActiveSheet()->setTitle('Kecamatan Arut Selatan');

        $objPHPExcel->createSheet();

        /* Add some data to the second sheet, resembling some different data types*/
        $objPHPExcel->setActiveSheetIndex(1);

        $data_peserta_aruta = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt,id_master_rt
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
	            WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL
                GROUP BY kelurahan_master_wilayah_id,rt
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620102' AND rt IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "Nama Wilayah / RT");
        $objPHPExcel->getActiveSheet()->SetCellValue("B1", "Jumlah Warga Positif");
        $objPHPExcel->getActiveSheet()->SetCellValue("C1", "Jumlah Rumah Kasus Aktif");
        $objPHPExcel->getActiveSheet()->getStyle("A1:C1")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->mergeCells("C1:F1");

        if ($data_peserta_aruta) {
            $row = 2;
            foreach ($data_peserta_aruta as $key => $val) {
                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val->nama_wilayah);
                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("C" . $row . ":F" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->mergeCells("C" . $row . ":F" . $row);
                $row++;
                if ($val->id_master_rt) {
                    $spl_rt_domisili = explode("|", $val->id_master_rt);
                    $spl_rt = explode("|", $val->rt_domisili);
                    $spl_jumlah_peserta = explode("|", $val->jumlah_peserta);
                    foreach ($spl_rt_domisili as $key_rt => $val_rt) {
                        $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                            "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$val->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.master_rt_domisili_id = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                            GROUP BY unique_id
                        ) AS tbl
                        "
                        )->row();

                        if ($this->session->userdata("id_user") == "61") {
                            if ($jumlah_rumah->jumlah_rumah != "0") {
                                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $spl_rt[$key_rt]);
                                $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                    "A" . $row,
                                    $spl_rt[$key_rt],
                                    PHPExcel_Cell_DataType::TYPE_STRING
                                );
                                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                                $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                                if ($jumlah_rumah->jumlah_rumah == '0') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                                } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                                } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                                } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                                }
                                // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                                $row++;
                            }
                        } else {
                            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $spl_rt[$key_rt]);
                            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                "A" . $row,
                                $spl_rt[$key_rt],
                                PHPExcel_Cell_DataType::TYPE_STRING
                            );
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                            if ($jumlah_rumah->jumlah_rumah == '0') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                            }
                            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                            $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                            $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                            // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                            $row++;
                        }
                    }
                }
            }
        }

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(false);

        /* Rename 2nd sheet*/
        $objPHPExcel->getActiveSheet()->setTitle('Kecamatan Arut Utara');

        $objPHPExcel->createSheet();

        /* Add some data to the second sheet, resembling some different data types*/
        $objPHPExcel->setActiveSheetIndex(2);

        $data_peserta_kumai = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt,id_master_rt
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
	            WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL
                GROUP BY kelurahan_master_wilayah_id,rt
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620103' AND rt IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "Nama Wilayah / RT");
        $objPHPExcel->getActiveSheet()->SetCellValue("B1", "Jumlah Warga Positif");
        $objPHPExcel->getActiveSheet()->SetCellValue("C1", "Jumlah Rumah Kasus Aktif");
        $objPHPExcel->getActiveSheet()->getStyle("A1:C1")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->mergeCells("C1:F1");

        if ($data_peserta_kumai) {
            $row = 2;
            foreach ($data_peserta_kumai as $key => $val) {
                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val->nama_wilayah);
                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("C" . $row . ":F" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->mergeCells("C" . $row . ":F" . $row);
                $row++;
                if ($val->id_master_rt) {
                    $spl_rt_domisili = explode("|", $val->id_master_rt);
                    $spl_rt = explode("|", $val->rt_domisili);
                    $spl_jumlah_peserta = explode("|", $val->jumlah_peserta);
                    foreach ($spl_rt_domisili as $key_rt => $val_rt) {
                        $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                            "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$val->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.master_rt_domisili_id = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                            GROUP BY unique_id
                        ) AS tbl
                        "
                        )->row();

                        if ($this->session->userdata("id_user") == "61") {
                            if ($jumlah_rumah->jumlah_rumah != "0") {
                                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $spl_rt[$key_rt]);
                                $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                    "A" . $row,
                                    $spl_rt[$key_rt],
                                    PHPExcel_Cell_DataType::TYPE_STRING
                                );
                                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                                $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                                if ($jumlah_rumah->jumlah_rumah == '0') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                                } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                                } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                                } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                                }
                                // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                                $row++;
                            }
                        } else {
                            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $spl_rt[$key_rt]);
                            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                "A" . $row,
                                $spl_rt[$key_rt],
                                PHPExcel_Cell_DataType::TYPE_STRING
                            );
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                            if ($jumlah_rumah->jumlah_rumah == '0') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                            }
                            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                            $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                            $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                            // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                            $row++;
                        }
                    }
                }
            }
        }

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(false);

        /* Rename 2nd sheet*/
        $objPHPExcel->getActiveSheet()->setTitle('Kecamatan Kumai');

        $objPHPExcel->createSheet();

        /* Add some data to the second sheet, resembling some different data types*/
        $objPHPExcel->setActiveSheetIndex(3);

        $data_peserta_kolam = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt,id_master_rt
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
	            WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL
                GROUP BY kelurahan_master_wilayah_id,rt
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620104' AND rt IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "Nama Wilayah / RT");
        $objPHPExcel->getActiveSheet()->SetCellValue("B1", "Jumlah Warga Positif");
        $objPHPExcel->getActiveSheet()->SetCellValue("C1", "Jumlah Rumah Kasus Aktif");
        $objPHPExcel->getActiveSheet()->getStyle("A1:C1")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->mergeCells("C1:F1");

        if ($data_peserta_kolam) {
            $row = 2;
            foreach ($data_peserta_kolam as $key => $val) {
                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val->nama_wilayah);
                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("C" . $row . ":F" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->mergeCells("C" . $row . ":F" . $row);
                $row++;
                if ($val->id_master_rt) {
                    $spl_rt_domisili = explode("|", $val->id_master_rt);
                    $spl_rt = explode("|", $val->rt_domisili);
                    $spl_jumlah_peserta = explode("|", $val->jumlah_peserta);
                    foreach ($spl_rt_domisili as $key_rt => $val_rt) {
                        $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                            "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$val->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.master_rt_domisili_id = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                            GROUP BY unique_id
                        ) AS tbl
                        "
                        )->row();

                        if ($this->session->userdata("id_user") == "61") {
                            if ($jumlah_rumah->jumlah_rumah != "0") {
                                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $spl_rt[$key_rt]);
                                $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                    "A" . $row,
                                    $spl_rt[$key_rt],
                                    PHPExcel_Cell_DataType::TYPE_STRING
                                );
                                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                                $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                                if ($jumlah_rumah->jumlah_rumah == '0') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                                } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                                } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                                } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                                }
                                // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                                $row++;
                            }
                        } else {
                            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $spl_rt[$key_rt]);
                            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                "A" . $row,
                                $spl_rt[$key_rt],
                                PHPExcel_Cell_DataType::TYPE_STRING
                            );
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                            if ($jumlah_rumah->jumlah_rumah == '0') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                            }
                            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                            $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                            $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                            // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                            $row++;
                        }
                    }
                }
            }
        }

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(false);

        /* Rename 2nd sheet*/
        $objPHPExcel->getActiveSheet()->setTitle('Kecamatan Kolam');

        $objPHPExcel->createSheet();

        /* Add some data to the second sheet, resembling some different data types*/
        $objPHPExcel->setActiveSheetIndex(4);

        $data_peserta_banteng = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt,id_master_rt
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
	            WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL
                GROUP BY kelurahan_master_wilayah_id,rt
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620106' AND rt IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "Nama Wilayah / RT");
        $objPHPExcel->getActiveSheet()->SetCellValue("B1", "Jumlah Warga Positif");
        $objPHPExcel->getActiveSheet()->SetCellValue("C1", "Jumlah Rumah Kasus Aktif");
        $objPHPExcel->getActiveSheet()->getStyle("A1:C1")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->mergeCells("C1:F1");

        if ($data_peserta_banteng) {
            $row = 2;
            foreach ($data_peserta_banteng as $key => $val) {
                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val->nama_wilayah);
                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("C" . $row . ":F" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->mergeCells("C" . $row . ":F" . $row);
                $row++;
                if ($val->id_master_rt) {
                    $spl_rt_domisili = explode("|", $val->id_master_rt);
                    $spl_rt = explode("|", $val->rt_domisili);
                    $spl_jumlah_peserta = explode("|", $val->jumlah_peserta);
                    foreach ($spl_rt_domisili as $key_rt => $val_rt) {
                        $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                            "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$val->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.master_rt_domisili_id = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                            GROUP BY unique_id
                        ) AS tbl
                        "
                        )->row();

                        if ($this->session->userdata("id_user") == "61") {
                            if ($jumlah_rumah->jumlah_rumah != "0") {
                                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $spl_rt[$key_rt]);
                                $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                    "A" . $row,
                                    $spl_rt[$key_rt],
                                    PHPExcel_Cell_DataType::TYPE_STRING
                                );
                                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                                $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                                if ($jumlah_rumah->jumlah_rumah == '0') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                                } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                                } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                                } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                                }
                                // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                                $row++;
                            }
                        } else {
                            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $spl_rt[$key_rt]);
                            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                "A" . $row,
                                $spl_rt[$key_rt],
                                PHPExcel_Cell_DataType::TYPE_STRING
                            );
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                            if ($jumlah_rumah->jumlah_rumah == '0') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                            }
                            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                            $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                            $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                            // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                            $row++;
                        }
                    }
                }
            }
        }

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(false);

        /* Rename 2nd sheet*/
        $objPHPExcel->getActiveSheet()->setTitle('Kecamatan Pangkalan Banteng');

        $objPHPExcel->createSheet();

        /* Add some data to the second sheet, resembling some different data types*/
        $objPHPExcel->setActiveSheetIndex(5);

        $data_peserta_lada = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt,id_master_rt
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
	            WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL
                GROUP BY kelurahan_master_wilayah_id,rt
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620105' AND rt IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "Nama Wilayah / RT");
        $objPHPExcel->getActiveSheet()->SetCellValue("B1", "Jumlah Warga Positif");
        $objPHPExcel->getActiveSheet()->SetCellValue("C1", "Jumlah Rumah Kasus Aktif");
        $objPHPExcel->getActiveSheet()->getStyle("A1:C1")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->mergeCells("C1:F1");

        if ($data_peserta_lada) {
            $row = 2;
            foreach ($data_peserta_lada as $key => $val) {
                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val->nama_wilayah);
                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->getStyle("C" . $row . ":F" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->mergeCells("C" . $row . ":F" . $row);
                $row++;
                if ($val->id_master_rt) {
                    $spl_rt_domisili = explode("|", $val->id_master_rt);
                    $spl_rt = explode("|", $val->rt_domisili);
                    $spl_jumlah_peserta = explode("|", $val->jumlah_peserta);
                    foreach ($spl_rt_domisili as $key_rt => $val_rt) {
                        $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                            "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$val->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.master_rt_domisili_id = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                            GROUP BY unique_id
                        ) AS tbl
                        "
                        )->row();

                        if ($this->session->userdata("id_user") == "61") {
                            if ($jumlah_rumah->jumlah_rumah != "0") {
                                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $spl_rt[$key_rt]);
                                $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                    "A" . $row,
                                    $spl_rt[$key_rt],
                                    PHPExcel_Cell_DataType::TYPE_STRING
                                );
                                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                                $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                                if ($jumlah_rumah->jumlah_rumah == '0') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                                } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                                } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                                } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                                }
                                // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                                $row++;
                            }
                        } else {
                            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $spl_rt[$key_rt]);
                            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                                "A" . $row,
                                $spl_rt[$key_rt],
                                PHPExcel_Cell_DataType::TYPE_STRING
                            );
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_kanan);
                            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $spl_jumlah_peserta[$key_rt]);
                            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data_tengah);

                            if ($jumlah_rumah->jumlah_rumah == '0') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '1' || $jumlah_rumah->jumlah_rumah == '2') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah == '3' || $jumlah_rumah->jumlah_rumah == '4' || $jumlah_rumah->jumlah_rumah == '5') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $jumlah_rumah->jumlah_rumah);
                            } else if ($jumlah_rumah->jumlah_rumah >= '6') {
                                $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $jumlah_rumah->jumlah_rumah);
                            }
                            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);
                            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);
                            $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);
                            $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                            // $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);
                            $row++;
                        }
                    }
                }
            }
        }

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(false);

        /* Rename 2nd sheet*/
        $objPHPExcel->getActiveSheet()->setTitle('Kecamatan Pangkalan Lada');

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $writer->save('php://output');
        exit;
    }

    public function upload_infografis()
    {
        $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'ppkm', 'content' => 'PPKM', 'is_active' => false], ['link' => false, 'content' => 'Upload Infografis', 'is_active' => true]];
        $this->execute('index_upload_infografis', $data);
    }

    public function upload_infografis_form()
    {
        if (empty($_FILES)) {
            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'ppkm', 'content' => 'PPKM', 'is_active' => false], ['link' => true, 'url' => base_url() . 'ppkm/upload_infografis', 'content' => 'Upload Infografis', 'is_active' => false], ['link' => false, 'content' => 'Form Upload Infografis', 'is_active' => true]];
            $this->execute('form_upload_infografis', $data);
        } else {
            $upload_path = $this->config->item('infografis_path');
            $name_field = 'foto_infografis';
            $status = "";

            if ((!file_exists($upload_path)) && !(is_dir($upload_path))) {
                mkdir($upload_path);
            }
            $config = array();
            $filename = md5(uniqid(rand(), true));
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['file_name'] = $filename;

            $count_files = count($_FILES[$name_field]['name']);
            $files = $_FILES;

            if ($count_files != 0) {
                if ($count_files <= 4) {
                    for ($i = 0; $i < $count_files; $i++) {
                        if ($files[$name_field]['name'][$i]) {
                            $_FILES[$name_field]['name'] = $files[$name_field]['name'][$i];
                            $_FILES[$name_field]['type'] = $files[$name_field]['type'][$i];
                            $_FILES[$name_field]['tmp_name'] = $files[$name_field]['tmp_name'][$i];
                            $_FILES[$name_field]['error'] = $files[$name_field]['error'][$i];
                            $_FILES[$name_field]['size'] = $files[$name_field]['size'][$i];

                            $this->load->library('upload', $config);

                            if (!$this->upload->do_upload($name_field)) {
                                $status = array('error' => $this->upload->display_errors());
                            } else {
                                $status = array('data' => $this->upload->data());
                            }

                            $filename = $status['data']['file_name'];
                            $tanggal_upload = date('Y-m-d H:i:s');

                            $data_infografis = array(
                                "tipe_upload" => $this->ipost("tipe_upload"),
                                "filename" => $filename,
                                "created_at" => $this->datetime()
                            );

                            $this->infografis_model->save($data_infografis);
                        }
                    }
                    $this->session->set_flashdata('message', 'Data berhasil diupload');
                    redirect('ppkm/upload_infografis');
                } else {
                    $this->session->set_flashdata('message', 'Data gagal diupload, jumlah maksimal upload file hanya 4');
                    $this->session->set_flashdata('error', 'danger');
                    redirect('ppkm/upload_infografis_form');
                }
            } else {
                $this->session->set_flashdata('message', 'Data gagal diupload');
                $this->session->set_flashdata('error', 'danger');
                redirect('ppkm/upload_infografis_form');
            }
        }
    }

    public function edit_upload_infografis($tanggal, $tipe_upload)
    {
        $data_master = $this->infografis_model->get(
            array(
                "fields" => "GROUP_CONCAT(id_infografis ORDER BY id_infografis SEPARATOR '|') AS id_infografis, GROUP_CONCAT(filename ORDER BY id_infografis SEPARATOR '|') AS filename, DATE_FORMAT(created_at,'%Y-%m-%d') AS tanggal",
                "where_false" => "DATE_FORMAT(created_at,'%Y-%m-%d') = '${tanggal}'",
                "group_by" => "tanggal",
            ),
            "row"
        );

        if (!$data_master) {
            $this->page_error();
        }

        if (empty($_FILES)) {
            $data['content'] = $data_master;
            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'ppkm', 'content' => 'PPKM', 'is_active' => false], ['link' => true, 'url' => base_url() . 'ppkm/upload_infografis', 'content' => 'Upload Infografis', 'is_active' => false], ['link' => false, 'content' => 'Form Upload Infografis', 'is_active' => true]];
            $this->execute('form_upload_infografis', $data);
        } else {
            $upload_path = $this->config->item('infografis_path');
            $name_field = 'foto_infografis';
            $status = "";

            if ((!file_exists($upload_path)) && !(is_dir($upload_path))) {
                mkdir($upload_path);
            }
            $config = array();
            $filename = md5(uniqid(rand(), true));
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['file_name'] = $filename;

            $count_files = count($_FILES[$name_field]['name']);
            $files = $_FILES;

            if ($count_files != 0) {
                for ($i = 0; $i < $count_files; $i++) {
                    if ($files[$name_field]['name'][$i]) {
                        $_FILES[$name_field]['name'] = $files[$name_field]['name'][$i];
                        $_FILES[$name_field]['type'] = $files[$name_field]['type'][$i];
                        $_FILES[$name_field]['tmp_name'] = $files[$name_field]['tmp_name'][$i];
                        $_FILES[$name_field]['error'] = $files[$name_field]['error'][$i];
                        $_FILES[$name_field]['size'] = $files[$name_field]['size'][$i];

                        $this->load->library('upload', $config);

                        if (!$this->upload->do_upload($name_field)) {
                            $status = array('error' => $this->upload->display_errors());
                        } else {
                            $status = array('data' => $this->upload->data());
                        }

                        $filename = $status['data']['file_name'];
                        $tanggal_upload = date('Y-m-d H:i:s');

                        $data_infografis = array(
                            "tipe_upload" => $tipe_upload,
                            "filename" => $filename,
                            "created_at" => $this->datetime()
                        );

                        $this->infografis_model->save($data_infografis);
                    }
                }
            }
            $this->session->set_flashdata('message', 'Data berhasil diupload');

            redirect('ppkm/upload_infografis');
        }
    }

    public function delete_infografis()
    {
        $tanggal = $this->iget('tanggal');
        $data_delete = array(
            "deleted_at" => $this->datetime()
        );

        $status = $this->infografis_model->query(
            "
            UPDATE infografis SET deleted_at = '" . $this->datetime() . "' WHERE DATE_FORMAT(created_at,'%Y-%m-%d') = '${tanggal}'
            "
        );

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function delete_image_infografis()
    {
        $id_infografis = decrypt_data($this->iget('id_infografis'));
        $data_master = $this->infografis_model->get_by($id_infografis);

        if (!$data_master) {
            $this->page_error();
        }

        $status = $this->infografis_model->remove($id_infografis);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function cetak_laporan_zonasi()
    {
        $report_today = $this->iget("report_today");
        $versi_cetak = $this->iget("versi_cetak");

        $wh = "";
        if ($report_today == 'true') {
            $wh = "AND DATE_FORMAT(c.created_at,'%Y-%m-%d') ='" . date("Y-m-d") . "'";
        }

        $data_peserta_arsel = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY jumlah_rumah DESC SEPARATOR '|') AS jumlah_rumah
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL AND kelurahan_master_wilayah_id=master_wilayah_id
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN 
                    (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis
                        WHERE deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620101'
            GROUP BY id_master_wilayah
            "
        )->result();

        $arr_hijau_arsel = array();
        $arr_kuning_arsel = array();
        $arr_orange_arsel = array();
        $arr_merah_arsel = array();
        foreach ($data_peserta_arsel as $key => $val) {
            $expl_jumlah_rumah_arsel = explode("|", $val->jumlah_rumah);
            if ($expl_jumlah_rumah_arsel[0] == '0') {
                array_push($arr_hijau_arsel, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_arsel[0] == '1' || $expl_jumlah_rumah_arsel[0] == '2') {
                array_push($arr_kuning_arsel, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_arsel[0] == '3' || $expl_jumlah_rumah_arsel[0] == '4' || $expl_jumlah_rumah_arsel[0] == '5') {
                array_push($arr_orange_arsel, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_arsel[0] >= '6') {
                array_push($arr_merah_arsel, $val->nama_wilayah);
            }
        }

        $arr_max_arsel = max(count($arr_hijau_arsel), count($arr_kuning_arsel), count($arr_orange_arsel), count($arr_merah_arsel));

        $col_arr_arsel = array();
        for ($i = 0; $i < $arr_max_arsel; $i++) {
            $col_arr_arsel[$i] = array(!empty($arr_hijau_arsel[$i]) ? $arr_hijau_arsel[$i] : "", !empty($arr_kuning_arsel[$i]) ? $arr_kuning_arsel[$i] : "", !empty($arr_orange_arsel[$i]) ? $arr_orange_arsel[$i] : "", !empty($arr_merah_arsel[$i]) ? $arr_merah_arsel[$i] : "");
        }

        $data_peserta_aruta = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY jumlah_rumah DESC SEPARATOR '|') AS jumlah_rumah
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL AND kelurahan_master_wilayah_id=master_wilayah_id
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN 
                    (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis
                        WHERE deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620102'
            GROUP BY id_master_wilayah
            "
        )->result();

        $arr_hijau_aruta = array();
        $arr_kuning_aruta = array();
        $arr_orange_aruta = array();
        $arr_merah_aruta = array();
        foreach ($data_peserta_aruta as $key => $val) {
            $expl_jumlah_rumah_aruta = explode("|", $val->jumlah_rumah);
            if ($expl_jumlah_rumah_aruta[0] == '0') {
                array_push($arr_hijau_aruta, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_aruta[0] == '1' || $expl_jumlah_rumah_aruta[0] == '2') {
                array_push($arr_kuning_aruta, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_aruta[0] == '3' || $expl_jumlah_rumah_aruta[0] == '4' || $expl_jumlah_rumah_aruta[0] == '5') {
                array_push($arr_orange_aruta, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_aruta[0] >= '6') {
                array_push($arr_merah_aruta, $val->nama_wilayah);
            }
        }

        $arr_max_aruta = max(count($arr_hijau_aruta), count($arr_kuning_aruta), count($arr_orange_aruta), count($arr_merah_aruta));

        $col_arr_aruta = array();
        for ($i = 0; $i < $arr_max_aruta; $i++) {
            $col_arr_aruta[$i] = array(!empty($arr_hijau_aruta[$i]) ? $arr_hijau_aruta[$i] : "", !empty($arr_kuning_aruta[$i]) ? $arr_kuning_aruta[$i] : "", !empty($arr_orange_aruta[$i]) ? $arr_orange_aruta[$i] : "", !empty($arr_merah_aruta[$i]) ? $arr_merah_aruta[$i] : "");
        }

        $data_peserta_kumai = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY jumlah_rumah DESC SEPARATOR '|') AS jumlah_rumah
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL AND kelurahan_master_wilayah_id=master_wilayah_id
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN 
                    (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis
                        WHERE deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620103'
            GROUP BY id_master_wilayah
            "
        )->result();

        $arr_hijau_kumai = array();
        $arr_kuning_kumai = array();
        $arr_orange_kumai = array();
        $arr_merah_kumai = array();
        foreach ($data_peserta_kumai as $key => $val) {
            $expl_jumlah_rumah_kumai = explode("|", $val->jumlah_rumah);
            if ($expl_jumlah_rumah_kumai[0] == '0') {
                array_push($arr_hijau_kumai, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_kumai[0] == '1' || $expl_jumlah_rumah_kumai[0] == '2') {
                array_push($arr_kuning_kumai, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_kumai[0] == '3' || $expl_jumlah_rumah_kumai[0] == '4' || $expl_jumlah_rumah_kumai[0] == '5') {
                array_push($arr_orange_kumai, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_kumai[0] >= '6') {
                array_push($arr_merah_kumai, $val->nama_wilayah);
            }
        }

        $arr_max_kumai = max(count($arr_hijau_kumai), count($arr_kuning_kumai), count($arr_orange_kumai), count($arr_merah_kumai));

        $col_arr_kumai = array();
        for ($i = 0; $i < $arr_max_kumai; $i++) {
            $col_arr_kumai[$i] = array(!empty($arr_hijau_kumai[$i]) ? $arr_hijau_kumai[$i] : "", !empty($arr_kuning_kumai[$i]) ? $arr_kuning_kumai[$i] : "", !empty($arr_orange_kumai[$i]) ? $arr_orange_kumai[$i] : "", !empty($arr_merah_kumai[$i]) ? $arr_merah_kumai[$i] : "");
        }

        $data_peserta_kolam = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY jumlah_rumah DESC SEPARATOR '|') AS jumlah_rumah
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL AND kelurahan_master_wilayah_id=master_wilayah_id
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN 
                    (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis
                        WHERE deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620104'
            GROUP BY id_master_wilayah
            "
        )->result();

        $arr_hijau_kolam = array();
        $arr_kuning_kolam = array();
        $arr_orange_kolam = array();
        $arr_merah_kolam = array();
        foreach ($data_peserta_kolam as $key => $val) {
            $expl_jumlah_rumah_kolam = explode("|", $val->jumlah_rumah);
            if ($expl_jumlah_rumah_kolam[0] == '0') {
                array_push($arr_hijau_kolam, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_kolam[0] == '1' || $expl_jumlah_rumah_kolam[0] == '2') {
                array_push($arr_kuning_kolam, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_kolam[0] == '3' || $expl_jumlah_rumah_kolam[0] == '4' || $expl_jumlah_rumah_kolam[0] == '5') {
                array_push($arr_orange_kolam, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_kolam[0] >= '6') {
                array_push($arr_merah_kolam, $val->nama_wilayah);
            }
        }

        $arr_max_kolam = max(count($arr_hijau_kolam), count($arr_kuning_kolam), count($arr_orange_kolam), count($arr_merah_kolam));

        $col_arr_kolam = array();
        for ($i = 0; $i < $arr_max_kolam; $i++) {
            $col_arr_kolam[$i] = array(!empty($arr_hijau_kolam[$i]) ? $arr_hijau_kolam[$i] : "", !empty($arr_kuning_kolam[$i]) ? $arr_kuning_kolam[$i] : "", !empty($arr_orange_kolam[$i]) ? $arr_orange_kolam[$i] : "", !empty($arr_merah_kolam[$i]) ? $arr_merah_kolam[$i] : "");
        }

        $data_peserta_lada = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY jumlah_rumah DESC SEPARATOR '|') AS jumlah_rumah
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL AND kelurahan_master_wilayah_id=master_wilayah_id
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN 
                    (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis
                        WHERE deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620105'
            GROUP BY id_master_wilayah
            "
        )->result();

        $arr_hijau_lada = array();
        $arr_kuning_lada = array();
        $arr_orange_lada = array();
        $arr_merah_lada = array();
        foreach ($data_peserta_lada as $key => $val) {
            $expl_jumlah_rumah_lada = explode("|", $val->jumlah_rumah);
            if ($expl_jumlah_rumah_lada[0] == '0') {
                array_push($arr_hijau_lada, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_lada[0] == '1' || $expl_jumlah_rumah_lada[0] == '2') {
                array_push($arr_kuning_lada, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_lada[0] == '3' || $expl_jumlah_rumah_lada[0] == '4' || $expl_jumlah_rumah_lada[0] == '5') {
                array_push($arr_orange_lada, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_lada[0] >= '6') {
                array_push($arr_merah_lada, $val->nama_wilayah);
            }
        }

        $arr_max_lada = max(count($arr_hijau_lada), count($arr_kuning_lada), count($arr_orange_lada), count($arr_merah_lada));

        $col_arr_lada = array();
        for ($i = 0; $i < $arr_max_lada; $i++) {
            $col_arr_lada[$i] = array(!empty($arr_hijau_lada[$i]) ? $arr_hijau_lada[$i] : "", !empty($arr_kuning_lada[$i]) ? $arr_kuning_lada[$i] : "", !empty($arr_orange_lada[$i]) ? $arr_orange_lada[$i] : "", !empty($arr_merah_lada[$i]) ? $arr_merah_lada[$i] : "");
        }

        $data_peserta_banteng = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY jumlah_rumah DESC SEPARATOR '|') AS jumlah_rumah
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL AND kelurahan_master_wilayah_id=master_wilayah_id
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN 
                    (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis
                        WHERE deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620106'
            GROUP BY id_master_wilayah
            "
        )->result();

        $arr_hijau_banteng = array();
        $arr_kuning_banteng = array();
        $arr_orange_banteng = array();
        $arr_merah_banteng = array();
        foreach ($data_peserta_banteng as $key => $val) {
            $expl_jumlah_rumah_banteng = explode("|", $val->jumlah_rumah);
            if ($expl_jumlah_rumah_banteng[0] == '0') {
                array_push($arr_hijau_banteng, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_banteng[0] == '1' || $expl_jumlah_rumah_banteng[0] == '2') {
                array_push($arr_kuning_banteng, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_banteng[0] == '3' || $expl_jumlah_rumah_banteng[0] == '4' || $expl_jumlah_rumah_banteng[0] == '5') {
                array_push($arr_orange_banteng, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_banteng[0] >= '6') {
                array_push($arr_merah_banteng, $val->nama_wilayah);
            }
        }

        $arr_max_banteng = max(count($arr_hijau_banteng), count($arr_kuning_banteng), count($arr_orange_banteng), count($arr_merah_banteng));

        $col_arr_banteng = array();
        for ($i = 0; $i < $arr_max_banteng; $i++) {
            $col_arr_banteng[$i] = array(!empty($arr_hijau_banteng[$i]) ? $arr_hijau_banteng[$i] : "", !empty($arr_kuning_banteng[$i]) ? $arr_kuning_banteng[$i] : "", !empty($arr_orange_banteng[$i]) ? $arr_orange_banteng[$i] : "", !empty($arr_merah_banteng[$i]) ? $arr_merah_banteng[$i] : "");
        }

        $data = array("arsel" => array("data" => $col_arr_arsel, "arr_max" => $arr_max_arsel, "jumlah_des_kel" => count($data_peserta_arsel)), "aruta" => array("data" => $col_arr_aruta, "arr_max" => $arr_max_aruta, "jumlah_des_kel" => count($data_peserta_aruta)), "kumai" => array("data" => $col_arr_kumai, "arr_max" => $arr_max_kumai, "jumlah_des_kel" => count($data_peserta_kumai)), "kolam" => array("data" => $col_arr_kolam, "arr_max" => $arr_max_kolam, "jumlah_des_kel" => count($data_peserta_kolam)), "banteng" => array("data" => $col_arr_banteng, "arr_max" => $arr_max_banteng, "jumlah_des_kel" => count($data_peserta_banteng)), "lada" => array("data" => $col_arr_lada, "arr_max" => $arr_max_lada, "jumlah_des_kel" => count($data_peserta_lada)));

        if ($versi_cetak == "pdf") {
            $this->cetak_pdf($data);
        } else if ($versi_cetak == "excel") {
            $this->cetak_excel($data);
        }
    }

    public function cetak_excel($zonasi)
    {
        require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel.php');
        require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

        $filename = "Laporan.xlsx";

        $objPHPExcel = new PHPExcel();

        $style_header = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true
            ), 'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        $style_data = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $style_data_kecamatan = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ), 'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        $style_colum_jumlah_rumah_hijau = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '2c9f45')
            )
        );

        $style_colum_jumlah_rumah_kuning = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'e4e932')
            )
        );

        $style_colum_jumlah_rumah_orange = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'f48924')
            )
        );

        $style_colum_jumlah_rumah_merah = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'be0027')
            )
        );

        $objPHPExcel->getActiveSheet()->SetCellValue("B2", "Kecamatan");
        $objPHPExcel->getActiveSheet()->mergeCells("B2:B3");
        $objPHPExcel->getActiveSheet()->getStyle("B2:B3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->SetCellValue("C2", "Kelurahan/Desa");
        $objPHPExcel->getActiveSheet()->mergeCells("C2:F2");
        $objPHPExcel->getActiveSheet()->getStyle("C2:F2")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->SetCellValue("C3", "Level 4");
        $objPHPExcel->getActiveSheet()->getStyle("C3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->SetCellValue("D3", "Level 3");
        $objPHPExcel->getActiveSheet()->getStyle("D3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->SetCellValue("E3", "Level 2");
        $objPHPExcel->getActiveSheet()->getStyle("E3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->SetCellValue("F3", "Level 1");
        $objPHPExcel->getActiveSheet()->getStyle("F3")->applyFromArray($style_header);
        // $objPHPExcel->getActiveSheet()->SetCellValue("D1", "Jenis Kelamin");
        // $objPHPExcel->getActiveSheet()->SetCellValue("E1", "Pekerjaan");
        // $objPHPExcel->getActiveSheet()->SetCellValue("F1", "Alamat KTP");
        // $objPHPExcel->getActiveSheet()->SetCellValue("G1", "RT KTP");
        // $objPHPExcel->getActiveSheet()->SetCellValue("H1", "Alamat Domisili");
        // $objPHPExcel->getActiveSheet()->SetCellValue("I1", "RT Domisili");
        // $objPHPExcel->getActiveSheet()->SetCellValue("J1", "Nomor Telepon");
        // $objPHPExcel->getActiveSheet()->SetCellValue("K1", "Puskemas");
        // $objPHPExcel->getActiveSheet()->SetCellValue("L1", "Desa/Kel");
        // $objPHPExcel->getActiveSheet()->SetCellValue("M1", "Kecamatan");

        if ($zonasi) {
            $row = 4;
            $row_merge_kecamatan = 3;
            foreach ($zonasi['arsel']['data'] as $key => $value) {
                if ($key == 0) {
                    $row_merge_kecamatan += $zonasi['arsel']['arr_max'];
                    $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, "KECAMATAN ARSEL (" . $zonasi['arsel']['jumlah_des_kel'] . " DESA/KELURAHAN)");
                    $objPHPExcel->getActiveSheet()->mergeCells("B" . $row . ":B" . $row_merge_kecamatan);
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $row . ":B" . $row_merge_kecamatan)->applyFromArray($style_data_kecamatan);
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $row . ":B" . $objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);

                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value[3]);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);

                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $value[2]);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);

                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $value[1]);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);

                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $value[0]);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                } else {
                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value[3]);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);

                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $value[2]);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);

                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $value[1]);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);

                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $value[0]);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                }

                $row++;
            }

            foreach ($zonasi['aruta']['data'] as $key => $value) {
                if ($key == 0) {
                    $row_merge_kecamatan += $zonasi['aruta']['arr_max'];
                    $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, "KECAMATAN ARUTA (" . $zonasi['aruta']['jumlah_des_kel'] . " DESA/KELURAHAN)");
                    $objPHPExcel->getActiveSheet()->mergeCells("B" . $row . ":B" . $row_merge_kecamatan);
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $row . ":B" . $row_merge_kecamatan)->applyFromArray($style_data_kecamatan);
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $row . ":B" . $objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);

                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value[3]);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);

                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $value[2]);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);

                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $value[1]);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);

                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $value[0]);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                } else {
                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value[3]);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);

                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $value[2]);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);

                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $value[1]);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);

                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $value[0]);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                }

                $row++;
            }

            foreach ($zonasi['kumai']['data'] as $key => $value) {
                if ($key == 0) {
                    $row_merge_kecamatan += $zonasi['kumai']['arr_max'];
                    $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, "KECAMATAN KUMAI (" . $zonasi['kumai']['jumlah_des_kel'] . " DESA/KELURAHAN)");
                    $objPHPExcel->getActiveSheet()->mergeCells("B" . $row . ":B" . $row_merge_kecamatan);
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $row . ":B" . $row_merge_kecamatan)->applyFromArray($style_data_kecamatan);
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $row . ":B" . $objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);

                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value[3]);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);

                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $value[2]);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);

                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $value[1]);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);

                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $value[0]);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                } else {
                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value[3]);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);

                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $value[2]);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);

                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $value[1]);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);

                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $value[0]);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                }

                $row++;
            }

            foreach ($zonasi['kolam']['data'] as $key => $value) {
                if ($key == 0) {
                    $row_merge_kecamatan += $zonasi['kolam']['arr_max'];
                    $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, "KECAMATAN KOLAM (" . $zonasi['kolam']['jumlah_des_kel'] . " DESA/KELURAHAN)");
                    $objPHPExcel->getActiveSheet()->mergeCells("B" . $row . ":B" . $row_merge_kecamatan);
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $row . ":B" . $row_merge_kecamatan)->applyFromArray($style_data_kecamatan);
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $row . ":B" . $objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);

                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value[3]);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);

                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $value[2]);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);

                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $value[1]);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);

                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $value[0]);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                } else {
                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value[3]);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);

                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $value[2]);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);

                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $value[1]);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);

                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $value[0]);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                }

                $row++;
            }

            foreach ($zonasi['banteng']['data'] as $key => $value) {
                if ($key == 0) {
                    $row_merge_kecamatan += $zonasi['banteng']['arr_max'];
                    $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, "KECAMATAN BANTENG (" . $zonasi['banteng']['jumlah_des_kel'] . " DESA/KELURAHAN)");
                    $objPHPExcel->getActiveSheet()->mergeCells("B" . $row . ":B" . $row_merge_kecamatan);
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $row . ":B" . $row_merge_kecamatan)->applyFromArray($style_data_kecamatan);
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $row . ":B" . $objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);

                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value[3]);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);

                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $value[2]);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);

                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $value[1]);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);

                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $value[0]);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                } else {
                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value[3]);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);

                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $value[2]);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);

                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $value[1]);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);

                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $value[0]);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                }

                $row++;
            }

            foreach ($zonasi['lada']['data'] as $key => $value) {
                if ($key == 0) {
                    $row_merge_kecamatan += $zonasi['lada']['arr_max'];
                    $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, "KECAMATAN LADA (" . $zonasi['lada']['jumlah_des_kel'] . " DESA/KELURAHAN)");
                    $objPHPExcel->getActiveSheet()->mergeCells("B" . $row . ":B" . $row_merge_kecamatan);
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $row . ":B" . $row_merge_kecamatan)->applyFromArray($style_data_kecamatan);
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $row . ":B" . $objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);

                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value[3]);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);

                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $value[2]);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);

                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $value[1]);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);

                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $value[0]);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                } else {
                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value[3]);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_colum_jumlah_rumah_merah);

                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $value[2]);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_colum_jumlah_rumah_orange);

                    $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $value[1]);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_colum_jumlah_rumah_kuning);

                    $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $value[0]);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_colum_jumlah_rumah_hijau);
                }

                $row++;
            }
        }

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }

        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(false);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $writer->save('php://output');
        exit;
    }

    public function cetak_pdf($zonasi)
    {
        $data['zonasi'] = $zonasi;
        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'tempDir' => '/tmp']);
        $data = $this->load->view('cetak_zonasi_penyebaran', $data, TRUE);
        $mpdf->WriteHTML($data);
        $mpdf->Output();
    }

    public function detail_peserta_polsek($id_master_wilayah, $rt_domisili)
    {
        $data['wilayah'] = $this->master_wilayah_model->get_by(decrypt_data($id_master_wilayah));
        $data['rt'] = $this->master_rt_model->get_by($rt_domisili);
        if (empty($_POST)) {
            $data['id_master_wilayah'] = $id_master_wilayah;
            $data['rt_domisili'] = $rt_domisili;
            $data['status_data'] = "new";
            $data['status_jenis_pemeriksaan'] = "pcr";
            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'ppkm', 'content' => 'Peserta', 'is_active' => false], ['link' => false, 'content' => 'Detail Peserta', 'is_active' => true]];
            $this->execute('detail_peserta_polsek', $data);
        } else {
            $data_peserta = $this->peserta_model->query(
                "
            SELECT id_peserta,master_rt_domisili_id_ppkm
            FROM peserta
            INNER JOIN 
            (
                SELECT *
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                )
            ) AS c ON c.peserta_id=id_peserta
            LEFT JOIN detail_peserta_dalam_satu_rumah ON detail_peserta_dalam_satu_rumah.peserta_id=c.peserta_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
            WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND kelurahan_master_wilayah_id = '" . decrypt_data($id_master_wilayah) . "' AND master_rt_domisili_id = '" . $rt_domisili . "'
            "
            )->result();

            foreach ($data_peserta as $key => $value) {
                if ($this->ipost("rt_domisili")[$value->id_peserta] != "") {
                    $data_peserta_update = array(
                        "master_rt_domisili_id_ppkm" => decrypt_data($this->ipost("rt_domisili")[$value->id_peserta]),
                        "updated_at" => $this->datetime()
                    );

                    $this->peserta_model->edit($value->id_peserta, $data_peserta_update);
                } else if (isset($value->master_rt_domisili_id_ppkm)) {
                    $data_peserta_update = array(
                        "master_rt_domisili_id_ppkm" => NULL,
                        "updated_at" => $this->datetime()
                    );

                    $this->peserta_model->edit($value->id_peserta, $data_peserta_update);
                }
            }
            redirect('ppkm/detail_peserta_polsek/' . $id_master_wilayah . "/" . $rt_domisili);
        }
    }

    public function make_notify_puskesmas()
    {
        $checked = $this->ipost("checked");
        $id_peserta = decrypt_data($this->ipost("id_peserta"));

        if ($checked == "true") {
            $data = array(
                "notify_sembuh_from_polsek" => "1"
            );
        } else {
            $data = array(
                "notify_sembuh_from_polsek" => NULL
            );
        }

        $this->peserta_model->edit($id_peserta, $data);
    }
}
