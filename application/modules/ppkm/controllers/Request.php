<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('peserta/peserta_model', 'peserta_model');
        $this->load->model('peserta_dalam_satu_rumah_model');
        $this->load->model('detail_peserta_dalam_satu_rumah_model');
        $this->load->model('peserta/master_wilayah_model', 'master_wilayah_model');
        $this->load->model('migrate_rt_domisili/master_rt_model', 'master_rt_model');
        $this->load->model('infografis_model');
    }

    public function get_peserta_kodim()
    {
        $report_today = $this->iget("report_today");

        $wh = "";
        if ($report_today == 'true') {
            $wh = "AND DATE_FORMAT(c.created_at,'%Y-%m-%d') ='" . date("Y-m-d") . "'";
        }

        $data_peserta_arsel = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(master_rt_domisili_id ORDER BY rt_domisili SEPARATOR '|') AS master_rt_domisili_id
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili,IFNULL(master_rt_domisili_id,'') AS master_rt_domisili_id
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != '' " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620101' AND rt_domisili IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_arsel = array();
        foreach ($data_peserta_arsel as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_arsel[$key][$keys] = $rows;
            }

            $templist_arsel[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);

            $arr_jumlah_rumah_arsel = array();
            if ($row->rt_domisili) {
                $expl_rt_domisili = explode("|", $row->rt_domisili);
                foreach ($expl_rt_domisili as $key_rt => $val_rt) {
                    $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                        "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$row->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.rt_domisili = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != ''
                            GROUP BY unique_id
                        ) AS tbl
                        "
                    )->row();

                    array_push($arr_jumlah_rumah_arsel, $jumlah_rumah->jumlah_rumah);
                }
            }
            $templist_arsel[$key]['jumlah_rumah'] = implode("|", $arr_jumlah_rumah_arsel);
        }

        $data_peserta_aruta = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(master_rt_domisili_id ORDER BY rt_domisili SEPARATOR '|') AS master_rt_domisili_id
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili,IFNULL(master_rt_domisili_id,'') AS master_rt_domisili_id
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != '' " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620102' AND rt_domisili IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_aruta = array();
        foreach ($data_peserta_aruta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_aruta[$key][$keys] = $rows;
            }

            $templist_aruta[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);

            $arr_jumlah_rumah_aruta = array();
            if ($row->rt_domisili) {
                $expl_rt_domisili = explode("|", $row->rt_domisili);
                foreach ($expl_rt_domisili as $key_rt => $val_rt) {
                    $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                        "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$row->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.rt_domisili = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != ''
                            GROUP BY unique_id
                        ) AS tbl
                        "
                    )->row();

                    array_push($arr_jumlah_rumah_aruta, $jumlah_rumah->jumlah_rumah);
                }
            }
            $templist_aruta[$key]['jumlah_rumah'] = implode("|", $arr_jumlah_rumah_aruta);
        }

        $data_peserta_kumai = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(master_rt_domisili_id ORDER BY rt_domisili SEPARATOR '|') AS master_rt_domisili_id
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili,IFNULL(master_rt_domisili_id,'') AS master_rt_domisili_id
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != '' " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620103' AND rt_domisili IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_kumai = array();
        foreach ($data_peserta_kumai as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_kumai[$key][$keys] = $rows;
            }

            $templist_kumai[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);

            $arr_jumlah_rumah_kumai = array();
            if ($row->rt_domisili) {
                $expl_rt_domisili = explode("|", $row->rt_domisili);
                foreach ($expl_rt_domisili as $key_rt => $val_rt) {
                    $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                        "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$row->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.rt_domisili = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != ''
                            GROUP BY unique_id
                        ) AS tbl
                        "
                    )->row();

                    array_push($arr_jumlah_rumah_kumai, $jumlah_rumah->jumlah_rumah);
                }
            }
            $templist_kumai[$key]['jumlah_rumah'] = implode("|", $arr_jumlah_rumah_kumai);
        }

        $data_peserta_kolam = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(master_rt_domisili_id ORDER BY rt_domisili SEPARATOR '|') AS master_rt_domisili_id
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili,IFNULL(master_rt_domisili_id,'') AS master_rt_domisili_id
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != '' " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620104' AND rt_domisili IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_kolam = array();
        foreach ($data_peserta_kolam as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_kolam[$key][$keys] = $rows;
            }

            $templist_kolam[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);

            $arr_jumlah_rumah_kolam = array();
            if ($row->rt_domisili) {
                $expl_rt_domisili = explode("|", $row->rt_domisili);
                foreach ($expl_rt_domisili as $key_rt => $val_rt) {
                    $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                        "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$row->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.rt_domisili = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != ''
                            GROUP BY unique_id
                        ) AS tbl
                        "
                    )->row();

                    array_push($arr_jumlah_rumah_kolam, $jumlah_rumah->jumlah_rumah);
                }
            }
            $templist_kolam[$key]['jumlah_rumah'] = implode("|", $arr_jumlah_rumah_kolam);
        }

        $data_peserta_banteng = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(master_rt_domisili_id ORDER BY rt_domisili SEPARATOR '|') AS master_rt_domisili_id
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili,IFNULL(master_rt_domisili_id,'') AS master_rt_domisili_id
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != '' " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620106' AND rt_domisili IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_banteng = array();
        foreach ($data_peserta_banteng as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_banteng[$key][$keys] = $rows;
            }

            $templist_banteng[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);

            $arr_jumlah_rumah_banteng = array();
            if ($row->rt_domisili) {
                $expl_rt_domisili = explode("|", $row->rt_domisili);
                foreach ($expl_rt_domisili as $key_rt => $val_rt) {
                    $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                        "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$row->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.rt_domisili = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != ''
                            GROUP BY unique_id
                        ) AS tbl
                        "
                    )->row();

                    array_push($arr_jumlah_rumah_banteng, $jumlah_rumah->jumlah_rumah);
                }
            }
            $templist_banteng[$key]['jumlah_rumah'] = implode("|", $arr_jumlah_rumah_banteng);
        }

        $data_peserta_lada = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(master_rt_domisili_id ORDER BY rt_domisili SEPARATOR '|') AS master_rt_domisili_id
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili,IFNULL(master_rt_domisili_id,'') AS master_rt_domisili_id
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != '' " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620105' AND rt_domisili IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_lada = array();
        foreach ($data_peserta_lada as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_lada[$key][$keys] = $rows;
            }

            $templist_lada[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);

            $arr_jumlah_rumah_lada = array();
            if ($row->rt_domisili) {
                $expl_rt_domisili = explode("|", $row->rt_domisili);
                foreach ($expl_rt_domisili as $key_rt => $val_rt) {
                    $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                        "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$row->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.rt_domisili = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != ''
                            GROUP BY unique_id
                        ) AS tbl
                        "
                    )->row();

                    array_push($arr_jumlah_rumah_lada, $jumlah_rumah->jumlah_rumah);
                }
            }
            $templist_lada[$key]['jumlah_rumah'] = implode("|", $arr_jumlah_rumah_lada);
        }

        $data = array("arsel" => $templist_arsel, "aruta" => $templist_aruta, "kumai" => $templist_kumai, "kolam" => $templist_kolam, "banteng" => $templist_banteng, "lada" => $templist_lada);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_antigen_old()
    {
        $report_today = $this->iget("report_today");

        $wh = "";
        if ($report_today == 'true') {
            $wh = "AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') ='" . date("Y-m-d") . "'";
        }

        $data_peserta_arsel = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,
            GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,
            GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili
            FROM
            (
                SELECT COUNT(id_peserta) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                WHERE id_trx_pemeriksaan IN (
                    SELECT MAX(id_trx_pemeriksaan)
                    FROM trx_pemeriksaan
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '3' AND rt_domisili IS NOT NULL AND rt_domisili != '' AND trx_pemeriksaan.deleted_at IS NULL " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS tbl
            INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620101'
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_arsel = array();
        foreach ($data_peserta_arsel as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_arsel[$key][$keys] = $rows;
            }

            $templist_arsel[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data_peserta_aruta = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,
            GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,
            GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili
            FROM
            (
                SELECT COUNT(id_peserta) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                WHERE id_trx_pemeriksaan IN (
                    SELECT MAX(id_trx_pemeriksaan)
                    FROM trx_pemeriksaan
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '3' AND rt_domisili IS NOT NULL AND rt_domisili != '' AND trx_pemeriksaan.deleted_at IS NULL " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS tbl
            INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620101'
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_aruta = array();
        foreach ($data_peserta_aruta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_aruta[$key][$keys] = $rows;
            }

            $templist_aruta[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data_peserta_kumai = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,
            GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,
            GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili
            FROM
            (
                SELECT COUNT(id_peserta) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                WHERE id_trx_pemeriksaan IN (
                    SELECT MAX(id_trx_pemeriksaan)
                    FROM trx_pemeriksaan
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '3' AND rt_domisili IS NOT NULL AND rt_domisili != '' AND trx_pemeriksaan.deleted_at IS NULL " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS tbl
            INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620103'
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_kumai = array();
        foreach ($data_peserta_kumai as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_kumai[$key][$keys] = $rows;
            }

            $templist_kumai[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data_peserta_kolam = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,
            GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,
            GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili
            FROM
            (
                SELECT COUNT(id_peserta) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                WHERE id_trx_pemeriksaan IN (
                    SELECT MAX(id_trx_pemeriksaan)
                    FROM trx_pemeriksaan
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '3' AND rt_domisili IS NOT NULL AND rt_domisili != '' AND trx_pemeriksaan.deleted_at IS NULL " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS tbl
            INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620104'
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_kolam = array();
        foreach ($data_peserta_kolam as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_kolam[$key][$keys] = $rows;
            }

            $templist_kolam[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data_peserta_banteng = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,
            GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,
            GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili
            FROM
            (
                SELECT COUNT(id_peserta) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                WHERE id_trx_pemeriksaan IN (
                    SELECT MAX(id_trx_pemeriksaan)
                    FROM trx_pemeriksaan
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '3' AND rt_domisili IS NOT NULL AND rt_domisili != '' AND trx_pemeriksaan.deleted_at IS NULL " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS tbl
            INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620106'
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_banteng = array();
        foreach ($data_peserta_banteng as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_banteng[$key][$keys] = $rows;
            }

            $templist_banteng[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data_peserta_lada = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,
            GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,
            GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili
            FROM
            (
                SELECT COUNT(id_peserta) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                WHERE id_trx_pemeriksaan IN (
                    SELECT MAX(id_trx_pemeriksaan)
                    FROM trx_pemeriksaan
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '3' AND rt_domisili IS NOT NULL AND rt_domisili != '' AND trx_pemeriksaan.deleted_at IS NULL " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS tbl
            INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620105'
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_lada = array();
        foreach ($data_peserta_lada as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_lada[$key][$keys] = $rows;
            }

            $templist_lada[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data = array("arsel" => $templist_arsel, "aruta" => $templist_aruta, "kumai" => $templist_kumai, "kolam" => $templist_kolam, "banteng" => $templist_banteng, "lada" => $templist_lada);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_antigen_new()
    {
        $report_today = $this->iget("report_today");

        $wh = "";
        if ($report_today == 'true') {
            $wh = "AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') ='" . date("Y-m-d") . "'";
        }

        $data_peserta_arsel = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,
            GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,
            GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,
            GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM
            (
                SELECT COUNT(id_peserta) AS jumlah_peserta,kelurahan_master_wilayah_id,rt,id_master_rt
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                INNER JOIN master_rt ON id_master_rt=master_rt_domisili_id
                WHERE id_trx_pemeriksaan IN (
                    SELECT MAX(id_trx_pemeriksaan)
                    FROM trx_pemeriksaan
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '3' AND master_rt_domisili_id IS NOT NULL AND trx_pemeriksaan.deleted_at IS NULL " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt
            ) AS tbl
            INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620101'
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_arsel = array();
        foreach ($data_peserta_arsel as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_arsel[$key][$keys] = $rows;
            }
            $templist_arsel[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data_peserta_aruta = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,
            GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,
            GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,
            GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM
            (
                SELECT COUNT(id_peserta) AS jumlah_peserta,kelurahan_master_wilayah_id,rt,id_master_rt
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                INNER JOIN master_rt ON id_master_rt=master_rt_domisili_id
                WHERE id_trx_pemeriksaan IN (
                    SELECT MAX(id_trx_pemeriksaan)
                    FROM trx_pemeriksaan
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '3' AND master_rt_domisili_id IS NOT NULL AND trx_pemeriksaan.deleted_at IS NULL " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt
            ) AS tbl
            INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620102'
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_aruta = array();
        foreach ($data_peserta_aruta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_aruta[$key][$keys] = $rows;
            }

            $templist_aruta[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data_peserta_kumai = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,
            GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,
            GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,
            GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM
            (
                SELECT COUNT(id_peserta) AS jumlah_peserta,kelurahan_master_wilayah_id,rt,id_master_rt
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                INNER JOIN master_rt ON id_master_rt=master_rt_domisili_id
                WHERE id_trx_pemeriksaan IN (
                    SELECT MAX(id_trx_pemeriksaan)
                    FROM trx_pemeriksaan
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '3' AND master_rt_domisili_id IS NOT NULL AND trx_pemeriksaan.deleted_at IS NULL " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt
            ) AS tbl
            INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620103'
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_kumai = array();
        foreach ($data_peserta_kumai as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_kumai[$key][$keys] = $rows;
            }

            $templist_kumai[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data_peserta_kolam = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,
            GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,
            GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,
            GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM
            (
                SELECT COUNT(id_peserta) AS jumlah_peserta,kelurahan_master_wilayah_id,rt,id_master_rt
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                INNER JOIN master_rt ON id_master_rt=master_rt_domisili_id
                WHERE id_trx_pemeriksaan IN (
                    SELECT MAX(id_trx_pemeriksaan)
                    FROM trx_pemeriksaan
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '3' AND master_rt_domisili_id IS NOT NULL AND trx_pemeriksaan.deleted_at IS NULL " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt
            ) AS tbl
            INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620104'
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_kolam = array();
        foreach ($data_peserta_kolam as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_kolam[$key][$keys] = $rows;
            }

            $templist_kolam[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data_peserta_banteng = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,
            GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,
            GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,
            GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM
            (
                SELECT COUNT(id_peserta) AS jumlah_peserta,kelurahan_master_wilayah_id,rt,id_master_rt
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                INNER JOIN master_rt ON id_master_rt=master_rt_domisili_id
                WHERE id_trx_pemeriksaan IN (
                    SELECT MAX(id_trx_pemeriksaan)
                    FROM trx_pemeriksaan
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '3' AND master_rt_domisili_id IS NOT NULL AND trx_pemeriksaan.deleted_at IS NULL " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt
            ) AS tbl
            INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620106'
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_banteng = array();
        foreach ($data_peserta_banteng as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_banteng[$key][$keys] = $rows;
            }

            $templist_banteng[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data_peserta_lada = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,
            GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,
            GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,
            GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM
            (
                SELECT COUNT(id_peserta) AS jumlah_peserta,kelurahan_master_wilayah_id,rt,id_master_rt
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                INNER JOIN master_rt ON id_master_rt=master_rt_domisili_id
                WHERE id_trx_pemeriksaan IN (
                    SELECT MAX(id_trx_pemeriksaan)
                    FROM trx_pemeriksaan
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '3' AND master_rt_domisili_id IS NOT NULL AND trx_pemeriksaan.deleted_at IS NULL " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt
            ) AS tbl
            INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620105'
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_lada = array();
        foreach ($data_peserta_lada as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_lada[$key][$keys] = $rows;
            }

            $templist_lada[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data = array("arsel" => $templist_arsel, "aruta" => $templist_aruta, "kumai" => $templist_kumai, "kolam" => $templist_kolam, "banteng" => $templist_banteng, "lada" => $templist_lada);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_kodim_new()
    {
        $report_today = $this->iget("report_today");

        $wh = "";
        $wh2 = "";
        $wh3 = "";
        if ($report_today == 'true') {
            $wh = "AND DATE_FORMAT(c.created_at,'%Y-%m-%d') = CURDATE()";
            $wh2 = "AND DATE_FORMAT(e.created_at,'%Y-%m-%d') = CURDATE() AND DATE_FORMAT(detail_peserta_dalam_satu_rumah.created_at,'%Y-%m-%d') = CURDATE()";
            $wh3 = "AND DATE_FORMAT(a.created_at,'%Y-%m-%d') = CURDATE() AND DATE_FORMAT(detail_peserta_dalam_satu_rumah.created_at,'%Y-%m-%d') = CURDATE()";
        }

        $data_peserta_arsel = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY rt SEPARATOR '|') AS jumlah_rumah,GROUP_CONCAT(jumlah_peserta_is_tracking ORDER BY rt SEPARATOR '|') AS jumlah_is_tracking,GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah,(IFNULL(jumlah_peserta,0) - IFNULL(z.jumlah_peserta_is_tracking,0)) AS jumlah_peserta_is_tracking
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL AND kelurahan_master_wilayah_id=master_wilayah_id {$wh}
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN 
                        (
                            SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                            FROM trx_terkonfirmasi_rilis
                            WHERE deleted_at IS NULL
                            GROUP BY peserta_id
                            ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL {$wh2}
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
                LEFT JOIN
                (
                    SELECT COUNT(*) AS jumlah_peserta_is_tracking,peserta.kelurahan_master_wilayah_id AS kelurahan_child, peserta.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN 
                        (
                            SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                            FROM trx_terkonfirmasi_rilis 
                            WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                            GROUP BY peserta_id
                            ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS a ON a.peserta_id=id_peserta
                    WHERE 
                    STATUS = '1' AND peserta_dalam_satu_rumah.deleted_at IS NULL {$wh3}
                    GROUP BY peserta.master_rt_domisili_id
                ) AS z ON z.master_rt_domisili_id=id_master_rt AND z.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620101' AND rt IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_arsel = array();
        foreach ($data_peserta_arsel as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_arsel[$key][$keys] = $rows;
            }

            $templist_arsel[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data_peserta_aruta = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY rt SEPARATOR '|') AS jumlah_rumah,GROUP_CONCAT(jumlah_peserta_is_tracking ORDER BY rt SEPARATOR '|') AS jumlah_is_tracking,GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah,(IFNULL(jumlah_peserta,0) - IFNULL(z.jumlah_peserta_is_tracking,0)) AS jumlah_peserta_is_tracking
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL AND kelurahan_master_wilayah_id=master_wilayah_id {$wh}
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN 
                        (
                            SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                            FROM trx_terkonfirmasi_rilis
                            WHERE deleted_at IS NULL
                            GROUP BY peserta_id
                            ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL {$wh2}
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
                LEFT JOIN
                (
                    SELECT COUNT(*) AS jumlah_peserta_is_tracking,peserta.kelurahan_master_wilayah_id AS kelurahan_child, peserta.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN 
                        (
                            SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                            FROM trx_terkonfirmasi_rilis 
                            WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                            GROUP BY peserta_id
                            ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS a ON a.peserta_id=id_peserta
                    WHERE 
                    STATUS = '1' AND peserta_dalam_satu_rumah.deleted_at IS NULL {$wh3}
                    GROUP BY peserta.master_rt_domisili_id
                ) AS z ON z.master_rt_domisili_id=id_master_rt AND z.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620102' AND rt IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_aruta = array();
        foreach ($data_peserta_aruta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_aruta[$key][$keys] = $rows;
            }

            $templist_aruta[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data_peserta_kumai = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY rt SEPARATOR '|') AS jumlah_rumah,GROUP_CONCAT(jumlah_peserta_is_tracking ORDER BY rt SEPARATOR '|') AS jumlah_is_tracking,GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah,(IFNULL(jumlah_peserta,0) - IFNULL(z.jumlah_peserta_is_tracking,0)) AS jumlah_peserta_is_tracking
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL AND kelurahan_master_wilayah_id=master_wilayah_id {$wh}
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN 
                        (
                            SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                            FROM trx_terkonfirmasi_rilis
                            WHERE deleted_at IS NULL
                            GROUP BY peserta_id
                            ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL {$wh2}
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
                LEFT JOIN
                (
                    SELECT COUNT(*) AS jumlah_peserta_is_tracking,peserta.kelurahan_master_wilayah_id AS kelurahan_child, peserta.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN 
                        (
                            SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                            FROM trx_terkonfirmasi_rilis 
                            WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                            GROUP BY peserta_id
                            ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS a ON a.peserta_id=id_peserta
                    WHERE 
                    STATUS = '1' AND peserta_dalam_satu_rumah.deleted_at IS NULL {$wh3}
                    GROUP BY peserta.master_rt_domisili_id
                ) AS z ON z.master_rt_domisili_id=id_master_rt AND z.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620103' AND rt IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_kumai = array();
        foreach ($data_peserta_kumai as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_kumai[$key][$keys] = $rows;
            }

            $templist_kumai[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data_peserta_kolam = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY rt SEPARATOR '|') AS jumlah_rumah,GROUP_CONCAT(jumlah_peserta_is_tracking ORDER BY rt SEPARATOR '|') AS jumlah_is_tracking,GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah,(IFNULL(jumlah_peserta,0) - IFNULL(z.jumlah_peserta_is_tracking,0)) AS jumlah_peserta_is_tracking
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL AND kelurahan_master_wilayah_id=master_wilayah_id {$wh}
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN 
                        (
                            SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                            FROM trx_terkonfirmasi_rilis
                            WHERE deleted_at IS NULL
                            GROUP BY peserta_id
                            ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL {$wh2}
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
                LEFT JOIN
                (
                    SELECT COUNT(*) AS jumlah_peserta_is_tracking,peserta.kelurahan_master_wilayah_id AS kelurahan_child, peserta.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN 
                        (
                            SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                            FROM trx_terkonfirmasi_rilis 
                            WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                            GROUP BY peserta_id
                            ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS a ON a.peserta_id=id_peserta
                    WHERE 
                    STATUS = '1' AND peserta_dalam_satu_rumah.deleted_at IS NULL {$wh3}
                    GROUP BY peserta.master_rt_domisili_id
                ) AS z ON z.master_rt_domisili_id=id_master_rt AND z.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620104' AND rt IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_kolam = array();
        foreach ($data_peserta_kolam as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_kolam[$key][$keys] = $rows;
            }

            $templist_kolam[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data_peserta_banteng = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY rt SEPARATOR '|') AS jumlah_rumah,GROUP_CONCAT(jumlah_peserta_is_tracking ORDER BY rt SEPARATOR '|') AS jumlah_is_tracking,GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah,(IFNULL(jumlah_peserta,0) - IFNULL(z.jumlah_peserta_is_tracking,0)) AS jumlah_peserta_is_tracking
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL AND kelurahan_master_wilayah_id=master_wilayah_id {$wh}
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN 
                        (
                            SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                            FROM trx_terkonfirmasi_rilis
                            WHERE deleted_at IS NULL
                            GROUP BY peserta_id
                            ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL {$wh2}
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
                LEFT JOIN
                (
                    SELECT COUNT(*) AS jumlah_peserta_is_tracking,peserta.kelurahan_master_wilayah_id AS kelurahan_child, peserta.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN 
                        (
                            SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                            FROM trx_terkonfirmasi_rilis 
                            WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                            GROUP BY peserta_id
                            ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS a ON a.peserta_id=id_peserta
                    WHERE 
                    STATUS = '1' AND peserta_dalam_satu_rumah.deleted_at IS NULL {$wh3}
                    GROUP BY peserta.master_rt_domisili_id
                ) AS z ON z.master_rt_domisili_id=id_master_rt AND z.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620106' AND rt IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_banteng = array();
        foreach ($data_peserta_banteng as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_banteng[$key][$keys] = $rows;
            }

            $templist_banteng[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data_peserta_lada = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY rt SEPARATOR '|') AS jumlah_rumah,GROUP_CONCAT(jumlah_peserta_is_tracking ORDER BY rt SEPARATOR '|') AS jumlah_is_tracking,GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah,(IFNULL(jumlah_peserta,0) - IFNULL(z.jumlah_peserta_is_tracking,0)) AS jumlah_peserta_is_tracking
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL AND kelurahan_master_wilayah_id=master_wilayah_id {$wh}
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN 
                        (
                            SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                            FROM trx_terkonfirmasi_rilis
                            WHERE deleted_at IS NULL
                            GROUP BY peserta_id
                            ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL {$wh2}
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
                LEFT JOIN
                (
                    SELECT COUNT(*) AS jumlah_peserta_is_tracking,peserta.kelurahan_master_wilayah_id AS kelurahan_child, peserta.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN 
                        (
                            SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                            FROM trx_terkonfirmasi_rilis 
                            WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                            GROUP BY peserta_id
                            ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS a ON a.peserta_id=id_peserta
                    WHERE 
                    STATUS = '1' AND peserta_dalam_satu_rumah.deleted_at IS NULL {$wh3}
                    GROUP BY peserta.master_rt_domisili_id
                ) AS z ON z.master_rt_domisili_id=id_master_rt AND z.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620105' AND rt IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_lada = array();
        foreach ($data_peserta_lada as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_lada[$key][$keys] = $rows;
            }

            $templist_lada[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data = array("arsel" => $templist_arsel, "aruta" => $templist_aruta, "kumai" => $templist_kumai, "kolam" => $templist_kolam, "banteng" => $templist_banteng, "lada" => $templist_lada);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_list_peserta_polres_kodim()
    {
        $id_master_wilayah = decrypt_data($this->iget("id_master_wilayah"));
        $rt_domisili = $this->iget("rt_domisili");
        $report_today = $this->iget("report_today");

        $wh = "";
        if ($report_today == 'true') {
            $wh = "AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') ='" . date("Y-m-d") . "'";
        }

        $data_peserta = $this->peserta_model->query(
            "
            SELECT peserta.*,DATE_FORMAT(c.created_at,'%Y-%m-%d') AS tanggal_terkonfirmasi,id_detail_peserta_dalam_satu_rumah
            FROM peserta
            INNER JOIN 
            (
                SELECT *
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                )
            ) AS c ON c.peserta_id=id_peserta
            LEFT JOIN detail_peserta_dalam_satu_rumah ON detail_peserta_dalam_satu_rumah.peserta_id=c.peserta_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
            WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != '' AND kelurahan_master_wilayah_id = '" . $id_master_wilayah . "' AND rt_domisili = '" . $rt_domisili . "' " . $wh . "
            "
        )->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $data_master = $this->master_wilayah_model->get_by($row->kelurahan_master_wilayah_id);

            $data_kecamatan = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_master->kode_induk
                    )
                ),
                "row"
            );

            $data_kabupaten = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_kecamatan->kode_induk
                    )
                ),
                "row"
            );

            $data_provinsi = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_kabupaten->kode_induk
                    )
                ),
                "row"
            );

            if ($row->kelurahan_master_wilayah_id == "109") {
                $templist[$key]['alamat_domisili'] = "Wilayah Kobar Lainnya";
            } else {
                $templist[$key]['alamat_domisili'] = $row->alamat_domisili . (!empty($row->rt_domisili) ? " RT. " . $row->rt_domisili : "") . ($data_master->klasifikasi == "KEL" ? " Kelurahan" : " Desa") . " " . $data_master->nama_wilayah . ", Kecamatan " . ucwords(strtolower($data_kecamatan->nama_wilayah)) . ", Kabupaten " . ucwords(strtolower($data_kabupaten->nama_wilayah)) . ", Provinisi " . ucwords(strtolower($data_provinsi->nama_wilayah));
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
            $templist[$key]['tanggal_terkonfirmasi_custom'] = longdate_indo($row->tanggal_terkonfirmasi);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_list_peserta_polres_kodim_antigen()
    {
        $id_master_wilayah = decrypt_data($this->iget("id_master_wilayah"));
        $rt_domisili = $this->iget("rt_domisili");
        $report_today = $this->iget("report_today");

        $wh = "";
        if ($report_today == 'true') {
            $wh = "AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') ='" . date("Y-m-d") . "'";
        }

        $data_peserta = $this->peserta_model->query(
            "
            SELECT peserta.*,DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') AS tanggal_terkonfirmasi
            FROM trx_pemeriksaan
            INNER JOIN peserta ON id_peserta=peserta_id
            WHERE id_trx_pemeriksaan IN (
                SELECT MAX(id_trx_pemeriksaan)
                FROM trx_pemeriksaan
                WHERE trx_pemeriksaan.deleted_at IS NULL
                GROUP BY peserta_id
            )
            AND jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '3'
            AND kelurahan_master_wilayah_id = '" . $id_master_wilayah . "' AND rt_domisili = '" . $rt_domisili . "' " . $wh . "
            "
        )->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $data_master = $this->master_wilayah_model->get_by($row->kelurahan_master_wilayah_id);

            $data_kecamatan = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_master->kode_induk
                    )
                ),
                "row"
            );

            $data_kabupaten = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_kecamatan->kode_induk
                    )
                ),
                "row"
            );

            $data_provinsi = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_kabupaten->kode_induk
                    )
                ),
                "row"
            );

            if ($row->kelurahan_master_wilayah_id == "109") {
                $templist[$key]['alamat_domisili'] = "Wilayah Kobar Lainnya";
            } else {
                $templist[$key]['alamat_domisili'] = $row->alamat_domisili . (!empty($row->rt_domisili) ? " RT. " . $row->rt_domisili : "") . ($data_master->klasifikasi == "KEL" ? " Kelurahan" : " Desa") . " " . $data_master->nama_wilayah . ", Kecamatan " . ucwords(strtolower($data_kecamatan->nama_wilayah)) . ", Kabupaten " . ucwords(strtolower($data_kabupaten->nama_wilayah)) . ", Provinisi " . ucwords(strtolower($data_provinsi->nama_wilayah));
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
            $templist[$key]['tanggal_terkonfirmasi_custom'] = longdate_indo($row->tanggal_terkonfirmasi);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_list_peserta_polres_kodim_new_antigen()
    {
        $id_master_wilayah = decrypt_data($this->iget("id_master_wilayah"));
        $rt_domisili = $this->iget("rt_domisili");
        $report_today = $this->iget("report_today");

        $wh = "";
        if ($report_today == 'true') {
            $wh = "AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') ='" . date("Y-m-d") . "'";
        }

        $data_peserta = $this->peserta_model->query(
            "
            SELECT peserta.*,DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') AS tanggal_terkonfirmasi
            FROM trx_pemeriksaan
            INNER JOIN peserta ON id_peserta=peserta_id
            INNER JOIN master_rt ON id_master_rt=master_rt_domisili_id
            WHERE id_trx_pemeriksaan IN (
                SELECT MAX(id_trx_pemeriksaan)
                FROM trx_pemeriksaan
                WHERE trx_pemeriksaan.deleted_at IS NULL
                GROUP BY peserta_id
            )
            AND jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '3'
            AND kelurahan_master_wilayah_id = '" . $id_master_wilayah . "' AND master_rt_domisili_id = '" . $rt_domisili . "' " . $wh . "
            AND master_rt_domisili_id IS NOT NULL
            "
        )->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $data_master = $this->master_wilayah_model->get_by($row->kelurahan_master_wilayah_id);

            $data_kecamatan = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_master->kode_induk
                    )
                ),
                "row"
            );

            $data_kabupaten = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_kecamatan->kode_induk
                    )
                ),
                "row"
            );

            $data_provinsi = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_kabupaten->kode_induk
                    )
                ),
                "row"
            );

            if ($row->kelurahan_master_wilayah_id == "109") {
                $templist[$key]['alamat_domisili'] = "Wilayah Kobar Lainnya";
            } else {
                $templist[$key]['alamat_domisili'] = $row->alamat_domisili . (!empty($row->rt_domisili) ? " RT. " . $row->rt_domisili : "") . ($data_master->klasifikasi == "KEL" ? " Kelurahan" : " Desa") . " " . $data_master->nama_wilayah . ", Kecamatan " . ucwords(strtolower($data_kecamatan->nama_wilayah)) . ", Kabupaten " . ucwords(strtolower($data_kabupaten->nama_wilayah)) . ", Provinisi " . ucwords(strtolower($data_provinsi->nama_wilayah));
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
            $templist[$key]['tanggal_terkonfirmasi_custom'] = longdate_indo($row->tanggal_terkonfirmasi);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_list_peserta_polres_kodim_new()
    {
        $id_master_wilayah = decrypt_data($this->iget("id_master_wilayah"));
        $rt_domisili = $this->iget("rt_domisili");
        $report_today = $this->iget("report_today");

        $data_rt_domisili = $this->master_rt_model->get_by($rt_domisili);

        $wh = "";
        if ($report_today == 'true') {
            $wh = "AND DATE_FORMAT(c.created_at,'%Y-%m-%d') = CURDATE()";
        }

        $data_peserta = $this->peserta_model->query(
            "
            SELECT peserta.*,DATE_FORMAT(c.created_at,'%Y-%m-%d') AS tanggal_terkonfirmasi,id_detail_peserta_dalam_satu_rumah,peserta_dalam_satu_rumah_id
            FROM peserta
            INNER JOIN 
            (
                SELECT *
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                )
            ) AS c ON c.peserta_id=id_peserta
            LEFT JOIN detail_peserta_dalam_satu_rumah ON detail_peserta_dalam_satu_rumah.peserta_id=c.peserta_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
            WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND kelurahan_master_wilayah_id = '" . $id_master_wilayah . "' AND master_rt_domisili_id = '" . $rt_domisili . "' " . $wh . "
            "
        )->result();

        $data_rt = $this->master_rt_model->get(
            array(
                "where" => array(
                    "master_wilayah_id" => $id_master_wilayah
                ),
                "order_by" => array(
                    "rt" => "ASC"
                )
            )
        );

        $templist = array();
        foreach ($data_peserta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $html = "<select onchange='select_one_group(this)' name='rt_domisili[" . $row->id_peserta . "]' data-class-group='" . $row->peserta_dalam_satu_rumah_id . "' class='form-control group_" . $row->peserta_dalam_satu_rumah_id . "'><option value=''>-- Pilih RT Domisili --</option>";
            foreach ($data_rt as $key_rt => $row_rt) {
                $html .= "<option " . ($row->master_rt_domisili_id_ppkm == $row_rt->id_master_rt ? 'selected' : '') . " value='" . encrypt_data($row_rt->id_master_rt) . "'>" . $row_rt->rt . "</option>";
            }

            $html .= "</select>";

            $data_master = $this->master_wilayah_model->get_by($row->kelurahan_master_wilayah_id);

            $data_kecamatan = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_master->kode_induk
                    )
                ),
                "row"
            );

            $data_kabupaten = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_kecamatan->kode_induk
                    )
                ),
                "row"
            );

            $data_provinsi = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_kabupaten->kode_induk
                    )
                ),
                "row"
            );

            if ($row->kelurahan_master_wilayah_id == "109") {
                $templist[$key]['alamat_domisili'] = "Wilayah Kobar Lainnya";
            } else {
                $templist[$key]['alamat_domisili'] = $row->alamat_domisili . (!empty($row->rt_domisili) ? " RT. " . $row->rt_domisili : "") . ($data_master->klasifikasi == "KEL" ? " Kelurahan" : " Desa") . " " . $data_master->nama_wilayah . ", Kecamatan " . ucwords(strtolower($data_kecamatan->nama_wilayah)) . ", Kabupaten " . ucwords(strtolower($data_kabupaten->nama_wilayah)) . ", Provinisi " . ucwords(strtolower($data_provinsi->nama_wilayah));
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
            $templist[$key]['tanggal_terkonfirmasi_custom'] = longdate_indo($row->tanggal_terkonfirmasi);
            $templist[$key]['rt_domisili_dropdown'] = ($data_rt_domisili->rt == "00" ? $html : "");
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_list_peserta_polres_kodim_per_rumah()
    {
        $id_master_wilayah = decrypt_data($this->iget("id_master_wilayah"));
        $rt_domisili = $this->iget("rt_domisili");
        $report_today = $this->iget("report_today");

        $wh = "";
        if ($report_today == 'true') {
            $wh = "AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') ='" . date("Y-m-d") . "'";
        }

        $data_peserta = $this->peserta_model->query(
            "
            SELECT unique_id,GROUP_CONCAT(DATE_FORMAT(c.created_at,'%Y-%m-%d') ORDER BY id_peserta SEPARATOR '|') AS tanggal_terkonfirmasi,GROUP_CONCAT(nama ORDER BY id_peserta SEPARATOR '|') AS nama_peserta,GROUP_CONCAT(id_peserta ORDER BY id_peserta SEPARATOR '|') AS id_peserta
            FROM peserta_dalam_satu_rumah
            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
            LEFT JOIN 
            (
                SELECT *
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                )
            ) AS c ON c.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
            LEFT JOIN peserta ON id_peserta=c.peserta_id
            WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != '' AND peserta.kelurahan_master_wilayah_id = '" . $id_master_wilayah . "' AND peserta.rt_domisili = '" . $rt_domisili . "' AND peserta_dalam_satu_rumah.deleted_at IS NULL " . $wh . "
            GROUP BY unique_id
            "
        )->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_list_peserta_polres_kodim_per_rumah_new()
    {
        $id_master_wilayah = decrypt_data($this->iget("id_master_wilayah"));
        $rt_domisili = $this->iget("rt_domisili");
        $report_today = $this->iget("report_today");

        $wh = "";
        if ($report_today == 'true') {
            $wh = "AND DATE_FORMAT(c.created_at,'%Y-%m-%d') = CURDATE() AND DATE_FORMAT(detail_peserta_dalam_satu_rumah.created_at,'%Y-%m-%d') = CURDATE()";
        }

        $data_peserta = $this->peserta_model->query(
            "
            SELECT unique_id,GROUP_CONCAT(DATE_FORMAT(c.created_at,'%Y-%m-%d') ORDER BY id_peserta SEPARATOR '|') AS tanggal_terkonfirmasi,GROUP_CONCAT(nama ORDER BY id_peserta SEPARATOR '|') AS nama_peserta,GROUP_CONCAT(id_peserta ORDER BY id_peserta SEPARATOR '|') AS id_peserta
            FROM peserta_dalam_satu_rumah
            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
            LEFT JOIN 
            (
                SELECT *
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                )
            ) AS c ON c.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
            LEFT JOIN peserta ON id_peserta=c.peserta_id
            WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL AND peserta.kelurahan_master_wilayah_id = '" . $id_master_wilayah . "' AND peserta.master_rt_domisili_id = '" . $rt_domisili . "' AND peserta_dalam_satu_rumah.deleted_at IS NULL {$wh}
            GROUP BY unique_id
            "
        )->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_detail_peserta_tni_polri()
    {
        $id_peserta = $this->iget("id_peserta");

        $peserta = $this->peserta_model->get(
            array(
                "fields" => "peserta.*,IFNULL(user.nama_lengkap,'') as nama_user",
                "left_join" => array(
                    "user" => "id_user=id_user_created"
                ),
                "where" => array(
                    "id_peserta" => decrypt_data($id_peserta)
                )
            ),
            "row"
        );

        $data_master = $this->master_wilayah_model->get_by($peserta->kelurahan_master_wilayah_id);

        $data_kecamatan = $this->master_wilayah_model->get(
            array(
                "fields" => "nama_wilayah,kode_induk",
                "where" => array(
                    "kode_wilayah" => $data_master->kode_induk
                )
            ),
            "row"
        );

        $data_kabupaten = $this->master_wilayah_model->get(
            array(
                "fields" => "nama_wilayah,kode_induk",
                "where" => array(
                    "kode_wilayah" => $data_kecamatan->kode_induk
                )
            ),
            "row"
        );

        $data_provinsi = $this->master_wilayah_model->get(
            array(
                "fields" => "nama_wilayah,kode_induk",
                "where" => array(
                    "kode_wilayah" => $data_kabupaten->kode_induk
                )
            ),
            "row"
        );

        $peserta->alamat_ktp = $peserta->alamat_ktp . (!empty($peserta->rt_ktp) ? " RT. " . $peserta->rt_ktp : "");

        $peserta->alamat_domisili = $peserta->alamat_domisili . (!empty($peserta->rt_domisili) ? " RT. " . $peserta->rt_domisili : "") . ($data_master->klasifikasi == "KEL" ? " Kelurahan" : "Desa") . " " . $data_master->nama_wilayah . ", Kecamatan " . ucwords(strtolower($data_kecamatan->nama_wilayah)) . ", Kabupaten " . ucwords(strtolower($data_kabupaten->nama_wilayah)) . ", Provinisi " . ucwords(strtolower($data_provinsi->nama_wilayah));

        $peserta->jenis_kelamin = ($peserta->jenis_kelamin == "L" ? "Laki-Laki" : "Perempuan");
        $peserta->pekerjaan_inti = ($peserta->pekerjaan_inti == "Lainnya" ? $peserta->pekerjaan_inti_lainnya : $peserta->pekerjaan_inti);

        $data = $peserta;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_upload_infografis()
    {
        $infografis = $this->infografis_model->get(
            array(
                "fields" => "GROUP_CONCAT(filename SEPARATOR '|') AS filename, DATE_FORMAT(created_at,'%Y-%m-%d') AS tanggal,tipe_upload",
                "group_by" => "tanggal,tipe_upload",
            )
        );

        $templist = array();
        foreach ($infografis as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['tanggal_custom'] = longdate_indo($row->tanggal);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function cek_date_upload()
    {
        $infografis = $this->infografis_model->get(
            array(
                "fields" => "DATE_FORMAT(created_at,'%Y-%m-%d') AS tanggal",
                "where" => array(
                    "tipe_upload" => "1"
                ),
                "where_false" => "DATE_FORMAT(created_at,'%Y-%m-%d') = CURDATE()",
                "group_by" => "tanggal",
            ),
            "row"
        );

        if ($infografis) {
            $data = false;
        } else {
            $data = true;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function cek_week_upload()
    {
        $last_week = $this->infografis_model->get(
            array(
                "fields" => "DATE_FORMAT(created_at,'%Y-%m-%d') AS tanggal",
                "where" => array(
                    "tipe_upload" => "2"
                ),
                "group_by" => "tanggal",
                "order_by" => array(
                    "tanggal" => "DESC"
                ),
                "limit" => "1"
            ),
            "row"
        );

        $week_last_week = date("W", strtotime($last_week->tanggal));

        $week_today = date("W");

        if ($week_last_week == $week_today) {
            $data = false;
        } else {
            $data = true;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_akumulasi_desa()
    {
        $report_today = $this->iget("report_today");

        $wh = "";
        if ($report_today == 'true') {
            $wh = "AND DATE_FORMAT(c.created_at,'%Y-%m-%d') ='" . date("Y-m-d") . "'";
        }

        $data_peserta_arsel = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY jumlah_rumah DESC SEPARATOR '|') AS jumlah_rumah
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL  AND kelurahan_master_wilayah_id=master_wilayah_id
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN 
                    (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis
                        WHERE deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620101'
            GROUP BY id_master_wilayah
            "
        )->result();

        $arr_hijau_arsel = array();
        $arr_kuning_arsel = array();
        $arr_orange_arsel = array();
        $arr_merah_arsel = array();
        foreach ($data_peserta_arsel as $key => $val) {
            $expl_jumlah_rumah_arsel = explode("|", $val->jumlah_rumah);
            if ($expl_jumlah_rumah_arsel[0] == '0') {
                array_push($arr_hijau_arsel, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_arsel[0] == '1' || $expl_jumlah_rumah_arsel[0] == '2') {
                array_push($arr_kuning_arsel, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_arsel[0] == '3' || $expl_jumlah_rumah_arsel[0] == '4' || $expl_jumlah_rumah_arsel[0] == '5') {
                array_push($arr_orange_arsel, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_arsel[0] >= '6') {
                array_push($arr_merah_arsel, $val->nama_wilayah);
            }
        }

        $arr_max_arsel = max(count($arr_hijau_arsel), count($arr_kuning_arsel), count($arr_orange_arsel), count($arr_merah_arsel));

        $col_arr_arsel = array();
        for ($i = 0; $i < $arr_max_arsel; $i++) {
            $col_arr_arsel[$i] = array(!empty($arr_hijau_arsel[$i]) ? $arr_hijau_arsel[$i] : "", !empty($arr_kuning_arsel[$i]) ? $arr_kuning_arsel[$i] : "", !empty($arr_orange_arsel[$i]) ? $arr_orange_arsel[$i] : "", !empty($arr_merah_arsel[$i]) ? $arr_merah_arsel[$i] : "");
        }

        $data_peserta_aruta = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY jumlah_rumah DESC SEPARATOR '|') AS jumlah_rumah
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL  AND kelurahan_master_wilayah_id=master_wilayah_id
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN 
                    (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis
                        WHERE deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620102'
            GROUP BY id_master_wilayah
            "
        )->result();

        $arr_hijau_aruta = array();
        $arr_kuning_aruta = array();
        $arr_orange_aruta = array();
        $arr_merah_aruta = array();
        foreach ($data_peserta_aruta as $key => $val) {
            $expl_jumlah_rumah_aruta = explode("|", $val->jumlah_rumah);
            if ($expl_jumlah_rumah_aruta[0] == '0') {
                array_push($arr_hijau_aruta, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_aruta[0] == '1' || $expl_jumlah_rumah_aruta[0] == '2') {
                array_push($arr_kuning_aruta, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_aruta[0] == '3' || $expl_jumlah_rumah_aruta[0] == '4' || $expl_jumlah_rumah_aruta[0] == '5') {
                array_push($arr_orange_aruta, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_aruta[0] >= '6') {
                array_push($arr_merah_aruta, $val->nama_wilayah);
            }
        }

        $arr_max_aruta = max(count($arr_hijau_aruta), count($arr_kuning_aruta), count($arr_orange_aruta), count($arr_merah_aruta));

        $col_arr_aruta = array();
        for ($i = 0; $i < $arr_max_aruta; $i++) {
            $col_arr_aruta[$i] = array(!empty($arr_hijau_aruta[$i]) ? $arr_hijau_aruta[$i] : "", !empty($arr_kuning_aruta[$i]) ? $arr_kuning_aruta[$i] : "", !empty($arr_orange_aruta[$i]) ? $arr_orange_aruta[$i] : "", !empty($arr_merah_aruta[$i]) ? $arr_merah_aruta[$i] : "");
        }

        $data_peserta_kumai = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY jumlah_rumah DESC SEPARATOR '|') AS jumlah_rumah
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL  AND kelurahan_master_wilayah_id=master_wilayah_id
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN 
                    (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis
                        WHERE deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620103'
            GROUP BY id_master_wilayah
            "
        )->result();

        $arr_hijau_kumai = array();
        $arr_kuning_kumai = array();
        $arr_orange_kumai = array();
        $arr_merah_kumai = array();
        foreach ($data_peserta_kumai as $key => $val) {
            $expl_jumlah_rumah_kumai = explode("|", $val->jumlah_rumah);
            if ($expl_jumlah_rumah_kumai[0] == '0') {
                array_push($arr_hijau_kumai, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_kumai[0] == '1' || $expl_jumlah_rumah_kumai[0] == '2') {
                array_push($arr_kuning_kumai, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_kumai[0] == '3' || $expl_jumlah_rumah_kumai[0] == '4' || $expl_jumlah_rumah_kumai[0] == '5') {
                array_push($arr_orange_kumai, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_kumai[0] >= '6') {
                array_push($arr_merah_kumai, $val->nama_wilayah);
            }
        }

        $arr_max_kumai = max(count($arr_hijau_kumai), count($arr_kuning_kumai), count($arr_orange_kumai), count($arr_merah_kumai));

        $col_arr_kumai = array();
        for ($i = 0; $i < $arr_max_kumai; $i++) {
            $col_arr_kumai[$i] = array(!empty($arr_hijau_kumai[$i]) ? $arr_hijau_kumai[$i] : "", !empty($arr_kuning_kumai[$i]) ? $arr_kuning_kumai[$i] : "", !empty($arr_orange_kumai[$i]) ? $arr_orange_kumai[$i] : "", !empty($arr_merah_kumai[$i]) ? $arr_merah_kumai[$i] : "");
        }

        $data_peserta_kolam = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY jumlah_rumah DESC SEPARATOR '|') AS jumlah_rumah
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL  AND kelurahan_master_wilayah_id=master_wilayah_id
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN 
                    (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis
                        WHERE deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620104'
            GROUP BY id_master_wilayah
            "
        )->result();

        $arr_hijau_kolam = array();
        $arr_kuning_kolam = array();
        $arr_orange_kolam = array();
        $arr_merah_kolam = array();
        foreach ($data_peserta_kolam as $key => $val) {
            $expl_jumlah_rumah_kolam = explode("|", $val->jumlah_rumah);
            if ($expl_jumlah_rumah_kolam[0] == '0') {
                array_push($arr_hijau_kolam, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_kolam[0] == '1' || $expl_jumlah_rumah_kolam[0] == '2') {
                array_push($arr_kuning_kolam, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_kolam[0] == '3' || $expl_jumlah_rumah_kolam[0] == '4' || $expl_jumlah_rumah_kolam[0] == '5') {
                array_push($arr_orange_kolam, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_kolam[0] >= '6') {
                array_push($arr_merah_kolam, $val->nama_wilayah);
            }
        }

        $arr_max_kolam = max(count($arr_hijau_kolam), count($arr_kuning_kolam), count($arr_orange_kolam), count($arr_merah_kolam));

        $col_arr_kolam = array();
        for ($i = 0; $i < $arr_max_kolam; $i++) {
            $col_arr_kolam[$i] = array(!empty($arr_hijau_kolam[$i]) ? $arr_hijau_kolam[$i] : "", !empty($arr_kuning_kolam[$i]) ? $arr_kuning_kolam[$i] : "", !empty($arr_orange_kolam[$i]) ? $arr_orange_kolam[$i] : "", !empty($arr_merah_kolam[$i]) ? $arr_merah_kolam[$i] : "");
        }

        $data_peserta_lada = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY jumlah_rumah DESC SEPARATOR '|') AS jumlah_rumah
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL  AND kelurahan_master_wilayah_id=master_wilayah_id
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN 
                    (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis
                        WHERE deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620105'
            GROUP BY id_master_wilayah
            "
        )->result();

        $arr_hijau_lada = array();
        $arr_kuning_lada = array();
        $arr_orange_lada = array();
        $arr_merah_lada = array();
        foreach ($data_peserta_lada as $key => $val) {
            $expl_jumlah_rumah_lada = explode("|", $val->jumlah_rumah);
            if ($expl_jumlah_rumah_lada[0] == '0') {
                array_push($arr_hijau_lada, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_lada[0] == '1' || $expl_jumlah_rumah_lada[0] == '2') {
                array_push($arr_kuning_lada, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_lada[0] == '3' || $expl_jumlah_rumah_lada[0] == '4' || $expl_jumlah_rumah_lada[0] == '5') {
                array_push($arr_orange_lada, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_lada[0] >= '6') {
                array_push($arr_merah_lada, $val->nama_wilayah);
            }
        }

        $arr_max_lada = max(count($arr_hijau_lada), count($arr_kuning_lada), count($arr_orange_lada), count($arr_merah_lada));

        $col_arr_lada = array();
        for ($i = 0; $i < $arr_max_lada; $i++) {
            $col_arr_lada[$i] = array(!empty($arr_hijau_lada[$i]) ? $arr_hijau_lada[$i] : "", !empty($arr_kuning_lada[$i]) ? $arr_kuning_lada[$i] : "", !empty($arr_orange_lada[$i]) ? $arr_orange_lada[$i] : "", !empty($arr_merah_lada[$i]) ? $arr_merah_lada[$i] : "");
        }

        $data_peserta_banteng = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY jumlah_rumah DESC SEPARATOR '|') AS jumlah_rumah
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL  AND kelurahan_master_wilayah_id=master_wilayah_id
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN 
                    (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis
                        WHERE deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620106'
            GROUP BY id_master_wilayah
            "
        )->result();

        $arr_hijau_banteng = array();
        $arr_kuning_banteng = array();
        $arr_orange_banteng = array();
        $arr_merah_banteng = array();
        foreach ($data_peserta_banteng as $key => $val) {
            $expl_jumlah_rumah_banteng = explode("|", $val->jumlah_rumah);
            if ($expl_jumlah_rumah_banteng[0] == '0') {
                array_push($arr_hijau_banteng, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_banteng[0] == '1' || $expl_jumlah_rumah_banteng[0] == '2') {
                array_push($arr_kuning_banteng, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_banteng[0] == '3' || $expl_jumlah_rumah_banteng[0] == '4' || $expl_jumlah_rumah_banteng[0] == '5') {
                array_push($arr_orange_banteng, $val->nama_wilayah);
            } else if ($expl_jumlah_rumah_banteng[0] >= '6') {
                array_push($arr_merah_banteng, $val->nama_wilayah);
            }
        }

        $arr_max_banteng = max(count($arr_hijau_banteng), count($arr_kuning_banteng), count($arr_orange_banteng), count($arr_merah_banteng));

        $col_arr_banteng = array();
        for ($i = 0; $i < $arr_max_banteng; $i++) {
            $col_arr_banteng[$i] = array(!empty($arr_hijau_banteng[$i]) ? $arr_hijau_banteng[$i] : "", !empty($arr_kuning_banteng[$i]) ? $arr_kuning_banteng[$i] : "", !empty($arr_orange_banteng[$i]) ? $arr_orange_banteng[$i] : "", !empty($arr_merah_banteng[$i]) ? $arr_merah_banteng[$i] : "");
        }

        $data = array("arsel" => array("data" => $col_arr_arsel, "arr_max" => $arr_max_arsel, "jumlah_des_kel" => count($data_peserta_arsel)), "aruta" => array("data" => $col_arr_aruta, "arr_max" => $arr_max_aruta, "jumlah_des_kel" => count($data_peserta_aruta)), "kumai" => array("data" => $col_arr_kumai, "arr_max" => $arr_max_kumai, "jumlah_des_kel" => count($data_peserta_kumai)), "kolam" => array("data" => $col_arr_kolam, "arr_max" => $arr_max_kolam, "jumlah_des_kel" => count($data_peserta_kolam)), "banteng" => array("data" => $col_arr_banteng, "arr_max" => $arr_max_banteng, "jumlah_des_kel" => count($data_peserta_banteng)), "lada" => array("data" => $col_arr_lada, "arr_max" => $arr_max_lada, "jumlah_des_kel" => count($data_peserta_lada)));

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_kodim_new_by_kecamatan()
    {

        $kode_wilayah = $this->session->userdata("kode_wilayah");

        $data_peserta = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY rt SEPARATOR '|') AS jumlah_rumah,GROUP_CONCAT(jumlah_peserta_is_tracking ORDER BY rt SEPARATOR '|') AS jumlah_is_tracking,GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah,(IFNULL(jumlah_peserta,0) - IFNULL(z.jumlah_peserta_is_tracking,0)) AS jumlah_peserta_is_tracking
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL  AND kelurahan_master_wilayah_id=master_wilayah_id
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN 
                        (
                            SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                            FROM trx_terkonfirmasi_rilis
                            WHERE deleted_at IS NULL
                            GROUP BY peserta_id
                            ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
                LEFT JOIN
                (
                    SELECT COUNT(*) AS jumlah_peserta_is_tracking,peserta.kelurahan_master_wilayah_id AS kelurahan_child, peserta.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN 
                        (
                            SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                            FROM trx_terkonfirmasi_rilis 
                            WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                            GROUP BY peserta_id
                            ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS a ON a.peserta_id=id_peserta
                    WHERE 
                    STATUS = '1' AND peserta_dalam_satu_rumah.deleted_at IS NULL
                    GROUP BY peserta.master_rt_domisili_id
                ) AS z ON z.master_rt_domisili_id=id_master_rt AND z.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '" . $kode_wilayah . "' AND rt IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_kodim_new_by_polsek()
    {

        $kode_wilayah = $this->session->userdata("kode_wilayah");

        $data_peserta = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(IFNULL(jumlah_rumah,0) ORDER BY rt SEPARATOR '|') AS jumlah_rumah,GROUP_CONCAT(jumlah_peserta_is_tracking ORDER BY rt SEPARATOR '|') AS jumlah_is_tracking,GROUP_CONCAT(rt ORDER BY rt SEPARATOR '|') AS rt_domisili,GROUP_CONCAT(id_master_rt ORDER BY rt SEPARATOR '|') AS id_master_rt
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT IFNULL(jumlah_peserta,0) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_obj,rt,id_master_rt,IFNULL(c.jumlah_rumah,0) AS jumlah_rumah,(IFNULL(jumlah_peserta,0) - IFNULL(z.jumlah_peserta_is_tracking,0)) AS jumlah_peserta_is_tracking
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,kelurahan_master_wilayah_id AS kelurahan_obj,rt,id_master_rt
                    FROM peserta
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS c ON c.peserta_id=id_peserta
                    INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                    WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL AND kelurahan_master_wilayah_id=master_wilayah_id
                    GROUP BY kelurahan_master_wilayah_id,id_master_rt
                ) AS tbl2
                LEFT JOIN
                (
                    SELECT COUNT(DISTINCT id_peserta_dalam_satu_rumah) AS jumlah_rumah,peserta_dalam_satu_rumah.kelurahan_master_wilayah_id AS kelurahan_child, peserta_dalam_satu_rumah.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN 
                        (
                            SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                            FROM trx_terkonfirmasi_rilis
                            WHERE deleted_at IS NULL
                            GROUP BY peserta_id
                            ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                    INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                    WHERE 
                    STATUS = '1' AND peserta.master_rt_domisili_id IS NOT NULL
                    GROUP BY peserta_dalam_satu_rumah.master_rt_domisili_id
                ) AS c ON c.master_rt_domisili_id=id_master_rt AND c.kelurahan_child=kelurahan_obj
                LEFT JOIN
                (
                    SELECT COUNT(*) AS jumlah_peserta_is_tracking,peserta.kelurahan_master_wilayah_id AS kelurahan_child, peserta.master_rt_domisili_id
                    FROM peserta_dalam_satu_rumah 
                    INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                    INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
                    INNER JOIN 
                    (
                        SELECT *
                        FROM trx_terkonfirmasi_rilis
                        WHERE id_trx_terkonfirmasi_rilis IN 
                        (
                            SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                            FROM trx_terkonfirmasi_rilis 
                            WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                            GROUP BY peserta_id
                            ORDER BY id_trx_terkonfirmasi_rilis DESC
                        )
                    ) AS a ON a.peserta_id=id_peserta
                    WHERE 
                    STATUS = '1' AND peserta_dalam_satu_rumah.deleted_at IS NULL
                    GROUP BY peserta.master_rt_domisili_id
                ) AS z ON z.master_rt_domisili_id=id_master_rt AND z.kelurahan_child=kelurahan_obj
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '" . $kode_wilayah . "' AND rt IS NOT NULL
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_list_peserta_polsek()
    {
        $id_master_wilayah = decrypt_data($this->iget("id_master_wilayah"));
        $rt_domisili = $this->iget("rt_domisili");

        $data_rt_domisili = $this->master_rt_model->get_by($rt_domisili);

        $data_peserta = $this->peserta_model->query(
            "
            SELECT peserta.*,DATE_FORMAT(c.created_at,'%Y-%m-%d') AS tanggal_terkonfirmasi,id_detail_peserta_dalam_satu_rumah,peserta_dalam_satu_rumah_id,d.status AS status_sebelum_rilis
            FROM peserta
            INNER JOIN 
            (
                SELECT *
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                )
            ) AS c ON c.peserta_id=id_peserta
            LEFT JOIN 
            (
                SELECT *
                FROM trx_terkonfirmasi
                WHERE id_trx_terkonfirmasi IN (
                    SELECT MAX(id_trx_terkonfirmasi) AS id_trx_terkonfirmasi 
                    FROM trx_terkonfirmasi 
                    WHERE trx_terkonfirmasi.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi DESC
                )
            ) AS d ON d.peserta_id=id_peserta
            LEFT JOIN detail_peserta_dalam_satu_rumah ON detail_peserta_dalam_satu_rumah.peserta_id=c.peserta_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
            WHERE peserta.deleted_at IS NULL AND c.STATUS = '1' AND kelurahan_master_wilayah_id = '" . $id_master_wilayah . "' AND master_rt_domisili_id = '" . $rt_domisili . "'
            "
        )->result();

        $data_rt = $this->master_rt_model->get(
            array(
                "where" => array(
                    "master_wilayah_id" => $id_master_wilayah
                ),
                "order_by" => array(
                    "rt" => "ASC"
                )
            )
        );

        $templist = array();
        foreach ($data_peserta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $html = "<select onchange='select_one_group(this)' name='rt_domisili[" . $row->id_peserta . "]' data-class-group='" . $row->peserta_dalam_satu_rumah_id . "' class='form-control group_" . $row->peserta_dalam_satu_rumah_id . "'><option value=''>-- Pilih RT Domisili --</option>";
            foreach ($data_rt as $key_rt => $row_rt) {
                $html .= "<option " . ($row->master_rt_domisili_id_ppkm == $row_rt->id_master_rt ? 'selected' : '') . " value='" . encrypt_data($row_rt->id_master_rt) . "'>" . $row_rt->rt . "</option>";
            }

            $html .= "</select>";

            $data_master = $this->master_wilayah_model->get_by($row->kelurahan_master_wilayah_id);

            $data_kecamatan = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_master->kode_induk
                    )
                ),
                "row"
            );

            $data_kabupaten = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_kecamatan->kode_induk
                    )
                ),
                "row"
            );

            $data_provinsi = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_kabupaten->kode_induk
                    )
                ),
                "row"
            );

            if ($row->kelurahan_master_wilayah_id == "109") {
                $templist[$key]['alamat_domisili'] = "Wilayah Kobar Lainnya";
            } else {
                $templist[$key]['alamat_domisili'] = $row->alamat_domisili . (!empty($row->rt_domisili) ? " RT. " . $row->rt_domisili : "") . ($data_master->klasifikasi == "KEL" ? " Kelurahan" : " Desa") . " " . $data_master->nama_wilayah . ", Kecamatan " . ucwords(strtolower($data_kecamatan->nama_wilayah)) . ", Kabupaten " . ucwords(strtolower($data_kabupaten->nama_wilayah)) . ", Provinisi " . ucwords(strtolower($data_provinsi->nama_wilayah));
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
            $templist[$key]['tanggal_terkonfirmasi_custom'] = longdate_indo($row->tanggal_terkonfirmasi);
            $templist[$key]['rt_domisili_dropdown'] = ($data_rt_domisili->rt == "00" ? $html : "");
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_list_rt_domisili_new()
    {
        $id_master_wilayah = decrypt_data($this->iget("id_master_wilayah"));

        $data_rt = $this->master_rt_model->get(
            array(
                "where" => array(
                    "master_wilayah_id" => $id_master_wilayah
                ),
                "order_by" => array(
                    "rt" => "ASC"
                )
            )
        );

        $templist = array();
        foreach ($data_rt as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_rt);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_count_message_change_rt_bpbd()
    {
        if ($this->session->userdata("id_user") == "62") {

            $data = $this->master_wilayah_model->query(
                "
                SELECT COUNT(*) AS count_notif
                FROM 
                (
                    SELECT id_master_wilayah,nama_wilayah, id_master_rt, rt, jumlah_peserta
                    FROM master_wilayah
                    INNER JOIN
                    (
                        SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id, id_master_rt,rt
                        FROM peserta
                        INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                        INNER JOIN 
                        (
                            SELECT *
                            FROM trx_terkonfirmasi_rilis
                            WHERE id_trx_terkonfirmasi_rilis IN 
                            (
                                SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                FROM trx_terkonfirmasi_rilis 
                                WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                GROUP BY peserta_id
                                ORDER BY id_trx_terkonfirmasi_rilis DESC
                            )
                        ) AS c ON c.peserta_id=id_peserta
                        WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL AND master_rt_domisili_id_ppkm IS NOT NULL
                        GROUP BY kelurahan_master_wilayah_id,id_master_rt
                    ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
                    WHERE klasifikasi IN ('KEL','DESA') AND rt IS NOT NULL
                ) AS tbl
                "
            )->row();

            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
        }
    }

    public function get_list_perubahan_rt_domisili()
    {
        $data_perubahan = $this->master_wilayah_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah, id_master_rt, rt, jumlah_peserta
            FROM master_wilayah
            INNER JOIN
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id, id_master_rt,rt
                FROM peserta
                INNER JOIN master_rt ON id_master_rt = master_rt_domisili_id
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN 
                    (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND master_rt_domisili_id IS NOT NULL AND master_rt_domisili_id_ppkm IS NOT NULL
                GROUP BY kelurahan_master_wilayah_id,id_master_rt
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND rt IS NOT NULL
            "
        )->result();

        $templist = array();
        foreach ($data_perubahan as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data = $templist;

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
