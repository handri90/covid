<style>
    .input-group {
        flex-wrap: initial;
    }
</style>
<div class="content">
    <div class="card border-top-success justify-content-md-center">
        <div class="card-body">
            <span class="alert_form"></span>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Kelurahan : </label>
                        <select class="form-control select-search" name="kelurahan" onchange="get_rt()">
                            <option value="">-- Pilih Kelurahan --</option>
                            <?php
                            foreach ($list_wilayah as $key => $row) {
                            ?>
                                <option value="<?php echo encrypt_data($row->id_master_wilayah); ?>"><?php echo $row->nama_wilayah; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card border-top-success justify-content-md-center">
        <div class="card-body">
            <div class="row mb-3">
                <div class="col-md-6">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">RT Domisili Lama</h5>
                    </div>
                    <select class="form-control select-search" multiple name="old_rt" size="15" onchange="get_peserta()">
                    </select>
                </div>
                <div class="col-md-6">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">RT Domisili Baru</h5>
                    </div>
                    <div class="input-group">
                        <select class="form-control select-search" name="new_rt">
                            <option value="">-- Pilih RT Domisili Baru</option>
                        </select>
                        <span class="input-group-append" onclick="show_form_rt_domisili_baru()">
                            <button class="btn bg-teal" type="button"><i class="icon icon-database-add"></i></button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="text-right">
                <a href="#saveMigrate" onclick="save_migrate_rt_domisili()" class="btn btn-primary">Pindah <i class="icon-paperplane ml-2"></i></a>
            </div>
        </div>
    </div>

    <div class="card border-top-success justify-content-md-center">
        <div class="card-body">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">List Peserta</h5>
            </div>
            <div class="card card-table">
                <table id="datatablePeserta" class="table datatable-save-state">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>NIK</th>
                            <th>Alamat</th>
                            <th>RT</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="modalRT" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Tambah RT Domisili Baru</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <span class="alert_form"></span>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">RT <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="rt" placeholder="RT">
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary" onclick="save_rt()">Simpan <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let datatablePeserta = $("#datatablePeserta").DataTable();

    function get_rt() {
        let kelurahan = $("select[name='kelurahan']").val();
        $("select[name='old_rt']").html("");
        $("select[name='new_rt']").html("<option value=''>-- Pilih RT Domisili Baru</option>");
        datatablePeserta.clear().draw();

        if (kelurahan) {
            $.ajax({
                url: base_url + 'migrate_rt_domisili/request/get_old_rt',
                data: {
                    kelurahan: kelurahan
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    let html = "";
                    $.each(response, function(index, value) {
                        html += "<option value='" + value.rt_domisili + "'>" + value.rt_domisili + "</option>";
                    });

                    $("select[name='old_rt']").html(html);
                },
                complete: function() {
                    HoldOn.close();
                }
            });
            get_rt_domisili_baru(kelurahan);
        } else {
            $("select[name='old_rt']").html("");
        }
    }

    function get_rt_domisili_baru(kelurahan) {
        if (kelurahan) {
            $.ajax({
                url: base_url + 'migrate_rt_domisili/request/get_new_rt',
                data: {
                    kelurahan: kelurahan
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    let html = "";
                    html += '<option value="">-- Pilih RT Domisili Baru</option>'
                    $.each(response, function(index, value) {
                        html += "<option value='" + value.id_encrypt + "'>" + value.rt + "</option>";
                    });

                    $("select[name='new_rt']").html(html);
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        } else {
            $("select[name='new_rt']").html("<option value=''>-- Pilih RT Domisili Baru</option>");
        }
    }

    function save_migrate_rt_domisili() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let kelurahan = $("select[name='kelurahan']").val();
        let old_rt = $("select[name='old_rt']").val();
        let new_rt = $("select[name='new_rt']").val();

        if (!kelurahan) {
            $(".alert_form").html("<div class='alert alert-danger'>Kelurahan tidak boleh kosong.</div>");
        } else if (old_rt.length == 0) {
            $(".alert_form").html("<div class='alert alert-danger'>RT Domisili Lama tidak boleh kosong.</div>");
        } else if (!new_rt) {
            $(".alert_form").html("<div class='alert alert-danger'>RT Domisili Baru tidak boleh kosong.</div>");
        } else {
            $.ajax({
                url: base_url + 'migrate_rt_domisili/save_migrate_rt_domisili',
                data: {
                    kelurahan: kelurahan,
                    old_rt: old_rt,
                    new_rt: new_rt,
                },
                type: 'POST',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    get_rt();
                    get_peserta();
                    if (response) {
                        swalInit(
                            'Berhasil',
                            'Data sudah dihapus',
                            'success'
                        );
                    } else {
                        swalInit(
                            'Gagal',
                            'Data tidak bisa dihapus',
                            'error'
                        );
                    }
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }

    function get_peserta() {
        let kelurahan = $("select[name='kelurahan']").val();
        let old_rt = $("select[name='old_rt']").val();

        datatablePeserta.clear().draw();
        if (old_rt.length != 0) {
            $.ajax({
                url: base_url + 'migrate_rt_domisili/request/get_peserta',
                data: {
                    kelurahan: kelurahan,
                    old_rt: old_rt
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    let no = 1;
                    $.each(response, function(index, value) {
                        datatablePeserta.row.add([
                            no,
                            value.nama,
                            value.nik,
                            value.alamat_domisili,
                            value.rt_domisili,
                            ""
                        ]).draw(false);
                        no++;
                    });
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }

    function show_form_rt_domisili_baru() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let kelurahan = $("select[name='kelurahan']").val();

        if (!kelurahan) {
            swalInit(
                'Gagal',
                'Kelurahan Belum Dipilih',
                'error'
            );
        } else {
            $("#modalRT").modal("show");
            $("input[name='rt']").val("");
        }

    }

    function save_rt() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let rt = $("input[name='rt']").val();
        let kelurahan = $("select[name='kelurahan']").val();

        if (!kelurahan) {
            $(".alert_form").html("<div class='alert alert-danger'>Pilihan kelurahan tidak boleh kosong.</div>");
        } else if (!rt) {
            $(".alert_form").html("<div class='alert alert-danger'>Nama Rekanan tidak boleh kosong.</div>");
        } else {
            $.ajax({
                url: base_url + 'migrate_rt_domisili/save_rt',
                data: {
                    rt: rt,
                    kelurahan: kelurahan,
                },
                type: 'POST',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    swalInit(
                        'Berhasil',
                        'Data berhasil dimasukkan',
                        'success'
                    );
                    $("input[name='rt']").val("");
                    $("#modalRT").modal("toggle");
                    get_rt_domisili_baru(kelurahan);
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }
</script>