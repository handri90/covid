<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migrate_rt_domisili extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model("peserta/master_wilayah_model", "master_wilayah_model");
        $this->load->model('peserta/peserta_model', 'peserta_model');
        $this->load->model('peserta/peserta_non_puskesmas_model', 'peserta_non_puskesmas_model');
        $this->load->model('ppkm/peserta_dalam_satu_rumah_model', 'peserta_dalam_satu_rumah_model');
        $this->load->model("master_rt_model");
    }

    public function index()
    {
        $data['list_wilayah'] = $this->master_wilayah_model->get(
            array(
                "where_false" => "klasifikasi IN ('KEL','DESA') AND id_master_wilayah != '109'",
                "order_by" => array(
                    "nama_wilayah" => "ASC"
                )
            )
        );
        $data['breadcrumb'] = [['link' => false, 'content' => 'Migrate RT Domisili', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function save_migrate_rt_domisili()
    {
        $kelurahan = decrypt_data($this->ipost("kelurahan"));
        $old_rt = $this->ipost("old_rt");
        $new_rt = decrypt_data($this->ipost("new_rt"));

        $gen_old_rt = array();

        foreach ($old_rt as $val) {
            if ($val != "null") {
                array_push($gen_old_rt, $val);
            }
        }

        $wh_rt_domisili_arr = array();
        $wh_rt_domisili_not_null = "";
        if (count($gen_old_rt) > 0) {
            $old_rt_implode = "'" . implode("','", $gen_old_rt) . "'";
            $wh_rt_domisili_not_null = "rt_domisili IN (${old_rt_implode})";
            array_push($wh_rt_domisili_arr, $wh_rt_domisili_not_null);
        }

        $wh_rt_domisili_is_null = "";
        if (in_array("null", $old_rt)) {
            $wh_rt_domisili_is_null = "rt_domisili IS NULL";
            array_push($wh_rt_domisili_arr, $wh_rt_domisili_is_null);
        }

        $wh_rt_domisili_impl = " AND (" . implode(" OR ", $wh_rt_domisili_arr) . ")";

        $data_peserta = $this->peserta_model->query(
            "
            SELECT *
            FROM
            (
            SELECT id_peserta,nama,nik,alamat_domisili,rt_domisili,'non_klinik_swasta' AS asal_data
            FROM peserta
            WHERE kelurahan_master_wilayah_id = '${kelurahan}' ${wh_rt_domisili_impl}
            UNION ALL
            SELECT id_peserta_non_puskesmas,nama,nik,alamat_domisili,rt_domisili,'klinik_swasta' AS asal_data
            FROM peserta_non_puskesmas
            WHERE kelurahan_master_wilayah_id = '${kelurahan}' ${wh_rt_domisili_impl}
            ) AS tbl
            "
        )->result();

        foreach ($data_peserta as $key => $val) {
            if ($val->asal_data == "klinik_swasta") {
                //non_puskesmas
                $data_peserta = array(
                    "master_rt_domisili_id" => $new_rt
                );

                $this->peserta_non_puskesmas_model->edit($val->id_peserta, $data_peserta);
            } else {
                //puskesmas
                $data_peserta = array(
                    "master_rt_domisili_id" => $new_rt
                );

                $this->peserta_model->edit($val->id_peserta, $data_peserta);
            }
        }

        foreach ($old_rt as $key => $val) {
            $get_data_peserta_dalam_satu_rumah = $this->peserta_dalam_satu_rumah_model->get(
                array(
                    "where" => array(
                        "kelurahan_master_wilayah_id" => $kelurahan,
                        "rt_domisili" => $val,
                    )
                )
            );

            if ($get_data_peserta_dalam_satu_rumah) {
                foreach ($get_data_peserta_dalam_satu_rumah as $key_rumah => $val_rumah) {
                    $data_rumah = array(
                        "master_rt_domisili_id" => $new_rt
                    );

                    $this->peserta_dalam_satu_rumah_model->edit($val_rumah->id_peserta_dalam_satu_rumah, $data_rumah);
                }
            }
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(true));
    }

    public function save_rt()
    {
        $data = array(
            "rt"     => $this->ipost("rt"),
            "master_wilayah_id" => decrypt_data($this->ipost("kelurahan")),
            "created_at" => $this->datetime()
        );

        $status = $this->master_rt_model->save($data);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }
}
