<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('peserta/peserta_model', 'peserta_model');
        $this->load->model("master_rt_model");
    }

    public function get_old_rt()
    {
        $kelurahan = decrypt_data($this->iget("kelurahan"));

        $data_peserta = $this->peserta_model->query(
            "
            SELECT *
            FROM
            (
            SELECT rt_domisili,COUNT(id_peserta) AS jumlah
            FROM peserta
            WHERE master_rt_domisili_id IS NULL AND kelurahan_master_wilayah_id = '${kelurahan}'
            GROUP BY rt_domisili
            UNION ALL
            SELECT rt_domisili,COUNT(id_peserta_non_puskesmas) AS jumlah
            FROM peserta_non_puskesmas
            WHERE master_rt_domisili_id IS NULL AND kelurahan_master_wilayah_id = '${kelurahan}'
            GROUP BY rt_domisili
            ) AS tbl
            GROUP BY rt_domisili
            ORDER BY rt_domisili ASC
            "
        )->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_new_rt()
    {
        $kelurahan = decrypt_data($this->iget("kelurahan"));

        $data_rt = $this->master_rt_model->get(
            array(
                "where" => array(
                    "master_wilayah_id" => $kelurahan
                ),
                "order_by" => array(
                    "rt" => "ASC"
                )
            )
        );

        $templist = array();
        foreach ($data_rt as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_rt);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta()
    {
        $kelurahan = decrypt_data($this->iget("kelurahan"));
        $old_rt = $this->iget("old_rt");

        $gen_old_rt = array();

        foreach ($old_rt as $val) {
            if ($val != "null") {
                array_push($gen_old_rt, $val);
            }
        }

        $wh_rt_domisili_arr = array();
        $wh_rt_domisili_not_null = "";
        if (count($gen_old_rt) > 0) {
            $old_rt_implode = "'" . implode("','", $gen_old_rt) . "'";
            $wh_rt_domisili_not_null = "rt_domisili IN (${old_rt_implode})";
            array_push($wh_rt_domisili_arr, $wh_rt_domisili_not_null);
        }

        $wh_rt_domisili_is_null = "";
        if (in_array("null", $old_rt)) {
            $wh_rt_domisili_is_null = "rt_domisili IS NULL";
            array_push($wh_rt_domisili_arr, $wh_rt_domisili_is_null);
        }

        $wh_rt_domisili_impl = " AND (" . implode(" OR ", $wh_rt_domisili_arr) . ")";

        $data_peserta = $this->peserta_model->query(
            "
            SELECT *
            FROM
            (
            SELECT id_peserta,nama,nik,alamat_domisili,rt_domisili,'non_klinik_swasta'
            FROM peserta
            WHERE master_rt_domisili_id IS NULL AND kelurahan_master_wilayah_id = '${kelurahan}' ${wh_rt_domisili_impl}
            UNION ALL
            SELECT id_peserta_non_puskesmas,nama,nik,alamat_domisili,rt_domisili,'klinik_swasta'
            FROM peserta_non_puskesmas
            WHERE master_rt_domisili_id IS NULL AND kelurahan_master_wilayah_id = '${kelurahan}' ${wh_rt_domisili_impl}
            ) AS tbl
            "
        )->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
