<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("daftar_vaksin_model");
        $this->load->model("jenis_vaksin_model");
        $this->load->model('peserta_vaksin/detail_daftar_vaksin_model', 'detail_daftar_vaksin_model');
        $this->load->model('migrate_rt_domisili/master_rt_model', 'master_rt_model');
        $this->load->model('jadwal_vaksin/jadwal_vaksin_model', 'jadwal_vaksin_model');
    }

    public function get_peserta_vaksin_puskesmas()
    {
        $kategori_ticket = $this->iget("kategori_ticket");
        $pilihan_dosis = $this->iget("pilihan_dosis");
        $kel_desa = decrypt_data($this->iget("kel_desa"));
        $rt = decrypt_data($this->iget("rt"));
        $star_date = $this->iget("start_age");
        $end_date = $this->iget("end_age");
        $arr_peserta_id = $this->iget("arr_peserta_id");

        $exp_arr_peserta_id = explode(",", $arr_peserta_id);

        $wh_kategori = array();
        if ($kategori_ticket) {
            $wh_kategori['kategori_ticket'] = $kategori_ticket;
        }

        if ($kel_desa) {
            $wh_kategori['kelurahan_master_wilayah_id'] = $kel_desa;
        }

        if ($rt) {
            $wh_kategori['master_rt_id'] = $rt;
        }

        if ($pilihan_dosis) {
            $wh_kategori['pilihan_vaksin'] = $pilihan_dosis;
        }

        $range_umur = "";
        if ($star_date && $end_date) {
            $range_umur = " AND IFNULL(TIMESTAMPDIFF(YEAR, `tanggal_lahir`, CURDATE()), 0) BETWEEN " . $star_date . " AND " . $end_date;
        }

        $peserta = $this->daftar_vaksin_model->get(
            array(
                "fields" => "id_daftar_vaksin,nama,nik,IF(pertanyaan_1 = '1','TIDAK',IF(pertanyaan_1 = '2','YA','')) AS pertanyaan_1,IF(pertanyaan_2 = '1','TIDAK',IF(pertanyaan_2 = '2','YA','')) AS pertanyaan_2,IF(pertanyaan_3 = '1','TIDAK',IF(pertanyaan_3 = '2','YA','')) AS pertanyaan_3,IF(pertanyaan_4 = '1','TIDAK',IF(pertanyaan_4 = '2','YA','')) AS pertanyaan_4,IF(pertanyaan_5 = '1','TIDAK',IF(pertanyaan_5 = '2','YA','')) AS pertanyaan_5,IF(pertanyaan_6 = '1','TIDAK',IF(pertanyaan_6 = '2','YA','')) AS pertanyaan_6,IF(pertanyaan_7 = '1','TIDAK',IF(pertanyaan_7 = '2','YA','')) AS pertanyaan_7,IF(pertanyaan_8 = '1','TIDAK',IF(pertanyaan_8 = '2','YA','')) AS pertanyaan_8,IF(pertanyaan_9 = '1','TIDAK',IF(pertanyaan_9 = '2','YA','')) AS pertanyaan_9,IF(pertanyaan_10 = '1','TIDAK',IF(pertanyaan_10 = '2','YA','')) AS pertanyaan_10,IF(pertanyaan_11 = '1','TIDAK',IF(pertanyaan_11 = '2','YA','')) AS pertanyaan_11,IF(pertanyaan_12 = '1','TIDAK',IF(pertanyaan_12 = '2','YA','')) AS pertanyaan_12,IF(usia_kehamilan = '1','Trisemester 1 (sd 13 minggu)',IF(usia_kehamilan = '2','Trisemester 2 (14 sd 28 minggu)',IF(usia_kehamilan = '3','Trisemester 3 (29 minggu s.d aterm)',''))) AS usia_kehamilan, IF(hasil_skrining_petugas = '1','Lanjut Vaksin',IF(hasil_skrining_petugas = '2','Ditunda',IF(hasil_skrining_petugas = '3','Tidak Diberikan',''))) AS hasil_skrining_petugas, IF(kategori_ticket = '1','Umum',IF(kategori_ticket = '2','Anak-anak',IF(kategori_ticket = '3','Ibu Hamil',''))) AS kategori_ticket,IFNULL(TIMESTAMPDIFF(YEAR, `tanggal_lahir`, CURDATE()), 0) AS umur,DATE_FORMAT(tanggal_lahir,'%d-%m-%Y') AS tanggal_lahir,nama_wilayah,rt,alamat",
                "join" => array(
                    "master_wilayah" => "id_master_wilayah=kelurahan_master_wilayah_id",
                    "master_rt" => "id_master_rt=master_rt_id",
                ),
                "where" => $wh_kategori,
                "where_false" => "hasil_skrining_petugas IS NULL " . $range_umur . " AND kelurahan_master_wilayah_id IN (SELECT id_master_wilayah FROM master_wilayah
                WHERE puskesmas_id = '" . $this->session->userdata('puskesmas_id') . "')",
                "order_by" => array(
                    "daftar_vaksin.created_at" => "ASC"
                )
            )
        );

        $templist = array();
        $no = 1;
        foreach ($peserta as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['no'] = $no++;
            if ($row->hasil_skrining_petugas == "") {
                $checkbox = "";
                if (in_array($row->id_daftar_vaksin, $exp_arr_peserta_id)) {
                    $checkbox = "checked";
                }
                $templist[$key]['checkbox_vaksin'] = "<input " . $checkbox . " type='checkbox' onclick='select_for_hasil_skrining(this)' name='id_daftar_vaksin[]' value='" . $row->id_daftar_vaksin . "' />";
            } else {
                $templist[$key]['checkbox_vaksin'] = "";
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_daftar_vaksin);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_daftar_peserta_vaksin_by_id()
    {
        $id_daftar_vaksin = decrypt_data($this->iget("id_daftar_vaksin"));
        $data = $this->daftar_vaksin_model->get(
            array(
                "fields" => "id_daftar_vaksin,nama,nik,IF(pertanyaan_1 = '1','TIDAK',IF(pertanyaan_1 = '2','YA','')) AS pertanyaan_1,IF(pertanyaan_2 = '1','TIDAK',IF(pertanyaan_2 = '2','YA','')) AS pertanyaan_2,IF(pertanyaan_3 = '1','TIDAK',IF(pertanyaan_3 = '2','YA','')) AS pertanyaan_3,IF(pertanyaan_4 = '1','TIDAK',IF(pertanyaan_4 = '2','YA','')) AS pertanyaan_4,IF(pertanyaan_5 = '1','TIDAK',IF(pertanyaan_5 = '2','YA','')) AS pertanyaan_5,IF(pertanyaan_6 = '1','TIDAK',IF(pertanyaan_6 = '2','YA','')) AS pertanyaan_6,IF(pertanyaan_7 = '1','TIDAK',IF(pertanyaan_7 = '2','YA','')) AS pertanyaan_7,IF(pertanyaan_8 = '1','TIDAK',IF(pertanyaan_8 = '2','YA','')) AS pertanyaan_8,IF(pertanyaan_9 = '1','TIDAK',IF(pertanyaan_9 = '2','YA','')) AS pertanyaan_9,IF(pertanyaan_10 = '1','TIDAK',IF(pertanyaan_10 = '2','YA','')) AS pertanyaan_10,IF(pertanyaan_11 = '1','TIDAK',IF(pertanyaan_11 = '2','YA','')) AS pertanyaan_11,IF(pertanyaan_12 = '1','TIDAK',IF(pertanyaan_12 = '2','YA','')) AS pertanyaan_12,IF(usia_kehamilan = '1','Trisemester 1 (sd 13 minggu)',IF(usia_kehamilan = '2','Trisemester 2 (14 sd 28 minggu)',IF(usia_kehamilan = '3','Trisemester 3 (29 minggu s.d aterm)',''))) AS usia_kehamilan, IF(hasil_skrining_petugas = '1','Lanjut Vaksin',IF(hasil_skrining_petugas = '2','Ditunda',IF(hasil_skrining_petugas = '3','Tidak Diberikan',''))) AS hasil_skrining_petugas,kategori_ticket, IF(kategori_ticket = '1','Umum',IF(kategori_ticket = '2','Anak-anak',IF(kategori_ticket = '3','Ibu Hamil',''))) AS kategori_ticket_custom,IFNULL(TIMESTAMPDIFF(YEAR, `tanggal_lahir`, CURDATE()), 0) AS umur,DATE_FORMAT(tanggal_lahir,'%d-%m-%Y') AS tanggal_lahir,nama_wilayah,rt,alamat",
                "join" => array(
                    "master_wilayah" => "id_master_wilayah=kelurahan_master_wilayah_id",
                    "master_rt" => "id_master_rt=master_rt_id",
                ),
                "where" => array(
                    "id_daftar_vaksin" => $id_daftar_vaksin
                ),
            ),
            "row"
        );

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_checlist()
    {
        $id_daftar_vaksin = $this->iget("id_daftar_vaksin");
        $imp_id_daftar_vaksin = "'" . implode("','", $id_daftar_vaksin) . "'";

        $peserta = $this->daftar_vaksin_model->get(
            array(
                "fields" => "id_daftar_vaksin,nama,nik,IF(pertanyaan_1 = '1','TIDAK',IF(pertanyaan_1 = '2','YA','')) AS pertanyaan_1,IF(pertanyaan_2 = '1','TIDAK',IF(pertanyaan_2 = '2','YA','')) AS pertanyaan_2,IF(pertanyaan_3 = '1','TIDAK',IF(pertanyaan_3 = '2','YA','')) AS pertanyaan_3,IF(pertanyaan_4 = '1','TIDAK',IF(pertanyaan_4 = '2','YA','')) AS pertanyaan_4,IF(pertanyaan_5 = '1','TIDAK',IF(pertanyaan_5 = '2','YA','')) AS pertanyaan_5,IF(pertanyaan_6 = '1','TIDAK',IF(pertanyaan_6 = '2','YA','')) AS pertanyaan_6,IF(pertanyaan_7 = '1','TIDAK',IF(pertanyaan_7 = '2','YA','')) AS pertanyaan_7,IF(pertanyaan_8 = '1','TIDAK',IF(pertanyaan_8 = '2','YA','')) AS pertanyaan_8,IF(pertanyaan_9 = '1','TIDAK',IF(pertanyaan_9 = '2','YA','')) AS pertanyaan_9,IF(pertanyaan_10 = '1','TIDAK',IF(pertanyaan_10 = '2','YA','')) AS pertanyaan_10,IF(pertanyaan_11 = '1','TIDAK',IF(pertanyaan_11 = '2','YA','')) AS pertanyaan_11,IF(pertanyaan_12 = '1','TIDAK',IF(pertanyaan_12 = '2','YA','')) AS pertanyaan_12,IF(usia_kehamilan = '1','Trisemester 1 (sd 13 minggu)',IF(usia_kehamilan = '2','Trisemester 2 (14 sd 28 minggu)',IF(usia_kehamilan = '3','Trisemester 3 (29 minggu s.d aterm)',''))) AS usia_kehamilan, IF(hasil_skrining_petugas = '1','Lanjut Vaksin',IF(hasil_skrining_petugas = '2','Ditunda',IF(hasil_skrining_petugas = '3','Tidak Diberikan',''))) AS hasil_skrining_petugas, IF(kategori_ticket = '1','Umum',IF(kategori_ticket = '2','Anak-anak',IF(kategori_ticket = '3','Ibu Hamil',''))) AS kategori_ticket,IFNULL(TIMESTAMPDIFF(YEAR, `tanggal_lahir`, CURDATE()), 0) AS umur,DATE_FORMAT(tanggal_lahir,'%d-%m-%Y') AS tanggal_lahir",
                "where_false" => "id_daftar_vaksin IN (" . $imp_id_daftar_vaksin . ")",
                "order_by" => array(
                    "daftar_vaksin.created_at" => "DESC"
                )
            )
        );

        $templist = array();
        $no = 1;
        foreach ($peserta as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['no'] = $no++;
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_jenis_vaksin()
    {

        $jenis_vaksin = $this->jenis_vaksin_model->get();

        $templist = array();
        foreach ($jenis_vaksin as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_jenis_vaksin);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_jadwal_vaksin()
    {

        $jadwal_vaksin = $this->jadwal_vaksin_model->get(
            array(
                "where" => array(
                    "id_user_created" => $this->session->userdata('id_user')
                )
            )
        );

        $templist = array();
        foreach ($jadwal_vaksin as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['tanggal_jam_custom'] = longdate_indo($row->tanggal_vaksin) . " " . $row->jam_vaksin;
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_jadwal_vaksin);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_jadwal_vaksinasi()
    {
        $data_jadwal = $this->detail_daftar_vaksin_model->get(
            array(
                "fields" => "CONCAT(DATE_FORMAT(tanggal_vaksin,'%d-%m-%Y'),' ',TIME_FORMAT(jam_vaksin,'%H:%i')) AS tanggal_vaksin",
                "group_by" => "tanggal_vaksin,jam_vaksin,jenis_vaksin_id"
            )
        );

        $templist = array();
        foreach ($data_jadwal as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_jadwal_jenis_vaksinasi()
    {
        $jadwal_vaksin = decrypt_data($this->iget("jadwal_vaksin"));

        $jenis_vaksin = $this->daftar_vaksin_model->get(
            array(
                "fields" => "id_jenis_vaksin,nama_vaksin",
                "join" => array(
                    "jenis_vaksin" => "id_jenis_vaksin=jenis_vaksin_id"
                ),
                "where" => array(
                    "jadwal_vaksin_id" => $jadwal_vaksin
                ),
                "group_by" => "id_jenis_vaksin"

            )
        );

        $templist = array();
        foreach ($jenis_vaksin as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_jenis_vaksin);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_jadwal_vaksin()
    {
        $jenis_vaksin = decrypt_data($this->iget("jenis_vaksin"));
        $jadwal_vaksin = decrypt_data($this->iget("jadwal_vaksin"));

        $wh = array();
        if ($jenis_vaksin) {
            $wh['jenis_vaksin_id'] = $jenis_vaksin;
        }

        if ($jadwal_vaksin) {
            $wh['jadwal_vaksin_id'] = $jadwal_vaksin;
        }

        $data_peserta = $this->daftar_vaksin_model->get(
            array(
                "fields" => "
                id_daftar_vaksin,nama,nik,DATE_FORMAT(tanggal_lahir,'%d-%m-%Y') AS tanggal_lahir,no_hp,alamat,
                IFNULL(pertanyaan_1,'') AS pertanyaan_1,
                IFNULL(pertanyaan_2,'') AS pertanyaan_2,
                IFNULL(pertanyaan_3,'') AS pertanyaan_3,
                IFNULL(pertanyaan_4,'') AS pertanyaan_4,
                IFNULL(pertanyaan_5,'') AS pertanyaan_5,	
                IFNULL(pertanyaan_6,'') AS pertanyaan_6,
                IFNULL(pertanyaan_7,'') AS pertanyaan_7,
                IFNULL(pertanyaan_8,'') AS pertanyaan_8,
                IFNULL(pertanyaan_9,'') AS pertanyaan_9,
                IFNULL(pertanyaan_10,'') AS pertanyaan_10,
                IFNULL(pertanyaan_11,'') AS pertanyaan_11,
                IFNULL(pertanyaan_12,'') AS pertanyaan_12,
                kategori_ticket,usia_kehamilan,kelurahan_master_wilayah_id,master_rt_id,nama_wilayah,rt,
                IFNULL(hasil_skrining_petugas,'') AS hasil_skrining_petugas,
                IFNULL(tempat_pelaksanaan,'') AS tempat_pelaksanaan, nama_puskesmas, 
                IFNULL(nama_vaksin,'') AS nama_vaksin,
                IFNULL(DATE_FORMAT(jam_vaksin,'%H:%i'),'') AS jam_vaksin,
                IFNULL(tanggal_vaksin,'') AS tanggal_vaksin,
                IFNULL(is_confirm,'') AS is_confirm,IFNULL(no_antrian,'') AS no_antrian,pilihan_vaksin,
                IF(hasil_skrining_petugas='1',
                    UNIX_TIMESTAMP(DATE_FORMAT(
                        IF(before_after_schedule_vaksin = '1',
                            IF(satuan_interval = 'HOUR',
                                DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                                DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                            ),
                            IF(satuan_interval = 'HOUR',
                                DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                                DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                            )
                        ), '%Y-%m-%d %H:%i:%s')
                    ),''
                ) AS timestamp_tanggal_jam_vaksin,
                IF(hasil_skrining_petugas='1',
                    FROM_UNIXTIME(UNIX_TIMESTAMP(DATE_FORMAT(
                        IF(before_after_schedule_vaksin = '1',
                            IF(satuan_interval = 'HOUR',
                                DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                                DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                            ),
                            IF(satuan_interval = 'HOUR',
                                DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                                DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                            )
                        ), '%Y-%m-%d %H:%i:%s')
                    )),''
                ) AS tanggal_terakhir_konfirmasi,
                UNIX_TIMESTAMP() AS timestamp_tanggal_jam_now,
                FROM_UNIXTIME(UNIX_TIMESTAMP()) AS tanggal_jam_server,
                is_undangan_cadangan,is_done",
                "join" => array(
                    "jadwal_vaksin" => "id_jadwal_vaksin=jadwal_vaksin_id",
                    "jenis_vaksin" => "id_jenis_vaksin=jenis_vaksin_id",
                    "master_wilayah" => "id_master_wilayah=kelurahan_master_wilayah_id",
                    "master_rt" => "id_master_rt=master_rt_id",
                    "master_puskesmas" => "puskesmas_id=id_master_puskesmas"
                ),
                "where" => $wh,
                "where_false" => "hasil_skrining_petugas = '1' AND kelurahan_master_wilayah_id IN (SELECT id_master_wilayah FROM master_wilayah
                WHERE puskesmas_id = '" . $this->session->userdata('puskesmas_id') . "')",
                "order_by" => array(
                    "CAST(no_antrian AS UNSIGNED)" => "ASC"
                )
            )
        );

        $templist = array();
        $no = 1;
        $jumlah_hadir = 0;
        $jumlah_tidak_hadir = 0;
        $jumlah_belum_konfirmasi = 0;
        foreach ($data_peserta as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $tanggal_terakhir_konfirmasi = date("Y-m-d", strtotime($row->tanggal_terakhir_konfirmasi));
            $jam_terakhir_konfirmasi = date("H:i", strtotime($row->tanggal_terakhir_konfirmasi));


            $templist[$key]['tanggal_terakhir_konfirmasi'] = longdate_indo($tanggal_terakhir_konfirmasi) . " " . $jam_terakhir_konfirmasi;

            $templist[$key]['no'] = $no++;

            if ($row->is_confirm == '1') {
                $jumlah_hadir++;
                $templist[$key]['status_hadir'] = "Hadir";
                $templist[$key]['checkbox_vaksin_selesai'] = "<input " . ($row->is_done == "2" ? "checked" : "") . " type='checkbox' name='id_daftar_vaksin' onclick=\"selesai_vaksin('" . encrypt_data($row->id_daftar_vaksin) . "',this)\" />";
            } else if ($row->is_confirm == '2') {
                $jumlah_tidak_hadir++;
                $templist[$key]['status_hadir'] = "Tidak Hadir";
                $templist[$key]['checkbox_vaksin_selesai'] = "";
            } else if ($row->is_confirm == '' && $row->timestamp_tanggal_jam_now > $row->timestamp_tanggal_jam_vaksin) {
                $jumlah_tidak_hadir++;
                $templist[$key]['status_hadir'] = "Tidak Hadir";
                $templist[$key]['checkbox_vaksin_selesai'] = "";
            } else if ($row->is_confirm == '' && $row->timestamp_tanggal_jam_now < $row->timestamp_tanggal_jam_vaksin) {
                $jumlah_belum_konfirmasi++;
                $templist[$key]['status_hadir'] = "Belum Konfirmasi Kehadiran";
                $templist[$key]['checkbox_vaksin_selesai'] = "";
            }
        }

        $data = array("data" => $templist, "info_konfirmasi" => array("jumlah_hadir" => $jumlah_hadir, "jumlah_tidak_hadir" => $jumlah_tidak_hadir, "jumlah_belum_konfirmasi" => $jumlah_belum_konfirmasi));
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_tunda_vaksin()
    {
        $data_peserta = $this->daftar_vaksin_model->get(
            array(
                "fields" => "nama,nik",
                "where" => array(
                    "hasil_skrining_petugas" => "2",
                ),
                "where_false" => "kelurahan_master_wilayah_id IN (SELECT id_master_wilayah FROM master_wilayah
                WHERE puskesmas_id = '" . $this->session->userdata('puskesmas_id') . "')"
            )
        );

        $templist = array();
        $no = 1;
        foreach ($data_peserta as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['no'] = $no++;
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_tidak_diberikan_vaksin()
    {
        $data_peserta = $this->daftar_vaksin_model->get(
            array(
                "fields" => "nama,nik",
                "where" => array(
                    "hasil_skrining_petugas" => "3",
                ),
                "where_false" => "kelurahan_master_wilayah_id IN (SELECT id_master_wilayah FROM master_wilayah
                WHERE puskesmas_id = '" . $this->session->userdata('puskesmas_id') . "')"
            )
        );

        $templist = array();
        $no = 1;
        foreach ($data_peserta as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['no'] = $no++;
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_vaksin()
    {
        $puskesmas = decrypt_data($this->iget("puskesmas"));

        $peserta = $this->daftar_vaksin_model->get(
            array(
                "fields" => "id_daftar_vaksin,nama,nik, IF(hasil_skrining_petugas = '1', 'Lanjut Vaksin', 
                IF(hasil_skrining_petugas = '2', 'Ditunda', 
                IF(hasil_skrining_petugas = '3', 'Tidak Diberikan', 'Belum Diskrining'))) AS hasil_skrining_petugas, IF(kategori_ticket = '1', 'Umum', IF(kategori_ticket = '2', 'Anak-anak', IF(kategori_ticket = '3', 'Ibu Hamil',''))) AS kategori_ticket",
                "where_false" => "kelurahan_master_wilayah_id IN (SELECT id_master_wilayah FROM master_wilayah
                WHERE puskesmas_id = '" . $puskesmas . "')",
                "order_by" => array(
                    "daftar_vaksin.created_at" => "DESC"
                )
            )
        );

        $templist = array();
        $no = 1;
        foreach ($peserta as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['no'] = $no++;
            if ($row->hasil_skrining_petugas == "") {
                $templist[$key]['checkbox_vaksin'] = "<input type='checkbox' name='id_daftar_vaksin[]' value='" . $row->id_daftar_vaksin . "' />";
            } else {
                $templist[$key]['checkbox_vaksin'] = "";
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_daftar_vaksin);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_master_rt()
    {
        $kel_desa = decrypt_data($this->iget("kel_desa"));

        $data_rt = $this->master_rt_model->get(
            array(
                "where" => array(
                    "master_wilayah_id" => $kel_desa
                ),
                "join" => array(
                    "daftar_vaksin" => "master_rt_id=id_master_rt"
                ),
                "order_by" => array(
                    "rt" => "ASC"
                ),
                "group_by" => "id_master_rt"
            )
        );

        $templist = array();
        foreach ($data_rt as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_rt);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
