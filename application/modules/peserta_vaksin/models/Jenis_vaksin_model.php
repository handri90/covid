<?php

class Jenis_vaksin_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "jenis_vaksin";
        $this->primary_id = "id_jenis_vaksin";
    }
}
