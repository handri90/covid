<?php

class Detail_daftar_vaksin_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "detail_daftar_vaksin";
        $this->primary_id = "id_detail_daftar_vaksin";
    }
}
