<html>

<head>
    <style>
        body {
            font-family: Georgia, 'Times New Roman', serif;
            font-size: 9pt;
            font-weight: normal;
        }

        .head_print {
            text-align: center;
            font-weight: 100;
            line-height: 0.6;
        }

        .wd-number {
            width: 10px;
        }

        .wd-label {
            width: 100px;
        }

        .wd-label-2 {
            width: 60px;

        }

        .wd-peserta {
            width: 200px;
        }

        .wd-peserta-2 {
            width: 310px;
        }

        .wd-titik {
            width: 2px;
        }

        table {
            margin: 5px 0;
        }

        .tbl-sign {
            margin-top: 20px;
            text-align: center;
        }

        .ruler {
            border-bottom: 2px double black;
        }

        .logo {
            float: left;
            width: 20%;
        }

        .deskripsi {
            float: left;
            width: 60%;
        }

        .alamat {
            line-height: 0.5;
        }

        .text-center {
            text-align: center;
        }

        .border-td {
            border: 1px solid #000;
            text-align: center;
        }
    </style>
</head>

<body>
    <table width="100%" style="border-collapse:collapse;">
        <tr>
            <th class="border-td" width="20">NO</th>
            <th class="border-td" width="160">NAMA</th>
            <th class="border-td" width="120">NIK</th>
            <th class="border-td" width="120">JENIS KELAMIN</th>
            <th class="border-td" width="120">ALAMAT</th>
            <th class="border-td" width="120">RT</th>
            <th class="border-td" width="120">DESA / KELURAHAN</th>
            <th class="border-td" width="20">UMUR</th>
            <th class="border-td" width="120">PEKERJAAN</th>
            <th class="border-td" width="120">GEJALA KLINIS</th>
        </tr>
        <?php
        if ($peserta) {
            $no = 1;
            foreach ($peserta as $key => $val) {
        ?>
                <tr>
                    <td class="border-td"><?php echo $no; ?></td>
                    <td class="border-td"><?php echo $val->nama; ?></td>
                    <td class="border-td"><?php echo $val->nik; ?></td>
                    <td class="border-td"><?php echo $val->jenis_kelamin; ?></td>
                    <td class="border-td"><?php echo $val->alamat_ktp; ?></td>
                    <td class="border-td"><?php echo $val->rt_ktp; ?></td>
                    <td class="border-td"><?php echo $val->nama_desa_kel; ?></td>
                    <td class="border-td"><?php echo $val->umur_kena_covid; ?></td>
                    <td class="border-td"><?php echo $val->pekerjaan_inti; ?></td>
                    <td class="border-td"></td>
                </tr>
        <?php
                $no++;
            }
        }
        ?>
    </table>
</body>

</html>