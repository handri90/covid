<style>
    .ft-bld {
        font-weight: bold;
    }
</style>

<div class="content">
    <div class="card">
        <div class="card-body">
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Puskesmas</label>
                    <div class="col-lg-10">
                        <select class="form-control select-search" name="puskesmas" onchange="get_peserta_vaksin_puskesmas()">
                            <option value="">-- Pilih --</option>
                            <?php
                            foreach ($puskesmas as $key => $value) {
                            ?>
                                <option value="<?php echo encrypt_data($value->id_master_puskesmas); ?>"><?php echo $value->nama_puskesmas; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <!-- <div class="form-group row">
                    <label class="col-form-label col-lg-2">Kategori Ticket</label>
                    <div class="col-lg-10">
                        <select class="form-control select-search" name="kategori_ticket" onchange="get_peserta_vaksin_puskesmas()">
                            <option value="">-- Pilih --</option>
                            <option value="1">Umum</option>
                            <option value="2">Anak-Anak</option>
                            <option value="3">Ibu Hamil</option>
                        </select>
                    </div>
                </div> -->
            </div>
            <div class="card card-table umum-table">
                <table id="datatablePesertaVaksin" class="table datatable-save-state table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>NIK</th>
                            <th>Hasil Skrining Petugas</th>
                            <th>Kategori Tiket</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $("#datatablePesertaVaksin").DataTable();

    function get_peserta_vaksin_puskesmas() {
        if ($.fn.DataTable.isDataTable('#datatablePesertaVaksin')) {
            $('#datatablePesertaVaksin').DataTable().destroy();
            // $('#datatablePesertaVaksin').DataTable().clear();
        }
        if ($("select[name='puskesmas']").val()) {
            $("#datatablePesertaVaksin").DataTable({
                ajax: {
                    "url": base_url + 'peserta_vaksin/request/get_peserta_vaksin',
                    "beforeSend": function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    "data": function(d) {
                        return $.extend({}, d, {
                            "puskesmas": $("select[name='puskesmas']").val()
                        });
                    },
                    "dataSrc": '',
                    "complete": function(response) {
                        HoldOn.close();
                    }
                },
                "ordering": false,
                "columns": [{
                    data: "no"
                }, {
                    data: "nama"
                }, {
                    data: "nik"
                }, {
                    data: "hasil_skrining_petugas"
                }, {
                    data: "kategori_ticket"
                }]
            });
        } else {
            $("#datatablePesertaVaksin").destroy();
        }

    }
</script>