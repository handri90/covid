<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model('jenis_pemeriksaan/jenis_pemeriksaan_model', 'jenis_pemeriksaan_model');
        $this->load->model('user_model');
        $this->load->model('peserta/peserta_model', 'peserta_model');
        $this->load->model('peserta/master_wilayah_model', 'master_wilayah_model');
        $this->load->model('migrate_rt_domisili/master_rt_model', 'master_rt_model');
    }

    public function index()
    {
        $data['user'] = $this->user_model->get(
            array(
                "where" => array(
                    "level_user_id" => "9"
                )
            )
        );

        $data['jenis_pemeriksaan'] = $this->jenis_pemeriksaan_model->query(
            "SELECT *
            FROM (
                SELECT id_jenis_pemeriksaan,nama_jenis_pemeriksaan
                FROM jenis_pemeriksaan
                WHERE jenis_pemeriksaan.deleted_at IS NULL
                UNION
                SELECT '9001','Sembuh'
                UNION
                SELECT '9002','Meninggal'
                UNION
                SELECT '9003','Probable'
                UNION
                SELECT '9004','Usulan Faskes : SWAB PCR'
                UNION
                SELECT '9005','Usulan Faskes : Usulan Isolasi Mandiri'
                UNION
                SELECT '9006','Bebas Isolasi Mandiri'
                UNION
                SELECT '9007','Bebas Isolasi Mandiri Suspek'
                ) AS tbl
            ORDER BY id_jenis_pemeriksaan ASC"
        )->result();

        $data['tempat_pemeriksaan'] = $this->user_model->get(
            array(
                "where_false" => "level_user_id IN ('2','7')",
                "order_by" => array(
                    "nama_lengkap" => "ASC"
                )
            )
        );

        $data['breadcrumb'] = [['link' => false, 'content' => 'Laporan', 'is_active' => true]];
        if ($this->session->userdata("level_user_id") == "2") {
            $this->execute('index_faskes_puskesmas', $data);
        } else if ($this->session->userdata("level_user_id") == "4") {
            $this->execute('index_rumah_sakit', $data);
        } else if (in_array($this->session->userdata("level_user_id"), array("6", "11"))) {
            $this->execute('index_dinkes', $data);
        } else {
            $this->execute('index', $data);
        }
    }

    public function cetak_laporan()
    {
        $data['user_cetak'] = $this->user_model->get_by($this->session->userdata("id_user"));

        $data['user_head_lab'] = $this->user_model->get(
            array(
                "where" => array(
                    "is_kepala_laboratorium" => "1"
                )
            ),
            "row"
        );

        $tanggal_pengambilan = $this->iget("tanggal_pengambilan");
        $tanggal_pengujian = $this->iget("tanggal_pengujian");
        $dokter = decrypt_data($this->iget("dokter"));

        $data['dokter'] = $this->user_model->get_by($dokter);

        $data['peserta'] = $this->peserta_model->query(
            "
                SELECT no_rekam_medis,nik,nama,DATE_FORMAT(tanggal_lahir,'%d-%m-%Y') AS tanggal_lahir,jenis_kelamin,a.*,b.jumlah_swab
                FROM peserta
                INNER JOIN 
                (
                    SELECT peserta_id,jenis_spesimen,nama_hasil_pemeriksaan,id_trx_pemeriksaan,trx_pemeriksaan.created_at AS tanggal_pengambilan_sampel,kode_sample
                    FROM trx_pemeriksaan
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id=id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    AND trx_pemeriksaan.jenis_pemeriksaan_id = '1'
                    AND DATE_FORMAT(trx_pemeriksaan.created_at,'%d-%m-%Y') = '" . $tanggal_pengambilan . "'
                    AND DATE_FORMAT(tanggal_terima_sampel,'%d-%m-%Y') = '" . $tanggal_pengujian . "'
                    AND id_user_dokter = '" . $dokter . "'
                    GROUP BY peserta_id
                ) AS a ON a.peserta_id=id_peserta
                INNER JOIN 
                (
                    SELECT COUNT(*) AS jumlah_swab,peserta_id
                    FROM trx_pemeriksaan
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    AND jenis_pemeriksaan_id = '1'
                    AND DATE_FORMAT(trx_pemeriksaan.created_at,'%d-%m-%Y') <= '" . $tanggal_pengambilan . "'
                    GROUP BY peserta_id
                ) AS b ON b.peserta_id=id_peserta
                "
        )->result();

        $data['jumlah_sample'] = 0;
        if ($data['peserta']) {
            $data['jumlah_sample'] = count($data['peserta']);
        }

        $data['tanggal_pengambilan'] = $tanggal_pengambilan;
        $data['tanggal_pengujian'] = $tanggal_pengujian;

        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'orientation' => 'L', 'tempDir' => '/tmp']);
        $data = $this->load->view('cetak_hasil_pcr', $data, TRUE);
        $mpdf->WriteHTML($data);
        $mpdf->Output();
    }

    public function cetak_laporan_puskesmas()
    {
        require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel.php');
        require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

        $filename = "Laporan.xlsx";

        $objPHPExcel = new PHPExcel();

        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");
        $jenis_pemeriksaan = decrypt_data($this->iget("jenis_pemeriksaan"));
        $hasil_pemeriksaan = decrypt_data($this->iget("hasil_pemeriksaan"));

        $wh = "";
        if ($jenis_pemeriksaan > '9000') {
            if ($jenis_pemeriksaan == '9001') {
                $wh = "AND is_sembuh = '1'";
            } else if ($jenis_pemeriksaan == '9002') {
                $wh = "AND is_meninggal = '1'";
            } else if ($jenis_pemeriksaan == '9003') {
                $wh = "AND is_probable = '1'";
            } else if ($jenis_pemeriksaan == '9004') {
                $wh = "AND c.usulan_faskes = '0'";
            } else if ($jenis_pemeriksaan == '9005') {
                $wh = "AND c.usulan_faskes = '1'";
            } else if ($jenis_pemeriksaan == '9006') {
                $wh = "AND c.usulan_faskes = '2'";
            } else if ($jenis_pemeriksaan == '9007') {
                $wh = "AND c.status_rawat = '3'";
            }
        } else if ($jenis_pemeriksaan && $hasil_pemeriksaan) {
            $wh = "AND jenis_pemeriksaan_id = '" . $jenis_pemeriksaan . "' AND hasil_pemeriksaan_id = '" . $hasil_pemeriksaan . "'";
        } else if ($jenis_pemeriksaan && !$hasil_pemeriksaan) {
            $wh = "AND jenis_pemeriksaan_id = '" . $jenis_pemeriksaan . "'";
        }

        $data_peserta = $this->peserta_model->query("
            SELECT *
            FROM (
            SELECT 
            peserta.nama,
            peserta.nik,
            peserta.tanggal_lahir,
            peserta.jenis_kelamin,
            peserta.pekerjaan_inti,
            peserta.alamat_domisili,
            peserta.rt_domisili,
            peserta.master_rt_domisili_id,
            peserta.kelurahan_master_wilayah_id,
            peserta.nomor_telepon,
            peserta.deleted_at,
            peserta.kontak_erat,
            b.level_user_id,
            c.*
            FROM peserta
            LEFT JOIN user AS b ON peserta.id_user_created=b.id_user
            LEFT JOIN 
                (
                SELECT 
                id_trx_pemeriksaan AS id_trx_pemeriksaan_last,
                peserta_id,
                level_user_id AS level_user_id_last,
                nama_jenis_pemeriksaan AS nama_jenis_pemeriksaan_last,
                nama_hasil_pemeriksaan AS nama_hasil_pemeriksaan_last,
                usulan_faskes,
                DATE_FORMAT(trx_pemeriksaan.created_at,'%d-%m-%Y %H:%i:%s') AS tanggal_pemeriksaan,
                DATE_FORMAT(trx_pemeriksaan.updated_at,'%d-%m-%Y %H:%i:%s') AS tanggal_perubahan_pemeriksaan,
                DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') AS tanggal_pemeriksaan_where,
                hasil_pemeriksaan_id,
                trx_pemeriksaan.jenis_pemeriksaan_id,
                is_sembuh,
                is_meninggal,
                is_probable
                FROM trx_pemeriksaan 
                LEFT JOIN jenis_pemeriksaan ON id_jenis_pemeriksaan=jenis_pemeriksaan_id AND jenis_pemeriksaan.deleted_at IS NULL
                LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                LEFT JOIN user ON id_user=id_user_created 
                WHERE id_trx_pemeriksaan IN (
                    SELECT MAX(id_trx_pemeriksaan) AS id_trx_pemeriksaan 
                    FROM trx_pemeriksaan 
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_pemeriksaan DESC
                    ) 
                AND trx_pemeriksaan.deleted_at IS NULL
                ) AS c ON c.peserta_id=id_peserta
            WHERE (peserta.id_user_created='" . $this->session->userdata("id_user") . "' 
            OR kelurahan_master_wilayah_id IN 
            (
                SELECT id_master_wilayah
                FROM master_wilayah
                WHERE puskesmas_id = '" . $this->session->userdata("puskesmas_id") . "'
            ) AND peserta.deleted_at IS NULL)
            AND c.tanggal_pemeriksaan_where BETWEEN '" . $start_date . "' AND '" . $end_date . "'
            {$wh}
            GROUP BY id_peserta
            ORDER BY id_trx_pemeriksaan_last DESC
            ) AS tbl
            WHERE deleted_at IS NULL
            ORDER BY nama ASC
        ")->result();

        $style_header = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true
            ), 'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'dae1f3')
            )
        );

        $style_data = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "Nama");
        $objPHPExcel->getActiveSheet()->SetCellValue("B1", "NIK");
        $objPHPExcel->getActiveSheet()->SetCellValue("C1", "Tanggal Lahir");
        $objPHPExcel->getActiveSheet()->SetCellValue("D1", "Jenis Kelamin");
        $objPHPExcel->getActiveSheet()->SetCellValue("E1", "Pekerjaan");
        $objPHPExcel->getActiveSheet()->SetCellValue("F1", "Alamat Domisili");
        $objPHPExcel->getActiveSheet()->SetCellValue("G1", "Nomor Telepon");
        $objPHPExcel->getActiveSheet()->SetCellValue("H1", "Jenis Pemeriksaan");
        $objPHPExcel->getActiveSheet()->SetCellValue("I1", "Hasil Pemeriksaan");
        $objPHPExcel->getActiveSheet()->SetCellValue("J1", "Tanggal Pemeriksaan");
        $objPHPExcel->getActiveSheet()->SetCellValue("K1", "Kontak Erat");
        $objPHPExcel->getActiveSheet()->getStyle("A1:K1")->applyFromArray($style_header);

        if ($data_peserta) {
            $row = 2;
            foreach ($data_peserta as $key => $val) {
                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $val->nama);
                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $val->nik);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                    "B" . $row,
                    $val->nik,
                    PHPExcel_Cell_DataType::TYPE_STRING
                );
                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, date("d/m/Y", strtotime($val->tanggal_lahir)));
                $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $val->jenis_kelamin);
                $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $val->pekerjaan_inti);
                $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data);

                $data_master = $this->master_wilayah_model->get_by($val->kelurahan_master_wilayah_id);

                $data_kecamatan = $this->master_wilayah_model->get(
                    array(
                        "fields" => "nama_wilayah,kode_induk",
                        "where" => array(
                            "kode_wilayah" => $data_master->kode_induk
                        )
                    ),
                    "row"
                );

                $data_kabupaten = $this->master_wilayah_model->get(
                    array(
                        "fields" => "nama_wilayah,kode_induk",
                        "where" => array(
                            "kode_wilayah" => $data_kecamatan->kode_induk
                        )
                    ),
                    "row"
                );

                $data_provinsi = $this->master_wilayah_model->get(
                    array(
                        "fields" => "nama_wilayah,kode_induk",
                        "where" => array(
                            "kode_wilayah" => $data_kabupaten->kode_induk
                        )
                    ),
                    "row"
                );

                if ($val->master_rt_domisili_id) {
                    $get_data = $this->master_rt_model->get_by($val->master_rt_domisili_id);
                    $rt_domisili = $get_data->rt;
                } else {
                    $rt_domisili = $val->rt_domisili;
                }

                $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $val->alamat_domisili . (!empty($rt_domisili) ? " RT. " . $rt_domisili : "") . ($data_master->klasifikasi == "KEL" ? " Kelurahan" : " Desa") . " " . $data_master->nama_wilayah);
                $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);

                $objPHPExcel->getActiveSheet()->SetCellValue("G" . $row, $val->nomor_telepon);
                $objPHPExcel->getActiveSheet()->getStyle("G" . $row)->applyFromArray($style_data);

                $objPHPExcel->getActiveSheet()->SetCellValue("H" . $row, ($val->nama_jenis_pemeriksaan_last ? $val->nama_jenis_pemeriksaan_last . " " : ($val->is_sembuh == "1" ? "Sembuh " : ($val->is_meninggal == "1" ? "Meninggal " : ($val->is_probable == "1" ? "Probable " : "")))) . ($val->usulan_faskes == "0" ? "(Usulan Faskes : Swab PCR)" : ($val->usulan_faskes == "1" ? "(Usulan Faskes : Isolasi Mandiri)" : ($val->usulan_faskes == "2" ? "(Usulan Faskes : Bebas Isolasi Mandiri)" : ""))));
                $objPHPExcel->getActiveSheet()->getStyle("H" . $row)->applyFromArray($style_data);

                $objPHPExcel->getActiveSheet()->SetCellValue("I" . $row, ($val->nama_hasil_pemeriksaan_last ? $val->nama_hasil_pemeriksaan_last : ""));
                $objPHPExcel->getActiveSheet()->getStyle("I" . $row)->applyFromArray($style_data);

                $objPHPExcel->getActiveSheet()->SetCellValue("J" . $row, ($val->tanggal_pemeriksaan_where ? date("d/m/Y", strtotime($val->tanggal_pemeriksaan_where)) : ""));
                $objPHPExcel->getActiveSheet()->getStyle("J" . $row)->applyFromArray($style_data);

                $objPHPExcel->getActiveSheet()->SetCellValue("K" . $row, ($val->kontak_erat ? $val->kontak_erat : ""));
                $objPHPExcel->getActiveSheet()->getStyle("K" . $row)->applyFromArray($style_data);

                $row++;
            }
        }

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $writer->save('php://output');
        exit;
    }
}
