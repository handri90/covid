<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("peserta/master_wilayah_model", "master_wilayah_model");
        $this->load->model("peserta/peserta_model", "peserta_model");
        $this->load->model("peserta/peserta_non_puskesmas_model", "peserta_non_puskesmas_model");
        $this->load->model("trx_pemeriksaan_model");
        $this->load->model('hasil_pemeriksaan/hasil_pemeriksaan_model', 'hasil_pemeriksaan_model');
    }

    public function get_laporan_per_kecamatan()
    {
        $table = "";
        if (!in_array($this->session->userdata("level_user_id"), array('6', '2'))) {
            $table = "trx_terkonfirmasi_rilis";
        } else {
            $table = "trx_terkonfirmasi";
        }

        $wh_1 = "";
        $wh_2 = "";
        if ($this->session->userdata("level_user_id") == "2") {
            $wh_1 = "AND puskesmas_id = '" . $this->session->userdata("puskesmas_id") . "'";
            $wh_2 = "OR peserta.`id_user_created` = '" . $this->session->userdata("id_user") . "'";
        }

        $tanggal = date("Y-m-d", strtotime($this->iget("tanggal")));

        $wh = "";
        if ($this->session->userdata("level_user_id") == "2") {
            $wh = "AND peserta.id_user_created='" . $this->session->userdata("id_user") . "'";
        }

        $wilayah = $this->master_wilayah_model->query("
        SELECT nama_wilayah,IFNULL(a.jumlah_positif,0) AS jumlah_positif,IFNULL(b.jumlah_sembuh,0) AS jumlah_sembuh,IFNULL(c.jumlah_meninggal,0) AS jumlah_meninggal
        FROM master_wilayah
        LEFT JOIN (
            SELECT COUNT(*) AS jumlah_positif,kode_induk
            FROM master_wilayah
            INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
            INNER JOIN {$table} ON id_peserta=peserta_id AND {$table}.`deleted_at` IS NULL
            WHERE status = 1 AND DATE_FORMAT({$table}.created_at,'%Y-%m-%d') = '" . $tanggal . "' {$wh_1}
            GROUP BY kode_induk
            )AS a ON a.kode_induk=kode_wilayah
        LEFT JOIN (
            SELECT COUNT(*) AS jumlah_sembuh,kode_induk
            FROM master_wilayah
            INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
            INNER JOIN {$table} ON id_peserta=peserta_id AND {$table}.`deleted_at` IS NULL
            WHERE status = 2 AND DATE_FORMAT({$table}.created_at,'%Y-%m-%d') = '" . $tanggal . "' {$wh_1}
            GROUP BY kode_induk
            )AS b ON b.kode_induk=kode_wilayah
        LEFT JOIN (
            SELECT COUNT(*) AS jumlah_meninggal,kode_induk
            FROM master_wilayah
            INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
            INNER JOIN {$table} ON id_peserta=peserta_id AND {$table}.`deleted_at` IS NULL
            WHERE status = 3 AND DATE_FORMAT({$table}.created_at,'%Y-%m-%d') = '" . $tanggal . "' {$wh_1}
            GROUP BY kode_induk
            )AS c ON c.kode_induk=kode_wilayah
        WHERE klasifikasi = 'KEC'
        ")->result();

        $templist = array();
        foreach ($wilayah as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_laporan_per_pekerjaan()
    {
        if (!in_array($this->session->userdata("level_user_id"), array('6', '2'))) {
            $table = "trx_terkonfirmasi_rilis";
        } else {
            $table = "trx_terkonfirmasi";
        }

        $wh_1 = "";
        $wh_2 = "";
        if ($this->session->userdata("level_user_id") == "2") {
            $wh_1 = "AND puskesmas_id = '" . $this->session->userdata("puskesmas_id") . "'";
            $wh_2 = "OR peserta.`id_user_created` = '" . $this->session->userdata("id_user") . "'";
        }

        $tanggal = date("Y-m-d", strtotime($this->iget("tanggal")));

        $pekerjaan = array((object) array("nama_pekerjaan" => "Wiraswasta"), (object) array("nama_pekerjaan" => "ASN"), (object) array("nama_pekerjaan" => "TNI/POLRI"), (object) array("nama_pekerjaan" => "Swasta"), (object) array("nama_pekerjaan" => "Pelajar/Mahasiswa"), (object) array("nama_pekerjaan" => "Tenaga Kesehatan"), (object) array("nama_pekerjaan" => "Lainnya"));

        $wh = "";
        if ($this->session->userdata("level_user_id") == "2") {
            $wh = "AND peserta.id_user_created='" . $this->session->userdata("id_user") . "'";
        }

        $templist = array();
        foreach ($pekerjaan as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $jumlah_positif = $this->peserta_model->query("
            SELECT IFNULL(COUNT(*),0) AS jumlah_positif FROM (
                SELECT peserta.*
                FROM peserta
                INNER JOIN master_wilayah ON kelurahan_master_wilayah_id=id_master_wilayah
                INNER JOIN {$table} ON id_peserta=peserta_id AND {$table}.`deleted_at` IS NULL
                WHERE status = '1' AND peserta.`deleted_at` IS NULL AND DATE_FORMAT({$table}.created_at,'%Y-%m-%d') = '" . $tanggal . "' {$wh_1} 
            )AS tbl
            WHERE pekerjaan_inti = '" . $row->nama_pekerjaan . "'
            ")->row();

            $jumlah_sembuh = $this->peserta_model->query("
            SELECT IFNULL(COUNT(*),0) AS jumlah_sembuh FROM (
                SELECT peserta.*
                FROM peserta
                INNER JOIN master_wilayah ON kelurahan_master_wilayah_id=id_master_wilayah
                INNER JOIN {$table} ON id_peserta=peserta_id AND {$table}.`deleted_at` IS NULL
                WHERE status = '2' AND peserta.`deleted_at` IS NULL AND DATE_FORMAT({$table}.created_at,'%Y-%m-%d') = '" . $tanggal . "' {$wh_1} 
            )AS tbl
            WHERE pekerjaan_inti = '" . $row->nama_pekerjaan . "'
            ")->row();

            $jumlah_meninggal = $this->peserta_model->query("
            SELECT IFNULL(COUNT(*),0) AS jumlah_meninggal FROM (
                SELECT peserta.*
                FROM peserta
                INNER JOIN master_wilayah ON kelurahan_master_wilayah_id=id_master_wilayah
                INNER JOIN {$table} ON id_peserta=peserta_id AND {$table}.`deleted_at` IS NULL
                WHERE status = '3' AND peserta.`deleted_at` IS NULL AND DATE_FORMAT({$table}.created_at,'%Y-%m-%d') = '" . $tanggal . "' {$wh_1} 
            )AS tbl
            WHERE pekerjaan_inti = '" . $row->nama_pekerjaan . "'
            ")->row();

            $templist[$key]['jumlah_positif'] = $jumlah_positif->jumlah_positif;
            $templist[$key]['jumlah_sembuh'] = $jumlah_sembuh->jumlah_sembuh;
            $templist[$key]['jumlah_meninggal'] = $jumlah_meninggal->jumlah_meninggal;
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_laporan_per_jenis_kelamin()
    {
        if (!in_array($this->session->userdata("level_user_id"), array('6', '2'))) {
            $table = "trx_terkonfirmasi_rilis";
        } else {
            $table = "trx_terkonfirmasi";
        }

        $wh_1 = "";
        $wh_2 = "";
        if ($this->session->userdata("level_user_id") == "2") {
            $wh_1 = "AND puskesmas_id = '" . $this->session->userdata("puskesmas_id") . "'";
            $wh_2 = "OR peserta.`id_user_created` = '" . $this->session->userdata("id_user") . "'";
        }

        $tanggal = date("Y-m-d", strtotime($this->iget("tanggal")));

        $jenis_kelamin = array((object) array("jenis_kelamin" => "L"), (object) array("jenis_kelamin" => "P"));

        $wh = "";
        if ($this->session->userdata("level_user_id") == "2") {
            $wh = "AND peserta.id_user_created='" . $this->session->userdata("id_user") . "'";
        }

        $templist = array();
        foreach ($jenis_kelamin as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $count_peserta = 0;
            $jumlah_positif = $this->peserta_model->query("
            SELECT IFNULL(COUNT(*),0) AS jumlah_positif FROM (
                SELECT peserta.*
                FROM peserta
                INNER JOIN master_wilayah ON kelurahan_master_wilayah_id=id_master_wilayah
                INNER JOIN {$table} ON id_peserta=peserta_id AND {$table}.`deleted_at` IS NULL
                WHERE status = '1' AND peserta.`deleted_at` IS NULL AND DATE_FORMAT({$table}.created_at,'%Y-%m-%d') = '" . $tanggal . "' {$wh_1}
            )AS tbl
            WHERE jenis_kelamin = '" . $row->jenis_kelamin . "'
            ")->row();

            $jumlah_sembuh = $this->peserta_model->query("
            SELECT IFNULL(COUNT(*),0) AS jumlah_sembuh FROM (
                SELECT peserta.*
                FROM peserta
                INNER JOIN master_wilayah ON kelurahan_master_wilayah_id=id_master_wilayah
                INNER JOIN {$table} ON id_peserta=peserta_id AND {$table}.`deleted_at` IS NULL
                WHERE status = '2' AND peserta.`deleted_at` IS NULL AND DATE_FORMAT({$table}.created_at,'%Y-%m-%d') = '" . $tanggal . "' {$wh_1}
            )AS tbl
            WHERE jenis_kelamin = '" . $row->jenis_kelamin . "'
            ")->row();

            $jumlah_meninggal = $this->peserta_model->query("
            SELECT IFNULL(COUNT(*),0) AS jumlah_meninggal FROM (
                SELECT peserta.*
                FROM peserta
                INNER JOIN master_wilayah ON kelurahan_master_wilayah_id=id_master_wilayah
                INNER JOIN {$table} ON id_peserta=peserta_id AND {$table}.`deleted_at` IS NULL
                WHERE status = '3' AND peserta.`deleted_at` IS NULL AND DATE_FORMAT({$table}.created_at,'%Y-%m-%d') = '" . $tanggal . "' {$wh_1}
            )AS tbl
            WHERE jenis_kelamin = '" . $row->jenis_kelamin . "'
            ")->row();

            $templist[$key]['jumlah_positif'] = $jumlah_positif->jumlah_positif;
            $templist[$key]['jumlah_sembuh'] = $jumlah_sembuh->jumlah_sembuh;
            $templist[$key]['jumlah_meninggal'] = $jumlah_meninggal->jumlah_meninggal;
            $templist[$key]['jenis_kelamin_parse'] = ($row->jenis_kelamin == "L" ? "Laki-Laki" : "Perempuan");
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_laporan_puskesmas()
    {
        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");
        $jenis_pemeriksaan = decrypt_data($this->iget("jenis_pemeriksaan"));
        $hasil_pemeriksaan = decrypt_data($this->iget("hasil_pemeriksaan"));

        $wh = "";
        if ($jenis_pemeriksaan > '9000') {
            if ($jenis_pemeriksaan == '9001') {
                $wh = "AND is_sembuh = '1'";
            } else if ($jenis_pemeriksaan == '9002') {
                $wh = "AND is_meninggal = '1'";
            } else if ($jenis_pemeriksaan == '9003') {
                $wh = "AND is_probable = '1'";
            } else if ($jenis_pemeriksaan == '9004') {
                $wh = "AND c.usulan_faskes = '0'";
            } else if ($jenis_pemeriksaan == '9005') {
                $wh = "AND c.usulan_faskes = '1'";
            } else if ($jenis_pemeriksaan == '9006') {
                $wh = "AND c.usulan_faskes = '2'";
            } else if ($jenis_pemeriksaan == '9007') {
                $wh = "AND c.status_rawat = '3'";
            }
        } else if ($jenis_pemeriksaan && $hasil_pemeriksaan) {
            $wh = "AND jenis_pemeriksaan_id = '" . $jenis_pemeriksaan . "' AND hasil_pemeriksaan_id = '" . $hasil_pemeriksaan . "'";
        } else if ($jenis_pemeriksaan && !$hasil_pemeriksaan) {
            $wh = "AND jenis_pemeriksaan_id = '" . $jenis_pemeriksaan . "'";
        }

        if ($this->session->userdata("level_user_id") == "7") {
            $data_peserta = $this->peserta_non_puskesmas_model->query("
            SELECT id_peserta_non_puskesmas AS id_peserta,peserta_non_puskesmas.nama,nik,peserta_non_puskesmas.usulan_faskes AS usulan_faskes_tanpa_pemeriksaan,b.level_user_id,peserta_non_puskesmas.id_user_created,
            GROUP_CONCAT(IFNULL(als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS id_trx_pemeriksaan, 
            GROUP_CONCAT(IFNULL(a.level_user_id,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS level_user_id_pemeriksaan, 
            GROUP_CONCAT(IFNULL(nama_hasil_pemeriksaan,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS nama_hasil_pemeriksaan, 
            GROUP_CONCAT(IFNULL(nama_jenis_pemeriksaan,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS nama_jenis_pemeriksaan, 
            GROUP_CONCAT(IFNULL(is_sembuh,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS is_sembuh, 
            GROUP_CONCAT(IFNULL(is_meninggal,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS is_meninggal, 
            GROUP_CONCAT(IFNULL(is_probable,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS is_probable, 
            GROUP_CONCAT(IFNULL(class_badge,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS class_badge, 
            GROUP_CONCAT(IFNULL(als_trx_pemeriksaan_non_puskesmas.usulan_faskes,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS usulan_pemeriksaan_faskes,
            GROUP_CONCAT(IFNULL(DATE_FORMAT(als_trx_pemeriksaan_non_puskesmas.created_at,'%d-%m-%Y %H:%i:%s'),'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS date_created,
            GROUP_CONCAT(IFNULL(DATE_FORMAT(als_trx_pemeriksaan_non_puskesmas.updated_at,'%d-%m-%Y %H:%i:%s'),'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS date_updated,
            GROUP_CONCAT(IFNULL(a.nama_lengkap,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS nama_user_verification,
            c.*
            FROM peserta_non_puskesmas
            LEFT JOIN trx_pemeriksaan_non_puskesmas AS als_trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas=peserta_non_puskesmas_id AND als_trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
            LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL
            LEFT JOIN jenis_pemeriksaan ON id_jenis_pemeriksaan=als_trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id AND jenis_pemeriksaan.deleted_at IS NULL
            LEFT JOIN user AS a ON als_trx_pemeriksaan_non_puskesmas.id_user_created=a.id_user
            LEFT JOIN user AS b ON peserta_non_puskesmas.id_user_created=b.id_user
            LEFT JOIN 
                (
                    SELECT id_trx_pemeriksaan_non_puskesmas AS id_trx_pemeriksaan_last,peserta_non_puskesmas_id,level_user_id AS level_user_id_last,class_badge AS class_badge_last,usulan_faskes AS usulan_faskes_pemeriksaan,status_rawat
                    FROM trx_pemeriksaan_non_puskesmas 
                    LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                    LEFT JOIN user ON id_user=id_user_created 
                    WHERE id_trx_pemeriksaan_non_puskesmas IN (
                        SELECT MAX(id_trx_pemeriksaan_non_puskesmas) AS id_trx_pemeriksaan_non_puskesmas 
                        FROM trx_pemeriksaan_non_puskesmas 
                        WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                        GROUP BY peserta_non_puskesmas_id
                        ORDER BY id_trx_pemeriksaan_non_puskesmas DESC
                        ) 
                    AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                ) AS c ON c.peserta_non_puskesmas_id=id_peserta_non_puskesmas
            WHERE peserta_non_puskesmas.deleted_at IS NULL AND peserta_non_puskesmas.id_user_created=" . $this->session->userdata("id_user") . "
            GROUP BY id_peserta_non_puskesmas
            ORDER BY id_trx_pemeriksaan_last DESC
            ")->result();
        } else {
            $data_peserta = $this->peserta_model->query("
            SELECT *
            FROM (
            SELECT 
            id_peserta,
            peserta.nama,
            nik,
            b.level_user_id,
            peserta.id_user_created,
            peserta.deleted_at,
            peserta.from_excel,
            c.*
            FROM peserta
            LEFT JOIN user AS b ON peserta.id_user_created=b.id_user
            LEFT JOIN 
                (
                SELECT 
                id_trx_pemeriksaan AS id_trx_pemeriksaan_last,
                peserta_id,
                level_user_id AS level_user_id_last,
                nama_jenis_pemeriksaan AS nama_jenis_pemeriksaan_last,
                nama_hasil_pemeriksaan AS nama_hasil_pemeriksaan_last,
                usulan_faskes,
                DATE_FORMAT(trx_pemeriksaan.created_at,'%d-%m-%Y %H:%i:%s') AS tanggal_pemeriksaan,
                DATE_FORMAT(trx_pemeriksaan.updated_at,'%d-%m-%Y %H:%i:%s') AS tanggal_perubahan_pemeriksaan,
                DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') AS tanggal_pemeriksaan_where,
                hasil_pemeriksaan_id,
                trx_pemeriksaan.jenis_pemeriksaan_id,
                is_sembuh,
                is_meninggal,
                is_probable
                FROM trx_pemeriksaan 
                LEFT JOIN jenis_pemeriksaan ON id_jenis_pemeriksaan=jenis_pemeriksaan_id AND jenis_pemeriksaan.deleted_at IS NULL
                LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                LEFT JOIN user ON id_user=id_user_created 
                WHERE id_trx_pemeriksaan IN (
                    SELECT MAX(id_trx_pemeriksaan) AS id_trx_pemeriksaan 
                    FROM trx_pemeriksaan 
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_pemeriksaan DESC
                    ) 
                AND trx_pemeriksaan.deleted_at IS NULL
                ) AS c ON c.peserta_id=id_peserta
            WHERE (peserta.id_user_created='" . $this->session->userdata("id_user") . "' 
            OR kelurahan_master_wilayah_id IN 
            (
                SELECT id_master_wilayah
                FROM master_wilayah
                WHERE puskesmas_id = '" . $this->session->userdata("puskesmas_id") . "'
            ) AND peserta.deleted_at IS NULL)
            AND c.tanggal_pemeriksaan_where BETWEEN '" . $start_date . "' AND '" . $end_date . "'
            {$wh}
            GROUP BY id_peserta
            ORDER BY id_trx_pemeriksaan_last DESC
            ) AS tbl
            WHERE deleted_at IS NULL
            ORDER BY id_trx_pemeriksaan_last DESC
            ")->result();
        }

        $templist = array();
        foreach ($data_peserta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_hasil_pemeriksaan()
    {
        $id_jenis_pemeriksaan = decrypt_data($this->iget("id_jenis_pemeriksaan"));

        $data_hasil_pemeriksaan = $this->hasil_pemeriksaan_model->get(
            array(
                "where" => array(
                    "jenis_pemeriksaan_id" => $id_jenis_pemeriksaan
                )
            )
        );

        $templist = array();
        foreach ($data_hasil_pemeriksaan as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_hasil_pemeriksaan);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_laporan_dinkes()
    {
        $jenis_pemeriksaan = decrypt_data($this->iget("jenis_pemeriksaan"));
        $hasil_pemeriksaan = decrypt_data($this->iget("hasil_pemeriksaan"));
        $tempat_pemeriksaan = decrypt_data($this->iget("tempat_pemeriksaan"));
        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");
        $sumber_nar = $this->iget("sumber_nar");

        $tabel_trx_pemeriksaan = "";
        if ($this->iget("from_faskes") == "faskes") {
            $tabel_trx_pemeriksaan = "trx_pemeriksaan";
        } else if ($this->iget("from_faskes") == "non-faskes") {
            $tabel_trx_pemeriksaan = "trx_pemeriksaan_non_puskesmas";
        }

        $wh = array();
        $wh_union_jenis_pemeriksaan = "";
        $wh_union_jenis_pemeriksaan_non_puskesmas = "";
        $wh_union_trx_pemeriksaan = "";
        $wh_union_trx_pemeriksaan_non_puskesmas = "";
        if ($jenis_pemeriksaan > '9000') {
            if ($jenis_pemeriksaan == '9001') {
                $wh = array(
                    "is_sembuh" => "1"
                );
                $wh_union_trx_pemeriksaan = "AND is_sembuh = '1'";
                $wh_union_trx_pemeriksaan_non_puskesmas = "AND is_sembuh = '1'";
            } else if ($jenis_pemeriksaan == '9002') {
                $wh = array(
                    "is_meninggal" => "1"
                );
                $wh_union_trx_pemeriksaan = "AND is_meninggal = '1'";
                $wh_union_trx_pemeriksaan_non_puskesmas = "AND is_meninggal = '1'";
            } else if ($jenis_pemeriksaan == '9003') {
                $wh = array(
                    "is_probable" => "1"
                );
                $wh_union_trx_pemeriksaan = "AND is_probable = '1'";
                $wh_union_trx_pemeriksaan_non_puskesmas = "AND is_probable = '1'";
            } else if ($jenis_pemeriksaan == '9004') {
                $wh = array(
                    $tabel_trx_pemeriksaan . ".usulan_faskes" => "0"
                );
                $wh_union_trx_pemeriksaan = "AND trx_pemeriksaan.usulan_faskes = '0'";
                $wh_union_trx_pemeriksaan_non_puskesmas = "AND trx_pemeriksaan_non_puskesmas.usulan_faskes = '0'";
            } else if ($jenis_pemeriksaan == '9005') {
                $wh = array(
                    $tabel_trx_pemeriksaan . "usulan_faskes" => "1"
                );
                $wh_union_trx_pemeriksaan = "AND trx_pemeriksaan.usulan_faskes = '1'";
                $wh_union_trx_pemeriksaan_non_puskesmas = "AND trx_pemeriksaan_non_puskesmas.usulan_faskes = '1'";
            } else if ($jenis_pemeriksaan == '9006') {
                $wh = array(
                    $tabel_trx_pemeriksaan . ".usulan_faskes" => "2"
                );
                $wh_union_trx_pemeriksaan = "AND trx_pemeriksaan.usulan_faskes = '2'";
                $wh_union_trx_pemeriksaan_non_puskesmas = "AND trx_pemeriksaan_non_puskesmas.usulan_faskes = '2'";
            } else if ($jenis_pemeriksaan == '9007') {
                $wh = array(
                    $tabel_trx_pemeriksaan . ".status_rawat" => "3"
                );
                $wh_union_trx_pemeriksaan = "AND trx_pemeriksaan.usulan_faskes = '3'";
                $wh_union_trx_pemeriksaan_non_puskesmas = "AND trx_pemeriksaan_non_puskesmas.usulan_faskes = '3'";
            }
        } else if ($jenis_pemeriksaan && $hasil_pemeriksaan) {
            $wh = array(
                $tabel_trx_pemeriksaan . ".jenis_pemeriksaan_id" => $jenis_pemeriksaan,
                $tabel_trx_pemeriksaan . ".hasil_pemeriksaan_id" => $hasil_pemeriksaan
            );
            $wh_union_jenis_pemeriksaan = "AND trx_pemeriksaan.jenis_pemeriksaan_id = '" . $jenis_pemeriksaan . "' AND trx_pemeriksaan.hasil_pemeriksaan_id = '" . $hasil_pemeriksaan . "'";
            $wh_union_jenis_pemeriksaan_non_puskesmas = "AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '" . $jenis_pemeriksaan . "' AND trx_pemeriksaan_non_puskesmas.hasil_pemeriksaan_id = '" . $hasil_pemeriksaan . "'";
        } else if ($jenis_pemeriksaan && !$hasil_pemeriksaan) {
            $wh = array(
                $tabel_trx_pemeriksaan . ".jenis_pemeriksaan_id" => $jenis_pemeriksaan
            );

            $wh_union_jenis_pemeriksaan = "AND trx_pemeriksaan.jenis_pemeriksaan_id = '" . $jenis_pemeriksaan . "'";
            $wh_union_jenis_pemeriksaan_non_puskesmas = "AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '" . $jenis_pemeriksaan . "'";
        }

        $wh_tempat_pemeriksaan = "";
        if ($tempat_pemeriksaan) {
            $wh_tempat_pemeriksaan = "AND " . $tabel_trx_pemeriksaan . ".id_user_created = '" . $tempat_pemeriksaan . "'";
        }

        if ($this->iget("from_faskes") == "faskes") {
            $peserta = $this->peserta_model->get(
                array(
                    "fields" => "IFNULL(TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d')),0) AS umur_kena_covid,peserta.*,nama_wilayah,IF(trx_pemeriksaan.jenis_pemeriksaan_id IS NULL OR trx_pemeriksaan.jenis_pemeriksaan_id = '',IF(trx_pemeriksaan.usulan_faskes = '0','Usulan Faskes : SWAB PCR',IF(trx_pemeriksaan.usulan_faskes = '1','Usulan Faskes : Isolasi Mandiri',IF(trx_pemeriksaan.usulan_faskes = '2','Bebas Isolasi Mandiri',IF(trx_pemeriksaan.usulan_faskes = '3','Usulan Faskes : Rujuk Rumah Sakit','')))),nama_jenis_pemeriksaan) AS nama_jenis_pemeriksaan,IF(hasil_pemeriksaan_id IS NULL OR hasil_pemeriksaan_id = '',IF(is_sembuh = '1','Sembuh',IF(is_meninggal = '1','Meninggal',IF(is_probable = '1','Probable',''))), nama_hasil_pemeriksaan) AS nama_hasil_pemeriksaan, DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') AS tanggal_pemeriksaan",
                    "join"   => array(
                        "trx_pemeriksaan" => "id_peserta=peserta_id AND trx_pemeriksaan.deleted_at IS NULL",
                        "master_wilayah" => "id_master_wilayah=kelurahan_master_wilayah_id",
                    ),
                    "left_join" => array(
                        "jenis_pemeriksaan" => "jenis_pemeriksaan_id=id_jenis_pemeriksaan",
                        "hasil_pemeriksaan" => "hasil_pemeriksaan_id=id_hasil_pemeriksaan",
                    ),
                    "where" => $wh,
                    "where_false" => "DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "' {$wh_tempat_pemeriksaan}"
                )
            );
        } else if ($this->iget("from_faskes") == "non-faskes") {
            $peserta = $this->peserta_non_puskesmas_model->get(
                array(
                    "fields" => "IFNULL(TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d')),0) AS umur_kena_covid,peserta_non_puskesmas.*,id_peserta_non_puskesmas AS id_peserta,nama_wilayah,IF(trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NULL OR trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '',IF(trx_pemeriksaan_non_puskesmas.usulan_faskes = '0','Usulan Faskes : SWAB PCR',IF(trx_pemeriksaan_non_puskesmas.usulan_faskes = '1','Usulan Faskes : Isolasi Mandiri',IF(trx_pemeriksaan_non_puskesmas.usulan_faskes = '2','Bebas Isolasi Mandiri',IF(trx_pemeriksaan_non_puskesmas.usulan_faskes = '3','Usulan Faskes : Rujuk Rumah Sakit','')))),nama_jenis_pemeriksaan) AS nama_jenis_pemeriksaan,IF(hasil_pemeriksaan_id IS NULL OR hasil_pemeriksaan_id = '',IF(is_sembuh = '1','Sembuh',IF(is_meninggal = '1','Meninggal',IF(is_probable = '1','Probable',''))), nama_hasil_pemeriksaan) AS nama_hasil_pemeriksaan, DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') AS tanggal_pemeriksaan",
                    "join"   => array(
                        "trx_pemeriksaan_non_puskesmas" => "id_peserta_non_puskesmas=peserta_non_puskesmas_id AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL",
                        "master_wilayah" => "id_master_wilayah=kelurahan_master_wilayah_id",
                    ),
                    "left_join" => array(
                        "jenis_pemeriksaan" => "jenis_pemeriksaan_id=id_jenis_pemeriksaan",
                        "hasil_pemeriksaan" => "hasil_pemeriksaan_id=id_hasil_pemeriksaan",
                    ),
                    "where" => $wh,
                    "where_false" => "DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "' {$wh_tempat_pemeriksaan}"
                )
            );
        } else {
            if ($sumber_nar == "true") {
                $peserta = $this->peserta_model->query(
                    "
                    SELECT
                    IFNULL(TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d')),0) AS umur_kena_covid,id_peserta,nama,nik,alamat_ktp,rt_ktp,pekerjaan_inti,nama_wilayah,
                    IF(trx_pemeriksaan.jenis_pemeriksaan_id IS NULL OR trx_pemeriksaan.jenis_pemeriksaan_id = '',IF(trx_pemeriksaan.usulan_faskes = '0','Usulan Faskes : SWAB PCR',IF(trx_pemeriksaan.usulan_faskes = '1','Usulan Faskes : Isolasi Mandiri',IF(trx_pemeriksaan.usulan_faskes = '2','Bebas Isolasi Mandiri',IF(trx_pemeriksaan.usulan_faskes = '3','Usulan Faskes : Rujuk Rumah Sakit','')))),nama_jenis_pemeriksaan) AS nama_jenis_pemeriksaan,
                    IF(hasil_pemeriksaan_id IS NULL OR hasil_pemeriksaan_id = '',IF(is_sembuh = '1','Sembuh',IF(is_meninggal = '1','Meninggal',IF(is_probable = '1','Probable',''))), nama_hasil_pemeriksaan) AS nama_hasil_pemeriksaan, DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') AS tanggal_pemeriksaan
                    FROM master_wilayah
                    INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    LEFT JOIN jenis_pemeriksaan ON trx_pemeriksaan.jenis_pemeriksaan_id=id_jenis_pemeriksaan
                    LEFT JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id=id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.deleted_at IS NULL AND trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '3' AND trx_pemeriksaan.from_excel_import_faskes = '2' AND 
                    (DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') ${wh_tempat_pemeriksaan} ${wh_union_trx_pemeriksaan} ${wh_tempat_pemeriksaan} ${wh_union_jenis_pemeriksaan}
                    GROUP BY peserta_id
                    "
                )->result();
            } else {
                $peserta = $this->peserta_model->query(
                    "
                    SELECT IFNULL(TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d')),0) AS umur_kena_covid,id_peserta,nama,nik,alamat_ktp,rt_ktp,pekerjaan_inti,nama_wilayah,
                    IF(trx_pemeriksaan.jenis_pemeriksaan_id IS NULL OR trx_pemeriksaan.jenis_pemeriksaan_id = '',IF(trx_pemeriksaan.usulan_faskes = '0','Usulan Faskes : SWAB PCR',IF(trx_pemeriksaan.usulan_faskes = '1','Usulan Faskes : Isolasi Mandiri',IF(trx_pemeriksaan.usulan_faskes = '2','Bebas Isolasi Mandiri',IF(trx_pemeriksaan.usulan_faskes = '3','Usulan Faskes : Rujuk Rumah Sakit','')))),nama_jenis_pemeriksaan) AS nama_jenis_pemeriksaan,
                    IF(hasil_pemeriksaan_id IS NULL OR hasil_pemeriksaan_id = '',IF(is_sembuh = '1','Sembuh',IF(is_meninggal = '1','Meninggal',IF(is_probable = '1','Probable',''))), nama_hasil_pemeriksaan) AS nama_hasil_pemeriksaan, DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') AS tanggal_pemeriksaan
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id AND trx_pemeriksaan.deleted_at IS NULL
                    INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
                    LEFT JOIN jenis_pemeriksaan ON jenis_pemeriksaan_id=id_jenis_pemeriksaan
                    LEFT JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id=id_hasil_pemeriksaan
                    WHERE (DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') AND peserta.deleted_at IS NULL ${wh_union_trx_pemeriksaan} ${wh_tempat_pemeriksaan} ${wh_union_jenis_pemeriksaan}
                    UNION 
                    SELECT IFNULL(TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d')),0) AS umur_kena_covid,id_peserta_non_puskesmas AS id_peserta,nama,nik,alamat_ktp,rt_ktp,pekerjaan_inti,nama_wilayah,
                    IF(trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NULL OR trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '',IF(trx_pemeriksaan_non_puskesmas.usulan_faskes = '0','Usulan Faskes : SWAB PCR',IF(trx_pemeriksaan_non_puskesmas.usulan_faskes = '1','Usulan Faskes : Isolasi Mandiri',IF(trx_pemeriksaan_non_puskesmas.usulan_faskes = '2','Bebas Isolasi Mandiri',IF(trx_pemeriksaan_non_puskesmas.usulan_faskes = '3','Usulan Faskes : Rujuk Rumah Sakit','')))),nama_jenis_pemeriksaan) AS nama_jenis_pemeriksaan,
                    IF(hasil_pemeriksaan_id IS NULL OR hasil_pemeriksaan_id = '',IF(is_sembuh = '1','Sembuh',IF(is_meninggal = '1','Meninggal',IF(is_probable = '1','Probable',''))), nama_hasil_pemeriksaan) AS nama_hasil_pemeriksaan, DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') AS tanggal_pemeriksaan
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas=peserta_non_puskesmas_id AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
                    LEFT JOIN jenis_pemeriksaan ON jenis_pemeriksaan_id=id_jenis_pemeriksaan
                    LEFT JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id=id_hasil_pemeriksaan
                    WHERE (DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') AND peserta_non_puskesmas.deleted_at IS NULL ${wh_union_trx_pemeriksaan_non_puskesmas} ${wh_tempat_pemeriksaan} ${wh_union_jenis_pemeriksaan_non_puskesmas}
                    "
                )->result();
            }
        }

        $templist = array();
        $no = 1;
        foreach ($peserta as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['no'] = $no++;
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
            $templist[$key]['tanggal_pemeriksaan_custom'] = date_indo($row->tanggal_pemeriksaan);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
