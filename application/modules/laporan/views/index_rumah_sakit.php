<style>
    .ft-bld {
        font-weight: bold;
    }
</style>

<div class="content">
    <div class="card border-top-success justify-content-md-center">
        <div class="card-body">
            <?php echo form_open(base_url() . "laporan/cetak_laporan", array("target" => "_blank", "method" => "get")); ?>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tanggal Pengambilan: </label><br>
                        <input type="text" class="form-control daterange-single1" readonly name="tanggal_pengambilan">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tanggal Pengujian: </label><br>
                        <input type="text" class="form-control daterange-single2" readonly name="tanggal_pengujian">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Dokter Yang Memeriksa : </label>
                        <select class="form-control select-search" name="dokter">
                            <option value="">-- Pilih Dokter --</option>
                            <?php
                            foreach ($user as $key => $row) {
                            ?>
                                <option value="<?php echo encrypt_data($row->id_user); ?>"><?php echo $row->nama_lengkap; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Cetak <i class="icon-printer2 ml-2"></i></button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <!-- <div class="card card-table table-responsive">
        <table id="datatableLaporan" class="table datatable-save-state">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>NAMA</th>
                    <th>NIK</th>
                    <th>TANGGAL LAHIR</th>
                    <th>JK</th>
                    <th>HP</th>
                    <th>ALAMAT DOMISILI</th>
                    <th>ALAMAT KTP</th>
                    <th>PEKERJAAN</th>
                    <th>WILKER PKM</th>
                    <th>KRITERIA PASIEN</th>
                    <th>SWAB</th>
                    <th>KODE SAMPEL</th>
                </tr>
            </thead>
        </table>
    </div> -->
</div>

<script>
    let datatableLaporan = $("#datatableLaporan").DataTable();

    $('.daterange-single1').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD-MM-YYYY'
        },
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10)
    });

    $('.daterange-single2').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD-MM-YYYY'
        },
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10)
    });

    function get_laporan() {
        let tanggal_pengambilan = $("input[name=tanggal_pengambilan]").val();
        let tanggal_pengujian = $("input[name=tanggal_pengujian]").val();

        datatableLaporan.clear().draw();
        $.ajax({
            url: base_url + 'laporan/request/get_laporan_hasil_pemeriksaan_swab',
            data: {
                tanggal_pengambilan: tanggal_pengambilan,
                tanggal_pengujian: tanggal_pengujian
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                // $.each(response, function(index, value) {
                //     datatableLaporan.row.add([
                //         value.nama,
                //         value.nik,
                //         value.tanggal_pemeriksaan,
                //         "<div>" + (value.nama_jenis_pemeriksaan_last ? value.nama_jenis_pemeriksaan_last : (value.is_sembuh == "1" ? "Sembuh" : (value.is_meninggal == "1" ? "Meninggal" : (value.is_probable == "1" ? "Probable" : "")))) + " " + (value.nama_hasil_pemeriksaan_last ? value.nama_hasil_pemeriksaan_last : "") + "</div><div>" + (value.usulan_faskes == "0" ? "Usulan Faskes : Swab PCR" : (value.usulan_faskes == "1" ? "Usulan Faskes : Isolasi Mandiri" : "")) + "</div>"
                //     ]).draw(false);
                // });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }
</script>