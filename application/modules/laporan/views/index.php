<style>
    .ft-bld {
        font-weight: bold;
    }
</style>

<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tanggal: </label><br>
                        <input type="text" class="form-control daterange-single" readonly name="tanggal" onchange="format_laporan()">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Laporan Berdasarkan : </label>
                        <select class="form-control select-search" name="format_laporan" onchange="format_laporan()">
                            <option value="">-- Pilih Format Laporan --</option>
                            <option value="kecamatan">Kecamatan</option>
                            <option value="pekerjaan">Pekerjaan</option>
                            <option value="jenis_kelamin">Jenis Kelamin</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-table">
    </div>
</div>

<script>
    $(".card-table").hide();
    $(".card-chart").hide();

    function format_laporan() {
        let format_laporan = $("select[name='format_laporan']").val();
        let tanggal = $("input[name='tanggal']").val();

        if (format_laporan) {
            if (format_laporan == "kecamatan") {
                $.ajax({
                    url: base_url + 'laporan/request/get_laporan_per_kecamatan',
                    data: {
                        tanggal: tanggal
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        $(".card-table").show();
                        $(".card-chart").show();
                        let html = "<table class='table'>";
                        html += "<tr><th>Nama Wilayah</th><th class='text-center'>Jumlah Positif</th><th class='text-center'>Jumlah Sembuh</th><th class='text-center'>Jumlah Meninggal</th></tr>";
                        let total_positif = 0;
                        let total_sembuh = 0;
                        let total_meninggal = 0;
                        $.each(response, function(index, value) {
                            html += "<tr>";
                            html += "<td>" + value.nama_wilayah + "</td>";
                            html += "<td class='text-center'>" + value.jumlah_positif + "</td>";
                            html += "<td class='text-center'>" + value.jumlah_sembuh + "</td>";
                            html += "<td class='text-center'>" + value.jumlah_meninggal + "</td>";
                            html += "</tr>";

                            total_positif += parseInt(value.jumlah_positif);
                            total_sembuh += parseInt(value.jumlah_sembuh);
                            total_meninggal += parseInt(value.jumlah_meninggal);
                        });
                        html += "<tr><td class='font-weight-bold text-center'>TOTAL</td><td class='font-weight-bold text-center'>" + total_positif + "</td><td class='font-weight-bold text-center'>" + total_sembuh + "</td><td class='font-weight-bold text-center'>" + total_meninggal + "</td></tr>";
                        html += "</table>";
                        $(".card-table").html(html);
                    },
                    complete: function() {
                        HoldOn.close();
                    }
                });
            } else if (format_laporan == "pekerjaan") {
                $.ajax({
                    url: base_url + 'laporan/request/get_laporan_per_pekerjaan',
                    data: {
                        tanggal: tanggal
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        $(".card-table").show();
                        $(".card-chart").show();
                        let html = "<table class='table'>";
                        html += "<tr><th>Nama Pekerjaan</th><th class='text-center'>Jumlah Positif</th><th class='text-center'>Jumlah Sembuh</th><th class='text-center'>Jumlah Meninggal</th></tr>";
                        let total_positif = 0;
                        let total_sembuh = 0;
                        let total_meninggal = 0;
                        $.each(response, function(index, value) {
                            html += "<tr>";
                            html += "<td>" + value.nama_pekerjaan + "</td>";
                            html += "<td class='text-center'>" + value.jumlah_positif + "</td>";
                            html += "<td class='text-center'>" + value.jumlah_sembuh + "</td>";
                            html += "<td class='text-center'>" + value.jumlah_meninggal + "</td>";
                            html += "</tr>";

                            total_positif += parseInt(value.jumlah_positif);
                            total_sembuh += parseInt(value.jumlah_sembuh);
                            total_meninggal += parseInt(value.jumlah_meninggal);
                        });
                        html += "<tr><td class='font-weight-bold text-center'>TOTAL</td><td class='font-weight-bold text-center'>" + total_positif + "</td><td class='font-weight-bold text-center'>" + total_sembuh + "</td><td class='font-weight-bold text-center'>" + total_meninggal + "</td></tr>";
                        html += "</table>";
                        $(".card-table").html(html);
                    },
                    complete: function() {
                        HoldOn.close();
                    }
                });
            } else if (format_laporan == "jenis_kelamin") {
                $.ajax({
                    url: base_url + 'laporan/request/get_laporan_per_jenis_kelamin',
                    data: {
                        tanggal: tanggal
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        $(".card-table").show();
                        $(".card-chart").show();
                        let html = "<table class='table'>";
                        html += "<tr><th>Jenis Kelamin</th><th class='text-center'>Jumlah Positif</th><th class='text-center'>Jumlah Sembuh</th><th class='text-center'>Jumlah Meninggal</th></tr>";
                        let total_positif = 0;
                        let total_sembuh = 0;
                        let total_meninggal = 0;
                        $.each(response, function(index, value) {
                            html += "<tr>";
                            html += "<td>" + value.jenis_kelamin_parse + "</td>";
                            html += "<td class='text-center'>" + value.jumlah_positif + "</td>";
                            html += "<td class='text-center'>" + value.jumlah_sembuh + "</td>";
                            html += "<td class='text-center'>" + value.jumlah_meninggal + "</td>";
                            html += "</tr>";

                            total_positif += parseInt(value.jumlah_positif);
                            total_sembuh += parseInt(value.jumlah_sembuh);
                            total_meninggal += parseInt(value.jumlah_meninggal);
                        });
                        html += "<tr><td class='font-weight-bold text-center'>TOTAL</td><td class='font-weight-bold text-center'>" + total_positif + "</td><td class='font-weight-bold text-center'>" + total_sembuh + "</td><td class='font-weight-bold text-center'>" + total_meninggal + "</td></tr>";
                        html += "</table>";
                        $(".card-table").html(html);
                    },
                    complete: function() {
                        HoldOn.close();
                    }
                });
            }
        }
    }

    $('.daterange-single').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD-MM-YYYY'
        },
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10)
    });
</script>