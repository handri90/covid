<html>

<head>
    <style>
        body {
            font-family: Georgia, 'Times New Roman', serif;
            font-size: 9pt;
            font-weight: normal;
        }

        .head_print {
            text-align: center;
            font-weight: 100;
            line-height: 0.6;
        }

        .wd-number {
            width: 10px;
        }

        .wd-label {
            width: 100px;
        }

        .wd-label-2 {
            width: 60px;

        }

        .wd-peserta {
            width: 200px;
        }

        .wd-peserta-2 {
            width: 310px;
        }

        .wd-titik {
            width: 2px;
        }

        table {
            margin: 5px 0;
        }

        .tbl-sign {
            margin-top: 20px;
            text-align: center;
        }

        .ruler {
            border-bottom: 2px double black;
        }

        .logo {
            float: left;
            width: 20%;
        }

        .deskripsi {
            float: left;
            width: 60%;
        }

        .alamat {
            line-height: 0.5;
        }

        .text-center {
            text-align: center;
        }

        .border-td {
            border: 1px solid #000;
            text-align: center;
        }
    </style>
</head>

<body>
    <table style="width:100%;margin-top:20px">
        <tr>
            <td colspan="5">
                <table>
                    <tr>
                        <td>Di Cetak Oleh</td>
                        <td>:</td>
                        <td><?php echo isset($user_cetak) ? $user_cetak->nama_lengkap : ""; ?></td>
                    </tr>
                    <tr>
                        <td>Tanggal Cetak</td>
                        <td>:</td>
                        <td><?php echo date_indo(date('Y-m-d')) . " " . date("H:i:s"); ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td rowspan="6" width="200"></td>
            <td rowspan="5" width="70">
                <img src="./assets/template_admin/logo_kobar.png" width="60" />
            </td>
            <td class="text-center">PEMERINTAH KABUPATEN KOTAWARINGIN BARAT</td>
            <td rowspan="5" width="70">
                <img src="./assets/template_admin/logo_rsud.png" width="60" />
            </td>
            <td rowspan="6" width="200"></td>
        </tr>
        <tr>
            <td class="text-center" style="font-weight:bold;font-size:12pt;">RUMAH SAKIT UMUM DAERAH</td>
        </tr>
        <tr>
            <td class="text-center">SULTAN IMANUDDIN PANGKALAN BUN</td>
        </tr>
        <tr>
            <td class="text-center">Akreditasi KARS Nomor : KARS-SERT/623/VII/2020 Tanggal 24 Juli 2020</td>
        </tr>
        <tr>
            <td class="text-center">Jalan Sutan Syahrir 17 Pangkalan Bun - 74112</td>
        </tr>
        <tr>
            <td colspan="3" style="border-top:3px double #000000;">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="5" class="text-center" style="font-weight:bold;">HASIL PEMERIKSAAN SAMPEL SWAB</td>
        </tr>
        <tr>
            <td colspan="5" class="text-center" style="font-weight:bold;">LABORATORIUM BIOLOGI MOLEKULER RSUD SULTAN IMANUDDIN</td>
        </tr>
        <tr>
            <td colspan="5">
                <table width="100%" style="border-collapse:collapse;font-size:9pt;">
                    <tr>
                        <td width="200">TANGGAL</td>
                        <td width="20">:</td>
                        <td><?php echo longdate_indo(date("Y-m-d", strtotime($tanggal_pengujian))); ?></td>
                    </tr>
                    <tr>
                        <td>NAMA PENGIRIM</td>
                        <td>:</td>
                        <td>RSSI SULTAN IMANUDDIN</td>
                    </tr>
                    <tr>
                        <td>ALAMAT</td>
                        <td>:</td>
                        <td>Jl. Sutan Syahrir 17 Pangkalan Bun - 74112</td>
                    </tr>
                    <tr>
                        <td>NO RUMAH SAKIT</td>
                        <td>:</td>
                        <td>017</td>
                    </tr>
                    <tr>
                        <td>TELEPHONE/HP</td>
                        <td>:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>METODE PENGUJIAN</td>
                        <td>:</td>
                        <td>PCR</td>
                    </tr>
                    <tr>
                        <td>JUMLAH SAMPEL</td>
                        <td>:</td>
                        <td><?php echo $jumlah_sample; ?></td>
                    </tr>
                    <tr>
                        <td>TANGGAL PENGAMBILAN</td>
                        <td>:</td>
                        <td><?php echo longdate_indo(date("Y-m-d", strtotime($tanggal_pengambilan))); ?></td>
                    </tr>
                    <tr>
                        <td>TANGGAL PENGUJIAN</td>
                        <td>:</td>
                        <td><?php echo longdate_indo(date("Y-m-d", strtotime($tanggal_pengujian))); ?></td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <table width="100%" style="border-collapse:collapse;">
                                <tr>
                                    <th class="border-td" width="20">NO</th>
                                    <th class="border-td" width="60">NO Lab</th>
                                    <th class="border-td" width="120">NAMA</th>
                                    <th class="border-td" width="120">NIK/RM</th>
                                    <th class="border-td" width="120">TANGGAL LAHIR</th>
                                    <th class="border-td" width="120">JK</th>
                                    <th class="border-td" width="120">ASAL PENGIRIM</th>
                                    <th class="border-td" width="120">PROVINSI PENGIRIM</th>
                                    <th class="border-td" width="120">JENIS SPESIMEN</th>
                                    <th class="border-td" width="120">SWAB</th>
                                    <th class="border-td" width="120">HASIL RT. PCR</th>
                                    <th class="border-td" width="120">KESIMPULAN</th>
                                </tr>
                                <?php
                                if ($peserta) {
                                    $no = 1;
                                    foreach ($peserta as $key => $val) {
                                ?>
                                        <tr>
                                            <td class="border-td"><?php echo $no; ?></td>
                                            <td class="border-td"><?php echo $val->kode_sample; ?></td>
                                            <td class="border-td"><?php echo $val->nama; ?></td>
                                            <td class="border-td"><?php echo $val->nik . ($val->no_rekam_medis ? "/ " . $val->no_rekam_medis : ""); ?></td>
                                            <td class="border-td"><?php echo $val->tanggal_lahir; ?></td>
                                            <td class="border-td"><?php echo $val->jenis_kelamin; ?></td>
                                            <td class="border-td">RSUD Sultan Imanuddin</td>
                                            <td class="border-td">Provinsi Kalimantan Tengah</td>
                                            <td class="border-td"><?php echo ($val->jenis_spesimen == "1" ? "Nasofaring" : ($val->jenis_spesimen == "2" ? "Orofaring" : "")); ?></td>
                                            <td class="border-td"><?php echo $val->jumlah_swab; ?></td>
                                            <td class="border-td"><?php echo $val->nama_hasil_pemeriksaan; ?></td>
                                            <td class="border-td"></td>
                                        </tr>
                                <?php
                                        $no++;
                                    }
                                }
                                ?>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="tbl-sign" width="100%">
        <tr>
            <td>Mengetahui</td>
            <td style="width:300px"></td>
            <td>Pangkalan Bun, <?php echo date_indo(date("Y-m-d", strtotime($tanggal_pengujian))); ?></td>
        </tr>
        <tr>
            <td>Kepala Instalasi Laboratorium</td>
            <td></td>
            <td>Penanggung Jawab</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="padding-top:40px;"><?php echo isset($user_head_lab) ? $user_head_lab->nama_lengkap : ""; ?></td>
            <td></td>
            <td style="padding-top:40px;"><?php echo isset($dokter) ? $dokter->nama_lengkap : ""; ?></td>
        </tr>
        <tr>
            <td><?php echo isset($user_head_lab) ? $user_head_lab->nip : ""; ?></td>
            <td></td>
            <td><?php echo isset($dokter) ? ($dokter->nip ? $dokter->nip : "") : ""; ?></td>
        </tr>
    </table>
</body>

</html>