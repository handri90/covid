<style>
    .ft-bld {
        font-weight: bold;
    }
</style>

<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <?php echo form_open(base_url() . "laporan/cetak_laporan_puskesmas", array("target" => "_blank", "method" => "get")); ?>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tanggal: </label><br>
                        <button type="button" class="btn btn-light daterange-predefined">
                            <i class="icon-calendar22 mr-2"></i>
                            <span></span>
                        </button>

                        <input type="hidden" name="start_date" />
                        <input type="hidden" name="end_date" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Jenis Pemeriksaan : </label>
                        <select class="form-control select-search" name="jenis_pemeriksaan" onchange="get_hasil_pemeriksaan()">
                            <option value="">-- Tampilkan Semua --</option>
                            <?php
                            foreach ($jenis_pemeriksaan as $key => $value) {
                            ?>
                                <option value="<?php echo encrypt_data($value->id_jenis_pemeriksaan); ?>"><?php echo $value->nama_jenis_pemeriksaan; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Hasil Pemeriksaan : </label>
                        <select class="form-control select-search" name="hasil_pemeriksaan" onchange="reload_datatable()">
                            <option value="">-- Tampilkan Semua --</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <div class="text-right">
                            <button type="submit" class="btn btn-success">Cetak</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <div class="card card-table">
        <table id="datatableLaporan" class="table datatable-save-state">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>Nik</th>
                    <th>Tanggal Pemeriksaan</th>
                    <th>Pemeriksaan</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    function reload_datatable() {
        $('#datatableLaporan').DataTable().ajax.reload();
    }

    $('.daterange-predefined').daterangepicker({
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.daterange-predefined span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date]').val(end.format('YYYY-MM-DD'));
            $('#datatableLaporan').DataTable().ajax.reload();
        }
    );

    // Display date format
    $('.daterange-predefined span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date]').val(moment().subtract(29, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date]').val(moment().format('YYYY-MM-DD'));

    get_laporan();

    function get_laporan() {
        $("#datatableLaporan").DataTable({
            ajax: {
                "url": base_url + 'laporan/request/get_laporan_puskesmas',
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "data": function(d) {
                    return $.extend({}, d, {
                        "start_date": $("input[name='start_date']").val(),
                        "end_date": $("input[name='end_date']").val(),
                        "jenis_pemeriksaan": $("select[name='jenis_pemeriksaan']").val(),
                        "hasil_pemeriksaan": $("select[name='hasil_pemeriksaan']").val()
                    });
                },
                "dataSrc": '',
                "complete": function(response) {
                    HoldOn.close();
                }
            },
            "ordering": false,
            "columns": [{
                data: "nama"
            }, {
                data: "nik"
            }, {
                data: "tanggal_pemeriksaan"
            }, {
                "width": "15%",
                "render": function(data, type, full, meta) {
                    return "<div>" + (full.nama_jenis_pemeriksaan_last ? full.nama_jenis_pemeriksaan_last : (full.is_sembuh == "1" ? "Sembuh" : (full.is_meninggal == "1" ? "Meninggal" : (full.is_probable == "1" ? "Probable" : "")))) + " <span class='badge badge-warning'>" + (full.nama_hasil_pemeriksaan_last ? full.nama_hasil_pemeriksaan_last : "") + "</span></div><div>" + (full.usulan_faskes == "0" ? "Usulan Faskes : Swab PCR" : (full.usulan_faskes == "1" ? "Usulan Faskes : Isolasi Mandiri" : (full.usulan_faskes == "2" ? "Usulan Faskes : Bebas Isolasi Mandiri" : ""))) + "</div>";
                }
            }]
        });
    }

    function get_hasil_pemeriksaan() {
        let id_jenis_pemeriksaan = $("select[name='jenis_pemeriksaan']").val();

        let html = "<option value=''>-- Pilih Hasil Pemeriksaan --</option>"
        if (id_jenis_pemeriksaan) {
            $.ajax({
                url: base_url + 'laporan/request/get_hasil_pemeriksaan',
                data: {
                    id_jenis_pemeriksaan: id_jenis_pemeriksaan
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $.each(response, function(index, value) {
                        html += "<option value='" + value.id_encrypt + "'>" + value.nama_hasil_pemeriksaan + "</option>";
                    });
                    $("select[name='hasil_pemeriksaan']").html(html);
                    $('#datatableLaporan').DataTable().ajax.reload();
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        } else {
            $("select[name='hasil_pemeriksaan']").html(html);
            $('#datatableLaporan').DataTable().ajax.reload();
        }

    }
</script>