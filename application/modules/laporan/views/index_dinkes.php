<style>
    .ft-bld {
        font-weight: bold;
    }
</style>

<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tanggal: </label><br>
                        <button type="button" class="btn btn-light daterange-predefined">
                            <i class="icon-calendar22 mr-2"></i>
                            <span></span>
                        </button>

                        <input type="hidden" name="start_date" />
                        <input type="hidden" name="end_date" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Jenis Pemeriksaan : </label>
                        <select class="form-control select-search" name="jenis_pemeriksaan" onchange="get_hasil_pemeriksaan()">
                            <option value="">-- Tampilkan Semua --</option>
                            <?php
                            foreach ($jenis_pemeriksaan as $key => $value) {
                            ?>
                                <option value="<?php echo encrypt_data($value->id_jenis_pemeriksaan); ?>"><?php echo $value->nama_jenis_pemeriksaan; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Hasil Pemeriksaan : </label>
                        <select class="form-control select-search" name="hasil_pemeriksaan" onchange="reload_datatable()">
                            <option value="">-- Tampilkan Semua --</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tempat Pemeriksaan : </label>
                        <select class="form-control select-search" name="tempat_pemeriksaan" onchange="reload_datatable()">
                            <option value="">-- Tampilkan Semua --</option>
                            <?php
                            foreach ($tempat_pemeriksaan as $key => $value) {
                            ?>
                                <option from-faskes="<?php echo ($value->level_user_id == '2' ? 'faskes' : 'non-faskes'); ?>" value="<?php echo encrypt_data($value->id_user); ?>"><?php echo $value->nama_lengkap; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-check form-check-inline form-check-right">
                            <label class="form-check-label">
                                Data NAR
                                <input type="checkbox" class="form-check-input-styled" name="data_nar" onchange="reload_datatable()">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-table table-responsive">
        <table id="datatableLaporan" class="table datatable-save-state">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>NIK</th>
                    <th>Alamat</th>
                    <th>RT</th>
                    <th>Desa / Kelurahan</th>
                    <th>Umur (tahun)</th>
                    <th>Pekerjaan</th>
                    <th>Jenis Pemeriksaan</th>
                    <th>Hasil Pemeriksaan</th>
                    <th>Tanggal Pemeriksaan</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    $('.form-check-input-styled').uniform();

    function reload_datatable() {
        $('#datatableLaporan').DataTable().ajax.reload();
        if ($("input[name='data_nar']").is(":checked")) {
            $("select[name='tempat_pemeriksaan']").attr("disabled", true);
        } else {
            $("select[name='tempat_pemeriksaan']").removeAttr("disabled");
        }
    }

    $('.daterange-predefined').daterangepicker({
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.daterange-predefined span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date]').val(end.format('YYYY-MM-DD'));
            $('#datatableLaporan').DataTable().ajax.reload();
        }
    );

    // Display date format
    $('.daterange-predefined span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date]').val(moment().subtract(29, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date]').val(moment().format('YYYY-MM-DD'));
    get_laporan();

    function get_laporan() {
        let jenis_pemeriksaan = $("select[name='jenis_pemeriksaan']").val();
        let hasil_pemeriksaan = $("select[name='hasil_pemeriksaan']").val();
        let tempat_pemeriksaan = $("select[name='tempat_pemeriksaan']").val();
        let start_date = $("input[name='start_date']").val();
        let end_date = $("input[name='end_date']").val();

        $("#datatableLaporan").DataTable({
            ajax: {
                "url": base_url + 'laporan/request/get_laporan_dinkes',
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "data": function(d) {
                    return $.extend({}, d, {
                        "start_date": $("input[name='start_date']").val(),
                        "end_date": $("input[name='end_date']").val(),
                        "jenis_pemeriksaan": $("select[name='jenis_pemeriksaan']").val(),
                        "hasil_pemeriksaan": $("select[name='hasil_pemeriksaan']").val(),
                        "tempat_pemeriksaan": $("select[name='tempat_pemeriksaan']").val(),
                        "from_faskes": $("select[name='tempat_pemeriksaan'] :selected").attr("from-faskes"),
                        "sumber_nar": $("input[name='data_nar']").is(":checked")
                    });
                },
                "dataSrc": '',
                "complete": function(response) {
                    HoldOn.close();
                }
            },
            "ordering": false,
            "columns": [{
                data: "no"
            }, {
                data: "nama"
            }, {
                data: "nik"
            }, {
                data: "alamat_ktp"
            }, {
                data: "rt_ktp"
            }, {
                data: "nama_wilayah"
            }, {
                data: "umur_kena_covid"
            }, {
                data: "pekerjaan_inti"
            }, {
                data: "nama_jenis_pemeriksaan"
            }, {
                data: "nama_hasil_pemeriksaan"
            }, {
                data: "tanggal_pemeriksaan_custom"
            }]
        });
    }

    function get_hasil_pemeriksaan() {
        let id_jenis_pemeriksaan = $("select[name='jenis_pemeriksaan']").val();

        let html = "<option value=''>-- Pilih Hasil Pemeriksaan --</option>"
        if (id_jenis_pemeriksaan) {
            $.ajax({
                url: base_url + 'laporan/request/get_hasil_pemeriksaan',
                data: {
                    id_jenis_pemeriksaan: id_jenis_pemeriksaan
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $.each(response, function(index, value) {
                        html += "<option value='" + value.id_encrypt + "'>" + value.nama_hasil_pemeriksaan + "</option>";
                    });
                    $("select[name='hasil_pemeriksaan']").html(html);
                    $('#datatableLaporan').DataTable().ajax.reload();
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        } else {
            $("select[name='hasil_pemeriksaan']").html(html);
            $('#datatableLaporan').DataTable().ajax.reload();
        }

    }
</script>