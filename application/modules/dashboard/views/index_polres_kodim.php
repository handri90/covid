<!-- Content area -->
<div class="content">
    <div class="row infografis">
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="chart-container">
                        <div class="chart has-fixed-height" id="area_stacked"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="chart-container">
                        <div class="chart has-fixed-height" id="columns_basic"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Jenis Kelamin</h5>
                </div>
                <div class="card-body">
                    <div class="chart-container">
                        <div class="chart has-fixed-height" id="columns_jenis_kelamin"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Data Covid</h5>
                </div>
                <div class="card-body">
                    <div class="chart-container">
                        <div class="table-responsive info-konfirmasi"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /content area -->

<script>
    $(function() {

        $.ajax({
            url: base_url + 'dashboard/request/get_statistik_area_per_bulan',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                // Define element
                var area_stacked_element = document.getElementById('area_stacked');


                //
                // Charts configuration
                //

                if (area_stacked_element) {

                    // Initialize chart
                    var area_stacked = echarts.init(area_stacked_element);


                    //
                    // Chart config
                    //

                    // Options
                    area_stacked.setOption({

                        // Define colors
                        color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],

                        // Global text styles
                        textStyle: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 13
                        },

                        // Chart animation duration
                        animationDuration: 750,

                        // Setup grid
                        grid: {
                            left: 0,
                            right: 40,
                            top: 35,
                            bottom: 0,
                            containLabel: true
                        },

                        // Add legend
                        legend: {
                            data: ['Positif', 'Sembuh', 'Meninggal'],
                            itemHeight: 8,
                            itemGap: 20
                        },

                        // Add tooltip
                        tooltip: {
                            trigger: 'axis',
                            backgroundColor: 'rgba(0,0,0,0.75)',
                            padding: [10, 15],
                            textStyle: {
                                fontSize: 13,
                                fontFamily: 'Roboto, sans-serif'
                            }
                        },

                        // Horizontal axis
                        xAxis: [{
                            type: 'category',
                            boundaryGap: false,
                            data: JSON.parse(response.bulan),
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                show: true,
                                lineStyle: {
                                    color: '#eee',
                                    type: 'dashed'
                                }
                            }
                        }],

                        // Vertical axis
                        yAxis: [{
                            type: 'value',
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                lineStyle: {
                                    color: '#eee'
                                }
                            },
                            splitArea: {
                                show: true,
                                areaStyle: {
                                    color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                                }
                            }
                        }],

                        // Add series
                        series: [{
                                name: 'Positif',
                                type: 'line',
                                stack: 'Total',
                                areaStyle: {
                                    normal: {
                                        opacity: 0.25
                                    }
                                },
                                smooth: true,
                                symbolSize: 7,
                                itemStyle: {
                                    normal: {
                                        borderWidth: 2
                                    }
                                },
                                data: response.positif
                            },
                            {
                                name: 'Sembuh',
                                type: 'line',
                                stack: 'Total',
                                areaStyle: {
                                    normal: {
                                        opacity: 0.25
                                    }
                                },
                                smooth: true,
                                symbolSize: 7,
                                itemStyle: {
                                    normal: {
                                        borderWidth: 2
                                    }
                                },
                                data: response.sembuh
                            },
                            {
                                name: 'Meninggal',
                                type: 'line',
                                stack: 'Total',
                                areaStyle: {
                                    normal: {
                                        opacity: 0.25
                                    }
                                },
                                smooth: true,
                                symbolSize: 7,
                                itemStyle: {
                                    normal: {
                                        borderWidth: 2
                                    }
                                },
                                data: response.meninggal
                            }
                        ]
                    });
                }
            },
            complete: function() {
                HoldOn.close();
            }
        });

        $.ajax({
            url: base_url + 'dashboard/request/get_statistik_per_bulan',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                // Define element
                var columns_basic_element = document.getElementById('columns_basic');


                //
                // Charts configuration
                //

                if (columns_basic_element) {

                    // Initialize chart
                    var columns_basic = echarts.init(columns_basic_element);


                    //
                    // Chart config
                    //

                    // Options
                    columns_basic.setOption({

                        // Define colors
                        color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],

                        // Global text styles
                        textStyle: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 13
                        },

                        // Chart animation duration
                        animationDuration: 750,

                        // Setup grid
                        grid: {
                            left: 0,
                            right: 40,
                            top: 35,
                            bottom: 0,
                            containLabel: true
                        },

                        // Add legend
                        legend: {
                            data: ['Positif', 'Sembuh', 'Meninggal'],
                            itemHeight: 8,
                            itemGap: 20,
                            textStyle: {
                                padding: [0, 5]
                            }
                        },

                        // Add tooltip
                        tooltip: {
                            trigger: 'axis',
                            backgroundColor: 'rgba(0,0,0,0.75)',
                            padding: [10, 15],
                            textStyle: {
                                fontSize: 13,
                                fontFamily: 'Roboto, sans-serif'
                            }
                        },

                        // Horizontal axis
                        xAxis: [{
                            type: 'category',
                            data: JSON.parse(response.bulan),
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                show: true,
                                lineStyle: {
                                    color: '#eee',
                                    type: 'dashed'
                                }
                            }
                        }],

                        // Vertical axis
                        yAxis: [{
                            type: 'value',
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                lineStyle: {
                                    color: ['#eee']
                                }
                            },
                            splitArea: {
                                show: true,
                                areaStyle: {
                                    color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                                }
                            }
                        }],

                        // Add series
                        series: [{
                                name: 'Positif',
                                type: 'bar',
                                data: response.positif,
                                itemStyle: {
                                    normal: {
                                        label: {
                                            show: true,
                                            position: 'top',
                                            textStyle: {
                                                fontWeight: 500
                                            }
                                        }
                                    }
                                }
                            },
                            {
                                name: 'Sembuh',
                                type: 'bar',
                                data: response.sembuh,
                                itemStyle: {
                                    normal: {
                                        label: {
                                            show: true,
                                            position: 'top',
                                            textStyle: {
                                                fontWeight: 500
                                            }
                                        }
                                    }
                                }
                            },
                            {
                                name: 'Meninggal',
                                type: 'bar',
                                data: response.meninggal,
                                itemStyle: {
                                    normal: {
                                        label: {
                                            show: true,
                                            position: 'top',
                                            textStyle: {
                                                fontWeight: 500
                                            }
                                        }
                                    }
                                }
                            }
                        ]
                    });
                }
            },
            complete: function() {
                HoldOn.close();
            }
        });

        $.ajax({
            url: base_url + 'dashboard/request/get_statistik_per_jenis_kelamin',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                // Define element
                var columns_basic_element = document.getElementById('columns_jenis_kelamin');


                //
                // Charts configuration
                //

                if (columns_basic_element) {

                    // Initialize chart
                    var columns_basic = echarts.init(columns_basic_element);


                    //
                    // Chart config
                    //

                    // Options
                    columns_basic.setOption({

                        // Define colors
                        color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],

                        // Global text styles
                        textStyle: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 13
                        },

                        // Chart animation duration
                        animationDuration: 750,

                        // Setup grid
                        grid: {
                            left: 0,
                            right: 40,
                            top: 35,
                            bottom: 0,
                            containLabel: true
                        },

                        // Add legend
                        legend: {
                            data: ['Positif', 'Sembuh', 'Meninggal'],
                            itemHeight: 8,
                            itemGap: 20,
                            textStyle: {
                                padding: [0, 5]
                            }
                        },

                        // Add tooltip
                        tooltip: {
                            trigger: 'axis',
                            backgroundColor: 'rgba(0,0,0,0.75)',
                            padding: [10, 15],
                            textStyle: {
                                fontSize: 13,
                                fontFamily: 'Roboto, sans-serif'
                            }
                        },

                        // Horizontal axis
                        xAxis: [{
                            type: 'category',
                            data: ['Laki', 'Perempuan'],
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                show: true,
                                lineStyle: {
                                    color: '#eee',
                                    type: 'dashed'
                                }
                            }
                        }],

                        // Vertical axis
                        yAxis: [{
                            type: 'value',
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                lineStyle: {
                                    color: ['#eee']
                                }
                            },
                            splitArea: {
                                show: true,
                                areaStyle: {
                                    color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                                }
                            }
                        }],

                        // Add series
                        series: [{
                                name: 'Positif',
                                type: 'bar',
                                data: response.positif,
                                itemStyle: {
                                    normal: {
                                        label: {
                                            show: true,
                                            position: 'top',
                                            textStyle: {
                                                fontWeight: 500
                                            }
                                        }
                                    }
                                }
                            },
                            {
                                name: 'Sembuh',
                                type: 'bar',
                                data: response.sembuh,
                                itemStyle: {
                                    normal: {
                                        label: {
                                            show: true,
                                            position: 'top',
                                            textStyle: {
                                                fontWeight: 500
                                            }
                                        }
                                    }
                                }
                            },
                            {
                                name: 'Meninggal',
                                type: 'bar',
                                data: response.meninggal,
                                itemStyle: {
                                    normal: {
                                        label: {
                                            show: true,
                                            position: 'top',
                                            textStyle: {
                                                fontWeight: 500
                                            }
                                        }
                                    }
                                }
                            }
                        ]
                    });
                }
            },
            complete: function() {
                HoldOn.close();
            }
        });
    });

    get_konfirmasi_covid();

    function get_konfirmasi_covid() {
        $.ajax({
            url: base_url + 'login/get_konfirmasi_covid',
            type: 'POST',
            success: function(response) {
                let html = "<table class='table'><tr><th>Nama Wilayah</th><th class='text-center'>Total Positif</th><th class='text-center'>Kasus Aktif</th><th class='text-center'>Sembuh</th><th class='text-center'>Meninggal</th></tr>";

                let jumlah_kasus_positif = 0;
                let jumlah_kasus_aktif = 0;
                let jumlah_kasus_sembuh = 0;
                let jumlah_kasus_meninggal = 0;
                $.each(response, function(key, value) {
                    html += "<tr><td>" + value.nama_wilayah + "</td><td class='text-center'><div class='d-flex align-items-center justify-content-center'>" + value.jumlah_kasus_positif + "<div class='ml-2 ft-size'><div>" + (value.jumlah_kasus_aktif_today != "0" ? "(+" + value.jumlah_kasus_aktif_today + ")" : "") + "</div></div></div></td><td class='text-center'><div class='d-flex align-items-center justify-content-center'>" + value.jumlah_kasus_aktif + "<div class='ml-2 ft-size'><div>" + (value.jumlah_kasus_aktif_today != "0" ? "(+" + value.jumlah_kasus_aktif_today + ")" : "") + "</div><span>" + (value.jumlah_kasus_sembuh_today != "0" ? "(-" + value.jumlah_kasus_sembuh_today + ")" : "") + "</span><span>" + (value.jumlah_kasus_meninggal_today != "0" ? "(-" + value.jumlah_kasus_meninggal_today + ")" : "") + "</span></div></div></td><td class='text-center'><div class='d-flex align-items-center justify-content-center'>" + value.jumlah_kasus_sembuh + "<div class='ml-2 ft-size'><div>" + (value.jumlah_kasus_sembuh_today != "0" ? "(+" + value.jumlah_kasus_sembuh_today + ")" : "") + "</div></div></div></td><td class='text-center'><div class='d-flex align-items-center justify-content-center'>" + value.jumlah_kasus_meninggal + "<div class='ml-2 ft-size'><div>" + (value.jumlah_kasus_meninggal_today != "0" ? "(+" + value.jumlah_kasus_meninggal_today + ")" : "") + "</div></div></div></td></tr>";

                    jumlah_kasus_positif += parseInt(value.jumlah_kasus_positif);
                    jumlah_kasus_aktif += parseInt(value.jumlah_kasus_aktif);
                    jumlah_kasus_sembuh += parseInt(value.jumlah_kasus_sembuh);
                    jumlah_kasus_meninggal += parseInt(value.jumlah_kasus_meninggal);
                });
                html += "<tr><td>TOTAL</td><td class='text-center'>" + jumlah_kasus_positif + "</td><td class='text-center'>" + jumlah_kasus_aktif + "</td><td class='text-center'>" + jumlah_kasus_sembuh + "</td><td class='text-center'>" + jumlah_kasus_meninggal + "</td></tr>";
                html += "</table>";

                $(".info-konfirmasi").html(html);
            }
        });
    }

    get_infografis_now();

    function get_infografis_now() {

        $.ajax({
            url: base_url + 'dashboard/request/get_infografis_now',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let html = "";
                if (response) {
                    let number = 1;
                    $.each(response, function(index, value) {
                        html += "<div class='col-sm-6 col-lg-3'>" +
                            "<div class='card'>" +
                            "<div class='card-img-actions m-1'>" +
                            "<img class='card-img img-fluid' src='" + base_url + "assets/infografis_path/" + value.filename + "' alt=''>" +
                            "<div class='card-img-actions-overlay card-img'>" +
                            "<a href='" + base_url + "assets/infografis_path/" + value.filename + "' class='btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round' data-popup='lightbox' rel='group'>" +
                            "<i class='icon-plus3'></i>" +
                            "</a>" +
                            "<a href='" + base_url + "assets/infografis_path/" + value.filename + "' download class='btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round ml-2'>" +
                            "<i class='icon-download'></i>" +
                            "</a>" +
                            "</div>" +
                            "</div>" +
                            "</div>" +
                            "</div>";
                    });

                    $(".infografis").html(html);
                    $('[data-popup="lightbox"]').fancybox({
                        padding: 3
                    });
                }
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }
</script>