<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('peserta/peserta_model', 'peserta_model');
        $this->load->model('ppkm/infografis_model', 'infografis_model');
        $this->load->model('peserta/master_wilayah_model', 'master_wilayah_model');
    }

    public function get_statistik_area_per_bulan()
    {
        $table = "";
        if (!in_array($this->session->userdata("level_user_id"), array('6', '2'))) {
            $table = "trx_terkonfirmasi_rilis";
        } else {
            $table = "trx_terkonfirmasi";
        }

        $data = array();

        $temp_content_positif = array();
        $temp_content_sembuh = array();
        $temp_content_meninggal = array();

        $temp_month = array();

        $wh_1 = "";
        $wh_2 = "";
        if ($this->session->userdata("level_user_id") == "2") {
            $wh_1 = "WHERE puskesmas_id = '" . $this->session->userdata("puskesmas_id") . "'";
            $wh_2 = "OR peserta.`id_user_created` = '" . $this->session->userdata("id_user") . "'";
        }

        $k = 12;
        for ($i = 11; $i >= 0; $i--) {
            $month_year = date("m/Y", strtotime(date("Y-m") . " -" . $i . " month"));
            $month = date("m", strtotime(date("Y-m") . " -" . $i . " month"));

            array_push($temp_month, bulan($month));

            $data_statistik_positif = $this->peserta_model->query("
            SELECT IFNULL(COUNT(*),0) AS jumlah_peserta,status,tbl.created_at
            FROM (
                SELECT DATE_FORMAT({$table}.created_at,'%m/%Y') AS bulan_tahun,status,{$table}.created_at
                FROM peserta
                INNER JOIN {$table} ON id_peserta=peserta_id AND {$table}.deleted_at IS NULL
                WHERE kelurahan_master_wilayah_id IN (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    {$wh_1}
                ) 
                AND peserta.`deleted_at` IS NULL
            ) AS tbl
            WHERE DATE_FORMAT(created_at,'%m/%Y') = '{$month_year}'
            AND status = 1")->row();

            $data_statistik_sembuh = $this->peserta_model->query("
            SELECT IFNULL(COUNT(*),0) AS jumlah_peserta,status,tbl.created_at
            FROM (
                SELECT DATE_FORMAT({$table}.created_at,'%m/%Y') AS bulan_tahun,status,{$table}.created_at
                FROM peserta
                INNER JOIN {$table} ON id_peserta=peserta_id AND {$table}.deleted_at IS NULL
                WHERE kelurahan_master_wilayah_id IN (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    {$wh_1}
                ) 
                AND peserta.`deleted_at` IS NULL
            ) AS tbl
            WHERE DATE_FORMAT(created_at,'%m/%Y') = '{$month_year}'
            AND status = 2")->row();

            $data_statistik_meninggal = $this->peserta_model->query("
            SELECT IFNULL(COUNT(*),0) AS jumlah_peserta,status,tbl.created_at
            FROM (
                SELECT DATE_FORMAT({$table}.created_at,'%m/%Y') AS bulan_tahun,status,{$table}.created_at
                FROM peserta
                INNER JOIN {$table} ON id_peserta=peserta_id AND {$table}.deleted_at IS NULL
                WHERE kelurahan_master_wilayah_id IN (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    {$wh_1}
                ) 
                AND peserta.`deleted_at` IS NULL
            ) AS tbl
            WHERE DATE_FORMAT(created_at,'%m/%Y') = '{$month_year}'
            AND status = 3")->row();

            array_push($temp_content_positif, (int) $data_statistik_positif->jumlah_peserta);
            array_push($temp_content_sembuh, (int) $data_statistik_sembuh->jumlah_peserta);
            array_push($temp_content_meninggal, (int) $data_statistik_meninggal->jumlah_peserta);
        }

        $data = array("bulan" => json_encode($temp_month), "positif" => $temp_content_positif, "sembuh" => $temp_content_sembuh, "meninggal" => $temp_content_meninggal);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_statistik_per_bulan()
    {
        $table = "";
        if (!in_array($this->session->userdata("level_user_id"), array('6', '2'))) {
            $table = "trx_terkonfirmasi_rilis";
        } else {
            $table = "trx_terkonfirmasi";
        }

        $data = array();

        $temp_content_positif = array();
        $temp_content_sembuh = array();
        $temp_content_meninggal = array();

        $temp_month = array();

        $wh_1 = "";
        $wh_2 = "";
        if ($this->session->userdata("level_user_id") == "2") {
            $wh_1 = "WHERE puskesmas_id = '" . $this->session->userdata("puskesmas_id") . "'";
            $wh_2 = "OR peserta.`id_user_created` = '" . $this->session->userdata("id_user") . "'";
        }

        $k = 12;
        for ($i = 11; $i >= 0; $i--) {
            $month_year = date("m/Y", strtotime(date("Y-m") . " -" . $i . " month"));
            $month = date("m", strtotime(date("Y-m") . " -" . $i . " month"));

            array_push($temp_month, bulan($month));

            $data_statistik_positif = $this->peserta_model->query("
            SELECT IFNULL(COUNT(*),0) AS jumlah_peserta,status,tbl.created_at
            FROM (
                SELECT DATE_FORMAT({$table}.created_at,'%m/%Y') AS bulan_tahun,status,{$table}.created_at
                FROM peserta
                INNER JOIN {$table} ON id_peserta=peserta_id AND {$table}.deleted_at IS NULL
                WHERE kelurahan_master_wilayah_id IN (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    {$wh_1}
                ) 
                AND peserta.`deleted_at` IS NULL
            ) AS tbl
            WHERE DATE_FORMAT(created_at,'%m/%Y') = '{$month_year}'
            AND status = 1")->row();

            $data_statistik_sembuh = $this->peserta_model->query("
            SELECT IFNULL(COUNT(*),0) AS jumlah_peserta,status,tbl.created_at
            FROM (
                SELECT DATE_FORMAT({$table}.created_at,'%m/%Y') AS bulan_tahun,status,{$table}.created_at
                FROM peserta
                INNER JOIN {$table} ON id_peserta=peserta_id AND {$table}.deleted_at IS NULL
                WHERE kelurahan_master_wilayah_id IN (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    {$wh_1}
                ) 
                AND peserta.`deleted_at` IS NULL
            ) AS tbl
            WHERE DATE_FORMAT(created_at,'%m/%Y') = '{$month_year}'
            AND status = 2")->row();

            $data_statistik_meninggal = $this->peserta_model->query("
            SELECT IFNULL(COUNT(*),0) AS jumlah_peserta,status,tbl.created_at
            FROM (
                SELECT DATE_FORMAT({$table}.created_at,'%m/%Y') AS bulan_tahun,status,{$table}.created_at
                FROM peserta
                INNER JOIN {$table} ON id_peserta=peserta_id AND {$table}.deleted_at IS NULL
                WHERE kelurahan_master_wilayah_id IN (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    {$wh_1}
                ) 
                AND peserta.`deleted_at` IS NULL
            ) AS tbl
            WHERE DATE_FORMAT(created_at,'%m/%Y') = '{$month_year}'
            AND status = 3")->row();

            array_push($temp_content_positif, (int) $data_statistik_positif->jumlah_peserta);
            array_push($temp_content_sembuh, (int) $data_statistik_sembuh->jumlah_peserta);
            array_push($temp_content_meninggal, (int) $data_statistik_meninggal->jumlah_peserta);
        }

        $data = array("bulan" => json_encode($temp_month), "positif" => $temp_content_positif, "sembuh" => $temp_content_sembuh, "meninggal" => $temp_content_meninggal);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_statistik_per_tahun()
    {
        $table = "";
        if (!in_array($this->session->userdata("level_user_id"), array('6', '2'))) {
            $table = "trx_terkonfirmasi_rilis";
        } else {
            $table = "trx_terkonfirmasi";
        }

        $data = array();

        $temp_content_positif = array();
        $temp_content_sembuh = array();
        $temp_content_meninggal = array();

        $temp_year = array();

        $wh_1 = "";
        $wh_2 = "";
        if ($this->session->userdata("level_user_id") == "2") {
            $wh_1 = "WHERE puskesmas_id = '" . $this->session->userdata("puskesmas_id") . "'";
            $wh_2 = "OR peserta.`id_user_created` = '" . $this->session->userdata("id_user") . "'";
        }

        for ($i = 2020; $i <= date("Y"); $i++) {
            array_push($temp_year, strval($i));

            $data_statistik_positif = $this->peserta_model->query("
            SELECT IFNULL(COUNT(*),0) AS jumlah_peserta,status,tbl.created_at
            FROM (
                SELECT DATE_FORMAT({$table}.created_at,'%m/%Y') AS bulan_tahun,status,{$table}.created_at
                FROM peserta
                INNER JOIN {$table} ON id_peserta=peserta_id AND {$table}.deleted_at IS NULL
                WHERE kelurahan_master_wilayah_id IN (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    {$wh_1}
                ) 
                AND peserta.`deleted_at` IS NULL
            ) AS tbl
            WHERE DATE_FORMAT(created_at,'%Y') = '{$i}'
            AND status = 1")->row();

            $data_statistik_sembuh = $this->peserta_model->query("
            SELECT IFNULL(COUNT(*),0) AS jumlah_peserta,status,tbl.created_at
            FROM (
                SELECT DATE_FORMAT({$table}.created_at,'%m/%Y') AS bulan_tahun,status,{$table}.created_at
                FROM peserta
                INNER JOIN {$table} ON id_peserta=peserta_id AND {$table}.deleted_at IS NULL
                WHERE kelurahan_master_wilayah_id IN (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    {$wh_1}
                ) 
                AND peserta.`deleted_at` IS NULL
            ) AS tbl
            WHERE DATE_FORMAT(created_at,'%Y') = '{$i}'
            AND status = 2")->row();

            $data_statistik_meninggal = $this->peserta_model->query("
            SELECT IFNULL(COUNT(*),0) AS jumlah_peserta,status,tbl.created_at
            FROM (
                SELECT DATE_FORMAT({$table}.created_at,'%m/%Y') AS bulan_tahun,status,{$table}.created_at
                FROM peserta
                INNER JOIN {$table} ON id_peserta=peserta_id AND {$table}.deleted_at IS NULL
                WHERE kelurahan_master_wilayah_id IN (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    {$wh_1}
                ) 
                AND peserta.`deleted_at` IS NULL
            ) AS tbl
            WHERE DATE_FORMAT(created_at,'%Y') = '{$i}'
            AND status = 3")->row();

            array_push($temp_content_positif, (int) $data_statistik_positif->jumlah_peserta);
            array_push($temp_content_sembuh, (int) $data_statistik_sembuh->jumlah_peserta);
            array_push($temp_content_meninggal, (int) $data_statistik_meninggal->jumlah_peserta);
        }

        $data = array("tahun" => json_encode($temp_year), "positif" => $temp_content_positif, "sembuh" => $temp_content_sembuh, "meninggal" => $temp_content_meninggal);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    function get_statistik_per_jenis_kelamin()
    {
        $table = "";
        $field = "";
        if (!in_array($this->session->userdata("level_user_id"), array('6', '2'))) {
            $table = "trx_terkonfirmasi_rilis";
            $field = "trx_terkonfirmasi_id";
        } else {
            $table = "trx_terkonfirmasi";
            $field = "id_trx_terkonfirmasi";
        }

        $temp = array();

        $jenis_kelamin = array("L", "P");

        $temp_positif = array();
        $temp_sembuh = array();
        $temp_meninggal = array();

        $wh_1 = "";
        if ($this->session->userdata("level_user_id") == "2") {
            $wh_1 = "WHERE puskesmas_id = '" . $this->session->userdata("puskesmas_id") . "'";
        }

        foreach ($jenis_kelamin as $row) {
            $data_statistik_positif = $this->peserta_model->query("
            SELECT IFNULL(COUNT(*),0) AS jumlah_peserta
            FROM(
                SELECT status,nama,jenis_kelamin
                FROM peserta
                LEFT JOIN 
                (
                    SELECT *
                    FROM {$table}
                    WHERE {$table}.deleted_at IS NULL
                ) AS a ON a.peserta_id=id_peserta
                WHERE kelurahan_master_wilayah_id IN (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    {$wh_1}
                )
                AND peserta.`deleted_at` IS NULL
            ) AS tbl
            WHERE jenis_kelamin = '" . $row . "' AND status = '1'
            ")->row();

            $data_statistik_sembuh = $this->peserta_model->query("
            SELECT IFNULL(COUNT(*),0) AS jumlah_peserta
            FROM(
                SELECT status,nama,jenis_kelamin
                FROM peserta
                LEFT JOIN 
                (
                    SELECT *
                    FROM {$table}
                    WHERE {$table}.deleted_at IS NULL
                ) AS a ON a.peserta_id=id_peserta
                WHERE kelurahan_master_wilayah_id IN (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    {$wh_1}
                )
                AND peserta.`deleted_at` IS NULL
            ) AS tbl
            WHERE jenis_kelamin = '" . $row . "' AND status = '2'
            ")->row();

            $data_statistik_meninggal = $this->peserta_model->query("
            SELECT IFNULL(COUNT(*),0) AS jumlah_peserta
            FROM(
                SELECT status,nama,jenis_kelamin
                FROM peserta
                LEFT JOIN 
                (
                    SELECT *
                    FROM {$table}
                    WHERE {$table}.deleted_at IS NULL
                ) AS a ON a.peserta_id=id_peserta
                WHERE kelurahan_master_wilayah_id IN (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    {$wh_1}
                )
                AND peserta.`deleted_at` IS NULL
            ) AS tbl
            WHERE jenis_kelamin = '" . $row . "' AND status = '3'
            ")->row();

            array_push($temp_positif, (int) $data_statistik_positif->jumlah_peserta);
            array_push($temp_sembuh, (int) $data_statistik_sembuh->jumlah_peserta);
            array_push($temp_meninggal, (int) $data_statistik_meninggal->jumlah_peserta);
        }

        $data = array("positif" => $temp_positif, "sembuh" => $temp_sembuh, "meninggal" => $temp_meninggal);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_jumlah_pemeriksaan_non_puskesmas()
    {
        $data_statistik = $this->peserta_model->query(
            "SELECT 
            id_user,
            nama_lengkap,
            IFNULL(a1.jumlah_swab_pcr_positif,0) AS jumlah_swab_pcr_positif,IFNULL(a2.jumlah_swab_pcr_negatif,0) AS jumlah_swab_pcr_negatif,IFNULL(a3.jumlah_swab_pcr_incon,0) AS jumlah_swab_pcr_incon,IFNULL(a4.jumlah_swab_pcr_invalid,0) AS jumlah_swab_pcr_invalid,IFNULL(a.jumlah_swab_pcr,0) AS jumlah_swab_pcr,
            IFNULL(b1.jumlah_rapid_antigen_positif,0) AS jumlah_rapid_antigen_positif,IFNULL(b2.jumlah_rapid_antigen_negatif,0) AS jumlah_rapid_antigen_negatif,IFNULL(b.jumlah_rapid_antigen,0) AS jumlah_rapid_antigen,
            IFNULL(c1.jumlah_rapid_antibody_reaktif_igg,0) AS jumlah_rapid_antibody_reaktif_igg,IFNULL(c2.jumlah_rapid_antibody_reaktif_igm,0) AS jumlah_rapid_antibody_reaktif_igm,IFNULL(c3.jumlah_rapid_antibody_reaktif_igg_igm,0) AS jumlah_rapid_antibody_reaktif_igg_igm,IFNULL(c4.jumlah_rapid_antibody_non_reaktif,0) AS jumlah_rapid_antibody_non_reaktif,IFNULL(c.jumlah_rapid_antibody,0) AS jumlah_rapid_antibody
            FROM user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_swab_pcr,trx_pemeriksaan_non_puskesmas.id_user_created
                FROM trx_pemeriksaan_non_puskesmas
                INNER JOIN peserta_non_puskesmas ON id_peserta_non_puskesmas=peserta_non_puskesmas_id
                WHERE jenis_pemeriksaan_id = '1' AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL AND peserta_non_puskesmas.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS a ON a.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_swab_pcr_positif,trx_pemeriksaan_non_puskesmas.id_user_created
                FROM trx_pemeriksaan_non_puskesmas
                INNER JOIN peserta_non_puskesmas ON id_peserta_non_puskesmas=peserta_non_puskesmas_id
                WHERE jenis_pemeriksaan_id = '1' AND hasil_pemeriksaan_id = '1' AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL AND peserta_non_puskesmas.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS a1 ON a1.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_swab_pcr_negatif,trx_pemeriksaan_non_puskesmas.id_user_created
                FROM trx_pemeriksaan_non_puskesmas
                INNER JOIN peserta_non_puskesmas ON id_peserta_non_puskesmas=peserta_non_puskesmas_id
                WHERE jenis_pemeriksaan_id = '1' AND hasil_pemeriksaan_id = '2' AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL AND peserta_non_puskesmas.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS a2 ON a2.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_swab_pcr_incon,trx_pemeriksaan_non_puskesmas.id_user_created
                FROM trx_pemeriksaan_non_puskesmas
                INNER JOIN peserta_non_puskesmas ON id_peserta_non_puskesmas=peserta_non_puskesmas_id
                WHERE jenis_pemeriksaan_id = '1' AND hasil_pemeriksaan_id = '7' AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL AND peserta_non_puskesmas.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS a3 ON a3.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_swab_pcr_invalid,trx_pemeriksaan_non_puskesmas.id_user_created
                FROM trx_pemeriksaan_non_puskesmas
                INNER JOIN peserta_non_puskesmas ON id_peserta_non_puskesmas=peserta_non_puskesmas_id
                WHERE jenis_pemeriksaan_id = '1' AND hasil_pemeriksaan_id = '11' AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL AND peserta_non_puskesmas.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS a4 ON a4.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_rapid_antigen,trx_pemeriksaan_non_puskesmas.id_user_created
                FROM trx_pemeriksaan_non_puskesmas
                INNER JOIN peserta_non_puskesmas ON id_peserta_non_puskesmas=peserta_non_puskesmas_id
                WHERE jenis_pemeriksaan_id = '2' AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL AND peserta_non_puskesmas.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS b ON b.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_rapid_antigen_positif,trx_pemeriksaan_non_puskesmas.id_user_created
                FROM trx_pemeriksaan_non_puskesmas
                INNER JOIN peserta_non_puskesmas ON id_peserta_non_puskesmas=peserta_non_puskesmas_id
                WHERE jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '3' AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL AND peserta_non_puskesmas.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS b1 ON b1.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_rapid_antigen_negatif,trx_pemeriksaan_non_puskesmas.id_user_created
                FROM trx_pemeriksaan_non_puskesmas
                INNER JOIN peserta_non_puskesmas ON id_peserta_non_puskesmas=peserta_non_puskesmas_id
                WHERE jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '4' AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL AND peserta_non_puskesmas.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS b2 ON b2.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_rapid_antibody,trx_pemeriksaan_non_puskesmas.id_user_created
                FROM trx_pemeriksaan_non_puskesmas
                INNER JOIN peserta_non_puskesmas ON id_peserta_non_puskesmas=peserta_non_puskesmas_id
                WHERE jenis_pemeriksaan_id = '3' AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL AND peserta_non_puskesmas.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS c ON c.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_rapid_antibody_reaktif_igg,trx_pemeriksaan_non_puskesmas.id_user_created
                FROM trx_pemeriksaan_non_puskesmas
                INNER JOIN peserta_non_puskesmas ON id_peserta_non_puskesmas=peserta_non_puskesmas_id
                WHERE jenis_pemeriksaan_id = '3' AND hasil_pemeriksaan_id = '5' AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL AND peserta_non_puskesmas.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS c1 ON c1.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_rapid_antibody_reaktif_igm,trx_pemeriksaan_non_puskesmas.id_user_created
                FROM trx_pemeriksaan_non_puskesmas
                INNER JOIN peserta_non_puskesmas ON id_peserta_non_puskesmas=peserta_non_puskesmas_id
                WHERE jenis_pemeriksaan_id = '3' AND hasil_pemeriksaan_id = '9' AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL AND peserta_non_puskesmas.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS c2 ON c2.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_rapid_antibody_reaktif_igg_igm,trx_pemeriksaan_non_puskesmas.id_user_created
                FROM trx_pemeriksaan_non_puskesmas
                INNER JOIN peserta_non_puskesmas ON id_peserta_non_puskesmas=peserta_non_puskesmas_id
                WHERE jenis_pemeriksaan_id = '3' AND hasil_pemeriksaan_id = '10' AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL AND peserta_non_puskesmas.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS c3 ON c3.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_rapid_antibody_non_reaktif,trx_pemeriksaan_non_puskesmas.id_user_created
                FROM trx_pemeriksaan_non_puskesmas
                INNER JOIN peserta_non_puskesmas ON id_peserta_non_puskesmas=peserta_non_puskesmas_id
                WHERE jenis_pemeriksaan_id = '3' AND hasil_pemeriksaan_id = '6' AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL AND peserta_non_puskesmas.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS c4 ON c4.id_user_created=id_user
            WHERE level_user_id IN ('7') AND user.deleted_at IS NULL
            UNION
            SELECT 
            id_user,
            nama_lengkap,
            IFNULL(a1.jumlah_swab_pcr_positif,0) AS jumlah_swab_pcr_positif,IFNULL(a2.jumlah_swab_pcr_negatif,0) AS jumlah_swab_pcr_negatif,IFNULL(a3.jumlah_swab_pcr_incon,0) AS jumlah_swab_pcr_incon,IFNULL(a4.jumlah_swab_pcr_invalid,0) AS jumlah_swab_pcr_invalid,IFNULL(a.jumlah_swab_pcr,0) AS jumlah_swab_pcr,
            IFNULL(b1.jumlah_rapid_antigen_positif,0) AS jumlah_rapid_antigen_positif,IFNULL(b2.jumlah_rapid_antigen_negatif,0) AS jumlah_rapid_antigen_negatif,IFNULL(b.jumlah_rapid_antigen,0) AS jumlah_rapid_antigen,
            IFNULL(c1.jumlah_rapid_antibody_reaktif_igg,0) AS jumlah_rapid_antibody_reaktif_igg,IFNULL(c2.jumlah_rapid_antibody_reaktif_igm,0) AS jumlah_rapid_antibody_reaktif_igm,IFNULL(c3.jumlah_rapid_antibody_reaktif_igg_igm,0) AS jumlah_rapid_antibody_reaktif_igg_igm,IFNULL(c4.jumlah_rapid_antibody_non_reaktif,0) AS jumlah_rapid_antibody_non_reaktif,IFNULL(c.jumlah_rapid_antibody,0) AS jumlah_rapid_antibody
            FROM user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_swab_pcr,trx_pemeriksaan.id_user_created
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id
                WHERE jenis_pemeriksaan_id = '1' AND trx_pemeriksaan.deleted_at IS NULL AND peserta.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS a ON a.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_swab_pcr_positif,trx_pemeriksaan.id_user_created
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id
                WHERE jenis_pemeriksaan_id = '1' AND hasil_pemeriksaan_id = '1' AND trx_pemeriksaan.deleted_at IS NULL AND peserta.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS a1 ON a1.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_swab_pcr_negatif,trx_pemeriksaan.id_user_created
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id
                WHERE jenis_pemeriksaan_id = '1' AND hasil_pemeriksaan_id = '2' AND trx_pemeriksaan.deleted_at IS NULL AND peserta.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS a2 ON a2.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_swab_pcr_incon,trx_pemeriksaan.id_user_created
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id
                WHERE jenis_pemeriksaan_id = '1' AND hasil_pemeriksaan_id = '7' AND trx_pemeriksaan.deleted_at IS NULL AND peserta.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS a3 ON a3.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_swab_pcr_invalid,trx_pemeriksaan.id_user_created
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id
                WHERE jenis_pemeriksaan_id = '1' AND hasil_pemeriksaan_id = '11' AND trx_pemeriksaan.deleted_at IS NULL AND peserta.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS a4 ON a4.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_rapid_antigen,trx_pemeriksaan.id_user_created
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id
                WHERE jenis_pemeriksaan_id = '2' AND trx_pemeriksaan.deleted_at IS NULL AND peserta.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS b ON b.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_rapid_antigen_positif,trx_pemeriksaan.id_user_created
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id
                WHERE jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '3' AND trx_pemeriksaan.deleted_at IS NULL AND peserta.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS b1 ON b1.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_rapid_antigen_negatif,trx_pemeriksaan.id_user_created
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id
                WHERE jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '4' AND trx_pemeriksaan.deleted_at IS NULL AND peserta.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS b2 ON b2.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_rapid_antibody,trx_pemeriksaan.id_user_created
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id
                WHERE jenis_pemeriksaan_id = '3' AND trx_pemeriksaan.deleted_at IS NULL AND peserta.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS c ON c.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_rapid_antibody_reaktif_igg,trx_pemeriksaan.id_user_created
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id
                WHERE jenis_pemeriksaan_id = '3' AND hasil_pemeriksaan_id = '5' AND trx_pemeriksaan.deleted_at IS NULL AND peserta.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS c1 ON c1.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_rapid_antibody_reaktif_igm,trx_pemeriksaan.id_user_created
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id
                WHERE jenis_pemeriksaan_id = '3' AND hasil_pemeriksaan_id = '9' AND trx_pemeriksaan.deleted_at IS NULL AND peserta.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS c2 ON c2.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_rapid_antibody_reaktif_igg_igm,trx_pemeriksaan.id_user_created
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id
                WHERE jenis_pemeriksaan_id = '3' AND hasil_pemeriksaan_id = '10' AND trx_pemeriksaan.deleted_at IS NULL AND peserta.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS c3 ON c3.id_user_created=id_user
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_rapid_antibody_non_reaktif,trx_pemeriksaan.id_user_created
                FROM trx_pemeriksaan
                INNER JOIN peserta ON id_peserta=peserta_id
                WHERE jenis_pemeriksaan_id = '3' AND hasil_pemeriksaan_id = '6' AND trx_pemeriksaan.deleted_at IS NULL AND peserta.deleted_at IS NULL
                GROUP BY id_user_created
            ) AS c4 ON c4.id_user_created=id_user
            WHERE level_user_id IN ('2','3') AND user.deleted_at IS NULL"
        )->result();

        $templist = array();
        foreach ($data_statistik as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }


    public function jumlah_peserta_nik_empty()
    {
        $jumlah_peserta_nik_empty = $this->peserta_model->query(
            "SELECT COUNT(*) AS jumlah_peserta_nik_empty
            FROM peserta
            WHERE 
            (
                id_user_created = '{$this->session->userdata("id_user")}'
                OR kelurahan_master_wilayah_id IN 
                (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    WHERE puskesmas_id = '{$this->session->userdata("puskesmas_id")}'
                ) 
            )
            AND 
            (
                nik IS NULL OR nik = ''
            )
            AND peserta.deleted_at IS NULL"
        )->row();

        $data = $jumlah_peserta_nik_empty;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function jumlah_peserta_rt_domisili_empty()
    {
        $jumlah_peserta_rt_domisili_empty = $this->peserta_model->query(
            "SELECT COUNT(*) AS jumlah_peserta_rt_domisili_empty
            FROM peserta
            WHERE 
            (
                id_user_created = '{$this->session->userdata("id_user")}'
                OR kelurahan_master_wilayah_id IN 
                (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    WHERE puskesmas_id = '{$this->session->userdata("puskesmas_id")}'
                ) 
            )
            AND 
            (
                rt_domisili IS NULL OR rt_domisili = '' OR rt_domisili = '0'
            )
            AND peserta.deleted_at IS NULL"
        )->row();

        $data = $jumlah_peserta_rt_domisili_empty;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }


    public function get_infografis_now()
    {
        $infografis = $this->infografis_model->query(
            "
            SELECT filename, DATE_FORMAT(created_at,'%Y-%m-%d') AS tanggal
            FROM infografis
            WHERE tipe_upload = '1' AND DATE_FORMAT(created_at,'%Y-%m-%d') = CURDATE() AND deleted_at IS NULL
            UNION
            SELECT filename, DATE_FORMAT(created_at,'%Y-%m-%d') AS tanggal
            FROM infografis
            WHERE tipe_upload = '2' AND deleted_at IS NULL AND (WEEK(CURDATE()) = WEEK(DATE_FORMAT(created_at,'%Y-%m-%d')))
            "
        )->result();

        $templist = array();
        foreach ($infografis as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }


    public function get_pie_sebaran_kecamatan()
    {
        $data_sebaran = $this->master_wilayah_model->query(
            "
            SELECT nama_wilayah,IFNULL(d.jumlah_kasus_aktif,0) AS jumlah_kasus_aktif
            FROM master_wilayah
            LEFT JOIN (
                SELECT COUNT(id_peserta) AS jumlah_kasus_aktif,kode_induk
                FROM master_wilayah
                INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
                WHERE id_peserta IN (
                SELECT peserta_id
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
                )
                GROUP BY kode_induk
            ) AS d ON d.kode_induk=kode_wilayah
            WHERE klasifikasi = 'KEC'
            "
        )->result();

        $name_arr_kec = array();

        $templist = array();
        foreach ($data_sebaran as $key => $row) {
            array_push($name_arr_kec, $row->nama_wilayah);

            $templist[$key]['name'] = $row->nama_wilayah;
            $templist[$key]['value'] = (int) $row->jumlah_kasus_aktif;
        }

        $data = array("nama_kecamatan" => $name_arr_kec, "data" => $templist);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_pie_sebaran_umur()
    {
        $data_sebaran = $this->peserta_model->query(
            "
            SELECT COUNT(*) AS jumlah_peserta_aktif, '0 - 5 Tahun' AS rentang_umur
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(tanggal_rilis,'%Y-%m-%d')) BETWEEN '0' AND '5'
            UNION
            SELECT COUNT(*) AS jumlah_peserta_aktif, '6 - 11 Tahun' AS rentang_umur
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(tanggal_rilis,'%Y-%m-%d')) BETWEEN '6' AND '11'
            UNION
            SELECT COUNT(*) AS jumlah_peserta_aktif, '12 - 25 Tahun' AS rentang_umur
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(tanggal_rilis,'%Y-%m-%d')) BETWEEN '12' AND '25'
            UNION
            SELECT COUNT(*) AS jumlah_peserta_aktif, '26 - 45 Tahun' AS rentang_umur
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(tanggal_rilis,'%Y-%m-%d')) BETWEEN '26' AND '45'
            UNION
            SELECT COUNT(*) AS jumlah_peserta_aktif, '6 - 11 Tahun' AS rentang_umur
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(tanggal_rilis,'%Y-%m-%d')) BETWEEN '6' AND '11'
            UNION
            SELECT COUNT(*) AS jumlah_peserta_aktif, '46 - 65 Tahun' AS rentang_umur
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(tanggal_rilis,'%Y-%m-%d')) BETWEEN '46' AND '65'
            UNION
            SELECT COUNT(*) AS jumlah_peserta_aktif, '66 - 200 Tahun' AS rentang_umur
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE TIMESTAMPDIFF(YEAR, tanggal_lahir, DATE_FORMAT(tanggal_rilis,'%Y-%m-%d')) BETWEEN '66' AND '200'
            "
        )->result();

        $name_arr_umur = array();

        $templist = array();
        foreach ($data_sebaran as $key => $row) {
            array_push($name_arr_umur, $row->rentang_umur);

            $templist[$key]['name'] = $row->rentang_umur;
            $templist[$key]['value'] = (int) $row->jumlah_peserta_aktif;
        }

        $data = array("rentang_umur" => $name_arr_umur, "data" => $templist);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_pie_sebaran_pekerjaan()
    {
        $data_sebaran = $this->peserta_model->query(
            "
            SELECT COUNT(*) AS jumlah_peserta_aktif, 'Polisi' AS pekerjaan
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE pekerjaan_inti = 'Polisi'
            UNION
            SELECT COUNT(*) AS jumlah_peserta_aktif, 'TNI' AS pekerjaan
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE pekerjaan_inti = 'TNI'
            UNION
            SELECT COUNT(*) AS jumlah_peserta_aktif, 'TNI/POLRI' AS pekerjaan
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE pekerjaan_inti = 'TNI/POLRI'
            UNION
            SELECT COUNT(*) AS jumlah_peserta_aktif, 'ASN' AS pekerjaan
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE pekerjaan_inti = 'ASN'
            UNION
            SELECT COUNT(*) AS jumlah_peserta_aktif, 'BUMN / BANK' AS pekerjaan
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE pekerjaan_inti = 'BUMN / BANK'
            UNION
            SELECT COUNT(*) AS jumlah_peserta_aktif, 'Tenaga Kesehatan' AS pekerjaan
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE pekerjaan_inti = 'Tenaga Kesehatan'
            UNION
            SELECT COUNT(*) AS jumlah_peserta_aktif, 'Dosen / Guru' AS pekerjaan
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE pekerjaan_inti = 'Dosen / Guru'
            UNION
            SELECT COUNT(*) AS jumlah_peserta_aktif, 'Pelajar/Mahasiswa' AS pekerjaan
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE pekerjaan_inti = 'Pelajar/Mahasiswa'
            UNION
            SELECT COUNT(*) AS jumlah_peserta_aktif, 'Ibu Rumah Tangga' AS pekerjaan
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE pekerjaan_inti = 'Ibu Rumah Tangga'
            UNION
            SELECT COUNT(*) AS jumlah_peserta_aktif, 'Ulama / Pendeta /  Rohaniawan' AS pekerjaan
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE pekerjaan_inti = 'Ulama / Pendeta /  Rohaniawan'
            UNION
            SELECT COUNT(*) AS jumlah_peserta_aktif, 'Swasta' AS pekerjaan
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE pekerjaan_inti = 'Swasta'
            UNION
            SELECT COUNT(*) AS jumlah_peserta_aktif, 'Wiraswasta' AS pekerjaan
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE pekerjaan_inti = 'Wiraswasta'
            UNION
            SELECT COUNT(*) AS jumlah_peserta_aktif, 'Umum / Keluarga / Pensiunan / Unidentifi' AS pekerjaan
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE pekerjaan_inti = 'Umum / Keluarga / Pensiunan / Unidentifi'
            UNION
            SELECT COUNT(*) AS jumlah_peserta_aktif, 'Lainnya' AS pekerjaan
            FROM(
                SELECT peserta_id,trx_terkonfirmasi_rilis.created_at AS tanggal_rilis
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis)
                    FROM trx_terkonfirmasi_rilis
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                )
                AND STATUS = '1'
                AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            ) AS tbl
            INNER JOIN peserta ON id_peserta=peserta_id AND peserta.deleted_at IS NULL
            WHERE pekerjaan_inti = 'Lainnya'
            "
        )->result();

        $name_arr_pekerjaan = array();

        $templist = array();
        foreach ($data_sebaran as $key => $row) {
            array_push($name_arr_pekerjaan, $row->pekerjaan);

            $templist[$key]['name'] = $row->pekerjaan;
            $templist[$key]['value'] = (int) $row->jumlah_peserta_aktif;
        }

        $data = array("pekerjaan" => $name_arr_pekerjaan, "data" => $templist);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_statistik_antigen_pcr()
    {
        $data_antigen_pcr = $this->peserta_model->query(
            "
            SELECT *
            FROM
            (
                SELECT tbl_pcr.jumlah_peserta AS jumlah_peserta_pcr,tbl_antigen.jumlah_peserta AS jumlah_peserta_antigen,tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr,tbl_pcr_positif.jumlah_peserta AS jumlah_kasus_terbaru_pcr_positif,tbl_pcr_antigen.jumlah_peserta AS jumlah_kasus_terbaru_antigen_positif,IFNULL(FORMAT(((tbl_pcr_positif.jumlah_peserta/tbl_pcr.jumlah_peserta)*100),1),0) AS positivity_persentase_pcr,IFNULL(FORMAT(((tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr/(tbl_antigen.jumlah_peserta+tbl_pcr.jumlah_peserta))*100),1),0) AS positivity_persentase_total,DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL -13 DAY),'%d/%m') AS tanggal_pemeriksaan
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -13 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -13 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -13 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -13 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_antigen,
                (
                    SELECT (tbl_pcr.jumlah_peserta + tbl_antigen.jumlah_peserta) AS jumlah_peserta_kasus_terbaru_antigen_pcr
                    FROM
                    (
                        SELECT COUNT(*) AS jumlah_peserta
                        FROM
                        (
                            SELECT id_peserta
                            FROM peserta
                            INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                            INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                            WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -13 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                            UNION ALL
                            SELECT id_peserta_non_puskesmas
                            FROM peserta_non_puskesmas
                            INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                            INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                            WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -13 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                        ) AS tbl_pcr
                    ) AS tbl_pcr,
                    (
                        SELECT COUNT(*) AS jumlah_peserta
                        FROM
                        (
                            SELECT id_peserta
                            FROM peserta
                            INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                            INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                            WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -13 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                            UNION ALL
                            SELECT id_peserta_non_puskesmas
                            FROM peserta_non_puskesmas
                            INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                            INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                            WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -13 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                        ) AS tbl_antigen
                    ) AS tbl_antigen
                ) AS tbl_kasus_terbaru_antigen_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -13 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -13 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr_positif,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -13 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -13 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_pcr_antigen
            ) AS tbl_all
            UNION ALL
            SELECT *
            FROM
            (
                SELECT tbl_pcr.jumlah_peserta AS jumlah_peserta_pcr,tbl_antigen.jumlah_peserta AS jumlah_peserta_antigen,tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr,tbl_pcr_positif.jumlah_peserta AS jumlah_kasus_terbaru_pcr_positif,tbl_pcr_antigen.jumlah_peserta AS jumlah_kasus_terbaru_antigen_positif,IFNULL(FORMAT(((tbl_pcr_positif.jumlah_peserta/tbl_pcr.jumlah_peserta)*100),1),0) AS positivity_persentase_pcr,IFNULL(FORMAT(((tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr/(tbl_antigen.jumlah_peserta+tbl_pcr.jumlah_peserta))*100),1),0) AS positivity_persentase_total,DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL -12 DAY),'%d/%m') AS tanggal_pemeriksaan
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -12 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -12 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -12 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -12 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_antigen,
                (
                    SELECT (tbl_pcr.jumlah_peserta + tbl_antigen.jumlah_peserta) AS jumlah_peserta_kasus_terbaru_antigen_pcr
                    FROM
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -12 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -12 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                    ) AS tbl_pcr,
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -12 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -12 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                    ) AS tbl_antigen
                ) AS tbl_kasus_terbaru_antigen_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -12 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -12 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr_positif,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -12 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -12 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_pcr_antigen
            ) AS tbl_all
            UNION ALL
            SELECT *
            FROM
            (
                SELECT tbl_pcr.jumlah_peserta AS jumlah_peserta_pcr,tbl_antigen.jumlah_peserta AS jumlah_peserta_antigen,tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr,tbl_pcr_positif.jumlah_peserta AS jumlah_kasus_terbaru_pcr_positif,tbl_pcr_antigen.jumlah_peserta AS jumlah_kasus_terbaru_antigen_positif,IFNULL(FORMAT(((tbl_pcr_positif.jumlah_peserta/tbl_pcr.jumlah_peserta)*100),1),0) AS positivity_persentase_pcr,IFNULL(FORMAT(((tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr/(tbl_antigen.jumlah_peserta+tbl_pcr.jumlah_peserta))*100),1),0) AS positivity_persentase_total,DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL -11 DAY),'%d/%m') AS tanggal_pemeriksaan
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -11 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -11 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -11 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -11 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_antigen,
                (
                    SELECT (tbl_pcr.jumlah_peserta + tbl_antigen.jumlah_peserta) AS jumlah_peserta_kasus_terbaru_antigen_pcr
                    FROM
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -11 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -11 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                    ) AS tbl_pcr,
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -11 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -11 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                    ) AS tbl_antigen
                ) AS tbl_kasus_terbaru_antigen_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -11 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -11 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr_positif,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -11 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -11 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_pcr_antigen
            ) AS tbl_all
            UNION ALL
            SELECT *
            FROM
            (
                SELECT tbl_pcr.jumlah_peserta AS jumlah_peserta_pcr,tbl_antigen.jumlah_peserta AS jumlah_peserta_antigen,tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr,tbl_pcr_positif.jumlah_peserta AS jumlah_kasus_terbaru_pcr_positif,tbl_pcr_antigen.jumlah_peserta AS jumlah_kasus_terbaru_antigen_positif,IFNULL(FORMAT(((tbl_pcr_positif.jumlah_peserta/tbl_pcr.jumlah_peserta)*100),1),0) AS positivity_persentase_pcr,IFNULL(FORMAT(((tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr/(tbl_antigen.jumlah_peserta+tbl_pcr.jumlah_peserta))*100),1),0) AS positivity_persentase_total,DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL -10 DAY),'%d/%m') AS tanggal_pemeriksaan
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -10 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -10 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -10 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -10 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_antigen,
                (
                    SELECT (tbl_pcr.jumlah_peserta + tbl_antigen.jumlah_peserta) AS jumlah_peserta_kasus_terbaru_antigen_pcr
                    FROM
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -10 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -10 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                    ) AS tbl_pcr,
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -10 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -10 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                    ) AS tbl_antigen
                ) AS tbl_kasus_terbaru_antigen_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -10 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -10 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr_positif,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -10 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -10 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_pcr_antigen
            ) AS tbl_all
            UNION ALL
            SELECT *
            FROM
            (
                SELECT tbl_pcr.jumlah_peserta AS jumlah_peserta_pcr,tbl_antigen.jumlah_peserta AS jumlah_peserta_antigen,tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr,tbl_pcr_positif.jumlah_peserta AS jumlah_kasus_terbaru_pcr_positif,tbl_pcr_antigen.jumlah_peserta AS jumlah_kasus_terbaru_antigen_positif,IFNULL(FORMAT(((tbl_pcr_positif.jumlah_peserta/tbl_pcr.jumlah_peserta)*100),1),0) AS positivity_persentase_pcr,IFNULL(FORMAT(((tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr/(tbl_antigen.jumlah_peserta+tbl_pcr.jumlah_peserta))*100),1),0) AS positivity_persentase_total,DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL -9 DAY),'%d/%m') AS tanggal_pemeriksaan
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -9 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -9 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -9 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -9 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_antigen,
                (
                    SELECT (tbl_pcr.jumlah_peserta + tbl_antigen.jumlah_peserta) AS jumlah_peserta_kasus_terbaru_antigen_pcr
                    FROM
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -9 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -9 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                    ) AS tbl_pcr,
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -9 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -9 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                    ) AS tbl_antigen
                ) AS tbl_kasus_terbaru_antigen_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -9 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -9 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr_positif,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -9 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -9 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_pcr_antigen
            ) AS tbl_all
            UNION ALL
            SELECT *
            FROM
            (
                SELECT tbl_pcr.jumlah_peserta AS jumlah_peserta_pcr,tbl_antigen.jumlah_peserta AS jumlah_peserta_antigen,tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr,tbl_pcr_positif.jumlah_peserta AS jumlah_kasus_terbaru_pcr_positif,tbl_pcr_antigen.jumlah_peserta AS jumlah_kasus_terbaru_antigen_positif,IFNULL(FORMAT(((tbl_pcr_positif.jumlah_peserta/tbl_pcr.jumlah_peserta)*100),1),0) AS positivity_persentase_pcr,IFNULL(FORMAT(((tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr/(tbl_antigen.jumlah_peserta+tbl_pcr.jumlah_peserta))*100),1),0) AS positivity_persentase_total,DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL -8 DAY),'%d/%m') AS tanggal_pemeriksaan
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -8 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -8 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -8 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -8 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_antigen,
                (
                    SELECT (tbl_pcr.jumlah_peserta + tbl_antigen.jumlah_peserta) AS jumlah_peserta_kasus_terbaru_antigen_pcr
                    FROM
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -8 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -8 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                    ) AS tbl_pcr,
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -8 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -8 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                    ) AS tbl_antigen
                ) AS tbl_kasus_terbaru_antigen_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -8 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -8 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr_positif,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -8 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -8 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_pcr_antigen
            ) AS tbl_all
            UNION ALL
            SELECT *
            FROM
            (
                SELECT tbl_pcr.jumlah_peserta AS jumlah_peserta_pcr,tbl_antigen.jumlah_peserta AS jumlah_peserta_antigen,tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr,tbl_pcr_positif.jumlah_peserta AS jumlah_kasus_terbaru_pcr_positif,tbl_pcr_antigen.jumlah_peserta AS jumlah_kasus_terbaru_antigen_positif,IFNULL(FORMAT(((tbl_pcr_positif.jumlah_peserta/tbl_pcr.jumlah_peserta)*100),1),0) AS positivity_persentase_pcr,IFNULL(FORMAT(((tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr/(tbl_antigen.jumlah_peserta+tbl_pcr.jumlah_peserta))*100),1),0) AS positivity_persentase_total,DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL -7 DAY),'%d/%m') AS tanggal_pemeriksaan
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_antigen,
                (
                    SELECT (tbl_pcr.jumlah_peserta + tbl_antigen.jumlah_peserta) AS jumlah_peserta_kasus_terbaru_antigen_pcr
                    FROM
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                    ) AS tbl_pcr,
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                    ) AS tbl_antigen
                ) AS tbl_kasus_terbaru_antigen_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr_positif,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_pcr_antigen
            ) AS tbl_all
            UNION ALL
            SELECT *
            FROM
            (
                SELECT tbl_pcr.jumlah_peserta AS jumlah_peserta_pcr,tbl_antigen.jumlah_peserta AS jumlah_peserta_antigen,tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr,tbl_pcr_positif.jumlah_peserta AS jumlah_kasus_terbaru_pcr_positif,tbl_pcr_antigen.jumlah_peserta AS jumlah_kasus_terbaru_antigen_positif,IFNULL(FORMAT(((tbl_pcr_positif.jumlah_peserta/tbl_pcr.jumlah_peserta)*100),1),0) AS positivity_persentase_pcr,IFNULL(FORMAT(((tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr/(tbl_antigen.jumlah_peserta+tbl_pcr.jumlah_peserta))*100),1),0) AS positivity_persentase_total,DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL -6 DAY),'%d/%m') AS tanggal_pemeriksaan
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -6 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -6 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -6 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -6 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_antigen,
                (
                    SELECT (tbl_pcr.jumlah_peserta + tbl_antigen.jumlah_peserta) AS jumlah_peserta_kasus_terbaru_antigen_pcr
                    FROM
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -6 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -6 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                    ) AS tbl_pcr,
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -6 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -6 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                    ) AS tbl_antigen
                ) AS tbl_kasus_terbaru_antigen_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -6 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -6 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr_positif,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -6 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -6 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_pcr_antigen
            ) AS tbl_all
            UNION ALL
            SELECT *
            FROM
            (
                SELECT tbl_pcr.jumlah_peserta AS jumlah_peserta_pcr,tbl_antigen.jumlah_peserta AS jumlah_peserta_antigen,tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr,tbl_pcr_positif.jumlah_peserta AS jumlah_kasus_terbaru_pcr_positif,tbl_pcr_antigen.jumlah_peserta AS jumlah_kasus_terbaru_antigen_positif,IFNULL(FORMAT(((tbl_pcr_positif.jumlah_peserta/tbl_pcr.jumlah_peserta)*100),1),0) AS positivity_persentase_pcr,IFNULL(FORMAT(((tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr/(tbl_antigen.jumlah_peserta+tbl_pcr.jumlah_peserta))*100),1),0) AS positivity_persentase_total,DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL -5 DAY),'%d/%m') AS tanggal_pemeriksaan
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -5 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -5 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -5 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -5 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_antigen,
                (
                    SELECT (tbl_pcr.jumlah_peserta + tbl_antigen.jumlah_peserta) AS jumlah_peserta_kasus_terbaru_antigen_pcr
                    FROM
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -5 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -5 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                    ) AS tbl_pcr,
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -5 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -5 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                    ) AS tbl_antigen
                ) AS tbl_kasus_terbaru_antigen_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -5 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -5 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr_positif,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -5 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -5 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_pcr_antigen
            ) AS tbl_all
            UNION ALL
            SELECT *
            FROM
            (
                SELECT tbl_pcr.jumlah_peserta AS jumlah_peserta_pcr,tbl_antigen.jumlah_peserta AS jumlah_peserta_antigen,tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr,tbl_pcr_positif.jumlah_peserta AS jumlah_kasus_terbaru_pcr_positif,tbl_pcr_antigen.jumlah_peserta AS jumlah_kasus_terbaru_antigen_positif,IFNULL(FORMAT(((tbl_pcr_positif.jumlah_peserta/tbl_pcr.jumlah_peserta)*100),1),0) AS positivity_persentase_pcr,IFNULL(FORMAT(((tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr/(tbl_antigen.jumlah_peserta+tbl_pcr.jumlah_peserta))*100),1),0) AS positivity_persentase_total,DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL -4 DAY),'%d/%m') AS tanggal_pemeriksaan
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -4 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -4 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -4 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -4 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_antigen,
                (
                    SELECT (tbl_pcr.jumlah_peserta + tbl_antigen.jumlah_peserta) AS jumlah_peserta_kasus_terbaru_antigen_pcr
                    FROM
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -4 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -4 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                    ) AS tbl_pcr,
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -4 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -4 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                    ) AS tbl_antigen
                ) AS tbl_kasus_terbaru_antigen_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -4 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -4 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr_positif,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -4 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -4 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_pcr_antigen
            ) AS tbl_all
            UNION ALL
            SELECT *
            FROM
            (
                SELECT tbl_pcr.jumlah_peserta AS jumlah_peserta_pcr,tbl_antigen.jumlah_peserta AS jumlah_peserta_antigen,tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr,tbl_pcr_positif.jumlah_peserta AS jumlah_kasus_terbaru_pcr_positif,tbl_pcr_antigen.jumlah_peserta AS jumlah_kasus_terbaru_antigen_positif,IFNULL(FORMAT(((tbl_pcr_positif.jumlah_peserta/tbl_pcr.jumlah_peserta)*100),1),0) AS positivity_persentase_pcr,IFNULL(FORMAT(((tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr/(tbl_antigen.jumlah_peserta+tbl_pcr.jumlah_peserta))*100),1),0) AS positivity_persentase_total,DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL -3 DAY),'%d/%m') AS tanggal_pemeriksaan
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -3 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -3 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -3 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -3 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_antigen,
                (
                    SELECT (tbl_pcr.jumlah_peserta + tbl_antigen.jumlah_peserta) AS jumlah_peserta_kasus_terbaru_antigen_pcr
                    FROM
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -3 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -3 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                    ) AS tbl_pcr,
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -3 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -3 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                    ) AS tbl_antigen
                ) AS tbl_kasus_terbaru_antigen_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -3 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -3 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr_positif,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -3 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -3 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_pcr_antigen
            ) AS tbl_all
            UNION ALL
            SELECT *
            FROM
            (
                SELECT tbl_pcr.jumlah_peserta AS jumlah_peserta_pcr,tbl_antigen.jumlah_peserta AS jumlah_peserta_antigen,tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr,tbl_pcr_positif.jumlah_peserta AS jumlah_kasus_terbaru_pcr_positif,tbl_pcr_antigen.jumlah_peserta AS jumlah_kasus_terbaru_antigen_positif,IFNULL(FORMAT(((tbl_pcr_positif.jumlah_peserta/tbl_pcr.jumlah_peserta)*100),1),0) AS positivity_persentase_pcr,IFNULL(FORMAT(((tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr/(tbl_antigen.jumlah_peserta+tbl_pcr.jumlah_peserta))*100),1),0) AS positivity_persentase_total,DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL -2 DAY),'%d/%m') AS tanggal_pemeriksaan
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -2 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -2 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -2 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -2 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_antigen,
                (
                    SELECT (tbl_pcr.jumlah_peserta + tbl_antigen.jumlah_peserta) AS jumlah_peserta_kasus_terbaru_antigen_pcr
                    FROM
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -2 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -2 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                    ) AS tbl_pcr,
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -2 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -2 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                    ) AS tbl_antigen
                ) AS tbl_kasus_terbaru_antigen_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -2 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -2 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr_positif,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -2 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -2 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_pcr_antigen
            ) AS tbl_all
            UNION ALL
            SELECT *
            FROM
            (
                SELECT tbl_pcr.jumlah_peserta AS jumlah_peserta_pcr,tbl_antigen.jumlah_peserta AS jumlah_peserta_antigen,tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr,tbl_pcr_positif.jumlah_peserta AS jumlah_kasus_terbaru_pcr_positif,tbl_pcr_antigen.jumlah_peserta AS jumlah_kasus_terbaru_antigen_positif,IFNULL(FORMAT(((tbl_pcr_positif.jumlah_peserta/tbl_pcr.jumlah_peserta)*100),1),0) AS positivity_persentase_pcr,IFNULL(FORMAT(((tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr/(tbl_antigen.jumlah_peserta+tbl_pcr.jumlah_peserta))*100),1),0) AS positivity_persentase_total,DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL -1 DAY),'%d/%m') AS tanggal_pemeriksaan
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_antigen,
                (
                    SELECT (tbl_pcr.jumlah_peserta + tbl_antigen.jumlah_peserta) AS jumlah_peserta_kasus_terbaru_antigen_pcr
                    FROM
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                    ) AS tbl_pcr,
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                    ) AS tbl_antigen
                ) AS tbl_kasus_terbaru_antigen_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr_positif,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_pcr_antigen
            ) AS tbl_all
            UNION ALL
            SELECT *
            FROM
            (
                SELECT tbl_pcr.jumlah_peserta AS jumlah_peserta_pcr,tbl_antigen.jumlah_peserta AS jumlah_peserta_antigen,tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr,tbl_pcr_positif.jumlah_peserta AS jumlah_kasus_terbaru_pcr_positif,tbl_pcr_antigen.jumlah_peserta AS jumlah_kasus_terbaru_antigen_positif,IFNULL(FORMAT(((tbl_pcr_positif.jumlah_peserta/tbl_pcr.jumlah_peserta)*100),1),0) AS positivity_persentase_pcr,IFNULL(FORMAT(((tbl_kasus_terbaru_antigen_pcr.jumlah_peserta_kasus_terbaru_antigen_pcr/(tbl_antigen.jumlah_peserta+tbl_pcr.jumlah_peserta))*100),1),0) AS positivity_persentase_total,DATE_FORMAT(CURDATE(),'%d/%m') AS tanggal_pemeriksaan
                FROM
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = CURDATE() AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '1' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = CURDATE() AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    WHERE jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = CURDATE() AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    WHERE peserta_id IS NULL AND jenis_pemeriksaan_id = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = CURDATE() AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_antigen,
                (
                    SELECT (tbl_pcr.jumlah_peserta + tbl_antigen.jumlah_peserta) AS jumlah_peserta_kasus_terbaru_antigen_pcr
                    FROM
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = CURDATE() AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = CURDATE() AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                    ) AS tbl_pcr,
                    (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                        SELECT id_peserta
                        FROM peserta
                        INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = CURDATE() AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                        UNION ALL
                        SELECT id_peserta_non_puskesmas
                        FROM peserta_non_puskesmas
                        INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                        INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                        WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = CURDATE() AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                    ) AS tbl_antigen
                ) AS tbl_kasus_terbaru_antigen_pcr,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = CURDATE() AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = CURDATE() AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_pcr
                ) AS tbl_pcr_positif,
                (
                    SELECT COUNT(*) AS jumlah_peserta
                    FROM
                    (
                    SELECT id_peserta
                    FROM peserta
                    INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE trx_pemeriksaan.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') = CURDATE() AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
                    UNION ALL
                    SELECT id_peserta_non_puskesmas
                    FROM peserta_non_puskesmas
                    INNER JOIN trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas =peserta_non_puskesmas_id
                    INNER JOIN hasil_pemeriksaan ON hasil_pemeriksaan_id = id_hasil_pemeriksaan
                    WHERE peserta_id IS NULL AND trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' AND class_badge = '2' AND DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') = CURDATE() AND peserta_non_puskesmas.deleted_at IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS tbl_antigen
                ) AS tbl_pcr_antigen
            ) AS tbl_all
            "
        )->result();

        $arr_antigen = array();
        $arr_pcr = array();
        $kasus_terbaru_pcr_positif = array();
        $kasus_terbaru_antigen_positif = array();
        $arr_tanggal_pemeriksaan = array();
        $html_tr_1 = "<tr>";
        $html_tr_2 = "<tr>";
        $html_tr_3 = "<tr>";
        foreach ($data_antigen_pcr as $key => $row) {
            array_push($arr_antigen, (int) $row->jumlah_peserta_antigen);
            array_push($arr_pcr, (int) $row->jumlah_peserta_pcr);
            array_push($kasus_terbaru_pcr_positif, (int) $row->jumlah_kasus_terbaru_pcr_positif);
            array_push($kasus_terbaru_antigen_positif, (int) $row->jumlah_kasus_terbaru_antigen_positif);
            array_push($arr_tanggal_pemeriksaan, $row->tanggal_pemeriksaan);
            $html_tr_1 .= "<th>" . $row->tanggal_pemeriksaan . "</th>";
            $html_tr_2 .= "<td><span class='badge badge-primary'>" . $row->positivity_persentase_pcr . "%</span></td>";
            $html_tr_3 .= "<td><span class='badge badge-secondary'>" . $row->positivity_persentase_total . "%</span></td>";
        }
        $html_tr_1 .= "</tr>";
        $html_tr_2 .= "</tr>";
        $html_tr_3 .= "</tr>";

        $html_table = "<table class='table'>" . $html_tr_1 . $html_tr_2 . $html_tr_3 . "</table>";


        $data = array("pcr" => $arr_pcr, "antigen" => $arr_antigen, "kasus_terbaru_pcr_positif" => $kasus_terbaru_pcr_positif, "kasus_terbaru_antigen_positif" => $kasus_terbaru_antigen_positif, "tanggal_pemeriksaan" => $arr_tanggal_pemeriksaan, "html_table" => $html_table);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_statistik_pcr_kel_desa()
    {
        $data_pcr_kel_desa = $this->peserta_model->query(
            "
            SELECT nama_wilayah,COUNT(id_peserta) AS jumlah_peserta
            FROM 
            (
                SELECT id_peserta,kelurahan_master_wilayah_id
                FROM peserta
                WHERE
                id_peserta IN 
                (
                    SELECT peserta_id
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN 
                    (
                        SELECT MAX(id_trx_terkonfirmasi_rilis)
                        FROM trx_terkonfirmasi_rilis
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                    )
                    AND STATUS = '1'
                    AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at,'%Y-%m-%d') = CURDATE()
                    AND trx_terkonfirmasi_rilis.deleted_at IS NULL
                )
                AND peserta.`deleted_at` IS NULL
            ) AS tbl
            RIGHT JOIN master_wilayah ON kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND id_master_wilayah NOT IN ('109')
            GROUP BY id_master_wilayah
            HAVING jumlah_peserta > 0
            ORDER BY jumlah_peserta ASC
            "
        )->result();

        $nama_wilayah = array();
        $jumlah_peserta = array();
        foreach ($data_pcr_kel_desa as $key => $row) {
            array_push($nama_wilayah, $row->nama_wilayah);
            array_push($jumlah_peserta, (int) $row->jumlah_peserta);
        }

        $data = array("nama_wilayah" => $nama_wilayah, "jumlah_peserta" => $jumlah_peserta);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
