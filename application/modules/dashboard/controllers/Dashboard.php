<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $data['breadcrumb'] = [];
        if ($this->session->userdata("level_user_id") == '7') {
            $this->execute('index_non_puskes', $data);
        } else if ($this->session->userdata("level_user_id") == '6') {
            $this->execute('index_dinkes', $data);
        } else if ($this->session->userdata("level_user_id") == '2') {
            $this->execute('index_puskesmas', $data);
        } else if ($this->session->userdata("level_user_id") == '11') {
            $this->execute('index_polres_kodim', $data);
        } else {
            $this->execute('index', $data);
        }
    }
}
