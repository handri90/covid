<style>
    .info-column {
        margin: 4px 0;
    }
</style>

<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Tanggal</label>
                <div class="col-lg-10">
                    <button type="button" class="btn btn-light daterange-predefined">
                        <i class="icon-calendar22 mr-2"></i>
                        <span></span>
                    </button>

                    <input type="hidden" name="start_date" />
                    <input type="hidden" name="end_date" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Jenis Pemeriksaan</label>
                <div class="col-lg-4">
                    <select class="form-control select-search" name="jenis_pemeriksaan" onchange="get_hasil_pemeriksaan()">
                        <option value="">-- SEMUA --</option>
                        <?php
                        foreach ($jenis_pemeriksaan as $key => $value) {
                        ?>
                            <option value="<?php echo encrypt_data($value->id_jenis_pemeriksaan); ?>"><?php echo $value->nama_jenis_pemeriksaan; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Hasil Pemeriksaan</label>
                <div class="col-lg-4">
                    <select class="form-control select-search" name="hasil_pemeriksaan" onchange="reload_datatable()">
                        <option value="">-- SEMUA --</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <!-- <a href="<?php echo base_url() . 'peserta/detail_peserta_non_puskesmas_ch'; ?>" class="btn btn-info">List Peserta Citra Husada</a> -->
                <a href="<?php echo base_url() . 'peserta/tambah_peserta'; ?>" class="btn btn-info">Tambah Peserta Rapid</a>
            </div>
        </div>
        <table id="datatablePesertaRapid" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>NIK</th>
                    <th>Pemeriksaan Faskes</th>
                    <th>Pemeriksaan Labkesda</th>
                    <th>Pemeriksaan Rumah Sakit</th>
                    <th>Pemeriksaan Dinkes</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="modalPemeriksaan" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Status Rawat</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <span class="alert_form"></span>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Status Rawat</label>
                    <div class="col-lg-9">
                        <input type="hidden" class="form-control" name="id_trx_pemeriksaan_last">
                        <input type="hidden" class="form-control" name="peserta_dari">
                        <input type="hidden" class="form-control" name="id_peserta">
                        <select class="form-control select-search" name="status_rawat" onchange="get_ruang_perawatan()">
                        </select>
                    </div>
                </div>
                <div class="form-group row ruang_perawatan_hidden">
                    <label class="col-form-label col-lg-3">Ruang Perawatan</label>
                    <div class="col-lg-9">
                        <select class="form-control select-search" name="ruang_perawatan">
                        </select>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary" onclick="action_form_status_rawat()">Simpan <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(".ruang_perawatan_hidden").hide();


    function reload_datatable() {
        $('#datatablePesertaRapid').DataTable().ajax.reload();
    }

    $('.daterange-predefined').daterangepicker({
            startDate: moment().subtract(14, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.daterange-predefined span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date]').val(end.format('YYYY-MM-DD'));
            $('#datatablePesertaRapid').DataTable().ajax.reload();
        }
    );

    // Display date format
    $('.daterange-predefined span').html(moment().subtract(14, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date]').val(moment().subtract(14, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date]').val(moment().format('YYYY-MM-DD'));
    get_peserta();

    function get_peserta() {
        $("#datatablePesertaRapid").DataTable({
            ajax: {
                "url": base_url + 'peserta/request/get_peserta_rs',
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "data": function(d) {
                    return $.extend({}, d, {
                        "start_date": $("input[name='start_date']").val(),
                        "end_date": $("input[name='end_date']").val(),
                        "jenis_pemeriksaan": $("select[name='jenis_pemeriksaan']").val(),
                        "hasil_pemeriksaan": $("select[name='hasil_pemeriksaan']").val()
                    });
                },
                "dataSrc": '',
                "complete": function(response) {
                    HoldOn.close();
                }
            },
            "ordering": false,
            "columns": [{
                data: "nama"
            }, {
                data: "nik"
            }, {
                data: "kolom_faskes"
            }, {
                data: "kolom_labkesda"
            }, {
                data: "kolom_rs"
            }, {
                defaultContent: ""
            }, {
                "width": "20%",
                "render": function(data, type, full, meta) {
                    return (full.stat_button ? "<a href='" + base_url + "peserta/edit_peserta/" + full.id_encrypt + "' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + full.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>" : "") + (full.stat_button_detail_peserta ? " <a href = '" + base_url + "peserta/detail_peserta/" + full.id_encrypt + "' class ='btn btn-primary btn-success'> <i class='icon-eye'> </i></a>" : "") + " <a class='btn btn-info btn-icon' onClick=\"show_form('" + full.id_trx_pemeriksaan_last_encrypt + "','" + full.peserta_dari + "','" + full.id_encrypt + "')\" href='#'><i class='icon-bed2'></i></a>";
                }
            }]
        });
    }

    function show_form(id_trx_pemeriksaan_last, peserta_dari, id_peserta) {
        $("input[name='id_trx_pemeriksaan_last']").val("");
        $("input[name='peserta_dari']").val("");
        $("input[name='id_peserta']").val("");
        $("select[name='status_rawat']").html("");

        $.ajax({
            url: base_url + 'peserta/request/get_status_rawat_peserta',
            data: {
                id_trx_pemeriksaan_last: id_trx_pemeriksaan_last,
                peserta_dari: peserta_dari,
                id_peserta: id_peserta
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $("input[name='id_trx_pemeriksaan_last']").val(id_trx_pemeriksaan_last);
                $("input[name='peserta_dari']").val(peserta_dari);
                $("input[name='id_peserta']").val(id_peserta);

                $("#modalPemeriksaan").modal("show");
                $(".title_modal").html("Tambah");
                get_status_rawat((response ? response.is_status : ""));
                get_ruang_perawatan((response ? response.ruang_perawatan : ""));
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function action_form_status_rawat() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let id_trx_pemeriksaan_last = $("input[name='id_trx_pemeriksaan_last']").val();
        let peserta_dari = $("input[name='peserta_dari']").val();
        let id_peserta = $("input[name='id_peserta']").val();
        let status_rawat = $("select[name='status_rawat']").val();
        let ruang_perawatan = $("select[name='ruang_perawatan']").val();

        $(".alert_form").html("");

        if (!status_rawat) {
            $(".alert_form").html("<div class='alert alert-danger'>Status Rawat tidak boleh kosong.</div>");
        } else if (status_rawat == '1' && !ruang_perawatan) {
            $(".alert_form").html("<div class='alert alert-danger'>Ruang Perawatan tidak boleh kosong.</div>");
        } else {
            $.ajax({
                url: base_url + 'peserta/change_status_rawat_peserta',
                data: {
                    id_trx_pemeriksaan_last: id_trx_pemeriksaan_last,
                    peserta_dari: peserta_dari,
                    status_rawat: status_rawat,
                    ruang_perawatan: ruang_perawatan,
                    id_peserta: id_peserta
                },
                type: 'POST',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $("input[name='id_trx_pemeriksaan_last']").val("");
                    $("input[name='peserta_dari']").val("");
                    $("input[name='id_peserta']").val("");
                    $("select[name='status_rawat']").html("");
                    if (response) {
                        $("#modalPemeriksaan").modal("toggle");
                        $('#datatablePesertaRapid').DataTable().ajax.reload();
                        swalInit(
                            'Berhasil',
                            'Data berhasil diubah',
                            'success'
                        );
                    } else {
                        $("#modalPemeriksaan").modal("toggle");
                        $('#datatablePesertaRapid').DataTable().ajax.reload();
                        swalInit(
                            'Gagal',
                            'Data tidak bisa diubah',
                            'error'
                        );
                    }
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }

    function get_status_rawat(param_index = "") {
        let arr = [];
        arr[1] = 'Rawat Inap';
        arr[2] = 'ICU';
        arr[3] = 'Rawat Jalan/Isoma';

        html = "";
        html += "<option value=''>-- Pilih Status Rawat --</option>";
        $.each(arr, function(index, value) {
            if (index != 0) {
                let selected = "";
                if (param_index == index) {
                    selected = "selected";
                }
                html += "<option " + selected + " value='" + index + "'>" + value + "</option>";
            }
        });

        $("select[name='status_rawat']").html(html);
    }

    function get_ruang_perawatan(param_index) {
        let status_rawat = $("select[name='status_rawat']").val();
        if (param_index == '1') {
            status_rawat = param_index;
        }
        if (status_rawat == '1') {
            $(".ruang_perawatan_hidden").show();
            let arr = [];

            let html = "";
            html += "<option value=''>-- Pilih Ruang Perawatan --</option>";
            for (let n = 1; n <= 10; n++) {
                let selected = "";
                if (param_index == n) {
                    selected = "selected";
                }
                html += "<option " + selected + " value='" + n + "'>" + n + "</option>";
            }

            $("select[name='ruang_perawatan']").html(html);
        } else {
            $("select[name='ruang_perawatan']").html("");
            $(".ruang_perawatan_hidden").hide();
        }
    }

    function confirm_delete(id_peserta) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/delete_peserta',
                    data: {
                        id_peserta: id_peserta
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            $('#datatablePesertaRapid').DataTable().ajax.reload();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            $('#datatablePesertaRapid').DataTable().ajax.reload();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        $('#datatablePesertaRapid').DataTable().ajax.reload();
                    }
                });
            }
        });
    }

    function get_hasil_pemeriksaan() {
        let id_jenis_pemeriksaan = $("select[name='jenis_pemeriksaan']").val();

        let html = "<option value=''>-- SEMUA --</option>"
        if (id_jenis_pemeriksaan) {
            $.ajax({
                url: base_url + 'peserta/request/get_hasil_pemeriksaan',
                data: {
                    id_jenis_pemeriksaan: id_jenis_pemeriksaan
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $.each(response, function(index, value) {
                        html += "<option value='" + value.id_encrypt + "'>" + value.nama_hasil_pemeriksaan + "</option>";
                    });
                    $("select[name='hasil_pemeriksaan']").html(html);
                    $('#datatablePesertaRapid').DataTable().ajax.reload();
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        } else {
            $("select[name='hasil_pemeriksaan']").html(html);
            $('#datatablePesertaRapid').DataTable().ajax.reload();
        }

    }
</script>