<style>
    .ft-bld {
        font-weight: bold;
    }
</style>

<div class="content">
    <div class="card card-table">
        <table id="datatableLaporan" class="table datatable-save-state">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>NIK</th>
                    <th>Alamat Domisili</th>
                    <th>Desa / Kelurahan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="modalPemeriksaan" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> RT Domisili</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <span class="alert_form"></span>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Nama</label>
                    <div class="col-lg-9">
                        <input type="text" readonly class="form-control" name="nama">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">RT Domisili</label>
                    <div class="col-lg-9">
                        <input type="hidden" class="form-control" name="id_peserta">
                        <input type="text" id="rt_domisili" class="form-control" name="rt_domisili">
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary" onclick="action_form_rt_domisili()">Simpan <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let datatableLaporan = $("#datatableLaporan").DataTable();
    get_peserta();

    function get_peserta() {
        datatableLaporan.clear().draw();
        $.ajax({
            url: base_url + 'peserta/request/get_peserta_rt_domisili_empty',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let no = 1;
                $.each(response, function(index, value) {
                    datatableLaporan.row.add([
                        no,
                        value.nama,
                        value.nik,
                        value.alamat_domisili,
                        value.nama_wilayah,
                        " <a class='btn btn-info btn-icon' onClick=\"show_form('" + value.id_encrypt + "')\" href='#'><i class='icon-pencil'></i></a>"
                    ]).draw(false);
                    no++;
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function show_form(id_peserta) {
        $("input[name='id_peserta']").val("");
        $("input[name='nama']").val("");
        $("input[name='rt_domisili']").val("");
        $(".alert_form").html("");

        $.ajax({
            url: base_url + 'peserta/request/get_detail_peserta_update_nik_rt_domisili',
            data: {
                id_peserta: id_peserta
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $("input[name='id_peserta']").val(response.id_encrypt);
                $("input[name='nama']").val(response.nama);
                $("input[name='rt_domisili']").val(response.rt_domisili);

                $("#modalPemeriksaan").modal("show");
                $(".title_modal").html("Ubah");
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function action_form_rt_domisili() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let id_peserta = $("input[name='id_peserta']").val();
        let rt_domisili = $("input[name='rt_domisili']").val();

        $(".alert_form").html("");

        if (!rt_domisili) {
            $(".alert_form").html("<div class='alert alert-danger'>RT Domisili tidak boleh kosong.</div>");
        } else {
            $.ajax({
                url: base_url + 'peserta/update_rt_domisili_peserta',
                data: {
                    id_peserta: id_peserta,
                    rt_domisili: rt_domisili
                },
                type: 'POST',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $("input[name='id_peserta']").val("");
                    $("input[name='nama']").val("");
                    $("input[name='rt_domisili']").val("");
                    if (response) {
                        $("#modalPemeriksaan").modal("toggle");
                        get_peserta();
                        swalInit(
                            'Berhasil',
                            'Data berhasil diubah',
                            'success'
                        );
                    } else {
                        $("#modalPemeriksaan").modal("toggle");
                        get_peserta();
                        swalInit(
                            'Gagal',
                            'Data tidak bisa diubah',
                            'error'
                        );
                    }
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }

    setInputFilter(document.getElementById("rt_domisili"), function(value) {
        return /^-?\d*$/.test(value);
    });
</script>