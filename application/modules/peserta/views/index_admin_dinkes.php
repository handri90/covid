<style>
    .info-column {
        margin: 4px 0;
    }
</style>

<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Tanggal</label>
                <div class="col-lg-10">
                    <button type="button" class="btn btn-light daterange-predefined">
                        <i class="icon-calendar22 mr-2"></i>
                        <span></span>
                    </button>

                    <input type="hidden" name="start_date" />
                    <input type="hidden" name="end_date" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Jenis Pemeriksaan</label>
                <div class="col-lg-4">
                    <select class="form-control select-search" name="jenis_pemeriksaan" onchange="get_hasil_pemeriksaan()">
                        <option value="">-- SEMUA --</option>
                        <?php
                        foreach ($jenis_pemeriksaan as $key => $value) {
                        ?>
                            <option value="<?php echo encrypt_data($value->id_jenis_pemeriksaan); ?>"><?php echo $value->nama_jenis_pemeriksaan; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Hasil Pemeriksaan</label>
                <div class="col-lg-4">
                    <select class="form-control select-search" name="hasil_pemeriksaan" onchange="reload_datatable()">
                        <option value="">-- SEMUA --</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <div class="card-header header-elements-inline">
            <span class="card-title d-inline-flex"><input type="checkbox" class="form-check-input-styled" name="greather_than_14" value="1" onclick="reload_datatable()" /> <span class="ml-2">Pemeriksaan > 14 Hari</span></span>
            <div class="header-elements d-inline-flex">
                <a href="<?php echo base_url() . 'peserta/tambah_peserta'; ?>" class="btn btn-info mr-1">Tambah Peserta Rapid</a>
                <?php echo form_open(base_url() . 'peserta/cetak_peserta_swab', array("target" => "_blank")); ?>
                <button type="submit" class="btn btn-success">Cetak Peserta SWAB</button>
                <input type="text" class="form-control daterange-single d-inline" style="width:auto;" readonly name="tanggal_cetak_peserta" required placeholder="Cetak Peserta">
                <?php echo form_close(); ?>
            </div>
        </div>
        <table id="datatablePesertaRapid" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>NIK</th>
                    <th>Pemeriksaan Faskes</th>
                    <th>Pemeriksaan Labkesda</th>
                    <th>Pemeriksaan Rumah Sakit</th>
                    <th>Pemeriksaan Dinkes</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    $('.form-check-input-styled').uniform();

    function reload_datatable() {
        $('#datatablePesertaRapid').DataTable().ajax.reload();
    }

    $('.daterange-single').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD/MM/YYYY'
        },
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10)
    });


    $('.daterange-predefined').daterangepicker({
            startDate: moment().subtract(30, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.daterange-predefined span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date]').val(end.format('YYYY-MM-DD'));
            $('#datatablePesertaRapid').DataTable().ajax.reload();
        }
    );

    // Display date format
    $('.daterange-predefined span').html(moment().subtract(30, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date]').val(moment().subtract(30, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date]').val(moment().format('YYYY-MM-DD'));
    get_peserta();

    function get_peserta() {

        $("#datatablePesertaRapid").DataTable({
            ajax: {
                "url": base_url + 'peserta/request/get_peserta_dinkes',
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "data": function(d) {
                    return $.extend({}, d, {
                        "start_date": $("input[name='start_date']").val(),
                        "end_date": $("input[name='end_date']").val(),
                        "jenis_pemeriksaan": $("select[name='jenis_pemeriksaan']").val(),
                        "hasil_pemeriksaan": $("select[name='hasil_pemeriksaan']").val(),
                        "greather_than_14": $("input[name='greather_than_14']").is(":checked"),
                    });
                },
                "dataSrc": '',
                "complete": function(response) {
                    HoldOn.close();
                }
            },
            "ordering": false,
            "columns": [{
                data: "nama"
            }, {
                data: "nik"
            }, {
                data: "kolom_faskes"
            }, {
                data: "kolom_labkesda"
            }, {
                data: "kolom_rs"
            }, {
                defaultContent: ""
            }, {
                "width": "15%",
                "render": function(data, type, full, meta) {
                    return "<a href='" + base_url + "peserta/edit_peserta/" + full.id_encrypt + "' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a href = '" + base_url + "peserta/detail_peserta/" + full.id_encrypt + "' class ='btn btn-primary btn-icon'> <i class='icon-eye'> </i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + full.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>";
                }
            }]
        });
    }

    function confirm_delete(id_peserta) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/delete_peserta',
                    data: {
                        id_peserta: id_peserta
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            $('#datatablePesertaRapid').DataTable().ajax.reload();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            $('#datatablePesertaRapid').DataTable().ajax.reload();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        $('#datatablePesertaRapid').DataTable().ajax.reload();
                    }
                });
            }
        });
    }

    function redirect_detail_peserta() {
        let id_peserta = $("select[name='nik_nama']").val();
        let kode_sample = $("select[name='kode_sample']").val();

        if (id_peserta || kode_sample) {
            if (kode_sample) {
                id_peserta = $("select[name='kode_sample']").find("option:selected").attr("id-peserta");
            }

            location.href = base_url + "peserta/detail_peserta/" + id_peserta + "/" + kode_sample;
        }
    }

    function get_hasil_pemeriksaan() {
        let id_jenis_pemeriksaan = $("select[name='jenis_pemeriksaan']").val();

        let html = "<option value=''>-- SEMUA --</option>"
        if (id_jenis_pemeriksaan) {
            $.ajax({
                url: base_url + 'peserta/request/get_hasil_pemeriksaan',
                data: {
                    id_jenis_pemeriksaan: id_jenis_pemeriksaan
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $.each(response, function(index, value) {
                        html += "<option value='" + value.id_encrypt + "'>" + value.nama_hasil_pemeriksaan + "</option>";
                    });
                    $("select[name='hasil_pemeriksaan']").html(html);
                    $('#datatablePesertaRapid').DataTable().ajax.reload();
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        } else {
            $("select[name='hasil_pemeriksaan']").html(html);
            $('#datatablePesertaRapid').DataTable().ajax.reload();
        }

    }
</script>