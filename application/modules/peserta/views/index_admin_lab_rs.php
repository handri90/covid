<style>
    .info-column {
        margin: 4px 0;
    }
</style>

<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Tanggal</label>
                <div class="col-lg-10">
                    <button type="button" class="btn btn-light daterange-predefined">
                        <i class="icon-calendar22 mr-2"></i>
                        <span></span>
                    </button>

                    <input type="hidden" name="start_date" />
                    <input type="hidden" name="end_date" />
                </div>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <table id="datatablePesertaRapid" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>NIK</th>
                    <th>Pemeriksaan Faskes</th>
                    <th>Pemeriksaan Labkesda</th>
                    <th>Pemeriksaan Rumah Sakit</th>
                    <th>Status Pemeriksaan</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="modalPemeriksaan" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Status Pemeriksaan</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <span class="alert_form"></span>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Status Pemeriksaan</label>
                    <div class="col-lg-9">
                        <input type="hidden" class="form-control" name="id_peserta">
                        <input type="hidden" class="form-control" name="id_trx_pemeriksaan">
                        <input type="hidden" class="form-control" name="peserta_dari">
                        <select class="form-control select-search" name="status_pemeriksaan">
                        </select>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary" onclick="action_form_status_pemeriksaan()">Simpan <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.daterange-predefined').daterangepicker({
            startDate: moment().subtract(14, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.daterange-predefined span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date]').val(end.format('YYYY-MM-DD'));
            $('#datatablePesertaRapid').DataTable().ajax.reload();
        }
    );

    // Display date format
    $('.daterange-predefined span').html(moment().subtract(14, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date]').val(moment().subtract(14, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date]').val(moment().format('YYYY-MM-DD'));
    get_peserta();

    function get_peserta() {

        $("#datatablePesertaRapid").DataTable({
            ajax: {
                "url": base_url + 'peserta/request/get_peserta_rs_lab',
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "data": function(d) {
                    return $.extend({}, d, {
                        "start_date": $("input[name='start_date']").val(),
                        "end_date": $("input[name='end_date']").val(),
                    });
                },
                "dataSrc": '',
                "complete": function(response) {
                    HoldOn.close();
                }
            },
            "ordering": false,
            "columns": [{
                data: "nama"
            }, {
                data: "nik"
            }, {
                data: "kolom_faskes"
            }, {
                data: "kolom_labkesda"
            }, {
                data: "kolom_rs"
            }, {
                "render": function(data, type, full, meta) {
                    return (full.tracking_pemeriksaan_rs == '1' ? "Belum" :
                        (full.tracking_pemeriksaan_rs == "2" ? "Proses" :
                            (full.tracking_pemeriksaan_rs == "3" ? "Selesai" :
                                "Belum"
                            )
                        )
                    );
                }
            }, {
                "width": "15%",
                "render": function(data, type, full, meta) {
                    return " <a class='btn btn-info btn-icon' onClick=\"show_form('" + full.id_trx_pemeriksaan_last_encrypt + "','" + full.peserta_dari +
                        "')\" href='#'><i class='icon-pencil'></i></a>";
                }
            }]
        });
    }

    function show_form(id_trx_pemeriksaan, peserta_dari) {
        $("input[name='id_peserta']").val("");
        $("input[name='id_trx_pemeriksaan']").val("");
        $("input[name='peserta_dari']").val("");
        $("select[name='status_pemeriksaan']").html("");

        $.ajax({
            url: base_url + 'peserta/request/get_detail_trx_peserta_admin_lab',
            data: {
                id_trx_pemeriksaan: id_trx_pemeriksaan,
                peserta_dari: peserta_dari
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $("input[name='id_peserta']").val(response.peserta_id_encrypt);
                $("input[name='id_trx_pemeriksaan']").val(response.trx_pemeriksaan_id_encrypt);
                $("input[name='peserta_dari']").val(peserta_dari);

                $("#modalPemeriksaan").modal("show");
                $(".title_modal").html("Tambah");
                get_status_pemeriksaan(response.tracking_pemeriksaan);
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function action_form_status_pemeriksaan() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let id_peserta = $("input[name='id_peserta']").val();
        let id_trx_pemeriksaan = $("input[name='id_trx_pemeriksaan']").val();
        let peserta_dari = $("input[name='peserta_dari']").val();
        let status_pemeriksaan = $("select[name='status_pemeriksaan']").val();

        $(".alert_form").html("");
        $.ajax({
            url: base_url + 'peserta/change_status_pemeriksaan_pcr',
            data: {
                id_trx_pemeriksaan: id_trx_pemeriksaan,
                id_peserta: id_peserta,
                peserta_dari: peserta_dari,
                status_pemeriksaan: status_pemeriksaan
            },
            type: 'POST',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $("input[name='id_peserta']").val("");
                $("input[name='id_trx_pemeriksaan']").val("");
                $("input[name='peserta_dari']").val("");
                $("select[name='status_pemeriksaan']").html("");
                if (response) {
                    $("#modalPemeriksaan").modal("toggle");
                    $('#datatablePesertaRapid').DataTable().ajax.reload();
                    swalInit(
                        'Berhasil',
                        'Data berhasil diubah',
                        'success'
                    );
                } else {
                    $("#modalPemeriksaan").modal("toggle");
                    $('#datatablePesertaRapid').DataTable().ajax.reload();
                    swalInit(
                        'Gagal',
                        'Data tidak bisa diubah',
                        'error'
                    );
                }
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function get_status_pemeriksaan(param_index = "") {
        let arr = [];
        arr[1] = 'Belum';
        arr[2] = 'Proses';
        arr[3] = 'Selesai';

        html = "";
        $.each(arr, function(index, value) {
            if (index != 0) {
                let selected = "";
                if (param_index == index) {
                    selected = "selected";
                }
                html += "<option " + selected + " value='" + index + "'>" + value + "</option>";
            }
        });

        $("select[name='status_pemeriksaan']").html(html);
    }

    function confirm_delete(id_peserta) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/delete_peserta',
                    data: {
                        id_peserta: id_peserta
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            $('#datatablePesertaRapid').DataTable().ajax.reload();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            $('#datatablePesertaRapid').DataTable().ajax.reload();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        $('#datatablePesertaRapid').DataTable().ajax.reload();
                    }
                });
            }
        });
    }
</script>