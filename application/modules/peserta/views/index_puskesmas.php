<style>
    .info-column {
        margin: 4px 0;
    }

    .count_list_non_puskes,
    .count_list_peserta_notify {
        font-style: normal;
        font-weight: 400;
        font-variant: normal;
        text-transform: none;
        line-height: 1;
        min-width: 1em;
        display: inline-block;
        text-align: center;
        font-size: 1rem;
        vertical-align: middle;
        position: relative;
    }
</style>

<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Tanggal</label>
                <div class="col-lg-10">
                    <button type="button" class="btn btn-light daterange-predefined">
                        <i class="icon-calendar22 mr-2"></i>
                        <span></span>
                    </button>

                    <input type="hidden" name="start_date" />
                    <input type="hidden" name="end_date" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Jenis Pemeriksaan</label>
                <div class="col-lg-4">
                    <select class="form-control select-search" name="jenis_pemeriksaan" onchange="get_hasil_pemeriksaan()">
                        <option value="">-- SEMUA --</option>
                        <?php
                        foreach ($jenis_pemeriksaan as $key => $value) {
                        ?>
                            <option value="<?php echo encrypt_data($value->id_jenis_pemeriksaan); ?>"><?php echo $value->nama_jenis_pemeriksaan; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Hasil Pemeriksaan</label>
                <div class="col-lg-4">
                    <select class="form-control select-search" name="hasil_pemeriksaan" onchange="reload_datatable()">
                        <option value="">-- SEMUA --</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <div class="card-header header-elements-inline">
            <span class="card-title d-inline-flex"><input type="checkbox" class="form-check-input-styled" name="greather_than_14" value="1" onclick="reload_datatable()" /> <span class="ml-2">Isolasi Mandiri > 14 Hari</span></span>
            <div class="header-elements">
                <a href="#" class="btn btn-warning btn-labeled btn-labeled-right mr-2 mb-2" onclick="peserta_pantauan_polsek()">Pantauan Polsek <b><i class="count_list_peserta_notify"></i></b></a>
                <a href="<?php echo base_url() . 'peserta/detail_peserta_non_puskesmas'; ?>" class="btn btn-success btn-labeled btn-labeled-right mr-2 mb-2">List Peserta Non Puskesmas <b><i class="count_list_non_puskes"></i></b></a>
                <a href="<?php echo base_url() . 'peserta/tambah_peserta'; ?>" class="btn btn-info mr-2 mb-2">Tambah Peserta Rapid</a>
            </div>
        </div>
        <table id="datatablePesertaRapid" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>NIK</th>
                    <th>Pemeriksaan Faskes</th>
                    <th>Pemeriksaan Labkesda</th>
                    <th>Pemeriksaan Rumah Sakit</th>
                    <th>Pemeriksaan Dinkes</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="modalPesertaNotifyPolsek" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> List Peserta</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="card card-table table-responsive shadow-0 mb-0">
                    <table id="datatablePesertaPantauanPolsek" class="table datatable-save-state table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>NIK</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.form-check-input-styled').uniform();

    function reload_datatable() {
        $('#datatablePesertaRapid').DataTable().ajax.reload();
    }

    $('.daterange-predefined').daterangepicker({
            startDate: moment().subtract(14, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.daterange-predefined span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date]').val(end.format('YYYY-MM-DD'));
            $('#datatablePesertaRapid').DataTable().ajax.reload();
        }
    );

    // Display date format
    $('.daterange-predefined span').html(moment().subtract(14, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date]').val(moment().subtract(14, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date]').val(moment().format('YYYY-MM-DD'));
    get_peserta();

    function get_peserta() {
        $("#datatablePesertaRapid").DataTable({
            ajax: {
                "url": base_url + 'peserta/request/get_peserta_faskes',
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "data": function(d) {
                    return $.extend({}, d, {
                        "start_date": $("input[name='start_date']").val(),
                        "end_date": $("input[name='end_date']").val(),
                        "jenis_pemeriksaan": $("select[name='jenis_pemeriksaan']").val(),
                        "hasil_pemeriksaan": $("select[name='hasil_pemeriksaan']").val(),
                        "greather_than_14": $("input[name='greather_than_14']").is(":checked"),
                    });
                },
                "dataSrc": '',
                "complete": function(response) {
                    HoldOn.close();
                }
            },
            "ordering": false,
            "columns": [{
                data: "nama"
            }, {
                data: "nik"
            }, {
                data: "kolom_faskes"
            }, {
                data: "kolom_labkesda"
            }, {
                data: "kolom_rs"
            }, {
                defaultContent: ""
            }, {
                "width": "15%",
                "render": function(data, type, full, meta) {
                    return (full.stat_button_delete ? "<a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + full.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>" : " <a class='btn btn-light btn-icon' onClick=\"call_admin('delete')\" href='#'><i class='icon-trash'></i></a>") + (full.stat_button_edit ? " <a href='" + base_url + "peserta/edit_peserta/" + full.id_encrypt + "' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a>" : " <a href='#' onClick=\"call_admin('delete')\" class='btn btn-light btn-icon'><i class='icon-pencil7'></i></a> ") + (full.stat_button_view ? " <a href='" + base_url + "peserta/detail_peserta/" + full.id_encrypt + "' class='btn btn-warning btn-icon'><i class='icon-eye'></i></a>" : "");
                }
            }]
        });
    }

    function peserta_pantauan_polsek() {
        $("#modalPesertaNotifyPolsek").modal("show");
        if ($.fn.dataTable.isDataTable('#datatablePesertaPantauanPolsek')) {
            $('#datatablePesertaPantauanPolsek').DataTable().ajax.reload();
        } else {
            $("#datatablePesertaPantauanPolsek").DataTable({
                ajax: {
                    "url": base_url + 'peserta/request/get_peserta_notify_polsek',
                    "beforeSend": function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    "beforeSend": function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    "dataSrc": '',
                    "complete": function(response) {
                        HoldOn.close();
                    }
                },
                "ordering": false,
                "columns": [{
                    data: "nama"
                }, {
                    data: "nik"
                }, {
                    "width": "15%",
                    "render": function(data, type, full, meta) {
                        return "<a class='btn btn-info btn-icon' onClick=\"confirm_sembuh('" + full.id_encrypt + "')\" href='#'><i class='icon-diff'></i> Sembuh</a>";
                    }
                }]
            });
        }


    }

    function confirm_sembuh(id_peserta) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menambah status sembuh?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/status_sembuh',
                    data: {
                        id_peserta: id_peserta
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            $('#datatablePesertaPantauanPolsek').DataTable().ajax.reload();
                            $('#datatablePesertaRapid').DataTable().ajax.reload();
                            get_count_peserta_notify();
                            swalInit(
                                'Berhasil',
                                'Data berhasil ditambahkan',
                                'success'
                            );
                        } else {
                            $('#datatablePesertaPantauanPolsek').DataTable().ajax.reload();
                            $('#datatablePesertaRapid').DataTable().ajax.reload();
                            get_count_peserta_notify();
                            swalInit(
                                'Gagal',
                                'Data gagal ditambahkan',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }

    function confirm_delete(id_peserta) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/delete_peserta',
                    data: {
                        id_peserta: id_peserta
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            $('#datatablePesertaRapid').DataTable().ajax.reload();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            $('#datatablePesertaRapid').DataTable().ajax.reload();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        $('#datatablePesertaRapid').DataTable().ajax.reload();
                    }
                });
            }
        });
    }

    get_count_peserta_non_puskesmas();

    function get_count_peserta_non_puskesmas() {
        $.ajax({
            url: base_url + 'peserta/request/count_peserta_non_puskesmas',
            type: 'GET',
            success: function(response) {
                $(".count_list_non_puskes").html((response) ? response.jumlah_peserta : "0");
            },
        });
    }

    $(function() {
        setInterval(get_count_peserta_non_puskesmas, 10000);
    })

    get_count_peserta_notify();

    function get_count_peserta_notify() {
        $.ajax({
            url: base_url + 'peserta/request/count_peserta_notify',
            type: 'GET',
            success: function(response) {
                $(".count_list_peserta_notify").html((response) ? response.jumlah_peserta : "0");
            },
        });
    }

    $(function() {
        setInterval(get_count_peserta_notify, 10000);
    })

    function call_admin(param) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let label = "";
        if (param == 'delete') {
            label = "Hubungi admin dinkes untuk menghapus data";
        } else if (param == 'edit') {
            label = "Hubungi admin dinkes untuk mengubah data";
        }

        swalInit(
            'Gagal',
            label,
            'error'
        );
    }

    function get_hasil_pemeriksaan() {
        let id_jenis_pemeriksaan = $("select[name='jenis_pemeriksaan']").val();

        let html = "<option value=''>-- SEMUA --</option>"
        if (id_jenis_pemeriksaan) {
            $.ajax({
                url: base_url + 'peserta/request/get_hasil_pemeriksaan',
                data: {
                    id_jenis_pemeriksaan: id_jenis_pemeriksaan
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $.each(response, function(index, value) {
                        html += "<option value='" + value.id_encrypt + "'>" + value.nama_hasil_pemeriksaan + "</option>";
                    });
                    $("select[name='hasil_pemeriksaan']").html(html);
                    $('#datatablePesertaRapid').DataTable().ajax.reload();
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        } else {
            $("select[name='hasil_pemeriksaan']").html(html);
            $('#datatablePesertaRapid').DataTable().ajax.reload();
        }

    }
</script>