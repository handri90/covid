<style>
    .ft-bld {
        font-weight: bold;
    }
</style>

<div class="content">
    <div class="card card-table">
        <table id="datatableLaporan" class="table datatable-save-state">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Alamat Domisili</th>
                    <th>Desa / Kelurahan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="modalPemeriksaan" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> NIK</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <span class="alert_form"></span>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Nama</label>
                    <div class="col-lg-9">
                        <input type="text" readonly class="form-control" name="nama">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">NIK</label>
                    <div class="col-lg-9">
                        <input type="hidden" class="form-control" name="id_peserta">
                        <input type="text" id="nik" class="form-control" name="nik">
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary" onclick="action_form_nik()">Simpan <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let datatableLaporan = $("#datatableLaporan").DataTable();
    get_peserta();

    function get_peserta() {
        datatableLaporan.clear().draw();
        $.ajax({
            url: base_url + 'peserta/request/get_peserta_nik_empty',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let no = 1;
                $.each(response, function(index, value) {
                    datatableLaporan.row.add([
                        no,
                        value.nama,
                        value.alamat_domisili,
                        value.nama_wilayah,
                        " <a class='btn btn-info btn-icon' onClick=\"show_form('" + value.id_encrypt + "')\" href='#'><i class='icon-pencil'></i></a>"
                    ]).draw(false);
                    no++;
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function show_form(id_peserta) {
        $("input[name='id_peserta']").val("");
        $("input[name='nama']").val("");
        $("input[name='nik']").val("");
        $(".alert_form").html("");

        $.ajax({
            url: base_url + 'peserta/request/get_detail_peserta_update_nik_rt_domisili',
            data: {
                id_peserta: id_peserta
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $("input[name='id_peserta']").val(response.id_encrypt);
                $("input[name='nama']").val(response.nama);
                $("input[name='nik']").val(response.nik);

                $("#modalPemeriksaan").modal("show");
                $(".title_modal").html("Ubah");
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function action_form_nik() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let id_peserta = $("input[name='id_peserta']").val();
        let nik = $("input[name='nik']").val();

        $(".alert_form").html("");

        if (!nik) {
            $(".alert_form").html("<div class='alert alert-danger'>NIK tidak boleh kosong.</div>");
        } else if (nik.length != 16) {
            $(".alert_form").html("<div class='alert alert-danger'>NIK harus berjumlah 16 Digit.</div>");
        } else {
            $.ajax({
                url: base_url + 'peserta/update_nik_peserta',
                data: {
                    id_peserta: id_peserta,
                    nik: nik
                },
                type: 'POST',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $("input[name='id_peserta']").val("");
                    $("input[name='nama']").val("");
                    $("input[name='nik']").val("");
                    if (response) {
                        $("#modalPemeriksaan").modal("toggle");
                        get_peserta();
                        swalInit(
                            'Berhasil',
                            'Data berhasil diubah',
                            'success'
                        );
                    } else {
                        $("#modalPemeriksaan").modal("toggle");
                        get_peserta();
                        swalInit(
                            'Gagal',
                            'Data tidak bisa diubah',
                            'error'
                        );
                    }
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }

    setInputFilter(document.getElementById("nik"), function(value) {
        return /^-?\d*$/.test(value);
    });
</script>