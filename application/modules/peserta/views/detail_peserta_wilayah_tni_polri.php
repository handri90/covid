<style>
    .header_tbl {
        width: 15%;
    }

    .header_tbl2 {
        width: 1%;
    }

    .info-column {
        margin: 4px 0;
    }
</style>
<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <input type="hidden" name="id_wilayah" value="<?php echo !empty($id_wilayah) ? $id_wilayah : ""; ?>" />
            <div class="card card-table table-responsive shadow-0 mb-0">
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="header_tbl">Wilayah</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($wilayah) ? $wilayah->klasifikasi . " " . $wilayah->nama_wilayah : ""; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="card card-table">
                <table id="datatablePesertaRapid" class="table datatable-save-state table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Tanggal Terkonfirmasi</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="modalPemeriksaan" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Biodata</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="card card-table table-responsive shadow-0 mb-0">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td class="header_tbl">Nama</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="nama"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">NIK</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="nik"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">Tanggal Lahir</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="tanggal_lahir"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">Jenis Kelamin</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="jenis_kelamin"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">Pekerjaan</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="pekerjaan"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">Alamat KTP</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="alamat_ktp"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">Alamat Domisili</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="alamat_domisili"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">Nomor Telepon</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="nomor_telepon"></span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let datatablePesertaRapid = $("#datatablePesertaRapid").DataTable({
        "deferRender": true,
        "ordering": false,
        "paging": false,
        "columns": [{
                "width": "75%"
            },
            null,
            null
        ]
    });

    get_detail_peserta();

    function get_detail_peserta() {
        let id_wilayah = $("input[name='id_wilayah']").val();

        datatablePesertaRapid.clear().draw();

        $.ajax({
            url: base_url + 'peserta/request/get_detail_peserta_by_wilayah',
            data: {
                id_wilayah: id_wilayah
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    datatablePesertaRapid.row.add([
                        value.nama,
                        value.tanggal_terkonfirmasi_custom,
                        "<a href='#detailPeserta' onClick=\"show_detail_peserta('" + value.id_encrypt + "')\" class='btn btn-primary btn-icon'><i class='icon-eye'></i></a>"
                    ]).draw(false);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function show_detail_peserta(id_peserta) {
        $("#modalPemeriksaan").modal("show");
        $(".nama").html("");
        $(".nik").html("");
        $(".tanggal_lahir").html("");
        $(".jenis_kelamin").html("");
        $(".pekerjaan").html("");
        $(".alamat_ktp").html("");
        $(".alamat_domisili").html("");
        $(".nomor_telepon").html("");
        $.ajax({
            url: base_url + 'peserta/request/get_detail_peserta_tni_polri',
            data: {
                id_peserta: id_peserta
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $(".nama").html(response.nama);
                $(".nik").html(response.nik);
                $(".tanggal_lahir").html(response.tanggal_lahir);
                $(".jenis_kelamin").html(response.jenis_kelamin);
                $(".pekerjaan").html(response.pekerjaan);
                $(".alamat_ktp").html(response.alamat_ktp);
                $(".alamat_domisili").html(response.alamat_domisili);
                $(".nomor_telepon").html(response.telepon);
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }
</script>