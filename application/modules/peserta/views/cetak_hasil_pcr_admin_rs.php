<html>

<head>
    <style>
        body {
            font-family: Georgia, 'Times New Roman', serif;
            font-size: 10pt;
            font-weight: normal;
        }

        .head_print {
            text-align: center;
            font-weight: 100;
            line-height: 0.6;
        }

        .wd-number {
            width: 10px;
        }

        .wd-label {
            width: 100px;
        }

        .wd-label-2 {
            width: 60px;

        }

        .wd-peserta {
            width: 200px;
        }

        .wd-peserta-2 {
            width: 310px;
        }

        .wd-titik {
            width: 2px;
        }

        table {
            margin: 5px 0;
        }

        .tbl-sign {
            margin-top: 20px;
            text-align: center;
        }

        .ruler {
            border-bottom: 2px double black;
        }

        .logo {
            float: left;
            width: 20%;
        }

        .deskripsi {
            float: left;
            width: 60%;
        }

        .alamat {
            line-height: 0.5;
        }

        .text-center {
            text-align: center;
        }

        .border-td {
            border: 1px solid #000;
            text-align: center;
        }
    </style>
</head>

<body>
    <table style="width:100%;margin-top:20px">
        <tr>
            <td colspan="5">
                <table>
                    <tr>
                        <td>Di Cetak Oleh</td>
                        <td>:</td>
                        <td><?php echo isset($user) ? $user->nama_lengkap . " (" . $user->nama_level_user . ")" : ""; ?></td>
                    </tr>
                    <tr>
                        <td>Tanggal Cetak</td>
                        <td>:</td>
                        <td><?php echo date_indo(date('Y-m-d')) . " " . date("H:i:s"); ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td rowspan="6" width="200"></td>
            <td rowspan="5" width="70">
                <img src="./assets/template_admin/logo_kobar.png" width="60" />
            </td>
            <td class="text-center">PEMERINTAH KABUPATEN KOTAWARINGIN BARAT</td>
            <td rowspan="5" width="70">
                <img src="./assets/template_admin/logo_rsud.png" width="60" />
            </td>
            <td rowspan="6" width="200"></td>
        </tr>
        <tr>
            <td class="text-center" style="font-weight:bold;font-size:12pt;">RUMAH SAKIT UMUM DAERAH</td>
        </tr>
        <tr>
            <td class="text-center">SULTAN IMANUDDIN PANGKALAN BUN</td>
        </tr>
        <tr>
            <td class="text-center">Akreditasi KARS Nomor : KARS-SERT/623/VII/2020 Tanggal 24 Juli 2020</td>
        </tr>
        <tr>
            <td class="text-center">Jalan Sutan Syahrir 17 Pangkalan Bun - 74112</td>
        </tr>
        <tr>
            <td colspan="3" style="border-top:3px double #000000;">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" height="10">&nbsp;</td>
            <td width="200" height="10">
                <table>
                    <tr>
                        <td>Lampiran</td>
                        <td>:</td>
                        <td>1 Lembar</td>
                    </tr>
                    <tr>
                        <td>Nomor</td>
                        <td>:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td><?php echo isset($peserta) ? date_indo($peserta->tanggal_pemeriksaan) : ""; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="5" class="text-center" style="font-weight:bold;">HASIL PEMERIKSAAN LABORATORIUM</td>
        </tr>
        <tr>
            <td colspan="5" class="text-center" style="font-weight:bold;">RUMAH SAKIT UMUM DAERAH SULTAN IMANUDDIN PANGKALAN BUN</td>
        </tr>
        <tr>
            <td colspan="5">
                <table width="100%" style="border-collapse:collapse;font-size:9pt;">
                    <tr>
                        <td width="120">No. RM</td>
                        <td width="20">:</td>
                        <td><?php echo isset($peserta) ? $peserta->no_rekam_medis : ""; ?></td>
                        <td width="150" rowspan="5">&nbsp;</td>
                        <td width="120">Nama Laboratorium</td>
                        <td width="20">:</td>
                        <td><?php echo isset($peserta) ? $peserta->nama_lab : ""; ?></td>
                    </tr>
                    <tr>
                        <td>NIK</td>
                        <td>:</td>
                        <td><?php echo isset($peserta) ? $peserta->nik : ""; ?></td>
                        <td width="120">Kegiatan</td>
                        <td width="20">:</td>
                        <td>Covid-19</td>
                    </tr>
                    <tr>
                        <td>Nama Pasien</td>
                        <td>:</td>
                        <td><?php echo isset($peserta) ? $peserta->nama : ""; ?></td>
                        <td width="120">Metode Pemeriksaan</td>
                        <td width="20">:</td>
                        <td>PCR</td>
                    </tr>
                    <tr>
                        <td>Tanggal Lahir</td>
                        <td>:</td>
                        <td><?php echo isset($peserta) ? $peserta->tanggal_lahir : ""; ?></td>
                        <td width="140">Tanggal Pemeriksaan</td>
                        <td width="20">:</td>
                        <td><?php echo isset($peserta) ? date_indo($peserta->tanggal_pemeriksaan) : ""; ?></td>
                    </tr>
                    <tr>
                        <td>Alamat Pasien</td>
                        <td>:</td>
                        <td><?php echo isset($peserta) ? $peserta->alamat_domisili . " RT. " . $peserta->rt_domisili . " " . $peserta->klasifikasi . " " . $peserta->nama_wilayah : ""; ?></td>
                        <td colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <table width="100%" style="border-collapse:collapse;">
                                <tr>
                                    <th class="border-td" width="20">No.</th>
                                    <th class="border-td" width="60">No. Lab</th>
                                    <th class="border-td" width="120">Nama</th>
                                    <th class="border-td" width="120">Umur</th>
                                    <th class="border-td" width="120">Jenis Kelamin</th>
                                    <th class="border-td" width="120">Asal Pengirim</th>
                                    <th class="border-td" width="120">Provinsi Pengirim</th>
                                    <th class="border-td" width="120">Tanggal Terima Sampel</th>
                                    <th class="border-td" width="120">Jenis Spesimen</th>
                                    <th class="border-td" width="120">Hasil Pemeriksaan Laboratorium</th>
                                    <th class="border-td" width="120">Kesimpulan</th>
                                </tr>
                                <tr>
                                    <td class="border-td">1</td>
                                    <td class="border-td"><?php echo isset($peserta) ? $peserta->kode_sample : ""; ?></td>
                                    <td class="border-td"><?php echo isset($peserta) ? $peserta->nama : ""; ?></td>
                                    <td class="border-td">
                                        <?php
                                        if ($peserta && $peserta->tanggal_lahir_calc) {
                                            $birthDate = explode("/", $peserta->tanggal_lahir_calc);
                                            //get age from date or birthdate
                                            $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
                                                ? ((date("Y") - $birthDate[2]) - 1)
                                                : (date("Y") - $birthDate[2]));

                                            echo $age;
                                        }
                                        ?>
                                    </td>
                                    <td class="border-td"><?php echo isset($peserta) ? $peserta->jenis_kelamin : ""; ?></td>
                                    <td class="border-td">RSUD Sultan Imanuddin</td>
                                    <td class="border-td">Kalimantan Tengah</td>
                                    <td class="border-td"><?php echo isset($peserta) ? ($peserta->tanggal_terima_sampel ? date_indo($peserta->tanggal_terima_sampel) : "") : ""; ?></td>
                                    <td class="border-td"><?php echo isset($peserta) ? ($peserta->jenis_spesimen == "1" ? "Nasofaring" : ($peserta->jenis_spesimen == "2" ? "Orofaring" : "")) : ""; ?></td>
                                    <td class="border-td"><?php echo isset($peserta) ? ($peserta->class_badge_last == "1" ? "Negatif" : ($peserta->class_badge_last == "2" ? "Positif" : "")) : ""; ?></td>
                                    <td class="border-td"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="tbl-sign" width="100%">
        <tr>
            <td>Mengetahui</td>
            <td style="width:360px"></td>
            <td>Pangkalan Bun,<?php echo isset($peserta) ? date_indo($peserta->tanggal_pemeriksaan) : ""; ?></td>
        </tr>
        <tr>
            <td>Kepala Instalasi Laboratorium</td>
            <td></td>
            <td>Penanggung Jawab</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="padding-top:40px;"><?php echo isset($user_head_lab) ? $user_head_lab->nama_lengkap : ""; ?></td>
            <td></td>
            <td style="padding-top:40px;"><?php echo isset($peserta) ? $peserta->nama_dokter : ""; ?></td>
        </tr>
        <tr>
            <td><?php echo isset($user_head_lab) ? $user_head_lab->nip : ""; ?></td>
            <td></td>
            <td><?php echo isset($peserta) ? ($peserta->nip_dokter ? $peserta->nip_dokter : "") : ""; ?></td>
        </tr>
    </table>
</body>

</html>