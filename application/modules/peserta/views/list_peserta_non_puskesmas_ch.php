<style>
    .info-column {
        margin: 4px 0;
    }
</style>

<div class="content">
    <div class="card card-table">
        <table id="datatablePesertaNonPuskesmas" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>NIK</th>
                    <th>Tanggal Pemeriksaan</th>
                    <th>Jenis Pemeriksaan</th>
                    <th>Hasil Pemeriksaan</th>
                    <th>Tracking Pemeriksaan</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    let datatablePesertaNonPuskesmas = $("#datatablePesertaNonPuskesmas").DataTable({
        "deferRender": true
    });

    get_peserta();

    function get_peserta() {

        datatablePesertaNonPuskesmas.clear().draw();
        $.ajax({
            url: base_url + 'peserta/request/get_peserta_non_puskesmas_ch',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    datatablePesertaNonPuskesmas.row.add([
                        value.nama,
                        value.nik,
                        value.tanggal_pemeriksaan,
                        value.nama_jenis_pemeriksaan_faskes,
                        "<select class='form-control' name='hasil_pemeriksaan' onchange=\"change_hasil_pemeriksaan_pcr('" + value.id_encrypt + "','" + value.id_trx_pemeriksaan_last_encrypt + "')\"><option value=''>-- Pilih Hasil Pemeriksaan --</option><option " + (value.hasil_pemeriksaan_id_last == '1' ? 'selected' : '') + " value='1'>Positif</option><option " + (value.hasil_pemeriksaan_id_last == '2' ? 'selected' : '') + " value='2'>Negatif</option></select>",
                        "<select class='form-control' name='tracking_pemeriksaan' onchange=\"change_tracking_pemeriksaan('" + value.id_encrypt + "','" + value.id_trx_pemeriksaan_last_encrypt + "')\"><option value=''>-- Pilih Tracking Pemeriksaan --</option><option " + (value.tracking_pemeriksaan_last == '1' ? 'selected' : '') + " value='1'>Belum</option><option " + (value.tracking_pemeriksaan_last == '2' ? 'selected' : '') + " value='2'>Proses</option><option " + (value.tracking_pemeriksaan_last == '3' ? 'selected' : '') + " value='3'>Selesai</option></select>"
                    ]).draw(false);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function change_hasil_pemeriksaan_pcr(id_peserta_non_puskesmas, id_trx_pemeriksaan_non_puskesmas_last) {
        let val_status_pcr = $("select[name='hasil_pemeriksaan']").val();
        console.log(val_status_pcr);
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin mengubah data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/change_hasil_pemeriksaan_pcr_citra_husada',
                    data: {
                        id_peserta_non_puskesmas: id_peserta_non_puskesmas,
                        id_trx_pemeriksaan_non_puskesmas_last: id_trx_pemeriksaan_non_puskesmas_last,
                        val_status_pcr: val_status_pcr
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_peserta();
                            swalInit(
                                'Berhasil',
                                'Data berhasil disimpan',
                                'success'
                            );
                        } else {
                            get_peserta();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa disimpan',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }

    function change_tracking_pemeriksaan(id_peserta_non_puskesmas, id_trx_pemeriksaan_non_puskesmas_last) {
        let val_status_pcr = $("select[name='tracking_pemeriksaan']").val();
        console.log(val_status_pcr);
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin mengubah data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/change_tracking_pemeriksaan_citra_husada',
                    data: {
                        id_peserta_non_puskesmas: id_peserta_non_puskesmas,
                        id_trx_pemeriksaan_non_puskesmas_last: id_trx_pemeriksaan_non_puskesmas_last,
                        val_status_pcr: val_status_pcr
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_peserta();
                            swalInit(
                                'Berhasil',
                                'Data berhasil disimpan',
                                'success'
                            );
                        } else {
                            get_peserta();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa disimpan',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }
</script>