<style>
    .header_tbl {
        width: 15%;
    }

    .header_tbl2 {
        width: 1%;
    }

    .info-column {
        margin: 4px 0;
    }

    #collectionOneHome {
        visibility: hidden;
        width: 100%;
        margin-left: -50%;
        background-color: #fff;
        color: #fff;
        border-radius: 10px 10px 0 0;
        padding: 16px;
        position: fixed;
        z-index: 10000;
        left: 50%;
        bottom: 0px;
        font-size: 17px;
        border-top: 1px solid orange;
    }

    #collectionOneHome.show {
        visibility: visible;
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }

    @-webkit-keyframes fadein {
        from {
            bottom: 0;
            opacity: 0;
        }

        to {
            bottom: 0px;
            opacity: 1;
        }
    }

    @keyframes fadein {
        from {
            bottom: 0;
            opacity: 0;
        }

        to {
            bottom: 0px;
            opacity: 1;
        }
    }
</style>
<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <input type="hidden" name="id_master_wilayah" value="<?php echo !empty($id_master_wilayah) ? $id_master_wilayah : ""; ?>" />
            <input type="hidden" name="rt_domisili" value="<?php echo !empty($rt_domisili) ? $rt_domisili : ""; ?>" />
            <input type="hidden" name="unique_id" />
            <div class="card card-table table-responsive shadow-0 mb-0">
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="header_tbl">Wilayah</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($wilayah) ? $wilayah->klasifikasi . " " . $wilayah->nama_wilayah : ""; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-highlight">
                <li class="nav-item"><a href="#detail_peserta" onclick="get_detail_peserta()" class="nav-link active" data-toggle="tab">Detail Peserta</a></li>
                <li class="nav-item"><a href="#detail_peserta_rumah" onclick="get_data_peserta_per_rumah()" class="nav-link" data-toggle="tab">Detail Peserta / Rumah</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade show active" id="detail_peserta">
                    <div class="card card-table">
                        <table id="datatablePesertaRapid" class="table datatable-save-state table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Alamat Domisili</th>
                                    <th>Tanggal Terkonfirmasi</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="detail_peserta_rumah">
                    <div class="card card-table">
                        <table id="datatablePesertaRapidPerRumah" class="table datatable-save-state table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>UNIQUE ID</th>
                                    <th>Nama Peserta</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalPemeriksaan" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Biodata</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="card card-table table-responsive shadow-0 mb-0">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td class="header_tbl">Nama</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="nama"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">NIK</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="nik"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">Tanggal Lahir</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="tanggal_lahir"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">Jenis Kelamin</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="jenis_kelamin"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">Pekerjaan</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="pekerjaan"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">Alamat KTP</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="alamat_ktp"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">Alamat Domisili</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="alamat_domisili"></span></td>
                            </tr>
                            <tr>
                                <td class="header_tbl">Nomor Telepon</td>
                                <td class="header_tbl2">:</td>
                                <td><span class="nomor_telepon"></span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalTambahPesertaRumah" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Peserta</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="card card-table table-responsive shadow-0 mb-0">
                    <table id="datatablePesertaRumah" class="table datatable-save-state table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Tanggal Terkonfirmasi</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="collectionOneHome">
    <div class="list_peserta">
    </div>
    <div class="text-right mt-1">
        <button type="submit" class="btn btn-danger" onclick="removeCollection()">Batal</button>
        <button type="submit" class="btn btn-warning" onclick="saveCollection()">Simpan</button>
    </div>
</div>

<script>
    let arr_id = [];

    let datatablePesertaRapid = $("#datatablePesertaRapid").DataTable({
        "deferRender": true,
        "ordering": false,
        "paging": false,
        "columns": [{
                "width": "25%"
            },
            null,
            null,
            {
                "width": "15%"
            },
        ]
    });

    let datatablePesertaRumah = $("#datatablePesertaRumah").DataTable({
        "deferRender": true,
        "ordering": false,
        "paging": false,
        "columns": [{
                "width": "75%"
            },
            null,
            null
        ]
    });

    let datatablePesertaRapidPerRumah = $("#datatablePesertaRapidPerRumah").DataTable({
        "deferRender": true,
        "ordering": false,
        "paging": false,
        "columns": [{
                "width": "75%"
            },
            null,
            null
        ]
    });

    get_detail_peserta();

    function get_detail_peserta() {
        let id_master_wilayah = $("input[name='id_master_wilayah']").val();
        let rt_domisili = $("input[name='rt_domisili']").val();

        datatablePesertaRapid.clear().draw();

        $.ajax({
            url: base_url + 'peserta/request/get_list_peserta_polres_kodim',
            data: {
                id_master_wilayah: id_master_wilayah,
                rt_domisili: rt_domisili
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    let str_preg = JSON.stringify(value.nama).replace(/((^")|("$))/g, "").trim();
                    datatablePesertaRapid.row.add([
                        value.nama,
                        value.alamat_domisili,
                        value.tanggal_terkonfirmasi_custom,
                        "<a href='#detailPeserta' onClick=\"show_detail_peserta('" + value.id_encrypt + "')\" class='btn btn-primary btn-icon'><i class='icon-eye'></i></a> " + (value.id_detail_peserta_dalam_satu_rumah ? "<a href='#detailPeserta' class='btn btn-secondary btn-icon'><i class='icon-home'></i></a>" : "<a href='#detailPeserta' onClick=\"collect_home('" + value.id_encrypt + "','" + str_preg + "')\" class='btn btn-success btn-icon'><i class='icon-home'></i></a>")
                    ]).draw(false);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function get_data_peserta_per_rumah() {
        let id_master_wilayah = $("input[name='id_master_wilayah']").val();
        let rt_domisili = $("input[name='rt_domisili']").val();

        datatablePesertaRapidPerRumah.clear().draw();

        $.ajax({
            url: base_url + 'peserta/request/get_list_peserta_polres_kodim_per_rumah',
            data: {
                id_master_wilayah: id_master_wilayah,
                rt_domisili: rt_domisili
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    let id_peserta_spl = value.id_peserta.split("|");
                    let nama_peserta_spl = value.nama_peserta.split("|");

                    let str = "";
                    if (id_peserta_spl) {
                        $.each(id_peserta_spl, function(index_spl, val_spl) {
                            str += "<span class='badge badge-info m-1'><div class='d-inline-flex'><span class='mt-1'>" + nama_peserta_spl[index_spl] + "</span><div><i class='icon-close2 ml-2' onClick=\"confirm_delete('" + id_peserta_spl[index_spl] + "','" + value.unique_id + "')\" style='color:black;'></i></div></div></span>";
                        });
                    }

                    datatablePesertaRapidPerRumah.row.add([
                        value.unique_id,
                        str,
                        "<a href='#detailPeserta' onClick=\"show_peserta_rumah('" + value.unique_id + "')\" class='btn btn-primary btn-icon'><i class='icon-plus-circle2'></i></a>"
                    ]).draw(false);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function show_peserta_rumah(unique_id) {
        let id_master_wilayah = $("input[name='id_master_wilayah']").val();
        let rt_domisili = $("input[name='rt_domisili']").val();

        $("#modalTambahPesertaRumah").modal("show");
        $("input[name='unique_id']").val(unique_id);

        datatablePesertaRumah.clear().draw();

        $.ajax({
            url: base_url + 'peserta/request/get_list_peserta_polres_kodim',
            data: {
                id_master_wilayah: id_master_wilayah,
                rt_domisili: rt_domisili
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    let str_preg = JSON.stringify(value.nama).replace(/((^")|("$))/g, "").trim();
                    datatablePesertaRumah.row.add([
                        value.nama,
                        value.tanggal_terkonfirmasi_custom,
                        (value.id_detail_peserta_dalam_satu_rumah ? "<a href='#detailPeserta' class='btn btn-secondary btn-icon'><i class='icon-home'></i></a>" : "<a href='#detailPeserta' onClick=\"collect_home('" + value.id_encrypt + "','" + str_preg + "')\" class='btn btn-success btn-icon'><i class='icon-home'></i></a>")
                    ]).draw(false);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function confirm_delete(id_peserta, unique_id) {

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/delete_peserta_per_rumah',
                    data: {
                        unique_id: unique_id,
                        id_peserta: id_peserta
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_data_peserta_per_rumah();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_data_peserta_per_rumah();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_data_peserta_per_rumah();
                    }
                });
            }
        });
    }

    function show_detail_peserta(id_peserta) {
        $("#modalPemeriksaan").modal("show");
        $(".nama").html("");
        $(".nik").html("");
        $(".tanggal_lahir").html("");
        $(".jenis_kelamin").html("");
        $(".pekerjaan").html("");
        $(".alamat_ktp").html("");
        $(".alamat_domisili").html("");
        $(".nomor_telepon").html("");
        $.ajax({
            url: base_url + 'peserta/request/get_detail_peserta_tni_polri',
            data: {
                id_peserta: id_peserta
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $(".nama").html(response.nama);
                $(".nik").html(response.nik);
                $(".tanggal_lahir").html(response.tanggal_lahir);
                $(".jenis_kelamin").html(response.jenis_kelamin);
                $(".pekerjaan").html(response.pekerjaan);
                $(".alamat_ktp").html(response.alamat_ktp);
                $(".alamat_domisili").html(response.alamat_domisili);
                $(".nomor_telepon").html(response.telepon);
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function collect_home(id_peserta, nama_peserta) {
        if (!$("#collectionOneHome").hasClass("show")) {
            $("#collectionOneHome").addClass("show");
        }
        if (!arr_id[id_peserta]) {
            arr_id[id_peserta] = '1';
            $(".list_peserta").append("<span class='badge badge-info mr-1'><input type='hidden' name='id_peserta[]' value='" + id_peserta + "' />" + nama_peserta + "</span>");
        }
    }

    function removeCollection() {
        $("#collectionOneHome").removeClass("show");
        $(".list_peserta").html("");
        $("input[name='unique_id']").val("");
        arr_id = [];
    }

    function saveCollection() {
        let id_master_wilayah = $("input[name='id_master_wilayah']").val();
        let unique_id = $("input[name='unique_id']").val();
        let rt_domisili = $("input[name='rt_domisili']").val();
        let id_peserta = $("input[name='id_peserta[]']")
            .map(function() {
                return $(this).val();
            }).get();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menambah data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/save_collection_home',
                    data: {
                        id_master_wilayah: id_master_wilayah,
                        rt_domisili: rt_domisili,
                        unique_id: unique_id,
                        id_peserta: id_peserta
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            $("#collectionOneHome").removeClass("show");
                            $(".list_peserta").html("");
                            $("input[name='unique_id']").val("");
                            arr_id = [];
                            if (!unique_id) {
                                get_detail_peserta();
                            } else {
                                $("#modalTambahPesertaRumah").modal("toggle");
                                get_data_peserta_per_rumah();
                            }
                            swalInit(
                                'Berhasil',
                                'Data berhasil disimpan',
                                'success'
                            );
                        } else {
                            $("#collectionOneHome").removeClass("show");
                            $(".list_peserta").html("");
                            $("input[name='unique_id']").val("");
                            arr_id = [];
                            if (!unique_id) {
                                get_detail_peserta();
                            } else {
                                $("#modalTambahPesertaRumah").modal("toggle");
                                get_data_peserta_per_rumah();
                            }
                            swalInit(
                                'Gagal',
                                'Data tidak bisa disimpan',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }
</script>