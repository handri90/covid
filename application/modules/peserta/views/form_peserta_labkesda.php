<style>
    .calendars {
        width: 350px;
    }

    .ktp-image-custom {
        margin-bottom: 20px;
    }

    .kk-image-custom {
        margin-bottom: 20px;
    }
</style>
<div class="content">
    <!-- Form inputs -->
    <div class="card">
        <div class="card-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-validate-jquery')); ?>
            <fieldset class="mb-3">
                <legend class="text-uppercase font-size-sm font-weight-bold">Peserta Rapid</legend>

                <input type="hidden" name="kel_desa_hidden" value="<?php echo !empty($content) ? encrypt_data($content->kelurahan_master_wilayah_id) : ""; ?>" />
                <input type="hidden" name="id_peserta" value="<?php echo !empty($content) ? encrypt_data($content->id_peserta) : ""; ?>" />
                <input type="hidden" name="level_user_id" value="<?php echo $this->session->userdata("level_user_id"); ?>" />
                <input type="hidden" name="rt_domisili_hidden" value="<?php echo !empty($content) ? $content->master_rt_domisili_id : ""; ?>" />

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nama <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->nama : ""; ?>" name="nama" required placeholder="Nama" oninput="inpueUppercase(event)">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">NIK</label>
                    <div class="col-lg-10">
                        <input type="text" pattern=".{16}" id="nik" title="Field must be 16 characters long" class="form-control" value="<?php echo !empty($content) ? $content->nik : ""; ?>" name="nik" placeholder="NIK">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Tanggal Lahir <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control daterange-single" readonly value="<?php echo !empty($content) ? $content->tanggal_lahir : ""; ?>" name="tanggal_lahir" required placeholder="Tanggal Lahir">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Jenis Kelamin <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-input-styled" name="jenis_kelamin" value="L" <?php echo !empty($content) ? ($content->jenis_kelamin == "L" ? "checked" : "") : ""; ?> required data-fouc>
                                Laki-Laki
                            </label>
                        </div>

                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-input-styled" name="jenis_kelamin" value="P" <?php echo !empty($content) ? ($content->jenis_kelamin == "P" ? "checked" : "") : ""; ?> required data-fouc>
                                Perempuan
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Pekerjaan <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <select class="form-control select-search" name="pekerjaan_inti" required onchange="show_input_other_pekerjaan()">
                            <option value="">-- Pilih Pekerjaan --</option>
                            <?php
                            foreach ($pekerjaan as $key => $row) {
                                $selected = "";
                                if (!empty($content)) {
                                    if ($row == $content->pekerjaan_inti) {
                                        $selected = 'selected="selected"';
                                    }
                                }
                            ?>
                                <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $row; ?></option>
                            <?php
                            }
                            ?>
                        </select>

                        <div class="form-check form-check-inline">
                            <span class="place_other_pekerjaan">
                                <?php
                                if (!empty($content)) {
                                    if (in_array($content->pekerjaan_inti, array("Swasta", "Wiraswasta", "Lainnya"))) {
                                ?>
                                        <input value="<?php echo !empty($content) ? $content->pekerjaan_inti_lainnya : ""; ?>" type='text' class='form-control' name='pekerjaan_lainnya' required />
                                <?php
                                    }
                                }
                                ?>
                            </span>
                        </div>
                    </div>
                </div>

                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Alamat KTP</legend>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Alamat</label>
                        <div class="col-lg-10">
                            <textarea class="form-control" name="alamat_ktp" placeholder="Alamat"><?php echo !empty($content) ? $content->alamat_ktp : ""; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">RT</label>
                        <div class="col-lg-2">
                            <input type="text" id="rt_ktp" class="form-control" value="<?php echo !empty($content) ? $content->rt_ktp : ""; ?>" name="rt_ktp" placeholder="RT">
                        </div>
                    </div>
                </fieldset>

                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Alamat Domisili
                        (<label for="custom_checkbox_stacked_unchecked">Sama dengan alamat KTP</label> <input type="checkbox" id="custom_checkbox_stacked_unchecked" name='copy_ktp' value='yes' onclick="copy_alamat_ktp()" <?php echo !empty($content) ? ($content->is_copy_ktp == "1" ? "checked" : "") : ""; ?>>) / (<label for="custom_checkbox_stacked_unchecked_luar_kobar"><span class="text-danger">Wilayah Luar Kobar</span></label> <input type="checkbox" id="custom_checkbox_stacked_unchecked_luar_kobar" name='luar_kobar' value='yes' onclick="luar_kobar_fun()" <?php echo !empty($content) ? ($content->kelurahan_master_wilayah_id == "109" ? "checked" : "") : ""; ?>>)
                    </legend>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Alamat <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <textarea class="form-control" name="alamat_domisili" required placeholder="Alamat" <?php echo !empty($content) ? ($content->is_copy_ktp == "1" || $content->kelurahan_master_wilayah_id == "109" ? "readonly" : "") : ""; ?>><?php echo !empty($content) ? $content->alamat_domisili : ""; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Kelurahan/Desa <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <select <?php echo !empty($content) ? ($content->kelurahan_master_wilayah_id == "109" ? "disabled" : "") : ""; ?> class="form-control select-search" name="kelurahan_desa" required onchange="get_kecamatan_kabupaten_provinsi()">
                                <option value="">-- Pilih Kelurahan/Desa --</option>
                                <?php
                                foreach ($master_wilayah as $key => $row) {
                                    $selected = "";
                                    if (!empty($content)) {
                                        if ($row->id_master_wilayah == $content->kelurahan_master_wilayah_id) {
                                            $selected = 'selected="selected"';
                                        }
                                    }
                                ?>
                                    <option <?php echo $selected; ?> value="<?php echo encrypt_data($row->id_master_wilayah); ?>"><?php echo ucwords(strtolower($row->nama_wilayah)); ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">RT / Afdeling<span class="text-danger">*</span></label>
                        <div class="col-lg-2">
                            <!-- !empty($content) ? $content->rt_domisili : ""; -->

                            <select <?php echo !empty($content) ? ($content->kelurahan_master_wilayah_id == "109" ? "disabled" : "") : ""; ?> class="form-control select-search" name="rt_domisili" id="rt_domisili" required>
                                <option value="">-- Pilih RT --</option>
                            </select>
                        </div>
                        <div class="col-lg-2"><?php echo !empty($content) ? (isset($content->rt_domisili) && $content->rt_domisili != "" ? "<h3>(" . $content->rt_domisili . ")</h3>" : "") : ""; ?></div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Kecamatan <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <span class="nama_kecamatan"></span>
                        </div>
                    </div>
                </fieldset>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nomor Telepon <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" id="nomor_telepon" class="form-control" value="<?php echo !empty($content) ? $content->nomor_telepon : ""; ?>" name="nomor_telepon" required placeholder="Nomor Telepon">
                    </div>
                </div>

                <?php if (empty($content)) { ?>
                    <fieldset class="mb-3">
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Tanggal Pemeriksaan <span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control daterange-single" readonly name="tanggal_pemeriksaan" required placeholder="Tanggal Pemeriksaan">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Jenis Pemeriksaan <span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <select class="form-control select-search" name="jenis_pemeriksaan" onchange="get_hasil_pemeriksaan()" required>
                                    <option data-id="" value="">-- Pilih Jenis Pemeriksaan --</option>
                                    <?php
                                    foreach ($jenis_pemeriksaan as $key => $row) {
                                    ?>
                                        <option data-id="<?php echo $row->id_jenis_pemeriksaan; ?>" value="<?php echo encrypt_data($row->id_jenis_pemeriksaan); ?>"><?php echo $row->nama_jenis_pemeriksaan; ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row is_show_base_jenis_spesimen">
                            <label class="col-form-label col-lg-2">Jenis Spesimen <span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <select class="form-control select-search" name="jenis_spesimen">
                                    <option value="">-- Pilih Jenis Spesimen --</option>
                                    <option value="1">Nasofaring</option>
                                    <option value="2">Orofaring</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row is_show_base_jenis_pemeriksaan">
                            <label class="col-form-label col-lg-2">Hasil Pemeriksaan</label>
                            <div class="col-lg-10">
                                <select class="form-control select-search" name="hasil_pemeriksaan">
                                    <option value="">-- Pilih Hasil Pemeriksaan --</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row is_show_base_jenis_pemeriksaan">
                            <label class="col-form-label col-lg-2">Kode Sample <span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="kode_sample" placeholder="Kode Sample">
                            </div>
                        </div>
                    </fieldset>
                <?php } ?>
            </fieldset>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <!-- /form inputs -->
</div>

<script>
    $(".is_show_base_jenis_pemeriksaan").hide();
    $(".is_show_base_jenis_spesimen").hide();

    $('.daterange-single').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD/MM/YYYY'
        },
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10)
    });

    $('.form-input-styled').uniform({
        fileButtonClass: 'action btn bg-pink-400'
    });

    function delete_input_other_pekerjaan() {
        $(".place_other_pekerjaan").html("");
    }

    function show_input_other_pekerjaan() {
        let pekerjaan_inti = $("select[name='pekerjaan_inti']").val();
        let arr = ["10", "11", "13"];
        if (arr.includes(pekerjaan_inti)) {
            $(".place_other_pekerjaan").html("<input type='text' class='form-control' name='pekerjaan_lainnya' required />");
        } else {
            $(".place_other_pekerjaan").html("");
        }
    }

    get_kecamatan_kabupaten_provinsi();

    function get_kecamatan_kabupaten_provinsi() {
        let kel_desa = $("select[name=kelurahan_desa]").val();
        let id_master_rt_edit = $("input[name=rt_domisili_hidden]").val();

        if (!kel_desa) {
            kel_desa = $("input[name=kel_desa_hidden]").val();
        }

        if (kel_desa) {
            $.ajax({
                url: base_url + 'peserta/request/get_kecamatan_kabupaten_provinsi',
                data: {
                    kel_desa: kel_desa
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $(".nama_kecamatan").html(response.kecamatan);
                    $(".nama_kabupaten").html(response.kabupaten);
                    $(".nama_provinsi").html(response.provinsi);
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });

            $.ajax({
                url: base_url + 'peserta/request/get_master_rt',
                data: {
                    kel_desa: kel_desa
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    let html = "<option value=''>-- Pilih RT --</option>";
                    $.each(response, function(index, value) {
                        let selected = "";
                        if (id_master_rt_edit) {
                            if (value.id_master_rt == id_master_rt_edit) {
                                selected = "selected";
                            }
                        }
                        html += "<option " + selected + " value='" + value.id_encrypt + "'>" + value.rt + "</option>";
                    });
                    $("select[name='rt_domisili']").html(html);
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        } else {
            $(".nama_kecamatan").html("");
            $("select[name='rt_domisili']").html("<option value=''>-- Pilih RT --</option>");
        }
    }

    function copy_alamat_ktp() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let alamat_ktp = $("textarea[name='alamat_ktp']").val();
        let rt_ktp = $("input[name='rt_ktp']").val();

        if (!$('#custom_checkbox_stacked_unchecked').is(":checked")) {
            $("textarea[name='alamat_domisili']").val("");
            $("textarea[name='alamat_domisili']").attr("readonly", false);
        } else {
            if (alamat_ktp == "") {
                swalInit(
                    'Gagal',
                    'Alamat KTP harus diisi',
                    'error'
                );

                $('#custom_checkbox_stacked_unchecked').prop('checked', false);
            } else {
                $("textarea[name='alamat_domisili']").val(alamat_ktp);
                $("textarea[name='alamat_domisili']").attr("readonly", true);
            }
        }
    }

    function get_hasil_pemeriksaan() {
        $(".is_show_base_jenis_spesimen").hide();
        let id_jenis_pemeriksaan = $("select[name='jenis_pemeriksaan']").val();
        let level_user_id = $("input[name='level_user_id']").val();

        let data_id_jenis_pemeriksaan = $("select[name='jenis_pemeriksaan']").find("option:selected").attr("data-id");

        $("select[name='hasil_pemeriksaan']").attr("disabled", false);

        if (data_id_jenis_pemeriksaan == "1") {
            $("select[name='hasil_pemeriksaan']").attr("disabled", true);
            $(".is_show_base_jenis_spesimen").show();
            $("select[name='jenis_spesimen']").attr("required", true);
        } else {
            $("select[name='jenis_spesimen']").attr("required", false);
        }

        let html = "<option value=''>-- Pilih Hasil Pemeriksaan --</option>"
        if (id_jenis_pemeriksaan) {
            $(".is_show_base_jenis_pemeriksaan").show();
            $("input[name='kode_sample']").attr("required", true);

            $.ajax({
                url: base_url + 'peserta/request/get_hasil_pemeriksaan',
                data: {
                    id_jenis_pemeriksaan: id_jenis_pemeriksaan
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $.each(response, function(index, value) {
                        html += "<option value='" + value.id_encrypt + "'>" + value.nama_hasil_pemeriksaan + "</option>";
                    });
                    $("select[name='hasil_pemeriksaan']").html(html);
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        } else {
            $(".is_show_base_jenis_pemeriksaan").hide();
            $("input[name='kode_sample']").attr("required", false);
            $("select[name='hasil_pemeriksaan']").html(html);
        }

    }

    function inpueUppercase(e) {
        var ss = e.target.selectionStart;
        var se = e.target.selectionEnd;
        e.target.value = e.target.value.toUpperCase();
        e.target.selectionStart = ss;
        e.target.selectionEnd = se;
    }

    setInputFilter(document.getElementById("nik"), function(value) {
        return /^-?\d*$/.test(value);
    });
    setInputFilter(document.getElementById("rt_ktp"), function(value) {
        return /^-?\d*$/.test(value);
    });
    setInputFilter(document.getElementById("rt_domisili"), function(value) {
        return /^-?\d*$/.test(value);
    });
    setInputFilter(document.getElementById("nomor_telepon"), function(value) {
        return /^-?\d*$/.test(value);
    });

    function luar_kobar_fun() {
        if ($("input[name='luar_kobar']").is(":checked")) {
            $("textarea[name='alamat_domisili']").val("");
            $("textarea[name='alamat_domisili']").attr("readonly", true);
            $("select[name='rt_domisili']").prop("selectedIndex", 0).change();
            $("select[name='rt_domisili']").prop("disabled", true);
            $("select[name='kelurahan_desa']").prop("selectedIndex", 0).change();
            $("select[name='kelurahan_desa']").prop("disabled", true);
            $(".nama_kecamatan").html("");
            $(".nama_kabupaten").html("");
            $(".nama_provinsi").html("");
        } else {
            $("textarea[name='alamat_domisili']").attr("readonly", false);
            $("select[name='rt_domisili']").prop("disabled", false);
            $("select[name='kelurahan_desa']").prop("disabled", false);
        }
    }
</script>