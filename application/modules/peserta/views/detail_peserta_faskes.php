<style>
    .header_tbl {
        width: 15%;
    }

    .header_tbl2 {
        width: 1%;
    }

    .info-column {
        margin: 4px 0;
    }
</style>
<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <input type="hidden" name="id_peserta" value="<?php echo !empty($id_peserta) ? $id_peserta : ""; ?>" />
            <div class="card card-table table-responsive shadow-0 mb-0">
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="header_tbl">Nama</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->nama : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">NIK</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->nik : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Tanggal Lahir</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->tanggal_lahir : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Jenis Kelamin</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->jenis_kelamin : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Pekerjaan</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->pekerjaan_inti : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Alamat KTP</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->alamat_ktp : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Alamat Domisili</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->alamat_domisili : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Nomor Telepon</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->nomor_telepon : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Kontak Erat</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->kontak_erat : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Petugas Input</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->nama_user : ""; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="card card-table table-responsive">
                <div class="card-body">
                    <div class="text-right">
                        <a href="#tambahPemeriksaan" id="tambahPemeriksaan" class="btn btn-info">Tambah Pemeriksaan</a>
                        <a href="#" id="sembuh" onclick="confirm_sembuh()" class="btn btn-success">Sembuh</a>
                        <a href="#" id="bebasisoma" onclick="confirm_bebas_isoma()" class="btn btn-success">Bebas Isoma</a>
                        <a href="#" id="meninggal" onclick="confirm_meninggal()" class="btn btn-dark">Meninggal</a>
                        <a href="#" id="probable" onclick="confirm_probable()" class="btn btn-secondary">Probable</a>
                        <a href="#" id="bebasisomasuspek" onclick="confirm_bebas_isoma_suspek()" class="btn btn-success">Bebas Isoma Suspek</a>
                    </div>
                </div>
                <span class="card-table-detail"></span>
            </div>
        </div>
    </div>
</div>

<div id="modalPemeriksaan" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Pemeriksaan</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <span class="alert_form"></span>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Tanggal/Waktu <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="hidden" class="form-control" name="id_trx_pemeriksaan">
                        <input type="text" class="form-control" name="tanggal_waktu" readonly placeholder="Tanggal/Waktu">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Jenis Pemeriksaan</label>
                    <div class="col-lg-9">
                        <select class="form-control select-search" name="jenis_pemeriksaan" onchange="get_hasil_pemeriksaan()">
                            <option value="">-- Pilih Jenis Pemeriksaan --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row is_show_hasil_pemeriksaan">
                    <label class="col-form-label col-lg-3">Hasil Pemeriksaan <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <select class="form-control select-search" name="hasil_pemeriksaan" required onchange="is_show_usulan_faskes()">
                            <option data-id='' value="">-- Pilih Hasil Pemeriksaan --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row is_show_usulan_faskes">
                    <label class="col-form-label col-lg-3">Usulan Faskes <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <span class="list-usulan-faskes"></span>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary" onclick="action_form_pemeriksaan()">Simpan <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(".is_show_hasil_pemeriksaan").hide();
    $("#bebasisoma").hide();
    $("#bebasisomasuspek").hide();

    get_detail_peserta();

    function get_detail_peserta() {
        let id_peserta = $("input[name='id_peserta']").val();

        $.ajax({
            url: base_url + 'peserta/request/get_detail_peserta',
            data: {
                id_peserta: id_peserta
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let html = "<table class='table'>";
                html += "<tr><th>Tanggal</th><th>Jam</th><th>Kode Sample</th><th>Jenis</th><th>Hasil</th><th>User Verifikasi</th><th>Action</th></tr>";
                $.each(response, function(index, value) {
                    html += "<tr>";
                    html += "<td>" + value.tanggal_pemeriksaan + "</td>";
                    html += "<td>" + value.jam_pemeriksaan + "</td>";
                    html += "<td>" + (value.kode_sample ? value.kode_sample : "") + "</td>";
                    html += "<td>" + (value.nama_jenis_pemeriksaan ? value.nama_jenis_pemeriksaan : "") + "</td>";
                    html += "<td>" + (value.id_trx_pemeriksaan ? (value.nama_hasil_pemeriksaan ? "<div class='info-column'>" + (value.class_badge == '1' ? "<span class='badge badge-success'>" + value.nama_hasil_pemeriksaan + "</span> " : (value.class_badge == '2' ? "<span class='badge badge-warning'>" + value.nama_hasil_pemeriksaan + "</span> " : "")) + "</div><div class='info-column'>" + (value.usulan_faskes == "0" ? "<span class='badge badge-danger'>Usulan Faskes : SWAB PCR</span>" : (value.usulan_faskes == "1" ? "<span class='badge badge-danger'>Usulan Faskes : Isolasi Mandiri</span>" : (value.usulan_faskes == "2" ? "<span class='badge badge-success'>Bebas Isolasi Mandiri</span>" : (value.usulan_faskes == "3" ? "<span class='badge badge-warning'>Usulan Faskes : Rujuk Rumah Sakit</span>" : "")))) + "</div>" : (value.nama_jenis_pemeriksaan ? "<div class='info-column'><span class='badge badge-secondary'>Hasil belum keluar</span></div>" : "") + "<div class='info-column'>" + (value.usulan_faskes == "0" ? "<span class='badge badge-danger'>Usulan Faskes : SWAB PCR</span>" : (value.usulan_faskes == "1" ? "<span class='badge badge-danger'>Usulan Faskes : Isolasi Mandiri</span>" : (value.usulan_faskes == "2" ? "<span class='badge badge-success'>Bebas Isolasi Mandiri</span>" : (value.usulan_faskes == "3" ? "<span class='badge badge-warning'>Usulan Faskes : Rujuk Rumah Sakit</span>" : (value.is_sembuh ? "<span class='badge badge-success'>Sembuh</span>" : (value.is_meninggal == "1" ? "<span class='badge badge-dark'>Meninggal</span>" : (value.is_probable == "1" ? "<span class='badge badge-dark'>Probable</span>" : ""))))))) + "</div>") + (value.status_rawat ? "<div class='mt-2'><span class='badge badge-light badge-striped badge-striped-left border-left-violet'>" + (value.status_rawat == '1' ? 'Rawat Inap' : (value.status_rawat == '2' ? 'Rawat Jalan' : (value.status_rawat == '3' ? 'Bebas Isolasi Mandiri' : ''))) + "</span></div>" : "") : (value.status_terkonfirmasi == "Positif" ? "<span class='badge badge-danger'>" + value.status_terkonfirmasi + "</span>" : (value.status_terkonfirmasi == "Sembuh" ? "<span class='badge badge-info'>" + value.status_terkonfirmasi + "</span>" : (value.status_terkonfirmasi == "Meninggal" ? "<div class='info-column'><span class='badge badge-info'>" + value.status_terkonfirmasi + "</span>" : "")))) + (value.user_non_puskes ? "<span class='badge badge-light badge-striped badge-striped-left border-left-violet'>" + value.user_non_puskes + "</span></div>" : "") + "</td>";

                    html += "<td>" + (value.user_update != "" ? value.user_update : value.user_create) + "</td>";
                    html += "<td>" + (value.is_edit ? "<a href='#editDetailPeserta' onClick=\"show_detail_trx_peserta('" + value.id_encrypt + "')\" class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a>" : "") + (value.is_delete ? " <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>" : "") + "</td>";
                    html += "</tr>";
                });

                html += "</table>";
                $(".card-table-detail").html(html);
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function confirm_delete(id_pemeriksaan) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/delete_pemeriksaan',
                    data: {
                        id_pemeriksaan: id_pemeriksaan
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_detail_peserta();
                    }
                });
            }
        });
    }

    function get_jenis_pemeriksaan(jenis_pemeriksaan_id) {

        let html = "<option value=''>-- Pilih Jenis Pemeriksaan --</option>"
        $.ajax({
            url: base_url + 'peserta/request/get_jenis_pemeriksaan',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                if (response) {
                    let selected = "";
                    $.each(response, function(index, value) {
                        if (value.id_jenis_pemeriksaan == jenis_pemeriksaan_id) {
                            selected = "selected";
                        } else {
                            selected = "";
                        }
                        html += "<option " + selected + " value='" + value.id_encrypt + "'>" + value.nama_jenis_pemeriksaan + "</option>";
                    });
                    $("select[name='jenis_pemeriksaan']").html(html);
                } else {
                    $("select[name='jenis_pemeriksaan']").html(html);
                }
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function get_hasil_pemeriksaan(jenis_pemeriksaan_id_encrypt, hasil_pemeriksaan_id) {
        $(".is_show_hasil_pemeriksaan").hide();

        let id_jenis_pemeriksaan = $("select[name='jenis_pemeriksaan']").val();

        if (jenis_pemeriksaan_id_encrypt) {
            id_jenis_pemeriksaan = jenis_pemeriksaan_id_encrypt;
        }

        let html = "<option data-id='' value=''>-- Pilih Hasil Pemeriksaan --</option>"
        if (id_jenis_pemeriksaan) {
            $(".is_show_hasil_pemeriksaan").show();

            $.ajax({
                url: base_url + 'peserta/request/get_hasil_pemeriksaan',
                data: {
                    id_jenis_pemeriksaan: id_jenis_pemeriksaan
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    let selected = "";
                    $.each(response, function(index, value) {
                        if (value.id_hasil_pemeriksaan == hasil_pemeriksaan_id) {
                            selected = "selected";
                        } else {
                            selected = "";
                        }
                        html += "<option data-id='" + value.class_badge + "' " + selected + " value = '" + value.id_encrypt + "' > " + value.nama_hasil_pemeriksaan + " </option>";
                    });
                    $("select[name='hasil_pemeriksaan']").html(html);
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        } else {
            $("select[name='hasil_pemeriksaan']").html(html);
            $(".is_show_hasil_pemeriksaan").hide();
        }
    }

    function action_form_pemeriksaan() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let id_peserta = $("input[name='id_peserta']").val();
        let id_trx_pemeriksaan = $("input[name='id_trx_pemeriksaan']").val();
        let jenis_pemeriksaan = $("select[name='jenis_pemeriksaan']").val();
        let hasil_pemeriksaan = $("select[name='hasil_pemeriksaan']").val();
        let usulan_faskes = $("input[name='usulan_faskes']:checked").val();
        let class_badge = $("select[name='hasil_pemeriksaan']").find("option:selected").attr("data-id");

        if (jenis_pemeriksaan && !hasil_pemeriksaan) {
            $(".alert_form").html("<div class='alert alert-danger'>Hasil Pemeriksaan tidak boleh kosong.</div>");
        } else if (!jenis_pemeriksaan && typeof(usulan_faskes) === 'undefined') {
            $(".alert_form").html("<div class='alert alert-danger'>Usulan Faskes tidak boleh kosong.</div>");
        } else if (jenis_pemeriksaan && class_badge == "2" && typeof(usulan_faskes) === 'undefined') {
            $(".alert_form").html("<div class='alert alert-danger'>Usulan Faskes tidak boleh kosong.</div>");
        } else {
            $(".alert_form").html("");
            $.ajax({
                url: base_url + 'peserta/action_form_pemeriksaan',
                data: {
                    id_trx_pemeriksaan: id_trx_pemeriksaan,
                    id_peserta: id_peserta,
                    jenis_pemeriksaan: jenis_pemeriksaan,
                    hasil_pemeriksaan: hasil_pemeriksaan,
                    usulan_faskes: usulan_faskes
                },
                type: 'POST',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    if (response) {
                        $("#modalPemeriksaan").modal("toggle");
                        get_detail_peserta();
                        cek_button_status();
                        swalInit(
                            'Berhasil',
                            'Data berhasil ditambahkan',
                            'success'
                        );
                    } else {
                        $("#modalPemeriksaan").modal("toggle");
                        get_detail_peserta();
                        cek_button_status();
                        swalInit(
                            'Gagal',
                            'Data tidak bisa ditambahkan',
                            'error'
                        );
                    }
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }

    function show_detail_trx_peserta(id_trx_pemeriksaan) {
        $("input[name='id_trx_pemeriksaan']").val("");
        $("select[name='jenis_pemeriksaan']").html("");
        $("select[name='hasil_pemeriksaan']").html("");
        $.ajax({
            url: base_url + 'peserta/request/get_detail_trx_peserta',
            data: {
                id_trx_pemeriksaan: id_trx_pemeriksaan
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $("#modalPemeriksaan").modal("show");
                $(".title_modal").html("Ubah");
                $("input[name='id_trx_pemeriksaan']").val(id_trx_pemeriksaan);
                get_jenis_pemeriksaan(response.jenis_pemeriksaan_id);
                if (response.hasil_pemeriksaan_id) {
                    get_hasil_pemeriksaan(response.jenis_pemeriksaan_id_encrypt, response.hasil_pemeriksaan_id);
                } else {
                    $(".is_show_hasil_pemeriksaan").hide();
                }
                $("input[name='tanggal_waktu']").val(response.tanggal_format);
                $("input[name='kode_sample']").val(response.kode_sample);
                is_show_usulan_faskes(response.class_badge);
                show_list_usulan_faskes(response.usulan_faskes);
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    $(function() {
        $("#tambahPemeriksaan").on("click", function() {
            $("input[name='id_trx_pemeriksaan']").val("");
            $("select[name='jenis_pemeriksaan']").html("");
            $("select[name='hasil_pemeriksaan']").html("");

            var swalInit = swal.mixin({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-light'
            });

            let id_peserta = $("input[name='id_peserta']").val();

            $.ajax({
                url: base_url + 'peserta/request/cek_kondisi_pemeriksaan_terakhir',
                data: {
                    id_peserta: id_peserta
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    if (response) {
                        let dateJS = new Date();
                        $("#modalPemeriksaan").modal("show");
                        $(".title_modal").html("Tambah");
                        $("select[name='jenis_pemeriksaan']").html("");
                        get_jenis_pemeriksaan();
                        $("select[name='hasil_pemeriksaan']").html("<option value=''>-- Pilih Hasil Pemeriksaan --</option>");
                        $("input[name='id_trx_pemeriksaan']").val("");
                        $("input[name='tanggal_waktu']").val(moment().format('DD-MM-YYYY H:m:s'));
                        is_show_usulan_faskes();
                        show_list_usulan_faskes();
                        $(".is_show_hasil_pemeriksaan").hide();
                    } else {
                        swalInit(
                            'Gagal',
                            'Tidak bisa menambahkan pemeriksaan baru. Hasil pemeriksaan terakhir belum keluar',
                            'error'
                        );
                    }
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        });
    })

    function is_show_usulan_faskes(class_badge_parse = "") {
        let class_badge = $("select[name='hasil_pemeriksaan']").find("option:selected").attr("data-id");
        if (class_badge_parse) {
            class_badge = class_badge_parse;
        }

        $(".is_show_usulan_faskes").show();
        $("input[name='usulan_faskes']").attr("required", true);
        if (class_badge == "2") {
            $(".is_show_usulan_faskes").show();
            $("input[name='usulan_faskes']").attr("required", true);
        } else if (class_badge == "1" || class_badge == "0") {
            $(".is_show_usulan_faskes").hide();
            $("input[name='usulan_faskes']").attr("required", false);
        }
    }

    function show_list_usulan_faskes(usulan_faskes_db) {
        let usulan_faskes = [];
        usulan_faskes[0] = "SWAB PCR";
        usulan_faskes[1] = "Isolasi Mandiri";
        usulan_faskes[3] = "Rujuk Rumah Sakit";
        let html = "";
        $.each(usulan_faskes, function(index, value) {
            if (index != '2') {
                let checked = "";
                if (index == usulan_faskes_db) {
                    checked = "checked";
                }
                html += "<div class='form-check form-check-inline'>" +
                    "<label class='form-check-label'>" +
                    "<input " + checked + " type='radio' class='form-input-styled' name='usulan_faskes' value='" + index + "' required>" +
                    value +
                    "</label>" +
                    "</div>";
            }
        });

        $(".list-usulan-faskes").html(html);
        $('.form-input-styled').uniform({
            fileButtonClass: 'action btn bg-pink-400'
        });
    }

    function confirm_sembuh() {
        let id_peserta = $("input[name='id_peserta']").val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menambah status sembuh?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/status_sembuh',
                    data: {
                        id_peserta: id_peserta
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Berhasil',
                                'Data berhasil ditambahkan',
                                'success'
                            );
                        } else {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Gagal',
                                'Data gagal ditambahkan',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }

    function confirm_bebas_isoma() {
        let id_peserta = $("input[name='id_peserta']").val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menambah status Bebas Isolasi Mandiri?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/status_bebas_isoma',
                    data: {
                        id_peserta: id_peserta
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Berhasil',
                                'Data berhasil ditambahkan',
                                'success'
                            );
                        } else {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Gagal',
                                'Data gagal ditambahkan',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }

    function confirm_meninggal() {
        let id_peserta = $("input[name='id_peserta']").val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menambah status meninggal?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/status_meninggal',
                    data: {
                        id_peserta: id_peserta
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Berhasil',
                                'Data berhasil ditambahkan',
                                'success'
                            );
                        } else {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Gagal',
                                'Data gagal ditambahkan',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }

    function confirm_probable() {
        let id_peserta = $("input[name='id_peserta']").val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menambah status probable?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/status_probable',
                    data: {
                        id_peserta: id_peserta
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Berhasil',
                                'Data berhasil ditambahkan',
                                'success'
                            );
                        } else {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Gagal',
                                'Data gagal ditambahkan',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }

    function confirm_bebas_isoma_suspek() {
        let id_peserta = $("input[name='id_peserta']").val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menambah status Bebas Isolasi Mandiri Suspek?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/status_bebas_isolasi_mandiri_suspek',
                    data: {
                        id_peserta: id_peserta
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Berhasil',
                                'Data berhasil ditambahkan',
                                'success'
                            );
                        } else {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Gagal',
                                'Data gagal ditambahkan',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }

    $(function() {
        cek_button_status();
    });

    function cek_button_status() {
        let id_peserta = $("input[name='id_peserta']").val();

        $.ajax({
            url: base_url + 'peserta/request/check_stat_pemeriksaan_last',
            data: {
                id_peserta: id_peserta
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                if (response.jenis_pemeriksaan_id == '1' && response.hasil_pemeriksaan_id == '1' && (response.status_rawat == '1' || response.status_rawat == null)) {

                    $("#bebasisoma").hide();
                    $("#bebasisomasuspek").hide();
                } else if (response.jenis_pemeriksaan_id == '1' && response.hasil_pemeriksaan_id == '1' && response.status_rawat == '2') {
                    $("#bebasisoma").show();
                    $("#bebasisomasuspek").hide();
                } else if (response.is_sembuh == '1') {
                    $("#bebasisoma").hide();
                    $("#bebasisomasuspek").hide();
                } else if (response.is_meninggal == '3') {
                    $("#bebasisoma").hide();
                    $("#bebasisomasuspek").hide();
                } else if (response.is_probable == '4') {
                    $("#bebasisoma").hide();
                    $("#bebasisomasuspek").hide();
                } else if (response.usulan_faskes == '1') {
                    $("#bebasisoma").hide();
                    $("#bebasisomasuspek").show();
                }
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }
</script>