<style>
    .header_tbl {
        width: 15%;
    }

    .header_tbl2 {
        width: 1%;
    }

    .info-column {
        margin: 4px 0;
    }
</style>
<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <input type="hidden" name="id_peserta" value="<?php echo !empty($id_peserta) ? $id_peserta : ""; ?>" />
            <input type="hidden" name="level_user_id" value="<?php echo $this->session->userdata("level_user_id"); ?>" />
            <div class="card card-table table-responsive shadow-0 mb-0">
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="header_tbl">Nomor Rekam Medis</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->no_rekam_medis : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Nama</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->nama : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">NIK</td>
                            <td class="header_tbl2">:</td>
                            <td>
                                <?php echo isset($content) ? ($content->nik ? $content->nik : "<button onclick='tambah_nik()' type='button' class='btn bg-teal-400 btn-labeled btn-labeled-left'><b><i class='icon-make-group mr-2'></i></b> Tambah NIK</button>") : ""; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Tanggal Lahir</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->tanggal_lahir : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Jenis Kelamin</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->jenis_kelamin : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Pekerjaan</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->pekerjaan_inti : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Alamat KTP</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->alamat_ktp : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Alamat Domisili</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->alamat_domisili : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Nomor Telepon</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->nomor_telepon : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Kategori Data</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? ($content->kategori_data ? ($content->kategori_data == '1' ? "Mandiri" : ($content->kategori_data == '2' ? "Non Mandiri" : "")) : "") : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Petugas Input</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->nama_user : ""; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="card card-table">
                <div class="card-body">
                    <div class="text-right">
                        <a href="#" id="tambahSembuh" onclick="confirm_sembuh()" class="btn btn-success">Sembuh</a>
                        <a href="#" id="meninggal" onclick="confirm_meninggal()" class="btn btn-dark">Meninggal</a>
                        <a href="#" id="probable" onclick="confirm_probable()" class="btn btn-secondary">Probable</a>
                    </div>
                </div>
                <span class="card-table-detail"></span>
            </div>
        </div>
    </div>
</div>

<div id="modalPemeriksaan" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Pemeriksaan</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <span class="alert_form"></span>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Tanggal/Waktu <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="hidden" class="form-control" name="id_trx_pemeriksaan">
                        <input type="text" class="form-control" name="tanggal_waktu" readonly placeholder="Tanggal/Waktu">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Jenis Pemeriksaan <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <select class="form-control select-search" name="jenis_pemeriksaan" onchange="get_hasil_pemeriksaan()" required>
                            <option data-id='' value="">-- Pilih Jenis Pemeriksaan --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Hasil Pemeriksaan</label>
                    <div class="col-lg-9">
                        <select class="form-control select-search" name="hasil_pemeriksaan" required>
                            <option value="">-- Pilih Hasil Pemeriksaan --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Kode Sample <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="kode_sample" placeholder="Kode Sample" required>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary" onclick="action_form_pemeriksaan()">Simpan <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalNik" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Tambah NIK</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <span class="alert_form_nik"></span>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">NIK <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="number" class="form-control" name="nik" placeholder="NIK" required>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary" onclick="action_form_nik()">Simpan <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#tambahSembuh").hide();
    $("#meninggal").hide();

    get_detail_peserta();

    function get_detail_peserta() {
        let id_peserta = $("input[name='id_peserta']").val();

        $.ajax({
            url: base_url + 'peserta/request/get_detail_peserta',
            data: {
                id_peserta: id_peserta
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let html = "<table class='table'>";
                html += "<tr><th>Tanggal</th><th>Jam</th><th>Kode Sample</th><th>Jenis</th><th>Hasil</th><th>User Verifikasi</th><th>Action</th></tr>";
                $.each(response, function(index, value) {
                    html += "<tr>";
                    html += "<td>" + value.tanggal_pemeriksaan + "</td>";
                    html += "<td>" + value.jam_pemeriksaan + "</td>";
                    html += "<td>" + (value.kode_sample ? value.kode_sample : "") + "</td>";
                    html += "<td>" + (value.nama_jenis_pemeriksaan ? value.nama_jenis_pemeriksaan : "") + "</td>";
                    html += "<td>" + (value.id_trx_pemeriksaan ? (value.nama_hasil_pemeriksaan ? "<div class='info-column'>" + (value.class_badge == '1' ? "<span class='badge badge-success'>" + value.nama_hasil_pemeriksaan + "</span> " : (value.class_badge == '2' ? "<span class='badge badge-warning'>" + value.nama_hasil_pemeriksaan + "</span> " : "")) + "</div><div class='info-column'>" + (value.usulan_faskes == "0" ? "<span class='badge badge-danger'>Usulan Faskes : SWAB PCR</span>" : (value.usulan_faskes == "1" ? "<span class='badge badge-danger'>Usulan Faskes : Isolasi Mandiri</span>" : (value.usulan_faskes == "2" ? "<span class='badge badge-success'>Bebas Isolasi Mandiri</span>" : (value.usulan_faskes == "3" ? "<span class='badge badge-warning'>Usulan Faskes : Rujuk Rumah Sakit</span>" : "")))) + "</div>" : (value.nama_jenis_pemeriksaan ? "<div class='info-column'><span class='badge badge-secondary'>Hasil belum keluar</span></div>" : "") + "<div class='info-column'>" + (value.usulan_faskes == "0" ? "<span class='badge badge-danger'>Usulan Faskes : SWAB PCR</span>" : (value.usulan_faskes == "1" ? "<span class='badge badge-danger'>Usulan Faskes : Isolasi Mandiri</span>" : (value.usulan_faskes == "2" ? "<span class='badge badge-success'>Bebas Isolasi Mandiri</span>" : (value.usulan_faskes == "3" ? "<span class='badge badge-warning'>Usulan Faskes : Rujuk Rumah Sakit</span>" : (value.is_sembuh ? "<span class='badge badge-success'>Sembuh</span>" : (value.is_meninggal == "1" ? "<span class='badge badge-dark'>Meninggal</span>" : (value.is_probable == "1" ? "<span class='badge badge-dark'>Probable</span>" : ""))))))) + "</div>") : (value.status_terkonfirmasi == "Positif" ? "<span class='badge badge-danger'>" + value.status_terkonfirmasi + "</span>" : (value.status_terkonfirmasi == "Sembuh" ? "<span class='badge badge-info'>" + value.status_terkonfirmasi + "</span>" : (value.status_terkonfirmasi == "Meninggal" ? "<span class='badge badge-info'>" + value.status_terkonfirmasi + "</span>" : "")))) + "</td>";

                    html += "<td>" + (value.user_update != "" ? value.user_update : value.user_create) + "</td>";
                    html += "<td>" + (value.is_edit ? "<a href='#editDetailPeserta' onClick=\"show_detail_trx_peserta('" + value.id_encrypt + "')\" class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a>" : "") + (value.is_delete ? " <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>" : "") + "</td>";
                    html += "</tr>";
                });

                html += "</table>";
                $(".card-table-detail").html(html);
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function confirm_delete(id_pemeriksaan) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/delete_pemeriksaan',
                    data: {
                        id_pemeriksaan: id_pemeriksaan
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_detail_peserta();
                            show_btn_sembuh();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_detail_peserta();
                            show_btn_sembuh();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }

    function confirm_sembuh() {
        let id_peserta = $("input[name='id_peserta']").val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menambah status sembuh?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/status_sembuh',
                    data: {
                        id_peserta: id_peserta
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_detail_peserta();
                            show_btn_sembuh();
                            swalInit(
                                'Berhasil',
                                'Data berhasil ditambahkan',
                                'success'
                            );
                        } else {
                            get_detail_peserta();
                            show_btn_sembuh();
                            swalInit(
                                'Gagal',
                                'Data gagal ditambahkan',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }

    function confirm_meninggal() {
        let id_peserta = $("input[name='id_peserta']").val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menambah status meninggal?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/status_meninggal',
                    data: {
                        id_peserta: id_peserta
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_detail_peserta();
                            show_btn_sembuh();
                            swalInit(
                                'Berhasil',
                                'Data berhasil ditambahkan',
                                'success'
                            );
                        } else {
                            get_detail_peserta();
                            show_btn_sembuh();
                            swalInit(
                                'Gagal',
                                'Data gagal ditambahkan',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }

    function confirm_probable() {
        let id_peserta = $("input[name='id_peserta']").val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menambah status probable?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/status_probable',
                    data: {
                        id_peserta: id_peserta
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_detail_peserta();
                            show_btn_sembuh();
                            swalInit(
                                'Berhasil',
                                'Data berhasil ditambahkan',
                                'success'
                            );
                        } else {
                            get_detail_peserta();
                            show_btn_sembuh();
                            swalInit(
                                'Gagal',
                                'Data gagal ditambahkan',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }

    function get_jenis_pemeriksaan(jenis_pemeriksaan_id) {

        let html = "<option data-id='' value=''>-- Pilih Jenis Pemeriksaan --</option>"
        $.ajax({
            url: base_url + 'peserta/request/get_jenis_pemeriksaan_all',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                if (response) {
                    let selected = "";
                    $.each(response, function(index, value) {
                        if (value.id_jenis_pemeriksaan == jenis_pemeriksaan_id) {
                            selected = "selected";
                        } else {
                            selected = "";
                        }
                        html += "<option data-id='" + value.id_jenis_pemeriksaan + "' " + selected + " value='" + value.id_encrypt + "'>" + value.nama_jenis_pemeriksaan + "</option>";
                    });
                    $("select[name='jenis_pemeriksaan']").html(html);
                } else {
                    $("select[name='jenis_pemeriksaan']").html(html);
                }
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function get_hasil_pemeriksaan(jenis_pemeriksaan_id_encrypt, hasil_pemeriksaan_id, jenis_pemeriksaan_id) {
        let id_jenis_pemeriksaan = $("select[name='jenis_pemeriksaan']").val();
        let level_user_id = $("input[name='level_user_id']").val();
        let data_id_jenis_pemeriksaan = $("select[name='jenis_pemeriksaan']").find("option:selected").attr("data-id");

        if (jenis_pemeriksaan_id) {
            data_id_jenis_pemeriksaan = jenis_pemeriksaan_id;
        }

        $("select[name='hasil_pemeriksaan']").attr("disabled", false);

        if (level_user_id == "3" && data_id_jenis_pemeriksaan == "1") {
            $("select[name='hasil_pemeriksaan']").attr("disabled", true);
        }

        if (jenis_pemeriksaan_id_encrypt) {
            id_jenis_pemeriksaan = jenis_pemeriksaan_id_encrypt;
        }

        let html = "<option value=''>-- Pilih Hasil Pemeriksaan --</option>"
        if (id_jenis_pemeriksaan) {
            $.ajax({
                url: base_url + 'peserta/request/get_hasil_pemeriksaan',
                data: {
                    id_jenis_pemeriksaan: id_jenis_pemeriksaan
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    let selected = "";
                    $.each(response, function(index, value) {
                        if (value.id_hasil_pemeriksaan == hasil_pemeriksaan_id) {
                            selected = "selected";
                        } else {
                            selected = "";
                        }
                        html += "<option " + selected + " value = '" + value.id_encrypt + "' > " + value.nama_hasil_pemeriksaan + " </option>";
                    });
                    $("select[name='hasil_pemeriksaan']").html(html);
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        } else {
            $("select[name='hasil_pemeriksaan']").html(html);
        }
    }

    function action_form_pemeriksaan() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let id_peserta = $("input[name='id_peserta']").val();
        let id_trx_pemeriksaan = $("input[name='id_trx_pemeriksaan']").val();
        let jenis_pemeriksaan = $("select[name='jenis_pemeriksaan']").val();
        let hasil_pemeriksaan = $("select[name='hasil_pemeriksaan']").val();
        let kode_sample = $("input[name='kode_sample']").val();
        if (!jenis_pemeriksaan) {
            $(".alert_form").html("<div class='alert alert-danger'>Jenis Pemeriksaan tidak boleh kosong.</div>");
        } else if (!kode_sample) {
            $(".alert_form").html("<div class='alert alert-danger'>Kode Sample tidak boleh kosong.</div>");
        } else {
            $(".alert_form").html("");
            $.ajax({
                url: base_url + 'peserta/action_form_pemeriksaan_except_faskes',
                data: {
                    id_trx_pemeriksaan: id_trx_pemeriksaan,
                    id_peserta: id_peserta,
                    jenis_pemeriksaan: jenis_pemeriksaan,
                    hasil_pemeriksaan: hasil_pemeriksaan,
                    kode_sample: kode_sample
                },
                type: 'POST',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    if (response) {
                        $("#modalPemeriksaan").modal("toggle");
                        get_detail_peserta();
                        show_btn_sembuh();
                        swalInit(
                            'Berhasil',
                            'Data berhasil ditambahkan',
                            'success'
                        );
                    } else {
                        $("#modalPemeriksaan").modal("toggle");
                        get_detail_peserta();
                        swalInit(
                            'Gagal',
                            'Data tidak bisa ditambahkan',
                            'error'
                        );
                    }
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }

    function action_form_nik() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let id_peserta = $("input[name='id_peserta']").val();
        let nik = $("input[name='nik']").val();
        if (!nik) {
            $(".alert_form_nik").html("<div class='alert alert-danger'>NIK tidak boleh kosong.</div>");
        } else {
            $(".alert_form_nik").html("");
            $.ajax({
                url: base_url + 'peserta/action_form_nik',
                data: {
                    id_peserta: id_peserta,
                    nik: nik
                },
                type: 'POST',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    if (response) {
                        $("#modalNik").modal("toggle");
                        swalInit(
                            'Berhasil',
                            'Data berhasil ditambahkan',
                            'success'
                        );
                        location.reload();
                    } else {
                        $("#modalNik").modal("toggle");
                        swalInit(
                            'Gagal',
                            'Data tidak bisa ditambahkan',
                            'error'
                        );
                    }
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }

    function show_detail_trx_peserta(id_trx_pemeriksaan) {
        $("input[name='id_trx_pemeriksaan']").val("");
        $("select[name='jenis_pemeriksaan']").html("");
        $("select[name='hasil_pemeriksaan']").html("");
        $.ajax({
            url: base_url + 'peserta/request/get_detail_trx_peserta',
            data: {
                id_trx_pemeriksaan: id_trx_pemeriksaan
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $("#modalPemeriksaan").modal("show");
                $(".title_modal").html("Ubah");
                $("input[name='id_trx_pemeriksaan']").val(id_trx_pemeriksaan);
                get_jenis_pemeriksaan(response.jenis_pemeriksaan_id);
                get_hasil_pemeriksaan(response.jenis_pemeriksaan_id_encrypt, response.hasil_pemeriksaan_id, response.jenis_pemeriksaan_id);
                $("input[name='tanggal_waktu']").val(response.tanggal_format);
                $("input[name='kode_sample']").val(response.kode_sample);
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    $(function() {
        show_btn_sembuh();

        let kode_sample_redirect = $("input[name='kode_sample_redirect']").val();

        if (kode_sample_redirect) {
            show_detail_trx_peserta(kode_sample_redirect);
        }

        $("#tambahPemeriksaan").on("click", function() {
            var swalInit = swal.mixin({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-light'
            });

            let id_peserta = $("input[name='id_peserta']").val();

            $.ajax({
                url: base_url + 'peserta/request/cek_kondisi_pemeriksaan_terakhir',
                data: {
                    id_peserta: id_peserta
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    if (response) {
                        let dateJS = new Date();
                        $("#modalPemeriksaan").modal("show");
                        $(".title_modal").html("Tambah");
                        $("select[name='jenis_pemeriksaan']").html("");
                        get_jenis_pemeriksaan();
                        $("select[name='hasil_pemeriksaan']").html("<option value=''>-- Pilih Hasil Pemeriksaan --</option>");
                        $("input[name='id_trx_pemeriksaan']").val("");
                        $("input[name='tanggal_waktu']").val(moment().format('DD-MM-YYYY h:m:s'));
                        $("input[name='kode_sample']").val("");
                    } else {
                        swalInit(
                            'Gagal',
                            'Tidak bisa menambahkan pemeriksaan baru. Hasil pemeriksaan terakhir belum keluar',
                            'error'
                        );
                    }
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        });
    });


    function show_btn_sembuh() {
        let id_peserta = $("input[name='id_peserta']").val();

        $.ajax({
            url: base_url + 'peserta/request/check_stat_terkonfirmasi_last',
            data: {
                id_peserta: id_peserta
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                if (!response) {
                    $("#tambahSembuh").hide();
                    $("#meninggal").hide();
                } else {
                    $("#tambahSembuh").show();
                    $("#meninggal").show();
                }
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function tambah_nik() {
        $("#modalNik").modal("show");
        $("input[name='nik']").val("");
    }
</script>