<style>
    .header_tbl {
        width: 15%;
    }

    .header_tbl2 {
        width: 1%;
    }
</style>
<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <input type="hidden" name="id_peserta" value="<?php echo !empty($id_peserta) ? $id_peserta : ""; ?>" />
            <div class="card card-table table-responsive shadow-0 mb-0">
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="header_tbl">Nama</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->nama : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">NIK</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->nik : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Tanggal Lahir</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->tanggal_lahir : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Jenis Kelamin</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->jenis_kelamin : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Pekerjaan</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->pekerjaan_inti : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Alamat KTP</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->alamat_ktp : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Alamat Domisili</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->alamat_domisili : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Nomor Telepon</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->nomor_telepon : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Petugas Input</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->nama_user : ""; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="card card-table">
                <div class="card-body">
                    <div class="text-right">
                        <a href="#" id="sembuh" onclick="confirm_sembuh()" class="btn btn-success">Sembuh</a>
                        <a href="#" id="bebasisoma" onclick="confirm_bebas_isoma()" class="btn btn-success">Bebas Isoma</a>
                        <a href="#" id="meninggal" onclick="confirm_meninggal()" class="btn btn-dark">Meninggal</a>
                        <a href="#" id="probable" onclick="confirm_probable()" class="btn btn-secondary">Probable</a>
                        <a href="#" id="bebasisomasuspek" onclick="confirm_bebas_isoma_suspek()" class="btn btn-success">Bebas Isoma Suspek</a>
                    </div>
                </div>
                <span class="card-table-detail"></span>
            </div>
        </div>
    </div>
</div>

<script>
    $("#bebasisoma").hide();
    $("#bebasisomasuspek").hide();

    get_detail_peserta();

    function get_detail_peserta() {
        let id_peserta = $("input[name='id_peserta']").val();

        $.ajax({
            url: base_url + 'peserta/request/get_detail_peserta',
            data: {
                id_peserta: id_peserta
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let html = "<table class='table'>";
                html += "<tr><th>Tanggal</th><th>Jam</th><th>Kode Sample</th><th>Jenis</th><th>Hasil</th><th>User Verifikasi</th><th>Action</th></tr>";
                $.each(response, function(index, value) {
                    html += "<tr>";
                    html += "<td>" + value.tanggal_pemeriksaan + "</td>";
                    html += "<td>" + value.jam_pemeriksaan + "</td>";
                    html += "<td>" + (value.kode_sample ? value.kode_sample : "") + "</td>";
                    html += "<td>" + (value.nama_jenis_pemeriksaan ? value.nama_jenis_pemeriksaan : "") + "</td>";
                    html += "<td>" + (value.id_trx_pemeriksaan ? (value.nama_hasil_pemeriksaan ? "<div>" + (value.class_badge == '1' ? "<span class='badge badge-success'>" + value.nama_hasil_pemeriksaan + "</span> " : (value.class_badge == '2' ? "<span class='badge badge-warning'>" + value.nama_hasil_pemeriksaan + "</span> " : "")) + "</div><div>" + (value.usulan_faskes == "0" ? "<span class='badge badge-danger'>Usulan Faskes : SWAB PCR</span>" : (value.usulan_faskes == "1" ? "<span class='badge badge-danger'>Usulan Faskes : Isolasi Mandiri</span>" : (value.usulan_faskes == "2" ? "<span class='badge badge-success'>Bebas Isolasi Mandiri</span>" : (value.usulan_faskes == "3" ? "<span class='badge badge-warning'>Usulan Faskes : Rujuk Rumah Sakit</span>" : "")))) + "</div>" : (value.nama_jenis_pemeriksaan ? "<div><span class='badge badge-secondary'>Hasil belum keluar</span></div>" : "") + "<div>" + (value.usulan_faskes == "0" ? "<span class='badge badge-danger'>Usulan Faskes : SWAB PCR</span>" : (value.usulan_faskes == "1" ? "<span class='badge badge-danger'>Usulan Faskes : Isolasi Mandiri</span>" : (value.usulan_faskes == "2" ? "<span class='badge badge-success'>Bebas Isolasi Mandiri</span>" : (value.usulan_faskes == "3" ? "<span class='badge badge-warning'>Usulan Faskes : Rujuk Rumah Sakit</span>" : (value.is_sembuh ? "<span class='badge badge-success'>Sembuh</span>" : (value.is_meninggal == "1" ? "<span class='badge badge-dark'>Meninggal</span>" : (value.is_probable == "1" ? "<span class='badge badge-dark'>Probable</span>" : ""))))))) + "</div>") + (value.status_rawat ? "<div class='mt-2'><span class='badge badge-light badge-striped badge-striped-left border-left-violet'>" + (value.status_rawat == '1' ? 'Rawat Inap' : (value.status_rawat == '2' ? 'Rawat Jalan' : (value.status_rawat == '3' ? 'Bebas Isolasi Mandiri' : ''))) + "</span></div>" : "") : (value.status_terkonfirmasi == "Positif" ? "<span class='badge badge-danger'>" + value.status_terkonfirmasi + "</span>" : (value.status_terkonfirmasi == "Sembuh" ? "<span class='badge badge-info'>" + value.status_terkonfirmasi + "</span>" : (value.status_terkonfirmasi == "Meninggal" ? "<span class='badge badge-info'>" + value.status_terkonfirmasi + "</span>" : "")))) + "</td>";

                    html += "<td>" + (value.user_update != "" ? value.user_update : value.user_create) + "</td>";
                    html += "<td>" + (value.is_edit ? "" : "") + (value.is_delete ? "<a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>" : "") + "</td>";
                    html += "</tr>";
                });

                html += "</table>";
                $(".card-table-detail").html(html);
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function confirm_sembuh() {
        let id_peserta = $("input[name='id_peserta']").val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menambah status sembuh?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/status_sembuh',
                    data: {
                        id_peserta: id_peserta
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Berhasil',
                                'Data berhasil ditambahkan',
                                'success'
                            );
                        } else {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Gagal',
                                'Data gagal ditambahkan',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }

    function confirm_bebas_isoma() {
        let id_peserta = $("input[name='id_peserta']").val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menambah status Bebas Isolasi Mandiri?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/status_bebas_isoma',
                    data: {
                        id_peserta: id_peserta
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Berhasil',
                                'Data berhasil ditambahkan',
                                'success'
                            );
                        } else {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Gagal',
                                'Data gagal ditambahkan',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }

    function confirm_meninggal() {
        let id_peserta = $("input[name='id_peserta']").val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menambah status meninggal?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/status_meninggal',
                    data: {
                        id_peserta: id_peserta
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Berhasil',
                                'Data berhasil ditambahkan',
                                'success'
                            );
                        } else {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Gagal',
                                'Data gagal ditambahkan',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }

    function confirm_probable() {
        let id_peserta = $("input[name='id_peserta']").val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menambah status probable?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/status_probable',
                    data: {
                        id_peserta: id_peserta
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Berhasil',
                                'Data berhasil ditambahkan',
                                'success'
                            );
                        } else {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Gagal',
                                'Data gagal ditambahkan',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }

    function confirm_bebas_isoma_suspek() {
        let id_peserta = $("input[name='id_peserta']").val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menambah status Bebas Isolasi Mandiri Suspek?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/status_bebas_isolasi_mandiri_suspek',
                    data: {
                        id_peserta: id_peserta
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Berhasil',
                                'Data berhasil ditambahkan',
                                'success'
                            );
                        } else {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Gagal',
                                'Data gagal ditambahkan',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }

    function confirm_delete(id_pemeriksaan) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/delete_pemeriksaan',
                    data: {
                        id_pemeriksaan: id_pemeriksaan
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_detail_peserta();
                            cek_button_status();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }

    $(function() {
        cek_button_status();
    });

    function cek_button_status() {
        let id_peserta = $("input[name='id_peserta']").val();

        $.ajax({
            url: base_url + 'peserta/request/check_stat_pemeriksaan_last',
            data: {
                id_peserta: id_peserta
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                if (response.jenis_pemeriksaan_id == '1' && response.hasil_pemeriksaan_id == '1' && (response.status_rawat == '1' || response.status_rawat == null)) {
                    $("#bebasisoma").hide();
                    $("#bebasisomasuspek").hide();
                } else if (response.jenis_pemeriksaan_id == '1' && response.hasil_pemeriksaan_id == '1' && response.status_rawat == '2') {
                    $("#bebasisoma").show();
                    $("#bebasisomasuspek").hide();
                } else if (response.is_sembuh == '1') {
                    $("#bebasisoma").hide();
                    $("#bebasisomasuspek").hide();
                } else if (response.is_meninggal == '3') {
                    $("#bebasisoma").hide();
                    $("#bebasisomasuspek").hide();
                } else if (response.is_probable == '4') {
                    $("#bebasisoma").hide();
                    $("#bebasisomasuspek").hide();
                } else if (response.usulan_faskes == '1') {
                    $("#bebasisoma").hide();
                    $("#bebasisomasuspek").show();
                }
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }
</script>