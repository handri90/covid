<style>
    .info-column {
        margin: 4px 0;
    }
</style>

<div class="content">
    <div class="card card-table">
        <table id="datatablePesertaNonPuskesmas" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>NIK</th>
                    <th>Hasil Pemeriksaan</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="modalPemeriksaan" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Pencocokan Database Puskesmas</h5>
                <div class="ml-5">
                    <button type="submit" class="btn btn-success" onclick="save_new_peserta()">Simpan sebagai data baru</button>
                </div>
                <input type="hidden" name="id_peserta_non_puskesmas" />
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="card card-table">
                    <table id="datatablePesertaSimiliar" class="table datatable-save-state table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>NIK</th>
                                <th>Tanggal Lahir</th>
                                <th>Jenis Kelamin</th>
                                <th>Pekerjaan</th>
                                <th>Alamat KTP</th>
                                <th>Alamat Domisili</th>
                                <th>No. Telepon</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let datatablePesertaNonPuskesmas = $("#datatablePesertaNonPuskesmas").DataTable({
        "deferRender": true
    });

    let datatablePesertaSimiliar = $("#datatablePesertaSimiliar").DataTable({
        "deferRender": true
    });

    get_peserta();

    function get_peserta() {

        datatablePesertaNonPuskesmas.clear().draw();
        $.ajax({
            url: base_url + 'peserta/request/get_peserta_non_puskesmas',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    datatablePesertaNonPuskesmas.row.add([
                        value.nama,
                        value.nik,
                        (value.id_trx_pemeriksaan ? (typeof(value.nama_jenis_pemeriksaan_faskes) !== 'undefined' ? "<div class='info-column'>" + (value.usulan_pemeriksaan_faskes_row == "0" ? "<span class='badge badge-danger'>Usulan Faskes : SWAB PCR</span>" : (value.usulan_pemeriksaan_faskes_row == "1" ? "<span class='badge badge-danger'>Usulan Faskes : Isolasi Mandiri</span>" : "")) + "</div><div class='info-column'>" + value.nama_jenis_pemeriksaan_faskes + " " + (value.class_badge_faskes == '1' ? "<span class='badge badge-success'>" + value.nama_hasil_pemeriksaan_faskes + "</span>" : (value.class_badge_faskes == '2' ? "<span class='badge badge-warning'>" + value.nama_hasil_pemeriksaan_faskes + "</span>" : (value.nama_jenis_pemeriksaan_faskes ? "<span class='badge badge-secondary'>Hasil Belum Keluar" + "</span>" : ""))) + "</div><div class='info-column'><span class='badge badge-info'>" + value.tanggal_pemeriksaan + "</span></div>" : (value.usulan_faskes_tanpa_pemeriksaan == "0" ? "<span class='badge badge-danger'>Usulan Faskes : SWAB PCR</span>" : "<span class='badge badge-danger'>Usulan Faskes : Isolasi Mandiri</span>")) + "<div><span class='badge bg-purple-400'>" + value.nama_asal_faskes + "</span></div>" : (value.usulan_faskes_tanpa_pemeriksaan == "0" ? "<span class='badge badge-danger'>Usulan Faskes : SWAB PCR</span>" : "")),
                        "<a href='#' onClick=\"sync_data_peserta('" + value.id_encrypt + "')\" class='btn bg-pink-400 btn-icon rounded-round'><i class='icon-link'></i></a>"
                    ]).draw(false);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function sync_data_peserta(id_peserta) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menambahkan data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $("input[name='id_peserta_non_puskesmas']").val(id_peserta);
                $.ajax({
                    url: base_url + 'peserta/request/sync_data_peserta',
                    data: {
                        id_peserta: id_peserta
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response == 'ada') {
                            get_peserta();
                            swalInit(
                                'Berhasil',
                                'Data sudah ditambahkan',
                                'success'
                            );
                        } else {
                            if (response.length > 0) {
                                $("#modalPemeriksaan").modal("show");

                                datatablePesertaSimiliar.clear().draw();

                                $.each(response, function(index, value) {
                                    datatablePesertaSimiliar.row.add([
                                        value.nama,
                                        value.nik,
                                        value.tanggal_lahir,
                                        value.jenis_kelamin,
                                        value.pekerjaan_inti,
                                        value.alamat_ktp,
                                        value.alamat_domisili,
                                        value.nomor_telepon,
                                        "<a href='#' onClick=\"copy_data('" + value.id_encrypt + "')\" class='btn bg-pink-400 btn-icon rounded-round'><i class='icon-link'></i></a>"
                                    ]).draw(false);
                                });
                            } else {
                                swalInit({
                                    title: 'Data tidak ditemukan di database puskesmas. Apakah anda ingin menambahkan data ini ke database puskesmas?',
                                    type: 'warning',
                                    showCancelButton: true,
                                    confirmButtonText: 'Ya!',
                                    cancelButtonText: 'Batal!',
                                    confirmButtonClass: 'btn btn-success',
                                    cancelButtonClass: 'btn btn-danger',
                                    buttonsStyling: false
                                }).then(function(result) {
                                    if (result.value) {
                                        $.ajax({
                                            url: base_url + 'peserta/clone_data_non_puskesmas',
                                            data: {
                                                id_peserta: id_peserta
                                            },
                                            type: 'GET',
                                            beforeSend: function() {
                                                HoldOn.open(optionsHoldOn);
                                            },
                                            success: function(response) {
                                                if (response) {
                                                    get_peserta();
                                                    swalInit(
                                                        'Berhasil',
                                                        'Data sudah ditambahkan',
                                                        'success'
                                                    );
                                                } else {
                                                    get_peserta();
                                                    swalInit(
                                                        'Gagal',
                                                        'Data tidak bisa ditambahkan',
                                                        'error'
                                                    );
                                                }

                                            },
                                            complete: function() {
                                                HoldOn.close();
                                            }
                                        });
                                    } else if (result.dismiss === swal.DismissReason.cancel) {
                                        swalInit(
                                            'Batal',
                                            'Data masih tersimpan!',
                                            'error'
                                        ).then(function(results) {
                                            HoldOn.close();
                                            if (result.results) {
                                                get_peserta();
                                            }
                                        });
                                    }
                                });
                            }
                        }

                    },
                    complete: function() {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }

    function save_new_peserta() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let id_peserta_non_puskesmas = $("input[name='id_peserta_non_puskesmas']").val();

        $.ajax({
            url: base_url + 'peserta/clone_data_non_puskesmas',
            data: {
                id_peserta: id_peserta_non_puskesmas
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                if (response) {
                    get_peserta();
                    $("#modalPemeriksaan").modal("toggle");
                    swalInit(
                        'Berhasil',
                        'Data sudah ditambahkan',
                        'success'
                    );
                } else {
                    get_peserta();
                    $("#modalPemeriksaan").modal("toggle");
                    swalInit(
                        'Gagal',
                        'Data tidak bisa ditambahkan',
                        'error'
                    );
                }

            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function copy_data(id_peserta) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menambahkan data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                let id_peserta_non_puskesmas = $("input[name='id_peserta_non_puskesmas']").val();

                $.ajax({
                    url: base_url + 'peserta/copy_data',
                    data: {
                        id_peserta: id_peserta,
                        id_peserta_non_puskesmas: id_peserta_non_puskesmas
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_peserta();
                            $("#modalPemeriksaan").modal("toggle");

                            datatablePesertaSimiliar.clear().draw();

                            $("input[name='id_peserta_non_puskesmas']").val("");

                            swalInit(
                                'Berhasil',
                                'Data sudah ditambahkan',
                                'success'
                            );
                        } else {
                            $("input[name='id_peserta_non_puskesmas']").val("");
                            get_peserta();
                            $("#modalPemeriksaan").modal("toggle");
                            swalInit(
                                'Gagal',
                                'Data tidak bisa ditambahkan',
                                'error'
                            );
                        }

                    },
                    complete: function() {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                $("input[name='id_peserta_non_puskesmas']").val("");
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta();
                    }
                });
            }
        });
    }
</script>