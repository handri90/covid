<style>
    .header_tbl {
        width: 30%;
    }

    .header_tbl2 {
        width: 1%;
    }

    .info-column {
        margin: 4px 0;
    }

    .detail {
        float: right;
        font-weight: 400;
        line-height: 0.6;
        text-shadow: none;
        color: #fff !important;
        top: -5px;
    }
</style>

<div class="content">
    <div class="card">
        <div class="card-header header-elements-inline">
            <div class="header-elements">
                <div class="form-check form-check-switchery form-check-switchery-sm">
                    <label class="form-check-label">
                        Hari Ini:
                        <input type="checkbox" name="report_today" class="form-check-input-switchery-primary" onclick="get_peserta()" data-fouc>
                    </label>
                </div>
            </div>
            <div class="text-right">
                <!-- <a href="<?php echo base_url() . 'peserta/detail_peserta_non_puskesmas_ch'; ?>" class="btn btn-info">List Peserta Citra Husada</a> -->
                <a href="<?php echo base_url() . 'peserta/cetak_peserta_ppkm'; ?>" class="btn btn-info">Cetak Peserta</a>
            </div>
        </div>
        <div class="card-body">
            <h5>Kecamatan Arut Selatan</h5>
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-highlight nav-justified">
                <li class="nav-item"><a href="#kec-arsel" class="nav-link active" data-toggle="tab">KEC - ARSEL</a></li>
                <li class="nav-item"><a href="#kec-aruta" class="nav-link" data-toggle="tab">KEC - ARUTA</a></li>
                <li class="nav-item"><a href="#kec-kumai" class="nav-link" data-toggle="tab">KEC - KUMAI</a></li>
                <li class="nav-item"><a href="#kec-kolam" class="nav-link" data-toggle="tab">KEC - KOLAM</a></li>
                <li class="nav-item"><a href="#kec-pbanteng" class="nav-link" data-toggle="tab">KEC - P. BANTENG</a></li>
                <li class="nav-item"><a href="#kec-plada" class="nav-link" data-toggle="tab">KEC - P. LADA</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade show active" id="kec-arsel">
                    <div class="card card-table table-responsive shadow-0 mb-0">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="200" class="text-center">Nama Wilayah / RT</th>
                                    <th class="text-center">Jumlah Warga Positif</th>
                                    <th class="text-center">Jumlah Rumah Kasus Aktif</th>
                                    <th width="50" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="list_kecamatan_arsel">

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="kec-aruta">
                    <div class="card card-table table-responsive shadow-0 mb-0">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="200" class="text-center">Nama Wilayah / RT</th>
                                    <th class="text-center">Jumlah Warga Positif</th>
                                    <th class="text-center">Jumlah Rumah Kasus Aktif</th>
                                    <th width="50" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="list_kecamatan_aruta">

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="kec-kumai">
                    <div class="card card-table table-responsive shadow-0 mb-0">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="200" class="text-center">Nama Wilayah / RT</th>
                                    <th class="text-center">Jumlah Warga Positif</th>
                                    <th class="text-center">Jumlah Rumah Kasus Aktif</th>
                                    <th width="50" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="list_kecamatan_kumai">

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="kec-kolam">
                    <div class="card card-table table-responsive shadow-0 mb-0">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="200" class="text-center">Nama Wilayah / RT</th>
                                    <th class="text-center">Jumlah Warga Positif</th>
                                    <th class="text-center">Jumlah Rumah Kasus Aktif</th>
                                    <th width="50" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="list_kecamatan_kolam">

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="kec-pbanteng">
                    <div class="card card-table table-responsive shadow-0 mb-0">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="200" class="text-center">Nama Wilayah / RT</th>
                                    <th class="text-center">Jumlah Warga Positif</th>
                                    <th class="text-center">Jumlah Rumah Kasus Aktif</th>
                                    <th width="50" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="list_kecamatan_banteng">

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="kec-plada">
                    <div class="card card-table table-responsive shadow-0 mb-0">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="200" class="text-center">Nama Wilayah / RT</th>
                                    <th class="text-center">Jumlah Warga Positif</th>
                                    <th class="text-center">Jumlah Rumah Kasus Aktif</th>
                                    <th width="50" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="list_kecamatan_lada">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var primary = document.querySelector('.form-check-input-switchery-primary');
    var switchery = new Switchery(primary, {
        color: '#2196F3'
    });

    get_peserta();

    function get_peserta() {
        $(".list_kecamatan_arsel").html("");
        $(".list_kecamatan_aruta").html("");
        $(".list_kecamatan_kumai").html("");
        $(".list_kecamatan_kolam").html("");
        $(".list_kecamatan_banteng").html("");
        $(".list_kecamatan_lada").html("");
        let report_today = $("input[name='report_today']").is(":checked");

        $.ajax({
            url: base_url + 'peserta/request/get_peserta_kodim',
            data: {
                report_today: report_today
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let html_arsel = "";
                $.each(response['arsel'], function(index, value) {
                    html_arsel += "<tr><td>" + value.nama_wilayah + "</td><td></td><td></td><td></td></tr>";
                    if (value.rt_domisili) {
                        let split_rt_domisili_arsel = value.rt_domisili.split("|");
                        let split_jumlah_peserta_arsel = value.jumlah_peserta.split("|");
                        let split_jumlah_rumah_arsel = value.jumlah_rumah.split("|");
                        $.each(split_rt_domisili_arsel, function(index_rt_domisili, value_rt_domisili) {

                            let warna_rt = "";
                            let warna_font = "";
                            if (split_jumlah_rumah_arsel[index_rt_domisili] == '0') {
                                warna_rt = "green";
                                warna_font = "white";
                            } else if (split_jumlah_rumah_arsel[index_rt_domisili] == '1' || split_jumlah_rumah_arsel[index_rt_domisili] == '2') {
                                warna_rt = "yellow";
                                warna_font = "black";
                            } else if (split_jumlah_rumah_arsel[index_rt_domisili] == '3' || split_jumlah_rumah_arsel[index_rt_domisili] == '4' || split_jumlah_rumah_arsel[index_rt_domisili] == '5') {
                                warna_rt = "orange";
                                warna_font = "black";
                            } else if (split_jumlah_rumah_arsel[index_rt_domisili] >= '6') {
                                warna_rt = "red";
                                warna_font = "white";
                            }

                            html_arsel += "<tr><td class='text-right'>" + value_rt_domisili + "</td><td class='text-center'>" + split_jumlah_peserta_arsel[index_rt_domisili] + "</td><td class='text-center' style='background-color:" + warna_rt + ";color:" + warna_font + ";'><h5>" + split_jumlah_rumah_arsel[index_rt_domisili] + "</h5></td><td><a href = '" + base_url + "peserta/detail_peserta_polres_kodim/" + value.id_encrypt + "/" + value_rt_domisili + "' class ='btn btn-primary btn-success'> <i class='icon-eye'> </i></a></td></tr>";
                        })
                    }
                })
                $(".list_kecamatan_arsel").html(html_arsel);

                let html_aruta = "";
                $.each(response['aruta'], function(index, value) {
                    html_aruta += "<tr><td>" + value.nama_wilayah + "</td><td></td><td></td><td></td></tr>";
                    if (value.rt_domisili) {
                        let split_rt_domisili_aruta = value.rt_domisili.split("|");
                        let split_jumlah_peserta_aruta = value.jumlah_peserta.split("|");
                        let split_jumlah_rumah_aruta = value.jumlah_rumah.split("|");
                        $.each(split_rt_domisili_aruta, function(index_rt_domisili, value_rt_domisili) {

                            let warna_rt = "";
                            let warna_font = "";
                            if (split_jumlah_rumah_aruta[index_rt_domisili] == '0') {
                                warna_rt = "green";
                                warna_font = "white";
                            } else if (split_jumlah_rumah_aruta[index_rt_domisili] == '1' || split_jumlah_rumah_aruta[index_rt_domisili] == '2') {
                                warna_rt = "yellow";
                                warna_font = "black";
                            } else if (split_jumlah_rumah_aruta[index_rt_domisili] == '3' || split_jumlah_rumah_aruta[index_rt_domisili] == '4' || split_jumlah_rumah_aruta[index_rt_domisili] == '5') {
                                warna_rt = "orange";
                                warna_font = "black";
                            } else if (split_jumlah_rumah_aruta[index_rt_domisili] >= '6') {
                                warna_rt = "red";
                                warna_font = "white";
                            }

                            html_aruta += "<tr><td class='text-right'>" + value_rt_domisili + "</td><td class='text-center'>" + split_jumlah_peserta_aruta[index_rt_domisili] + "</td><td class='text-center' style='background-color:" + warna_rt + ";color:" + warna_font + ";'><h5>" + split_jumlah_rumah_aruta[index_rt_domisili] + "</h5></td><td><a href = '" + base_url + "peserta/detail_peserta_polres_kodim/" + value.id_encrypt + "/" + value_rt_domisili + "' class ='btn btn-primary btn-success'> <i class='icon-eye'> </i></a></td></tr>";
                        })
                    }
                })
                $(".list_kecamatan_aruta").html(html_aruta);

                let html_kumai = "";
                $.each(response['kumai'], function(index, value) {
                    html_kumai += "<tr><td>" + value.nama_wilayah + "</td><td></td><td></td><td></td></tr>";
                    if (value.rt_domisili) {
                        let split_rt_domisili_kumai = value.rt_domisili.split("|");
                        let split_jumlah_peserta_kumai = value.jumlah_peserta.split("|");
                        let split_jumlah_rumah_kumai = value.jumlah_rumah.split("|");
                        $.each(split_rt_domisili_kumai, function(index_rt_domisili, value_rt_domisili) {

                            let warna_rt = "";
                            let warna_font = "";
                            if (split_jumlah_rumah_kumai[index_rt_domisili] == '0') {
                                warna_rt = "green";
                                warna_font = "white";
                            } else if (split_jumlah_rumah_kumai[index_rt_domisili] == '1' || split_jumlah_rumah_kumai[index_rt_domisili] == '2') {
                                warna_rt = "yellow";
                                warna_font = "black";
                            } else if (split_jumlah_rumah_kumai[index_rt_domisili] == '3' || split_jumlah_rumah_kumai[index_rt_domisili] == '4' || split_jumlah_rumah_kumai[index_rt_domisili] == '5') {
                                warna_rt = "orange";
                                warna_font = "black";
                            } else if (split_jumlah_rumah_kumai[index_rt_domisili] >= '6') {
                                warna_rt = "red";
                                warna_font = "white";
                            }

                            html_kumai += "<tr><td class='text-right'>" + value_rt_domisili + "</td><td class='text-center'>" + split_jumlah_peserta_kumai[index_rt_domisili] + "</td><td class='text-center' style='background-color:" + warna_rt + ";color:" + warna_font + ";'><h5>" + split_jumlah_rumah_kumai[index_rt_domisili] + "</h5></td><td><a href = '" + base_url + "peserta/detail_peserta_polres_kodim/" + value.id_encrypt + "/" + value_rt_domisili + "' class ='btn btn-primary btn-success'> <i class='icon-eye'> </i></a></td></tr>";
                        })
                    }
                })
                $(".list_kecamatan_kumai").html(html_kumai);

                let html_kolam = "";
                $.each(response['kolam'], function(index, value) {
                    html_kolam += "<tr><td>" + value.nama_wilayah + "</td><td></td><td></td><td></td></tr>";
                    if (value.rt_domisili) {
                        let split_rt_domisili_kolam = value.rt_domisili.split("|");
                        let split_jumlah_peserta_kolam = value.jumlah_peserta.split("|");
                        let split_jumlah_rumah_kolam = value.jumlah_rumah.split("|");
                        $.each(split_rt_domisili_kolam, function(index_rt_domisili, value_rt_domisili) {

                            let warna_rt = "";
                            let warna_font = "";
                            if (split_jumlah_rumah_kolam[index_rt_domisili] == '0') {
                                warna_rt = "green";
                                warna_font = "white";
                            } else if (split_jumlah_rumah_kolam[index_rt_domisili] == '1' || split_jumlah_rumah_kolam[index_rt_domisili] == '2') {
                                warna_rt = "yellow";
                                warna_font = "black";
                            } else if (split_jumlah_rumah_kolam[index_rt_domisili] == '3' || split_jumlah_rumah_kolam[index_rt_domisili] == '4' || split_jumlah_rumah_kolam[index_rt_domisili] == '5') {
                                warna_rt = "orange";
                                warna_font = "black";
                            } else if (split_jumlah_rumah_kolam[index_rt_domisili] >= '6') {
                                warna_rt = "red";
                                warna_font = "white";
                            }

                            html_kolam += "<tr><td class='text-right'>" + value_rt_domisili + "</td><td class='text-center'>" + split_jumlah_peserta_kolam[index_rt_domisili] + "</td><td class='text-center' style='background-color:" + warna_rt + ";color:" + warna_font + ";'><h5>" + split_jumlah_rumah_kolam[index_rt_domisili] + "</h5></td><td><a href = '" + base_url + "peserta/detail_peserta_polres_kodim/" + value.id_encrypt + "/" + value_rt_domisili + "' class ='btn btn-primary btn-success'> <i class='icon-eye'> </i></a></td></tr>";
                        })
                    }
                })
                $(".list_kecamatan_kolam").html(html_kolam);

                let html_banteng = "";
                $.each(response['banteng'], function(index, value) {
                    html_banteng += "<tr><td>" + value.nama_wilayah + "</td><td></td><td></td><td></td></tr>";
                    if (value.rt_domisili) {
                        let split_rt_domisili_banteng = value.rt_domisili.split("|");
                        let split_jumlah_peserta_banteng = value.jumlah_peserta.split("|");
                        let split_jumlah_rumah_banteng = value.jumlah_rumah.split("|");
                        $.each(split_rt_domisili_banteng, function(index_rt_domisili, value_rt_domisili) {

                            let warna_rt = "";
                            let warna_font = "";
                            if (split_jumlah_rumah_banteng[index_rt_domisili] == '0') {
                                warna_rt = "green";
                                warna_font = "white";
                            } else if (split_jumlah_rumah_banteng[index_rt_domisili] == '1' || split_jumlah_rumah_banteng[index_rt_domisili] == '2') {
                                warna_rt = "yellow";
                                warna_font = "black";
                            } else if (split_jumlah_rumah_banteng[index_rt_domisili] == '3' || split_jumlah_rumah_banteng[index_rt_domisili] == '4' || split_jumlah_rumah_banteng[index_rt_domisili] == '5') {
                                warna_rt = "orange";
                                warna_font = "black";
                            } else if (split_jumlah_rumah_banteng[index_rt_domisili] >= '6') {
                                warna_rt = "red";
                                warna_font = "white";
                            }

                            html_banteng += "<tr><td class='text-right'>" + value_rt_domisili + "</td><td class='text-center'>" + split_jumlah_peserta_banteng[index_rt_domisili] + "</td><td class='text-center' style='background-color:" + warna_rt + ";color:" + warna_font + ";'><h5>" + split_jumlah_rumah_banteng[index_rt_domisili] + "</h5></td><td><a href = '" + base_url + "peserta/detail_peserta_polres_kodim/" + value.id_encrypt + "/" + value_rt_domisili + "' class ='btn btn-primary btn-success'> <i class='icon-eye'> </i></a></td></tr>";
                        })
                    }
                })
                $(".list_kecamatan_banteng").html(html_banteng);

                let html_lada = "";
                $.each(response['lada'], function(index, value) {
                    html_lada += "<tr><td>" + value.nama_wilayah + "</td><td></td><td></td><td></td></tr>";
                    if (value.rt_domisili) {
                        let split_rt_domisili_lada = value.rt_domisili.split("|");
                        let split_jumlah_peserta_lada = value.jumlah_peserta.split("|");
                        let split_jumlah_rumah_lada = value.jumlah_rumah.split("|");
                        $.each(split_rt_domisili_lada, function(index_rt_domisili, value_rt_domisili) {

                            let warna_rt = "";
                            let warna_font = "";
                            if (split_jumlah_rumah_lada[index_rt_domisili] == '0') {
                                warna_rt = "green";
                                warna_font = "white";
                            } else if (split_jumlah_rumah_lada[index_rt_domisili] == '1' || split_jumlah_rumah_lada[index_rt_domisili] == '2') {
                                warna_rt = "yellow";
                                warna_font = "black";
                            } else if (split_jumlah_rumah_lada[index_rt_domisili] == '3' || split_jumlah_rumah_lada[index_rt_domisili] == '4' || split_jumlah_rumah_lada[index_rt_domisili] == '5') {
                                warna_rt = "orange";
                                warna_font = "black";
                            } else if (split_jumlah_rumah_lada[index_rt_domisili] >= '6') {
                                warna_rt = "red";
                                warna_font = "white";
                            }

                            html_lada += "<tr><td class='text-right'>" + value_rt_domisili + "</td><td class='text-center'>" + split_jumlah_peserta_lada[index_rt_domisili] + "</td><td class='text-center' style='background-color:" + warna_rt + ";color:" + warna_font + ";'><h5>" + split_jumlah_rumah_lada[index_rt_domisili] + "</h5></td><td><a href = '" + base_url + "peserta/detail_peserta_polres_kodim/" + value.id_encrypt + "/" + value_rt_domisili + "' class ='btn btn-primary btn-success'> <i class='icon-eye'> </i></a></td></tr>";
                        })
                    }
                })
                $(".list_kecamatan_lada").html(html_lada);
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }
</script>