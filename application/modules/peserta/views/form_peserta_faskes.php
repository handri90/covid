<style>
    .calendars {
        width: 350px;
    }

    .place_other_pendidikan,
    .place_other_pekerjaan {
        margin-top: -.5rem;
        margin-left: .5rem;
    }

    .ktp-image-custom {
        margin-bottom: 20px;
    }

    .kk-image-custom {
        margin-bottom: 20px;
    }
</style>
<div class="content">
    <!-- Form inputs -->
    <div class="card">
        <div class="card-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-validate-jquery')); ?>
            <fieldset class="mb-3">
                <legend class="text-uppercase font-size-sm font-weight-bold">Peserta Rapid</legend>

                <input type="hidden" name="id_peserta" value="<?php echo !empty($content) ? encrypt_data($content->id_peserta) : ""; ?>" />
                <input type="hidden" name="kel_desa_hidden" value="<?php echo !empty($content) ? encrypt_data($content->kelurahan_master_wilayah_id) : ""; ?>" />
                <input type="hidden" name="rt_domisili_hidden" value="<?php echo !empty($content) ? $content->master_rt_domisili_id : ""; ?>" />

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nama <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->nama : ""; ?>" name="nama" required placeholder="Nama" oninput="inpueUppercase(event)">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">NIK</label>
                    <div class="col-lg-10">
                        <input type="text" pattern=".{16}" id="nik" title="Field must be 16 characters long" class="form-control" value="<?php echo !empty($content) ? $content->nik : ""; ?>" name="nik" placeholder="NIK">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Tanggal Lahir <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control daterange-single" readonly value="<?php echo !empty($content) ? $content->tanggal_lahir : ""; ?>" name="tanggal_lahir" required placeholder="Tanggal Lahir">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Jenis Kelamin <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-input-styled" name="jenis_kelamin" value="L" <?php echo !empty($content) ? ($content->jenis_kelamin == "L" ? "checked" : "") : ""; ?> required data-fouc>
                                Laki-Laki
                            </label>
                        </div>

                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-input-styled" name="jenis_kelamin" value="P" <?php echo !empty($content) ? ($content->jenis_kelamin == "P" ? "checked" : "") : ""; ?> required data-fouc>
                                Perempuan
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Pekerjaan <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <select class="form-control select-search" name="pekerjaan_inti" required onchange="show_input_other_pekerjaan()">
                            <option value="">-- Pilih Pekerjaan --</option>
                            <?php
                            foreach ($pekerjaan as $key => $row) {
                                $selected = "";
                                if (!empty($content)) {
                                    if ($row == $content->pekerjaan_inti) {
                                        $selected = 'selected="selected"';
                                    }
                                }
                            ?>
                                <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $row; ?></option>
                            <?php
                            }
                            ?>
                        </select>

                        <div class="form-check form-check-inline">
                            <span class="place_other_pekerjaan">
                                <?php
                                if (!empty($content)) {
                                    if (in_array($content->pekerjaan_inti, array("Swasta", "Wiraswasta", "Lainnya"))) {
                                ?>
                                        <input value="<?php echo !empty($content) ? $content->pekerjaan_inti_lainnya : ""; ?>" type='text' class='form-control' name='pekerjaan_lainnya' required />
                                <?php
                                    }
                                }
                                ?>
                            </span>
                        </div>
                    </div>
                </div>

                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Alamat KTP</legend>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Alamat</label>
                        <div class="col-lg-10">
                            <textarea class="form-control" name="alamat_ktp" placeholder="Alamat"><?php echo !empty($content) ? $content->alamat_ktp : ""; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">RT</label>
                        <div class="col-lg-2">
                            <input type="text" id="rt_ktp" class="form-control" value="<?php echo !empty($content) ? $content->rt_ktp : ""; ?>" name="rt_ktp" placeholder="RT">
                        </div>
                    </div>
                </fieldset>

                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Alamat Domisili
                        (<label for="custom_checkbox_stacked_unchecked">Sama dengan alamat KTP</label> <input type="checkbox" id="custom_checkbox_stacked_unchecked" name='copy_ktp' value='yes' onclick="copy_alamat_ktp()" <?php echo !empty($content) ? ($content->is_copy_ktp == "1" ? "checked" : "") : ""; ?>>) / (<label for="custom_checkbox_stacked_unchecked_luar_kobar"><span class="text-danger">Wilayah Luar Kobar</span></label> <input type="checkbox" id="custom_checkbox_stacked_unchecked_luar_kobar" name='luar_kobar' value='yes' onclick="luar_kobar_fun()" <?php echo !empty($content) ? ($content->kelurahan_master_wilayah_id == "109" ? "checked" : "") : ""; ?>>)
                    </legend>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Alamat <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <textarea class="form-control" name="alamat_domisili" required placeholder="Alamat" <?php echo !empty($content) ? ($content->is_copy_ktp == "1" || $content->kelurahan_master_wilayah_id == "109" ? "readonly" : "") : ""; ?>><?php echo !empty($content) ? $content->alamat_domisili : ""; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Kelurahan/Desa <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <select <?php echo !empty($content) ? ($content->kelurahan_master_wilayah_id == "109" ? "disabled" : "") : ""; ?> class="form-control select-search" name="kelurahan_desa" required onchange="get_kecamatan_kabupaten_provinsi()">
                                <option value="">-- Pilih Kelurahan/Desa --</option>
                                <?php
                                foreach ($master_wilayah as $key => $row) {
                                    $selected = "";
                                    if (!empty($content)) {
                                        if ($row->id_master_wilayah == $content->kelurahan_master_wilayah_id) {
                                            $selected = 'selected="selected"';
                                        }
                                    }
                                ?>
                                    <option <?php echo $selected; ?> value="<?php echo encrypt_data($row->id_master_wilayah); ?>"><?php echo ucwords(strtolower($row->nama_wilayah)); ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">RT / Afdeling<span class="text-danger">*</span></label>
                        <div class="col-lg-2">
                            <!-- !empty($content) ? $content->rt_domisili : ""; -->

                            <select <?php echo !empty($content) ? ($content->kelurahan_master_wilayah_id == "109" ? "disabled" : "") : ""; ?> class="form-control select-search" name="rt_domisili" id="rt_domisili" required>
                                <option value="">-- Pilih RT --</option>
                            </select>
                        </div>
                        <div class="col-lg-2"><?php echo !empty($content) ? (isset($content->rt_domisili) && $content->rt_domisili != "" ? "<h3>(" . $content->rt_domisili . ")</h3>" : "") : ""; ?></div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Kecamatan <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <span class="nama_kecamatan"></span>
                        </div>
                    </div>
                </fieldset>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nomor Telepon <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" id="nomor_telepon" class="form-control" value="<?php echo !empty($content) ? $content->nomor_telepon : ""; ?>" name="nomor_telepon" required placeholder="Nomor Telepon">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Tanggal Mulai Bergejala <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control daterange-single" readonly value="<?php echo !empty($content) ? $content->tanggal_mulai_bergejala : ""; ?>" name="tanggal_mulai_bergejala" required placeholder="Tanggal Mulai Bergejala">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Kontak Erat</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->kontak_erat : ""; ?>" name="kontak_erat" placeholder="Kontak Erat">
                    </div>
                </div>

                <?php if (empty($content)) { ?>
                    <fieldset class="mb-3">
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Tanggal Pemeriksaan <span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control daterange-single" readonly name="tanggal_pemeriksaan" required placeholder="Tanggal Pemeriksaan">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Jenis Pemeriksaan</label>
                            <div class="col-lg-10">
                                <select class="form-control select-search" name="jenis_pemeriksaan" onchange="get_hasil_pemeriksaan()">
                                    <option value="">-- Pilih Jenis Pemeriksaan --</option>
                                    <?php
                                    foreach ($jenis_pemeriksaan as $key => $row) {
                                    ?>
                                        <option id_jenis="<?php echo $row->id_jenis_pemeriksaan; ?>" value="<?php echo encrypt_data($row->id_jenis_pemeriksaan); ?>"><?php echo $row->nama_jenis_pemeriksaan; ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row is_show_base_jenis_pemeriksaan">
                            <label class="col-form-label col-lg-2">Hasil Pemeriksaan <span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <select class="form-control select-search" name="hasil_pemeriksaan" onchange="is_show_usulan_faskes()">
                                    <option data-id='' value="">-- Pilih Hasil Pemeriksaan --</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row is_show_usulan_faskes">
                            <label class="col-form-label col-lg-2">Usulan Faskes <span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-input-styled" name="usulan_faskes" value="0" required>
                                        SWAB PCR
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-input-styled" name="usulan_faskes" value="1" required>
                                        Isolasi Mandiri
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-input-styled" name="usulan_faskes" value="3" required>
                                        Rujuk Rumah Sakit
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                <?php } ?>
            </fieldset>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <!-- /form inputs -->
</div>

<div id="modalNIK" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Database Peserta</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="card card-table">
                    <table id="datatablePesertaSimiliar" class="table datatable-save-state table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>NIK</th>
                                <th>Tanggal Lahir</th>
                                <th>Jenis Kelamin</th>
                                <th>Pekerjaan</th>
                                <th>Alamat KTP</th>
                                <th>Alamat Domisili</th>
                                <th>No. Telepon</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let datatablePesertaSimiliar = $("#datatablePesertaSimiliar").DataTable({
        "deferRender": true
    });

    $(".is_show_base_jenis_pemeriksaan").hide();

    $('.daterange-single').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD/MM/YYYY'
        },
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10)
    });

    $('.form-input-styled').uniform({
        fileButtonClass: 'action btn bg-pink-400'
    });

    function delete_input_other_pekerjaan() {
        $(".place_other_pekerjaan").html("");
    }

    function show_input_other_pekerjaan() {
        let pekerjaan_inti = $("select[name='pekerjaan_inti']").val();
        let arr = ["10", "11", "13"];
        if (arr.includes(pekerjaan_inti)) {
            $(".place_other_pekerjaan").html("<input type='text' class='form-control' name='pekerjaan_lainnya' required />");
        } else {
            $(".place_other_pekerjaan").html("");
        }
    }

    get_kecamatan_kabupaten_provinsi();

    function get_kecamatan_kabupaten_provinsi() {
        let kel_desa = $("select[name=kelurahan_desa]").val();
        let id_master_rt_edit = $("input[name=rt_domisili_hidden]").val();

        if (!kel_desa) {
            kel_desa = $("input[name=kel_desa_hidden]").val();
        }

        if (kel_desa && !$("input[name='luar_kobar']").is(":checked")) {
            $.ajax({
                url: base_url + 'peserta/request/get_kecamatan_kabupaten_provinsi',
                data: {
                    kel_desa: kel_desa
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $(".nama_kecamatan").html(response.kecamatan);
                    $(".nama_kabupaten").html(response.kabupaten);
                    $(".nama_provinsi").html(response.provinsi);
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });

            $.ajax({
                url: base_url + 'peserta/request/get_master_rt',
                data: {
                    kel_desa: kel_desa
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    let html = "<option value=''>-- Pilih RT --</option>";
                    $.each(response, function(index, value) {
                        let selected = "";
                        if (id_master_rt_edit) {
                            if (value.id_master_rt == id_master_rt_edit) {
                                selected = "selected";
                            }
                        }
                        html += "<option " + selected + " value='" + value.id_encrypt + "'>" + value.rt + "</option>";
                    });
                    $("select[name='rt_domisili']").html(html);
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        } else {
            $(".nama_kecamatan").html("");
            $("select[name='rt_domisili']").html("<option value=''>-- Pilih RT --</option>");
        }
    }

    function copy_alamat_ktp() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let alamat_ktp = $("textarea[name='alamat_ktp']").val();
        let rt_ktp = $("input[name='rt_ktp']").val();

        if (!$('#custom_checkbox_stacked_unchecked').is(":checked")) {
            $("textarea[name='alamat_domisili']").val("");
            $("textarea[name='alamat_domisili']").attr("readonly", false);
        } else {
            if (alamat_ktp == "") {
                swalInit(
                    'Gagal',
                    'Alamat KTP harus diisi',
                    'error'
                );

                $('#custom_checkbox_stacked_unchecked').prop('checked', false);
            } else {
                $("textarea[name='alamat_domisili']").val(alamat_ktp);
                $("textarea[name='alamat_domisili']").attr("readonly", true);
            }
        }
    }

    function get_hasil_pemeriksaan() {
        let id_jenis_pemeriksaan = $("select[name='jenis_pemeriksaan']").val();
        $(".is_show_usulan_faskes").show();

        let html = "<option data-id='' value=''>-- Pilih Hasil Pemeriksaan --</option>"
        if (id_jenis_pemeriksaan) {
            $(".is_show_base_jenis_pemeriksaan").show();
            $("select[name='hasil_pemeriksaan']").attr("required", true);

            $.ajax({
                url: base_url + 'peserta/request/get_hasil_pemeriksaan',
                data: {
                    id_jenis_pemeriksaan: id_jenis_pemeriksaan
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $.each(response, function(index, value) {
                        html += "<option data-id='" + value.class_badge + "' value='" + value.id_encrypt + "'>" + value.nama_hasil_pemeriksaan + "</option>";
                    });
                    $("select[name='hasil_pemeriksaan']").html(html);
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        } else {
            $(".is_show_base_jenis_pemeriksaan").hide();
            $("select[name='hasil_pemeriksaan']").attr("required", false);
            $("select[name='hasil_pemeriksaan']").html(html);
        }

    }

    function is_show_usulan_faskes() {
        let class_badge = $("select[name='hasil_pemeriksaan']").find("option:selected").attr("data-id");
        $(".is_show_usulan_faskes").show();
        $("input[name='usulan_faskes']").attr("required", true);
        if (class_badge == "2") {
            $(".is_show_usulan_faskes").show();
            $("input[name='usulan_faskes']").attr("required", true);
        } else if (class_badge == "1" || class_badge == "0") {
            $(".is_show_usulan_faskes").hide();
            $("input[name='usulan_faskes']").attr("required", false);
        }
    }

    function inpueUppercase(e) {
        var ss = e.target.selectionStart;
        var se = e.target.selectionEnd;
        e.target.value = e.target.value.toUpperCase();
        e.target.selectionStart = ss;
        e.target.selectionEnd = se;
    }



    setInputFilter(document.getElementById("nik"), function(value) {
        return /^-?\d*$/.test(value);
    });
    setInputFilter(document.getElementById("rt_ktp"), function(value) {
        return /^-?\d*$/.test(value);
    });
    setInputFilter(document.getElementById("rt_domisili"), function(value) {
        return /^-?\d*$/.test(value);
    });
    setInputFilter(document.getElementById("nomor_telepon"), function(value) {
        return /^-?\d*$/.test(value);
    });

    function luar_kobar_fun() {
        if ($("input[name='luar_kobar']").is(":checked")) {
            $("textarea[name='alamat_domisili']").val("");
            $("textarea[name='alamat_domisili']").attr("readonly", true);
            $("select[name='rt_domisili']").prop("selectedIndex", 0).change();
            $("select[name='rt_domisili']").prop("disabled", true);
            $("select[name='kelurahan_desa']").prop("selectedIndex", 0).change();
            $("select[name='kelurahan_desa']").prop("disabled", true);
            $(".nama_kecamatan").html("");
            $(".nama_kabupaten").html("");
            $(".nama_provinsi").html("");
        } else {
            $("textarea[name='alamat_domisili']").attr("readonly", false);
            $("select[name='rt_domisili']").prop("disabled", false);
            $("select[name='kelurahan_desa']").prop("disabled", false);
        }
    }

    $("form").submit(function() {
        let id_peserta = $("input[name=id_peserta]").val();
        let nik = $("input[name=nik]").val();
        let status = true;

        if (nik) {
            $.ajax({
                url: base_url + 'peserta/request/cek_nik',
                async: false,
                data: {
                    nik: nik,
                    id_peserta: id_peserta
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    if (response.length > 0) {
                        $("#modalNIK").modal("show");
                        status = false;

                        datatablePesertaSimiliar.clear().draw();

                        $.each(response, function(index, value) {
                            datatablePesertaSimiliar.row.add([
                                value.nama,
                                value.nik,
                                value.tanggal_lahir,
                                value.jenis_kelamin,
                                value.pekerjaan_inti,
                                value.alamat_ktp,
                                value.alamat_domisili,
                                value.nomor_telepon,
                                "<a href='" + base_url + "peserta/detail_peserta/" + value.id_encrypt + "' class='btn bg-pink-400 btn-icon rounded-round'><i class='icon-link'></i></a>"
                            ]).draw(false);
                        });
                    }
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        }

        if (!status) {
            return false;
        }
    });
</script>