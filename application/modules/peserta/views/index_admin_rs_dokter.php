<style>
    .info-column {
        margin: 4px 0;
    }
</style>

<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Tanggal</label>
                <div class="col-lg-10">
                    <button type="button" class="btn btn-light daterange-predefined">
                        <i class="icon-calendar22 mr-2"></i>
                        <span></span>
                    </button>

                    <input type="hidden" name="start_date" />
                    <input type="hidden" name="end_date" />
                </div>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <table id="datatablePesertaRapid" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>NIK</th>
                    <th>Pemeriksaan Faskes</th>
                    <th>Pemeriksaan Labkesda</th>
                    <th>Pemeriksaan Rumah Sakit</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    $('.daterange-predefined').daterangepicker({
            startDate: moment().subtract(14, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.daterange-predefined span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date]').val(end.format('YYYY-MM-DD'));
            $('#datatablePesertaRapid').DataTable().ajax.reload();
        }
    );

    // Display date format
    $('.daterange-predefined span').html(moment().subtract(14, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date]').val(moment().subtract(14, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date]').val(moment().format('YYYY-MM-DD'));
    get_peserta();

    function get_peserta() {

        $("#datatablePesertaRapid").DataTable({
            ajax: {
                "url": base_url + 'peserta/request/get_peserta_rs_dokter',
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "data": function(d) {
                    return $.extend({}, d, {
                        "start_date": $("input[name='start_date']").val(),
                        "end_date": $("input[name='end_date']").val(),
                    });
                },
                "dataSrc": '',
                "complete": function(response) {
                    HoldOn.close();
                }
            },
            "ordering": false,
            "columns": [{
                data: "nama"
            }, {
                data: "nik"
            }, {
                data: "kolom_faskes"
            }, {
                "render": function(data, type, full, meta) {
                    return full.kolom_labkesda + " " + (full.jenis_pemeriksaan_id_last == '1' && full.select_pcr_labkesda ? "<select class='form-control' name='hasil_pemeriksaan_" + full.n + "' onchange=\"change_hasil_pemeriksaan_pcr('" + full.id_encrypt + "','" + full.id_trx_pemeriksaan_last_encrypt + "','" + full.peserta_dari + "'," + full.n + ")\"><option value=''>-- Pilih Hasil Pemeriksaan --</option><option " + (full.hasil_pemeriksaan_id_lab == '1' ? 'selected' : '') + " value='1'>Positif</option><option " + (full.hasil_pemeriksaan_id_lab == '2' ? 'selected' : '') + " value='2'>Negatif</option><option " + (full.hasil_pemeriksaan_id_lab == '7' ? 'selected' : '') + " value='7'>Incon</option><option " + (full.hasil_pemeriksaan_id_lab == '11' ? 'selected' : '') + " value='11'>Invalid</option></select>" : "");
                }
            }, {
                "render": function(data, type, full, meta) {
                    return full.kolom_rs + " " + (full.jenis_pemeriksaan_id_last == '1' && full.select_pcr_rs ? "<select class='form-control' name='hasil_pemeriksaan_" + full.n + "' onchange=\"change_hasil_pemeriksaan_pcr('" + full.id_encrypt + "','" + full.id_trx_pemeriksaan_last_encrypt + "','" + full.peserta_dari + "'," + full.n + ")\"><option value=''>-- Pilih Hasil Pemeriksaan --</option><option " + (full.hasil_pemeriksaan_id_rs == '1' ? 'selected' : '') + " value='1'>Positif</option><option " + (full.hasil_pemeriksaan_id_rs == '2' ? 'selected' : '') + " value='2'>Negatif</option><option " + (full.hasil_pemeriksaan_id_rs == '7' ? 'selected' : '') + " value='7'>Incon</option><option " + (full.hasil_pemeriksaan_id_rs == '11' ? 'selected' : '') + " value='11'>Invalid</option></select>" : "");
                }
            }, {
                "width": "15%",
                "render": function(data, type, full, meta) {
                    return " <a href = '" + base_url + "peserta/detail_peserta_dokter/" + full.id_encrypt + "/" + full.peserta_dari + "' class ='btn btn-primary btn-icon'> <i class='icon-eye'> </i></a> " + (full.stat_button_print ? " <a target='_blank' href = '" + base_url + "peserta/cetak_hasil_pcr_admin_rs/" + full.id_encrypt + "/" + full.peserta_dari + "' class ='btn btn-primary btn-warning'> <i class='icon-printer'> </i></a>" : "");
                }
            }]
        });
    }

    function change_hasil_pemeriksaan_pcr(id_peserta, id_trx_pemeriksaan_last, data_from, index) {
        let val_status_pcr = $("select[name='hasil_pemeriksaan_" + index + "']").val();
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin mengubah data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/change_hasil_pemeriksaan_pcr',
                    data: {
                        id_peserta: id_peserta,
                        id_trx_pemeriksaan_last: id_trx_pemeriksaan_last,
                        val_status_pcr: val_status_pcr,
                        data_from: data_from
                    },
                    type: 'POST',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            $('#datatablePesertaRapid').DataTable().ajax.reload();
                            swalInit(
                                'Berhasil',
                                'Data berhasil disimpan',
                                'success'
                            );
                        } else {
                            $('#datatablePesertaRapid').DataTable().ajax.reload();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa disimpan',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        $('#datatablePesertaRapid').DataTable().ajax.reload();
                    }
                });
            }
        });
    }
</script>