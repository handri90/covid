<style>
    .header_tbl {
        width: 15%;
    }

    .header_tbl2 {
        width: 1%;
    }
</style>
<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <input type="hidden" name="id_peserta_non_puskesmas" value="<?php echo !empty($id_peserta_non_puskesmas) ? $id_peserta_non_puskesmas : ""; ?>" />
            <div class="card card-table table-responsive shadow-0 mb-0">
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="header_tbl">Nama</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->nama : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">NIK</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->nik : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Tanggal Lahir</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->tanggal_lahir : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Jenis Kelamin</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->jenis_kelamin : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Pekerjaan</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->pekerjaan_inti : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Alamat KTP</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->alamat_ktp : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Alamat Domisili</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->alamat_domisili : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Nomor Telepon</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->nomor_telepon : ""; ?></td>
                        </tr>
                        <tr>
                            <td class="header_tbl">Petugas Input</td>
                            <td class="header_tbl2">:</td>
                            <td><?php echo isset($content) ? $content->nama_user : ""; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="card card-table">
                <div class="card-body">
                    <div class="text-right">
                        <a href="#tambahPemeriksaan" id="tambahPemeriksaan" class="btn btn-info">Tambah Pemeriksaan</a>
                    </div>
                </div>
                <span class="card-table-detail"></span>
            </div>
        </div>
    </div>
</div>

<div id="modalPemeriksaan" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Pemeriksaan</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <span class="alert_form"></span>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Tanggal/Waktu <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="hidden" class="form-control" name="id_trx_pemeriksaan_non_puskesmas">
                        <input type="text" class="form-control" name="tanggal_waktu" readonly placeholder="Tanggal/Waktu">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Jenis Pemeriksaan <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <select class="form-control select-search" name="jenis_pemeriksaan" onchange="get_hasil_pemeriksaan()">
                            <option value="">-- Pilih Jenis Pemeriksaan --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row is_show_base_jenis_spesimen">
                    <label class="col-form-label col-lg-3">Jenis Spesimen <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <select class="form-control select-search" name="jenis_spesimen">
                            <option value="">-- Pilih Jenis Spesimen --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row is_show_hasil_pemeriksaan">
                    <label class="col-form-label col-lg-3">Hasil Pemeriksaan</label>
                    <div class="col-lg-9">
                        <select class="form-control select-search" name="hasil_pemeriksaan">
                            <option data-id='' value="">-- Pilih Hasil Pemeriksaan --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row is_show_kode_sample">
                    <label class="col-form-label col-lg-3">Kode Sample <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="kode_sample" placeholder="Kode Sample" required>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary" onclick="action_form_pemeriksaan_non_puskesmas()">Simpan <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(".is_show_kode_sample").hide();
    $(".is_show_base_jenis_spesimen").hide();

    get_detail_peserta_non_puskesmas();

    function get_detail_peserta_non_puskesmas() {
        let id_peserta_non_puskesmas = $("input[name='id_peserta_non_puskesmas']").val();

        $.ajax({
            url: base_url + 'peserta/request/get_detail_peserta_non_puskesmas',
            data: {
                id_peserta_non_puskesmas: id_peserta_non_puskesmas
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let html = "<table class='table'>";
                html += "<tr><th>Tanggal</th><th>Jam</th><th>Kode Sample</th><th>Jenis</th><th>Hasil</th><th>User Verifikasi</th><th>Action</th></tr>";
                $.each(response, function(index, value) {
                    html += "<tr>";
                    html += "<td>" + value.tanggal_pemeriksaan + "</td>";
                    html += "<td>" + value.jam_pemeriksaan + "</td>";
                    html += "<td>" + (value.kode_sample ? value.kode_sample : "") + "</td>";
                    html += "<td>" + (value.nama_jenis_pemeriksaan ? value.nama_jenis_pemeriksaan : "") + "</td>";
                    html += "<td>" + (value.nama_hasil_pemeriksaan ? "<div>" + (value.class_badge == '1' ? "<span class='badge badge-success'>" + value.nama_hasil_pemeriksaan + "</span> " : (value.class_badge == '2' ? "<span class='badge badge-warning'>" + value.nama_hasil_pemeriksaan + "</span> " : "")) + "</div><div>" + (value.usulan_faskes == "0" ? "<span class='badge badge-danger'>Usulan Faskes : SWAB PCR</span>" : (value.usulan_faskes == "1" ? "<span class='badge badge-danger'>Usulan Faskes : Isolasi Mandiri</span>" : (value.usulan_faskes == "2" ? "<span class='badge badge-success'>Bebas Isolasi Mandiri</span>" : (value.usulan_faskes == "3" ? "<span class='badge badge-warning'>Usulan Faskes : Rujuk Rumah Sakit</span>" : "")))) + "</div>" : (value.nama_jenis_pemeriksaan ? "<div><span class='badge badge-secondary'>Hasil belum keluar</span></div>" : "") + "<div>" + (value.usulan_faskes == "0" ? "<span class='badge badge-danger'>Usulan Faskes : SWAB PCR</span>" : (value.usulan_faskes == "1" ? "<span class='badge badge-danger'>Usulan Faskes : Isolasi Mandiri</span>" : (value.usulan_faskes == "2" ? "<span class='badge badge-success'>Bebas Isolasi Mandiri</span>" : (value.usulan_faskes == "3" ? "<span class='badge badge-warning'>Usulan Faskes : Rujuk Rumah Sakit</span>" : "")))) + "</div>") + "</td>";
                    html += "<td>" + (value.user_update != "" ? value.user_update : value.user_create) + "</td>";
                    html += "<td>" + (value.is_edit_deleted ? "<a href='#editDetailPeserta' onClick=\"show_detail_trx_peserta_non_puskesmas('" + value.id_encrypt + "')\" class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>" : "") + "</td>";
                    html += "</tr>";
                });

                html += "</table>";
                $(".card-table-detail").html(html);
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function confirm_delete(id_pemeriksaan_non_puskesmas) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'peserta/delete_pemeriksaan_non_puskesmas',
                    data: {
                        id_pemeriksaan_non_puskesmas: id_pemeriksaan_non_puskesmas
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_detail_peserta_non_puskesmas();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_detail_peserta_non_puskesmas();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_peserta_non_puskesmas();
                    }
                });
            }
        });
    }

    function get_jenis_pemeriksaan(jenis_pemeriksaan_id) {

        let html = "<option value=''>-- Pilih Jenis Pemeriksaan --</option>"
        $.ajax({
            url: base_url + 'peserta/request/get_jenis_pemeriksaan',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                if (response) {
                    let selected = "";
                    $.each(response, function(index, value) {
                        if (value.id_jenis_pemeriksaan == jenis_pemeriksaan_id) {
                            selected = "selected";
                        } else {
                            selected = "";
                        }
                        html += "<option data-id='" + value.id_jenis_pemeriksaan + "' " + selected + " value='" + value.id_encrypt + "'>" + value.nama_jenis_pemeriksaan + "</option>";
                    });
                    $("select[name='jenis_pemeriksaan']").html(html);
                } else {
                    $("select[name='jenis_pemeriksaan']").html(html);
                }
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function get_hasil_pemeriksaan(jenis_pemeriksaan_id_encrypt, hasil_pemeriksaan_id, jenis_pemeriksaan_id) {
        $(".is_show_base_jenis_spesimen").hide();
        let id_jenis_pemeriksaan = $("select[name='jenis_pemeriksaan']").val();
        let data_id_jenis_pemeriksaan = $("select[name='jenis_pemeriksaan']").find("option:selected").attr("data-id");

        if (jenis_pemeriksaan_id) {
            id_jenis_pemeriksaan = jenis_pemeriksaan_id_encrypt;
        }

        if (typeof(data_id_jenis_pemeriksaan) == 'undefined') {
            data_id_jenis_pemeriksaan = jenis_pemeriksaan_id;
        }

        if (data_id_jenis_pemeriksaan == "1") {
            $("select[name='hasil_pemeriksaan']").attr("disabled", true);
            $(".is_show_kode_sample").show();
            $(".is_show_base_jenis_spesimen").show();
            $("select[name='jenis_spesimen']").attr("required", true);
        } else {
            $("select[name='hasil_pemeriksaan']").attr("disabled", false);
            $(".is_show_kode_sample").hide();
            $("input[name='kode_sample']").attr("disabled", false);
            $("select[name='jenis_spesimen']").attr("required", false);
        }

        let html = "<option data-id='' value=''>-- Pilih Hasil Pemeriksaan --</option>"
        if (id_jenis_pemeriksaan) {

            $.ajax({
                url: base_url + 'peserta/request/get_hasil_pemeriksaan',
                data: {
                    id_jenis_pemeriksaan: id_jenis_pemeriksaan
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    let selected = "";
                    $.each(response, function(index, value) {
                        if (value.id_hasil_pemeriksaan == hasil_pemeriksaan_id) {
                            selected = "selected";
                        } else {
                            selected = "";
                        }
                        html += "<option data-id='" + value.class_badge + "' " + selected + " value = '" + value.id_encrypt + "' > " + value.nama_hasil_pemeriksaan + " </option>";
                    });
                    $("select[name='hasil_pemeriksaan']").html(html);
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        } else {
            $("select[name='hasil_pemeriksaan']").html(html);
        }
    }

    function get_jenis_spesimen(jenis_spesimen) {
        let arr = [];
        arr[1] = 'Nasofaring';
        arr[2] = 'Orofaring';

        let html = "<option value=''>-- Pilih Jenis Spesimen --</option>"
        $.each(arr, function(index, value) {
            if (index != 0) {
                let selected = "";
                if (index == jenis_spesimen) {
                    selected = "selected";
                }
                html += "<option " + selected + " value='" + index + "'>" + value + "</option>";
            }
        });

        $("select[name='jenis_spesimen']").html(html);
    }

    function action_form_pemeriksaan_non_puskesmas() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let id_peserta_non_puskesmas = $("input[name='id_peserta_non_puskesmas']").val();
        let id_trx_pemeriksaan_non_puskesmas = $("input[name='id_trx_pemeriksaan_non_puskesmas']").val();
        let jenis_pemeriksaan = $("select[name='jenis_pemeriksaan']").val();
        let hasil_pemeriksaan = $("select[name='hasil_pemeriksaan']").val();
        let jenis_spesimen = $("select[name='jenis_spesimen']").val();
        let kode_sample = $("input[name='kode_sample']").val();
        let class_badge = $("select[name='hasil_pemeriksaan']").find("option:selected").attr("data-id");
        let data_id_jenis_pemeriksaan = $("select[name='jenis_pemeriksaan']").find("option:selected").attr("data-id");

        if (!jenis_pemeriksaan) {
            $(".alert_form").html("<div class='alert alert-danger'>Jenis Pemeriksaan tidak boleh kosong.</div>");
        } else if (data_id_jenis_pemeriksaan == '1' && !jenis_spesimen) {
            $(".alert_form").html("<div class='alert alert-danger'>Jenis Spesimen tidak boleh kosong.</div>");
        } else if (data_id_jenis_pemeriksaan != '1' && !hasil_pemeriksaan) {
            $(".alert_form").html("<div class='alert alert-danger'>Hasil Pemeriksaan tidak boleh kosong.</div>");
        } else if (data_id_jenis_pemeriksaan == '1' && !kode_sample) {
            $(".alert_form").html("<div class='alert alert-danger'>Kode Sample tidak boleh kosong.</div>");
        } else {
            $(".alert_form").html("");
            $.ajax({
                url: base_url + 'peserta/action_form_pemeriksaan_non_puskesmas',
                data: {
                    id_trx_pemeriksaan_non_puskesmas: id_trx_pemeriksaan_non_puskesmas,
                    id_peserta_non_puskesmas: id_peserta_non_puskesmas,
                    jenis_pemeriksaan: jenis_pemeriksaan,
                    hasil_pemeriksaan: hasil_pemeriksaan,
                    kode_sample: kode_sample,
                    jenis_spesimen: jenis_spesimen
                },
                type: 'POST',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    if (response) {
                        $("#modalPemeriksaan").modal("toggle");
                        get_detail_peserta_non_puskesmas();
                        swalInit(
                            'Berhasil',
                            'Data berhasil ditambahkan',
                            'success'
                        );
                    } else {
                        $("#modalPemeriksaan").modal("toggle");
                        get_detail_peserta_non_puskesmas();
                        swalInit(
                            'Gagal',
                            'Data tidak bisa ditambahkan',
                            'error'
                        );
                    }
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }

    function show_detail_trx_peserta_non_puskesmas(id_trx_pemeriksaan_non_puskesmas) {
        $("input[name='id_trx_pemeriksaan_non_puskesmas']").val("");
        $("select[name='jenis_pemeriksaan']").html("");
        $("select[name='hasil_pemeriksaan']").html("");
        $.ajax({
            url: base_url + 'peserta/request/get_detail_trx_peserta_non_puskesmas',
            data: {
                id_trx_pemeriksaan_non_puskesmas: id_trx_pemeriksaan_non_puskesmas
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $("#modalPemeriksaan").modal("show");
                $(".title_modal").html("Ubah");
                $("input[name='id_trx_pemeriksaan_non_puskesmas']").val(id_trx_pemeriksaan_non_puskesmas);
                get_jenis_pemeriksaan(response.jenis_pemeriksaan_id);
                get_hasil_pemeriksaan(response.jenis_pemeriksaan_id_encrypt, response.hasil_pemeriksaan_id, response.jenis_pemeriksaan_id);
                get_jenis_spesimen(response.jenis_spesimen);
                $("input[name='tanggal_waktu']").val(response.tanggal_format);
                $("input[name='kode_sample']").val(response.kode_sample);
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    $(function() {
        $("#tambahPemeriksaan").on("click", function() {
            $("input[name='id_trx_pemeriksaan_non_puskesmas']").val("");
            $("select[name='jenis_pemeriksaan']").html("");
            $("select[name='hasil_pemeriksaan']").html("");

            var swalInit = swal.mixin({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-light'
            });

            let id_peserta_non_puskesmas = $("input[name='id_peserta_non_puskesmas']").val();

            $.ajax({
                url: base_url + 'peserta/request/cek_kondisi_pemeriksaan_terakhir_non_puskesmas',
                data: {
                    id_peserta_non_puskesmas: id_peserta_non_puskesmas
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    if (response) {
                        let dateJS = new Date();
                        $("#modalPemeriksaan").modal("show");
                        $(".title_modal").html("Tambah");
                        $("select[name='jenis_pemeriksaan']").html("");
                        get_jenis_pemeriksaan();
                        get_jenis_spesimen();
                        $("select[name='hasil_pemeriksaan']").html("<option value=''>-- Pilih Hasil Pemeriksaan --</option>");
                        $("input[name='id_trx_pemeriksaan_non_puskesmas']").val("");
                        $("input[name='tanggal_waktu']").val(moment().format('DD-MM-YYYY H:m:s'));
                    } else {
                        swalInit(
                            'Gagal',
                            'Tidak bisa menambahkan pemeriksaan baru. Hasil pemeriksaan terakhir belum keluar',
                            'error'
                        );
                    }
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        });
    })
</script>