<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('trx_pemeriksaan_model');
        $this->load->model('trx_pemeriksaan_non_puskesmas_model');
        $this->load->model('peserta_model');
        $this->load->model('status_rawat_model');
        $this->load->model('status_rawat_non_puskesmas_model');
        $this->load->model('peserta_non_puskesmas_model');
        $this->load->model('master_wilayah_model');
        $this->load->model('user_model');
        $this->load->model('peserta_dalam_satu_rumah_model');
        $this->load->model('detail_peserta_dalam_satu_rumah_model');
        $this->load->model('jenis_pemeriksaan/jenis_pemeriksaan_model', 'jenis_pemeriksaan_model');
        $this->load->model('hasil_pemeriksaan/hasil_pemeriksaan_model', 'hasil_pemeriksaan_model');
        $this->load->model('status_peserta/trx_terkonfirmasi_model', 'trx_terkonfirmasi_model');
        $this->load->model('migrate_rt_domisili/master_rt_model', 'master_rt_model');
    }

    public function get_peserta_faskes()
    {

        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");
        $jenis_pemeriksaan = decrypt_data($this->iget("jenis_pemeriksaan"));
        $hasil_pemeriksaan = decrypt_data($this->iget("hasil_pemeriksaan"));
        $greather_than_14 = $this->iget("greather_than_14");

        $wh = "";
        $wh_2 = "";
        if ($greather_than_14 == 'true') {
            $wh_2 = " AND DATE_ADD(DATE_FORMAT(created_at_last,'%Y-%m-%d'), INTERVAL 14 DAY) <= CURDATE() ";
        }

        if ($this->session->userdata("level_user_id") == "7") {
            if ($jenis_pemeriksaan && $hasil_pemeriksaan) {
                $wh = "AND jenis_pemeriksaan_id_last = '" . $jenis_pemeriksaan . "' AND hasil_pemeriksaan_id_last = '" . $hasil_pemeriksaan . "'";
            } else if ($jenis_pemeriksaan && !$hasil_pemeriksaan) {
                $wh = "AND jenis_pemeriksaan_id_last = '" . $jenis_pemeriksaan . "'";
            }

            $data_peserta = $this->peserta_non_puskesmas_model->query("
            SELECT 
                id_peserta_non_puskesmas AS id_peserta,
                nama,
                nik,
                CONCAT(
                    '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                    '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                    '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
                    '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
                ) AS kolom,
                IF(peserta_id IS NOT NULL,CONCAT('<span class=\'badge badge-light badge-striped badge-striped-left border-left-success\'>Sinkronisasi data oleh Puskesmas ',nama_puskesmas,'</span>'),'') AS sinkronisasi_data
                FROM (
                    SELECT 
                    id_trx_pemeriksaan_non_puskesmas,
                    IF(level_user_id IN ('2','7'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan,
                    IF(level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan,
                    IF(level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other,
                    IF(level_user_id IN ('2','7'),a.class_badge,NULL) AS class_badge,
                    IF(level_user_id IN ('2','7'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan,
                    IF(level_user_id IN ('2','7'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan,
                    IF(level_user_id IN ('2','7'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat,
                    id_peserta_non_puskesmas,
                    peserta_non_puskesmas.nama,
                    nik,
                    peserta_id,
                    a.created_at AS tanggal_urutan_pemeriksaan,
                    c.*,
                    d.nama_puskesmas
                    FROM peserta_non_puskesmas
                    LEFT JOIN
                    (
                    SELECT 
                        id_trx_pemeriksaan_non_puskesmas,
                        peserta_non_puskesmas_id,
                        level_user_id,
                        IF(
                        trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NULL AND hasil_pemeriksaan_id IS NULL,
                            IF(is_sembuh = '1',
                            '<span class=\'badge badge-success\'>Sembuh</span>',
                            IF(is_meninggal = '1',
                            '<span class=\'badge badge-secondary\'>Meninggal</span>',
                            IF(is_probable = '1',
                            '<span class=\'badge badge-secondary\'>Probable</span>',''
                            )
                            )
                            ),
                            IF(
                            trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NULL,
                            '<span class=\'badge badge-secondary\'>Hasil Belum Keluar</span>',
                            IF(
                                trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NOT NULL,
                                IF(
                                class_badge = '1',
                                    CONCAT('<span class=\'badge badge-success\'>',nama_hasil_pemeriksaan,'</span>'),
                                IF(class_badge = '2',
                                    CONCAT('<span class=\'badge badge-warning\'>',nama_hasil_pemeriksaan,'</span>'),''
                                )
                                ),
                                ''
                            )
                            )
                        ) AS nama_hasil_pemeriksaan,
                        IF(usulan_faskes = '0',
                        '<span class=\'badge badge-danger\'>Usulan Faskes : SWAB PCR</span>',
                        IF(usulan_faskes = '1',
                        '<span class=\'badge badge-danger\'>Usulan Faskes : Isolasi Mandiri</span>',
                        IF(usulan_faskes = '2',
                        '<span class=\'badge badge-success\'>Usulan Faskes : Bebas Isolasi Mandiri</span>',
                        IF(usulan_faskes = '3',
                        '<span class=\'badge badge-warning\'>Usulan Faskes : Rujuk Rumah Sakit</span>',''
                        )
                        )
                        )
                        ) AS nama_jenis_pemeriksaan_other,
                        IF(trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NOT NULL,nama_jenis_pemeriksaan,'') AS nama_jenis_pemeriksaan,
                        IFNULL(is_sembuh,''),
                        IFNULL(is_meninggal,''),
                        IFNULL(is_probable,''),
                        IFNULL(class_badge,'') AS class_badge,
                        IFNULL(usulan_faskes,'') AS usulan_pemeriksaan,
                        IFNULL(DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_buat_pemeriksaan,
                        IFNULL(DATE_FORMAT(trx_pemeriksaan_non_puskesmas.updated_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_ubah_pemeriksaan,
                        IFNULL(nama_lengkap,'') AS nama_asal_pembuat,
                        trx_pemeriksaan_non_puskesmas.created_at
                    FROM trx_pemeriksaan_non_puskesmas
                    LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL
                    LEFT JOIN jenis_pemeriksaan ON id_jenis_pemeriksaan=trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id AND jenis_pemeriksaan.deleted_at IS NULL
                    INNER JOIN user ON id_user_created=id_user
                    WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS a ON a.peserta_non_puskesmas_id=id_peserta_non_puskesmas
                    LEFT JOIN 
                    (
                        SELECT 
                            trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas AS id_trx_pemeriksaan_non_puskesmas_last,
                            peserta_non_puskesmas_id,
                            level_user_id AS level_user_id_last,
                            class_badge AS class_badge_last,
                            usulan_faskes AS usulan_faskes_pemeriksaan_last,
                            trx_pemeriksaan_non_puskesmas.created_at AS created_at_last,
                            DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') AS created_pemeriksaan_last,
                            status_rawat AS status_rawat_last,
                            trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id AS jenis_pemeriksaan_id_last,
                            hasil_pemeriksaan_id AS hasil_pemeriksaan_id_last,
                            is_sembuh AS is_sembuh_last,
                            is_meninggal AS is_meninggal_last,
                            is_probable AS is_probable_last
                        FROM (
                            SELECT SUBSTRING_INDEX(GROUP_CONCAT(id_trx_pemeriksaan_non_puskesmas ORDER BY trx_pemeriksaan_non_puskesmas.created_at DESC),',',1) AS id_trx_pemeriksaan_non_puskesmas
                            FROM trx_pemeriksaan_non_puskesmas
                            WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                            GROUP BY peserta_non_puskesmas_id
                        ) AS tbl
                        LEFT JOIN trx_pemeriksaan_non_puskesmas ON trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas = tbl.id_trx_pemeriksaan_non_puskesmas
                        LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                        LEFT JOIN user ON id_user=id_user_created
                        WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS c ON c.peserta_non_puskesmas_id=id_peserta_non_puskesmas
                    LEFT JOIN
                    (
                    SELECT id_peserta,nama_puskesmas
                    FROM peserta
                    JOIN user ON id_user_created=id_user
                    JOIN master_puskesmas ON puskesmas_id=id_master_puskesmas
                    ) AS d ON d.id_peserta=peserta_id
                    WHERE peserta_non_puskesmas.id_user_created='" . $this->session->userdata("id_user") . "' AND peserta_non_puskesmas.deleted_at IS NULL
                    ORDER BY id_trx_pemeriksaan_non_puskesmas ASC
                ) AS tbl
                WHERE created_pemeriksaan_last BETWEEN '" . $start_date . "' AND '" . $end_date . "'
                {$wh}
                GROUP BY id_peserta_non_puskesmas
                ORDER BY id_trx_pemeriksaan_non_puskesmas_last DESC
            ")->result();
        } else {
            if ($jenis_pemeriksaan > '9000') {
                if ($jenis_pemeriksaan == '9001') {
                    $wh = " AND is_sembuh_last = '1'";
                } else if ($jenis_pemeriksaan == '9002') {
                    $wh = " AND is_meninggal_last = '1'";
                } else if ($jenis_pemeriksaan == '9003') {
                    $wh = " AND is_probable_last = '1'";
                } else if ($jenis_pemeriksaan == '9004') {
                    $wh = " AND usulan_faskes_pemeriksaan = '0'";
                } else if ($jenis_pemeriksaan == '9005') {
                    $wh = " AND usulan_faskes_pemeriksaan = '1'";
                } else if ($jenis_pemeriksaan == '9006') {
                    $wh = " AND usulan_faskes_pemeriksaan = '2'";
                } else if ($jenis_pemeriksaan == '9007') {
                    $wh = "AND status_rawat = '3'";
                }
            } else if ($jenis_pemeriksaan && $hasil_pemeriksaan) {
                $wh = "AND jenis_pemeriksaan_id_last = '" . $jenis_pemeriksaan . "' AND hasil_pemeriksaan_id = '" . $hasil_pemeriksaan . "'";
            } else if ($jenis_pemeriksaan && !$hasil_pemeriksaan) {
                $wh = "AND jenis_pemeriksaan_id_last = '" . $jenis_pemeriksaan . "'";
            }
            $data_peserta = $this->peserta_model->query("
            SELECT
                id_peserta,
                nama,
                nik,
                CONCAT(
                    '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                    '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                    '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
                    '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
                ) AS kolom_faskes,

                CONCAT(
                    '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                    '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                    '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
                    '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
                ) AS kolom_labkesda,
                
                CONCAT(
                    '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                    '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                    '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
                    '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
                ) AS kolom_rs
                FROM (
                    SELECT 
                        id_trx_pemeriksaan,
                        IF(level_user_id IN ('2','7'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_faskes,
                        IF(level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_faskes,
                        IF(level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_faskes,
                        IF(level_user_id IN ('2','7'),a.class_badge,NULL) AS class_badge_faskes,
                        IF(level_user_id IN ('2','7'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_faskes,
                        IF(level_user_id IN ('2','7'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_faskes,
                        IF(level_user_id IN ('2','7'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_faskes,
                        
                        IF(level_user_id NOT IN ('2','7') AND level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_labkesda,
                        IF(level_user_id NOT IN ('2','7') AND level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_labkesda,
                        IF(level_user_id NOT IN ('2','7') AND level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_labkesda,
                        IF(level_user_id NOT IN ('2','7') AND level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.class_badge,NULL) AS class_badge_labkesda,
                        IF(level_user_id NOT IN ('2','7') AND level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_labkesda,
                        IF(level_user_id NOT IN ('2','7') AND level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_labkesda,
                        IF(level_user_id NOT IN ('2','7') AND level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_labkesda,
                        
                        IF(level_user_id NOT IN ('2','7','3') AND level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_rs,
                        IF(level_user_id NOT IN ('2','7','3') AND level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_rs,
                        IF(level_user_id NOT IN ('2','7','3') AND level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_rs,
                        IF(level_user_id NOT IN ('2','7','3') AND level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.class_badge,NULL) AS class_badge_rs,
                        IF(level_user_id NOT IN ('2','7','3') AND level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_rs,
                        IF(level_user_id NOT IN ('2','7','3') AND level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_rs,
                        IF(level_user_id NOT IN ('2','7','3') AND level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_rs,
                        id_peserta,
                        peserta.nama,
                        nik,
                        kategori_data,
                        a.created_at AS tanggal_urutan_pemeriksaan,
                        c.*
                    FROM peserta
                    LEFT JOIN
                    (
                        SELECT 
                            id_trx_pemeriksaan,
                            peserta_id,
                            level_user_id,
                            IF(
                                trx_pemeriksaan.jenis_pemeriksaan_id IS NULL AND hasil_pemeriksaan_id IS NULL,
                                    IF(is_sembuh = '1',
                                        '<span class=\'badge badge-success\'>Sembuh</span>',
                                    IF(is_meninggal = '1',
                                        '<span class=\'badge badge-secondary\'>Meninggal</span>',
                                    IF(is_probable = '1',
                                        '<span class=\'badge badge-secondary\'>Probable</span>',''
                                    )
                                    )
                                    ),
                                    IF(
                                        trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NULL,
                                        '<span class=\'badge badge-secondary\'>Hasil Belum Keluar</span>',
                                        IF(
                                            trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NOT NULL,
                                            IF(
                                                class_badge = '1',
                                                    CONCAT('<span class=\'badge badge-success\'>',nama_hasil_pemeriksaan,'</span>'),
                                                IF(class_badge = '2',
                                                    CONCAT('<span class=\'badge badge-warning\'>',nama_hasil_pemeriksaan,'</span>'),''
                                                )
                                            ),
                                            ''
                                        )
                                    )
                            ) AS nama_hasil_pemeriksaan,
                            IF(usulan_faskes = '0',
                                '<span class=\'badge badge-danger\'>Usulan Faskes : SWAB PCR</span>',
                            IF(usulan_faskes = '1',
                                '<span class=\'badge badge-danger\'>Usulan Faskes : Isolasi Mandiri</span>',
                            IF(usulan_faskes = '2',
                                '<span class=\'badge badge-success\'>Usulan Faskes : Bebas Isolasi Mandiri</span>',
                            IF(usulan_faskes = '3',
                                '<span class=\'badge badge-warning\'>Usulan Faskes : Rujuk Rumah Sakit</span>',''
                            )
                            )
                            )
                            ) AS nama_jenis_pemeriksaan_other,
                            IF(trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL,nama_jenis_pemeriksaan,'') AS nama_jenis_pemeriksaan,
                            IFNULL(is_sembuh,''),
                            IFNULL(is_meninggal,''),
                            IFNULL(is_probable,''),
                            IFNULL(class_badge,'') AS class_badge,
                            IFNULL(usulan_faskes,'') AS usulan_pemeriksaan,
                            IFNULL(DATE_FORMAT(trx_pemeriksaan.created_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_buat_pemeriksaan,
                            IFNULL(DATE_FORMAT(trx_pemeriksaan.updated_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_ubah_pemeriksaan,
                            IFNULL(nama_lengkap,'') AS nama_asal_pembuat,
                            trx_pemeriksaan.created_at
                        FROM trx_pemeriksaan
                        LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL
                        LEFT JOIN jenis_pemeriksaan ON id_jenis_pemeriksaan=trx_pemeriksaan.jenis_pemeriksaan_id AND jenis_pemeriksaan.deleted_at IS NULL
                        INNER JOIN user ON id_user_created=id_user
                        WHERE trx_pemeriksaan.deleted_at IS NULL
                    ) AS a ON a.peserta_id=id_peserta
                    LEFT JOIN 
                    (
                        SELECT 
                            trx_pemeriksaan.id_trx_pemeriksaan AS id_trx_pemeriksaan_last,
                            peserta_id,
                            level_user_id AS level_user_id_last,
                            class_badge AS class_badge_last,
                            usulan_faskes AS usulan_faskes_pemeriksaan_last,
                            trx_pemeriksaan.created_at AS created_at_last,
                            DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') AS created_pemeriksaan_last,
                            status_rawat AS status_rawat_last,
                            trx_pemeriksaan.jenis_pemeriksaan_id AS jenis_pemeriksaan_id_last,
                            hasil_pemeriksaan_id,is_sembuh AS is_sembuh_last,
                            is_meninggal AS is_meninggal_last,
                            is_probable AS is_probable_last
                        FROM (
                            SELECT SUBSTRING_INDEX(GROUP_CONCAT(id_trx_pemeriksaan ORDER BY trx_pemeriksaan.created_at DESC),',',1) AS id_trx_pemeriksaan
                            FROM trx_pemeriksaan
                            WHERE trx_pemeriksaan.deleted_at IS NULL
                            GROUP BY peserta_id
                        ) AS tbl
                        LEFT JOIN trx_pemeriksaan ON trx_pemeriksaan.id_trx_pemeriksaan = tbl.id_trx_pemeriksaan
                        LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                        LEFT JOIN user ON id_user=id_user_created 
                        WHERE trx_pemeriksaan.deleted_at IS NULL
                    ) AS c ON c.peserta_id=id_peserta
                    WHERE peserta.id_user_created='" . $this->session->userdata("id_user") . "' 
                    OR kelurahan_master_wilayah_id IN 
                    (
                        SELECT id_master_wilayah
                        FROM master_wilayah
                        WHERE puskesmas_id = '" . $this->session->userdata("puskesmas_id") . "'
                    ) AND peserta.deleted_at IS NULL
                    ORDER BY id_trx_pemeriksaan ASC
                ) AS tbl
                WHERE IF(kategori_data IS NOT NULL,(kategori_data = '2' OR (kategori_data = '1' AND class_badge_last IS NOT NULL)),1) AND created_pemeriksaan_last BETWEEN '" . $start_date . "' AND '" . $end_date . "'
                {$wh_2}
                {$wh}
                GROUP BY id_peserta
                ORDER BY id_trx_pemeriksaan_last DESC
            ")->result();
        }

        $templist = array();
        foreach ($data_peserta as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
            $templist[$key]['stat_button_edit'] = true;
            $templist[$key]['stat_button_delete'] = true;
            $templist[$key]['stat_button_view'] = true;
            $templist[$key]['is_lock'] = true;
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_labkesda()
    {
        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");
        $jenis_pemeriksaan = decrypt_data($this->iget("jenis_pemeriksaan"));
        $hasil_pemeriksaan = decrypt_data($this->iget("hasil_pemeriksaan"));

        $wh_2 = "";
        if ($jenis_pemeriksaan > '9000') {
            if ($jenis_pemeriksaan == '9001') {
                $wh_2 = " AND is_sembuh_last = '1'";
            } else if ($jenis_pemeriksaan == '9002') {
                $wh_2 = " AND is_meninggal_last = '1'";
            } else if ($jenis_pemeriksaan == '9003') {
                $wh_2 = " AND is_probable_last = '1'";
            } else if ($jenis_pemeriksaan == '9004') {
                $wh_2 = " AND usulan_faskes_pemeriksaan_last = '0'";
            } else if ($jenis_pemeriksaan == '9005') {
                $wh_2 = " AND usulan_faskes_pemeriksaan_last = '1'";
            } else if ($jenis_pemeriksaan == '9006') {
                $wh_2 = " AND usulan_faskes_pemeriksaan_last = '2'";
            } else if ($jenis_pemeriksaan == '9007') {
                $wh_2 = "AND status_rawat_last = '3'";
            }
        } else if ($jenis_pemeriksaan && $hasil_pemeriksaan) {
            $wh_2 = " AND jenis_pemeriksaan_id_last = '" . $jenis_pemeriksaan . "' AND hasil_pemeriksaan_id = '" . $hasil_pemeriksaan . "'";
        } else if ($jenis_pemeriksaan && !$hasil_pemeriksaan) {
            $wh_2 = " AND jenis_pemeriksaan_id_last = '" . $jenis_pemeriksaan . "'";
        }

        $wh = "AND 
            (
                (
                    IF(level_user_id_last = '2' AND (usulan_faskes_pemeriksaan_last = '0' AND (class_badge_last = '2' OR class_badge_last IS NULL)),1,0)
                )
                OR 
                (
                    (level_user_id_last = '4' AND jenis_pemeriksaan_id_last != '1') OR 
                    (level_user_id_last = '4' AND jenis_pemeriksaan_id_last = '1' AND (class_badge_last IS NULL OR class_badge_last = '1'))
                )
                OR 
                (
                    (level_user_id_last = '3')
                )
                OR 
                (
                    (level_user_id_last = '6')
                )
                OR
                (
                    level_user_id_peserta = '" . $this->session->userdata("level_user_id") . "'
                ) 
                OR
                (
                    level_user_id_peserta = '6'
                )
            )";

        $data_peserta = $this->peserta_model->query("
            SELECT
                id_peserta,
                nama,
                nik,
                CONCAT(
                    '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                    '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                    '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
                    '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
                ) AS kolom_faskes,
    
                CONCAT(
                    '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                    '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                    '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
                    '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
                ) AS kolom_labkesda,
    
                CONCAT(
                    '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                    '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                    '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
                    '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
                ) AS kolom_rs,
                jumlah_swab,
                rekam_medis_peserta
                FROM (
                    SELECT 
                    id_trx_pemeriksaan,
                    IF(a.level_user_id IN ('2','7'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_faskes,
                    IF(a.level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_faskes,
                    IF(a.level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_faskes,
                    IF(a.level_user_id IN ('2','7'),a.class_badge,NULL) AS class_badge_faskes,
                    IF(a.level_user_id IN ('2','7'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_faskes,
                    IF(a.level_user_id IN ('2','7'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_faskes,
                    IF(a.level_user_id IN ('2','7'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_faskes,
                    
                    IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_labkesda,
                    IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_labkesda,
                    IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_labkesda,
                    IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.class_badge,NULL) AS class_badge_labkesda,
                    IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_labkesda,
                    IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_labkesda,
                    IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_labkesda,
                    
                    IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_rs,
                    IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_rs,
                    IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_rs,
                    IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.class_badge,NULL) AS class_badge_rs,
                    IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_rs,
                    IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_rs,
                    IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_rs,
                    id_peserta,
                    peserta.nama,
                    nik,
                    kategori_data,
                    a.created_at AS tanggal_urutan_pemeriksaan,
                    c.*,
                    usr.level_user_id AS level_user_id_peserta,
                    d.jumlah_swab,
                    e.rekam_medis_peserta
                    FROM peserta
                    LEFT JOIN
                    (
                    SELECT 
                        id_trx_pemeriksaan,
                        peserta_id,
                        level_user_id,
                        IF(
                            trx_pemeriksaan.jenis_pemeriksaan_id IS NULL AND hasil_pemeriksaan_id IS NULL,
                                IF(is_sembuh = '1',
                            '<span class=\'badge badge-success\'>Sembuh</span>',
                                IF(is_meninggal = '1',
                            '<span class=\'badge badge-secondary\'>Meninggal</span>',
                                IF(is_probable = '1',
                            '<span class=\'badge badge-secondary\'>Probable</span>',''
                            )
                            )
                            ),
                            IF(
                                trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NULL,
                                    '<span class=\'badge badge-secondary\'>Hasil Belum Keluar</span>',
                                IF(
                                    trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NOT NULL,
                                IF(
                                    class_badge = '1',
                                        CONCAT('<span class=\'badge badge-success\'>',nama_hasil_pemeriksaan,'</span>'),
                                    IF(class_badge = '2',
                                        CONCAT('<span class=\'badge badge-warning\'>',nama_hasil_pemeriksaan,'</span>'),''
                                    )
                                ),
                                ''
                            )
                            )
                        ) AS nama_hasil_pemeriksaan,
                        IF(usulan_faskes = '0',
                        '<span class=\'badge badge-danger\'>Usulan Faskes : SWAB PCR</span>',
                        IF(usulan_faskes = '1',
                        '<span class=\'badge badge-danger\'>Usulan Faskes : Isolasi Mandiri</span>',
                        IF(usulan_faskes = '2',
                        '<span class=\'badge badge-success\'>Usulan Faskes : Bebas Isolasi Mandiri</span>',
                        IF(usulan_faskes = '3',
                        '<span class=\'badge badge-warning\'>Usulan Faskes : Rujuk Rumah Sakit</span>',''
                        )
                        )
                        )
                        ) AS nama_jenis_pemeriksaan_other,
                        IF(trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL,nama_jenis_pemeriksaan,'') AS nama_jenis_pemeriksaan,
                        IFNULL(is_sembuh,''),
                        IFNULL(is_meninggal,''),
                        IFNULL(is_probable,''),
                        IFNULL(class_badge,'') AS class_badge,
                        IFNULL(usulan_faskes,'') AS usulan_pemeriksaan,
                        IFNULL(DATE_FORMAT(trx_pemeriksaan.created_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_buat_pemeriksaan,
                        IFNULL(DATE_FORMAT(trx_pemeriksaan.updated_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_ubah_pemeriksaan,
                        IFNULL(nama_lengkap,'') AS nama_asal_pembuat,
                        trx_pemeriksaan.created_at
                    FROM trx_pemeriksaan
                    LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL
                    LEFT JOIN jenis_pemeriksaan ON id_jenis_pemeriksaan=trx_pemeriksaan.jenis_pemeriksaan_id AND jenis_pemeriksaan.deleted_at IS NULL
                    INNER JOIN user ON id_user_created=id_user
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    ) AS a ON a.peserta_id=id_peserta
                    LEFT JOIN 
                    (
                        SELECT 
                        trx_pemeriksaan.id_trx_pemeriksaan AS id_trx_pemeriksaan_last,
                            peserta_id,
                            level_user_id AS level_user_id_last,
                            class_badge AS class_badge_last,
                            usulan_faskes AS usulan_faskes_pemeriksaan_last,
                            trx_pemeriksaan.created_at AS created_at_last,
                            DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') AS created_pemeriksaan_last,
                            status_rawat AS status_rawat_last,
                            trx_pemeriksaan.jenis_pemeriksaan_id AS jenis_pemeriksaan_id_last,
                            hasil_pemeriksaan_id,is_sembuh AS is_sembuh_last,
                            is_meninggal AS is_meninggal_last,
                            is_probable AS is_probable_last
                        FROM (
                            SELECT SUBSTRING_INDEX(GROUP_CONCAT(id_trx_pemeriksaan ORDER BY trx_pemeriksaan.created_at DESC),',',1) AS id_trx_pemeriksaan
                            FROM trx_pemeriksaan
                            WHERE trx_pemeriksaan.deleted_at IS NULL
                            GROUP BY peserta_id
                        ) AS tbl
                        LEFT JOIN trx_pemeriksaan ON trx_pemeriksaan.id_trx_pemeriksaan = tbl.id_trx_pemeriksaan
                        LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                        LEFT JOIN user ON id_user=id_user_created 
                        WHERE trx_pemeriksaan.deleted_at IS NULL
                    ) AS c ON c.peserta_id=id_peserta
                    LEFT JOIN user AS usr ON peserta.id_user_created=id_user
                    LEFT JOIN 
                    (
                    SELECT COUNT(*) AS jumlah_swab,peserta_id
                    FROM trx_pemeriksaan
                    WHERE trx_pemeriksaan.deleted_at IS NULL AND jenis_pemeriksaan_id = '1'
                    GROUP BY peserta_id
                    ) AS d ON d.peserta_id=id_peserta
                    LEFT JOIN 
                    (
                    SELECT 
                    GROUP_CONCAT(STATUS ORDER BY id_trx_terkonfirmasi ASC SEPARATOR '|') AS rekam_medis_peserta,
                    peserta_id
                    FROM trx_terkonfirmasi 
                    WHERE trx_terkonfirmasi.deleted_at IS NULL
                    GROUP BY peserta_id
                    ) AS e ON e.peserta_id=id_peserta
                    WHERE peserta.deleted_at IS NULL
                    ORDER BY id_trx_pemeriksaan ASC
                ) AS tbl
                WHERE IF(kategori_data IS NOT NULL,(kategori_data = '2' OR (kategori_data = '1' AND class_badge_last IS NOT NULL)),1)
                {$wh} AND created_pemeriksaan_last BETWEEN '" . $start_date . "' AND '" . $end_date . "' {$wh_2}
                GROUP BY id_peserta
                ORDER BY id_trx_pemeriksaan_last DESC
            ")->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {
            $bool_status_sembuh = false;
            $bool_status_reinfeksi = false;
            $count_reinfeksi = 0;

            $rekam_medis_peserta = explode("|", $row->rekam_medis_peserta);

            if ($row->rekam_medis_peserta) {
                foreach ($rekam_medis_peserta as $key_resume => $val_resume) {
                    if ($val_resume == '1') {
                        if ($bool_status_sembuh) {
                            $bool_status_reinfeksi = true;
                            $count_reinfeksi++;
                            $bool_status_sembuh = false;
                        }
                    } else if ($val_resume == '2') {
                        $bool_status_sembuh = true;
                    }
                }

                if ($bool_status_reinfeksi) {
                    $templist[$key]['status_reinfeksi'] = "Ya";
                    $templist[$key]['kali_reinfeksi'] = $count_reinfeksi;
                } else {
                    $templist[$key]['status_reinfeksi'] = "Tidak";
                    $templist[$key]['kali_reinfeksi'] = "";
                }
            } else {
                $templist[$key]['status_reinfeksi'] = "";
                $templist[$key]['kali_reinfeksi'] = "";
            }

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
            $templist[$key]['stat_button'] = true;
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_rs()
    {

        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");
        $jenis_pemeriksaan = decrypt_data($this->iget("jenis_pemeriksaan"));
        $hasil_pemeriksaan = decrypt_data($this->iget("hasil_pemeriksaan"));

        $wh = "";
        if ($jenis_pemeriksaan > '9000') {
            if ($jenis_pemeriksaan == '9001') {
                $wh = " AND is_sembuh_last = '1'";
            } else if ($jenis_pemeriksaan == '9002') {
                $wh = " AND is_meninggal_last = '1'";
            } else if ($jenis_pemeriksaan == '9003') {
                $wh = " AND is_probable_last = '1'";
            } else if ($jenis_pemeriksaan == '9004') {
                $wh = " AND usulan_faskes_pemeriksaan_last = '0'";
            } else if ($jenis_pemeriksaan == '9005') {
                $wh = " AND usulan_faskes_pemeriksaan_last = '1'";
            } else if ($jenis_pemeriksaan == '9006') {
                $wh = " AND usulan_faskes_pemeriksaan_last = '2'";
            } else if ($jenis_pemeriksaan == '9007') {
                $wh = "AND status_rawat_last = '3'";
            }
        } else if ($jenis_pemeriksaan && $hasil_pemeriksaan) {
            $wh = " AND jenis_pemeriksaan_id_last = '" . $jenis_pemeriksaan . "' AND hasil_pemeriksaan_id_last = '" . $hasil_pemeriksaan . "'";
        } else if ($jenis_pemeriksaan && !$hasil_pemeriksaan) {
            $wh = " AND jenis_pemeriksaan_id_last = '" . $jenis_pemeriksaan . "'";
        }

        $wh2 = " AND 
                ((
                    ((level_user_id_last = '3' OR level_user_id_last = '4') AND class_badge_last = '2') OR
                    ((level_user_id_last = '3' OR level_user_id_last = '4') AND jenis_pemeriksaan_id_last = '1') OR
                    ((level_user_id_last = '2' AND usulan_faskes_pemeriksaan_last = '3'))
                ) OR (level_user_id_peserta_created='" . $this->session->userdata("level_user_id") . "'))";

        $data_peserta = $this->peserta_model->query("
        SELECT *
        FROM 
        (
            SELECT
                id_peserta,
                nama,
                nik,
                CONCAT(
                '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
                '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
                ) AS kolom_faskes,

                CONCAT(
                '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
                '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
                ) AS kolom_labkesda,
                
                CONCAT(
                '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
                '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
                ) AS kolom_rs,
                'rsud' AS peserta_dari,
                id_trx_pemeriksaan_last AS id_trx_pemeriksaan_last,
                level_user_id_peserta_created AS level_user_id_rsud,
                is_sembuh_last,
                is_meninggal_last,
                is_probable_last,
                usulan_faskes_pemeriksaan_last,
                jenis_pemeriksaan_id_last,
                hasil_pemeriksaan_id_last
                FROM (
                SELECT 
                    id_trx_pemeriksaan,
                    IF(a.level_user_id IN ('2','7'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_faskes,
                    IF(a.level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_faskes,
                    IF(a.level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_faskes,
                    IF(a.level_user_id IN ('2','7'),a.class_badge,NULL) AS class_badge_faskes,
                    IF(a.level_user_id IN ('2','7'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_faskes,
                    IF(a.level_user_id IN ('2','7'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_faskes,
                    IF(a.level_user_id IN ('2','7'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_faskes,
                    
                    IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_labkesda,
                    IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_labkesda,
                    IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_labkesda,
                    IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.class_badge,NULL) AS class_badge_labkesda,
                    IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_labkesda,
                    IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_labkesda,
                    IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_labkesda,
                    
                    IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_rs,
                    IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_rs,
                    IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_rs,
                    IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.class_badge,NULL) AS class_badge_rs,
                    IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_rs,
                    IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_rs,
                    IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_rs,
                    id_peserta,
                    peserta.nama,
                    a.created_at AS tanggal_urutan_pemeriksaan,
                    nik,
                    kategori_data,
                    c.*,
                    usr_peserta.level_user_id AS level_user_id_peserta_created
                FROM peserta
                LEFT JOIN
                (
                    SELECT 
                    id_trx_pemeriksaan,
                    peserta_id,
                    level_user_id,
                    IF(
                        trx_pemeriksaan.jenis_pemeriksaan_id IS NULL AND hasil_pemeriksaan_id IS NULL,
                        IF(is_sembuh = '1',
                            '<span class=\'badge badge-success\'>Sembuh</span>',
                        IF(is_meninggal = '1',
                            '<span class=\'badge badge-secondary\'>Meninggal</span>',
                        IF(is_probable = '1',
                            '<span class=\'badge badge-secondary\'>Probable</span>',''
                        )
                        )
                        ),
                        IF(
                            trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NULL,
                            '<span class=\'badge badge-secondary\'>Hasil Belum Keluar</span>',
                            IF(
                            trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NOT NULL,
                            IF(
                                class_badge = '1',
                                CONCAT('<span class=\'badge badge-success\'>',nama_hasil_pemeriksaan,'</span>'),
                                IF(class_badge = '2',
                                CONCAT('<span class=\'badge badge-warning\'>',nama_hasil_pemeriksaan,'</span>'),''
                                )
                            ),
                            ''
                            )
                        )
                    ) AS nama_hasil_pemeriksaan,
                    IF(usulan_faskes = '0',
                        '<span class=\'badge badge-danger\'>Usulan Faskes : SWAB PCR</span>',
                    IF(usulan_faskes = '1',
                        '<span class=\'badge badge-danger\'>Usulan Faskes : Isolasi Mandiri</span>',
                    IF(usulan_faskes = '2',
                        '<span class=\'badge badge-success\'>Usulan Faskes : Bebas Isolasi Mandiri</span>',
                    IF(usulan_faskes = '3',
                        '<span class=\'badge badge-warning\'>Usulan Faskes : Rujuk Rumah Sakit</span>',''
                    )
                    )
                    )
                    ) AS nama_jenis_pemeriksaan_other,
                    IF(trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL,nama_jenis_pemeriksaan,'') AS nama_jenis_pemeriksaan,
                    IFNULL(is_sembuh,''),
                    IFNULL(is_meninggal,''),
                    IFNULL(is_probable,''),
                    IFNULL(class_badge,'') AS class_badge,
                    IFNULL(usulan_faskes,'') AS usulan_pemeriksaan,
                    IFNULL(DATE_FORMAT(trx_pemeriksaan.created_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_buat_pemeriksaan,
                    IFNULL(DATE_FORMAT(trx_pemeriksaan.updated_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_ubah_pemeriksaan,
                    IFNULL(nama_lengkap,'') AS nama_asal_pembuat,
                    trx_pemeriksaan.created_at
                    FROM trx_pemeriksaan
                    LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL
                    LEFT JOIN jenis_pemeriksaan ON id_jenis_pemeriksaan=trx_pemeriksaan.jenis_pemeriksaan_id AND jenis_pemeriksaan.deleted_at IS NULL
                    INNER JOIN user ON id_user_created=id_user
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                ) AS a ON a.peserta_id=id_peserta
                LEFT JOIN 
                (
                    SELECT 
                        trx_pemeriksaan.id_trx_pemeriksaan AS id_trx_pemeriksaan_last,
                        peserta_id,
                        level_user_id AS level_user_id_last,
                        class_badge AS class_badge_last,
                        usulan_faskes AS usulan_faskes_pemeriksaan_last,
                        trx_pemeriksaan.created_at AS created_at_last,
                        DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') AS created_pemeriksaan_last,
                        status_rawat AS status_rawat_last,
                        trx_pemeriksaan.jenis_pemeriksaan_id AS jenis_pemeriksaan_id_last,
                        hasil_pemeriksaan_id AS hasil_pemeriksaan_id_last,
                        is_sembuh AS is_sembuh_last,
                        is_meninggal AS is_meninggal_last,
                        is_probable AS is_probable_last
                    FROM (
                        SELECT SUBSTRING_INDEX(GROUP_CONCAT(id_trx_pemeriksaan ORDER BY trx_pemeriksaan.created_at DESC),',',1) AS id_trx_pemeriksaan
                        FROM trx_pemeriksaan
                        WHERE trx_pemeriksaan.deleted_at IS NULL
                        GROUP BY peserta_id
                    ) AS tbl
                    LEFT JOIN trx_pemeriksaan ON trx_pemeriksaan.id_trx_pemeriksaan = tbl.id_trx_pemeriksaan
                    LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                    LEFT JOIN user ON id_user=id_user_created 
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                ) AS c ON c.peserta_id=id_peserta
                LEFT JOIN user AS usr_peserta ON peserta.id_user_created=id_user
                WHERE peserta.deleted_at IS NULL
                ORDER BY id_trx_pemeriksaan ASC
                ) AS tbl
            WHERE created_pemeriksaan_last BETWEEN '" . $start_date . "' AND '" . $end_date . "' 
            {$wh}
            {$wh2}
            GROUP BY id_peserta

            UNION 

            SELECT 
                id_peserta_non_puskesmas AS id_peserta,
                nama,
                nik,
                '' AS kolom_faskes,
                '' AS kolom_labkesda,
                CONCAT(
                    '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                    '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                    '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
                    '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
                ) AS kolom_rs,
                'ch' AS peserta_dari,
                id_trx_pemeriksaan_non_puskesmas_last AS id_trx_pemeriksaan_last,
                '' AS level_user_id_rsud,
                is_sembuh_last,
                is_meninggal_last,
                is_probable_last,
                usulan_faskes_pemeriksaan_last,
                jenis_pemeriksaan_id_last,
                hasil_pemeriksaan_id_last
                FROM (
                    SELECT 
                    id_trx_pemeriksaan_non_puskesmas,
                    IF(level_user_id IN ('2','7'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan,
                    IF(level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan,
                    IF(level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other,
                    IF(level_user_id IN ('2','7'),a.class_badge,NULL) AS class_badge,
                    IF(level_user_id IN ('2','7'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan,
                    IF(level_user_id IN ('2','7'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan,
                    IF(level_user_id IN ('2','7'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat,
                    id_peserta_non_puskesmas,
                    peserta_non_puskesmas.nama,
                    a.created_at AS tanggal_urutan_pemeriksaan,
                    nik,
                    peserta_id,
                    c.*
                    FROM peserta_non_puskesmas
                    LEFT JOIN
                    (
                        SELECT 
                        id_trx_pemeriksaan_non_puskesmas,
                        peserta_non_puskesmas_id,
                        level_user_id,
                        IF(
                        trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NULL AND hasil_pemeriksaan_id IS NULL,
                            IF(is_sembuh = '1',
                            '<span class=\'badge badge-success\'>Sembuh</span>',
                            IF(is_meninggal = '1',
                            '<span class=\'badge badge-secondary\'>Meninggal</span>',
                            IF(is_probable = '1',
                            '<span class=\'badge badge-secondary\'>Probable</span>',''
                            )
                            )
                            ),
                            IF(
                            trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NULL,
                            '<span class=\'badge badge-secondary\'>Hasil Belum Keluar</span>',
                            IF(
                            trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NOT NULL,
                            IF(
                            class_badge = '1',
                                CONCAT('<span class=\'badge badge-success\'>',nama_hasil_pemeriksaan,'</span>'),
                            IF(class_badge = '2',
                                CONCAT('<span class=\'badge badge-warning\'>',nama_hasil_pemeriksaan,'</span>'),''
                            )
                            ),
                            ''
                            )
                            )
                        ) AS nama_hasil_pemeriksaan,
                        IF(usulan_faskes = '0',
                        '<span class=\'badge badge-danger\'>Usulan Faskes : SWAB PCR</span>',
                        IF(usulan_faskes = '1',
                        '<span class=\'badge badge-danger\'>Usulan Faskes : Isolasi Mandiri</span>',
                        IF(usulan_faskes = '2',
                        '<span class=\'badge badge-success\'>Usulan Faskes : Bebas Isolasi Mandiri</span>',
                        IF(usulan_faskes = '3',
                        '<span class=\'badge badge-warning\'>Usulan Faskes : Rujuk Rumah Sakit</span>',''
                        )
                        )
                        )
                        ) AS nama_jenis_pemeriksaan_other,
                        IF(trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NOT NULL,nama_jenis_pemeriksaan,'') AS nama_jenis_pemeriksaan,
                        IFNULL(is_sembuh,''),
                        IFNULL(is_meninggal,''),
                        IFNULL(is_probable,''),
                        IFNULL(class_badge,'') AS class_badge,
                        IFNULL(usulan_faskes,'') AS usulan_pemeriksaan,
                        IFNULL(DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_buat_pemeriksaan,
                        IFNULL(DATE_FORMAT(trx_pemeriksaan_non_puskesmas.updated_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_ubah_pemeriksaan,
                        IFNULL(nama_lengkap,'') AS nama_asal_pembuat,
                        trx_pemeriksaan_non_puskesmas.created_at
                        FROM trx_pemeriksaan_non_puskesmas
                        LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL
                        LEFT JOIN jenis_pemeriksaan ON id_jenis_pemeriksaan=trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id AND jenis_pemeriksaan.deleted_at IS NULL
                        INNER JOIN user ON id_user_created=id_user
                        WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS a ON a.peserta_non_puskesmas_id=id_peserta_non_puskesmas
                    LEFT JOIN 
                    (
                        SELECT 
                            trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas AS id_trx_pemeriksaan_non_puskesmas_last,
                            peserta_non_puskesmas_id,
                            level_user_id AS level_user_id_last,
                            class_badge AS class_badge_last,
                            usulan_faskes AS usulan_faskes_pemeriksaan_last,
                            trx_pemeriksaan_non_puskesmas.created_at AS created_at_last,
                            DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') AS created_pemeriksaan_last,
                            status_rawat AS status_rawat_last,
                            trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id AS jenis_pemeriksaan_id_last,
                            hasil_pemeriksaan_id AS hasil_pemeriksaan_id_last,
                            is_sembuh AS is_sembuh_last,
                            is_meninggal AS is_meninggal_last,
                            is_probable AS is_probable_last
                        FROM (
                            SELECT SUBSTRING_INDEX(GROUP_CONCAT(id_trx_pemeriksaan_non_puskesmas ORDER BY trx_pemeriksaan_non_puskesmas.created_at DESC),',',1) AS id_trx_pemeriksaan_non_puskesmas
                            FROM trx_pemeriksaan_non_puskesmas
                            WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                            GROUP BY peserta_non_puskesmas_id
                        ) AS tbl
                        LEFT JOIN trx_pemeriksaan_non_puskesmas ON trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas = tbl.id_trx_pemeriksaan_non_puskesmas
                        LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                        LEFT JOIN user ON id_user=id_user_created
                        WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    ) AS c ON c.peserta_non_puskesmas_id=id_peserta_non_puskesmas
                    WHERE peserta_non_puskesmas.id_user_created='" . $this->config->item("id_user_citra_husada") . "' AND peserta_non_puskesmas.deleted_at IS NULL AND peserta_id IS NULL
                    ORDER BY id_trx_pemeriksaan_non_puskesmas ASC
                ) AS tbl
            WHERE created_pemeriksaan_last BETWEEN '" . $start_date . "' AND '" . $end_date . "' AND jenis_pemeriksaan_id_last = '1'
            {$wh}
            GROUP BY id_peserta_non_puskesmas
        ) AS tbl
        ORDER BY id_trx_pemeriksaan_last DESC
        ")->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
            $templist[$key]['id_trx_pemeriksaan_last_encrypt'] = encrypt_data($row->id_trx_pemeriksaan_last);
            if ($row->level_user_id_rsud == $this->session->userdata("level_user_id")) {
                $templist[$key]['stat_button'] = true;
            } else {
                $templist[$key]['stat_button'] = false;
            }

            $templist[$key]['stat_button_detail_peserta'] = true;
            if ($row->peserta_dari == "ch") {
                $templist[$key]['stat_button_detail_peserta'] = false;
            }

            $templist[$key]['stat_button_print'] = false;
            if ($row->jenis_pemeriksaan_id_last == '1') {
                $templist[$key]['stat_button_print'] = true;
            }
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_rs_lab()
    {

        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");
        $wh_arsip = "AND peserta.from_excel IS NULL";
        $wh = "";
        $wh = " AND
        (
            ((level_user_id_last = '3' OR level_user_id_last = '4') AND jenis_pemeriksaan_id_last = '1')
        )";

        $data_peserta = $this->peserta_model->query("
        SELECT *
        FROM 
        (
            SELECT
            id_peserta,
            nama,
            nik,
            CONCAT(
            '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
            '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
            '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
            '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
            ) AS kolom_faskes,

            CONCAT(
            '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
            '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
            '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
            '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
            ) AS kolom_labkesda,
            
            CONCAT(
            '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
            '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
            '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
            '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
            ) AS kolom_rs,
            IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tracking_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),'') AS tracking_pemeriksaan_rs,
            'rsud' AS peserta_dari,
            id_trx_pemeriksaan_last AS id_trx_pemeriksaan_last,
            level_user_id_peserta_created AS level_user_id_rsud,
            is_sembuh_last,
            is_meninggal_last,
            is_probable_last,
            usulan_faskes_pemeriksaan_last,
            jenis_pemeriksaan_id_last,
            hasil_pemeriksaan_id_last
            FROM (
            SELECT 
                id_trx_pemeriksaan,
                IF(a.level_user_id IN ('2','7'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_faskes,
                IF(a.level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_faskes,
                IF(a.level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_faskes,
                IF(a.level_user_id IN ('2','7'),a.class_badge,NULL) AS class_badge_faskes,
                IF(a.level_user_id IN ('2','7'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_faskes,
                IF(a.level_user_id IN ('2','7'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_faskes,
                IF(a.level_user_id IN ('2','7'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_faskes,
                
                IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_labkesda,
                IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_labkesda,
                IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_labkesda,
                IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.class_badge,NULL) AS class_badge_labkesda,
                IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_labkesda,
                IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_labkesda,
                IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_labkesda,
                
                IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_rs,
                IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_rs,
                IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_rs,
                IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.class_badge,NULL) AS class_badge_rs,
                IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_rs,
                IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_rs,
                IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_rs,
                IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.tracking_pemeriksaan,NULL) AS tracking_pemeriksaan_rs,
                id_peserta,
                peserta.nama,
                a.created_at AS tanggal_urutan_pemeriksaan,
                nik,
                kategori_data,
                c.*,
                usr_peserta.level_user_id AS level_user_id_peserta_created
            FROM peserta
            LEFT JOIN
            (
                SELECT 
                id_trx_pemeriksaan,
                peserta_id,
                level_user_id,
                IF(
                trx_pemeriksaan.jenis_pemeriksaan_id IS NULL AND hasil_pemeriksaan_id IS NULL,
                IF(is_sembuh = '1',
                    '<span class=\'badge badge-success\'>Sembuh</span>',
                IF(is_meninggal = '1',
                    '<span class=\'badge badge-secondary\'>Meninggal</span>',
                IF(is_probable = '1',
                    '<span class=\'badge badge-secondary\'>Probable</span>',''
                )
                )
                ),
                IF(
                    trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NULL,
                    '<span class=\'badge badge-secondary\'>Hasil Belum Keluar</span>',
                    IF(
                    trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NOT NULL,
                    IF(
                    class_badge = '1',
                    CONCAT('<span class=\'badge badge-success\'>',nama_hasil_pemeriksaan,'</span>'),
                    IF(class_badge = '2',
                    CONCAT('<span class=\'badge badge-warning\'>',nama_hasil_pemeriksaan,'</span>'),''
                    )
                    ),
                    ''
                    )
                )
                ) AS nama_hasil_pemeriksaan,
                IF(usulan_faskes = '0',
                '<span class=\'badge badge-danger\'>Usulan Faskes : SWAB PCR</span>',
                IF(usulan_faskes = '1',
                '<span class=\'badge badge-danger\'>Usulan Faskes : Isolasi Mandiri</span>',
                IF(usulan_faskes = '2',
                '<span class=\'badge badge-success\'>Usulan Faskes : Bebas Isolasi Mandiri</span>',
                IF(usulan_faskes = '3',
                '<span class=\'badge badge-warning\'>Usulan Faskes : Rujuk Rumah Sakit</span>',''
                )
                )
                )
                ) AS nama_jenis_pemeriksaan_other,
                IF(trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL,nama_jenis_pemeriksaan,'') AS nama_jenis_pemeriksaan,
                IFNULL(is_sembuh,''),
                IFNULL(is_meninggal,''),
                IFNULL(is_probable,''),
                IFNULL(class_badge,'') AS class_badge,
                IFNULL(usulan_faskes,'') AS usulan_pemeriksaan,
                IFNULL(DATE_FORMAT(trx_pemeriksaan.created_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_buat_pemeriksaan,
                IFNULL(DATE_FORMAT(trx_pemeriksaan.updated_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_ubah_pemeriksaan,
                IFNULL(nama_lengkap,'') AS nama_asal_pembuat,
                trx_pemeriksaan.created_at,
                tracking_pemeriksaan
                FROM trx_pemeriksaan
                LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL
                LEFT JOIN jenis_pemeriksaan ON id_jenis_pemeriksaan=trx_pemeriksaan.jenis_pemeriksaan_id AND jenis_pemeriksaan.deleted_at IS NULL
                INNER JOIN user ON id_user_created=id_user
                WHERE trx_pemeriksaan.deleted_at IS NULL
            ) AS a ON a.peserta_id=id_peserta
            LEFT JOIN 
            (
                SELECT 
                trx_pemeriksaan.id_trx_pemeriksaan AS id_trx_pemeriksaan_last,
                peserta_id,
                level_user_id AS level_user_id_last,
                class_badge AS class_badge_last,
                usulan_faskes AS usulan_faskes_pemeriksaan_last,
                trx_pemeriksaan.created_at AS created_at_last,
                DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') AS created_pemeriksaan_last,
                status_rawat AS status_rawat_last,
                trx_pemeriksaan.jenis_pemeriksaan_id AS jenis_pemeriksaan_id_last,
                hasil_pemeriksaan_id AS hasil_pemeriksaan_id_last,
                is_sembuh AS is_sembuh_last,
                is_meninggal AS is_meninggal_last,
                is_probable AS is_probable_last
                FROM (
                SELECT SUBSTRING_INDEX(GROUP_CONCAT(id_trx_pemeriksaan ORDER BY trx_pemeriksaan.created_at DESC),',',1) AS id_trx_pemeriksaan
                FROM trx_pemeriksaan
                WHERE trx_pemeriksaan.deleted_at IS NULL
                GROUP BY peserta_id
                ) AS tbl
                LEFT JOIN trx_pemeriksaan ON trx_pemeriksaan.id_trx_pemeriksaan = tbl.id_trx_pemeriksaan
                LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                LEFT JOIN user ON id_user=id_user_created 
                WHERE trx_pemeriksaan.deleted_at IS NULL
            ) AS c ON c.peserta_id=id_peserta
            LEFT JOIN user AS usr_peserta ON peserta.id_user_created=id_user
            WHERE peserta.deleted_at IS NULL
            ORDER BY id_trx_pemeriksaan ASC
            ) AS tbl
            WHERE created_pemeriksaan_last BETWEEN '" . $start_date . "' AND '" . $end_date . "'
            {$wh}
            GROUP BY id_peserta

            UNION 

            SELECT 
            id_peserta_non_puskesmas AS id_peserta,
            nama,
            nik,
            '' AS kolom_faskes,
            '' AS kolom_labkesda,
            CONCAT(
                '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
                '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
            ) AS kolom_rs,
            IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tracking_pemeriksaan ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),'') AS tracking_pemeriksaan,
            'ch' AS peserta_dari,
            id_trx_pemeriksaan_non_puskesmas_last AS id_trx_pemeriksaan_last,
            '' AS level_user_id_rsud,
            is_sembuh_last,
            is_meninggal_last,
            is_probable_last,
            usulan_faskes_pemeriksaan_last,
            jenis_pemeriksaan_id_last,
            hasil_pemeriksaan_id_last
            FROM (
                SELECT 
                id_trx_pemeriksaan_non_puskesmas,
                IF(level_user_id IN ('2','7'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan,
                IF(level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan,
                IF(level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other,
                IF(level_user_id IN ('2','7'),a.class_badge,NULL) AS class_badge,
                IF(level_user_id IN ('2','7'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan,
                IF(level_user_id IN ('2','7'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan,
                IF(level_user_id IN ('2','7'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat,
                id_peserta_non_puskesmas,
                peserta_non_puskesmas.nama,
                a.created_at AS tanggal_urutan_pemeriksaan,
                a.tracking_pemeriksaan,
                nik,
                peserta_id,
                c.*
                FROM peserta_non_puskesmas
                LEFT JOIN
                (
                SELECT 
                id_trx_pemeriksaan_non_puskesmas,
                peserta_non_puskesmas_id,
                level_user_id,
                IF(
                trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NULL AND hasil_pemeriksaan_id IS NULL,
                    IF(is_sembuh = '1',
                    '<span class=\'badge badge-success\'>Sembuh</span>',
                    IF(is_meninggal = '1',
                    '<span class=\'badge badge-secondary\'>Meninggal</span>',
                    IF(is_probable = '1',
                    '<span class=\'badge badge-secondary\'>Probable</span>',''
                    )
                    )
                    ),
                    IF(
                    trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NULL,
                    '<span class=\'badge badge-secondary\'>Hasil Belum Keluar</span>',
                    IF(
                    trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NOT NULL,
                    IF(
                    class_badge = '1',
                    CONCAT('<span class=\'badge badge-success\'>',nama_hasil_pemeriksaan,'</span>'),
                    IF(class_badge = '2',
                    CONCAT('<span class=\'badge badge-warning\'>',nama_hasil_pemeriksaan,'</span>'),''
                    )
                    ),
                    ''
                    )
                    )
                ) AS nama_hasil_pemeriksaan,
                IF(usulan_faskes = '0',
                '<span class=\'badge badge-danger\'>Usulan Faskes : SWAB PCR</span>',
                IF(usulan_faskes = '1',
                '<span class=\'badge badge-danger\'>Usulan Faskes : Isolasi Mandiri</span>',
                IF(usulan_faskes = '2',
                '<span class=\'badge badge-success\'>Usulan Faskes : Bebas Isolasi Mandiri</span>',
                IF(usulan_faskes = '3',
                '<span class=\'badge badge-warning\'>Usulan Faskes : Rujuk Rumah Sakit</span>',''
                )
                )
                )
                ) AS nama_jenis_pemeriksaan_other,
                IF(trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NOT NULL,nama_jenis_pemeriksaan,'') AS nama_jenis_pemeriksaan,
                IFNULL(is_sembuh,''),
                IFNULL(is_meninggal,''),
                IFNULL(is_probable,''),
                IFNULL(class_badge,'') AS class_badge,
                IFNULL(usulan_faskes,'') AS usulan_pemeriksaan,
                IFNULL(DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_buat_pemeriksaan,
                IFNULL(DATE_FORMAT(trx_pemeriksaan_non_puskesmas.updated_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_ubah_pemeriksaan,
                IFNULL(nama_lengkap,'') AS nama_asal_pembuat,
                trx_pemeriksaan_non_puskesmas.created_at,
                tracking_pemeriksaan
                FROM trx_pemeriksaan_non_puskesmas
                LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL
                LEFT JOIN jenis_pemeriksaan ON id_jenis_pemeriksaan=trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id AND jenis_pemeriksaan.deleted_at IS NULL
                INNER JOIN user ON id_user_created=id_user
                WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                ) AS a ON a.peserta_non_puskesmas_id=id_peserta_non_puskesmas
                LEFT JOIN 
                (
                SELECT 
                    trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas AS id_trx_pemeriksaan_non_puskesmas_last,
                    peserta_non_puskesmas_id,
                    level_user_id AS level_user_id_last,
                    class_badge AS class_badge_last,
                    usulan_faskes AS usulan_faskes_pemeriksaan_last,
                    trx_pemeriksaan_non_puskesmas.created_at AS created_at_last,
                    DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') AS created_pemeriksaan_last,
                    status_rawat AS status_rawat_last,
                    trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id AS jenis_pemeriksaan_id_last,
                    hasil_pemeriksaan_id AS hasil_pemeriksaan_id_last,
                    is_sembuh AS is_sembuh_last,
                    is_meninggal AS is_meninggal_last,
                    is_probable AS is_probable_last
                FROM (
                    SELECT SUBSTRING_INDEX(GROUP_CONCAT(id_trx_pemeriksaan_non_puskesmas ORDER BY trx_pemeriksaan_non_puskesmas.created_at DESC),',',1) AS id_trx_pemeriksaan_non_puskesmas
                    FROM trx_pemeriksaan_non_puskesmas
                    WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    GROUP BY peserta_non_puskesmas_id
                ) AS tbl
                LEFT JOIN trx_pemeriksaan_non_puskesmas ON trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas = tbl.id_trx_pemeriksaan_non_puskesmas
                LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                LEFT JOIN user ON id_user=id_user_created
                WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                ) AS c ON c.peserta_non_puskesmas_id=id_peserta_non_puskesmas
                WHERE peserta_non_puskesmas.id_user_created='" . $this->config->item("id_user_citra_husada") . "' AND peserta_non_puskesmas.deleted_at IS NULL AND peserta_id IS NULL
                ORDER BY id_trx_pemeriksaan_non_puskesmas ASC
            ) AS tbl
            WHERE created_pemeriksaan_last BETWEEN '" . $start_date . "' AND '" . $end_date . "' AND jenis_pemeriksaan_id_last = '1'
            GROUP BY id_peserta_non_puskesmas
        ) AS tbl
        ORDER BY id_trx_pemeriksaan_last DESC
        ")->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
            $templist[$key]['id_trx_pemeriksaan_last_encrypt'] = encrypt_data($row->id_trx_pemeriksaan_last);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_rs_dokter()
    {

        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");
        $wh = "";
        $wh = " AND

        (
            ((level_user_id_last = '3' OR level_user_id_last = '4') AND jenis_pemeriksaan_id_last = '1')
        )";

        $data_peserta = $this->peserta_model->query("
        SELECT *
        FROM 
        (
            SELECT
            id_peserta,
            nama,
            nik,
            CONCAT(
            '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
            '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
            '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
            '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
            ) AS kolom_faskes,

            CONCAT(
            '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
            '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
            '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
            '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
            ) AS kolom_labkesda,
            IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(hasil_pemeriksaan_id_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),'') AS hasil_pemeriksaan_id_labkesda,
            
            CONCAT(
            '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
            '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
            '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
            '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
            ) AS kolom_rs,
            IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tracking_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),'') AS tracking_pemeriksaan_rs,
            IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(hasil_pemeriksaan_id_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),'') AS hasil_pemeriksaan_id_rs,
            'rsud' AS peserta_dari,
            id_trx_pemeriksaan_last AS id_trx_pemeriksaan_last,
            level_user_id_peserta_created AS level_user_id_rsud,
            is_sembuh_last,
            is_meninggal_last,
            is_probable_last,
            usulan_faskes_pemeriksaan_last,
            jenis_pemeriksaan_id_last,
            hasil_pemeriksaan_id_last,
            level_user_id_last
            FROM (
            SELECT 
                id_trx_pemeriksaan,
                IF(a.level_user_id IN ('2','7'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_faskes,
                IF(a.level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_faskes,
                IF(a.level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_faskes,
                IF(a.level_user_id IN ('2','7'),a.class_badge,NULL) AS class_badge_faskes,
                IF(a.level_user_id IN ('2','7'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_faskes,
                IF(a.level_user_id IN ('2','7'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_faskes,
                IF(a.level_user_id IN ('2','7'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_faskes,
                
                IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_labkesda,
                IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_labkesda,
                IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_labkesda,
                IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.class_badge,NULL) AS class_badge_labkesda,
                IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_labkesda,
                IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_labkesda,
                IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_labkesda,
                IF(a.level_user_id NOT IN ('2','7') AND a.level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.hasil_pemeriksaan_id,NULL) AS hasil_pemeriksaan_id_labkesda,
                
                IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_rs,
                IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_rs,
                IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_rs,
                IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.class_badge,NULL) AS class_badge_rs,
                IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_rs,
                IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_rs,
                IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_rs,
                IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.tracking_pemeriksaan,NULL) AS tracking_pemeriksaan_rs,
                IF(a.level_user_id NOT IN ('2','7','3') AND a.level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.hasil_pemeriksaan_id,NULL) AS hasil_pemeriksaan_id_rs,
                id_peserta,
                peserta.nama,
                a.created_at AS tanggal_urutan_pemeriksaan,
                nik,
                kategori_data,
                c.*,
                usr_peserta.level_user_id AS level_user_id_peserta_created
            FROM peserta
            LEFT JOIN
            (
                SELECT 
                id_trx_pemeriksaan,
                peserta_id,
                level_user_id,
                IF(
                trx_pemeriksaan.jenis_pemeriksaan_id IS NULL AND hasil_pemeriksaan_id IS NULL,
                IF(is_sembuh = '1',
                    '<span class=\'badge badge-success\'>Sembuh</span>',
                IF(is_meninggal = '1',
                    '<span class=\'badge badge-secondary\'>Meninggal</span>',
                IF(is_probable = '1',
                    '<span class=\'badge badge-secondary\'>Probable</span>',''
                )
                )
                ),
                IF(
                    trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NULL,
                    '<span class=\'badge badge-secondary\'>Hasil Belum Keluar</span>',
                    IF(
                    trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NOT NULL,
                    IF(
                    class_badge = '1',
                    CONCAT('<span class=\'badge badge-success\'>',nama_hasil_pemeriksaan,'</span>'),
                    IF(class_badge = '2',
                    CONCAT('<span class=\'badge badge-warning\'>',nama_hasil_pemeriksaan,'</span>'),''
                    )
                    ),
                    ''
                    )
                )
                ) AS nama_hasil_pemeriksaan,
                IF(usulan_faskes = '0',
                '<span class=\'badge badge-danger\'>Usulan Faskes : SWAB PCR</span>',
                IF(usulan_faskes = '1',
                '<span class=\'badge badge-danger\'>Usulan Faskes : Isolasi Mandiri</span>',
                IF(usulan_faskes = '2',
                '<span class=\'badge badge-success\'>Usulan Faskes : Bebas Isolasi Mandiri</span>',
                IF(usulan_faskes = '3',
                '<span class=\'badge badge-warning\'>Usulan Faskes : Rujuk Rumah Sakit</span>',''
                )
                )
                )
                ) AS nama_jenis_pemeriksaan_other,
                IF(trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL,nama_jenis_pemeriksaan,'') AS nama_jenis_pemeriksaan,
                IFNULL(is_sembuh,''),
                IFNULL(is_meninggal,''),
                IFNULL(is_probable,''),
                IFNULL(class_badge,'') AS class_badge,
                IFNULL(usulan_faskes,'') AS usulan_pemeriksaan,
                IFNULL(DATE_FORMAT(trx_pemeriksaan.created_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_buat_pemeriksaan,
                IFNULL(DATE_FORMAT(trx_pemeriksaan.updated_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_ubah_pemeriksaan,
                IFNULL(nama_lengkap,'') AS nama_asal_pembuat,
                trx_pemeriksaan.created_at,
                tracking_pemeriksaan,
                hasil_pemeriksaan_id
                FROM trx_pemeriksaan
                LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL
                LEFT JOIN jenis_pemeriksaan ON id_jenis_pemeriksaan=trx_pemeriksaan.jenis_pemeriksaan_id AND jenis_pemeriksaan.deleted_at IS NULL
                INNER JOIN user ON id_user_created=id_user
                WHERE trx_pemeriksaan.deleted_at IS NULL
            ) AS a ON a.peserta_id=id_peserta
            LEFT JOIN 
            (
                SELECT 
                trx_pemeriksaan.id_trx_pemeriksaan AS id_trx_pemeriksaan_last,
                peserta_id,
                level_user_id AS level_user_id_last,
                class_badge AS class_badge_last,
                usulan_faskes AS usulan_faskes_pemeriksaan_last,
                trx_pemeriksaan.created_at AS created_at_last,
                DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') AS created_pemeriksaan_last,
                status_rawat AS status_rawat_last,
                trx_pemeriksaan.jenis_pemeriksaan_id AS jenis_pemeriksaan_id_last,
                hasil_pemeriksaan_id AS hasil_pemeriksaan_id_last,
                is_sembuh AS is_sembuh_last,
                is_meninggal AS is_meninggal_last,
                is_probable AS is_probable_last
                FROM (
                SELECT SUBSTRING_INDEX(GROUP_CONCAT(id_trx_pemeriksaan ORDER BY trx_pemeriksaan.created_at DESC),',',1) AS id_trx_pemeriksaan
                FROM trx_pemeriksaan
                WHERE trx_pemeriksaan.deleted_at IS NULL
                GROUP BY peserta_id
                ) AS tbl
                LEFT JOIN trx_pemeriksaan ON trx_pemeriksaan.id_trx_pemeriksaan = tbl.id_trx_pemeriksaan
                LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                LEFT JOIN user ON id_user=id_user_created 
                WHERE trx_pemeriksaan.deleted_at IS NULL
            ) AS c ON c.peserta_id=id_peserta
            LEFT JOIN user AS usr_peserta ON peserta.id_user_created=id_user
            WHERE peserta.deleted_at IS NULL
            ORDER BY id_trx_pemeriksaan ASC
            ) AS tbl
            WHERE created_pemeriksaan_last BETWEEN '" . $start_date . "' AND '" . $end_date . "' 
            ${wh}
            GROUP BY id_peserta

            UNION 

            SELECT 
            id_peserta_non_puskesmas AS id_peserta,
            nama,
            nik,
            '' AS kolom_faskes,
            '' AS kolom_labkesda,
            '' AS hasil_pemeriksaan_id_labkesda,
            CONCAT(
                '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
                '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
            ) AS kolom_rs,
            IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tracking_pemeriksaan ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),'') AS tracking_pemeriksaan,
            IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(hasil_pemeriksaan_id ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),'') AS hasil_pemeriksaan_id,
            'ch' AS peserta_dari,
            id_trx_pemeriksaan_non_puskesmas_last AS id_trx_pemeriksaan_last,
            '' AS level_user_id_rsud,
            is_sembuh_last,
            is_meninggal_last,
            is_probable_last,
            usulan_faskes_pemeriksaan_last,
            jenis_pemeriksaan_id_last,
            hasil_pemeriksaan_id_last,
            level_user_id_last
            FROM (
                SELECT 
                id_trx_pemeriksaan_non_puskesmas,
                IF(level_user_id IN ('2','7'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan,
                IF(level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan,
                IF(level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other,
                IF(level_user_id IN ('2','7'),a.class_badge,NULL) AS class_badge,
                IF(level_user_id IN ('2','7'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan,
                IF(level_user_id IN ('2','7'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan,
                IF(level_user_id IN ('2','7'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat,
                IF(level_user_id IN ('2','7'),a.hasil_pemeriksaan_id,NULL) AS hasil_pemeriksaan_id,
                id_peserta_non_puskesmas,
                peserta_non_puskesmas.nama,
                a.created_at AS tanggal_urutan_pemeriksaan,
                a.tracking_pemeriksaan,
                nik,
                peserta_id,
                c.*
                FROM peserta_non_puskesmas
                LEFT JOIN
                (
                SELECT 
                id_trx_pemeriksaan_non_puskesmas,
                peserta_non_puskesmas_id,
                level_user_id,
                IF(
                trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NULL AND hasil_pemeriksaan_id IS NULL,
                    IF(is_sembuh = '1',
                    '<span class=\'badge badge-success\'>Sembuh</span>',
                    IF(is_meninggal = '1',
                    '<span class=\'badge badge-secondary\'>Meninggal</span>',
                    IF(is_probable = '1',
                    '<span class=\'badge badge-secondary\'>Probable</span>',''
                    )
                    )
                    ),
                    IF(
                    trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NULL,
                    '<span class=\'badge badge-secondary\'>Hasil Belum Keluar</span>',
                    IF(
                    trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NOT NULL,
                    IF(
                    class_badge = '1',
                    CONCAT('<span class=\'badge badge-success\'>',nama_hasil_pemeriksaan,'</span>'),
                    IF(class_badge = '2',
                    CONCAT('<span class=\'badge badge-warning\'>',nama_hasil_pemeriksaan,'</span>'),''
                    )
                    ),
                    ''
                    )
                    )
                ) AS nama_hasil_pemeriksaan,
                IF(usulan_faskes = '0',
                '<span class=\'badge badge-danger\'>Usulan Faskes : SWAB PCR</span>',
                IF(usulan_faskes = '1',
                '<span class=\'badge badge-danger\'>Usulan Faskes : Isolasi Mandiri</span>',
                IF(usulan_faskes = '2',
                '<span class=\'badge badge-success\'>Usulan Faskes : Bebas Isolasi Mandiri</span>',
                IF(usulan_faskes = '3',
                '<span class=\'badge badge-warning\'>Usulan Faskes : Rujuk Rumah Sakit</span>',''
                )
                )
                )
                ) AS nama_jenis_pemeriksaan_other,
                IF(trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id IS NOT NULL,nama_jenis_pemeriksaan,'') AS nama_jenis_pemeriksaan,
                IFNULL(is_sembuh,''),
                IFNULL(is_meninggal,''),
                IFNULL(is_probable,''),
                IFNULL(class_badge,'') AS class_badge,
                IFNULL(usulan_faskes,'') AS usulan_pemeriksaan,
                IFNULL(DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_buat_pemeriksaan,
                IFNULL(DATE_FORMAT(trx_pemeriksaan_non_puskesmas.updated_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_ubah_pemeriksaan,
                IFNULL(nama_lengkap,'') AS nama_asal_pembuat,
                trx_pemeriksaan_non_puskesmas.created_at,
                tracking_pemeriksaan,
                hasil_pemeriksaan_id
                FROM trx_pemeriksaan_non_puskesmas
                LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL
                LEFT JOIN jenis_pemeriksaan ON id_jenis_pemeriksaan=trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id AND jenis_pemeriksaan.deleted_at IS NULL
                INNER JOIN user ON id_user_created=id_user
                WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                ) AS a ON a.peserta_non_puskesmas_id=id_peserta_non_puskesmas
                LEFT JOIN 
                (
                SELECT 
                    trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas AS id_trx_pemeriksaan_non_puskesmas_last,
                    peserta_non_puskesmas_id,
                    level_user_id AS level_user_id_last,
                    class_badge AS class_badge_last,
                    usulan_faskes AS usulan_faskes_pemeriksaan_last,
                    trx_pemeriksaan_non_puskesmas.created_at AS created_at_last,
                    DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') AS created_pemeriksaan_last,
                    status_rawat AS status_rawat_last,
                    trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id AS jenis_pemeriksaan_id_last,
                    hasil_pemeriksaan_id AS hasil_pemeriksaan_id_last,
                    is_sembuh AS is_sembuh_last,
                    is_meninggal AS is_meninggal_last,
                    is_probable AS is_probable_last
                FROM (
                    SELECT SUBSTRING_INDEX(GROUP_CONCAT(id_trx_pemeriksaan_non_puskesmas ORDER BY trx_pemeriksaan_non_puskesmas.created_at DESC),',',1) AS id_trx_pemeriksaan_non_puskesmas
                    FROM trx_pemeriksaan_non_puskesmas
                    WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    GROUP BY peserta_non_puskesmas_id
                ) AS tbl
                LEFT JOIN trx_pemeriksaan_non_puskesmas ON trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas = tbl.id_trx_pemeriksaan_non_puskesmas
                LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                LEFT JOIN user ON id_user=id_user_created
                WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                ) AS c ON c.peserta_non_puskesmas_id=id_peserta_non_puskesmas
                WHERE peserta_non_puskesmas.id_user_created='" . $this->config->item("id_user_citra_husada") . "' AND peserta_non_puskesmas.deleted_at IS NULL AND peserta_id IS NULL
                ORDER BY id_trx_pemeriksaan_non_puskesmas ASC
            ) AS tbl
            WHERE created_pemeriksaan_last BETWEEN '" . $start_date . "' AND '" . $end_date . "' AND jenis_pemeriksaan_id_last = '1'
            GROUP BY id_peserta_non_puskesmas
        ) AS tbl
        ORDER BY id_trx_pemeriksaan_last DESC
        ")->result();

        $templist = array();
        $select_pcr_labkesda = false;
        $select_pcr_rs = false;
        $n = 1;
        foreach ($data_peserta as $key => $row) {
            if ($row->peserta_dari == "rsud") {
                if ($row->level_user_id_last == '3') {
                    $select_pcr_labkesda = true;
                    $select_pcr_rs = false;
                } else if ($row->level_user_id_last == '4') {
                    $select_pcr_labkesda = false;
                    $select_pcr_rs = true;
                }
            } else {
                $select_pcr_labkesda = false;
                $select_pcr_rs = true;
            }

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
            $templist[$key]['id_trx_pemeriksaan_last_encrypt'] = encrypt_data($row->id_trx_pemeriksaan_last);
            $templist[$key]['select_pcr_labkesda'] = $select_pcr_labkesda;
            $templist[$key]['select_pcr_rs'] = $select_pcr_rs;
            $templist[$key]['n'] = $n++;

            $templist[$key]['stat_button_print'] = false;
            if ($row->jenis_pemeriksaan_id_last == '1') {
                $templist[$key]['stat_button_print'] = true;
            }
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_dinkes()
    {
        $greather_than_14 = $this->iget("greather_than_14");
        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");
        $jenis_pemeriksaan = decrypt_data($this->iget("jenis_pemeriksaan"));
        $hasil_pemeriksaan = decrypt_data($this->iget("hasil_pemeriksaan"));

        $wh = "";
        if ($jenis_pemeriksaan > '9000') {
            if ($jenis_pemeriksaan == '9001') {
                $wh = " AND is_sembuh_last = '1'";
            } else if ($jenis_pemeriksaan == '9002') {
                $wh = " AND is_meninggal_last = '1'";
            } else if ($jenis_pemeriksaan == '9003') {
                $wh = " AND is_probable_last = '1'";
            } else if ($jenis_pemeriksaan == '9004') {
                $wh = " AND usulan_faskes_pemeriksaan = '0'";
            } else if ($jenis_pemeriksaan == '9005') {
                $wh = " AND usulan_faskes_pemeriksaan = '1'";
            } else if ($jenis_pemeriksaan == '9006') {
                $wh = " AND usulan_faskes_pemeriksaan = '2'";
            } else if ($jenis_pemeriksaan == '9007') {
                $wh = "AND status_rawat = '3'";
            }
        } else if ($jenis_pemeriksaan && $hasil_pemeriksaan) {
            $wh = " AND jenis_pemeriksaan_id_last = '" . $jenis_pemeriksaan . "' AND hasil_pemeriksaan_id_last = '" . $hasil_pemeriksaan . "'";
        } else if ($jenis_pemeriksaan && !$hasil_pemeriksaan) {
            $wh = " AND jenis_pemeriksaan_id_last = '" . $jenis_pemeriksaan . "'";
        }

        $wh_2 = "";

        if ($greather_than_14 == 'true') {
            $wh_2 = " AND DATE_ADD(DATE_FORMAT(created_at_last,'%Y-%m-%d'), INTERVAL 14 DAY) <= CURDATE() ";
        }

        $data_peserta = $this->peserta_model->query("
        SELECT
            id_peserta,
            nama,
            nik,
            CONCAT(
                '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
                '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_faskes ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
            ) AS kolom_faskes,

            CONCAT(
                '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
                '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_labkesda ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
            ) AS kolom_labkesda,
            
            CONCAT(
                '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_other_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                '<div class=\'info-column\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_jenis_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),' ',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_hasil_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</div>',
                '<div class=\'info-column\'><span class=\'badge badge-info\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(tanggal_buat_pemeriksaan_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>',
                '<div class=\'info-column\'><span class=\'badge bg-purple-400\'>',IFNULL(SUBSTRING_INDEX(GROUP_CONCAT(nama_asal_pembuat_rs ORDER BY tanggal_urutan_pemeriksaan DESC SEPARATOR '|'),'|',1),''),'</span></div>'
            ) AS kolom_rs
            FROM (
                SELECT 
                    id_trx_pemeriksaan,
                    IF(level_user_id IN ('2','7'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_faskes,
                    IF(level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_faskes,
                    IF(level_user_id IN ('2','7'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_faskes,
                    IF(level_user_id IN ('2','7'),a.class_badge,NULL) AS class_badge_faskes,
                    IF(level_user_id IN ('2','7'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_faskes,
                    IF(level_user_id IN ('2','7'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_faskes,
                    IF(level_user_id IN ('2','7'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_faskes,
                    
                    IF(level_user_id NOT IN ('2','7') AND level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_labkesda,
                    IF(level_user_id NOT IN ('2','7') AND level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_labkesda,
                    IF(level_user_id NOT IN ('2','7') AND level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_labkesda,
                    IF(level_user_id NOT IN ('2','7') AND level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.class_badge,NULL) AS class_badge_labkesda,
                    IF(level_user_id NOT IN ('2','7') AND level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_labkesda,
                    IF(level_user_id NOT IN ('2','7') AND level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_labkesda,
                    IF(level_user_id NOT IN ('2','7') AND level_user_id = '3' AND level_user_id_last NOT IN ('2'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_labkesda,
                    
                    IF(level_user_id NOT IN ('2','7','3') AND level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_hasil_pemeriksaan,NULL) AS nama_hasil_pemeriksaan_rs,
                    IF(level_user_id NOT IN ('2','7','3') AND level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_jenis_pemeriksaan,NULL) AS nama_jenis_pemeriksaan_rs,
                    IF(level_user_id NOT IN ('2','7','3') AND level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_jenis_pemeriksaan_other,NULL) AS nama_jenis_pemeriksaan_other_rs,
                    IF(level_user_id NOT IN ('2','7','3') AND level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.class_badge,NULL) AS class_badge_rs,
                    IF(level_user_id NOT IN ('2','7','3') AND level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.usulan_pemeriksaan,NULL) AS usulan_pemeriksaan_rs,
                    IF(level_user_id NOT IN ('2','7','3') AND level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.tanggal_buat_pemeriksaan,NULL) AS tanggal_buat_pemeriksaan_rs,
                    IF(level_user_id NOT IN ('2','7','3') AND level_user_id = '4' AND level_user_id_last NOT IN ('2','3'),a.nama_asal_pembuat,NULL) AS nama_asal_pembuat_rs,
                    id_peserta,
                    peserta.nama,
                    nik,
                    kategori_data,
                    a.created_at AS tanggal_urutan_pemeriksaan,
                    c.*
                FROM peserta
                LEFT JOIN
                (
                    SELECT 
                        id_trx_pemeriksaan,
                        peserta_id,
                        level_user_id,
                        IF(
                            trx_pemeriksaan.jenis_pemeriksaan_id IS NULL AND hasil_pemeriksaan_id IS NULL,
                                IF(is_sembuh = '1',
                                    '<span class=\'badge badge-success\'>Sembuh</span>',
                                IF(is_meninggal = '1',
                                    '<span class=\'badge badge-secondary\'>Meninggal</span>',
                                IF(is_probable = '1',
                                    '<span class=\'badge badge-secondary\'>Probable</span>',''
                                )
                                )
                                ),
                                IF(
                                    trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NULL,
                                    '<span class=\'badge badge-secondary\'>Hasil Belum Keluar</span>',
                                    IF(
                                        trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL AND hasil_pemeriksaan_id IS NOT NULL,
                                        IF(
                                            class_badge = '1',
                                                CONCAT('<span class=\'badge badge-success\'>',nama_hasil_pemeriksaan,'</span>'),
                                            IF(class_badge = '2',
                                                CONCAT('<span class=\'badge badge-warning\'>',nama_hasil_pemeriksaan,'</span>'),''
                                            )
                                        ),
                                        ''
                                    )
                                )
                        ) AS nama_hasil_pemeriksaan,
                        IF(usulan_faskes = '0',
                            '<span class=\'badge badge-danger\'>Usulan Faskes : SWAB PCR</span>',
                        IF(usulan_faskes = '1',
                            '<span class=\'badge badge-danger\'>Usulan Faskes : Isolasi Mandiri</span>',
                        IF(usulan_faskes = '2',
                            '<span class=\'badge badge-success\'>Usulan Faskes : Bebas Isolasi Mandiri</span>',
                        IF(usulan_faskes = '3',
                            '<span class=\'badge badge-warning\'>Usulan Faskes : Rujuk Rumah Sakit</span>',''
                        )
                        )
                        )
                        ) AS nama_jenis_pemeriksaan_other,
                        IF(trx_pemeriksaan.jenis_pemeriksaan_id IS NOT NULL,nama_jenis_pemeriksaan,'') AS nama_jenis_pemeriksaan,
                        IFNULL(is_sembuh,''),
                        IFNULL(is_meninggal,''),
                        IFNULL(is_probable,''),
                        IFNULL(class_badge,'') AS class_badge,
                        IFNULL(usulan_faskes,'') AS usulan_pemeriksaan,
                        IFNULL(DATE_FORMAT(trx_pemeriksaan.created_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_buat_pemeriksaan,
                        IFNULL(DATE_FORMAT(trx_pemeriksaan.updated_at,'%d-%m-%Y %H:%i:%s'),'') AS tanggal_ubah_pemeriksaan,
                        IFNULL(nama_lengkap,'') AS nama_asal_pembuat,
                        trx_pemeriksaan.created_at
                    FROM trx_pemeriksaan
                    LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL
                    LEFT JOIN jenis_pemeriksaan ON id_jenis_pemeriksaan=trx_pemeriksaan.jenis_pemeriksaan_id AND jenis_pemeriksaan.deleted_at IS NULL
                    INNER JOIN user ON id_user_created=id_user
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                ) AS a ON a.peserta_id=id_peserta
                LEFT JOIN 
                (
                    SELECT 
                            trx_pemeriksaan.id_trx_pemeriksaan AS id_trx_pemeriksaan_last,
                            peserta_id,
                            level_user_id AS level_user_id_last,
                            class_badge AS class_badge_last,
                            usulan_faskes AS usulan_faskes_pemeriksaan_last,
                            trx_pemeriksaan.created_at AS created_at_last,
                            DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') AS created_pemeriksaan_last,
                            status_rawat AS status_rawat_last,
                            trx_pemeriksaan.jenis_pemeriksaan_id AS jenis_pemeriksaan_id_last,
                            hasil_pemeriksaan_id AS hasil_pemeriksaan_id_last,is_sembuh AS is_sembuh_last,
                            is_meninggal AS is_meninggal_last,
                            is_probable AS is_probable_last
                        FROM (
                            SELECT SUBSTRING_INDEX(GROUP_CONCAT(id_trx_pemeriksaan ORDER BY trx_pemeriksaan.created_at DESC),',',1) AS id_trx_pemeriksaan
                            FROM trx_pemeriksaan
                            WHERE trx_pemeriksaan.deleted_at IS NULL
                            GROUP BY peserta_id
                        ) AS tbl
                        LEFT JOIN trx_pemeriksaan ON trx_pemeriksaan.id_trx_pemeriksaan = tbl.id_trx_pemeriksaan
                        LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                        LEFT JOIN user ON id_user=id_user_created 
                        WHERE trx_pemeriksaan.deleted_at IS NULL
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL
                ORDER BY id_trx_pemeriksaan ASC
            ) AS tbl
            WHERE created_pemeriksaan_last BETWEEN '" . $start_date . "' AND '" . $end_date . "'
            {$wh_2}
            {$wh}
            GROUP BY id_peserta
            ORDER BY id_trx_pemeriksaan_last DESC
        ")->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
            $templist[$key]['stat_button'] = true;
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_kecamatan_kabupaten_provinsi()
    {
        $kel_desa = decrypt_data($this->iget("kel_desa"));

        // echo $kel_desa;

        if ($kel_desa == "109") {
            $data['kecamatan'] = "";
            $data['kabupaten'] = "";
            $data['provinsi'] = "";
        } else {

            $data_master = $this->master_wilayah_model->get_by($kel_desa);

            $data_kecamatan = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_master->kode_induk
                    )
                ),
                "row"
            );

            $data_kabupaten = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_kecamatan->kode_induk
                    )
                ),
                "row"
            );

            $data_provinsi = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_kabupaten->kode_induk
                    )
                ),
                "row"
            );

            $data['kecamatan'] = ucwords(strtolower($data_kecamatan->nama_wilayah));
            $data['kabupaten'] = ucwords(strtolower($data_kabupaten->nama_wilayah));
            $data['provinsi'] = ucwords(strtolower($data_provinsi->nama_wilayah));
        }


        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_jenis_pemeriksaan()
    {
        $wh = array();
        if (in_array($this->session->userdata("level_user_id"), array("2", "7", "3"))) {
            if ($this->session->userdata("id_user") != $this->config->item('id_user_citra_husada')) {
                $wh = array(
                    "id_jenis_pemeriksaan !=" => "1"
                );
            }
        }

        $data_jenis_pemeriksaan = $this->jenis_pemeriksaan_model->get(
            array(
                "where" => $wh
            )
        );

        $templist = array();
        foreach ($data_jenis_pemeriksaan as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_jenis_pemeriksaan);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_jenis_pemeriksaan_all()
    {
        $wh = array();
        $data_jenis_pemeriksaan = $this->jenis_pemeriksaan_model->get();

        $templist = array();
        foreach ($data_jenis_pemeriksaan as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_jenis_pemeriksaan);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_hasil_pemeriksaan()
    {
        $id_jenis_pemeriksaan = decrypt_data($this->iget("id_jenis_pemeriksaan"));

        $data_hasil_pemeriksaan = $this->hasil_pemeriksaan_model->get(
            array(
                "where" => array(
                    "jenis_pemeriksaan_id" => $id_jenis_pemeriksaan
                )
            )
        );

        $templist = array();
        foreach ($data_hasil_pemeriksaan as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_hasil_pemeriksaan);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_detail_peserta()
    {
        $id_peserta = decrypt_data($this->iget("id_peserta"));

        $data_hasil_pemeriksaan = $this->trx_pemeriksaan_model->query("
        SELECT *
        FROM (
            SELECT `trx_pemeriksaan`.*, `nama_jenis_pemeriksaan`, `nama_hasil_pemeriksaan`, 
            DATE_FORMAT(trx_pemeriksaan.created_at, '%Y-%m-%d') AS tanggal_pemeriksaan, 
            DATE_FORMAT(trx_pemeriksaan.created_at, '%H:%i:%s') AS jam_pemeriksaan, `class_badge`, `a`.`level_user_id`, 
            IFNULL(a.nama_lengkap, '') AS user_create, 
            IFNULL(b.nama_lengkap, '') AS user_update,
            IFNULL(c.nama_lengkap, '') AS user_non_puskes 
            FROM `trx_pemeriksaan` 
            LEFT JOIN `jenis_pemeriksaan` ON `id_jenis_pemeriksaan`=`jenis_pemeriksaan_id` 
            LEFT JOIN `hasil_pemeriksaan` ON `id_hasil_pemeriksaan`=`hasil_pemeriksaan_id` 
            LEFT JOIN `user` AS `a` ON `a`.`id_user`=`id_user_created` 
            LEFT JOIN `user` AS `b` ON `b`.`id_user`=`id_user_updated`
            LEFT JOIN `user` AS `c` ON `c`.`id_user`=`id_user_non_puskes` 
            WHERE `peserta_id` = '" . $id_peserta . "' AND `trx_pemeriksaan`.`deleted_at` IS NULL
        )AS tbl
        ORDER BY created_at,id_trx_pemeriksaan
        ")->result();

        $max_trx = $this->trx_pemeriksaan_model->get(
            array(
                "where" => array(
                    "peserta_id" => $id_peserta
                ),
                "order_by" => array(
                    "created_at" => "DESC",
                    "id_trx_pemeriksaan" => "DESC"
                ),
                "limit" => "1"
            ),
            "row"
        );

        $templist = array();
        foreach ($data_hasil_pemeriksaan as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_trx_pemeriksaan);
            $templist[$key]['tanggal_pemeriksaan'] = date_indo($row->tanggal_pemeriksaan);
            $templist[$key]['stat_button_print'] = false;

            if ($this->session->userdata("level_user_id") == "4" && $row->jenis_pemeriksaan_id == "1") {
                $templist[$key]['stat_button_print'] = true;
            }

            if ($this->session->userdata("level_user_id") == 6) {
                if (!$row->jenis_pemeriksaan_id) {
                    $templist[$key]['is_edit'] = false;
                    $templist[$key]['is_delete'] = true;
                } else {
                    $templist[$key]['is_edit'] = true;
                    $templist[$key]['is_delete'] = true;
                }
            } else {
                if ($row->level_user_id == $this->session->userdata("level_user_id")) {
                    if ($row->from_non_puskes == "1") {
                        $templist[$key]['is_edit'] = false;
                        $templist[$key]['is_delete'] = false;
                    } else {
                        if ($max_trx->id_trx_pemeriksaan == $row->id_trx_pemeriksaan) {
                            $templist[$key]['is_edit'] = true;
                            $templist[$key]['is_delete'] = true;
                        } else {
                            $templist[$key]['is_edit'] = false;
                            $templist[$key]['is_delete'] = false;
                        }
                    }
                } else {
                    if ($this->session->userdata("level_user_id") == "4") {
                        if ($row->level_user_id == "3") {
                            if ($max_trx->id_trx_pemeriksaan == $row->id_trx_pemeriksaan) {
                                $templist[$key]['is_edit'] = false;
                                $templist[$key]['is_delete'] = false;
                            } else {
                                $templist[$key]['is_edit'] = false;
                                $templist[$key]['is_delete'] = false;
                            }
                        }
                    } else {
                        $templist[$key]['is_edit'] = false;
                        $templist[$key]['is_delete'] = false;
                    }
                }
            }
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_detail_peserta_non_puskesmas()
    {
        $id_peserta_non_puskesmas = decrypt_data($this->iget("id_peserta_non_puskesmas"));

        $data_hasil_pemeriksaan = $this->trx_pemeriksaan_non_puskesmas_model->query("
        SELECT *
        FROM (
            SELECT `trx_pemeriksaan_non_puskesmas`.*, `nama_jenis_pemeriksaan`, `nama_hasil_pemeriksaan`, 
            DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at, '%Y-%m-%d') AS tanggal_pemeriksaan, 
            DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at, '%H:%i:%s') AS jam_pemeriksaan, `class_badge`, `a`.`level_user_id`, 
            IFNULL(a.nama_lengkap, '') AS user_create, 
            IFNULL(b.nama_lengkap, '') AS user_update,
            '' AS status_terkonfirmasi 
            FROM `trx_pemeriksaan_non_puskesmas` 
            LEFT JOIN `jenis_pemeriksaan` ON `id_jenis_pemeriksaan`=`jenis_pemeriksaan_id` 
            LEFT JOIN `hasil_pemeriksaan` ON `id_hasil_pemeriksaan`=`hasil_pemeriksaan_id` 
            LEFT JOIN `user` AS `a` ON `a`.`id_user`=`id_user_created` 
            LEFT JOIN `user` AS `b` ON `b`.`id_user`=`id_user_updated` 
            WHERE `peserta_non_puskesmas_id` = '" . $id_peserta_non_puskesmas . "' AND `trx_pemeriksaan_non_puskesmas`.`deleted_at` IS NULL 
        )AS tbl
        ORDER BY created_at,IF(peserta_non_puskesmas_id = '' OR peserta_non_puskesmas_id IS NULL,1,0), peserta_non_puskesmas_id
        ")->result();

        $templist = array();
        foreach ($data_hasil_pemeriksaan as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_trx_pemeriksaan_non_puskesmas);
            $templist[$key]['tanggal_pemeriksaan'] = date_indo($row->tanggal_pemeriksaan);

            if ($row->level_user_id == $this->session->userdata("level_user_id")) {
                $templist[$key]['is_edit_deleted'] = true;
            } else {
                if ($this->session->userdata("level_user_id") == "4") {
                    if ($row->level_user_id == "3") {
                        $templist[$key]['is_edit_deleted'] = true;
                    }
                } else {
                    $templist[$key]['is_edit_deleted'] = false;
                }
            }
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function cek_kondisi_pemeriksaan_terakhir()
    {
        $id_peserta = decrypt_data($this->iget("id_peserta"));

        $data_pemeriksaan = $this->trx_pemeriksaan_model->get(
            array(
                "fields" => "hasil_pemeriksaan_id,jenis_pemeriksaan_id,usulan_faskes",
                "where" => array(
                    "peserta_id" => $id_peserta
                ),
                "where_false" => "is_sembuh IS NULL AND is_meninggal IS NULL AND is_probable IS NULL",
                "order_by" => array(
                    "id_trx_pemeriksaan" => "DESC"
                ),
                "limit" => "1"
            ),
            "row"
        );

        if ($data_pemeriksaan) {
            if ($data_pemeriksaan->hasil_pemeriksaan_id) {
                $data = true;
            } else if ($data_pemeriksaan->jenis_pemeriksaan_id) {
                $data = false;
            } else {
                if (in_array($data_pemeriksaan->usulan_faskes, array("0", "1"))) {
                    $data = true;
                } else {
                    $data = false;
                }
            }
        } else {
            $data = true;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function check_stat_terkonfirmasi_last()
    {
        $id_peserta = decrypt_data($this->iget("id_peserta"));

        $data = $this->trx_terkonfirmasi_model->get(
            array(
                "fields" => "trx_terkonfirmasi.*,status_rawat,usulan_faskes",
                "where" => array(
                    "trx_terkonfirmasi.peserta_id" => $id_peserta
                ),
                "join" => array(
                    "trx_pemeriksaan" => "trx_pemeriksaan_id=id_trx_pemeriksaan"
                ),
                "order_by" => array(
                    "id_trx_terkonfirmasi" => "DESC"
                ),
                "limit" => "1"
            ),
            "row"
        );

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function check_stat_pemeriksaan_last()
    {
        $id_peserta = decrypt_data($this->iget("id_peserta"));

        $data = $this->trx_pemeriksaan_model->get(
            array(
                "where" => array(
                    "peserta_id" => $id_peserta
                ),
                "order_by" => array(
                    "id_trx_pemeriksaan" => "DESC"
                ),
                "limit" => "1"
            ),
            "row"
        );

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function cek_kondisi_pemeriksaan_terakhir_non_puskesmas()
    {
        $id_peserta_non_puskesmas = decrypt_data($this->iget("id_peserta_non_puskesmas"));

        $data_pemeriksaan = $this->trx_pemeriksaan_non_puskesmas_model->get(
            array(
                "fields" => "hasil_pemeriksaan_id,jenis_pemeriksaan_id,usulan_faskes",
                "where" => array(
                    "peserta_non_puskesmas_id" => $id_peserta_non_puskesmas
                ),
                "order_by" => array(
                    "id_trx_pemeriksaan_non_puskesmas" => "DESC"
                ),
                "limit" => "1"
            ),
            "row"
        );

        if ($data_pemeriksaan) {
            if ($data_pemeriksaan->hasil_pemeriksaan_id) {
                $data = true;
            } else if ($data_pemeriksaan->jenis_pemeriksaan_id) {
                $data = false;
            } else {
                if (in_array($data_pemeriksaan->usulan_faskes, array("0", "1"))) {
                    $data = true;
                } else {
                    $data = false;
                }
            }
        } else {
            $data = true;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_detail_trx_peserta()
    {
        $id_trx_pemeriksaan = decrypt_data($this->iget("id_trx_pemeriksaan"));

        $data = $this->trx_pemeriksaan_model->get(
            array(
                "fields" => "trx_pemeriksaan.*,class_badge",
                "left_join" => array(
                    "hasil_pemeriksaan" => "id_hasil_pemeriksaan=hasil_pemeriksaan_id"
                ),
                "where" => array(
                    "id_trx_pemeriksaan" => $id_trx_pemeriksaan
                )
            ),
            "row"
        );

        $date_create = date_create($data->created_at);
        $data->tanggal_format = date_format($date_create, "d-m-Y H:i:s");
        $data->jenis_pemeriksaan_id_encrypt = encrypt_data($data->jenis_pemeriksaan_id);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_detail_trx_peserta_admin_lab()
    {
        $id_trx_pemeriksaan = decrypt_data($this->iget("id_trx_pemeriksaan"));
        $peserta_dari = $this->iget("peserta_dari");

        if ($peserta_dari == 'rsud') {
            $data = $this->trx_pemeriksaan_model->get(
                array(
                    "fields" => "trx_pemeriksaan.*,no_rekam_medis",
                    "join" => array(
                        "peserta" => "id_peserta=peserta_id"
                    ),
                    "where" => array(
                        "id_trx_pemeriksaan" => $id_trx_pemeriksaan
                    )
                ),
                "row"
            );
        } else if ($peserta_dari = 'ch') {
            $data = $this->trx_pemeriksaan_non_puskesmas_model->get(
                array(
                    "fields" => "id_trx_pemeriksaan_non_puskesmas AS id_trx_pemeriksaan,peserta_non_puskesmas_id AS peserta_id,tracking_pemeriksaan,no_rekam_medis",
                    "join" => array(
                        "peserta_non_puskesmas" => "id_peserta_non_puskesmas=peserta_non_puskesmas_id"
                    ),
                    "where" => array(
                        "id_trx_pemeriksaan_non_puskesmas" => $id_trx_pemeriksaan
                    )
                ),
                "row"
            );
        }

        $data->peserta_id_encrypt = encrypt_data($data->peserta_id);
        $data->trx_pemeriksaan_id_encrypt = encrypt_data($data->id_trx_pemeriksaan);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_detail_trx_peserta_non_puskesmas()
    {
        $id_trx_pemeriksaan_non_puskesmas = decrypt_data($this->iget("id_trx_pemeriksaan_non_puskesmas"));

        $data = $this->trx_pemeriksaan_non_puskesmas_model->get(
            array(
                "fields" => "trx_pemeriksaan_non_puskesmas.*,class_badge",
                "left_join" => array(
                    "hasil_pemeriksaan" => "id_hasil_pemeriksaan=hasil_pemeriksaan_id"
                ),
                "where" => array(
                    "id_trx_pemeriksaan_non_puskesmas" => $id_trx_pemeriksaan_non_puskesmas
                )
            ),
            "row"
        );

        $date_create = date_create($data->created_at);
        $data->tanggal_format = date_format($date_create, "d-m-Y H:i:s");
        $data->jenis_pemeriksaan_id_encrypt = encrypt_data($data->jenis_pemeriksaan_id);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_non_puskesmas()
    {
        $data_peserta = $this->peserta_non_puskesmas_model->query("
        SELECT *
        FROM (
            SELECT id_peserta_non_puskesmas AS id_peserta,peserta_non_puskesmas.nama,nik,peserta_non_puskesmas.usulan_faskes AS usulan_faskes_tanpa_pemeriksaan,b.level_user_id,peserta_non_puskesmas.id_user_created,
            GROUP_CONCAT(IFNULL(als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS id_trx_pemeriksaan, 
            GROUP_CONCAT(IFNULL(a.level_user_id,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS level_user_id_pemeriksaan, 
            GROUP_CONCAT(IFNULL(nama_hasil_pemeriksaan,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS nama_hasil_pemeriksaan, 
            GROUP_CONCAT(IFNULL(nama_jenis_pemeriksaan,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS nama_jenis_pemeriksaan, 
            GROUP_CONCAT(IFNULL(class_badge,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS class_badge, 
            GROUP_CONCAT(IFNULL(als_trx_pemeriksaan_non_puskesmas.usulan_faskes,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS usulan_pemeriksaan_faskes,
            GROUP_CONCAT(IFNULL(DATE_FORMAT(als_trx_pemeriksaan_non_puskesmas.created_at,'%d-%m-%Y %H:%i:%s'),'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS date_created,
            GROUP_CONCAT(IFNULL(DATE_FORMAT(als_trx_pemeriksaan_non_puskesmas.updated_at,'%d-%m-%Y %H:%i:%s'),'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS date_updated,
            GROUP_CONCAT(IFNULL(a.nama_lengkap,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS nama_user_verification,
            c.*
            FROM peserta_non_puskesmas
            LEFT JOIN trx_pemeriksaan_non_puskesmas AS als_trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas=peserta_non_puskesmas_id AND als_trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
            LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL
            LEFT JOIN jenis_pemeriksaan ON id_jenis_pemeriksaan=als_trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id AND jenis_pemeriksaan.deleted_at IS NULL
            LEFT JOIN user AS a ON als_trx_pemeriksaan_non_puskesmas.id_user_created=a.id_user
            LEFT JOIN user AS b ON peserta_non_puskesmas.id_user_created=b.id_user
            LEFT JOIN 
                (
                    SELECT id_trx_pemeriksaan_non_puskesmas AS id_trx_pemeriksaan_last,peserta_non_puskesmas_id,level_user_id AS level_user_id_last,class_badge AS class_badge_last,usulan_faskes AS usulan_faskes_pemeriksaan,trx_pemeriksaan_id,trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id AS jenis_pemeriksaan_last,trx_pemeriksaan_non_puskesmas.tracking_pemeriksaan as tracking_pemeriksaan_last
                    FROM trx_pemeriksaan_non_puskesmas 
                    LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                    LEFT JOIN user ON id_user=id_user_created 
                    WHERE id_trx_pemeriksaan_non_puskesmas IN (
                        SELECT MAX(id_trx_pemeriksaan_non_puskesmas) AS id_trx_pemeriksaan_non_puskesmas 
                        FROM trx_pemeriksaan_non_puskesmas 
                        WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                        GROUP BY peserta_non_puskesmas_id
                        ) 
                    AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                ) AS c ON c.peserta_non_puskesmas_id=id_peserta_non_puskesmas
            WHERE peserta_non_puskesmas.deleted_at IS NULL AND kelurahan_master_wilayah_id IN (
                SELECT id_master_wilayah
                FROM master_wilayah
                WHERE puskesmas_id = " . $this->session->userdata("puskesmas_id") . "
            ) AND (peserta_id IS NULL OR c.trx_pemeriksaan_id IS NULL)
            GROUP BY id_peserta_non_puskesmas
            ORDER BY id_peserta_non_puskesmas ASC
        ) AS tbl
        WHERE IF((jenis_pemeriksaan_last = '2' OR jenis_pemeriksaan_last = '3') AND class_badge_last = '2',1,0) OR IF((jenis_pemeriksaan_last = '1' AND class_badge_last = '2'),1,0) 
        ")->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {
            if ($row->id_trx_pemeriksaan) {
                $id_trx_pemeriksaan = explode("|", $row->id_trx_pemeriksaan);
                $level_user_id_pemeriksaan = explode("|", $row->level_user_id_pemeriksaan);
                $nama_hasil_pemeriksaan = explode("|", $row->nama_hasil_pemeriksaan);
                $nama_jenis_pemeriksaan = explode("|", $row->nama_jenis_pemeriksaan);
                $class_badge = explode("|", $row->class_badge);
                $usulan_pemeriksaan_faskes = explode("|", $row->usulan_pemeriksaan_faskes);
                $date_created = explode("|", $row->date_created);
                $nama_user_verification = explode("|", $row->nama_user_verification);

                foreach ($id_trx_pemeriksaan as $key_pemeriksaan => $row_pemeriksaan) {

                    if (in_array($level_user_id_pemeriksaan[$key_pemeriksaan], array("2", "7"))) {
                        $templist[$key]['nama_hasil_pemeriksaan_faskes'] = $nama_hasil_pemeriksaan[$key_pemeriksaan];
                        $templist[$key]['nama_jenis_pemeriksaan_faskes'] = $nama_jenis_pemeriksaan[$key_pemeriksaan];
                        $templist[$key]['class_badge_faskes'] = $class_badge[$key_pemeriksaan];
                        $templist[$key]['usulan_pemeriksaan_faskes_row'] = $usulan_pemeriksaan_faskes[$key_pemeriksaan];
                        $templist[$key]['tanggal_pemeriksaan'] = $date_created[$key_pemeriksaan];
                        $templist[$key]['nama_asal_faskes'] = $nama_user_verification[$key_pemeriksaan];
                    } else if ($level_user_id_pemeriksaan[$key_pemeriksaan] == "3") {
                        if ($row->level_user_id_last == "2") {
                            if ($id_trx_pemeriksaan[$key_pemeriksaan] < $row->id_trx_pemeriksaan_last) {
                                $templist[$key]['nama_hasil_pemeriksaan_dinkes'] = "";
                                $templist[$key]['nama_jenis_pemeriksaan_dinkes'] = "";
                                $templist[$key]['class_badge_dinkes'] = "";
                            }
                        } else {
                            $templist[$key]['nama_hasil_pemeriksaan_dinkes'] = $nama_hasil_pemeriksaan[$key_pemeriksaan];
                            $templist[$key]['nama_jenis_pemeriksaan_dinkes'] = $nama_jenis_pemeriksaan[$key_pemeriksaan];
                            $templist[$key]['class_badge_dinkes'] = $class_badge[$key_pemeriksaan];
                            $templist[$key]['tanggal_pemeriksaan'] = $date_created[$key_pemeriksaan];
                        }
                    } else if ($level_user_id_pemeriksaan[$key_pemeriksaan] == "4") {
                        if (in_array($row->level_user_id_last, array("2", "3"))) {
                            if ($id_trx_pemeriksaan[$key_pemeriksaan] < $row->id_trx_pemeriksaan_last) {
                                $templist[$key]['nama_hasil_pemeriksaan_rs'] = "";
                                $templist[$key]['nama_jenis_pemeriksaan_rs'] = "";
                                $templist[$key]['class_badge_rs'] = "";
                            }
                        } else {
                            $templist[$key]['nama_hasil_pemeriksaan_rs'] = $nama_hasil_pemeriksaan[$key_pemeriksaan];
                            $templist[$key]['nama_jenis_pemeriksaan_rs'] = $nama_jenis_pemeriksaan[$key_pemeriksaan];
                            $templist[$key]['class_badge_rs'] = $class_badge[$key_pemeriksaan];
                            $templist[$key]['tanggal_pemeriksaan'] = $date_created[$key_pemeriksaan];
                        }
                    }
                }
            }

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_non_puskesmas_ch()
    {
        $data_peserta = $this->peserta_non_puskesmas_model->query("
        SELECT *
        FROM (
            SELECT id_peserta_non_puskesmas AS id_peserta,peserta_non_puskesmas.nama,nik,peserta_non_puskesmas.usulan_faskes AS usulan_faskes_tanpa_pemeriksaan,b.level_user_id,peserta_non_puskesmas.id_user_created,
            GROUP_CONCAT(IFNULL(als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS id_trx_pemeriksaan, 
            GROUP_CONCAT(IFNULL(a.level_user_id,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS level_user_id_pemeriksaan, 
            GROUP_CONCAT(IFNULL(nama_hasil_pemeriksaan,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS nama_hasil_pemeriksaan, 
            GROUP_CONCAT(IFNULL(nama_jenis_pemeriksaan,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS nama_jenis_pemeriksaan, 
            GROUP_CONCAT(IFNULL(class_badge,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS class_badge, 
            GROUP_CONCAT(IFNULL(als_trx_pemeriksaan_non_puskesmas.usulan_faskes,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS usulan_pemeriksaan_faskes,
            GROUP_CONCAT(IFNULL(DATE_FORMAT(als_trx_pemeriksaan_non_puskesmas.created_at,'%d-%m-%Y %H:%i:%s'),'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS date_created,
            GROUP_CONCAT(IFNULL(DATE_FORMAT(als_trx_pemeriksaan_non_puskesmas.updated_at,'%d-%m-%Y %H:%i:%s'),'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS date_updated,
            GROUP_CONCAT(IFNULL(a.nama_lengkap,'') ORDER BY als_trx_pemeriksaan_non_puskesmas.id_trx_pemeriksaan_non_puskesmas ASC SEPARATOR '|') AS nama_user_verification,
            c.*
            FROM peserta_non_puskesmas
            LEFT JOIN trx_pemeriksaan_non_puskesmas AS als_trx_pemeriksaan_non_puskesmas ON id_peserta_non_puskesmas=peserta_non_puskesmas_id AND als_trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
            LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL
            LEFT JOIN jenis_pemeriksaan ON id_jenis_pemeriksaan=als_trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id AND jenis_pemeriksaan.deleted_at IS NULL
            LEFT JOIN user AS a ON als_trx_pemeriksaan_non_puskesmas.id_user_created=a.id_user
            LEFT JOIN user AS b ON peserta_non_puskesmas.id_user_created=b.id_user
            LEFT JOIN 
                (
                    SELECT id_trx_pemeriksaan_non_puskesmas AS id_trx_pemeriksaan_last,peserta_non_puskesmas_id,level_user_id AS level_user_id_last,class_badge AS class_badge_last,usulan_faskes AS usulan_faskes_pemeriksaan,trx_pemeriksaan_id,trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id AS jenis_pemeriksaan_last,trx_pemeriksaan_non_puskesmas.hasil_pemeriksaan_id as hasil_pemeriksaan_id_last,trx_pemeriksaan_non_puskesmas.tracking_pemeriksaan as tracking_pemeriksaan_last
                    FROM trx_pemeriksaan_non_puskesmas 
                    LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                    LEFT JOIN user ON id_user=id_user_created 
                    WHERE id_trx_pemeriksaan_non_puskesmas IN (
                        SELECT MAX(id_trx_pemeriksaan_non_puskesmas) AS id_trx_pemeriksaan_non_puskesmas 
                        FROM trx_pemeriksaan_non_puskesmas 
                        WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                        GROUP BY peserta_non_puskesmas_id
                        ) 
                    AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                ) AS c ON c.peserta_non_puskesmas_id=id_peserta_non_puskesmas
            WHERE peserta_non_puskesmas.deleted_at IS NULL AND (peserta_id IS NULL OR c.trx_pemeriksaan_id IS NULL)
            GROUP BY id_peserta_non_puskesmas
            ORDER BY id_peserta_non_puskesmas ASC
        ) AS tbl
        WHERE jenis_pemeriksaan_last = '1'
        ")->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {
            if ($row->id_trx_pemeriksaan) {
                $id_trx_pemeriksaan = explode("|", $row->id_trx_pemeriksaan);
                $level_user_id_pemeriksaan = explode("|", $row->level_user_id_pemeriksaan);
                $nama_hasil_pemeriksaan = explode("|", $row->nama_hasil_pemeriksaan);
                $nama_jenis_pemeriksaan = explode("|", $row->nama_jenis_pemeriksaan);
                $class_badge = explode("|", $row->class_badge);
                $usulan_pemeriksaan_faskes = explode("|", $row->usulan_pemeriksaan_faskes);
                $date_created = explode("|", $row->date_created);
                $nama_user_verification = explode("|", $row->nama_user_verification);

                foreach ($id_trx_pemeriksaan as $key_pemeriksaan => $row_pemeriksaan) {

                    if (in_array($level_user_id_pemeriksaan[$key_pemeriksaan], array("2", "7"))) {
                        $templist[$key]['nama_hasil_pemeriksaan_faskes'] = $nama_hasil_pemeriksaan[$key_pemeriksaan];
                        $templist[$key]['nama_jenis_pemeriksaan_faskes'] = $nama_jenis_pemeriksaan[$key_pemeriksaan];
                        $templist[$key]['class_badge_faskes'] = $class_badge[$key_pemeriksaan];
                        $templist[$key]['usulan_pemeriksaan_faskes_row'] = $usulan_pemeriksaan_faskes[$key_pemeriksaan];
                        $templist[$key]['tanggal_pemeriksaan'] = $date_created[$key_pemeriksaan];
                        $templist[$key]['nama_asal_faskes'] = $nama_user_verification[$key_pemeriksaan];
                    } else if ($level_user_id_pemeriksaan[$key_pemeriksaan] == "3") {
                        if ($row->level_user_id_last == "2") {
                            if ($id_trx_pemeriksaan[$key_pemeriksaan] < $row->id_trx_pemeriksaan_last) {
                                $templist[$key]['nama_hasil_pemeriksaan_dinkes'] = "";
                                $templist[$key]['nama_jenis_pemeriksaan_dinkes'] = "";
                                $templist[$key]['class_badge_dinkes'] = "";
                            }
                        } else {
                            $templist[$key]['nama_hasil_pemeriksaan_dinkes'] = $nama_hasil_pemeriksaan[$key_pemeriksaan];
                            $templist[$key]['nama_jenis_pemeriksaan_dinkes'] = $nama_jenis_pemeriksaan[$key_pemeriksaan];
                            $templist[$key]['class_badge_dinkes'] = $class_badge[$key_pemeriksaan];
                            $templist[$key]['tanggal_pemeriksaan'] = $date_created[$key_pemeriksaan];
                        }
                    } else if ($level_user_id_pemeriksaan[$key_pemeriksaan] == "4") {
                        if (in_array($row->level_user_id_last, array("2", "3"))) {
                            if ($id_trx_pemeriksaan[$key_pemeriksaan] < $row->id_trx_pemeriksaan_last) {
                                $templist[$key]['nama_hasil_pemeriksaan_rs'] = "";
                                $templist[$key]['nama_jenis_pemeriksaan_rs'] = "";
                                $templist[$key]['class_badge_rs'] = "";
                            }
                        } else {
                            $templist[$key]['nama_hasil_pemeriksaan_rs'] = $nama_hasil_pemeriksaan[$key_pemeriksaan];
                            $templist[$key]['nama_jenis_pemeriksaan_rs'] = $nama_jenis_pemeriksaan[$key_pemeriksaan];
                            $templist[$key]['class_badge_rs'] = $class_badge[$key_pemeriksaan];
                            $templist[$key]['tanggal_pemeriksaan'] = $date_created[$key_pemeriksaan];
                        }
                    }
                }
            }

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
            $templist[$key]['id_trx_pemeriksaan_last_encrypt'] = encrypt_data($row->id_trx_pemeriksaan_last);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function sync_data_peserta()
    {
        $id_peserta_non_puskesmas = decrypt_data($this->iget("id_peserta"));

        $peserta_non_puskesmas = $this->peserta_non_puskesmas_model->get_by($id_peserta_non_puskesmas);

        if ($peserta_non_puskesmas->peserta_id) {
            $data_trx_pemeriksaan_non_puskesmas = $this->trx_pemeriksaan_non_puskesmas_model->query("
            SELECT *
            FROM peserta_non_puskesmas
            INNER JOIN 
            (
                SELECT trx_pemeriksaan_non_puskesmas.*,class_badge
                FROM trx_pemeriksaan_non_puskesmas 
                LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                LEFT JOIN user ON id_user=id_user_created 
                WHERE id_trx_pemeriksaan_non_puskesmas IN 
                (
                    SELECT MAX(id_trx_pemeriksaan_non_puskesmas) AS id_trx_pemeriksaan_non_puskesmas
                    FROM trx_pemeriksaan_non_puskesmas
                    WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    GROUP BY peserta_non_puskesmas_id
                ) AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
            ) AS c ON c.peserta_non_puskesmas_id=id_peserta_non_puskesmas
            WHERE trx_pemeriksaan_id IS NULL AND peserta_non_puskesmas.deleted_at IS NULL AND id_peserta_non_puskesmas = '" . $id_peserta_non_puskesmas . "' AND (c.usulan_faskes = '0' OR c.class_badge = '2' OR c.usulan_faskes = '1')
            ")->result();

            if ($data_trx_pemeriksaan_non_puskesmas) {
                foreach ($data_trx_pemeriksaan_non_puskesmas as $key => $row) {
                    if (!$row->trx_pemeriksaan_id) {
                        $data_trx_pemeriksaan = array(
                            "peserta_id" => $peserta_non_puskesmas->peserta_id,
                            "jenis_pemeriksaan_id" => $row->jenis_pemeriksaan_id,
                            "hasil_pemeriksaan_id" => $row->hasil_pemeriksaan_id,
                            "usulan_faskes" => $row->usulan_faskes,
                            "jenis_spesimen" => ($row->jenis_spesimen ? $row->jenis_spesimen : NULL),
                            "tanggal_terima_sampel" => ($row->tanggal_terima_sampel ? $row->tanggal_terima_sampel : NULL),
                            "status_rawat" => ($row->status_rawat ? $row->status_rawat : NULL),
                            "kode_sample" => $row->kode_sample,
                            "id_user_lab" => ($row->id_user_lab ? $row->id_user_lab : NULL),
                            "from_non_puskes" => "1",
                            "id_user_non_puskes" => $row->id_user_created,
                            'created_at' => $row->created_at,
                            'id_user_created' => $this->session->userdata("id_user")
                        );

                        $status = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);

                        $data_update_trx_pemeriksaan_non_puskesmas = array(
                            "trx_pemeriksaan_id" => $status
                        );

                        $status = $this->trx_pemeriksaan_non_puskesmas_model->edit($row->id_trx_pemeriksaan_non_puskesmas, $data_update_trx_pemeriksaan_non_puskesmas);
                    }
                }
            }

            $templist = "ada";
        } else {
            $wh_nama = "";
            if ($peserta_non_puskesmas->nama) {
                $pisah_space = explode(" ", str_replace("'", "''", $peserta_non_puskesmas->nama));
                foreach ($pisah_space as $key => $row) {
                    $separator = "";
                    if ((count($pisah_space) >= 1) && ($key < (count($pisah_space)))) {
                        $separator = "OR";
                    }
                    $wh_nama .= $separator . " nama LIKE '%" . $row . "%' ";
                }
            }

            $alamat_domisili = str_replace(" ", "", str_replace("'", "''", $peserta_non_puskesmas->alamat_domisili), $peserta_non_puskesmas->alamat_domisili);

            $peserta = $this->peserta_model->get(
                array(
                    "fields" => "peserta.*,DATE_FORMAT(tanggal_lahir,'%d-%m-%Y') as tanggal_lahir",
                    "where_false" => "(nik LIKE '%" . $peserta_non_puskesmas->nik . "%' OR alamat_domisili LIKE '%" . $alamat_domisili . "%' " . $wh_nama . ") AND kelurahan_master_wilayah_id IN 
                    (
                        SELECT id_master_wilayah
                        FROM master_wilayah
                        WHERE puskesmas_id = '" . $this->session->userdata("puskesmas_id") . "'
                    )"
                )
            );

            $templist = array();
            foreach ($peserta as $key => $row) {
                foreach ($row as $keys => $rows) {
                    $templist[$key][$keys] = $rows;
                }

                $data_master = $this->master_wilayah_model->get_by($row->kelurahan_master_wilayah_id);

                $data_kecamatan = $this->master_wilayah_model->get(
                    array(
                        "fields" => "nama_wilayah,kode_induk",
                        "where" => array(
                            "kode_wilayah" => $data_master->kode_induk
                        )
                    ),
                    "row"
                );

                $data_kabupaten = $this->master_wilayah_model->get(
                    array(
                        "fields" => "nama_wilayah,kode_induk",
                        "where" => array(
                            "kode_wilayah" => $data_kecamatan->kode_induk
                        )
                    ),
                    "row"
                );

                $data_provinsi = $this->master_wilayah_model->get(
                    array(
                        "fields" => "nama_wilayah,kode_induk",
                        "where" => array(
                            "kode_wilayah" => $data_kabupaten->kode_induk
                        )
                    ),
                    "row"
                );

                $templist[$key]['alamat_ktp'] = $row->alamat_ktp . (!empty($row->rt_ktp) ? " RT. " . $row->rt_ktp : "");

                $templist[$key]['alamat_domisili'] = $row->alamat_domisili . (!empty($row->rt_domisili) ? " RT. " . $row->rt_domisili : "") . ($data_master->klasifikasi == "KEL" ? " Kelurahan" : "Desa") . " " . $data_master->nama_wilayah . ", Kecamatan " . ucwords(strtolower($data_kecamatan->nama_wilayah)) . ", Kabupaten " . ucwords(strtolower($data_kabupaten->nama_wilayah)) . ", Provinisi " . ucwords(strtolower($data_provinsi->nama_wilayah));

                $templist[$key]['jenis_kelamin'] = ($row->jenis_kelamin == "L" ? "Laki-Laki" : "Perempuan");
                $templist[$key]['pekerjaan_inti'] = ($row->pekerjaan_inti == "Lainnya" ? $row->pekerjaan_inti_lainnya : $row->pekerjaan_inti);

                $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
            }
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function count_peserta_non_puskesmas()
    {
        $data = $this->peserta_non_puskesmas_model->query("
        SELECT COUNT(*) AS jumlah_peserta
        FROM(
            SELECT peserta_non_puskesmas.*
            FROM peserta_non_puskesmas
            INNER JOIN (
                SELECT trx_pemeriksaan_non_puskesmas.*
                FROM trx_pemeriksaan_non_puskesmas
                LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id
                WHERE id_trx_pemeriksaan_non_puskesmas IN (
                    SELECT MAX(id_trx_pemeriksaan_non_puskesmas) AS id_trx_pemeriksaan_non_puskesmas
                    FROM trx_pemeriksaan_non_puskesmas
                    WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    GROUP BY peserta_non_puskesmas_id
                )
                AND trx_pemeriksaan_id IS NULL AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL AND
                (IF((trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '2' OR trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '3') AND class_badge = '2',1,0) OR IF((trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id = '1' AND class_badge = '2'),1,0))
                GROUP BY peserta_non_puskesmas_id
            ) AS a ON a.peserta_non_puskesmas_id=id_peserta_non_puskesmas
            WHERE peserta_id IS NULL AND kelurahan_master_wilayah_id IN 
            (
            SELECT id_master_wilayah
            FROM master_wilayah
            WHERE puskesmas_id = '" . $this->session->userdata("puskesmas_id") . "'
            ) AND peserta_non_puskesmas.deleted_at IS NULL
        ) AS tbl
        ")->row();

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_tni_polri()
    {

        $data_peserta = $this->peserta_non_puskesmas_model->query("
        SELECT master_wilayah.*,IFNULL(d.jumlah_peserta,0) AS jumlah_peserta
        FROM master_wilayah
        LEFT JOIN (
            SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id
            FROM (
                SELECT peserta.*
                FROM peserta
                WHERE kelurahan_master_wilayah_id IN
                (
                SELECT id_master_wilayah
                FROM master_wilayah
                WHERE kode_induk = 
                (
                    SELECT kode_wilayah
                    FROM master_wilayah
                    WHERE id_master_wilayah = '" . $this->session->userdata("master_wilayah_id") . "'
                )
                )
                AND peserta.deleted_at IS NULL
            )AS tbl
            INNER JOIN
            (
                SELECT *
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                )
                ) AS a ON id_peserta=peserta_id
            WHERE status = '1'
            GROUP BY kelurahan_master_wilayah_id
        ) AS d ON id_master_wilayah=kelurahan_master_wilayah_id
        WHERE kode_induk = (
            SELECT kode_wilayah
            FROM master_wilayah
            WHERE id_master_wilayah = '" . $this->session->userdata("master_wilayah_id") . "'
        )
        ORDER BY jumlah_peserta DESC
        ")->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_detail_peserta_by_wilayah()
    {
        $id_wilayah = decrypt_data($this->iget("id_wilayah"));

        $data_peserta = $this->peserta_non_puskesmas_model->query("
        SELECT tbl.*,DATE_FORMAT(a.created_at,'%Y-%m-%d') AS tanggal_terkonfirmasi
        FROM (
            SELECT peserta.*
            FROM peserta
            WHERE kelurahan_master_wilayah_id = '" . $id_wilayah . "'
            AND peserta.deleted_at IS NULL
        )AS tbl
        INNER JOIN
        (
            SELECT *
            FROM trx_terkonfirmasi_rilis
            WHERE id_trx_terkonfirmasi_rilis IN (
                SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                FROM trx_terkonfirmasi_rilis 
                WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                GROUP BY peserta_id
                ORDER BY id_trx_terkonfirmasi_rilis DESC
            )
            ) AS a ON id_peserta=peserta_id
        WHERE status = '1'
        ")->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
            $templist[$key]['tanggal_terkonfirmasi_custom'] = longdate_indo($row->tanggal_terkonfirmasi);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_detail_peserta_tni_polri()
    {
        $id_peserta = $this->iget("id_peserta");

        $peserta = $this->peserta_model->get(
            array(
                "fields" => "peserta.*,IFNULL(user.nama_lengkap,'') as nama_user",
                "left_join" => array(
                    "user" => "id_user=id_user_created"
                ),
                "where" => array(
                    "id_peserta" => decrypt_data($id_peserta)
                )
            ),
            "row"
        );

        $data_master = $this->master_wilayah_model->get_by($peserta->kelurahan_master_wilayah_id);

        $data_kecamatan = $this->master_wilayah_model->get(
            array(
                "fields" => "nama_wilayah,kode_induk",
                "where" => array(
                    "kode_wilayah" => $data_master->kode_induk
                )
            ),
            "row"
        );

        $data_kabupaten = $this->master_wilayah_model->get(
            array(
                "fields" => "nama_wilayah,kode_induk",
                "where" => array(
                    "kode_wilayah" => $data_kecamatan->kode_induk
                )
            ),
            "row"
        );

        $data_provinsi = $this->master_wilayah_model->get(
            array(
                "fields" => "nama_wilayah,kode_induk",
                "where" => array(
                    "kode_wilayah" => $data_kabupaten->kode_induk
                )
            ),
            "row"
        );

        $peserta->alamat_ktp = $peserta->alamat_ktp . (!empty($peserta->rt_ktp) ? " RT. " . $peserta->rt_ktp : "");

        $peserta->alamat_domisili = $peserta->alamat_domisili . (!empty($peserta->rt_domisili) ? " RT. " . $peserta->rt_domisili : "") . ($data_master->klasifikasi == "KEL" ? " Kelurahan" : "Desa") . " " . $data_master->nama_wilayah . ", Kecamatan " . ucwords(strtolower($data_kecamatan->nama_wilayah)) . ", Kabupaten " . ucwords(strtolower($data_kabupaten->nama_wilayah)) . ", Provinisi " . ucwords(strtolower($data_provinsi->nama_wilayah));

        $peserta->jenis_kelamin = ($peserta->jenis_kelamin == "L" ? "Laki-Laki" : "Perempuan");
        $peserta->pekerjaan_inti = ($peserta->pekerjaan_inti == "Lainnya" ? $peserta->pekerjaan_inti_lainnya : $peserta->pekerjaan_inti);

        $data = $peserta;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_count_peserta_positif_by_wilayah()
    {
        $peserta = $this->peserta_model->query(
            "SELECT IFNULL(COUNT(*),0) AS jumlah_peserta
            FROM (
                SELECT peserta.*
                FROM peserta
                WHERE kelurahan_master_wilayah_id IN
                (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    WHERE kode_induk = 
                    (
                        SELECT kode_wilayah
                        FROM master_wilayah
                        WHERE id_master_wilayah = '" . $this->session->userdata("master_wilayah_id") . "'
                    )
                )
                AND peserta.deleted_at IS NULL
            )AS tbl
            INNER JOIN 
            (
                SELECT *
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                )
            ) AS a ON id_peserta=peserta_id
            WHERE status = '1'"
        )->row();

        $data = $peserta;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_status_rawat_peserta()
    {
        $id_trx_pemeriksaan_last = decrypt_data($this->iget("id_trx_pemeriksaan_last"));
        $id_peserta = decrypt_data($this->iget("id_peserta"));
        $peserta_dari = $this->iget("peserta_dari");

        if ($peserta_dari == "ch") {
            $data = $this->status_rawat_non_puskesmas_model->get(
                array(
                    "where" => array(
                        "peserta_non_puskesmas_id" => $id_peserta
                    ),
                    "order_by" => array(
                        "id_status_rawat_non_puskesmas" => "DESC"
                    ),
                    "limit" => "1"
                ),
                "row"
            );
        } else {
            $data = $this->status_rawat_model->get(
                array(
                    "where" => array(
                        "peserta_id" => $id_peserta
                    ),
                    "order_by" => array(
                        "id_status_rawat" => "DESC"
                    ),
                    "limit" => "1"
                ),
                "row"
            );
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function cek_nik()
    {
        $id_peserta = decrypt_data($this->iget("id_peserta"));
        $nik = $this->iget("nik");

        $data_peserta = $this->peserta_model->get(
            array(
                "where" => array(
                    "nik" => $nik
                ),
                "where_false" => "id_peserta != '{$id_peserta}'"
            )
        );

        $templist = array();
        foreach ($data_peserta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }


    public function get_peserta_kodim()
    {
        $report_today = $this->iget("report_today");

        $wh = "";
        if ($report_today == 'true') {
            $wh = "AND DATE_FORMAT(c.created_at,'%Y-%m-%d') ='" . date("Y-m-d") . "'";
        }

        $data_peserta_arsel = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != '' " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620101'
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_arsel = array();
        foreach ($data_peserta_arsel as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_arsel[$key][$keys] = $rows;
            }

            $templist_arsel[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);

            $arr_jumlah_rumah_arsel = array();
            if ($row->rt_domisili) {
                $expl_rt_domisili = explode("|", $row->rt_domisili);
                foreach ($expl_rt_domisili as $key_rt => $val_rt) {
                    $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                        "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$row->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.rt_domisili = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != ''
                            GROUP BY unique_id
                        ) AS tbl
                        "
                    )->row();

                    array_push($arr_jumlah_rumah_arsel, $jumlah_rumah->jumlah_rumah);
                }
            }
            $templist_arsel[$key]['jumlah_rumah'] = implode("|", $arr_jumlah_rumah_arsel);
        }

        $data_peserta_aruta = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != '' " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620102'
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_aruta = array();
        foreach ($data_peserta_aruta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_aruta[$key][$keys] = $rows;
            }

            $templist_aruta[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);

            $arr_jumlah_rumah_aruta = array();
            if ($row->rt_domisili) {
                $expl_rt_domisili = explode("|", $row->rt_domisili);
                foreach ($expl_rt_domisili as $key_rt => $val_rt) {
                    $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                        "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$row->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.rt_domisili = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != ''
                            GROUP BY unique_id
                        ) AS tbl
                        "
                    )->row();

                    array_push($arr_jumlah_rumah_aruta, $jumlah_rumah->jumlah_rumah);
                }
            }
            $templist_aruta[$key]['jumlah_rumah'] = implode("|", $arr_jumlah_rumah_aruta);
        }

        $data_peserta_kumai = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != '' " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620103'
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_kumai = array();
        foreach ($data_peserta_kumai as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_kumai[$key][$keys] = $rows;
            }

            $templist_kumai[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);

            $arr_jumlah_rumah_kumai = array();
            if ($row->rt_domisili) {
                $expl_rt_domisili = explode("|", $row->rt_domisili);
                foreach ($expl_rt_domisili as $key_rt => $val_rt) {
                    $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                        "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$row->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.rt_domisili = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != ''
                            GROUP BY unique_id
                        ) AS tbl
                        "
                    )->row();

                    array_push($arr_jumlah_rumah_kumai, $jumlah_rumah->jumlah_rumah);
                }
            }
            $templist_kumai[$key]['jumlah_rumah'] = implode("|", $arr_jumlah_rumah_kumai);
        }

        $data_peserta_kolam = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != '' " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620104'
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_kolam = array();
        foreach ($data_peserta_kolam as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_kolam[$key][$keys] = $rows;
            }

            $templist_kolam[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);

            $arr_jumlah_rumah_kolam = array();
            if ($row->rt_domisili) {
                $expl_rt_domisili = explode("|", $row->rt_domisili);
                foreach ($expl_rt_domisili as $key_rt => $val_rt) {
                    $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                        "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$row->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.rt_domisili = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != ''
                            GROUP BY unique_id
                        ) AS tbl
                        "
                    )->row();

                    array_push($arr_jumlah_rumah_kolam, $jumlah_rumah->jumlah_rumah);
                }
            }
            $templist_kolam[$key]['jumlah_rumah'] = implode("|", $arr_jumlah_rumah_kolam);
        }

        $data_peserta_banteng = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != '' " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620106'
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_banteng = array();
        foreach ($data_peserta_banteng as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_banteng[$key][$keys] = $rows;
            }

            $templist_banteng[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);

            $arr_jumlah_rumah_banteng = array();
            if ($row->rt_domisili) {
                $expl_rt_domisili = explode("|", $row->rt_domisili);
                foreach ($expl_rt_domisili as $key_rt => $val_rt) {
                    $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                        "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$row->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.rt_domisili = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != ''
                            GROUP BY unique_id
                        ) AS tbl
                        "
                    )->row();

                    array_push($arr_jumlah_rumah_banteng, $jumlah_rumah->jumlah_rumah);
                }
            }
            $templist_banteng[$key]['jumlah_rumah'] = implode("|", $arr_jumlah_rumah_banteng);
        }

        $data_peserta_lada = $this->peserta_model->query(
            "
            SELECT id_master_wilayah,nama_wilayah,GROUP_CONCAT(jumlah_peserta ORDER BY rt_domisili SEPARATOR '|') AS jumlah_peserta,GROUP_CONCAT(rt_domisili ORDER BY rt_domisili SEPARATOR '|') AS rt_domisili
            FROM master_wilayah
            LEFT JOIN 
            (
                SELECT COUNT(*) AS jumlah_peserta,kelurahan_master_wilayah_id,rt_domisili
                FROM peserta
                INNER JOIN 
                (
                    SELECT *
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                        FROM trx_terkonfirmasi_rilis 
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                        ORDER BY id_trx_terkonfirmasi_rilis DESC
                    )
                ) AS c ON c.peserta_id=id_peserta
                WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != '' " . $wh . "
                GROUP BY kelurahan_master_wilayah_id,rt_domisili
            ) AS a ON a.kelurahan_master_wilayah_id=id_master_wilayah
            WHERE klasifikasi IN ('KEL','DESA') AND kode_induk = '620105'
            GROUP BY id_master_wilayah
            "
        )->result();

        $templist_lada = array();
        foreach ($data_peserta_lada as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist_lada[$key][$keys] = $rows;
            }

            $templist_lada[$key]['id_encrypt'] = encrypt_data($row->id_master_wilayah);

            $arr_jumlah_rumah_lada = array();
            if ($row->rt_domisili) {
                $expl_rt_domisili = explode("|", $row->rt_domisili);
                foreach ($expl_rt_domisili as $key_rt => $val_rt) {
                    $jumlah_rumah = $this->peserta_dalam_satu_rumah_model->query(
                        "
                        SELECT COUNT(*) AS jumlah_rumah
                        FROM 
                        (
                            SELECT id_peserta_dalam_satu_rumah
                            FROM peserta_dalam_satu_rumah
                            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
                            INNER JOIN 
                            (
                                SELECT *
                                    FROM trx_terkonfirmasi_rilis
                                    WHERE id_trx_terkonfirmasi_rilis IN 
                                    (
                                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                                    FROM trx_terkonfirmasi_rilis 
                                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                                    GROUP BY peserta_id
                                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                                    )
                            ) AS e ON e.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
                            INNER JOIN peserta ON id_peserta=e.peserta_id AND peserta.deleted_at IS NULL
                            WHERE peserta_dalam_satu_rumah.kelurahan_master_wilayah_id = '{$row->id_master_wilayah}'
                            AND peserta_dalam_satu_rumah.rt_domisili = '{$val_rt}'
                            AND peserta_dalam_satu_rumah.deleted_at IS NULL
                            AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != ''
                            GROUP BY unique_id
                        ) AS tbl
                        "
                    )->row();

                    array_push($arr_jumlah_rumah_lada, $jumlah_rumah->jumlah_rumah);
                }
            }
            $templist_lada[$key]['jumlah_rumah'] = implode("|", $arr_jumlah_rumah_lada);
        }

        $data = array("arsel" => $templist_arsel, "aruta" => $templist_aruta, "kumai" => $templist_kumai, "kolam" => $templist_kolam, "banteng" => $templist_banteng, "lada" => $templist_lada);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_nik_empty()
    {
        $data_peserta_nik_empty = $this->peserta_model->query(
            "SELECT id_peserta,nama,alamat_domisili,rt_domisili,nama_wilayah
            FROM peserta
            INNER JOIN master_wilayah ON kelurahan_master_wilayah_id=id_master_wilayah
            WHERE 
            (
                id_user_created = '{$this->session->userdata("id_user")}'
                OR kelurahan_master_wilayah_id IN 
                (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    WHERE puskesmas_id = '{$this->session->userdata("puskesmas_id")}'
                ) 
            )
            AND 
            (
                nik IS NULL OR nik = ''
            )
            AND peserta.deleted_at IS NULL"
        )->result();

        $templist = array();
        foreach ($data_peserta_nik_empty as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_rt_domisili_empty()
    {
        $data_peserta_rt_domisili_empty = $this->peserta_model->query(
            "SELECT id_peserta,nama,nik,alamat_domisili,rt_domisili,nama_wilayah
            FROM peserta
            INNER JOIN master_wilayah ON kelurahan_master_wilayah_id=id_master_wilayah
            WHERE 
            (
                id_user_created = '{$this->session->userdata("id_user")}'
                OR kelurahan_master_wilayah_id IN 
                (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    WHERE puskesmas_id = '{$this->session->userdata("puskesmas_id")}'
                ) 
            )
            AND 
            (
                rt_domisili IS NULL OR rt_domisili = '' OR rt_domisili = '0'
            )
            AND peserta.deleted_at IS NULL"
        )->result();

        $templist = array();
        foreach ($data_peserta_rt_domisili_empty as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_detail_peserta_update_nik_rt_domisili()
    {
        $id_peserta = decrypt_data($this->iget("id_peserta"));

        $data = $this->peserta_model->get_by($id_peserta);
        $data->id_encrypt = encrypt_data($data->id_peserta);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_list_peserta_polres_kodim()
    {
        $id_master_wilayah = decrypt_data($this->iget("id_master_wilayah"));
        $rt_domisili = $this->iget("rt_domisili");

        $data_peserta = $this->peserta_model->query(
            "
            SELECT peserta.*,DATE_FORMAT(c.created_at,'%Y-%m-%d') AS tanggal_terkonfirmasi,id_detail_peserta_dalam_satu_rumah
            FROM peserta
            INNER JOIN 
            (
                SELECT *
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                )
            ) AS c ON c.peserta_id=id_peserta
            LEFT JOIN detail_peserta_dalam_satu_rumah ON detail_peserta_dalam_satu_rumah.peserta_id=c.peserta_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
            WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND rt_domisili IS NOT NULL AND rt_domisili != '' AND kelurahan_master_wilayah_id = '" . $id_master_wilayah . "' AND rt_domisili = '" . $rt_domisili . "'
            "
        )->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $data_master = $this->master_wilayah_model->get_by($row->kelurahan_master_wilayah_id);

            $data_kecamatan = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_master->kode_induk
                    )
                ),
                "row"
            );

            $data_kabupaten = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_kecamatan->kode_induk
                    )
                ),
                "row"
            );

            $data_provinsi = $this->master_wilayah_model->get(
                array(
                    "fields" => "nama_wilayah,kode_induk",
                    "where" => array(
                        "kode_wilayah" => $data_kabupaten->kode_induk
                    )
                ),
                "row"
            );

            if ($row->kelurahan_master_wilayah_id == "109") {
                $templist[$key]['alamat_domisili'] = "Wilayah Kobar Lainnya";
            } else {
                $templist[$key]['alamat_domisili'] = $row->alamat_domisili . (!empty($row->rt_domisili) ? " RT. " . $row->rt_domisili : "") . ($data_master->klasifikasi == "KEL" ? " Kelurahan" : " Desa") . " " . $data_master->nama_wilayah . ", Kecamatan " . ucwords(strtolower($data_kecamatan->nama_wilayah)) . ", Kabupaten " . ucwords(strtolower($data_kabupaten->nama_wilayah)) . ", Provinisi " . ucwords(strtolower($data_provinsi->nama_wilayah));
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
            $templist[$key]['tanggal_terkonfirmasi_custom'] = longdate_indo($row->tanggal_terkonfirmasi);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_list_peserta_polres_kodim_per_rumah()
    {
        $id_master_wilayah = decrypt_data($this->iget("id_master_wilayah"));
        $rt_domisili = $this->iget("rt_domisili");

        $data_peserta = $this->peserta_model->query(
            "
            SELECT unique_id,GROUP_CONCAT(DATE_FORMAT(c.created_at,'%Y-%m-%d') ORDER BY id_peserta SEPARATOR '|') AS tanggal_terkonfirmasi,GROUP_CONCAT(nama ORDER BY id_peserta SEPARATOR '|') AS nama_peserta,GROUP_CONCAT(id_peserta ORDER BY id_peserta SEPARATOR '|') AS id_peserta
            FROM peserta_dalam_satu_rumah
            INNER JOIN detail_peserta_dalam_satu_rumah ON id_peserta_dalam_satu_rumah=peserta_dalam_satu_rumah_id AND detail_peserta_dalam_satu_rumah.deleted_at IS NULL
            LEFT JOIN 
            (
                SELECT *
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                )
            ) AS c ON c.peserta_id=detail_peserta_dalam_satu_rumah.peserta_id
            LEFT JOIN peserta ON id_peserta=c.peserta_id
            WHERE peserta.deleted_at IS NULL AND STATUS = '1' AND peserta.rt_domisili IS NOT NULL AND peserta.rt_domisili != '' AND peserta.kelurahan_master_wilayah_id = '" . $id_master_wilayah . "' AND peserta.rt_domisili = '" . $rt_domisili . "' AND peserta_dalam_satu_rumah.deleted_at IS NULL
            GROUP BY unique_id
            "
        )->result();

        $templist = array();
        foreach ($data_peserta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function count_peserta_notify()
    {
        $data = $this->peserta_model->get(
            array(
                "fields" => "COUNT(*) AS jumlah_peserta",
                "where" => array(
                    "notify_sembuh_from_polsek" => "1"
                ),
                "where_false" => "kelurahan_master_wilayah_id IN
                (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    WHERE puskesmas_id = '" . $this->session->userdata('puskesmas_id') . "'
                )",
            ),
            "row"
        );

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_peserta_notify_polsek()
    {
        $data_peserta = $this->peserta_model->get(
            array(
                "where" => array(
                    "notify_sembuh_from_polsek" => "1"
                ),
                "where_false" => "kelurahan_master_wilayah_id IN
                (
                    SELECT id_master_wilayah
                    FROM master_wilayah
                    WHERE puskesmas_id = '" . $this->session->userdata('puskesmas_id') . "'
                )",
            )
        );

        $templist = array();
        foreach ($data_peserta as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_peserta);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_master_rt()
    {
        $kel_desa = decrypt_data($this->iget("kel_desa"));

        $data_rt = $this->master_rt_model->get(
            array(
                "where" => array(
                    "master_wilayah_id" => $kel_desa
                ),
                "order_by" => array(
                    "rt" => "ASC"
                )
            )
        );

        $templist = array();
        foreach ($data_rt as $key => $row) {

            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_rt);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
