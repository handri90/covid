<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Peserta extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model('trx_pemeriksaan_model');
        $this->load->model('peserta_rilis/trx_terkonfirmasi_rilis_model', 'trx_terkonfirmasi_rilis_model');
        $this->load->model('trx_pemeriksaan_non_puskesmas_model');
        $this->load->model('peserta_model');
        $this->load->model('status_rawat_model');
        $this->load->model('status_rawat_non_puskesmas_model');
        $this->load->model('peserta_non_puskesmas_model');
        $this->load->model('master_wilayah_model');
        $this->load->model('user_model');
        $this->load->model('peserta_dalam_satu_rumah_model');
        $this->load->model('detail_peserta_dalam_satu_rumah_model');
        $this->load->model('jenis_pemeriksaan/jenis_pemeriksaan_model', 'jenis_pemeriksaan_model');
        $this->load->model('hasil_pemeriksaan/hasil_pemeriksaan_model', 'hasil_pemeriksaan_model');
        $this->load->model('status_peserta/trx_terkonfirmasi_model', 'trx_terkonfirmasi_model');
    }

    public function index()
    {
        $data['breadcrumb'] = [['link' => false, 'content' => 'Peserta', 'is_active' => true]];

        $level = $this->session->userdata("level_user_id");

        //faskes non puskesmas
        if ($level == "7") {
            $data['jenis_pemeriksaan'] = $this->jenis_pemeriksaan_model->get(
                array(
                    "where" => array(
                        "id_jenis_pemeriksaan !=" => "1"
                    )
                )
            );
            //faskes puskesmas
        } else if ($level == '3') {
            $data['jenis_pemeriksaan'] = $this->jenis_pemeriksaan_model->query(
                "SELECT *
                FROM (
                    SELECT id_jenis_pemeriksaan,nama_jenis_pemeriksaan
                    FROM jenis_pemeriksaan
                    WHERE jenis_pemeriksaan.deleted_at IS NULL
                    UNION
                    SELECT '9004','Usulan Faskes : SWAB PCR'
                    ) AS tbl
                ORDER BY id_jenis_pemeriksaan ASC"
            )->result();
        } else {
            $data['jenis_pemeriksaan'] = $this->jenis_pemeriksaan_model->query(
                "SELECT *
                FROM (
                    SELECT id_jenis_pemeriksaan,nama_jenis_pemeriksaan
                    FROM jenis_pemeriksaan
                    WHERE jenis_pemeriksaan.deleted_at IS NULL
                    UNION
                    SELECT '9001','Sembuh'
                    UNION
                    SELECT '9002','Meninggal'
                    UNION
                    SELECT '9003','Probable'
                    UNION
                    SELECT '9004','Usulan Faskes : SWAB PCR'
                    UNION
                    SELECT '9005','Usulan Faskes : Isolasi Mandiri'
                    UNION
                    SELECT '9006','Bebas Isolasi Mandiri'
                    UNION
                    SELECT '9007','Bebas Isolasi Mandiri Suspek'
                    ) AS tbl
                ORDER BY id_jenis_pemeriksaan ASC"
            )->result();
            //labkesda
        }

        if ($this->session->userdata("level_user_id") == "4") {
            $this->execute('index_admin_rs', $data);
        } else if ($this->session->userdata("level_user_id") == "3") {
            $this->execute('index_admin_labkesda', $data);
        } else if ($this->session->userdata("level_user_id") == "6") {
            $this->execute('index_admin_dinkes', $data);
        } else if ($this->session->userdata("level_user_id") == "7") {
            $this->execute('index_admin_non_puskesmas', $data);
        } else if ($this->session->userdata("level_user_id") == "8") {
            $this->execute('index_admin_lab_rs', $data);
        } else if ($this->session->userdata("level_user_id") == "9") {
            $this->execute('index_admin_rs_dokter', $data);
        } else if ($this->session->userdata("level_user_id") == "10") {
            $data['master_wilayah'] = $this->master_wilayah_model->get(
                array(
                    "fields" => "master_wilayah.*,concat(klasifikasi,' - ',nama_wilayah) as nama_wilayah",
                    "where_false" => "klasifikasi NOT IN ('PROV','KAB','KEC') AND kode_induk = 
                    (
                        SELECT kode_wilayah
                        FROM master_wilayah
                        WHERE id_master_wilayah = '" . $this->session->userdata("master_wilayah_id") . "'
                    )",
                    "order_by_false" => 'field (klasifikasi,"KEC","KEL","DESA"),nama_wilayah'
                )
            );

            $this->execute('index_admin_tni_polri', $data);
        } else if ($this->session->userdata("level_user_id") == "11") {
            $this->execute('index_admin_polres_kodim', $data);
        } else {
            $this->execute('index_puskesmas', $data);
        }
    }

    public function tambah_peserta()
    {
        $data['pekerjaan'] = array("1" => "Polisi", "2" => "TNI", "3" => "ASN", "4" => "BUMN / BANK", "5" => "Tenaga Kesehatan", "6" => "Dosen / Guru", "7" => "Pelajar/Mahasiswa", "8" => "Ibu Rumah Tangga", "9" => "Ulama / Pendeta /  Rohaniawan", "10" => "Swasta", "11" => "Wiraswasta", "12" => "Umum / Keluarga / Pensiunan / Unidentifi", "13" => "Lainnya");

        if (empty($_POST)) {
            $data['master_wilayah'] = $this->master_wilayah_model->get(
                array(
                    "fields" => "master_wilayah.*,concat(klasifikasi,' - ',nama_wilayah) as nama_wilayah",
                    "where" => array(
                        "id_master_wilayah != " => "109"
                    ),
                    "where_false" => "klasifikasi NOT IN ('PROV','KAB','KEC')",
                    "order_by_false" => 'field (klasifikasi,"KEC","KEL","DESA"),nama_wilayah'
                )
            );

            $wh = array();
            if (in_array($this->session->userdata("level_user_id"), array("2", "7"))) {
                if ($this->session->userdata("id_user") != $this->config->item('id_user_citra_husada')) {
                    $wh = array(
                        "id_jenis_pemeriksaan !=" => "1"
                    );
                }
            }

            $data['jenis_pemeriksaan'] = $this->jenis_pemeriksaan_model->get(
                array(
                    "where" => $wh
                )
            );

            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'peserta', 'content' => 'Peserta', 'is_active' => false], ['link' => false, 'content' => 'Tambah Peserta', 'is_active' => true]];

            if ($this->session->userdata("level_user_id") == "2") {
                $this->execute('form_peserta_faskes', $data);
            } else if ($this->session->userdata("level_user_id") == "7") {
                if ($this->session->userdata("id_user") == $this->config->item('id_user_citra_husada')) {
                    $this->execute('form_peserta_non_faskes_ch', $data);
                } else {
                    $this->execute('form_peserta_non_faskes', $data);
                }
            } else if ($this->session->userdata("level_user_id") == "6") {
                $this->execute('form_peserta_dinkes', $data);
            } else if ($this->session->userdata("level_user_id") == "4") {
                $this->execute('form_peserta_rs', $data);
            } else if ($this->session->userdata("level_user_id") == "3") {
                $this->execute('form_peserta_labkesda', $data);
            } else {
                $this->execute('form_peserta', $data);
            }
        } else {

            $date_exp = explode("/", $this->ipost('tanggal_lahir'));
            $tanggal_lahir = $date_exp[2] . "-" . $date_exp[1] . "-" . $date_exp[0];

            if ($this->ipost('tanggal_pemeriksaan')) {
                $tanggal_pemeriksaan_exp = explode("/", $this->ipost('tanggal_pemeriksaan'));
                $tanggal_pemeriksaan = $tanggal_pemeriksaan_exp[2] . "-" . $tanggal_pemeriksaan_exp[1] . "-" . $tanggal_pemeriksaan_exp[0];
            } else {
                $tanggal_pemeriksaan = date("Y-m-d");
            }

            $date_exp = explode("/", $this->ipost('tanggal_mulai_bergejala'));
            $tanggal_mulai_bergejala = $date_exp[2] . "-" . $date_exp[1] . "-" . $date_exp[0];

            if ($this->session->userdata("level_user_id") == "7") {
                $data = array(
                    "nama" => $this->ipost('nama'),
                    "nik" => $this->ipost('nik'),
                    "tanggal_lahir" => $tanggal_lahir,
                    "jenis_kelamin" => $this->ipost('jenis_kelamin'),
                    "pekerjaan_inti" => $data['pekerjaan'][$this->ipost('pekerjaan_inti')],
                    "pekerjaan_inti_lainnya" => (in_array($this->ipost('pekerjaan_inti'), array("10", "11", "13")) ? $this->ipost('pekerjaan_lainnya') : ""),
                    "alamat_ktp" => $this->ipost('alamat_ktp'),
                    "rt_ktp" => $this->ipost('rt_ktp'),
                    "is_copy_ktp" => ($this->ipost('copy_ktp') == "yes" ? "1" : "0"),
                    "alamat_domisili" => ($this->ipost('copy_ktp') == "yes" ? $this->ipost('alamat_ktp') : $this->ipost('alamat_domisili')),
                    "master_rt_domisili_id" => ($this->ipost('luar_kobar') == "yes" ? NULL : decrypt_data($this->ipost('rt_domisili'))),
                    "kelurahan_master_wilayah_id" => ($this->ipost('luar_kobar') == "yes" ? "109" : decrypt_data($this->ipost('kelurahan_desa'))),
                    "nomor_telepon" => $this->ipost('nomor_telepon'),
                    "kontak_erat" => ($this->ipost('kontak_erat') ? $this->ipost('kontak_erat') : NULL),
                    "no_rekam_medis" => ($this->ipost('nomor_rekam_medis') ? $this->ipost('nomor_rekam_medis') : NULL),
                    'created_at' => $tanggal_pemeriksaan . " " . date("H:i:s"),
                    'id_user_created' => $this->session->userdata("id_user")
                );

                $status = $this->peserta_non_puskesmas_model->save($data);
            } else {
                $data = array(
                    "nama" => $this->ipost('nama'),
                    "nik" => $this->ipost('nik'),
                    "tanggal_lahir" => $tanggal_lahir,
                    "jenis_kelamin" => $this->ipost('jenis_kelamin'),
                    "pekerjaan_inti" => $data['pekerjaan'][$this->ipost('pekerjaan_inti')],
                    "pekerjaan_inti_lainnya" => (in_array($this->ipost('pekerjaan_inti'), array("10", "11", "13")) ? $this->ipost('pekerjaan_lainnya') : ""),
                    "alamat_ktp" => $this->ipost('alamat_ktp'),
                    "rt_ktp" => $this->ipost('rt_ktp'),
                    "is_copy_ktp" => ($this->ipost('copy_ktp') == "yes" ? "1" : "0"),
                    "alamat_domisili" => ($this->ipost('copy_ktp') == "yes" ? $this->ipost('alamat_ktp') : $this->ipost('alamat_domisili')),
                    "master_rt_domisili_id" => ($this->ipost('luar_kobar') == "yes" ? NULL : decrypt_data($this->ipost('rt_domisili'))),
                    "kelurahan_master_wilayah_id" => ($this->ipost('luar_kobar') == "yes" ? "109" : decrypt_data($this->ipost('kelurahan_desa'))),
                    "nomor_telepon" => $this->ipost('nomor_telepon'),
                    "kontak_erat" => ($this->ipost('kontak_erat') ? $this->ipost('kontak_erat') : NULL),
                    "kategori_data" => ($this->ipost('kategori_data') ? $this->ipost('kategori_data') : NULL),
                    "tanggal_mulai_bergejala" => ($this->ipost('tanggal_mulai_bergejala') ? $tanggal_mulai_bergejala : NULL),
                    "no_rekam_medis" => ($this->ipost('nomor_rekam_medis') ? $this->ipost('nomor_rekam_medis') : NULL),
                    'created_at' => $tanggal_pemeriksaan . " " . date("H:i:s"),
                    'id_user_created' => $this->session->userdata("id_user")
                );

                $status = $this->peserta_model->save($data);
            }


            if ($status) {
                if ($this->ipost("jenis_pemeriksaan")) {
                    if (in_array($this->session->userdata("level_user_id"), array("2", "7"))) {
                        $data_hasil_pemeriksaan = $this->hasil_pemeriksaan_model->get_by(decrypt_data($this->ipost("hasil_pemeriksaan")));

                        if ($data_hasil_pemeriksaan) {
                            if ($data_hasil_pemeriksaan->class_badge == "2") {
                                $usulan_faskes = $this->ipost("usulan_faskes");
                            } else {
                                $usulan_faskes = NULL;
                            }
                        } else {
                            $usulan_faskes = NULL;
                        }

                        if ($this->session->userdata("level_user_id") == "7") {
                            if ($this->session->userdata("id_user") == $this->config->item('id_user_citra_husada')) {
                                $data_trx_pemeriksaan = array(
                                    "peserta_non_puskesmas_id" => $status,
                                    "jenis_pemeriksaan_id" => decrypt_data($this->ipost("jenis_pemeriksaan")),
                                    "hasil_pemeriksaan_id" => ($this->ipost("hasil_pemeriksaan") ? decrypt_data($this->ipost("hasil_pemeriksaan")) : NULL),
                                    "tracking_pemeriksaan" => "1",
                                    "kode_sample" => ($this->ipost("kode_sample") ? $this->ipost("kode_sample") : NULL),
                                    "jenis_spesimen" => ($this->ipost("jenis_spesimen") ? $this->ipost("jenis_spesimen") : NULL),
                                    'created_at' => $tanggal_pemeriksaan . " " . date("H:i:s"),
                                    'id_user_created' => $this->session->userdata("id_user")
                                );
                            } else {
                                $data_trx_pemeriksaan = array(
                                    "peserta_non_puskesmas_id" => $status,
                                    "jenis_pemeriksaan_id" => decrypt_data($this->ipost("jenis_pemeriksaan")),
                                    "hasil_pemeriksaan_id" => decrypt_data($this->ipost("hasil_pemeriksaan")),
                                    "tracking_pemeriksaan" => "1",
                                    "usulan_faskes" => NULL,
                                    'created_at' => $tanggal_pemeriksaan . " " . date("H:i:s"),
                                    'id_user_created' => $this->session->userdata("id_user")
                                );
                            }

                            $this->trx_pemeriksaan_non_puskesmas_model->save($data_trx_pemeriksaan);
                        } else {
                            $data_trx_pemeriksaan = array(
                                "peserta_id" => $status,
                                "jenis_pemeriksaan_id" => decrypt_data($this->ipost("jenis_pemeriksaan")),
                                "hasil_pemeriksaan_id" => decrypt_data($this->ipost("hasil_pemeriksaan")),
                                "tracking_pemeriksaan" => "1",
                                "usulan_faskes" => $usulan_faskes,
                                'created_at' => $tanggal_pemeriksaan . " " . date("H:i:s"),
                                'id_user_created' => $this->session->userdata("id_user")
                            );

                            $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);
                        }
                    } else {

                        $data_trx_pemeriksaan = array(
                            "peserta_id" => $status,
                            "jenis_pemeriksaan_id" => decrypt_data($this->ipost("jenis_pemeriksaan")),
                            "hasil_pemeriksaan_id" => ($this->ipost("hasil_pemeriksaan") ? decrypt_data($this->ipost("hasil_pemeriksaan")) : NULL),
                            "kode_sample" => $this->ipost("kode_sample"),
                            "jenis_spesimen" => ($this->ipost("jenis_spesimen") ? $this->ipost("jenis_spesimen") : NULL),
                            "tracking_pemeriksaan" => "1",
                            'created_at' => $tanggal_pemeriksaan . " " . date("H:i:s"),
                            'id_user_created' => $this->session->userdata("id_user")
                        );

                        $status_pemeriksaan = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);

                        if ($this->session->userdata("level_user_id") == "4" && decrypt_data($this->ipost("jenis_pemeriksaan")) == "1" && decrypt_data($this->ipost("hasil_pemeriksaan")) == "1") {
                            $data_trx_terkonfirmasi = array(
                                "peserta_id" => $status,
                                "tanggal_terkonfirmasi" => $this->datetime(),
                                "status" => "1",
                                "trx_pemeriksaan_id" => $status_pemeriksaan,
                                'created_at' => $tanggal_pemeriksaan . " " . date("H:i:s"),
                                'id_user_created' => $this->session->userdata("id_user")
                            );

                            $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi);
                        }
                    }
                } else {

                    if ($this->session->userdata("level_user_id") != "6") {
                        $data_trx_pemeriksaan = array(
                            "peserta_id" => $status,
                            "usulan_faskes" => $this->ipost("usulan_faskes"),
                            "tracking_pemeriksaan" => "1",
                            'created_at' => $tanggal_pemeriksaan . " " . date("H:i:s"),
                            'id_user_created' => $this->session->userdata("id_user")
                        );

                        $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);
                    }
                }

                $this->session->set_flashdata('message', 'Data baru berhasil ditambahkan');
            } else {
                $this->session->set_flashdata('message', 'Data baru gagal ditambahkan');
            }

            redirect('peserta');
        }
    }

    public function edit_peserta($id_peserta)
    {
        if ($this->session->userdata("level_user_id") == "7") {
            $data_master = $this->peserta_non_puskesmas_model->get(
                array(
                    "fields" => "peserta_non_puskesmas.*,DATE_FORMAT(tanggal_lahir,'%d/%m/%Y') as tanggal_lahir",
                    "where" => array(
                        "id_peserta_non_puskesmas" => decrypt_data($id_peserta)
                    )
                ),
                "row"
            );
        } else {
            $data_master = $this->peserta_model->get(
                array(
                    "fields" => "peserta.*,DATE_FORMAT(tanggal_lahir,'%d/%m/%Y') as tanggal_lahir,DATE_FORMAT(tanggal_mulai_bergejala,'%d/%m/%Y') as tanggal_mulai_bergejala",
                    "where" => array(
                        "id_peserta" => decrypt_data($id_peserta)
                    )
                ),
                "row"
            );
        }

        if (!$data_master) {
            $this->page_error();
        }

        $data['master_wilayah'] = $this->master_wilayah_model->get(
            array(
                "fields" => "master_wilayah.*,concat(klasifikasi,' - ',nama_wilayah) as nama_wilayah",
                "where" => array(
                    "id_master_wilayah != " => "109"
                ),
                "where_false" => "klasifikasi NOT IN ('PROV','KAB','KEC')",
                "order_by_false" => 'field (klasifikasi,"KEC","KEL","DESA"),nama_wilayah'
            )
        );

        $data['pekerjaan'] = array("1" => "Polisi", "2" => "TNI", "3" => "ASN", "4" => "BUMN / BANK", "5" => "Tenaga Kesehatan", "6" => "Dosen / Guru", "7" => "Pelajar/Mahasiswa", "8" => "Ibu Rumah Tangga", "9" => "Ulama / Pendeta /  Rohaniawan", "10" => "Swasta", "11" => "Wiraswasta", "12" => "Umum / Keluarga / Pensiunan / Unidentifi", "13" => "Lainnya");

        if (empty($_POST)) {
            $data['content'] = $data_master;

            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'peserta', 'content' => 'Peserta', 'is_active' => false], ['link' => false, 'content' => 'Ubah Peserta', 'is_active' => true]];
            if ($this->session->userdata("level_user_id") == "2") {
                $this->execute('form_peserta_faskes', $data);
            } else if ($this->session->userdata("level_user_id") == "7") {
                if ($this->session->userdata("id_user") == $this->config->item('id_user_citra_husada')) {
                    $this->execute('form_peserta_non_faskes_ch', $data);
                } else {
                    $this->execute('form_peserta_non_faskes', $data);
                }
            } else if ($this->session->userdata("level_user_id") == "6") {
                $this->execute('form_peserta_dinkes', $data);
            } else if ($this->session->userdata("level_user_id") == "4") {
                $this->execute('form_peserta_rs', $data);
            } else {
                $this->execute('form_peserta', $data);
            }
        } else {

            $date_exp = explode("/", $this->ipost('tanggal_lahir'));
            $tanggal_lahir = $date_exp[2] . "-" . $date_exp[1] . "-" . $date_exp[0];

            $date_exp = explode("/", $this->ipost('tanggal_mulai_bergejala'));
            $tanggal_mulai_bergejala = $date_exp[2] . "-" . $date_exp[1] . "-" . $date_exp[0];

            if ($this->session->userdata("level_user_id") == "7") {
                $data = array(
                    "nama" => $this->ipost('nama'),
                    "nik" => $this->ipost('nik'),
                    "tanggal_lahir" => $tanggal_lahir,
                    "jenis_kelamin" => $this->ipost('jenis_kelamin'),
                    "pekerjaan_inti" => $data['pekerjaan'][$this->ipost('pekerjaan_inti')],
                    "pekerjaan_inti_lainnya" => (in_array($this->ipost('pekerjaan_inti'), array("10", "11", "13")) ? $this->ipost('pekerjaan_lainnya') : ""),
                    "alamat_ktp" => $this->ipost('alamat_ktp'),
                    "rt_ktp" => $this->ipost('rt_ktp'),
                    "is_copy_ktp" => ($this->ipost('copy_ktp') == "yes" ? "1" : "0"),
                    "alamat_domisili" => ($this->ipost('copy_ktp') == "yes" ? $this->ipost('alamat_ktp') : $this->ipost('alamat_domisili')),
                    "master_rt_domisili_id" => ($this->ipost('luar_kobar') == "yes" ? NULL : decrypt_data($this->ipost('rt_domisili'))),
                    "kelurahan_master_wilayah_id" => ($this->ipost('luar_kobar') == "yes" ? "109" : decrypt_data($this->ipost('kelurahan_desa'))),
                    "nomor_telepon" => $this->ipost('nomor_telepon'),
                    "kontak_erat" => ($this->ipost('kontak_erat') ? $this->ipost('kontak_erat') : NULL),
                    "no_rekam_medis" => ($this->ipost('nomor_rekam_medis') ? $this->ipost('nomor_rekam_medis') : NULL),
                    'updated_at' => $this->datetime(),
                    'id_user_updated' => $this->session->userdata("id_user")
                );

                $status = $this->peserta_non_puskesmas_model->edit(decrypt_data($id_peserta), $data);
            } else {
                $data = array(
                    "nama" => $this->ipost('nama'),
                    "nik" => $this->ipost('nik'),
                    "tanggal_lahir" => $tanggal_lahir,
                    "jenis_kelamin" => $this->ipost('jenis_kelamin'),
                    "pekerjaan_inti" => $data['pekerjaan'][$this->ipost('pekerjaan_inti')],
                    "pekerjaan_inti_lainnya" => (in_array($this->ipost('pekerjaan_inti'), array("10", "11", "13")) ? $this->ipost('pekerjaan_lainnya') : ""),
                    "alamat_ktp" => $this->ipost('alamat_ktp'),
                    "rt_ktp" => $this->ipost('rt_ktp'),
                    "is_copy_ktp" => ($this->ipost('copy_ktp') == "yes" ? "1" : "0"),
                    "alamat_domisili" => ($this->ipost('copy_ktp') == "yes" ? $this->ipost('alamat_ktp') : $this->ipost('alamat_domisili')),
                    "master_rt_domisili_id" => ($this->ipost('luar_kobar') == "yes" ? NULL : decrypt_data($this->ipost('rt_domisili'))),
                    "kelurahan_master_wilayah_id" => ($this->ipost('luar_kobar') == "yes" ? "109" : decrypt_data($this->ipost('kelurahan_desa'))),
                    "nomor_telepon" => $this->ipost('nomor_telepon'),
                    "kontak_erat" => ($this->ipost('kontak_erat') ? $this->ipost('kontak_erat') : NULL),
                    "kategori_data" => ($this->ipost('kategori_data') ? $this->ipost('kategori_data') : NULL),
                    "tanggal_mulai_bergejala" => ($this->ipost('tanggal_mulai_bergejala') ? $tanggal_mulai_bergejala : NULL),
                    "no_rekam_medis" => ($this->ipost('nomor_rekam_medis') ? $this->ipost('nomor_rekam_medis') : NULL),
                    'updated_at' => $this->datetime(),
                    'id_user_updated' => $this->session->userdata("id_user")
                );

                $status = $this->peserta_model->edit(decrypt_data($id_peserta), $data);
            }


            if ($status) {
                $this->session->set_flashdata('message', 'Data berhasil diubah');
            } else {
                $this->session->set_flashdata('message', 'Data gagal diubah');
            }

            redirect('peserta');
        }
    }

    public function delete_peserta()
    {
        $id_peserta = decrypt_data($this->iget('id_peserta'));
        $data_master = $this->peserta_model->get_by($id_peserta);

        if (!$data_master) {
            $this->page_error();
        }

        //hapus transaction
        $trx_pemeriksaan = $this->trx_pemeriksaan_model->get(
            array(
                "where" => array(
                    "peserta_id" => $id_peserta
                )
            )
        );

        if (count($trx_pemeriksaan) > 0) {
            foreach ($trx_pemeriksaan as $key => $row) {
                //cek trx pemeriksaan non puskes
                $trx_pemeriksaan_non_puskes = $this->trx_pemeriksaan_non_puskesmas_model->get(
                    array(
                        "where" => array(
                            "trx_pemeriksaan_id" => $row->id_trx_pemeriksaan
                        )
                    ),
                    "row"
                );

                if ($trx_pemeriksaan_non_puskes) {
                    $this->trx_pemeriksaan_non_puskesmas_model->edit($trx_pemeriksaan_non_puskes->id_trx_pemeriksaan_non_puskesmas, array("trx_pemeriksaan_id" => NULL));
                }

                $this->trx_pemeriksaan_model->edit($row->id_trx_pemeriksaan, array("deleted_at" => $this->datetime(), "id_user_deleted" => $this->session->userdata("id_user")));
            }
        }

        //cek peserta non puskes connection
        $peserta_non_puskes_cek_id = $this->peserta_non_puskesmas_model->get(
            array(
                "where" => array(
                    "peserta_id" => $id_peserta
                )
            ),
            "row"
        );

        if ($peserta_non_puskes_cek_id) {
            $this->peserta_non_puskesmas_model->edit($peserta_non_puskes_cek_id->id_peserta_non_puskesmas, array("peserta_id" => NULL));
        }

        $data = array(
            "deleted_at" => $this->datetime(),
            "id_user_deleted" => $this->session->userdata("id_user")
        );

        $status = $this->peserta_model->edit($id_peserta, $data);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function delete_peserta_non_puskesmas()
    {
        $id_peserta = $this->iget('id_peserta');
        $data_master = $this->peserta_non_puskesmas_model->get_by(decrypt_data($id_peserta));

        if (!$data_master) {
            $this->page_error();
        }

        $data = array(
            "deleted_at" => $this->datetime(),
            "id_user_deleted" => $this->session->userdata("id_user")
        );

        $status = $this->peserta_non_puskesmas_model->edit(decrypt_data($id_peserta), $data);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function detail_peserta($id_peserta, $kode_sample = "")
    {

        $data['kode_sample'] = $kode_sample;
        if ($this->session->userdata("level_user_id") == "7") {
            $data['id_peserta_non_puskesmas'] = $id_peserta;

            $data['content'] = $this->peserta_non_puskesmas_model->get(
                array(
                    "fields" => "peserta_non_puskesmas.*,IFNULL(user.nama_lengkap,'') as nama_user",
                    "left_join" => array(
                        "user" => "id_user=id_user_created"
                    ),
                    "where" => array(
                        "id_peserta_non_puskesmas" => decrypt_data($id_peserta)
                    )
                ),
                "row"
            );
        } else {
            $data['id_peserta'] = $id_peserta;

            $data['content'] = $this->peserta_model->get(
                array(
                    "fields" => "peserta.*,IFNULL(user.nama_lengkap,'') as nama_user",
                    "left_join" => array(
                        "user" => "id_user=id_user_created"
                    ),
                    "where" => array(
                        "id_peserta" => decrypt_data($id_peserta)
                    )
                ),
                "row"
            );
        }


        $data_master = $this->master_wilayah_model->get_by($data['content']->kelurahan_master_wilayah_id);

        $data_kecamatan = $this->master_wilayah_model->get(
            array(
                "fields" => "nama_wilayah,kode_induk",
                "where" => array(
                    "kode_wilayah" => $data_master->kode_induk
                )
            ),
            "row"
        );

        $data_kabupaten = $this->master_wilayah_model->get(
            array(
                "fields" => "nama_wilayah,kode_induk",
                "where" => array(
                    "kode_wilayah" => $data_kecamatan->kode_induk
                )
            ),
            "row"
        );

        $data_provinsi = $this->master_wilayah_model->get(
            array(
                "fields" => "nama_wilayah,kode_induk",
                "where" => array(
                    "kode_wilayah" => $data_kabupaten->kode_induk
                )
            ),
            "row"
        );

        $data['content']->alamat_ktp = $data['content']->alamat_ktp . (!empty($data['content']->rt_ktp) ? " RT. " . $data['content']->rt_ktp : "");

        if ($data['content']->kelurahan_master_wilayah_id == "109") {
            $data['content']->alamat_domisili = "Wilayah Kobar Lainnya";
        } else {
            $data['content']->alamat_domisili = $data['content']->alamat_domisili . (!empty($data['content']->rt_domisili) ? " RT. " . $data['content']->rt_domisili : "") . ($data_master->klasifikasi == "KEL" ? " Kelurahan" : "Desa") . " " . $data_master->nama_wilayah . ", Kecamatan " . ucwords(strtolower($data_kecamatan->nama_wilayah)) . ", Kabupaten " . ucwords(strtolower($data_kabupaten->nama_wilayah)) . ", Provinisi " . ucwords(strtolower($data_provinsi->nama_wilayah));
        }

        $data['content']->jenis_kelamin = ($data['content']->jenis_kelamin == "L" ? "Laki-Laki" : "Perempuan");
        $data['content']->pekerjaan_inti = ($data['content']->pekerjaan_inti == "Lainnya" ? $data['content']->pekerjaan_inti_lainnya : $data['content']->pekerjaan_inti);
        $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'peserta', 'content' => 'Peserta', 'is_active' => false], ['link' => false, 'content' => 'Detail Peserta', 'is_active' => true]];

        if ($this->session->userdata("level_user_id") == "6") {
            $this->execute('detail_peserta_dinkes', $data);
        } else if ($this->session->userdata("level_user_id") == "2") {
            $this->execute('detail_peserta_faskes', $data);
        } else if ($this->session->userdata("level_user_id") == "7") {
            if ($this->session->userdata("id_user") != $this->config->item('id_user_citra_husada')) {
                $this->execute('detail_peserta_non_puskesmas', $data);
            } else {
                $this->execute('detail_peserta_non_puskesmas_ch', $data);
            }
        } else if ($this->session->userdata("level_user_id") == "4") {
            $this->execute('detail_peserta_rs', $data);
        } else if ($this->session->userdata("level_user_id") == "9") {
            $this->execute('detail_peserta_rs_dokter', $data);
        } else if ($this->session->userdata("level_user_id") == "3") {
            $this->execute('detail_peserta_labkesda', $data);
        } else {
            $this->execute('detail_peserta', $data);
        }
    }

    public function detail_peserta_dokter($id_peserta, $peserta_dari)
    {
        if ($peserta_dari == "ch") {
            $data['id_peserta_non_puskesmas'] = $id_peserta;

            $data['content'] = $this->peserta_non_puskesmas_model->get(
                array(
                    "fields" => "peserta_non_puskesmas.*,IFNULL(user.nama_lengkap,'') as nama_user",
                    "left_join" => array(
                        "user" => "id_user=id_user_created"
                    ),
                    "where" => array(
                        "id_peserta_non_puskesmas" => decrypt_data($id_peserta)
                    )
                ),
                "row"
            );
        } else {
            $data['id_peserta'] = $id_peserta;

            $data['content'] = $this->peserta_model->get(
                array(
                    "fields" => "peserta.*,IFNULL(user.nama_lengkap,'') as nama_user",
                    "left_join" => array(
                        "user" => "id_user=id_user_created"
                    ),
                    "where" => array(
                        "id_peserta" => decrypt_data($id_peserta)
                    )
                ),
                "row"
            );
        }


        $data_master = $this->master_wilayah_model->get_by($data['content']->kelurahan_master_wilayah_id);

        $data_kecamatan = $this->master_wilayah_model->get(
            array(
                "fields" => "nama_wilayah,kode_induk",
                "where" => array(
                    "kode_wilayah" => $data_master->kode_induk
                )
            ),
            "row"
        );

        $data_kabupaten = $this->master_wilayah_model->get(
            array(
                "fields" => "nama_wilayah,kode_induk",
                "where" => array(
                    "kode_wilayah" => $data_kecamatan->kode_induk
                )
            ),
            "row"
        );

        $data_provinsi = $this->master_wilayah_model->get(
            array(
                "fields" => "nama_wilayah,kode_induk",
                "where" => array(
                    "kode_wilayah" => $data_kabupaten->kode_induk
                )
            ),
            "row"
        );

        $data['content']->alamat_ktp = $data['content']->alamat_ktp . (!empty($data['content']->rt_ktp) ? " RT. " . $data['content']->rt_ktp : "");

        if ($data['content']->kelurahan_master_wilayah_id == "109") {
            $data['content']->alamat_domisili = "Wilayah Kobar Lainnya";
        } else {
            $data['content']->alamat_domisili = $data['content']->alamat_domisili . (!empty($data['content']->rt_domisili) ? " RT. " . $data['content']->rt_domisili : "") . ($data_master->klasifikasi == "KEL" ? " Kelurahan" : "Desa") . " " . $data_master->nama_wilayah . ", Kecamatan " . ucwords(strtolower($data_kecamatan->nama_wilayah)) . ", Kabupaten " . ucwords(strtolower($data_kabupaten->nama_wilayah)) . ", Provinisi " . ucwords(strtolower($data_provinsi->nama_wilayah));
        }

        $data['content']->jenis_kelamin = ($data['content']->jenis_kelamin == "L" ? "Laki-Laki" : "Perempuan");
        $data['content']->pekerjaan_inti = ($data['content']->pekerjaan_inti == "Lainnya" ? $data['content']->pekerjaan_inti_lainnya : $data['content']->pekerjaan_inti);
        $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'peserta', 'content' => 'Peserta', 'is_active' => false], ['link' => false, 'content' => 'Detail Peserta', 'is_active' => true]];

        $this->execute('detail_peserta_rs_dokter', $data);
    }

    public function action_form_pemeriksaan()
    {
        $id_trx_pemeriksaan = decrypt_data($this->ipost("id_trx_pemeriksaan"));
        $id_peserta = decrypt_data($this->ipost("id_peserta"));
        $jenis_pemeriksaan = decrypt_data($this->ipost("jenis_pemeriksaan"));
        $hasil_pemeriksaan = decrypt_data($this->ipost("hasil_pemeriksaan"));
        $usulan_faskes = $this->ipost("usulan_faskes");

        if ($id_trx_pemeriksaan) {
            if ($hasil_pemeriksaan) {
                $data_hasil_pemeriksaan = $this->hasil_pemeriksaan_model->get_by($hasil_pemeriksaan);
                if ($data_hasil_pemeriksaan->class_badge == "2") {
                    $usulan_faskes = $this->ipost("usulan_faskes");
                } else {
                    $usulan_faskes = NULL;
                }
            } else {
                $usulan_faskes = $this->ipost("usulan_faskes");
            }
            //update
            $data_trx_pemeriksaan = array(
                "peserta_id" => $id_peserta,
                "jenis_pemeriksaan_id" => ($jenis_pemeriksaan ? $jenis_pemeriksaan : NULL),
                "hasil_pemeriksaan_id" => ($hasil_pemeriksaan ? $hasil_pemeriksaan : NULL),
                "usulan_faskes" => $usulan_faskes,
                'updated_at' => $this->datetime(),
                'id_user_updated' => $this->session->userdata("id_user")
            );

            $status = $this->trx_pemeriksaan_model->edit($id_trx_pemeriksaan, $data_trx_pemeriksaan);
        } else {
            //insert
            if ($hasil_pemeriksaan) {
                $data_hasil_pemeriksaan = $this->hasil_pemeriksaan_model->get_by($hasil_pemeriksaan);
                if ($data_hasil_pemeriksaan->class_badge == "2") {
                    $usulan_faskes = $this->ipost("usulan_faskes");
                } else {
                    $usulan_faskes = NULL;
                }
            } else {
                $usulan_faskes = $this->ipost("usulan_faskes");
            }
            //update
            $data_trx_pemeriksaan = array(
                "peserta_id" => $id_peserta,
                "jenis_pemeriksaan_id" => ($jenis_pemeriksaan ? $jenis_pemeriksaan : NULL),
                "hasil_pemeriksaan_id" => ($hasil_pemeriksaan ? $hasil_pemeriksaan : NULL),
                "tracking_pemeriksaan" => "1",
                "usulan_faskes" => $usulan_faskes,
                'created_at' => $this->datetime(),
                'id_user_created' => $this->session->userdata("id_user")
            );

            $status = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function action_form_pemeriksaan_non_puskesmas()
    {
        $id_trx_pemeriksaan_non_puskesmas = decrypt_data($this->ipost("id_trx_pemeriksaan_non_puskesmas"));
        $id_peserta_non_puskesmas = decrypt_data($this->ipost("id_peserta_non_puskesmas"));
        $jenis_pemeriksaan = decrypt_data($this->ipost("jenis_pemeriksaan"));
        $hasil_pemeriksaan = decrypt_data($this->ipost("hasil_pemeriksaan"));
        $kode_sample = $this->ipost("kode_sample");
        $jenis_spesimen = $this->ipost("jenis_spesimen");

        if ($id_trx_pemeriksaan_non_puskesmas) {
            //update
            $data_trx_pemeriksaan_non_puskesmas = array(
                "jenis_pemeriksaan_id" => ($jenis_pemeriksaan ? $jenis_pemeriksaan : NULL),
                "hasil_pemeriksaan_id" => ($hasil_pemeriksaan ? $hasil_pemeriksaan : NULL),
                "kode_sample" => $kode_sample,
                "jenis_spesimen" => $jenis_spesimen,
                "usulan_faskes" => NULL,
                'updated_at' => $this->datetime(),
                'id_user_updated' => $this->session->userdata("id_user")
            );

            $status = $this->trx_pemeriksaan_non_puskesmas_model->edit($id_trx_pemeriksaan_non_puskesmas, $data_trx_pemeriksaan_non_puskesmas);
        } else {
            //update
            $data_trx_pemeriksaan_non_puskesmas = array(
                "peserta_non_puskesmas_id" => $id_peserta_non_puskesmas,
                "jenis_pemeriksaan_id" => ($jenis_pemeriksaan ? $jenis_pemeriksaan : NULL),
                "hasil_pemeriksaan_id" => ($hasil_pemeriksaan ? $hasil_pemeriksaan : NULL),
                "tracking_pemeriksaan" => "1",
                "kode_sample" => $kode_sample,
                "jenis_spesimen" => $jenis_spesimen,
                "usulan_faskes" => NULL,
                'created_at' => $this->datetime(),
                'id_user_created' => $this->session->userdata("id_user")
            );

            $status = $this->trx_pemeriksaan_non_puskesmas_model->save($data_trx_pemeriksaan_non_puskesmas);

            //cek peserta id
            $data_peserta_non_puskesmas = $this->peserta_non_puskesmas_model->get_by($id_peserta_non_puskesmas);


            if ($data_peserta_non_puskesmas->peserta_id) {
                $data_peserta = $this->peserta_model->get_by($data_peserta_non_puskesmas->peserta_id);

                $data_trx_pemeriksaan = array(
                    "peserta_id" => $data_peserta_non_puskesmas->peserta_id,
                    "jenis_pemeriksaan_id" => ($jenis_pemeriksaan ? $jenis_pemeriksaan : NULL),
                    "hasil_pemeriksaan_id" => ($hasil_pemeriksaan ? $hasil_pemeriksaan : NULL),
                    "tracking_pemeriksaan" => "1",
                    "from_non_puskes" => "1",
                    "id_user_non_puskes" => $this->session->userdata("id_user"),
                    "kode_sample" => $kode_sample,
                    "jenis_spesimen" => $jenis_spesimen,
                    "usulan_faskes" => NULL,
                    'created_at' => $this->datetime(),
                    'id_user_created' => $data_peserta->id_user_created
                );

                $id_trx_pemeriksaan = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);

                $this->trx_pemeriksaan_non_puskesmas_model->edit($status, array("trx_pemeriksaan_id" => $id_trx_pemeriksaan));
            }
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function action_form_pemeriksaan_except_faskes()
    {
        $id_trx_pemeriksaan = decrypt_data($this->ipost("id_trx_pemeriksaan"));
        $id_peserta = decrypt_data($this->ipost("id_peserta"));
        $jenis_pemeriksaan = decrypt_data($this->ipost("jenis_pemeriksaan"));
        $hasil_pemeriksaan = decrypt_data($this->ipost("hasil_pemeriksaan"));
        $kode_sample = $this->ipost("kode_sample");
        $jenis_spesimen = $this->ipost("jenis_spesimen");

        if ($id_trx_pemeriksaan) {

            //update
            $data_trx_pemeriksaan = array(
                "peserta_id" => $id_peserta,
                "jenis_pemeriksaan_id" => ($jenis_pemeriksaan ? $jenis_pemeriksaan : NULL),
                "hasil_pemeriksaan_id" => ($hasil_pemeriksaan ? $hasil_pemeriksaan : NULL),
                "kode_sample" => $kode_sample,
                "jenis_spesimen" => $jenis_spesimen,
                'updated_at' => $this->datetime(),
                'id_user_updated' => $this->session->userdata("id_user")
            );

            $status = $this->trx_pemeriksaan_model->edit($id_trx_pemeriksaan, $data_trx_pemeriksaan);

            if (in_array($this->session->userdata("level_user_id"), array("9", "6")) && $jenis_pemeriksaan == "1" && $hasil_pemeriksaan == "1") {
                $cek_trx_terkonfirmasi_positif = $this->trx_terkonfirmasi_model->get(
                    array(
                        "where" => array(
                            "peserta_id" => $id_peserta
                        ),
                        "order_by" => array(
                            "id_trx_terkonfirmasi" => "DESC"
                        ),
                        "limit" => "1"
                    ),
                    "row"
                );

                if ($cek_trx_terkonfirmasi_positif) {
                    if ($cek_trx_terkonfirmasi_positif->status != "1") {
                        $data_trx_terkonfirmasi = array(
                            "peserta_id" => $id_peserta,
                            "tanggal_terkonfirmasi" => $this->datetime(),
                            "status" => "1",
                            "trx_pemeriksaan_id" => $id_trx_pemeriksaan,
                            'created_at' => $this->datetime(),
                            'id_user_created' => $this->session->userdata("id_user")
                        );

                        $status = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi);
                    }
                } else {
                    $data_trx_terkonfirmasi = array(
                        "peserta_id" => $id_peserta,
                        "tanggal_terkonfirmasi" => $this->datetime(),
                        "status" => "1",
                        "trx_pemeriksaan_id" => $id_trx_pemeriksaan,
                        'created_at' => $this->datetime(),
                        'id_user_created' => $this->session->userdata("id_user")
                    );

                    $status = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi);
                }
            } else {
                $data = array(
                    "deleted_at" => $this->datetime(),
                    "id_user_deleted" => $this->session->userdata("id_user")
                );

                $status = $this->trx_terkonfirmasi_model->edit_by(array("trx_pemeriksaan_id" => $id_trx_pemeriksaan, "peserta_id" => $id_peserta), $data);

                $status = $this->trx_terkonfirmasi_rilis_model->edit_by(array("trx_pemeriksaan_id" => $id_trx_pemeriksaan, "peserta_id" => $id_peserta), $data);
            }
        } else {
            //insert
            $data_trx_pemeriksaan = array(
                "peserta_id" => $id_peserta,
                "jenis_pemeriksaan_id" => $jenis_pemeriksaan,
                "hasil_pemeriksaan_id" => ($hasil_pemeriksaan ? $hasil_pemeriksaan : NULL),
                "tracking_pemeriksaan" => "1",
                "kode_sample" => $kode_sample,
                "jenis_spesimen" => $jenis_spesimen,
                'created_at' => $this->datetime(),
                'id_user_created' => $this->session->userdata("id_user")
            );

            $status = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);

            if (in_array($this->session->userdata("level_user_id"), array("4", "6")) && $jenis_pemeriksaan == "1" && $hasil_pemeriksaan == "1") {
                $cek_trx_terkonfirmasi_positif = $this->trx_terkonfirmasi_model->get(
                    array(
                        "where" => array(
                            "peserta_id" => $id_peserta
                        ),
                        "order_by" => array(
                            "id_trx_terkonfirmasi" => "DESC"
                        ),
                        "limit" => "1"
                    ),
                    "row"
                );

                if ($cek_trx_terkonfirmasi_positif) {
                    if ($cek_trx_terkonfirmasi_positif->status != "1") {
                        $data_trx_terkonfirmasi = array(
                            "peserta_id" => $id_peserta,
                            "tanggal_terkonfirmasi" => $this->datetime(),
                            "status" => "1",
                            "trx_pemeriksaan_id" => $status,
                            'created_at' => $this->datetime(),
                            'id_user_created' => $this->session->userdata("id_user")
                        );

                        $status = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi);
                    }
                } else {
                    $data_trx_terkonfirmasi = array(
                        "peserta_id" => $id_peserta,
                        "tanggal_terkonfirmasi" => $this->datetime(),
                        "status" => "1",
                        "trx_pemeriksaan_id" => $status,
                        'created_at' => $this->datetime(),
                        'id_user_created' => $this->session->userdata("id_user")
                    );

                    $status = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi);
                }
            }
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function change_hasil_pemeriksaan_pcr()
    {
        $id_trx_pemeriksaan_last = decrypt_data($this->ipost("id_trx_pemeriksaan_last"));
        $val_status_pcr = $this->ipost("val_status_pcr");
        $id_peserta = decrypt_data($this->ipost("id_peserta"));
        $data_from = $this->ipost("data_from");

        //update
        $data_trx_pemeriksaan = array(
            "hasil_pemeriksaan_id" => $val_status_pcr,
            "id_user_dokter" => ($val_status_pcr ? $this->session->userdata("id_user") : NULL),
            'updated_at' => $this->datetime(),
            'id_user_updated' => $this->session->userdata("id_user")
        );

        if ($data_from == "rsud") {
            $status = $this->trx_pemeriksaan_model->edit($id_trx_pemeriksaan_last, $data_trx_pemeriksaan);

            $cek_trx_terkonfirmasi_positif = $this->trx_terkonfirmasi_model->get(
                array(
                    "where" => array(
                        "peserta_id" => $id_peserta
                    ),
                    "order_by" => array(
                        "id_trx_terkonfirmasi" => "DESC"
                    ),
                    "limit" => "1"
                ),
                "row"
            );

            if (in_array($this->session->userdata("level_user_id"), array("6", "9")) && $val_status_pcr == "1") {
                $cek_trx_terkonfirmasi_positif = $this->trx_terkonfirmasi_model->get(
                    array(
                        "where" => array(
                            "peserta_id" => $id_peserta
                        ),
                        "order_by" => array(
                            "id_trx_terkonfirmasi" => "DESC"
                        ),
                        "limit" => "1"
                    ),
                    "row"
                );

                if ($cek_trx_terkonfirmasi_positif) {
                    if ($cek_trx_terkonfirmasi_positif->status != "1") {
                        $data_trx_terkonfirmasi = array(
                            "peserta_id" => $id_peserta,
                            "tanggal_terkonfirmasi" => $this->datetime(),
                            "status" => "1",
                            "trx_pemeriksaan_id" => $id_trx_pemeriksaan_last,
                            'created_at' => $this->datetime(),
                            'id_user_created' => $this->session->userdata("id_user")
                        );

                        $status = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi);
                    }
                } else {
                    $data_trx_terkonfirmasi = array(
                        "peserta_id" => $id_peserta,
                        "tanggal_terkonfirmasi" => $this->datetime(),
                        "status" => "1",
                        "trx_pemeriksaan_id" => $id_trx_pemeriksaan_last,
                        'created_at' => $this->datetime(),
                        'id_user_created' => $this->session->userdata("id_user")
                    );

                    $status = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi);
                }
            } else if (in_array($this->session->userdata("level_user_id"), array("6", "9")) && $val_status_pcr != "1") {
                $data = array(
                    "deleted_at" => $this->datetime(),
                    "id_user_deleted" => $this->session->userdata("id_user")
                );

                $this->trx_terkonfirmasi_model->edit_by(array("trx_pemeriksaan_id" => $id_trx_pemeriksaan_last, "peserta_id" => $id_peserta), $data);

                $this->trx_terkonfirmasi_rilis_model->edit_by(array("trx_pemeriksaan_id" => $id_trx_pemeriksaan_last, "peserta_id" => $id_peserta), $data);
            }
        } else {
            $status = $this->trx_pemeriksaan_non_puskesmas_model->edit($id_trx_pemeriksaan_last, $data_trx_pemeriksaan);
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function change_status_pemeriksaan_pcr()
    {
        $id_trx_pemeriksaan = decrypt_data($this->ipost("id_trx_pemeriksaan"));
        $id_peserta = decrypt_data($this->ipost("id_peserta"));
        $peserta_dari = $this->ipost("peserta_dari");
        $nomor_rekam_medis = $this->ipost("nomor_rekam_medis");
        $status_pemeriksaan = $this->ipost("status_pemeriksaan");

        // $data_peserta_rekam_medis = array(
        //     "no_rekam_medis" => $nomor_rekam_medis
        // );

        //update
        if ($status_pemeriksaan == "2") {
            $data_trx_pemeriksaan = array(
                "tracking_pemeriksaan"  => $status_pemeriksaan,
                "tanggal_terima_sampel" => $this->datetime(),
                "id_user_lab"           => $this->session->userdata("id_user"),
                'updated_at'            => $this->datetime(),
                'id_user_updated'       => $this->session->userdata("id_user")
            );
        } else {
            if ($status_pemeriksaan == "1") {
                $data_trx_pemeriksaan = array(
                    "tracking_pemeriksaan"  => $status_pemeriksaan,
                    "tanggal_terima_sampel" => NULL,
                    "id_user_lab"           => NULL,
                    'updated_at'            => $this->datetime(),
                    'id_user_updated'       => $this->session->userdata("id_user")
                );
            } else {
                $data_trx_pemeriksaan = array(
                    "tracking_pemeriksaan" => $status_pemeriksaan,
                    'updated_at'           => $this->datetime(),
                    'id_user_updated'      => $this->session->userdata("id_user")
                );
            }
        }

        if ($peserta_dari == "rsud") {
            // $this->peserta_model->edit($id_peserta, $data_peserta_rekam_medis);
            $status = $this->trx_pemeriksaan_model->edit($id_trx_pemeriksaan, $data_trx_pemeriksaan);
        } else {
            // $this->peserta_non_puskesmas_model->edit($id_peserta, $data_peserta_rekam_medis);
            $status = $this->trx_pemeriksaan_non_puskesmas_model->edit($id_trx_pemeriksaan, $data_trx_pemeriksaan);
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function change_hasil_pemeriksaan_pcr_citra_husada()
    {
        $id_trx_pemeriksaan_non_puskesmas_last = decrypt_data($this->ipost("id_trx_pemeriksaan_non_puskesmas_last"));
        $val_status_pcr = $this->ipost("val_status_pcr");

        //update
        $data_trx_pemeriksaan = array(
            "hasil_pemeriksaan_id" => ($val_status_pcr ? $val_status_pcr : NULL),
            'updated_at' => $this->datetime(),
            'id_user_updated' => $this->session->userdata("id_user")
        );

        $status = $this->trx_pemeriksaan_non_puskesmas_model->edit($id_trx_pemeriksaan_non_puskesmas_last, $data_trx_pemeriksaan);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function change_tracking_pemeriksaan_citra_husada()
    {
        $id_trx_pemeriksaan_non_puskesmas_last = decrypt_data($this->ipost("id_trx_pemeriksaan_non_puskesmas_last"));
        $val_status_pcr = $this->ipost("val_status_pcr");

        //update
        $data_trx_pemeriksaan = array(
            "tracking_pemeriksaan" => ($val_status_pcr ? $val_status_pcr : NULL),
            'updated_at' => $this->datetime(),
            'id_user_updated' => $this->session->userdata("id_user")
        );

        $status = $this->trx_pemeriksaan_non_puskesmas_model->edit($id_trx_pemeriksaan_non_puskesmas_last, $data_trx_pemeriksaan);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function delete_pemeriksaan()
    {
        $id_pemeriksaan = $this->iget('id_pemeriksaan');
        $data_master = $this->trx_pemeriksaan_model->get_by(decrypt_data($id_pemeriksaan));

        if (!$data_master) {
            $this->page_error();
        }

        //cek jumlah transaksi
        $cek_trx = $this->trx_pemeriksaan_model->get(
            array(
                "fields" => "IFNULL(count(*),0) AS jumlah_trx",
                "where" => array(
                    "peserta_id" => $data_master->peserta_id
                )
            ),
            "row"
        );

        if ($cek_trx->jumlah_trx > 1) {
            //cek non puskesmas connection
            $cek_trx_non_puskesmas = $this->trx_pemeriksaan_non_puskesmas_model->get(
                array(
                    "where" => array(
                        "trx_pemeriksaan_id" => decrypt_data($id_pemeriksaan)
                    )
                ),
                "row"
            );

            if ($cek_trx_non_puskesmas) {
                $this->trx_pemeriksaan_non_puskesmas_model->edit($cek_trx_non_puskesmas->id_trx_pemeriksaan_non_puskesmas, array("trx_pemeriksaan_id" => NULL));
            }

            //cek trx terkonfirmasi rilis
            $cek_trx_terkonfirmasi_rilis = $this->trx_terkonfirmasi_rilis_model->get(
                array(
                    "where" => array(
                        "peserta_id" => $data_master->peserta_id,
                        "trx_pemeriksaan_id" => decrypt_data($id_pemeriksaan)
                    )
                ),
                "row"
            );

            if ($cek_trx_terkonfirmasi_rilis) {
                $this->trx_terkonfirmasi_rilis_model->edit(
                    $cek_trx_terkonfirmasi_rilis->id_trx_terkonfirmasi_rilis,
                    array(
                        "deleted_at" => $this->datetime(),
                        "id_user_deleted" => $this->session->userdata("id_user")
                    )
                );
            }

            //cek trx terkonfirmasi
            $cek_trx_terkonfirmasi = $this->trx_terkonfirmasi_model->get(
                array(
                    "where" => array(
                        "peserta_id" => $data_master->peserta_id,
                        "trx_pemeriksaan_id" => decrypt_data($id_pemeriksaan)
                    )
                ),
                "row"
            );

            if ($cek_trx_terkonfirmasi) {
                $this->trx_terkonfirmasi_model->edit(
                    $cek_trx_terkonfirmasi->id_trx_terkonfirmasi,
                    array(
                        "deleted_at" => $this->datetime(),
                        "id_user_deleted" => $this->session->userdata("id_user")
                    )
                );
            }

            $data = array(
                "deleted_at" => $this->datetime(),
                "id_user_deleted" => $this->session->userdata("id_user")
            );

            $status = $this->trx_pemeriksaan_model->edit(decrypt_data($id_pemeriksaan), $data);
        } else {
            $status = false;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function delete_pemeriksaan_non_puskesmas()
    {
        $id_pemeriksaan_non_puskesmas = $this->iget('id_pemeriksaan_non_puskesmas');
        $data_master = $this->trx_pemeriksaan_non_puskesmas_model->get_by(decrypt_data($id_pemeriksaan_non_puskesmas));

        if (!$data_master) {
            $this->page_error();
        }

        $data = array(
            "deleted_at" => $this->datetime(),
            "id_user_deleted" => $this->session->userdata("id_user")
        );

        $status = $this->trx_pemeriksaan_non_puskesmas_model->edit(decrypt_data($id_pemeriksaan_non_puskesmas), $data);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function action_form_nik()
    {
        $id_peserta = decrypt_data($this->ipost("id_peserta"));
        $nik = $this->ipost("nik");

        $data_master = $this->peserta_model->get_by($id_peserta);

        if (!$data_master) {
            $this->page_error();
        }

        $data = array(
            "nik" => $nik
        );

        $status = $this->peserta_model->edit($id_peserta, $data);

        $by_pass = false;
        if ($status) {
            $by_pass = true;
        } else {
            $by_pass = false;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($by_pass));
    }

    public function detail_peserta_non_puskesmas()
    {

        $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'peserta', 'content' => 'Peserta', 'is_active' => false], ['link' => false, 'content' => 'List Peserta Non Puskesmas', 'is_active' => true]];
        $this->execute('list_peserta_non_puskesmas', $data);
    }

    public function detail_peserta_non_puskesmas_ch()
    {

        $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'peserta', 'content' => 'Peserta', 'is_active' => false], ['link' => false, 'content' => 'List Peserta Citra Husada', 'is_active' => true]];
        $this->execute('list_peserta_non_puskesmas_ch', $data);
    }

    public function copy_data()
    {
        $id_peserta_non_puskesmas = decrypt_data($this->ipost("id_peserta_non_puskesmas"));
        $id_peserta = decrypt_data($this->ipost("id_peserta"));

        $peserta_non_puskesmas = $this->peserta_non_puskesmas_model->get_by($id_peserta_non_puskesmas);

        $data_peserta_non_puskesmas = array(
            "peserta_id" => $id_peserta
        );

        $this->peserta_non_puskesmas_model->edit($id_peserta_non_puskesmas, $data_peserta_non_puskesmas);

        $data_trx_pemeriksaan_non_puskesmas = $this->trx_pemeriksaan_non_puskesmas_model->query("
            SELECT *
            FROM peserta_non_puskesmas
            INNER JOIN 
            (
                SELECT trx_pemeriksaan_non_puskesmas.*
                FROM trx_pemeriksaan_non_puskesmas 
                LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                LEFT JOIN user ON id_user=id_user_created 
                WHERE id_trx_pemeriksaan_non_puskesmas IN (
                SELECT MAX(id_trx_pemeriksaan_non_puskesmas) AS id_trx_pemeriksaan_non_puskesmas 
                FROM trx_pemeriksaan_non_puskesmas 
                WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                GROUP BY peserta_non_puskesmas_id
                ORDER BY id_trx_pemeriksaan_non_puskesmas DESC
                ) AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
            ) AS c ON c.peserta_non_puskesmas_id=id_peserta_non_puskesmas
            WHERE peserta_non_puskesmas.deleted_at IS NULL AND id_peserta_non_puskesmas = '" . $id_peserta_non_puskesmas . "'
        ")->row();

        $status = false;
        if (!$data_trx_pemeriksaan_non_puskesmas->trx_pemeriksaan_id) {
            $data_trx_pemeriksaan = array(
                "peserta_id" => $id_peserta,
                "jenis_pemeriksaan_id" => ($data_trx_pemeriksaan_non_puskesmas->jenis_pemeriksaan_id ? $data_trx_pemeriksaan_non_puskesmas->jenis_pemeriksaan_id : NULL),
                "hasil_pemeriksaan_id" => ($data_trx_pemeriksaan_non_puskesmas->hasil_pemeriksaan_id ? $data_trx_pemeriksaan_non_puskesmas->hasil_pemeriksaan_id : NULL),
                "usulan_faskes" => ($data_trx_pemeriksaan_non_puskesmas->usulan_faskes ? $data_trx_pemeriksaan_non_puskesmas->usulan_faskes : NULL),
                "tracking_pemeriksaan" => ($data_trx_pemeriksaan_non_puskesmas->tracking_pemeriksaan ? $data_trx_pemeriksaan_non_puskesmas->tracking_pemeriksaan : NULL),
                "jenis_spesimen" => ($data_trx_pemeriksaan_non_puskesmas->jenis_spesimen ? $data_trx_pemeriksaan_non_puskesmas->jenis_spesimen : NULL),
                "tanggal_terima_sampel" => ($data_trx_pemeriksaan_non_puskesmas->tanggal_terima_sampel ? $data_trx_pemeriksaan_non_puskesmas->tanggal_terima_sampel : NULL),
                "id_user_lab" => ($data_trx_pemeriksaan_non_puskesmas->id_user_lab ? $data_trx_pemeriksaan_non_puskesmas->id_user_lab : NULL),
                "id_user_dokter" => ($data_trx_pemeriksaan_non_puskesmas->id_user_dokter ? $data_trx_pemeriksaan_non_puskesmas->id_user_dokter : NULL),
                "from_non_puskes" => "1",
                "kode_sample" => ($data_trx_pemeriksaan_non_puskesmas->kode_sample ? $data_trx_pemeriksaan_non_puskesmas->kode_sample : NULL),
                "id_user_non_puskes" => $data_trx_pemeriksaan_non_puskesmas->id_user_created,
                'created_at' => $this->datetime(),
                'id_user_created' => $this->session->userdata("id_user")
            );

            $status = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);

            if ($status) {
                $data_update_trx_pemeriksaan_non_puskesmas = array(
                    "trx_pemeriksaan_id" => $status
                );

                $status = $this->trx_pemeriksaan_non_puskesmas_model->edit($data_trx_pemeriksaan_non_puskesmas->id_trx_pemeriksaan_non_puskesmas, $data_update_trx_pemeriksaan_non_puskesmas);
            }
        }

        $by_pass = false;
        if ($status) {
            $by_pass = true;
        } else {
            $by_pass = false;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($by_pass));
    }

    public function clone_data_non_puskesmas()
    {
        $id_peserta = decrypt_data($this->iget("id_peserta"));

        $peserta_non_puskesmas = $this->peserta_non_puskesmas_model->get_by($id_peserta);

        $data = array(
            "nama" => $peserta_non_puskesmas->nama,
            "nik" => $peserta_non_puskesmas->nik,
            "tanggal_lahir" => $peserta_non_puskesmas->tanggal_lahir,
            "jenis_kelamin" => $peserta_non_puskesmas->jenis_kelamin,
            "pekerjaan_inti" => $peserta_non_puskesmas->pekerjaan_inti,
            "pekerjaan_inti_lainnya" => $peserta_non_puskesmas->pekerjaan_inti_lainnya,
            "alamat_ktp" => $peserta_non_puskesmas->alamat_ktp,
            "rt_ktp" => $peserta_non_puskesmas->rt_ktp,
            "is_copy_ktp" => $peserta_non_puskesmas->is_copy_ktp,
            "alamat_domisili" => $peserta_non_puskesmas->alamat_domisili,
            "rt_domisili" => $peserta_non_puskesmas->rt_domisili,
            "kelurahan_master_wilayah_id" => $peserta_non_puskesmas->kelurahan_master_wilayah_id,
            "nomor_telepon" => $peserta_non_puskesmas->nomor_telepon,
            "no_rekam_medis" => $peserta_non_puskesmas->no_rekam_medis,
            "status_rawat" => $peserta_non_puskesmas->status_rawat,
            'created_at' => $peserta_non_puskesmas->created_at,
            'id_user_created' => ($peserta_non_puskesmas->id_user_created == $this->config->item('id_user_citra_husada') ? $this->config->item('id_user_rsud_fo') : $this->session->userdata("id_user"))
        );

        $status_peserta = $this->peserta_model->save($data);

        $data_peserta_non_puskesmas = array(
            "peserta_id" => $status_peserta
        );

        $this->peserta_non_puskesmas_model->edit($id_peserta, $data_peserta_non_puskesmas);

        $data_trx_pemeriksaan_non_puskesmas = $this->trx_pemeriksaan_non_puskesmas_model->query("
            SELECT *
            FROM peserta_non_puskesmas
            INNER JOIN 
            (
                SELECT trx_pemeriksaan_non_puskesmas.*
                FROM trx_pemeriksaan_non_puskesmas 
                LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                LEFT JOIN user ON id_user=id_user_created 
                WHERE id_trx_pemeriksaan_non_puskesmas IN (
                SELECT MAX(id_trx_pemeriksaan_non_puskesmas) AS id_trx_pemeriksaan_non_puskesmas 
                FROM trx_pemeriksaan_non_puskesmas 
                WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                GROUP BY peserta_non_puskesmas_id
                ORDER BY id_trx_pemeriksaan_non_puskesmas DESC
                ) AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
            ) AS c ON c.peserta_non_puskesmas_id=id_peserta_non_puskesmas
            WHERE peserta_non_puskesmas.deleted_at IS NULL AND id_peserta_non_puskesmas = '" . $id_peserta . "'
        ")->result();

        if ($data_trx_pemeriksaan_non_puskesmas) {
            foreach ($data_trx_pemeriksaan_non_puskesmas as $key => $row) {
                //status_rawat
                $data_status_rawat = $this->status_rawat_non_puskesmas_model->get(
                    array(
                        "where" => array(
                            "peserta_non_puskesmas_id" => $id_peserta,
                            "trx_terkonfirmasi_non_puskesmas_last_id" => $row->id_trx_pemeriksaan_non_puskesmas,
                        )
                    )
                );

                $data_trx_pemeriksaan = array(
                    "peserta_id"            => $status_peserta,
                    "jenis_pemeriksaan_id"  => $row->jenis_pemeriksaan_id,
                    "hasil_pemeriksaan_id"  => $row->hasil_pemeriksaan_id,
                    "usulan_faskes"         => $row->usulan_faskes,
                    "tracking_pemeriksaan"  => $row->tracking_pemeriksaan,
                    "jenis_spesimen"        => ($row->jenis_spesimen ? $row->jenis_spesimen : NULL),
                    "tanggal_terima_sampel" => ($row->tanggal_terima_sampel ? $row->tanggal_terima_sampel : NULL),
                    "status_rawat"          => ($row->status_rawat ? $row->status_rawat : NULL),
                    "id_user_lab"           => ($row->id_user_lab ? $row->id_user_lab : NULL),
                    "id_user_dokter"        => ($row->id_user_dokter ? $row->id_user_dokter : NULL),
                    "kode_sample"           => $row->kode_sample,
                    "from_non_puskes"       => "1",
                    "id_user_non_puskes"    => $row->id_user_created,
                    'created_at'            => $row->created_at,
                    'id_user_created'       => ($row->id_user_created == $this->config->item('id_user_citra_husada') ? $this->config->item('id_user_rsud_fo') : $this->session->userdata("id_user")),
                    'id_user_updated'       => ($row->id_user_created == $this->config->item('id_user_citra_husada') ? $this->config->item('id_user_rsud_fo') : NULL)
                );

                $status_pemeriksaan = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);

                if ($data_status_rawat) {
                    foreach ($data_status_rawat as $key_status_rawat => $row_status_rawat) {
                        $data_clone_status_rawat = array(
                            "peserta_id"                => $status_peserta,
                            "is_status"                 => $row_status_rawat->is_status,
                            "ruang_perawatan"           => $row_status_rawat->ruang_perawatan,
                            "trx_terkonfirmasi_last_id" => $status_pemeriksaan,
                            "created_at"                => $this->datetime()
                        );

                        $this->status_rawat_model->save($data_clone_status_rawat);
                    }
                }

                $data_update_trx_pemeriksaan_non_puskesmas = array(
                    "trx_pemeriksaan_id" => $status_pemeriksaan
                );

                $status = $this->trx_pemeriksaan_non_puskesmas_model->edit($row->id_trx_pemeriksaan_non_puskesmas, $data_update_trx_pemeriksaan_non_puskesmas);

                if ($row->jenis_pemeriksaan_id == '1' && $row->hasil_pemeriksaan_id == '1') {
                    $data_trx_terkonfirmasi = array(
                        "peserta_id"            => $status_peserta,
                        "tanggal_terkonfirmasi" => $row->created_at,
                        "status"                => "1",
                        "trx_pemeriksaan_id"    => $status_pemeriksaan,
                        'created_at'            => $row->created_at,
                        'id_user_created'       => $this->session->userdata("id_user")
                    );

                    $status = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi);
                }
            }
        }


        $by_pass = false;
        if ($status) {
            $by_pass = true;
        } else {
            $by_pass = false;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($by_pass));
    }

    public function status_sembuh()
    {
        $id_peserta = decrypt_data($this->ipost("id_peserta"));

        $data_trx_pemeriksaan = array(
            "peserta_id" => $id_peserta,
            "jenis_pemeriksaan_id" => NULL,
            "hasil_pemeriksaan_id" => NULL,
            "kode_sample" => NULL,
            "is_sembuh" => "1",
            'created_at' => $this->datetime(),
            'id_user_created' => $this->session->userdata("id_user")
        );

        $status = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);

        $data_trx_terkonfirmasi = array(
            "peserta_id" => $id_peserta,
            "tanggal_terkonfirmasi" => $this->datetime(),
            "status" => "2",
            "trx_pemeriksaan_id" => $status,
            'created_at' => $this->datetime(),
            'id_user_created' => $this->session->userdata("id_user")
        );

        $status = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi);

        $data_notify_polsek = array(
            "notify_sembuh_from_polsek" => NULL
        );

        $this->peserta_model->edit($id_peserta, $data_notify_polsek);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function status_bebas_isoma()
    {
        $id_peserta = decrypt_data($this->ipost("id_peserta"));

        $cek_trx_terkonfirmasi_positif = $this->trx_terkonfirmasi_model->get(
            array(
                "where" => array(
                    "peserta_id" => $id_peserta
                ),
                "order_by" => array(
                    "id_trx_terkonfirmasi" => "DESC"
                ),
                "limit" => "1"
            ),
            "row"
        );

        $status = false;
        if ($cek_trx_terkonfirmasi_positif) {
            if ($cek_trx_terkonfirmasi_positif->status == "1") {
                $data_trx_pemeriksaan = array(
                    "peserta_id" => $id_peserta,
                    "jenis_pemeriksaan_id" => NULL,
                    "hasil_pemeriksaan_id" => NULL,
                    "kode_sample" => NULL,
                    "is_sembuh" => "1",
                    "status_rawat" => "3",
                    'created_at' => $this->datetime(),
                    'id_user_created' => $this->session->userdata("id_user")
                );

                $status = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);

                $data_trx_terkonfirmasi = array(
                    "peserta_id" => $id_peserta,
                    "tanggal_terkonfirmasi" => $this->datetime(),
                    "status" => "2",
                    "trx_pemeriksaan_id" => $status,
                    'created_at' => $this->datetime(),
                    'id_user_created' => $this->session->userdata("id_user")
                );

                $status = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi);
            }
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function status_meninggal()
    {
        $id_peserta = decrypt_data($this->ipost("id_peserta"));

        $data_trx_pemeriksaan = array(
            "peserta_id" => $id_peserta,
            "jenis_pemeriksaan_id" => NULL,
            "hasil_pemeriksaan_id" => NULL,
            "kode_sample" => NULL,
            "is_meninggal" => "1",
            'created_at' => $this->datetime(),
            'id_user_created' => $this->session->userdata("id_user")
        );

        $status = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);

        $data_trx_terkonfirmasi = array(
            "peserta_id" => $id_peserta,
            "tanggal_terkonfirmasi" => $this->datetime(),
            "status" => "3",
            "trx_pemeriksaan_id" => $status,
            'created_at' => $this->datetime(),
            'id_user_created' => $this->session->userdata("id_user")
        );

        $status = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function status_probable()
    {
        $id_peserta = decrypt_data($this->ipost("id_peserta"));

        $data_trx_pemeriksaan = array(
            "peserta_id" => $id_peserta,
            "jenis_pemeriksaan_id" => NULL,
            "hasil_pemeriksaan_id" => NULL,
            "kode_sample" => NULL,
            "is_probable" => "1",
            'created_at' => $this->datetime(),
            'id_user_created' => $this->session->userdata("id_user")
        );

        $status_pemeriksaan = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);

        $data_trx_terkonfirmasi = array(
            "peserta_id" => $id_peserta,
            "tanggal_terkonfirmasi" => $this->datetime(),
            "status" => "4",
            "trx_pemeriksaan_id" => $status_pemeriksaan,
            'created_at' => $this->datetime(),
            'id_user_created' => $this->session->userdata("id_user")
        );

        $status = $this->trx_terkonfirmasi_model->save($data_trx_terkonfirmasi);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function status_bebas_isolasi_mandiri_suspek()
    {
        $id_peserta = decrypt_data($this->ipost("id_peserta"));

        $data_trx_pemeriksaan = array(
            "peserta_id" => $id_peserta,
            "jenis_pemeriksaan_id" => NULL,
            "hasil_pemeriksaan_id" => NULL,
            "kode_sample" => NULL,
            "usulan_faskes" => "2",
            'created_at' => $this->datetime(),
            'id_user_created' => $this->session->userdata("id_user")
        );

        $status = $this->trx_pemeriksaan_model->save($data_trx_pemeriksaan);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function cetak_peserta_swab()
    {
        $tanggal = $this->ipost("tanggal_cetak_peserta");
        $cetak_after_check = $this->ipost("cetak_after_check");

        if ($cetak_after_check == "2") {
            $data_peserta_swab = $this->peserta_model->query("
            SELECT peserta.*,master_puskesmas.`nama_puskesmas`,
            c.*,d.*,DATE_FORMAT(tanggal_lahir,'%d/%m/%Y') as tanggal_lahir
            FROM peserta
            LEFT JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
            LEFT JOIN master_puskesmas ON id_master_puskesmas=puskesmas_id
            LEFT JOIN 
                (
                SELECT id_trx_pemeriksaan AS id_trx_pemeriksaan_last,peserta_id,level_user_id AS level_user_id_last,class_badge AS class_badge_last,usulan_faskes AS usulan_faskes_pemeriksaan,trx_pemeriksaan.jenis_pemeriksaan_id,trx_pemeriksaan.hasil_pemeriksaan_id,trx_pemeriksaan.created_at,kode_sample
                FROM trx_pemeriksaan 
                LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                LEFT JOIN user ON id_user=id_user_created 
                WHERE id_trx_pemeriksaan IN (
                    SELECT MAX(id_trx_pemeriksaan) AS id_trx_pemeriksaan 
                    FROM trx_pemeriksaan 
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_pemeriksaan DESC
                    ) 
                    AND trx_pemeriksaan.deleted_at IS NULL
                ) AS c ON c.peserta_id=id_peserta
            LEFT JOIN 
                (
                SELECT COUNT(*) AS jumlah_swab,peserta_id
                FROM trx_pemeriksaan
                WHERE jenis_pemeriksaan_id = '1' AND DATE_FORMAT(created_at,'%d/%m/%Y') <= '" . $tanggal . "' AND trx_pemeriksaan.deleted_at IS NULL
                GROUP BY peserta_id
            ) AS d ON d.peserta_id=id_peserta
            WHERE peserta.deleted_at IS NULL AND (level_user_id_last = '3' AND c.jenis_pemeriksaan_id = '1' AND (c.hasil_pemeriksaan_id IS NULL OR c.hasil_pemeriksaan_id = '')) AND DATE_FORMAT(c.created_at,'%d/%m/%Y') = '" . $tanggal . "'
            GROUP BY id_peserta
            ORDER BY id_trx_pemeriksaan_last DESC
            ")->result();
        } else {

            $data_peserta_swab = $this->peserta_model->query("
            SELECT peserta.*,master_puskesmas.`nama_puskesmas`,
            c.*,d.*,DATE_FORMAT(tanggal_lahir,'%d/%m/%Y') as tanggal_lahir
            FROM peserta
            LEFT JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
            LEFT JOIN master_puskesmas ON id_master_puskesmas=puskesmas_id
            LEFT JOIN 
                (
                SELECT id_trx_pemeriksaan AS id_trx_pemeriksaan_last,peserta_id,level_user_id AS level_user_id_last,class_badge AS class_badge_last,usulan_faskes AS usulan_faskes_pemeriksaan,trx_pemeriksaan.jenis_pemeriksaan_id,trx_pemeriksaan.hasil_pemeriksaan_id,trx_pemeriksaan.created_at,kode_sample
                FROM trx_pemeriksaan 
                LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                LEFT JOIN user ON id_user=id_user_created 
                WHERE id_trx_pemeriksaan IN (
                    SELECT MAX(id_trx_pemeriksaan) AS id_trx_pemeriksaan 
                    FROM trx_pemeriksaan 
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_pemeriksaan DESC
                    ) 
                    AND trx_pemeriksaan.deleted_at IS NULL
                ) AS c ON c.peserta_id=id_peserta
            LEFT JOIN 
                (
                SELECT COUNT(*) AS jumlah_swab,peserta_id
                FROM trx_pemeriksaan
                WHERE jenis_pemeriksaan_id = '1' AND DATE_FORMAT(created_at,'%d/%m/%Y') <= '" . $tanggal . "' AND trx_pemeriksaan.deleted_at IS NULL
                GROUP BY peserta_id
            ) AS d ON d.peserta_id=id_peserta
            WHERE peserta.deleted_at IS NULL AND (level_user_id_last = '2' AND c.usulan_faskes_pemeriksaan = '0') AND DATE_FORMAT(c.created_at,'%d/%m/%Y') = '" . $tanggal . "'
            GROUP BY id_peserta
            ORDER BY id_trx_pemeriksaan_last DESC
            ")->result();
        }

        require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel.php');
        require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

        $exp_tanggal = explode("/", $tanggal);

        $filename = "Manifest " . date_indo($exp_tanggal[2] . "-" . $exp_tanggal[1] . "-" . $exp_tanggal[0]) . ".xlsx";

        $style_header_table = array(
            'font' => array(
                'size' => 11,
                'bold' => true
            ), 'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        $style_header = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true
            ), 'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'dae1f3')
            )
        );

        $style_data = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $style_data_kiri = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
            )
        );

        $style_data_tengah = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "DATA PENGAMBILAN SWAB DINAS KESEHATAN");
        $objPHPExcel->getActiveSheet()->SetCellValue("A2", "KABUPATEN KOTAWARINGIN BARAT");
        $objPHPExcel->getActiveSheet()->mergeCells("A1:M1");
        $objPHPExcel->getActiveSheet()->mergeCells("A2:M2");

        $objPHPExcel->getActiveSheet()->getStyle("A1:M2")->applyFromArray($style_header_table);

        $objPHPExcel->getActiveSheet()->SetCellValue("A4", "Tanggal Pengambilan");
        $objPHPExcel->getActiveSheet()->SetCellValue("C4", longdate_indo($exp_tanggal[2] . "-" . $exp_tanggal[1] . "-" . $exp_tanggal[0]));
        $objPHPExcel->getActiveSheet()->mergeCells("A4:B4");

        $objPHPExcel->getActiveSheet()->SetCellValue("A6", "No");
        $objPHPExcel->getActiveSheet()->SetCellValue("B6", "Nama");
        $objPHPExcel->getActiveSheet()->SetCellValue("C6", "NIK");
        $objPHPExcel->getActiveSheet()->SetCellValue("D6", "TL");
        $objPHPExcel->getActiveSheet()->SetCellValue("E6", "JK");
        $objPHPExcel->getActiveSheet()->SetCellValue("F6", "HP");
        $objPHPExcel->getActiveSheet()->SetCellValue("G6", "Alamat/Domisili");
        $objPHPExcel->getActiveSheet()->SetCellValue("H6", "Alamat KTP");
        $objPHPExcel->getActiveSheet()->SetCellValue("I6", "Pekerjaan");
        $objPHPExcel->getActiveSheet()->SetCellValue("J6", "Wilker PKM");
        $objPHPExcel->getActiveSheet()->SetCellValue("K6", "Kriteria Pasien");
        $objPHPExcel->getActiveSheet()->SetCellValue("L6", "SWAB");
        $objPHPExcel->getActiveSheet()->SetCellValue("M6", "Kode Sampel");

        $objPHPExcel->getActiveSheet()->getStyle("A6:M6")->applyFromArray($style_header);

        $number_column = 7;
        $no_row = 1;
        foreach ($data_peserta_swab as $key => $row) {
            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $number_column, $no_row);
            $objPHPExcel->getActiveSheet()->getStyle("A" . $number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $number_column, $row->nama);
            $objPHPExcel->getActiveSheet()->getStyle("B" . $number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("C" . $number_column, $row->nik);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                "C" . $number_column,
                $row->nik,
                PHPExcel_Cell_DataType::TYPE_STRING
            );
            $objPHPExcel->getActiveSheet()->getStyle("C" . $number_column)->applyFromArray($style_data_kiri);

            $objPHPExcel->getActiveSheet()->SetCellValue("D" . $number_column, $row->tanggal_lahir);
            $objPHPExcel->getActiveSheet()->getStyle("D" . $number_column)->applyFromArray($style_data_kiri);

            $objPHPExcel->getActiveSheet()->SetCellValue("E" . $number_column, $row->jenis_kelamin);
            $objPHPExcel->getActiveSheet()->getStyle("E" . $number_column)->applyFromArray($style_data_tengah);

            $objPHPExcel->getActiveSheet()->SetCellValue("F" . $number_column, $row->nomor_telepon);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                "F" . $number_column,
                $row->nomor_telepon,
                PHPExcel_Cell_DataType::TYPE_STRING
            );
            $objPHPExcel->getActiveSheet()->getStyle("F" . $number_column)->applyFromArray($style_data_kiri);

            $objPHPExcel->getActiveSheet()->SetCellValue("G" . $number_column, $row->alamat_domisili);
            $objPHPExcel->getActiveSheet()->getStyle("G" . $number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("H" . $number_column, $row->alamat_ktp);
            $objPHPExcel->getActiveSheet()->getStyle("H" . $number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("I" . $number_column, $row->pekerjaan_inti);
            $objPHPExcel->getActiveSheet()->getStyle("I" . $number_column)->applyFromArray($style_data_tengah);

            $objPHPExcel->getActiveSheet()->SetCellValue("J" . $number_column, $row->nama_puskesmas);
            $objPHPExcel->getActiveSheet()->getStyle("J" . $number_column)->applyFromArray($style_data_tengah);

            // $objPHPExcel->getActiveSheet()->SetCellValue("K" . $number_column, $row->nama);
            $objPHPExcel->getActiveSheet()->getStyle("K" . $number_column)->applyFromArray($style_data);

            $objPHPExcel->getActiveSheet()->SetCellValue("L" . $number_column, $row->jumlah_swab);
            $objPHPExcel->getActiveSheet()->getStyle("L" . $number_column)->applyFromArray($style_data_tengah);

            $objPHPExcel->getActiveSheet()->SetCellValue("M" . $number_column, $row->kode_sample);
            $objPHPExcel->getActiveSheet()->getStyle("M" . $number_column)->applyFromArray($style_data_tengah);



            $number_column++;
            $no_row++;
        }

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }

        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(false);

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);

        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth('30');
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(false);

        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth('30');
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(false);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $writer->save('php://output');
        exit;
    }

    public function cetak_hasil_pcr_admin_rs($id_peserta, $peserta_dari)
    {
        $data['user'] = $this->user_model->get(
            array(
                "fields" => "user.*,nama_level_user",
                "join" => array(
                    "level_user" => "id_level_user=level_user_id"
                ),
                "where" => array(
                    "id_user" => $this->session->userdata("id_user")
                )
            ),
            "row"
        );

        $data['user_head_lab'] = $this->user_model->get(
            array(
                "where" => array(
                    "is_kepala_laboratorium" => "1"
                )
            ),
            "row"
        );
        $id_peserta = decrypt_data($id_peserta);

        if ($peserta_dari == 'ch') {
            $data['peserta'] = $this->peserta_model->query("
            SELECT id_peserta_non_puskesmas,nama,nik,DATE_FORMAT(tanggal_lahir,'%m/%d/%Y') AS tanggal_lahir_calc,DATE_FORMAT(tanggal_lahir,'%d/%m/%Y') AS tanggal_lahir,no_rekam_medis,jenis_kelamin,alamat_domisili,rt_domisili,klasifikasi,nama_wilayah,a.*
            FROM peserta_non_puskesmas
            INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
            LEFT JOIN (
                SELECT
                    id_trx_pemeriksaan_non_puskesmas AS id_trx_pemeriksaan_non_puskesmas_last,
                    peserta_non_puskesmas_id,
                    a.level_user_id AS level_user_id_last,
                    class_badge AS class_badge_last,
                    usulan_faskes AS usulan_faskes_pemeriksaan,
                    trx_pemeriksaan_non_puskesmas.jenis_pemeriksaan_id AS jenis_pemeriksaan_id_last,
                    trx_pemeriksaan_non_puskesmas.hasil_pemeriksaan_id AS hasil_pemeriksaan_id_last,
                    is_sembuh AS is_sembuh_last,
                    is_meninggal AS is_meninggal_last,
                    is_probable AS is_probable_last,
                    kode_sample,
                    tanggal_terima_sampel,
                    jenis_spesimen,
                    DATE_FORMAT(trx_pemeriksaan_non_puskesmas.created_at,'%Y-%m-%d') AS tanggal_pemeriksaan,
                    b.nama_lengkap AS nama_lab,
                    c.nama_lengkap AS nama_dokter,
                    c.nip AS nip_dokter
                FROM trx_pemeriksaan_non_puskesmas 
                LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                LEFT JOIN user a ON a.id_user=id_user_created 
                LEFT JOIN user b ON b.id_user=id_user_lab 
                LEFT JOIN user c ON c.id_user=id_user_dokter
                WHERE id_trx_pemeriksaan_non_puskesmas IN (
                    SELECT MAX(id_trx_pemeriksaan_non_puskesmas) AS id_trx_pemeriksaan_non_puskesmas 
                    FROM trx_pemeriksaan_non_puskesmas 
                    WHERE trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
                    GROUP BY peserta_non_puskesmas_id
                    ORDER BY id_trx_pemeriksaan_non_puskesmas DESC
                    ) 
                AND trx_pemeriksaan_non_puskesmas.deleted_at IS NULL
            ) AS a ON a.peserta_non_puskesmas_id=id_peserta_non_puskesmas
            WHERE id_peserta_non_puskesmas = '" . $id_peserta . "' AND peserta_non_puskesmas.deleted_at IS NULL
            ")->row();
        } else {
            $data['peserta'] = $this->peserta_model->query("
            SELECT id_peserta,nama,nik,DATE_FORMAT(tanggal_lahir,'%m/%d/%Y') AS tanggal_lahir_calc,DATE_FORMAT(tanggal_lahir,'%d/%m/%Y') AS tanggal_lahir,no_rekam_medis,jenis_kelamin,alamat_domisili,rt_domisili,klasifikasi,nama_wilayah,a.*
            FROM peserta
            INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
            LEFT JOIN (
                SELECT
                    id_trx_pemeriksaan AS id_trx_pemeriksaan_last,
                    peserta_id,
                    a.level_user_id AS level_user_id_last,
                    class_badge AS class_badge_last,
                    usulan_faskes AS usulan_faskes_pemeriksaan,
                    trx_pemeriksaan.jenis_pemeriksaan_id AS jenis_pemeriksaan_id_last,
                    trx_pemeriksaan.hasil_pemeriksaan_id AS hasil_pemeriksaan_id_last,
                    is_sembuh AS is_sembuh_last,
                    is_meninggal AS is_meninggal_last,
                    is_probable AS is_probable_last,
                    kode_sample,
                    tanggal_terima_sampel,
                    jenis_spesimen,
                    DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') AS tanggal_pemeriksaan,
                    b.nama_lengkap AS nama_lab,
                    c.nama_lengkap AS nama_dokter,
                    c.nip AS nip_dokter
                FROM trx_pemeriksaan 
                LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL 
                LEFT JOIN user a ON a.id_user=id_user_created 
                LEFT JOIN user b ON b.id_user=id_user_lab 
                LEFT JOIN user c ON c.id_user=id_user_dokter
                WHERE id_trx_pemeriksaan IN (
                    SELECT MAX(id_trx_pemeriksaan) AS id_trx_pemeriksaan 
                    FROM trx_pemeriksaan 
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_pemeriksaan DESC
                    ) 
                AND trx_pemeriksaan.deleted_at IS NULL
            ) AS a ON a.peserta_id=id_peserta
            WHERE id_peserta = '" . $id_peserta . "' AND peserta.deleted_at IS NULL
            ")->row();
        }

        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'orientation' => 'L', 'tempDir' => '/tmp']);
        $data = $this->load->view('cetak_hasil_pcr_admin_rs', $data, TRUE);
        $mpdf->WriteHTML($data);
        $mpdf->Output();
    }

    public function cetak_hasil_pcr_single_admin_rs($id_trx_pemeriksaan)
    {
        $data['user'] = $this->user_model->get(
            array(
                "fields" => "user.*,nama_level_user",
                "join" => array(
                    "level_user" => "id_level_user=level_user_id"
                ),
                "where" => array(
                    "id_user" => $this->session->userdata("id_user")
                )
            ),
            "row"
        );

        $data['user_head_lab'] = $this->user_model->get(
            array(
                "where" => array(
                    "is_kepala_laboratorium" => "1"
                )
            ),
            "row"
        );

        $id_trx_pemeriksaan = decrypt_data($id_trx_pemeriksaan);
        $data['peserta'] = $this->peserta_model->query("
        SELECT 
            id_peserta,
            nama,
            nik,
            DATE_FORMAT(tanggal_lahir,'%m/%d/%Y') AS tanggal_lahir_calc,
            DATE_FORMAT(tanggal_lahir,'%d/%m/%Y') AS tanggal_lahir,
            no_rekam_medis,
            jenis_kelamin,
            alamat_domisili,
            rt_domisili,
            klasifikasi,
            nama_wilayah,
            id_trx_pemeriksaan AS id_trx_pemeriksaan_last,
            peserta_id,
            a.level_user_id AS level_user_id_last,
            class_badge AS class_badge_last,
            trx_pemeriksaan.usulan_faskes AS usulan_faskes_pemeriksaan,
            trx_pemeriksaan.jenis_pemeriksaan_id AS jenis_pemeriksaan_id_last,
            trx_pemeriksaan.hasil_pemeriksaan_id AS hasil_pemeriksaan_id_last,
            is_sembuh AS is_sembuh_last,
            is_meninggal AS is_meninggal_last,
            is_probable AS is_probable_last,
            kode_sample,
            tanggal_terima_sampel,
            jenis_spesimen,
            DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') AS tanggal_pemeriksaan,
            b.nama_lengkap AS nama_lab,
            c.nama_lengkap AS nama_dokter,
            c.nip AS nip_dokter
        FROM peserta
        INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
        INNER JOIN trx_pemeriksaan ON peserta_id=id_peserta
        LEFT JOIN hasil_pemeriksaan ON id_hasil_pemeriksaan=hasil_pemeriksaan_id AND hasil_pemeriksaan.deleted_at IS NULL
        LEFT JOIN user a ON a.id_user=trx_pemeriksaan.id_user_created 
        LEFT JOIN user b ON b.id_user=id_user_lab
        LEFT JOIN user c ON c.id_user=id_user_dokter
        WHERE id_trx_pemeriksaan = '" . $id_trx_pemeriksaan . "' AND peserta.deleted_at IS NULL AND trx_pemeriksaan.deleted_at IS NULL
        ")->row();

        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'orientation' => 'L', 'tempDir' => '/tmp']);
        $data = $this->load->view('cetak_hasil_pcr_admin_rs', $data, TRUE);
        $mpdf->WriteHTML($data);
        $mpdf->Output();
    }

    public function detail_peserta_tni_polri($id_wilayah)
    {
        $data['id_wilayah'] = $id_wilayah;

        $data['wilayah'] = $this->master_wilayah_model->get_by(decrypt_data($id_wilayah));
        $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'peserta', 'content' => 'Peserta', 'is_active' => false], ['link' => false, 'content' => 'Detail Wilayah', 'is_active' => true]];
        $this->execute('detail_peserta_wilayah_tni_polri', $data);
    }

    public function change_status_rawat_peserta()
    {
        $id_trx_pemeriksaan_last = decrypt_data($this->ipost("id_trx_pemeriksaan_last"));
        $peserta_dari = $this->ipost("peserta_dari");
        $status_rawat = $this->ipost("status_rawat");
        $ruang_perawatan = $this->ipost("ruang_perawatan");
        $id_peserta = decrypt_data($this->ipost("id_peserta"));

        //update

        if ($peserta_dari == "rsud") {
            $data_peserta = array(
                "is_status"                 => $status_rawat,
                "ruang_perawatan"           => $ruang_perawatan,
                "peserta_id"                => $id_peserta,
                "trx_terkonfirmasi_last_id" => $id_trx_pemeriksaan_last,
                "created_at"                => $this->datetime()
            );

            $status = $this->status_rawat_model->save($data_peserta);
        } else {
            $data_peserta = array(
                "is_status"                               => $status_rawat,
                "ruang_perawatan"                         => $ruang_perawatan,
                "peserta_non_puskesmas_id"                => $id_peserta,
                "trx_terkonfirmasi_non_puskesmas_last_id" => $id_trx_pemeriksaan_last,
                "created_at"                              => $this->datetime()
            );

            $status = $this->status_rawat_non_puskesmas_model->save($data_peserta);
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function list_peserta_nik_empty()
    {
        $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'peserta', 'content' => 'Peserta', 'is_active' => false], ['link' => false, 'content' => 'List Peserta - NIK Kosong', 'is_active' => true]];
        $this->execute('index_peserta_nik_empty', $data);
    }

    public function update_nik_peserta()
    {
        $id_peserta = decrypt_data($this->ipost("id_peserta"));
        $nik = $this->ipost("nik");

        $data = array(
            "nik" => $nik
        );

        $status = $this->peserta_model->edit($id_peserta, $data);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function list_peserta_rt_domisili_empty()
    {
        $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'peserta', 'content' => 'Peserta', 'is_active' => false], ['link' => false, 'content' => 'List Peserta - RT Domisili Kosong', 'is_active' => true]];
        $this->execute('index_peserta_rt_domisili_empty', $data);
    }

    public function update_rt_domisili_peserta()
    {
        $id_peserta = decrypt_data($this->ipost("id_peserta"));
        $rt_domisili = $this->ipost("rt_domisili");

        $data = array(
            "rt_domisili" => $rt_domisili
        );

        $status = $this->peserta_model->edit($id_peserta, $data);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function cetak_cohort()
    {
        require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel.php');
        require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

        $filename = "Laporan Cohort.xlsx";

        $objPHPExcel = new PHPExcel();

        $style_data_tengah = array(
            'borders' => array(
                'right' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                ),
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                ),
                'left' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );

        $style_data_kanan = array(
            'borders' => array(
                'right' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                ),
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                ),
                'left' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
            )
        );

        $style_data = array(
            'borders' => array(
                'right' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                ),
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                ),
                'left' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
        );

        $style_header = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'font' => array(
                'size' => 11,
                'bold' => true
            ), 'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'dae1f3')
            )
        );

        $style_header_table = array(
            'font' => array(
                'size' => 11,
                'bold' => true
            ), 'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "DATA PENGAMBILAN SWAB DINAS KESEHATAN");
        $objPHPExcel->getActiveSheet()->mergeCells("A1:M1");
        $objPHPExcel->getActiveSheet()->SetCellValue("A2", "KABUPATEN KOTAWARINGIN BARAT");
        $objPHPExcel->getActiveSheet()->mergeCells("A2:M2");
        $objPHPExcel->getActiveSheet()->getStyle("A1:M2")->applyFromArray($style_header_table);

        $objPHPExcel->getActiveSheet()->SetCellValue("A7", "No");
        $objPHPExcel->getActiveSheet()->SetCellValue("B7", "Nama");
        $objPHPExcel->getActiveSheet()->SetCellValue("C7", "NIK");
        $objPHPExcel->getActiveSheet()->SetCellValue("D7", "TL");
        $objPHPExcel->getActiveSheet()->SetCellValue("E7", "JK");
        $objPHPExcel->getActiveSheet()->SetCellValue("F7", "HP");
        $objPHPExcel->getActiveSheet()->SetCellValue("G7", "Alamat / Domisili");
        $objPHPExcel->getActiveSheet()->SetCellValue("H7", "Alamat KTP");
        $objPHPExcel->getActiveSheet()->SetCellValue("I7", "Pekerjaan");
        $objPHPExcel->getActiveSheet()->SetCellValue("J7", "Wilker PKM");
        $objPHPExcel->getActiveSheet()->SetCellValue("K7", "Kriteria Pasien");
        $objPHPExcel->getActiveSheet()->SetCellValue("L7", "SWAB");
        $objPHPExcel->getActiveSheet()->SetCellValue("M7", "Kode Sample");
        $objPHPExcel->getActiveSheet()->getStyle("A7")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("B7")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("C7")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("D7")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("E7")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("F7")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("G7")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("H7")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("I7")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("J7")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("K7")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("L7")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("M7")->applyFromArray($style_header);

        $data_suspek = $this->peserta_model->query(
            "
            SELECT 
                nama,
                nik,
                DATE_FORMAT(tanggal_lahir,'%d/%m/%Y') AS tanggal_lahir,
                jenis_kelamin,
                nomor_telepon,
                IF(kelurahan_master_wilayah_id != '109',
                    CONCAT(alamat_domisili,' RT. ',
                        IF(master_rt_domisili_id IS NULL,
                            rt_domisili,
                            rt
                        ),
                        ' ',klasifikasi,' ',
                        nama_wilayah
                    ),
                    'Wilayah Kobar Lainnya'
                ) AS alamat_domisili_custom,
                CONCAT(alamat_ktp,' RT. ',rt_ktp) AS alamat_ktp_custom,
                pekerjaan_inti,
                IFNULL(nama_puskesmas,'') AS nama_puskesmas,
                'Suspek' AS kriteria_pasien,
                b.jumlah_swab AS jumlah_swab,
                kode_sample
            FROM peserta
            INNER JOIN
            (
                SELECT id_trx_pemeriksaan AS id_trx_pemeriksaan_last,peserta_id,kode_sample
                FROM trx_pemeriksaan 
                WHERE id_trx_pemeriksaan IN 
                (
                    SELECT MAX(id_trx_pemeriksaan) AS id_trx_pemeriksaan 
                    FROM trx_pemeriksaan 
                    WHERE trx_pemeriksaan.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_pemeriksaan DESC
                ) 
                AND trx_pemeriksaan.deleted_at IS NULL
                AND jenis_pemeriksaan_id = '1'
                AND id_user_created = '" . $this->config->item('id_user_labkesda') . "'
                AND hasil_pemeriksaan_id IS NULL
            ) AS a ON a.peserta_id=id_peserta
            INNER JOIN master_wilayah ON id_master_wilayah=kelurahan_master_wilayah_id
            LEFT JOIN master_rt ON id_master_rt=master_rt_domisili_id
            LEFT JOIN master_puskesmas ON id_master_puskesmas=puskesmas_id
            LEFT JOIN
            (
                SELECT COUNT(*) AS jumlah_swab,peserta_id
                FROM trx_pemeriksaan
                WHERE trx_pemeriksaan.deleted_at IS NULL AND jenis_pemeriksaan_id = '1'
                GROUP BY peserta_id
            ) AS b ON b.peserta_id=id_peserta
            WHERE peserta.deleted_at IS NULL
            "
        )->result();

        $row = 8;
        $no = 1;
        foreach ($data_suspek as $key => $value) {
            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $no);
            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $value->nama);
            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value->nik);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                "C" . $row,
                $value->nik,
                PHPExcel_Cell_DataType::TYPE_STRING
            );
            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $value->tanggal_lahir);
            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $value->jenis_kelamin);
            $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $value->nomor_telepon);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                "C" . $row,
                $value->nomor_telepon,
                PHPExcel_Cell_DataType::TYPE_STRING
            );
            $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->SetCellValue("G" . $row, $value->alamat_domisili_custom);
            $objPHPExcel->getActiveSheet()->getStyle("G" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->SetCellValue("H" . $row, $value->alamat_ktp_custom);
            $objPHPExcel->getActiveSheet()->getStyle("H" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->SetCellValue("I" . $row, $value->pekerjaan_inti);
            $objPHPExcel->getActiveSheet()->getStyle("I" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->SetCellValue("J" . $row, $value->nama_puskesmas);
            $objPHPExcel->getActiveSheet()->getStyle("J" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->SetCellValue("K" . $row, $value->kriteria_pasien);
            $objPHPExcel->getActiveSheet()->getStyle("K" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->SetCellValue("L" . $row, $value->jumlah_swab);
            $objPHPExcel->getActiveSheet()->getStyle("L" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->SetCellValue("M" . $row, $value->kode_sample);
            $objPHPExcel->getActiveSheet()->getStyle("M" . $row)->applyFromArray($style_data_tengah);

            $row++;
            $no++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('SUSPEK');

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }



        $objPHPExcel->createSheet();

        $objPHPExcel->setActiveSheetIndex(1);

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "LAPORAN HARIAN PASIEN DALAM PENGAWASAN POSITIF COVID 19 KOTAWARINGIN BARAT");
        $objPHPExcel->getActiveSheet()->mergeCells("A1:Q1");
        $objPHPExcel->getActiveSheet()->getStyle("A1:Q1")->applyFromArray($style_header_table);

        $objPHPExcel->getActiveSheet()->SetCellValue("A3", "NO");
        $objPHPExcel->getActiveSheet()->SetCellValue("B3", "PUSKESMAS");
        $objPHPExcel->getActiveSheet()->SetCellValue("C3", "NAMA");
        $objPHPExcel->getActiveSheet()->SetCellValue("D3", "KECAMATAN");
        $objPHPExcel->getActiveSheet()->SetCellValue("E3", "KEL/DESA");
        $objPHPExcel->getActiveSheet()->SetCellValue("F3", "TANGGAL RILIS KONFIRMASI");
        $objPHPExcel->getActiveSheet()->SetCellValue("G3", "JENIS KELAMIN");
        $objPHPExcel->getActiveSheet()->SetCellValue("H3", "TANGGAL LAHIR");
        $objPHPExcel->getActiveSheet()->SetCellValue("I3", "UMUR");
        $objPHPExcel->getActiveSheet()->SetCellValue("J3", "NOMOR KONTAK");
        $objPHPExcel->getActiveSheet()->SetCellValue("K3", "STATUS");
        $objPHPExcel->getActiveSheet()->SetCellValue("L3", "PEKERJAAN");
        $objPHPExcel->getActiveSheet()->SetCellValue("M3", "RIWAYAT PASIEN (Pelaku Perjalanan, Kontak Erat, Tidak ada riwayat Perjalanan dan Kontak Erat)/ Riwayat Penyakit");
        $objPHPExcel->getActiveSheet()->SetCellValue("N3", "ALAMAT DOMISILI");
        $objPHPExcel->getActiveSheet()->SetCellValue("O3", "ALAMAT KTP");
        $objPHPExcel->getActiveSheet()->SetCellValue("P3", "NIK");
        $objPHPExcel->getActiveSheet()->SetCellValue("Q3", "TGL SWAB");

        $objPHPExcel->getActiveSheet()->getStyle("A3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("B3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("C3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("D3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("E3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("F3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("G3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("H3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("I3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("J3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("K3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("L3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("M3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("N3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("O3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("P3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("Q3")->applyFromArray($style_header);

        $data_konfirmasi = $this->peserta_model->query(
            "
            SELECT
                IFNULL(nama_puskesmas,'') AS nama_puskesmas,
                nama,
                b.nama_wilayah AS nama_kecamatan,
                IF(kelurahan_master_wilayah_id != '109',
                    a.nama_wilayah,
                    'Wilayah Kobar Lainnya'
                ) AS kel_desa,
                DATE_FORMAT(trx_terkonfirmasi_rilis.created_at,'%d/%m/%Y') AS tanggal_terkonfirmasi_rilis,
                jenis_kelamin,
                DATE_FORMAT(tanggal_lahir,'%d/%m/%Y') AS tanggal_lahir,
                IFNULL(TIMESTAMPDIFF(YEAR, `tanggal_lahir`, DATE_FORMAT(trx_terkonfirmasi_rilis.created_at, '%Y-%m-%d')), 0) AS umur_kena_covid,
                nomor_telepon,
                'Konfirmasi' AS status_konfirmasi,
                pekerjaan_inti,
                kontak_erat,
                IF(kelurahan_master_wilayah_id != '109',
                    CONCAT(alamat_domisili,' RT. ',
                        IF(master_rt_domisili_id IS NULL,
                            rt_domisili,
                            rt
                        )
                    ),
                    'Wilayah Kobar Lainnya'
                ) AS alamat_domisili_custom,
                CONCAT(alamat_ktp,' RT. ',rt_ktp) AS alamat_ktp_custom,
                nik,
                DATE_FORMAT(trx_pemeriksaan.created_at,'%d/%m/%Y') AS tanggal_swab
            FROM peserta
            INNER JOIN trx_terkonfirmasi_rilis ON id_peserta=peserta_id AND trx_terkonfirmasi_rilis.deleted_at IS NULL
            INNER JOIN trx_pemeriksaan ON id_trx_pemeriksaan=trx_pemeriksaan_id AND trx_pemeriksaan.deleted_at IS NULL
            INNER JOIN master_wilayah AS a ON a.id_master_wilayah=kelurahan_master_wilayah_id
            INNER JOIN master_wilayah AS b ON b.kode_wilayah=a.kode_induk
            LEFT JOIN master_rt ON id_master_rt=master_rt_domisili_id
            LEFT JOIN master_puskesmas ON id_master_puskesmas=a.puskesmas_id
            WHERE STATUS = '1' AND peserta.deleted_at IS NULL
            GROUP BY trx_terkonfirmasi_rilis.peserta_id
            "
        )->result();

        $row = 4;
        $no = 1;
        foreach ($data_konfirmasi as $key => $value) {

            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $no);
            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $value->nama_puskesmas);
            $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value->nama);
            $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $value->nama_kecamatan);
            $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $value->kel_desa);
            $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $value->tanggal_terkonfirmasi_rilis);
            $objPHPExcel->getActiveSheet()->SetCellValue("G" . $row, $value->jenis_kelamin);
            $objPHPExcel->getActiveSheet()->SetCellValue("H" . $row, $value->tanggal_lahir);
            $objPHPExcel->getActiveSheet()->SetCellValue("I" . $row, $value->umur_kena_covid);
            $objPHPExcel->getActiveSheet()->SetCellValue("J" . $row, $value->nomor_telepon);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                "J" . $row,
                $value->nomor_telepon,
                PHPExcel_Cell_DataType::TYPE_STRING
            );
            $objPHPExcel->getActiveSheet()->SetCellValue("K" . $row, $value->status_konfirmasi);
            $objPHPExcel->getActiveSheet()->SetCellValue("L" . $row, $value->pekerjaan_inti);
            $objPHPExcel->getActiveSheet()->SetCellValue("M" . $row, $value->kontak_erat);
            $objPHPExcel->getActiveSheet()->SetCellValue("N" . $row, $value->alamat_domisili_custom);
            $objPHPExcel->getActiveSheet()->SetCellValue("O" . $row, $value->alamat_ktp_custom);
            $objPHPExcel->getActiveSheet()->SetCellValue("P" . $row, $value->nik);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                "P" . $row,
                $value->nik,
                PHPExcel_Cell_DataType::TYPE_STRING
            );
            $objPHPExcel->getActiveSheet()->SetCellValue("Q" . $row, $value->tanggal_swab);

            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("G" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("H" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("I" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("J" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("K" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("L" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("M" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("N" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("O" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("P" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("Q" . $row)->applyFromArray($style_data_tengah);

            $row++;
            $no++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('TOTAL KONFIRMASI');

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }



        $objPHPExcel->createSheet();

        $objPHPExcel->setActiveSheetIndex(2);

        $objPHPExcel->getActiveSheet()->SetCellValue("A3", "NO");
        $objPHPExcel->getActiveSheet()->SetCellValue("B3", "PUSKESMAS");
        $objPHPExcel->getActiveSheet()->SetCellValue("C3", "NAMA");
        $objPHPExcel->getActiveSheet()->SetCellValue("D3", "KECAMATAN");
        $objPHPExcel->getActiveSheet()->SetCellValue("E3", "KEL/DESA");
        $objPHPExcel->getActiveSheet()->SetCellValue("F3", "TANGGAL RILIS KONFIRMASI");
        $objPHPExcel->getActiveSheet()->SetCellValue("G3", "JENIS KELAMIN");
        $objPHPExcel->getActiveSheet()->SetCellValue("H3", "TANGGAL LAHIR");
        $objPHPExcel->getActiveSheet()->SetCellValue("I3", "UMUR");
        $objPHPExcel->getActiveSheet()->SetCellValue("J3", "NOMOR KONTAK");
        $objPHPExcel->getActiveSheet()->SetCellValue("K3", "STATUS");
        $objPHPExcel->getActiveSheet()->SetCellValue("L3", "PEKERJAAN");
        $objPHPExcel->getActiveSheet()->SetCellValue("M3", "RIWAYAT PASIEN (Pelaku Perjalanan, Kontak Erat, Tidak ada riwayat Perjalanan dan Kontak Erat)/ Riwayat Penyakit");
        $objPHPExcel->getActiveSheet()->SetCellValue("N3", "ALAMAT DOMISILI");
        $objPHPExcel->getActiveSheet()->SetCellValue("O3", "ALAMAT KTP");
        $objPHPExcel->getActiveSheet()->SetCellValue("P3", "NIK");
        $objPHPExcel->getActiveSheet()->SetCellValue("Q3", "TGL SWAB");

        $objPHPExcel->getActiveSheet()->getStyle("A3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("B3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("C3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("D3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("E3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("F3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("G3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("H3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("I3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("J3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("K3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("L3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("M3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("N3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("O3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("P3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("Q3")->applyFromArray($style_header);

        $data_aktif = $this->peserta_model->query(
            "
            SELECT
                IFNULL(nama_puskesmas,'') AS nama_puskesmas,
                nama,
                b.nama_wilayah AS nama_kecamatan,
                IF(kelurahan_master_wilayah_id != '109',
                    a.nama_wilayah,
                    'Wilayah Kobar Lainnya'
                ) AS kel_desa,
                DATE_FORMAT(c.created_at,'%d/%m/%Y') AS tanggal_terkonfirmasi_rilis,
                jenis_kelamin,
                DATE_FORMAT(tanggal_lahir,'%d/%m/%Y') AS tanggal_lahir,
                IFNULL(TIMESTAMPDIFF(YEAR, `tanggal_lahir`, DATE_FORMAT(c.created_at, '%Y-%m-%d')), 0) AS umur_kena_covid,
                nomor_telepon,
                'Konfirmasi' AS status_konfirmasi,
                pekerjaan_inti,
                kontak_erat,
                IF(kelurahan_master_wilayah_id != '109',
                    CONCAT(alamat_domisili,' RT. ',
                        IF(master_rt_domisili_id IS NULL,
                            rt_domisili,
                            rt
                        )
                    ),
                    'Wilayah Kobar Lainnya'
                ) AS alamat_domisili_custom,
                CONCAT(alamat_ktp,' RT. ',rt_ktp) AS alamat_ktp_custom,
                nik,
                DATE_FORMAT(trx_pemeriksaan.created_at,'%d/%m/%Y') AS tanggal_swab
            FROM peserta
            INNER JOIN
            (
                SELECT *
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                )
            ) AS c ON id_peserta=c.peserta_id
            INNER JOIN trx_pemeriksaan ON id_trx_pemeriksaan=trx_pemeriksaan_id
            INNER JOIN master_wilayah AS a ON a.id_master_wilayah=kelurahan_master_wilayah_id
            INNER JOIN master_wilayah AS b ON b.kode_wilayah=a.kode_induk
            LEFT JOIN master_rt ON id_master_rt=master_rt_domisili_id
            LEFT JOIN master_puskesmas ON id_master_puskesmas=a.puskesmas_id
            WHERE c.status = '1' AND peserta.deleted_at IS NULL
            GROUP BY c.peserta_id
            "
        )->result();

        $row = 4;
        $no = 1;
        foreach ($data_aktif as $key => $value) {

            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $no);
            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $value->nama_puskesmas);
            $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value->nama);
            $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $value->nama_kecamatan);
            $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $value->kel_desa);
            $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $value->tanggal_terkonfirmasi_rilis);
            $objPHPExcel->getActiveSheet()->SetCellValue("G" . $row, $value->jenis_kelamin);
            $objPHPExcel->getActiveSheet()->SetCellValue("H" . $row, $value->tanggal_lahir);
            $objPHPExcel->getActiveSheet()->SetCellValue("I" . $row, $value->umur_kena_covid);
            $objPHPExcel->getActiveSheet()->SetCellValue("J" . $row, $value->nomor_telepon);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                "J" . $row,
                $value->nomor_telepon,
                PHPExcel_Cell_DataType::TYPE_STRING
            );
            $objPHPExcel->getActiveSheet()->SetCellValue("K" . $row, $value->status_konfirmasi);
            $objPHPExcel->getActiveSheet()->SetCellValue("L" . $row, $value->pekerjaan_inti);
            $objPHPExcel->getActiveSheet()->SetCellValue("M" . $row, $value->kontak_erat);
            $objPHPExcel->getActiveSheet()->SetCellValue("N" . $row, $value->alamat_domisili_custom);
            $objPHPExcel->getActiveSheet()->SetCellValue("O" . $row, $value->alamat_ktp_custom);
            $objPHPExcel->getActiveSheet()->SetCellValue("P" . $row, $value->nik);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                "P" . $row,
                $value->nik,
                PHPExcel_Cell_DataType::TYPE_STRING
            );
            $objPHPExcel->getActiveSheet()->SetCellValue("Q" . $row, $value->tanggal_swab);

            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("G" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("H" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("I" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("J" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("K" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("L" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("M" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("N" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("O" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("P" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("Q" . $row)->applyFromArray($style_data_tengah);

            $row++;
            $no++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('DALAM PERAWATAN HARI INI');

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }



        $objPHPExcel->createSheet();

        $objPHPExcel->setActiveSheetIndex(3);

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "LAPORAN HARIAN PASIEN SEMBUH KONFIRMASI COVID 19 KOTAWARINGIN BARAT");
        $objPHPExcel->getActiveSheet()->mergeCells("A1:Q1");
        $objPHPExcel->getActiveSheet()->getStyle("A1:Q1")->applyFromArray($style_header_table);

        $objPHPExcel->getActiveSheet()->SetCellValue("A3", "NO");
        $objPHPExcel->getActiveSheet()->SetCellValue("B3", "PUSKESMAS");
        $objPHPExcel->getActiveSheet()->SetCellValue("C3", "NAMA");
        $objPHPExcel->getActiveSheet()->SetCellValue("D3", "KECAMATAN");
        $objPHPExcel->getActiveSheet()->SetCellValue("E3", "KEL/DESA");
        $objPHPExcel->getActiveSheet()->SetCellValue("F3", "TANGGAL RILIS KONFIRMASI");
        $objPHPExcel->getActiveSheet()->SetCellValue("G3", "JENIS KELAMIN");
        $objPHPExcel->getActiveSheet()->SetCellValue("H3", "TANGGAL LAHIR");
        $objPHPExcel->getActiveSheet()->SetCellValue("I3", "UMUR");
        $objPHPExcel->getActiveSheet()->SetCellValue("J3", "NOMOR KONTAK");
        $objPHPExcel->getActiveSheet()->SetCellValue("K3", "STATUS");
        $objPHPExcel->getActiveSheet()->SetCellValue("L3", "PEKERJAAN");
        $objPHPExcel->getActiveSheet()->SetCellValue("M3", "RIWAYAT PASIEN (Pelaku Perjalanan, Kontak Erat, Tidak ada riwayat Perjalanan dan Kontak Erat)/ Riwayat Penyakit");
        $objPHPExcel->getActiveSheet()->SetCellValue("N3", "ALAMAT DOMISILI");
        $objPHPExcel->getActiveSheet()->SetCellValue("O3", "ALAMAT KTP");
        $objPHPExcel->getActiveSheet()->SetCellValue("P3", "NIK");
        $objPHPExcel->getActiveSheet()->SetCellValue("Q3", "TGL SWAB");

        $objPHPExcel->getActiveSheet()->getStyle("A3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("B3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("C3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("D3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("E3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("F3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("G3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("H3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("I3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("J3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("K3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("L3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("M3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("N3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("O3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("P3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("Q3")->applyFromArray($style_header);

        $data_sembuh = $this->peserta_model->query(
            "
            SELECT
                IFNULL(nama_puskesmas,'') AS nama_puskesmas,
                nama,
                b.nama_wilayah AS nama_kecamatan,
                IF(kelurahan_master_wilayah_id != '109',
                    a.nama_wilayah,
                    'Wilayah Kobar Lainnya'
                ) AS kel_desa,
                DATE_FORMAT(c.created_at,'%d/%m/%Y') AS tanggal_terkonfirmasi_rilis,
                jenis_kelamin,
                DATE_FORMAT(tanggal_lahir,'%d/%m/%Y') AS tanggal_lahir,
                IFNULL(TIMESTAMPDIFF(YEAR, `tanggal_lahir`, DATE_FORMAT(c.created_at, '%Y-%m-%d')), 0) AS umur_kena_covid,
                nomor_telepon,
                'Sembuh' AS status_konfirmasi,
                pekerjaan_inti,
                kontak_erat,
                IF(kelurahan_master_wilayah_id != '109',
                    CONCAT(alamat_domisili,' RT. ',
                        IF(master_rt_domisili_id IS NULL,
                            rt_domisili,
                            rt
                        )
                    ),
                    'Wilayah Kobar Lainnya'
                ) AS alamat_domisili_custom,
                CONCAT(alamat_ktp,' RT. ',rt_ktp) AS alamat_ktp_custom,
                nik,
                DATE_FORMAT(trx_pemeriksaan.created_at,'%d/%m/%Y') AS tanggal_swab
            FROM peserta
            INNER JOIN
            (
                SELECT *
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                )
            ) AS c ON id_peserta=c.peserta_id
            INNER JOIN trx_pemeriksaan ON id_trx_pemeriksaan=trx_pemeriksaan_id
            INNER JOIN master_wilayah AS a ON a.id_master_wilayah=kelurahan_master_wilayah_id
            INNER JOIN master_wilayah AS b ON b.kode_wilayah=a.kode_induk
            LEFT JOIN master_rt ON id_master_rt=master_rt_domisili_id
            LEFT JOIN master_puskesmas ON id_master_puskesmas=a.puskesmas_id
            WHERE c.status = '2' AND peserta.deleted_at IS NULL
            GROUP BY c.peserta_id
            "
        )->result();

        $row = 4;
        $no = 1;
        foreach ($data_sembuh as $key => $value) {

            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $no);
            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $value->nama_puskesmas);
            $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value->nama);
            $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $value->nama_kecamatan);
            $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $value->kel_desa);
            $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $value->tanggal_terkonfirmasi_rilis);
            $objPHPExcel->getActiveSheet()->SetCellValue("G" . $row, $value->jenis_kelamin);
            $objPHPExcel->getActiveSheet()->SetCellValue("H" . $row, $value->tanggal_lahir);
            $objPHPExcel->getActiveSheet()->SetCellValue("I" . $row, $value->umur_kena_covid);
            $objPHPExcel->getActiveSheet()->SetCellValue("J" . $row, $value->nomor_telepon);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                "J" . $row,
                $value->nomor_telepon,
                PHPExcel_Cell_DataType::TYPE_STRING
            );
            $objPHPExcel->getActiveSheet()->SetCellValue("K" . $row, $value->status_konfirmasi);
            $objPHPExcel->getActiveSheet()->SetCellValue("L" . $row, $value->pekerjaan_inti);
            $objPHPExcel->getActiveSheet()->SetCellValue("M" . $row, $value->kontak_erat);
            $objPHPExcel->getActiveSheet()->SetCellValue("N" . $row, $value->alamat_domisili_custom);
            $objPHPExcel->getActiveSheet()->SetCellValue("O" . $row, $value->alamat_ktp_custom);
            $objPHPExcel->getActiveSheet()->SetCellValue("P" . $row, $value->nik);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                "P" . $row,
                $value->nik,
                PHPExcel_Cell_DataType::TYPE_STRING
            );
            $objPHPExcel->getActiveSheet()->SetCellValue("Q" . $row, $value->tanggal_swab);

            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("G" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("H" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("I" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("J" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("K" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("L" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("M" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("N" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("O" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("P" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("Q" . $row)->applyFromArray($style_data_tengah);

            $row++;
            $no++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('TOTAL SEMBUH');

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }



        $objPHPExcel->createSheet();

        $objPHPExcel->setActiveSheetIndex(4);

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "LAPORAN HARIAN PASIEN SEMBUH KONFIRMASI COVID 19 KOTAWARINGIN BARAT");
        $objPHPExcel->getActiveSheet()->mergeCells("A1:Q1");
        $objPHPExcel->getActiveSheet()->getStyle("A1:Q1")->applyFromArray($style_header_table);

        $objPHPExcel->getActiveSheet()->SetCellValue("A3", "NO");
        $objPHPExcel->getActiveSheet()->SetCellValue("B3", "PUSKESMAS");
        $objPHPExcel->getActiveSheet()->SetCellValue("C3", "NAMA");
        $objPHPExcel->getActiveSheet()->SetCellValue("D3", "KECAMATAN");
        $objPHPExcel->getActiveSheet()->SetCellValue("E3", "KEL/DESA");
        $objPHPExcel->getActiveSheet()->SetCellValue("F3", "TANGGAL RILIS KONFIRMASI");
        $objPHPExcel->getActiveSheet()->SetCellValue("G3", "JENIS KELAMIN");
        $objPHPExcel->getActiveSheet()->SetCellValue("H3", "TANGGAL LAHIR");
        $objPHPExcel->getActiveSheet()->SetCellValue("I3", "UMUR");
        $objPHPExcel->getActiveSheet()->SetCellValue("J3", "NOMOR KONTAK");
        $objPHPExcel->getActiveSheet()->SetCellValue("K3", "STATUS");
        $objPHPExcel->getActiveSheet()->SetCellValue("L3", "PEKERJAAN");
        $objPHPExcel->getActiveSheet()->SetCellValue("M3", "RIWAYAT PASIEN (Pelaku Perjalanan, Kontak Erat, Tidak ada riwayat Perjalanan dan Kontak Erat)/ Riwayat Penyakit");
        $objPHPExcel->getActiveSheet()->SetCellValue("N3", "ALAMAT DOMISILI");
        $objPHPExcel->getActiveSheet()->SetCellValue("O3", "ALAMAT KTP");
        $objPHPExcel->getActiveSheet()->SetCellValue("P3", "NIK");
        $objPHPExcel->getActiveSheet()->SetCellValue("Q3", "TGL SWAB");

        $objPHPExcel->getActiveSheet()->getStyle("A3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("B3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("C3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("D3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("E3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("F3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("G3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("H3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("I3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("J3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("K3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("L3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("M3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("N3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("O3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("P3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("Q3")->applyFromArray($style_header);

        $data_sembuh_rilis_hari_ini = $this->peserta_model->query(
            "
            SELECT
                IFNULL(nama_puskesmas,'') AS nama_puskesmas,
                nama,
                b.nama_wilayah AS nama_kecamatan,
                IF(kelurahan_master_wilayah_id != '109',
                    a.nama_wilayah,
                    'Wilayah Kobar Lainnya'
                ) AS kel_desa,
                DATE_FORMAT(c.created_at,'%d/%m/%Y') AS tanggal_terkonfirmasi_rilis,
                jenis_kelamin,
                DATE_FORMAT(tanggal_lahir,'%d/%m/%Y') AS tanggal_lahir,
                IFNULL(TIMESTAMPDIFF(YEAR, `tanggal_lahir`, DATE_FORMAT(c.created_at, '%Y-%m-%d')), 0) AS umur_kena_covid,
                nomor_telepon,
                'Sembuh' AS status_konfirmasi,
                pekerjaan_inti,
                kontak_erat,
                IF(kelurahan_master_wilayah_id != '109',
                    CONCAT(alamat_domisili,' RT. ',
                        IF(master_rt_domisili_id IS NULL,
                            rt_domisili,
                            rt
                        )
                    ),
                    'Wilayah Kobar Lainnya'
                ) AS alamat_domisili_custom,
                CONCAT(alamat_ktp,' RT. ',rt_ktp) AS alamat_ktp_custom,
                nik,
                DATE_FORMAT(trx_pemeriksaan.created_at,'%d/%m/%Y') AS tanggal_swab
            FROM peserta
            INNER JOIN
            (
                SELECT *
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                )
            ) AS c ON id_peserta=c.peserta_id
            INNER JOIN trx_pemeriksaan ON id_trx_pemeriksaan=trx_pemeriksaan_id
            INNER JOIN master_wilayah AS a ON a.id_master_wilayah=kelurahan_master_wilayah_id
            INNER JOIN master_wilayah AS b ON b.kode_wilayah=a.kode_induk
            LEFT JOIN master_rt ON id_master_rt=master_rt_domisili_id
            LEFT JOIN master_puskesmas ON id_master_puskesmas=a.puskesmas_id
            WHERE c.status = '2' AND peserta.deleted_at IS NULL AND DATE_FORMAT(c.created_at,'%Y-%m-%d') = CURDATE()
            GROUP BY c.peserta_id
            "
        )->result();

        $row = 4;
        $no = 1;
        foreach ($data_sembuh_rilis_hari_ini as $key => $value) {

            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $no);
            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $value->nama_puskesmas);
            $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value->nama);
            $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $value->nama_kecamatan);
            $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $value->kel_desa);
            $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $value->tanggal_terkonfirmasi_rilis);
            $objPHPExcel->getActiveSheet()->SetCellValue("G" . $row, $value->jenis_kelamin);
            $objPHPExcel->getActiveSheet()->SetCellValue("H" . $row, $value->tanggal_lahir);
            $objPHPExcel->getActiveSheet()->SetCellValue("I" . $row, $value->umur_kena_covid);
            $objPHPExcel->getActiveSheet()->SetCellValue("J" . $row, $value->nomor_telepon);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                "J" . $row,
                $value->nomor_telepon,
                PHPExcel_Cell_DataType::TYPE_STRING
            );
            $objPHPExcel->getActiveSheet()->SetCellValue("K" . $row, $value->status_konfirmasi);
            $objPHPExcel->getActiveSheet()->SetCellValue("L" . $row, $value->pekerjaan_inti);
            $objPHPExcel->getActiveSheet()->SetCellValue("M" . $row, $value->kontak_erat);
            $objPHPExcel->getActiveSheet()->SetCellValue("N" . $row, $value->alamat_domisili_custom);
            $objPHPExcel->getActiveSheet()->SetCellValue("O" . $row, $value->alamat_ktp_custom);
            $objPHPExcel->getActiveSheet()->SetCellValue("P" . $row, $value->nik);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                "P" . $row,
                $value->nik,
                PHPExcel_Cell_DataType::TYPE_STRING
            );
            $objPHPExcel->getActiveSheet()->SetCellValue("Q" . $row, $value->tanggal_swab);

            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("G" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("H" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("I" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("J" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("K" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("L" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("M" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("N" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("O" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("P" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("Q" . $row)->applyFromArray($style_data_tengah);

            $row++;
            $no++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('SEMBUH RILIS HARI INI');

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }



        $objPHPExcel->createSheet();

        $objPHPExcel->setActiveSheetIndex(5);

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "LAPORAN HARIAN PASIEN MENINGGAL COVID 19 KOTAWARINGIN BARAT");
        $objPHPExcel->getActiveSheet()->mergeCells("A1:Q1");
        $objPHPExcel->getActiveSheet()->getStyle("A1:Q1")->applyFromArray($style_header_table);

        $objPHPExcel->getActiveSheet()->SetCellValue("A3", "NO");
        $objPHPExcel->getActiveSheet()->SetCellValue("B3", "PUSKESMAS");
        $objPHPExcel->getActiveSheet()->SetCellValue("C3", "NAMA");
        $objPHPExcel->getActiveSheet()->SetCellValue("D3", "KECAMATAN");
        $objPHPExcel->getActiveSheet()->SetCellValue("E3", "KEL/DESA");
        $objPHPExcel->getActiveSheet()->SetCellValue("F3", "TANGGAL RILIS KONFIRMASI");
        $objPHPExcel->getActiveSheet()->SetCellValue("G3", "JENIS KELAMIN");
        $objPHPExcel->getActiveSheet()->SetCellValue("H3", "TANGGAL LAHIR");
        $objPHPExcel->getActiveSheet()->SetCellValue("I3", "UMUR");
        $objPHPExcel->getActiveSheet()->SetCellValue("J3", "NOMOR KONTAK");
        $objPHPExcel->getActiveSheet()->SetCellValue("K3", "STATUS");
        $objPHPExcel->getActiveSheet()->SetCellValue("L3", "PEKERJAAN");
        $objPHPExcel->getActiveSheet()->SetCellValue("M3", "RIWAYAT PASIEN (Pelaku Perjalanan, Kontak Erat, Tidak ada riwayat Perjalanan dan Kontak Erat)/ Riwayat Penyakit");
        $objPHPExcel->getActiveSheet()->SetCellValue("N3", "ALAMAT DOMISILI");
        $objPHPExcel->getActiveSheet()->SetCellValue("O3", "ALAMAT KTP");
        $objPHPExcel->getActiveSheet()->SetCellValue("P3", "NIK");
        $objPHPExcel->getActiveSheet()->SetCellValue("Q3", "TGL SWAB");

        $objPHPExcel->getActiveSheet()->getStyle("A3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("B3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("C3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("D3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("E3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("F3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("G3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("H3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("I3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("J3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("K3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("L3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("M3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("N3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("O3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("P3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("Q3")->applyFromArray($style_header);

        $data_meninggal = $this->peserta_model->query(
            "
            SELECT
                IFNULL(nama_puskesmas,'') AS nama_puskesmas,
                nama,
                b.nama_wilayah AS nama_kecamatan,
                IF(kelurahan_master_wilayah_id != '109',
                    a.nama_wilayah,
                    'Wilayah Kobar Lainnya'
                ) AS kel_desa,
                DATE_FORMAT(c.created_at,'%d/%m/%Y') AS tanggal_terkonfirmasi_rilis,
                jenis_kelamin,
                DATE_FORMAT(tanggal_lahir,'%d/%m/%Y') AS tanggal_lahir,
                IFNULL(TIMESTAMPDIFF(YEAR, `tanggal_lahir`, DATE_FORMAT(c.created_at, '%Y-%m-%d')), 0) AS umur_kena_covid,
                nomor_telepon,
                'Meninggal' AS status_konfirmasi,
                pekerjaan_inti,
                kontak_erat,
                IF(kelurahan_master_wilayah_id != '109',
                    CONCAT(alamat_domisili,' RT. ',
                        IF(master_rt_domisili_id IS NULL,
                            rt_domisili,
                            rt
                        )
                    ),
                    'Wilayah Kobar Lainnya'
                ) AS alamat_domisili_custom,
                CONCAT(alamat_ktp,' RT. ',rt_ktp) AS alamat_ktp_custom,
                nik,
                DATE_FORMAT(trx_pemeriksaan.created_at,'%d/%m/%Y') AS tanggal_swab
            FROM peserta
            INNER JOIN
            (
                SELECT *
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                )
            ) AS c ON id_peserta=c.peserta_id
            INNER JOIN trx_pemeriksaan ON id_trx_pemeriksaan=trx_pemeriksaan_id
            INNER JOIN master_wilayah AS a ON a.id_master_wilayah=kelurahan_master_wilayah_id
            INNER JOIN master_wilayah AS b ON b.kode_wilayah=a.kode_induk
            LEFT JOIN master_rt ON id_master_rt=master_rt_domisili_id
            LEFT JOIN master_puskesmas ON id_master_puskesmas=a.puskesmas_id
            WHERE c.status = '3' AND peserta.deleted_at IS NULL
            GROUP BY c.peserta_id
            "
        )->result();

        $row = 4;
        $no = 1;
        foreach ($data_meninggal as $key => $value) {

            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $no);
            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $value->nama_puskesmas);
            $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value->nama);
            $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $value->nama_kecamatan);
            $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $value->kel_desa);
            $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $value->tanggal_terkonfirmasi_rilis);
            $objPHPExcel->getActiveSheet()->SetCellValue("G" . $row, $value->jenis_kelamin);
            $objPHPExcel->getActiveSheet()->SetCellValue("H" . $row, $value->tanggal_lahir);
            $objPHPExcel->getActiveSheet()->SetCellValue("I" . $row, $value->umur_kena_covid);
            $objPHPExcel->getActiveSheet()->SetCellValue("J" . $row, $value->nomor_telepon);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                "J" . $row,
                $value->nomor_telepon,
                PHPExcel_Cell_DataType::TYPE_STRING
            );
            $objPHPExcel->getActiveSheet()->SetCellValue("K" . $row, $value->status_konfirmasi);
            $objPHPExcel->getActiveSheet()->SetCellValue("L" . $row, $value->pekerjaan_inti);
            $objPHPExcel->getActiveSheet()->SetCellValue("M" . $row, $value->kontak_erat);
            $objPHPExcel->getActiveSheet()->SetCellValue("N" . $row, $value->alamat_domisili_custom);
            $objPHPExcel->getActiveSheet()->SetCellValue("O" . $row, $value->alamat_ktp_custom);
            $objPHPExcel->getActiveSheet()->SetCellValue("P" . $row, $value->nik);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                "P" . $row,
                $value->nik,
                PHPExcel_Cell_DataType::TYPE_STRING
            );
            $objPHPExcel->getActiveSheet()->SetCellValue("Q" . $row, $value->tanggal_swab);

            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("G" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("H" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("I" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("J" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("K" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("L" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("M" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("N" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("O" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("P" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("Q" . $row)->applyFromArray($style_data_tengah);

            $row++;
            $no++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('TOTAL DEATH KONFIRM');

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }



        $objPHPExcel->createSheet();

        $objPHPExcel->setActiveSheetIndex(6);

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "LAPORAN HARIAN PASIEN MENINGGAL COVID 19 KOTAWARINGIN BARAT");
        $objPHPExcel->getActiveSheet()->mergeCells("A1:Q1");
        $objPHPExcel->getActiveSheet()->getStyle("A1:Q1")->applyFromArray($style_header_table);

        $objPHPExcel->getActiveSheet()->SetCellValue("A3", "NO");
        $objPHPExcel->getActiveSheet()->SetCellValue("B3", "PUSKESMAS");
        $objPHPExcel->getActiveSheet()->SetCellValue("C3", "NAMA");
        $objPHPExcel->getActiveSheet()->SetCellValue("D3", "KECAMATAN");
        $objPHPExcel->getActiveSheet()->SetCellValue("E3", "KEL/DESA");
        $objPHPExcel->getActiveSheet()->SetCellValue("F3", "TANGGAL RILIS KONFIRMASI");
        $objPHPExcel->getActiveSheet()->SetCellValue("G3", "JENIS KELAMIN");
        $objPHPExcel->getActiveSheet()->SetCellValue("H3", "TANGGAL LAHIR");
        $objPHPExcel->getActiveSheet()->SetCellValue("I3", "UMUR");
        $objPHPExcel->getActiveSheet()->SetCellValue("J3", "NOMOR KONTAK");
        $objPHPExcel->getActiveSheet()->SetCellValue("K3", "STATUS");
        $objPHPExcel->getActiveSheet()->SetCellValue("L3", "PEKERJAAN");
        $objPHPExcel->getActiveSheet()->SetCellValue("M3", "RIWAYAT PASIEN (Pelaku Perjalanan, Kontak Erat, Tidak ada riwayat Perjalanan dan Kontak Erat)/ Riwayat Penyakit");
        $objPHPExcel->getActiveSheet()->SetCellValue("N3", "ALAMAT DOMISILI");
        $objPHPExcel->getActiveSheet()->SetCellValue("O3", "ALAMAT KTP");
        $objPHPExcel->getActiveSheet()->SetCellValue("P3", "NIK");
        $objPHPExcel->getActiveSheet()->SetCellValue("Q3", "TGL SWAB");

        $objPHPExcel->getActiveSheet()->getStyle("A3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("B3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("C3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("D3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("E3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("F3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("G3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("H3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("I3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("J3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("K3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("L3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("M3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("N3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("O3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("P3")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("Q3")->applyFromArray($style_header);

        $data_meninggal_hari_ini = $this->peserta_model->query(
            "
            SELECT
                IFNULL(nama_puskesmas,'') AS nama_puskesmas,
                nama,
                b.nama_wilayah AS nama_kecamatan,
                IF(kelurahan_master_wilayah_id != '109',
                    a.nama_wilayah,
                    'Wilayah Kobar Lainnya'
                ) AS kel_desa,
                DATE_FORMAT(c.created_at,'%d/%m/%Y') AS tanggal_terkonfirmasi_rilis,
                jenis_kelamin,
                DATE_FORMAT(tanggal_lahir,'%d/%m/%Y') AS tanggal_lahir,
                IFNULL(TIMESTAMPDIFF(YEAR, `tanggal_lahir`, DATE_FORMAT(c.created_at, '%Y-%m-%d')), 0) AS umur_kena_covid,
                nomor_telepon,
                'Meninggal' AS status_konfirmasi,
                pekerjaan_inti,
                kontak_erat,
                IF(kelurahan_master_wilayah_id != '109',
                    CONCAT(alamat_domisili,' RT. ',
                        IF(master_rt_domisili_id IS NULL,
                            rt_domisili,
                            rt
                        )
                    ),
                    'Wilayah Kobar Lainnya'
                ) AS alamat_domisili_custom,
                CONCAT(alamat_ktp,' RT. ',rt_ktp) AS alamat_ktp_custom,
                nik,
                DATE_FORMAT(trx_pemeriksaan.created_at,'%d/%m/%Y') AS tanggal_swab
            FROM peserta
            INNER JOIN
            (
                SELECT *
                FROM trx_terkonfirmasi_rilis
                WHERE id_trx_terkonfirmasi_rilis IN (
                    SELECT MAX(id_trx_terkonfirmasi_rilis) AS id_trx_terkonfirmasi_rilis 
                    FROM trx_terkonfirmasi_rilis 
                    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                    GROUP BY peserta_id
                    ORDER BY id_trx_terkonfirmasi_rilis DESC
                )
            ) AS c ON id_peserta=c.peserta_id
            INNER JOIN trx_pemeriksaan ON id_trx_pemeriksaan=trx_pemeriksaan_id
            INNER JOIN master_wilayah AS a ON a.id_master_wilayah=kelurahan_master_wilayah_id
            INNER JOIN master_wilayah AS b ON b.kode_wilayah=a.kode_induk
            LEFT JOIN master_rt ON id_master_rt=master_rt_domisili_id
            LEFT JOIN master_puskesmas ON id_master_puskesmas=a.puskesmas_id
            WHERE c.status = '3' AND peserta.deleted_at IS NULL AND DATE_FORMAT(c.created_at,'%Y-%m-%d') = CURDATE()
            GROUP BY c.peserta_id
            "
        )->result();

        $row = 4;
        $no = 1;
        foreach ($data_meninggal_hari_ini as $key => $value) {

            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $no);
            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $value->nama_puskesmas);
            $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value->nama);
            $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, $value->nama_kecamatan);
            $objPHPExcel->getActiveSheet()->SetCellValue("E" . $row, $value->kel_desa);
            $objPHPExcel->getActiveSheet()->SetCellValue("F" . $row, $value->tanggal_terkonfirmasi_rilis);
            $objPHPExcel->getActiveSheet()->SetCellValue("G" . $row, $value->jenis_kelamin);
            $objPHPExcel->getActiveSheet()->SetCellValue("H" . $row, $value->tanggal_lahir);
            $objPHPExcel->getActiveSheet()->SetCellValue("I" . $row, $value->umur_kena_covid);
            $objPHPExcel->getActiveSheet()->SetCellValue("J" . $row, $value->nomor_telepon);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                "J" . $row,
                $value->nomor_telepon,
                PHPExcel_Cell_DataType::TYPE_STRING
            );
            $objPHPExcel->getActiveSheet()->SetCellValue("K" . $row, $value->status_konfirmasi);
            $objPHPExcel->getActiveSheet()->SetCellValue("L" . $row, $value->pekerjaan_inti);
            $objPHPExcel->getActiveSheet()->SetCellValue("M" . $row, $value->kontak_erat);
            $objPHPExcel->getActiveSheet()->SetCellValue("N" . $row, $value->alamat_domisili_custom);
            $objPHPExcel->getActiveSheet()->SetCellValue("O" . $row, $value->alamat_ktp_custom);
            $objPHPExcel->getActiveSheet()->SetCellValue("P" . $row, $value->nik);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                "P" . $row,
                $value->nik,
                PHPExcel_Cell_DataType::TYPE_STRING
            );
            $objPHPExcel->getActiveSheet()->SetCellValue("Q" . $row, $value->tanggal_swab);

            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("G" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("H" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("I" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("J" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("K" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("L" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("M" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("N" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("O" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->getStyle("P" . $row)->applyFromArray($style_data_tengah);
            $objPHPExcel->getActiveSheet()->getStyle("Q" . $row)->applyFromArray($style_data_tengah);

            $row++;
            $no++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('DEATH KONFIRM RILIS HARI INI');

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $writer->save('php://output');
        exit;
    }
}
