<?php

class Peserta_non_puskesmas_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "peserta_non_puskesmas";
        $this->primary_id = "id_peserta_non_puskesmas";
    }
}
