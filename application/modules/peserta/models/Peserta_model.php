<?php

class Peserta_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "peserta";
        $this->primary_id = "id_peserta";
    }
}
