<?php

class Detail_peserta_dalam_satu_rumah_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "detail_peserta_dalam_satu_rumah";
        $this->primary_id = "id_detail_peserta_dalam_satu_rumah";
    }
}
