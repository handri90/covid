<?php

class Status_rawat_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "status_rawat";
        $this->primary_id = "id_status_rawat";
    }
}
