<?php

class Status_rawat_non_puskesmas_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "status_rawat_non_puskesmas";
        $this->primary_id = "id_status_rawat_non_puskesmas";
    }
}
