<?php

class Jenis_pemeriksaan_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "jenis_pemeriksaan";
        $this->primary_id = "id_jenis_pemeriksaan";
    }
}
