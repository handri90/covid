<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('peserta_rilis/trx_terkonfirmasi_rilis_model', 'trx_terkonfirmasi_rilis_model');
    }

    public function index()
    {
        if ($this->session->userdata('is_logged_in') == true) {
            redirect('dashboard');
        } else {
            $this->load->view("login");
        }
    }

    public function act_login()
    {
        $username = $this->input->post('username', true);
        $pass = $this->input->post('password');
        $status = false;

        $data_user = $this->user_model->get(
            array(
                'where' => array(
                    'username' => $username
                )
            ),
            'row'
        );

        if ($data_user) {
            if (password_verify($pass, $data_user->password)) {
                $this->session->set_userdata('nama_lengkap', $data_user->nama_lengkap);
                $this->session->set_userdata('foto_user', $data_user->foto_user);
                $this->session->set_userdata('level_user_id', $data_user->level_user_id);
                $this->session->set_userdata('id_user', $data_user->id_user);
                $this->session->set_userdata('puskesmas_id', $data_user->puskesmas_id);
                $this->session->set_userdata('master_wilayah_id', $data_user->master_wilayah_id_aparat);
                $this->session->set_userdata('kode_wilayah', $data_user->kode_wilayah);
                $this->session->set_userdata('is_logged_in', true);
                $status = true;
            } else {
                $status = false;
            }
        } else {
            $status = false;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function act_logout()
    {
        if (isset($_SESSION['is_logged_in']) && $_SESSION['is_logged_in'] === true) {

            // remove session datas
            foreach ($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }

            // user logout ok
            redirect('login');
        } else {

            redirect('login');
        }
    }

    public function get_konfirmasi_covid()
    {
        $table = "trx_terkonfirmasi_rilis";

        $wh_1 = "";

        $tanggal = date("Y-m-d");

        $wilayah = $this->trx_terkonfirmasi_rilis_model->query("
        SELECT master_wilayah.*,
        IFNULL(a.jumlah_kasus_positif,0) AS jumlah_kasus_positif,
        (IFNULL(a.jumlah_kasus_positif,0) + IFNULL(h.jumlah_kasus_positif_antigen,0)) AS jumlah_kasus_positif_pcr_antigen,
        IFNULL(h.jumlah_kasus_positif_antigen,0) AS jumlah_kasus_positif_antigen,
        IFNULL(b.jumlah_kasus_sembuh,0) AS jumlah_kasus_sembuh,
        IFNULL(i.jumlah_kasus_sembuh_antigen,0) AS jumlah_kasus_sembuh_antigen,
        IFNULL(c.jumlah_kasus_meninggal,0) AS jumlah_kasus_meninggal,
        IFNULL(d.jumlah_kasus_aktif,0) AS jumlah_kasus_aktif,
        IFNULL(e.jumlah_kasus_aktif_today,0) AS jumlah_kasus_aktif_today,
        IFNULL(f.jumlah_kasus_sembuh_today,0) AS jumlah_kasus_sembuh_today,
        IFNULL(g.jumlah_kasus_meninggal_today,0) AS jumlah_kasus_meninggal_today
        FROM master_wilayah
        LEFT JOIN (
        SELECT COUNT(id_peserta) AS jumlah_kasus_positif,kode_induk
            FROM (
                SELECT peserta.*,kode_induk
                FROM master_wilayah
                INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
                INNER JOIN trx_terkonfirmasi_rilis ON id_peserta=peserta_id
                WHERE STATUS = '1' AND trx_terkonfirmasi_rilis.deleted_at IS NULL
                GROUP BY peserta_id
            ) AS tbl
            GROUP BY kode_induk
        ) AS a ON a.kode_induk=kode_wilayah
        LEFT JOIN (
		SELECT COUNT(id_peserta) AS jumlah_kasus_sembuh,kode_induk
		FROM master_wilayah
		INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
		WHERE id_peserta IN (
			SELECT peserta_id
			FROM trx_terkonfirmasi_rilis
			WHERE id_trx_terkonfirmasi_rilis IN 
			(
			    SELECT MAX(id_trx_terkonfirmasi_rilis)
			    FROM trx_terkonfirmasi_rilis
			    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
			    GROUP BY peserta_id
			)
			AND STATUS = '2'
			AND trx_terkonfirmasi_rilis.deleted_at IS NULL
		)
		GROUP BY kode_induk
        ) AS b ON b.kode_induk=kode_wilayah
        LEFT JOIN (
		SELECT COUNT(id_peserta) AS jumlah_kasus_meninggal,kode_induk
		FROM master_wilayah
		INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
		WHERE id_peserta IN (
			SELECT peserta_id
			FROM trx_terkonfirmasi_rilis
			WHERE id_trx_terkonfirmasi_rilis IN 
			(
			    SELECT MAX(id_trx_terkonfirmasi_rilis)
			    FROM trx_terkonfirmasi_rilis
			    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
			    GROUP BY peserta_id
			)
			AND STATUS = '3'
			AND trx_terkonfirmasi_rilis.deleted_at IS NULL
		)
		GROUP BY kode_induk
        ) AS c ON c.kode_induk=kode_wilayah
        LEFT JOIN (
		SELECT COUNT(id_peserta) AS jumlah_kasus_aktif,kode_induk
		FROM master_wilayah
		INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
		WHERE id_peserta IN (
			SELECT peserta_id
			FROM trx_terkonfirmasi_rilis
			WHERE id_trx_terkonfirmasi_rilis IN 
			(
			    SELECT MAX(id_trx_terkonfirmasi_rilis)
			    FROM trx_terkonfirmasi_rilis
			    WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
			    GROUP BY peserta_id
			)
			AND STATUS = '1'
			AND trx_terkonfirmasi_rilis.deleted_at IS NULL
		)
		GROUP BY kode_induk
        ) AS d ON d.kode_induk=kode_wilayah
        LEFT JOIN (
            SELECT COUNT(id_peserta) AS jumlah_kasus_aktif_today,kode_induk
                FROM master_wilayah
                INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
                WHERE id_peserta IN (
                    SELECT peserta_id
                    FROM trx_terkonfirmasi_rilis
                    WHERE id_trx_terkonfirmasi_rilis IN (
                        SELECT MAX(id_trx_terkonfirmasi_rilis)
                        FROM trx_terkonfirmasi_rilis
                        WHERE trx_terkonfirmasi_rilis.deleted_at IS NULL
                        GROUP BY peserta_id
                    )
                    AND STATUS = '1'
                    AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at,'%Y-%m-%d') = CURDATE()
                    AND trx_terkonfirmasi_rilis.deleted_at IS NULL
                    )
                GROUP BY kode_induk
            ) AS e ON e.kode_induk=kode_wilayah
            LEFT JOIN (
            SELECT COUNT(id_peserta) AS jumlah_kasus_sembuh_today,kode_induk
                FROM (
                    SELECT peserta.*,kode_induk
                    FROM master_wilayah
                    INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
                    INNER JOIN trx_terkonfirmasi_rilis ON id_peserta=peserta_id
                    WHERE STATUS = '2' AND trx_terkonfirmasi_rilis.deleted_at IS NULL AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at,'%Y-%m-%d') = CURDATE()
                    GROUP BY peserta_id
                ) AS tbl
                GROUP BY kode_induk
            ) AS f ON f.kode_induk=kode_wilayah
            LEFT JOIN (
            SELECT COUNT(id_peserta) AS jumlah_kasus_meninggal_today,kode_induk
                FROM (
                    SELECT peserta.*,kode_induk
                    FROM master_wilayah
                    INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
                    INNER JOIN trx_terkonfirmasi_rilis ON id_peserta=peserta_id
                    WHERE STATUS = '3' AND trx_terkonfirmasi_rilis.deleted_at IS NULL AND DATE_FORMAT(trx_terkonfirmasi_rilis.created_at,'%Y-%m-%d') = CURDATE()
                    GROUP BY peserta_id
                ) AS tbl
                GROUP BY kode_induk
            ) AS g ON g.kode_induk=kode_wilayah
            LEFT JOIN (
            SELECT COUNT(id_peserta) AS jumlah_kasus_positif_antigen,kode_induk
                FROM (
                SELECT peserta.*,kode_induk
                FROM master_wilayah
                INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
                INNER JOIN trx_pemeriksaan ON id_peserta=peserta_id
                WHERE trx_pemeriksaan.deleted_at IS NULL AND jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '3' AND trx_pemeriksaan.from_excel_import_faskes = '2' AND DATE_FORMAT(trx_pemeriksaan.created_at,'%Y-%m-%d') >= '2021-08-11'
                GROUP BY peserta_id
                ) AS tbl
                GROUP BY kode_induk
            ) AS h ON h.kode_induk=kode_wilayah
            LEFT JOIN (
                SELECT COUNT(id_peserta) AS jumlah_kasus_sembuh_antigen,kode_induk
                FROM master_wilayah
                INNER JOIN peserta ON kelurahan_master_wilayah_id=id_master_wilayah AND peserta.`deleted_at` IS NULL
                WHERE id_peserta IN (
                    SELECT peserta_id
                    FROM trx_pemeriksaan
                    WHERE id_trx_pemeriksaan IN
                    (
                        SELECT MAX(id_trx_pemeriksaan)
                        FROM trx_pemeriksaan
                        WHERE peserta_id IN
                        (
                            SELECT peserta_id
                            FROM trx_pemeriksaan
                            WHERE jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '3' AND from_excel_import_faskes = '2'
                            AND DATE_FORMAT(created_at,'%Y-%m-%d') >= '2021-08-11'
                            GROUP BY peserta_id
                        )
                        AND jenis_pemeriksaan_id = '2' AND hasil_pemeriksaan_id = '4' AND from_excel_import_faskes = '2' AND DATE_FORMAT(created_at,'%Y-%m-%d') >= '2021-08-11'
                        GROUP BY peserta_id
                    )
                )
                GROUP BY kode_induk
            ) AS i ON i.kode_induk=kode_wilayah
        WHERE klasifikasi = 'KEC'
        ")->result();

        $templist = array();
        foreach ($wilayah as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
