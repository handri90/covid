<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Vaksin extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('master_puskesmas_model');
        $this->load->model('user_vaksin_model');
        $this->load->model('daftar_vaksin_model');
        $this->load->model('peserta/master_wilayah_model', 'master_wilayah_model');
        $this->load->model('migrate_rt_domisili/master_rt_model', 'master_rt_model');
        $this->load->model('peserta_vaksin/jenis_vaksin_model', 'jenis_vaksin_model');
    }

    public function listdaftarvaksin_get()
    {
        $id_user_created = $this->get("id_user_created");
        $daftar_vaksin = $this->daftar_vaksin_model->get(
            array(
                "fields" => "
                id_daftar_vaksin,nama,nik,DATE_FORMAT(tanggal_lahir,'%d-%m-%Y') AS tanggal_lahir,no_hp,alamat,
                IFNULL(pertanyaan_1,'') AS pertanyaan_1,
                IFNULL(pertanyaan_2,'') AS pertanyaan_2,
                IFNULL(pertanyaan_3,'') AS pertanyaan_3,
                IFNULL(pertanyaan_4,'') AS pertanyaan_4,
                IFNULL(pertanyaan_5,'') AS pertanyaan_5,	
                IFNULL(pertanyaan_6,'') AS pertanyaan_6,
                IFNULL(pertanyaan_7,'') AS pertanyaan_7,
                IFNULL(pertanyaan_8,'') AS pertanyaan_8,
                IFNULL(pertanyaan_9,'') AS pertanyaan_9,
                IFNULL(pertanyaan_10,'') AS pertanyaan_10,
                IFNULL(pertanyaan_11,'') AS pertanyaan_11,
                IFNULL(pertanyaan_12,'') AS pertanyaan_12,
                kategori_ticket,usia_kehamilan,kelurahan_master_wilayah_id,master_rt_id,nama_wilayah,rt,
                IFNULL(hasil_skrining_petugas,'') AS hasil_skrining_petugas,
                IFNULL(tempat_pelaksanaan,'') AS tempat_pelaksanaan, nama_puskesmas,
                IFNULL(DATE_FORMAT(jam_vaksin,'%H:%i'),'') AS jam_vaksin,
                IFNULL(tanggal_vaksin,'') AS tanggal_vaksin,
                IFNULL(is_confirm,'') AS is_confirm,IFNULL(no_antrian,'') AS no_antrian,
                pilihan_vaksin,
                IF(hasil_skrining_petugas='1',
                    UNIX_TIMESTAMP(DATE_FORMAT(
                        IF(before_after_schedule_vaksin = '1',
                            IF(satuan_interval = 'HOUR',
                                DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                                DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                            ),
                            IF(satuan_interval = 'HOUR',
                                DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                                DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                            )
                        ), '%Y-%m-%d %H:%i:%s')
                    ),''
                ) AS timestamp_tanggal_jam_vaksin,
                IF(hasil_skrining_petugas='1',
                    FROM_UNIXTIME(UNIX_TIMESTAMP(DATE_FORMAT(
                        IF(before_after_schedule_vaksin = '1',
                            IF(satuan_interval = 'HOUR',
                                DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                                DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                            ),
                            IF(satuan_interval = 'HOUR',
                                DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                                DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                            )
                        ), '%Y-%m-%d %H:%i:%s')
                    )),''
                ) AS tanggal_terakhir_konfirmasi,
                IFNULL(is_undangan_cadangan,'') AS is_undangan_cadangan,
                IFNULL(DATE_FORMAT(tanggal_vaksin_1,'%d-%m-%Y'),'') AS tanggal_vaksin_1,
                IFNULL(tanggal_vaksin_1,'') AS tanggal_vaksin_1_original,
                IFNULL(jenis_vaksin_id_vaksin_1,'') AS jenis_vaksin_pertama,
                IFNULL(a.nama_vaksin,'') AS nama_vaksin",
                "join" => array(
                    "master_wilayah" => "id_master_wilayah=kelurahan_master_wilayah_id",
                    "master_rt" => "id_master_rt=master_rt_id",
                    "master_puskesmas" => "puskesmas_id=id_master_puskesmas"
                ),
                "left_join" => array(
                    "jenis_vaksin AS a" => "a.id_jenis_vaksin=jenis_vaksin_id",
                    "jenis_vaksin AS b" => "b.id_jenis_vaksin=jenis_vaksin_id_vaksin_1",
                    "jadwal_vaksin" => "id_jadwal_vaksin=jadwal_vaksin_id",
                ),
                "where" => array(
                    "daftar_vaksin.id_user_created" => $id_user_created
                ),
                "order_by" => array(
                    "daftar_vaksin.created_at" => "DESC"
                )
            )
        );

        $templist = array();
        foreach ($daftar_vaksin as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            if ($row->hasil_skrining_petugas == "1") {
                $tanggal_terakhir_konfirmasi = date("Y-m-d", strtotime($row->tanggal_terakhir_konfirmasi));
                $jam_terakhir_konfirmasi = date("H:i", strtotime($row->tanggal_terakhir_konfirmasi));

                $templist[$key]['tanggal_terakhir_konfirmasi'] = $this->longdate_indo($tanggal_terakhir_konfirmasi) . " " . $jam_terakhir_konfirmasi;
            } else {
                $templist[$key]['tanggal_terakhir_konfirmasi'] = "";
            }

            if ($row->tanggal_vaksin_1_original != "") {
                $tanggal_vaksin_1_original = date("Y-m-d", strtotime($row->tanggal_vaksin_1_original));

                $templist[$key]['tanggal_vaksin_pertama_custom'] = $this->longdate_indo($tanggal_vaksin_1_original);
            } else {
                $templist[$key]['tanggal_vaksin_pertama_custom'] = "";
            }

            $templist[$key]['timestamp_tanggal_jam_now'] = strval(strtotime(date("Y-m-d H:i:s")));
            $templist[$key]['tanggal_jam_server'] = date("Y-m-d H:i:s");

            $templist[$key]['tanggal_vaksin'] = $row->hasil_skrining_petugas == '1' ? $this->longdate_indo($row->tanggal_vaksin) : "";
        }

        $this->response(array("data" => $templist), REST_Controller::HTTP_OK);
    }

    public function daftarvaksinbyid_get()
    {
        $id = $this->get("id_daftar_vaksin");
        $daftar_vaksin = $this->daftar_vaksin_model->get(
            array(
                "fields" => "
                id_daftar_vaksin,nama,nik,DATE_FORMAT(tanggal_lahir,'%d-%m-%Y') AS tanggal_lahir,no_hp,alamat,
                IFNULL(pertanyaan_1,'') AS pertanyaan_1,
                IFNULL(pertanyaan_2,'') AS pertanyaan_2,
                IFNULL(pertanyaan_3,'') AS pertanyaan_3,
                IFNULL(pertanyaan_4,'') AS pertanyaan_4,
                IFNULL(pertanyaan_5,'') AS pertanyaan_5,	
                IFNULL(pertanyaan_6,'') AS pertanyaan_6,
                IFNULL(pertanyaan_7,'') AS pertanyaan_7,
                IFNULL(pertanyaan_8,'') AS pertanyaan_8,
                IFNULL(pertanyaan_9,'') AS pertanyaan_9,
                IFNULL(pertanyaan_10,'') AS pertanyaan_10,
                IFNULL(pertanyaan_11,'') AS pertanyaan_11,
                IFNULL(pertanyaan_12,'') AS pertanyaan_12,
                kategori_ticket,usia_kehamilan,kelurahan_master_wilayah_id,master_rt_id,nama_wilayah,rt,
                IFNULL(hasil_skrining_petugas,'') AS hasil_skrining_petugas,
                IFNULL(tempat_pelaksanaan,'') AS tempat_pelaksanaan, nama_puskesmas, 
                IFNULL(DATE_FORMAT(jam_vaksin,'%H:%i'),'') AS jam_vaksin,
                IFNULL(tanggal_vaksin,'') AS tanggal_vaksin,
                IFNULL(is_confirm,'') AS is_confirm,IFNULL(no_antrian,'') AS no_antrian,pilihan_vaksin,
                IF(hasil_skrining_petugas='1',
                    UNIX_TIMESTAMP(DATE_FORMAT(
                        IF(before_after_schedule_vaksin = '1',
                            IF(satuan_interval = 'HOUR',
                                DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                                DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                            ),
                            IF(satuan_interval = 'HOUR',
                                DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                                DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                            )
                        ), '%Y-%m-%d %H:%i:%s')
                    ),''
                ) AS timestamp_tanggal_jam_vaksin,
                IF(hasil_skrining_petugas='1',
                    FROM_UNIXTIME(UNIX_TIMESTAMP(DATE_FORMAT(
                        IF(before_after_schedule_vaksin = '1',
                            IF(satuan_interval = 'HOUR',
                                DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                                DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                            ),
                            IF(satuan_interval = 'HOUR',
                                DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                                DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                            )
                        ), '%Y-%m-%d %H:%i:%s')
                    )),''
                ) AS tanggal_terakhir_konfirmasi,
                IFNULL(is_undangan_cadangan,'') AS is_undangan_cadangan,
                IFNULL(DATE_FORMAT(tanggal_vaksin_1,'%d-%m-%Y'),'') AS tanggal_vaksin_1,
                IFNULL(tanggal_vaksin_1,'') AS tanggal_vaksin_1_original,
                IFNULL(jenis_vaksin_id_vaksin_1,'') AS jenis_vaksin_pertama,
                IFNULL(a.nama_vaksin,'') AS nama_vaksin",
                "join" => array(
                    "master_wilayah" => "id_master_wilayah=kelurahan_master_wilayah_id",
                    "master_rt" => "id_master_rt=master_rt_id",
                    "master_puskesmas" => "puskesmas_id=id_master_puskesmas"
                ),
                "left_join" => array(
                    "jenis_vaksin AS a" => "a.id_jenis_vaksin=jenis_vaksin_id",
                    "jenis_vaksin AS b" => "b.id_jenis_vaksin=jenis_vaksin_id_vaksin_1",
                    "jadwal_vaksin" => "id_jadwal_vaksin=jadwal_vaksin_id",
                ),
                "where" => array(
                    "id_daftar_vaksin" => $id
                )
            ),
            "row"
        );

        if ($daftar_vaksin->hasil_skrining_petugas == "1") {
            $tanggal_terakhir_konfirmasi = date("Y-m-d", strtotime($daftar_vaksin->tanggal_terakhir_konfirmasi));
            $jam_terakhir_konfirmasi = date("H:i", strtotime($daftar_vaksin->tanggal_terakhir_konfirmasi));


            $daftar_vaksin->tanggal_terakhir_konfirmasi = $this->longdate_indo($tanggal_terakhir_konfirmasi) . " " . $jam_terakhir_konfirmasi;
        } else {
            $daftar_vaksin->tanggal_terakhir_konfirmasi = "";
        }

        if ($daftar_vaksin->tanggal_vaksin_1_original != "") {
            $tanggal_vaksin_1_original = date("Y-m-d", strtotime($daftar_vaksin->tanggal_vaksin_1_original));

            $daftar_vaksin->tanggal_vaksin_pertama_custom = $this->longdate_indo($tanggal_vaksin_1_original);
        } else {
            $daftar_vaksin->tanggal_vaksin_pertama_custom = "";
        }

        $daftar_vaksin->timestamp_tanggal_jam_now = strval(strtotime(date("Y-m-d H:i:s")));
        $daftar_vaksin->tanggal_jam_server = date("Y-m-d H:i:s");

        $daftar_vaksin->tanggal_vaksin = $daftar_vaksin->tanggal_vaksin ? $this->longdate_indo($daftar_vaksin->tanggal_vaksin) : "";

        $this->response(array("data" => $daftar_vaksin), REST_Controller::HTTP_OK);
    }

    public function userprofilebyid_get()
    {
        $id_user = $this->get("id_user");
        $data_user = $this->user_vaksin_model->get(
            array(
                "fields" => "id_user_vaksin,nama_lengkap,username,email,no_hp",
                "where" => array(
                    "id_user_vaksin" => $id_user
                )
            ),
            "row"
        );

        $this->response(array("data" => $data_user), REST_Controller::HTTP_OK);
    }

    public function cekemailforgotpassword_get()
    {
        $email = $this->get("email");
        $data_user = $this->user_vaksin_model->get(
            array(
                "where" => array(
                    "email" => $email
                )
            ),
            "row"
        );

        $this->response(array("data" => $data_user), REST_Controller::HTTP_OK);
    }

    public function cekkodeotp_get()
    {
        $id_user_vaksin = $this->get("id_user_vaksin");
        $kode_otp = $this->get("kode_otp");

        $data_user = $this->user_vaksin_model->get(
            array(
                "where" => array(
                    "kode_otp" => $kode_otp,
                    "id_user_vaksin" => $id_user_vaksin,
                )
            ),
            "row"
        );

        $this->response(array("data" => $data_user), REST_Controller::HTTP_OK);
    }

    public function ceknik_get()
    {
        $nik = $this->get("nik");
        $data_user = $this->daftar_vaksin_model->get(
            array(
                "where" => array(
                    "nik" => $nik
                )
            ),
            "row"
        );
        // $data_user = $this->daftar_vaksin_model->query(
        //     "
        //     SELECT *
        //     FROM
        //     (
        //         SELECT UNIX_TIMESTAMP(),UNIX_TIMESTAMP(DATE_FORMAT(DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL 1 DAY),'%Y-%m-%d %H:%i:%s')) AS timestamp_jadwal_vaksin,daftar_vaksin.* 
        //         FROM daftar_vaksin
        //         LEFT JOIN jadwal_vaksin ON id_jadwal_vaksin=jadwal_vaksin_id
        //         WHERE nik = '" . $nik . "' AND daftar_vaksin.deleted_at IS NULL
        //         ORDER BY id_daftar_vaksin DESC
        //         LIMIT 1
        //     ) AS tbl
        //     WHERE IF(hasil_skrining_petugas IS NOT NULL OR hasil_skrining_petugas != '',(IF(jadwal_vaksin_id IS NOT NULL OR jadwal_vaksin_id != '',UNIX_TIMESTAMP() < timestamp_jadwal_vaksin,hasil_skrining_petugas != '3')),1)
        //     "
        // )->row();

        $this->response(array("data" => $data_user), REST_Controller::HTTP_OK);
    }

    public function ceknikexceptid_get()
    {
        $nik = $this->get("nik");
        $idDaftarVaksin = $this->get("idDaftarVaksin");
        $data_user = $this->daftar_vaksin_model->get(
            array(
                "where" => array(
                    "nik" => $nik
                ),
                "where_false" => "id_daftar_vaksin != " . $idDaftarVaksin
            ),
            "row"
        );
        // $data_user = $this->daftar_vaksin_model->query(
        //     "
        //     SELECT *
        //     FROM
        //     (
        //         SELECT UNIX_TIMESTAMP(),UNIX_TIMESTAMP(DATE_FORMAT(DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL 1 DAY),'%Y-%m-%d %H:%i:%s')) AS timestamp_jadwal_vaksin,daftar_vaksin.* 
        //         FROM daftar_vaksin
        //         LEFT JOIN jadwal_vaksin ON id_jadwal_vaksin=jadwal_vaksin_id
        //         WHERE nik = '" . $nik . "' AND daftar_vaksin.deleted_at IS NULL
        //         ORDER BY id_daftar_vaksin DESC
        //         LIMIT 1
        //     ) AS tbl
        //     WHERE IF(hasil_skrining_petugas IS NOT NULL OR hasil_skrining_petugas != '',(IF(jadwal_vaksin_id IS NOT NULL OR jadwal_vaksin_id != '',UNIX_TIMESTAMP() < timestamp_jadwal_vaksin,hasil_skrining_petugas != '3')),1) AND id_daftar_vaksin != '" . $idDaftarVaksin . "'
        //     "
        // )->row();

        $this->response(array("data" => $data_user), REST_Controller::HTTP_OK);
    }

    public function savevaksin_post()
    {
        $tanggal_expl = explode("-", $this->post('tanggal_lahir'));
        $tanggal_lahir = $tanggal_expl[2] . "-" . $tanggal_expl[1] . "-" . $tanggal_expl[0];

        if ($this->post('tanggal_vaksin_pertama') != "") {
            $tanggal_vaksin_pertama_expl = explode("-", $this->post('tanggal_vaksin_pertama'));
            $tanggal_vaksin_pertama = $tanggal_vaksin_pertama_expl[2] . "-" . $tanggal_vaksin_pertama_expl[1] . "-" . $tanggal_vaksin_pertama_expl[0];
        } else {
            $tanggal_vaksin_pertama = NULL;
        }

        $data = array( // Automatically generated by the model
            'nama' => $this->post('nama'),
            'nik' => $this->post('nik'),
            'tanggal_lahir' => $tanggal_lahir,
            'no_hp' => $this->post('no_hp'),
            'alamat' => $this->post('alamat'),
            'pertanyaan_1' => ($this->post('pertanyaan_1') == "false" ? "1" : ($this->post('pertanyaan_1') == "true" ? "2" : NULL)),
            'pertanyaan_2' => ($this->post('pertanyaan_2') == "false" ? "1" : ($this->post('pertanyaan_2') == "true" ? "2" : NULL)),
            'pertanyaan_3' => ($this->post('pertanyaan_3') == "false" ? "1" : ($this->post('pertanyaan_3') == "true" ? "2" : NULL)),
            'pertanyaan_4' => ($this->post('pertanyaan_4') == "false" ? "1" : ($this->post('pertanyaan_4') == "true" ? "2" : NULL)),
            'pertanyaan_5' => ($this->post('pertanyaan_5') == "false" ? "1" : ($this->post('pertanyaan_5') == "true" ? "2" : NULL)),
            'pertanyaan_6' => ($this->post('pertanyaan_6') == "false" ? "1" : ($this->post('pertanyaan_6') == "true" ? "2" : NULL)),
            'pertanyaan_7' => ($this->post('pertanyaan_7') == "false" ? "1" : ($this->post('pertanyaan_7') == "true" ? "2" : NULL)),
            'pertanyaan_8' => ($this->post('pertanyaan_8') == "false" ? "1" : ($this->post('pertanyaan_8') == "true" ? "2" : NULL)),
            'pertanyaan_9' => ($this->post('pertanyaan_9') == "false" ? "1" : ($this->post('pertanyaan_9') == "true" ? "2" : NULL)),
            'pertanyaan_10' => ($this->post('pertanyaan_10') == "false" ? "1" : ($this->post('pertanyaan_10') == "true" ? "2" : NULL)),
            'pertanyaan_11' => ($this->post('pertanyaan_11') == "false" ? "1" : ($this->post('pertanyaan_11') == "true" ? "2" : NULL)),
            'pertanyaan_12' => ($this->post('pertanyaan_12') == "false" ? "1" : ($this->post('pertanyaan_12') == "true" ? "2" : NULL)),
            'kategori_ticket' => $this->post('kategori_ticket'),
            'usia_kehamilan' => $this->post('usia_kehamilan'),
            'kelurahan_master_wilayah_id' => $this->post('id_kelurahan_master_wilayah'),
            'master_rt_id' => $this->post('id_master_rt'),
            'pilihan_vaksin' => $this->post('pilihan_vaksin'),
            'id_user_created' => $this->post('id_user_created'),
            'created_at' => date('Y-m-d H:i:s'),
            'tanggal_vaksin_1' => $tanggal_vaksin_pertama,
            'jenis_vaksin_id_vaksin_1' => $this->post('jenis_vaksin_pertama') != "" && $this->post('jenis_vaksin_pertama') != "null" ? $this->post('jenis_vaksin_pertama') : NULL,
        );

        $status = $this->daftar_vaksin_model->save($data);

        if ($status) {
            $this->response([
                'status' => true
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function savecoba_post()
    {
        $data = array( // Automatically generated by the model
            'nama' => $this->post('nama')
        );

        $status = $this->coba_model->save($data);

        if ($status) {
            $this->response([
                'status' => true
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function updatevaksin_patch()
    {
        $id_daftar_vaksin = $this->patch("id_daftar_vaksin");
        $tanggal_expl = explode("-", $this->patch('tanggal_lahir'));
        $tanggal_lahir = $tanggal_expl[2] . "-" . $tanggal_expl[1] . "-" . $tanggal_expl[0];

        if ($this->patch('tanggal_vaksin_pertama') != "") {
            $tanggal_vaksin_pertama_expl = explode("-", $this->patch('tanggal_vaksin_pertama'));
            $tanggal_vaksin_pertama = $tanggal_vaksin_pertama_expl[2] . "-" . $tanggal_vaksin_pertama_expl[1] . "-" . $tanggal_vaksin_pertama_expl[0];
        } else {
            $tanggal_vaksin_pertama = NULL;
        }
        $data = array( // Automatically generated by the model
            'nama' => $this->patch('nama'),
            'nik' => $this->patch('nik'),
            'tanggal_lahir' => $tanggal_lahir,
            'no_hp' => $this->patch('no_hp'),
            'alamat' => $this->patch('alamat'),
            'pertanyaan_1' => ($this->patch('pertanyaan_1') ? ($this->patch('pertanyaan_1') == "false" ? "1" : "2") : NULL),
            'pertanyaan_2' => ($this->patch('pertanyaan_2') ? ($this->patch('pertanyaan_2') == "false" ? "1" : "2") : NULL),
            'pertanyaan_3' => ($this->patch('pertanyaan_3') ? ($this->patch('pertanyaan_3') == "false" ? "1" : "2") : NULL),
            'pertanyaan_4' => ($this->patch('pertanyaan_4') ? ($this->patch('pertanyaan_4') == "false" ? "1" : "2") : NULL),
            'pertanyaan_5' => ($this->patch('pertanyaan_5') ? ($this->patch('pertanyaan_5') == "false" ? "1" : "2") : NULL),
            'pertanyaan_6' => ($this->patch('pertanyaan_6') ? ($this->patch('pertanyaan_6') == "false" ? "1" : "2") : NULL),
            'pertanyaan_7' => ($this->patch('pertanyaan_7') ? ($this->patch('pertanyaan_7') == "false" ? "1" : "2") : NULL),
            'pertanyaan_8' => ($this->patch('pertanyaan_8') ? ($this->patch('pertanyaan_8') == "false" ? "1" : "2") : NULL),
            'pertanyaan_9' => ($this->patch('pertanyaan_9') ? ($this->patch('pertanyaan_9') == "false" ? "1" : "2") : NULL),
            'pertanyaan_10' => ($this->patch('pertanyaan_10') ? ($this->patch('pertanyaan_10') == "false" ? "1" : "2") : NULL),
            'pertanyaan_11' => ($this->patch('pertanyaan_11') ? ($this->patch('pertanyaan_11') == "false" ? "1" : "2") : NULL),
            'pertanyaan_12' => ($this->patch('pertanyaan_12') ? ($this->patch('pertanyaan_12') == "false" ? "1" : "2") : NULL),
            'kategori_ticket' => $this->patch('kategori_ticket'),
            'usia_kehamilan' => $this->patch('usia_kehamilan'),
            'kelurahan_master_wilayah_id' => $this->patch('id_kelurahan_master_wilayah'),
            'master_rt_id' => $this->patch('id_master_rt'),
            'pilihan_vaksin' => $this->patch('pilihan_vaksin'),
            'id_user_updated' => $this->patch('id_user_updated'),
            'updated_at' => date('Y-m-d H:i:s'),
            'tanggal_vaksin_1' => $tanggal_vaksin_pertama,
            'jenis_vaksin_id_vaksin_1' => $this->patch('jenis_vaksin_pertama') != "" ? $this->patch('jenis_vaksin_pertama') : NULL,
        );

        $status = $this->daftar_vaksin_model->edit($id_daftar_vaksin, $data);

        if ($status) {
            $this->response([
                'status' => true
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function changepassword_patch()
    {
        $id_user_vaksin = $this->patch("id_user_vaksin");
        $password = $this->patch("new_password");
        $data = array( // Automatically generated by the model
            'password' => password_hash($password, PASSWORD_BCRYPT, array('cost' => 12)),
            'updated_at' => date('Y-m-d H:i:s'),
        );

        $status = $this->user_vaksin_model->edit($id_user_vaksin, $data);

        if ($status) {
            $this->response([
                'status' => true
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function setislogintrue_patch()
    {
        $id_user = $this->patch("id_user");
        $data = array( // Automatically generated by the model
            'is_login' => "2",
            'updated_at' => date('Y-m-d H:i:s'),
        );

        $status = $this->user_vaksin_model->edit($id_user, $data);

        if ($status) {
            $this->response([
                'status' => true
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function setisloginfalse_patch()
    {
        $id_user = $this->patch("id_user");
        $data = array( // Automatically generated by the model
            'is_login' => "1",
            'updated_at' => date('Y-m-d H:i:s'),
        );

        $status = $this->user_vaksin_model->edit($id_user, $data);

        if ($status) {
            $this->response([
                'status' => true
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function deletedaftarvaksin_post()
    {
        $id_daftar_vaksin = $this->post("id_daftar_vaksin");

        $data = array(
            "deleted_at" => date('Y-m-d H:i:s'),
        );

        $status = $this->daftar_vaksin_model->edit($id_daftar_vaksin, $data);

        if ($status) {
            $this->response([
                'status' => true
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function confirmkehadiranvaksin_post()
    {
        $id_daftar_vaksin = $this->post("id_daftar_vaksin");
        $status_kehadiran = $this->post("status_kehadiran");

        $data_master = $this->daftar_vaksin_model->get(
            array(
                "fields" => "
                IF(hasil_skrining_petugas='1',
                    UNIX_TIMESTAMP(DATE_FORMAT(
                        IF(before_after_schedule_vaksin = '1',
                            IF(satuan_interval = 'HOUR',
                                DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                                DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                            ),
                            IF(satuan_interval = 'HOUR',
                                DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                                DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                            )
                        ), '%Y-%m-%d %H:%i:%s')
                    ),''
                ) AS timestamp_tanggal_jam_vaksin,
                UNIX_TIMESTAMP() AS timestamp_tanggal_jam_now
                ",
                "join" => array(
                    "jadwal_vaksin" => "id_jadwal_vaksin=jadwal_vaksin_id"
                ),
                "where" => array(
                    "id_daftar_vaksin" => $id_daftar_vaksin
                )
            ),
            "row"
        );

        $timestamp_tanggal_jam_now = strtotime(date("Y-m-d H:i:s"));

        if ($status_kehadiran == "1" && $timestamp_tanggal_jam_now > $data_master->timestamp_tanggal_jam_vaksin) {
            $status = false;
        } else {
            $data = array(
                "is_confirm" => $status_kehadiran,
                "updated_at" => date('Y-m-d H:i:s'),
            );

            $status = $this->daftar_vaksin_model->edit($id_daftar_vaksin, $data);
        }

        if ($status) {
            $this->response([
                'status' => true
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function getdatakelurahan_get()
    {
        $filter = $this->get("filter");
        $data_user = $this->master_wilayah_model->get(
            array(
                "fields" => "master_wilayah.*,concat(klasifikasi,' - ',nama_wilayah) as nama_wilayah",
                "where" => array(
                    "id_master_wilayah != " => "109",
                    "nama_wilayah LIKE " => "%${filter}%",
                ),
                "where_false" => "klasifikasi NOT IN ('PROV','KAB','KEC')",
                "order_by_false" => 'field (klasifikasi,"KEC","KEL","DESA"),nama_wilayah'
            )
        );

        $this->response(array("data" => $data_user), REST_Controller::HTTP_OK);
    }

    public function getdatart_get()
    {
        $id_master_wilayah = $this->get("id_master_rt");
        $filter = $this->get("filter");
        $data_rt = $this->master_rt_model->get(
            array(
                "where" => array(
                    "master_wilayah_id" => $id_master_wilayah,
                    "rt LIKE" => "%${filter}%",
                ),
                "order_by" => array(
                    "rt" => "ASC"
                )
            )
        );

        $this->response(array("data" => $data_rt), REST_Controller::HTTP_OK);
    }

    public function cekuser_get()
    {
        $username = $this->get("username");
        $password = $this->get("password");
        $token = $this->get("token");

        $data_user = $this->user_vaksin_model->get(
            array(
                'where' => array(
                    'username' => $username
                )
            ),
            'row'
        );

        if ($data_user) {
            if (password_verify($password, $data_user->password)) {
                $data_update_is_login = array(
                    "is_login" => true
                );

                $this->user_vaksin_model->edit($data_user->id_user_vaksin, $data_update_is_login);

                if ($data_user != $token) {

                    $data_update = array(
                        "token" => $token
                    );

                    $this->user_vaksin_model->edit($data_user->id_user_vaksin, $data_update);
                }
                $this->response(array("data" => $data_user), REST_Controller::HTTP_OK);
            } else {
                $this->response(array("data" => []), REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(array("data" => []), REST_Controller::HTTP_OK);
        }
    }

    public function cekuserv2_get()
    {
        $username = $this->get("username");
        $password = $this->get("password");

        $data_user = $this->user_vaksin_model->get(
            array(
                'where' => array(
                    'username' => $username
                )
            ),
            'row'
        );

        if ($data_user) {
            if (password_verify($password, $data_user->password)) {
                $data_update_is_login = array(
                    "is_login" => true
                );

                $this->user_vaksin_model->edit($data_user->id_user_vaksin, $data_update_is_login);

                $this->response(array("data" => $data_user), REST_Controller::HTTP_OK);
            } else {
                $this->response(array("data" => []), REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(array("data" => []), REST_Controller::HTTP_OK);
        }
    }

    public function cekusername_get()
    {
        $username = $this->get("username");

        $data_user = $this->user_vaksin_model->get(
            array(
                'where' => array(
                    'username' => $username
                )
            ),
            'row'
        );

        $this->response(array("data" => $data_user), REST_Controller::HTTP_OK);
    }

    public function cekusernamebyid_get()
    {
        $username = $this->get("username");
        $id_user_vaksin = $this->get("idUserVaksin");

        $data_user = $this->user_vaksin_model->get(
            array(
                'where' => array(
                    'username' => $username
                ),
                "where_false" => "id_user_vaksin != '" . $id_user_vaksin . "'"
            ),
            'row'
        );

        $this->response(array("data" => $data_user), REST_Controller::HTTP_OK);
    }

    public function postuservaksin_post()
    {
        $data = array( // Automatically generated by the model
            'nama_lengkap' => $this->post('nama_lengkap'),
            'username' => $this->post('username'),
            'email' => $this->post('email'),
            'no_hp' => $this->post('no_hp'),
            'token' => $this->post('token_firebase'),
            'password' => password_hash($this->post('password'), PASSWORD_BCRYPT, array('cost' => 12)),
            'created_at' => date('Y-m-d H:i:s'),
        );

        $status = $this->user_vaksin_model->save($data);

        if ($status) {
            $this->response([
                'status' => true
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function cekusernamesignup_get()
    {
        $username = $this->get("username");
        $email = $this->get("email");
        $no_hp = $this->get("no_hp");

        $data_user_up = [];

        $data_user_email = $this->user_vaksin_model->get(
            array(
                "where" => array(
                    "email" => $email
                )
            ),
            "row"
        );

        $data_user_no_hp = $this->user_vaksin_model->get(
            array(
                "where" => array(
                    "no_hp" => $no_hp
                )
            ),
            "row"
        );

        $data_user_username = $this->user_vaksin_model->get(
            array(
                "where" => array(
                    "username" => $username
                )
            ),
            "row"
        );

        if ($data_user_email) {
            $data_user_email->field_wrong = "Email";
            $data_user_up = $data_user_email;
        } else if ($data_user_no_hp) {
            $data_user_no_hp->field_wrong = "No. HP";
            $data_user_up = $data_user_no_hp;
        } else if ($data_user_username) {
            $data_user_username->field_wrong = "Username";
            $data_user_up = $data_user_username;
        } else {
            $data_user_up = null;
        }

        $this->response(array("data" => $data_user_up), REST_Controller::HTTP_OK);
    }

    function bulan($bln)
    {
        switch ($bln) {
            case 1:
                return "Januari";
                break;
            case 2:
                return "Februari";
                break;
            case 3:
                return "Maret";
                break;
            case 4:
                return "April";
                break;
            case 5:
                return "Mei";
                break;
            case 6:
                return "Juni";
                break;
            case 7:
                return "Juli";
                break;
            case 8:
                return "Agustus";
                break;
            case 9:
                return "September";
                break;
            case 10:
                return "Oktober";
                break;
            case 11:
                return "November";
                break;
            case 12:
                return "Desember";
                break;
        }
    }

    function longdate_indo($tanggal)
    {
        $ubah = gmdate($tanggal, time() + 60 * 60 * 8);
        $pecah = explode("-", $ubah);
        $tgl = $pecah[2];
        $bln = $pecah[1];
        $thn = $pecah[0];
        $bulan = bulan($pecah[1]);

        $nama = date("l", mktime(0, 0, 0, $bln, $tgl, $thn));
        $nama_hari = "";
        if ($nama == "Sunday") {
            $nama_hari = "Minggu";
        } else if ($nama == "Monday") {
            $nama_hari = "Senin";
        } else if ($nama == "Tuesday") {
            $nama_hari = "Selasa";
        } else if ($nama == "Wednesday") {
            $nama_hari = "Rabu";
        } else if ($nama == "Thursday") {
            $nama_hari = "Kamis";
        } else if ($nama == "Friday") {
            $nama_hari = "Jumat";
        } else if ($nama == "Saturday") {
            $nama_hari = "Sabtu";
        }
        return $nama_hari . ', ' . $tgl . ' ' . $bulan . ' ' . $thn;
    }

    public function sendotp_patch()
    {
        $email = $this->patch("email");

        $data_user = $this->user_vaksin_model->get(
            array(
                "where" => array(
                    "email" => $email
                )
            ),
            "row"
        );

        $kode_otp = $this->generateOTP(4);

        $this->load->library('PHPMailer_load'); //Load Library PHPMailer
        $mail = $this->phpmailer_load->load(); // Mendefinisikan Variabel Mail
        $mail->isSMTP();  // Mengirim menggunakan protokol SMTP
        $mail->Host = 'smtp.gmail.com'; // Host dari server SMTP
        $mail->SMTPAuth = true; // Autentikasi SMTP
        $mail->Username = 'kotawaringinbaratkab@gmail.com';
        $mail->Password = 'kobar2018KoBarJaya';
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;
        $mail->setFrom('kotawaringinbaratkab@gmail.com', 'Reset Password Data Vaksin'); // Sumber email
        $mail->addAddress($email, 'Reset Password Data Vaksin'); // Masukkan alamat email dari variabel $email
        $mail->Subject = "Reset Password Data Vaksin"; // Subjek Email
        $mail->msgHtml("Kode OTP : <h2>" . $kode_otp . "</h2>"); // Isi email dengan format HTML


        if (!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
            $this->response([
                'data' => false
            ], REST_Controller::HTTP_OK);
        } else {
            $data_kode_otp_user = array(
                "kode_otp" => $kode_otp
            );

            $status = $this->user_vaksin_model->edit($data_user->id_user_vaksin, $data_kode_otp_user);
            if ($status) {
                $this->response([
                    'data' => true
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'data' => false
                ], REST_Controller::HTTP_OK);
            }
        }
    }

    function generateOTP($n)
    {

        $generator = "1357902468";

        $result = "";

        for ($i = 1; $i <= $n; $i++) {
            $result .= substr($generator, (rand() % (strlen($generator))), 1);
        }
        return $result;
    }

    public function datajenisvaksin_get()
    {
        $daftar_vaksin = $this->jenis_vaksin_model->get(
            array(
                "fields" => "id_jenis_vaksin AS id,nama_vaksin AS nama",
                "order_by" => array(
                    "nama_vaksin" => "DESC"
                )
            )
        );

        $this->response(array("data" => $daftar_vaksin), REST_Controller::HTTP_OK);
    }

    public function ceklistpeserta_get()
    {
        $id_user_vaksin = $this->get("id_user_vaksin");

        $daftar_vaksin = $this->daftar_vaksin_model->get(
            array(
                "fields" => "COUNT(*) AS jumlah_peserta",
                "where" => array(
                    "id_user_created" => $id_user_vaksin
                )
            ),
            "row"
        );

        if ($daftar_vaksin->jumlah_peserta >= "15") {
            $status = "false";
        } else {
            $status = "true";
        }

        $this->response(array("data" => $status), REST_Controller::HTTP_OK);
    }

    public function listpuskesmas_get()
    {
        $list_puskesmas = $this->master_puskesmas_model->get(
            array(
                "order_by" => array(
                    "nama_puskesmas" => "ASC"
                )
            )
        );

        $templist = array();
        foreach ($list_puskesmas as $key => $row) {
            foreach ($row as $keys => $rows) {
                if (in_array($keys, array('nama_puskesmas', ''))) {
                    $templist[$key][$keys] = $rows;
                }
            }

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_puskesmas);
        }

        $this->response(array("data" => $templist), REST_Controller::HTTP_OK);
    }

    public function getdatapesertavaksin_get()
    {
        $puskesmas = decrypt_data($this->get("puskesmas"));
        $nik = $this->get("nik");

        if ($nik) {
            $wh = "";
            if ($puskesmas) {
                $wh = "kelurahan_master_wilayah_id IN (SELECT id_master_wilayah FROM master_wilayah
                WHERE puskesmas_id = '" . $puskesmas . "') AND (is_undangan_prioritas IS NULL OR is_undangan_prioritas = '2')";
            }
            $peserta = $this->daftar_vaksin_model->get(
                array(
                    "fields" => "nama,DATE_FORMAT(tanggal_lahir,'%d-%m-%Y') AS tanggal_lahir,nama_wilayah,rt,no_antrian,hasil_skrining_petugas,tanggal_vaksin,jam_vaksin,
                    IF(hasil_skrining_petugas='1',
                    UNIX_TIMESTAMP(DATE_FORMAT(
                        IF(before_after_schedule_vaksin = '1',
                            IF(satuan_interval = 'HOUR',
                            DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                            DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                            ),
                            IF(satuan_interval = 'HOUR',
                            DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                            DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                            )
                        ), '%Y-%m-%d %H:%i:%s')
                        ),''
                    ) AS timestamp_tanggal_jam_vaksin,
                    is_confirm,
                    IF(hasil_skrining_petugas = '1', 'Lanjut Vaksin', 
                    IF(hasil_skrining_petugas = '2', 'Ditunda', 
                    IF(hasil_skrining_petugas = '3', 'Tidak Diberikan', 'Belum Diskrining'))) AS hasil_skrining_petugas_label",
                    "join" => array(
                        "master_wilayah" => "id_master_wilayah=kelurahan_master_wilayah_id",
                        "master_rt" => "id_master_rt=master_rt_id",
                        "master_puskesmas" => "puskesmas_id=id_master_puskesmas"
                    ),
                    "left_join" => array(
                        "jenis_vaksin" => "id_jenis_vaksin=jenis_vaksin_id_vaksin_1",
                        "jadwal_vaksin" => "id_jadwal_vaksin=jadwal_vaksin_id",
                    ),
                    "where" => array(
                        "nik" => $nik,
                    ),
                    "where_false" => $wh,
                    "order_by" => array(
                        "ISNULL(jadwal_vaksin_id), jadwal_vaksin_id" => "DESC",
                        "ISNULL(no_antrian), CAST(no_antrian AS UNSIGNED)" => "ASC",
                    )
                )
            );
        } else {
            $peserta = $this->daftar_vaksin_model->get(
                array(
                    "fields" => "nama,DATE_FORMAT(tanggal_lahir,'%d-%m-%Y') AS tanggal_lahir,nama_wilayah,rt,no_antrian,hasil_skrining_petugas,tanggal_vaksin,jam_vaksin,
                    IF(hasil_skrining_petugas='1',
                    UNIX_TIMESTAMP(DATE_FORMAT(
                        IF(before_after_schedule_vaksin = '1',
                            IF(satuan_interval = 'HOUR',
                            DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                            DATE_SUB(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                            ),
                            IF(satuan_interval = 'HOUR',
                            DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval HOUR) , 
                            DATE_ADD(CONCAT(tanggal_vaksin,' ',jam_vaksin),INTERVAL time_interval MINUTE)
                            )
                        ), '%Y-%m-%d %H:%i:%s')
                        ),''
                    ) AS timestamp_tanggal_jam_vaksin,
                    is_confirm,
                    IF(hasil_skrining_petugas = '1', 'Lanjut Vaksin', 
                    IF(hasil_skrining_petugas = '2', 'Ditunda', 
                    IF(hasil_skrining_petugas = '3', 'Tidak Diberikan', 'Belum Diskrining'))) AS hasil_skrining_petugas_label",
                    "join" => array(
                        "master_wilayah" => "id_master_wilayah=kelurahan_master_wilayah_id",
                        "master_rt" => "id_master_rt=master_rt_id",
                        "master_puskesmas" => "puskesmas_id=id_master_puskesmas"
                    ),
                    "left_join" => array(
                        "jenis_vaksin" => "id_jenis_vaksin=jenis_vaksin_id_vaksin_1",
                        "jadwal_vaksin" => "id_jadwal_vaksin=jadwal_vaksin_id",
                    ),
                    "where" => array(
                        "hasil_skrining_petugas" => "1",
                    ),
                    "where_false" => "kelurahan_master_wilayah_id IN (SELECT id_master_wilayah FROM master_wilayah
                    WHERE puskesmas_id = '" . $puskesmas . "') AND (is_undangan_prioritas IS NULL OR is_undangan_prioritas = '2')",
                    "order_by" => array(
                        "ISNULL(jadwal_vaksin_id), jadwal_vaksin_id" => "DESC",
                        "ISNULL(no_antrian), CAST(no_antrian AS UNSIGNED)" => "ASC",
                    )
                )
            );
        }

        $templist = array();
        foreach ($peserta as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            if ($row->tanggal_vaksin) {
                $templist[$key]['tanggal_pelaksanaan_custom'] = longdate_indo($row->tanggal_vaksin) . ", " . date("H:i", strtotime($row->jam_vaksin));
            } else {
                $templist[$key]['tanggal_pelaksanaan_custom'] = "";
            }

            $tanggal_pelaksanaan_vaksin = strtotime($row->tanggal_vaksin . " " . $row->jam_vaksin);

            $timestamp_tanggal_jam_now = strtotime(date("Y-m-d H:i:s"));

            if ($row->hasil_skrining_petugas == '1') {
                if ($row->is_confirm == '1') {
                    $templist[$key]['status_konfirmasi'] = 'Hadir';
                } else if ($row->is_confirm == '2') {
                    $templist[$key]['status_konfirmasi'] = 'Tidak Hadir';
                } else if (!$row->is_confirm && $timestamp_tanggal_jam_now < $row->timestamp_tanggal_jam_vaksin) {
                    $templist[$key]['status_konfirmasi'] = 'Belum Konfirmasi';
                } else if (!$row->is_confirm && $timestamp_tanggal_jam_now > $row->timestamp_tanggal_jam_vaksin) {
                    $templist[$key]['status_konfirmasi'] = 'Tidak Hadir';
                }

                if ($row->is_confirm == '1' && $timestamp_tanggal_jam_now > $tanggal_pelaksanaan_vaksin) {
                    $templist[$key]['status_sudah_vaksin'] = 'Sudah';
                } else if ($row->is_confirm == '1' && $timestamp_tanggal_jam_now < $tanggal_pelaksanaan_vaksin) {
                    $templist[$key]['status_sudah_vaksin'] = 'Belum';
                } else if ($row->is_confirm == '2') {
                    $templist[$key]['status_sudah_vaksin'] = '';
                } else if (!$row->is_confirm) {
                    $templist[$key]['status_sudah_vaksin'] = '';
                }
            } else if ($row->hasil_skrining_petugas == '2') {
                $templist[$key]['status_konfirmasi'] = '';
                $templist[$key]['status_sudah_vaksin'] = '';
            } else if ($row->hasil_skrining_petugas == '3') {
                $templist[$key]['status_konfirmasi'] = '';
                $templist[$key]['status_sudah_vaksin'] = '';
            } else if (!$row->hasil_skrining_petugas) {
                $templist[$key]['status_konfirmasi'] = '';
                $templist[$key]['status_sudah_vaksin'] = '';
            }
        }

        $this->response(array("data" => $templist), REST_Controller::HTTP_OK);
    }
}
