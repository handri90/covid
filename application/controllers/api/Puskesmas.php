<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Puskesmas extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('master_puskesmas_model');
    }

    public function data_get()
    {
        $list_puskesmas = $this->master_puskesmas_model->get(
            array(
                "fields" => "id_puskesmas AS id, nama_puskesmas AS nama",
                "order_by" => array(
                    "nama_puskesmas" => "ASC"
                )
            )
        );

        $templist = array();
        foreach ($list_puskesmas as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            // $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_puskesmas);
        }

        $this->response(array("data" => $templist), REST_Controller::HTTP_OK);
    }
}
