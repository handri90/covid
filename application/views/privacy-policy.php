<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    Privacy Policy



    "Karamunting Wallpaper" team has adopted this privacy policy (“Privacy Policy”) to explain how "Karamunting
    Wallpaper" collects, stores, and uses the information collected in connection with "Karamunting Wallpaper"’s
    Services.



    BY INSTALLING, USING, REGISTERING TO OR OTHERWISE ACCESSING THE SERVICES, YOU AGREE TO THIS PRIVACY POLICY AND GIVE
    AN EXPLICIT AND INFORMED CONSENT TO THE PROCESSING OF YOUR PERSONAL DATA IN ACCORDANCE WITH THIS PRIVACY POLICY. IF
    YOU DO NOT AGREE TO THIS PRIVACY POLICY, PLEASE DO NOT INSTALL, USE, REGISTER TO OR OTHERWISE ACCESS THE SERVICES.
    "Karamunting Wallpaper" reserves the right to modify this Privacy Policy at reasonable times, so please review it
    frequently. If "Karamunting Wallpaper" makes material or significant changes to this Privacy Policy, "Karamunting
    Wallpaper" may post a notice on "Karamunting Wallpaper"'s website along with the updated Privacy Policy. Your
    continued use of Services will signify your acceptance of the changes to this Privacy Policy.











    Changes to this privacy policy





    The Data Controller reserves the right to make changes to this privacy policy at any time by giving notice to its
    Users on this page. It is strongly recommended to check this page often, referring to the date of the last
    modification listed at the bottom. If a User objects to any of the changes to the Policy, the User must cease using
    this Application and can request that the Data Controller erase the Personal Data. Unless stated otherwise, the
    then-current privacy policy applies to all Personal Data the Data Controller has about Users.



    This Application collects some Personal Data from its Users.



    Types of Data collected





    Among the types of Personal Data that this Application collects, by itself or through third parties, there are:
    Geographic position, Cookie and Usage Data. Other Personal Data collected may be described in other sections of this
    privacy policy or by dedicated explanation text contextually with the Data collection. The Personal Data may be
    freely provided by the User, or collected automatically when using this Application. Any use of Cookies - or of
    other tracking tools - by this Application or by the owners of third party services used by this Application, unless
    stated otherwise, serves to identify Users and remember their preferences, for the sole purpose of providing the
    service required by the User. Failure to provide certain Personal Data may make it impossible for this Application
    to provide its services. The User assumes responsibility for the Personal Data of third parties published or shared
    through this Application and declares to have the right to communicate or broadcast them, thus relieving the Data
    Controller of all responsibility.







    Mode and place of processing the Data

    Methods of processing





    The Data Controller processes the Data of Users in a proper manner and shall take appropriate security measures to
    prevent unauthorized access, disclosure, modification, or unauthorized destruction of the Data. The Data processing
    is carried out using computers and/or IT enabled tools, following organizational procedures and modes strictly
    related to the purposes indicated. In addition to the Data Controller, in some cases, the Data may be accessible to
    certain types of persons in charge, involved with the operation of the site (administration, sales, marketing,
    legal, system administration) or external parties (such as third party technical service providers, mail carriers,
    hosting providers, IT companies, communications agencies) appointed, if necessary, as Data Processors by the Owner.
    The updated list of these parties may be requested from the Data Controller at any time.







    Retention time





    The Data is kept for the time necessary to provide the service requested by the User, or stated by the purposes
    outlined in this document, and the User can always request that the Data Controller suspend or remove the data.



    The use of the collected Data





    The Data concerning the User is collected to allow the Application to provide its services, as well as for the
    following purposes: Access to third party services' accounts, Location-based interactions, Content commenting and
    Interaction with external social networks and platforms. The Personal Data used for each purpose is outlined in the
    specific sections of this document.



    Detailed information on the processing of Personal Data

    Personal Data is collected for the following purposes and using the following services:



    Access to third party services' accounts





    These services allow this Application to access Data from your account on a third party service and perform actions
    with it. These services are not activated automatically, but require explicit authorization by the User.







    Legal Action





    The User's Personal Data may be used for legal purposes by the Data Controller, in Court or in the stages leading to
    possible legal action arising from improper use of this Application or the related services. The User is aware of
    the fact that the Data Controller may be required to reveal personal data upon request of public authorities.







    Additional information about User's Personal Data





    In addition to the information contained in this privacy policy, this Application may provide the User with
    additional and contextual information concerning particular services or the collection and processing of Personal
    Data upon request.







    System Logs and Maintenance





    For operation and maintenance purposes, this Application and any third party services may collect files that record
    interaction with this Application (System Logs) or use for this purpose other Personal Data (such as IP Address).







    Information not contained in this policy





    More details concerning the collection or processing of Personal Data may be requested from the Data Controller at
    any time. Please see the contact information at the beginning of this document.



    Changes to this privacy policy





    The Data Controller reserves the right to make changes to this privacy policy at any time by giving notice to its
    Users on this page. It is strongly recommended to check this page often, referring to the date of the last
    modification listed at the bottom. If a User objects to any of the changes to the Policy, the User must cease using
    this Application and can request that the Data Controller erase the Personal Data. Unless stated otherwise, the
    then-current privacy policy applies to all Personal Data the Data Controller has about Users.



    Personal Data (or Data)





    Any information regarding a natural person, a legal person, an institution or an association, which is, or can be,
    identified, even indirectly, by reference to any other information, including a personal identification number.







    Usage Data





    Information collected automatically from this Application (or third party services employed in this Application ),
    which can include: the IP addresses or domain names of the computers utilized by the Users who use this Application,
    the URI addresses (Uniform Resource Identifier), the time of the request, the method utilized to submit the request
    to the server, the size of the file received in response, the numerical code indicating the status of the server's
    answer (successful outcome, error, etc.), the country of origin, the features of the browser and the operating
    system utilized by the User, the various time details per visit (e.g., the time spent on each page within the
    Application) and the details about the path followed within the Application with special reference to the sequence
    of pages visited, and other parameters about the device operating system and/or the User's IT environment. Some
    third parties collect other data from users for some particular purposes.For more informations; check the links down
    below ; as following :



    Google Play Services

    AdMob





    If you have any other questions, please contact us at: info@karamunting.com
</body>

</html>