<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">

	<title>Covid 19 - Kotawaringin Barat</title>
	<meta content="" name="description">

	<meta content="" name="keywords">

	<!-- Favicons -->
	<link href="<?php echo base_url(); ?>assets/template_login/img/favicon.png" rel="icon">

	<!-- Google Fonts -->
	<link href="<?php echo base_url(); ?>assets/template_login/css/font-family.css" rel="stylesheet">

	<!-- Vendor CSS Files -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template_login/vendor/bootstrap4/css/bootstrap.min.css">
	<link href="<?php echo base_url(); ?>assets/template_login/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/template_login/vendor/aos/aos.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/template_admin/css/HoldOn.min.css" rel="stylesheet" />

	<!-- Template Main CSS File -->
	<link href="<?php echo base_url(); ?>assets/template_login/css/style.css" rel="stylesheet">
	<script>
		var base_url = "<?php echo base_url(); ?>";
	</script>

	<style>
		.container-map {
			max-width: 800px;
			margin: auto;
		}
	</style>
</head>

<body data-bs-spy="scroll" data-bs-target="#navbar" data-bs-offset="100">

	<!-- ======= Header ======= -->
	<header id="header" class="header fixed-top">
		<div class="container-fluid container-xl d-flex align-items-center justify-content-between">

			<a href="index.html" class="logo d-flex align-items-center">
				<span>Covid 19</span>
			</a>

			<nav id="navbar" class="navbar">
				<ul>
					<li><a class="getstarted scrollto" href="#myModal" data-toggle="modal">Login</a></li>
				</ul>
				<i class="bi bi-list mobile-nav-toggle"></i>
			</nav><!-- .navbar -->

		</div>
	</header><!-- End Header -->

	<section id="hero" class="hero d-flex align-items-center">

		<div class="container">
			<div class="row">
				<div class="col-lg-8 d-flex flex-column justify-content-center">
					<h1 data-aos="fade-up" data-aos-delay="400" class="text-center">Sistem Informasi Covid 19</h1>
					<h1 data-aos="fade-up" data-aos-delay="400" class="text-center">Kabupaten Kotawaringin Barat</h1>
				</div>
				<div class="col-lg-4 hero-img" data-aos="zoom-out" data-aos-delay="200">
					<img src="<?php echo base_url(); ?>assets/template_login/img/covid.png" class="img-fluid" alt="">
				</div>
			</div>
		</div>

	</section><!-- End Hero -->

	<!-- ======= Footer ======= -->
	<footer id="footer" class="footer">

		<div class="footer-top">
			<div class="container">
				<div class="row gy-4">
					<div class="col-lg-5 col-md-12 footer-info">
						<a href="index.html" class="logo d-flex align-items-center">
							<span>Covid 19</span>
						</a>
						<p>Situs ini dibuat dan dikelola oleh Dinas Komunikasi Informatika Statistik dan Persandian Kabupaten Kotawaringin Barat.</p>
					</div>

					<div class="col-lg-4 col-6 footer-links">
						<h4>Halaman Terkait</h4>
						<ul>
							<li><i class="bi bi-chevron-right"></i> <a target="_blank" href="http://portal.kotawaringinbaratkab.go.id/">Kabupaten Kotawaringin Barat</a></li>
							<li><i class="bi bi-chevron-right"></i> <a target="_blank" href="https://mmc.kotawaringinbaratkab.go.id/">MMC Kotawaringin Barat</a></li>
						</ul>
					</div>

					<div class="col-lg-3 col-md-12 footer-contact text-center text-md-start">
						<h4>Alamat</h4>
						<p>
							Jalan Sutan Syahrir No. 62 <br>
							Arut Selatan, 74112<br>
							Kotawaringin Barat <br><br>
						</p>

					</div>

				</div>
			</div>
		</div>

		<div class="container">
			<div class="copyright">
				Copyright @ 2021 <strong><span>Diskominfo Kotawaringin Barat</span></strong>
			</div>
		</div>
	</footer><!-- End Footer -->

	<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

	<script src="<?php echo base_url(); ?>assets/template_login/vendor/jquery/jquery-3.5.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_login/vendor/bootstrap4/js/bootstrap.min.js"></script>
	<!-- Vendor JS Files -->
	<script src="<?php echo base_url(); ?>assets/template_login/vendor/bootstrap/js/bootstrap.bundle.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_login/vendor/aos/aos.js"></script>

	<!-- Template Main JS File -->
	<script src="<?php echo base_url(); ?>assets/template_login/js/main.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_admin/js/HoldOn.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js" charset="utf-8"></script>
	<script src="<?php echo base_url(); ?>assets/template_login/js/jquery.mapael.js" charset="utf-8"></script>
	<script src="<?php echo base_url(); ?>assets/template_login/js/kobar.js" charset="utf-8"></script>

	<div id="myModal" class="modal fade">
		<div class="modal-dialog modal-login">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Login</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<span class='error_field'></span>
					<div class="form-group">
						<label>Username</label>
						<input type="text" class="form-control" required="required" name="username">
					</div>
					<div class="form-group">
						<div class="clearfix">
							<label>Password</label>
						</div>

						<input type="password" class="form-control" required="required" name="password">
					</div>
				</div>
				<div class="modal-footer justify-content-between">
					<input type="submit" class="btn btn-primary" value="Login" onclick="check_auth()">
				</div>
			</div>
		</div>
	</div>

	<div id="modal-info-covid" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Info Covid</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="close_modal_info()">&times;</button>
				</div>
				<div class="modal-body">

				</div>
			</div>
		</div>
	</div>

</body>

</html>

<script>
	let optionsHoldOn = {
		theme: "sk-cube-grid",
		message: 'Loading',
		textColor: "white"
	};

	function check_auth() {
		let username = $("input[name='username']").val();
		let password = $("input[name='password']").val();
		$(".error_field").html("");

		if (!username) {
			$(".error_field").html("<div class='alert alert-danger'>Field username tidak boleh kosong</div>");
		} else if (!password) {
			$(".error_field").html("<div class='alert alert-danger'>Field Password tidak boleh kosong</div>");
		} else {
			$.ajax({
				url: base_url + 'Login/act_login',
				data: {
					username: username,
					password: password
				},
				type: 'POST',
				beforeSend: function() {
					HoldOn.open(optionsHoldOn);
				},
				success: function(response) {
					if (response == true) {
						$(".error_field").html("<div class='alert alert-success'>Berhasil...</div>");
						location.reload();
					} else {
						$(".error_field").html("<div class='alert alert-danger'>Username dan Password salah</div>");
					}
				},
				complete: function() {
					HoldOn.close();
				}
			});
		}
	}

	$(function() {

		$('#myModal').on('keypress', function(event) {
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if (keycode == '13') {
				check_auth();
			}
		});

		$(".mapcontainer").mapael({
			map: {
				// Set the name of the map to display
				name: "kobar",
				defaultArea: {
					eventHandlers: {
						click: function(e, id, mapElem, textElem, elemOptions) {
							get_detail_covid(id);
						}
					}
				}
			},
			areas: {
				"kolam": {
					attrs: {
						fill: "#3be8b0"
					},
					attrsHover: {
						fill: "#7efcd3"
					},
					text: {
						content: "Kotawaringin Lama",
						attrs: {
							"font-size": 6,
							"font-weight": "bold",
							fill: "#000",
							"position": "right"
						}
					}
				},
				"arsel": {
					attrs: {
						fill: "#1aafd0"
					},
					attrsHover: {
						fill: "#6bd9f1"
					},
					text: {
						content: "Arut Selatan",
						attrs: {
							"font-size": 6,
							"font-weight": "bold",
							fill: "#000",
							"position": "right"
						}
					}
				},
				"aruta": {
					attrs: {
						fill: "#6a67ce"
					},
					attrsHover: {
						fill: "#9e9cf8"
					},
					text: {
						content: "Arut Utara",
						attrs: {
							"font-size": 6,
							"font-weight": "bold",
							fill: "#000",
							"position": "right"
						}
					}
				},
				"banteng": {
					attrs: {
						fill: "#ffb900"
					},
					attrsHover: {
						fill: "#fcda7f"
					},
					text: {
						content: "Pangkalan Banteng",
						attrs: {
							"font-size": 6,
							"font-weight": "bold",
							fill: "#000",
							"position": "right"
						}
					}
				},
				"lada": {
					attrs: {
						fill: "#00ad45"
					},
					attrsHover: {
						fill: "#5ecc62"
					},
					text: {
						content: "Pangkalan Lada",
						attrs: {
							"font-size": 6,
							"font-weight": "bold",
							fill: "#000",
							"position": "right"
						}
					}
				},
				"kumai": {
					attrs: {
						fill: "#fc636b"
					},
					attrsHover: {
						fill: "#f89da1"
					},
					text: {
						content: "Kumai",
						attrs: {
							"font-size": 6,
							"font-weight": "bold",
							fill: "#000",
						},
						position: "center"
					}
				}
			}
		});
	});

	function get_detail_covid(id) {
		$("#modal-info-covid").modal("show");
	}

	function close_modal_info() {
		$("#modal-info-covid").modal("toggle");
	}
</script>